<?

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * "Принт" переменной
 *
 * @global CUser $USER
 *
 * @param mixed $var переменная для debug
 *
 * @param boolean $vd использовать var_dump для debug
 *
 * @param boolean $tofile вывод в файл $_SERVER['DOCUMENT_ROOT'] . "/debug.txt"
 *
 * @param boolean $onlyAdmin доступна ли функция только для админа
 */
function dm($var, $vd = false, $tofile = false, $onlyAdmin = true)
{

    global $USER;

    if ($onlyAdmin && !$USER->IsAdmin()) {
        return;
    }

    if ($tofile) {
        ob_start();
    }

    echo "<pre>";

    if ($vd) {
        var_dump($var);
    } else {
        print_r($var);
    }

    echo "</pre>";

    if ($tofile) {
        file_put_contents(Bitrix\Main\Application::getDocumentRoot() . "/debug.txt", ob_get_clean());
    }

}

/**
 * @param $imgArrayID
 * @param array $resize
 * @param string $noPhoto
 * @param int $count
 * @return array
 */
function getSrc($imgArrayID, $resize = array(), $noPhoto = "", $count = -1)
{
    $imgArrayID = (array)$imgArrayID;

    $src = [];
    if (isset($imgArrayID) && !empty($imgArrayID)) {
        foreach ($imgArrayID as $k => $img) {
            if ($count == $k) {
                break;
            }
            $img = (int)$img;
            if ($img > 0) {

                if ($resize['width'] > 0 && $resize['height'] > 0) {
                    $file = CFile::ResizeImageGet($img, $resize, BX_RESIZE_IMAGE_EXACT, true);
                    $src[] = $file['src'];
                } else {
                    $file = CFile::GetFileArray($img);
                    $src[] = $file['SRC'];
                }
            }
        }
    }
    if (empty($src)) {
        $src[] = $noPhoto;
    }
    return $src;
}

/**
 * Дынные вводить в формате 3.5
 * @param $rating
 * @return string
 */
function star_rating($rating)
{
    $full_stars = floor($rating);
    $half_stars = ceil($rating - $full_stars);
    $empty_stars = 5 - $full_stars - $half_stars;

    $output = '<div class="rating"><div class="br-wrapper br-theme-fontawesome-stars-o"><div class="br-widget br-readonly">';
    $output .= str_repeat('<a class="br-selected"></a> ', $full_stars);
    $output .= str_repeat('<a></a> ', $half_stars);
    $output .= str_repeat('<a></a> ', $empty_stars);

    $output .= '</div></div></div>';

    return $output;
}

/**
 * Обёртка для substr
 * @param $str
 * @param null $nos
 * @return string
 */
function substr2($str, $nos = null)
{

    $str = strip_tags($str);

    if ($nos === null || strlen($str) <= $nos) return $str;

    return substr($str, 0, $nos) . "...";
}

/**
 * Обёртка для implode
 * @var array $arr
 * @return string
 */
function implode2 ($arr, $delimiter = ', ', $no_strip = false) {

    return   implode(
        $delimiter,
        array_map(
            function ($it) {

                return $no_strip ? $it : strip_tags($it);

            }, array_filter(
                $arr,
                function ($el) { return ($el && !empty($el)); }
            )
        )
    );
}

function num2word($num, $words, $show_num = true)
{
    $num_text = '';
    $num = $num % 100;
    if ($num > 19) {
        $num = $num % 10;
    }

    if($show_num){
        $num_text = $num." ";
    }
    else{
        $num_text = "";
    }
    switch ($num) {
        case 1: {
            return $num_text.$words[0];
        }
        case 2: case 3: case 4: {
        return $num_text.$words[1];
    }
        default: {
            return $num_text.$words[2];
        }
    }
}


/**
 * Получаем поля элемента по его ID
 * @var integer $id
 * @return mixed
 */
function getIBElement($id) {

    if (!($res = getIBElementFields($id)))
        return false;

    $arr = $res;

    if (!($res = getIBElementProperties($id)))
        return false;

    $arr['PROPERTIES'] = $res;

    return $arr;
}

// только поля
function getIBElementFields($id) {

    if (!($res = CIBlockElement::GetByID($id)->GetNextElement()))
        return false;

    return $res->GetFields();
}

// только свойства
function getIBElementProperties($id) {

    if (!($res = CIBlockElement::GetByID($id)->GetNextElement()))
        return false;

    return $res->GetProperties();
}

function getIbPropertyIdByCode($iblock_id, $property_code) {

    \Bitrix\Main\Loader::includeModule("iblock");

    $result = null;

    for ($i = 0, $cnt = count($property_code); $i < $cnt; $i++) {

        $db_properties = \CIBlockProperty::GetList(array("ID" => "DESC"), array("IBLOCK_ID" => $iblock_id, "CODE" => $property_code[$i]));

        while ($property = $db_properties->Fetch()) {
            $result[$property["CODE"]] = $property["ID"];
        }
    }

    return $result;
}

function getIBElementsFieldsProperties($request, $prop = false) {

    \Bitrix\Main\Loader::includeModule("iblock");
    $res = \CIBlockElement::GetList(array(), $request["filter"], false, false, $request["select"]);
    /* if($res->SelectedRowsCount() < 1)
      return false; */

    $data = array();
    while ($fields = $res->GetNextElement()) {
        $data[] = $fields->GetFields();
        if ($prop) {
            $key = end(array_keys($data));
            $data[$key]["PROPERTIES"] = $fields->GetProperties();
        }
    }

    return $data;
}

/**
 * @param int $iblock_id
 * @return array
 */
function getMainSearchFormsIblockItems(int $iblock_id, array $additional_filter = []) {
    \Bitrix\Main\Loader::includeModule("iblock");

    $arFilter = array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y");
    if (!empty($additional_filter)) {
        $arFilter = array_merge($arFilter, $additional_filter);
    }

    $arResult = array();
    $cache = \Bitrix\Main\Data\Cache::createInstance();

    $cache_id = "main_search_form_items_iblock_" . md5(serialize($arFilter));

    $cache_dir = "/travelsoft/main_search_from_items_iblock_" . $iblock_id;

    if ($cache->initCache(360000000, $cache_id, $cache_dir)) {
        $arResult = $cache->getVars();
    } elseif ($cache->startDataCache()) {
        $arResult = \getIBElementsFieldsProperties(array(
            "filter" => $arFilter,
            "select" => array("NAME", "ID", "DETAIL_PAGE_URL")
        ), true);

        if (!empty($arResult)) {
            $cache->endDataCache($arResult);
        } else {
            $cache->abortDataCache();
        }

        if (defined('BX_COMP_MANAGED_CACHE')) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblock_id);
            $CACHE_MANAGER->EndTagCache();
        }
    }

    return $arResult;
}