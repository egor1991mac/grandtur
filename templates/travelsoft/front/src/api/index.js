import {stringify} from 'qs';
//import regeneratorRuntime from "regenerator-runtime";




function  _uniqData(data){
    return data.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    });
};
function  _createURL(string){
    let { ajaxUrl, body, sessid } = JSON.parse(string);
    body.travelbooking = JSON.parse(body.travelbooking);
    return `${ajaxUrl}/?${$.param(body)}`;

};
function _groupData(data){



        let newData = {};

        data.forEach(item=>{
            if(item.CITY == null || item.CITY == undefined){
                item.CITY = {};
                item.CITY.NAME = "CITY";
            }
        });

        let date = _uniqData(data.map(item => item.DATE_FROM));
        let city = _uniqData(data.map(item => item.CITY.NAME));
        //console.log(city);
        date.forEach(DATE => {
            newData[DATE] = {};
            city.forEach(CITY =>{
                newData[DATE][CITY] = data.filter(item => item.CITY.NAME  == CITY);
            })
        })

        return newData;


}

export {_uniqData, _createURL, _groupData};

/*
export default class API{



    _createURL(string){
        let { ajaxUrl, body, sessid } = JSON.parse(string);
        body.travelbooking = JSON.parse(body.travelbooking);
        return `${ajaxUrl}/?${$.param(body)}`;

    };

    async _fetchData(url){
        let response =  await fetch(url,myInit);
        return await response;

        /*
            .then(response => response.json())
            .then(response =>this._groupeData(response.data))
            .then(response => this.data = response);

    };

    _groupeData(data){
        if(data.length != 0  || Object.keys(data).length !=0){

            let newData = {};

            data.forEach(item=>{
                if(item.CITY == null || item.CITY == undefined){
                    item.CITY.NAME = "CITY";
                }
            });

            let date = this._uniqData(data.map(item => item.DATE_FROM));
            let city = this._uniqData(data.map(item => item.CITY.NAME));
            console.log(city);
            date.forEach(DATE => {
                newData[DATE] = {};
                city.forEach(CITY =>{
                    newData[DATE][CITY] = data.filter(item => item.CITY.NAME  == CITY);
                })
            })



        }

    };


}*/

