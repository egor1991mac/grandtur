import React, { Component, lazy, Suspense } from 'react';
import axios from 'axios';
const Table = lazy(() => import('./table/index.js'));

//..comoponent
import CircularProgress from '@material-ui/core/CircularProgress';

//..api
import {_groupData,_createURL} from "../api";
import regeneratorRuntime from "regenerator-runtime";
//..style
//import '../assets/css/template.scss';






var myInit = { method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    mode: 'no-cors',
    cache: 'no-cache'
};



export default class App extends Component {
    constructor(props){
        super(props);
        this.state= {
            data: null,
            loading: false
        }
    }

    async componentDidMount(){
        let data = localStorage.getItem('config');
        let url = _createURL(data);
        console.log(url);
        let { ajaxUrl, body, sessid } = JSON.parse(document.querySelector('#config_result-form').getAttribute('data-params'));

        let newUrl = `${ajaxUrl}/?${$.param(body)}`;
        console.log(newUrl,'url');
        console.log(url == newUrl);
        this.setState({loading:true});

        /*
        await fetch(url,myInit).then(response=>response.distjson()).then(response =>{

            this.setState({
                data: _groupData(response.data),
                loading:false,
                session: JSON.parse(data).body.sessid,
                people: {
                    adults: JSON.parse(JSON.parse(data).body.travelbooking).adults,
                    children: JSON.parse(JSON.parse(data).body.travelbooking).children,
                    children_age:JSON.parse(JSON.parse(data).body.travelbooking).children_age
                }
            })
        });*/

        //console.log(axios.get(url));
        let result = axios.get(newUrl);
        result.then(result => result.data).then(response =>
            this.setState({
                data: _groupData(response.data),
                loading:false,
                session: JSON.parse(data).body.sessid,
                people: {
                    adults: JSON.parse(JSON.parse(data).body.travelbooking).adults,
                    children: JSON.parse(JSON.parse(data).body.travelbooking).children,
                    children_age:JSON.parse(JSON.parse(data).body.travelbooking).children_age
                }
            })

        )


    }

    render() {
            const {loading, data} = this.state;


            if(loading == true){
                return (
                    <div className="ts-wrap">
                        <div className="ts-row ts-justify-content__center ts-align-items__center">
                            <CircularProgress disableShrink />
                           <span className="ts-px-1">Поиск результатов</span>
                        </div>
                    </div>
                )
            }
            else {
                return (
                    <Suspense fallback={<div>...Loading...</div>}>
                        {
                           data != null && Object.keys(data).length !== 0 ? <Table {...this.state} />
                               : <div>Поиск не дал результатов. Попробуйте выбрать другие параметры</div>
                        }
                    </Suspense>
                )
            }
    }
}
