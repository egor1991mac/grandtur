import React, { Component, lazy, Suspense } from 'react';


import '../../assets/js/proslider.js';


export default class TemplateSlider extends Component{
    constructor(props){
        super(props);
    }

   componentDidMount(){

       $( '#my-slider' ).sliderPro({
           width: 960,
           height: 500,
           arrows: true,
           buttons: false,

           fade: true,

       });

   }

    render(){
        const {IMG}  = this.props;
        const styleImg = {width:'100%', height:'100%'};
        const styleThumbnail = {width:'100%', height:'100%',maxHeight:'60px'};

        return (
                <div className="slider-pro" id="my-slider">
                    <div className="sp-slides">
                            {
                                IMG.map((item)=>
                                    <div key={item} className="sp-slide">
                                            <img src={item} style={styleImg}/>
                                    </div>
                                )
                        }
                        </div>
                    <div className="sp-thumbnails">
                            {
                                IMG.map((item)=>
                                    <div key={item + 'thumbnail'} className="sp-thumbnail">
                                        <img src={item} style={styleThumbnail}/>
                                    </div>
                                )
                            }
                    </div>
              </div>
        )
    }


}