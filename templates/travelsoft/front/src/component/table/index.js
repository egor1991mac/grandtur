
import React, { Component, lazy, Suspense } from 'react';
import './style.scss';
const Dialog = lazy(() => import('../dialog/index.js'));
const Tooltip = lazy (()=>import('../tooltip/index.js'));



export default class TicketResult extends Component {
  render() {
      const {data, people, session
      } = this.props;
      const DATE = Object.keys(data);

            return (
                <div className="ts-wrap offer-box">
                    {
                        DATE.map((date, d_index) =>
                            <div key={date} className={'tour-package'}>
                                {
                                    Object.keys(data[date]).map((city, c_index) =>

                                        <div key={city}
                                             className={`ts-row ${ d_index != DATE.length - 1 ? 'ts-border-bottom' : ''} ${ c_index % 2 == 0 ? 'ts-background__light-gray' : ''} `}>
                                            <div className="ts-col-24 ts-col-md-3 ts-justify-content__center">
                                                <ul>
                                                    <li>Выезд:</li>
                                                    <li>{date}</li>
                                                    <li>{city !== 'CITY' ? city : null}</li>
                                                </ul>
                                            </div>
                                            <div className={"ts-col-24 ts-col-md-21"}>
                                                {
                                                    data[date][city].map((elem, e_index) =>

                                                        <div key={elem.ID}
                                                             className={`ts-wrap offer-item  ts-py-1  ${e_index != data[date][city].length - 1 ? 'ts-border-bottom' : ''}`}>
                                                            <div className="ts-row ts-py-1">
                                                                {   Object.keys(elem).indexOf('PICTURE_PATH') !== -1 &&
                                                                    <Suspense fallback={<div>...Loading...</div>}>
                                                                        <div
                                                                            className="ts-col-24 ts-col-sm-6 ts-col-lg-4  ts-align-items__center ts-pl-lg-0 ts-justify-content__center">
                                                                            <Dialog {...elem}>
                                                                                <img className="img_small" src={elem.PICTURE_PATH}
                                                                                     alt={elem.NAME}/>
                                                                            </Dialog>
                                                                        </div>
                                                                    </Suspense>
                                                                }

                                                                <div className="ts-col-auto ">
                                                                    <ul>
                                                                        <li>
                                                                            {   Object.keys(elem).indexOf('PICTURE_PATH') !== -1 ?
                                                                                <Suspense
                                                                                    fallback={<div>...Loading...</div>}>
                                                                                    <Dialog {...elem}>
                                                                                        <h5 className="ts-p-0 ts-m-0 text-danger">Номер: {elem.NAME}</h5>
                                                                                    </Dialog>
                                                                                </Suspense>
                                                                                : <h5 className="ts-p-0 ts-m-0 text-danger">Номер: {elem.NAME}</h5>
                                                                            }

                                                                        </li>
                                                                        <li>Питание: {elem.FOOD.NAME}</li>
                                                                        {
                                                                            elem.DATE_FROM_PLACEMENT ?
                                                                                <li>Отдых
                                                                                    с: {elem.DATE_FROM_PLACEMENT} по {elem.DATE_TO_PLACEMENT} </li>

                                                                            : null
                                                                        }
                                                                        <li>
                                                                            Взрослых: { people.adults ? people.adults : 0  }
                                                                        </li>
                                                                        <li>
                                                                            Детей: {people.children ? people.children : 0}
                                                                            {
                                                                                people.children_age.length != 0 ?
                                                                                    <span className={'ts-px-1'}>
                                                                                       ({
                                                                                                people.children_age.map(item => {
                                                                                                    if (item == 1) {
                                                                                                        return `${item} год `;
                                                                                                    } else if (item < 5 && item > 1) {
                                                                                                        return `${item} года `
                                                                                                    } else {
                                                                                                        return `${item} лет `
                                                                                                    }

                                                                                                })
                                                                                            })
                                                                                    </span>
                                                                                    : null


                                                                            }
                                                                                    </li>

                                                                    </ul>
                                                                </div>
                                                                <Suspense fallback={<div>...Loading...</div>}>
                                                                    <Tooltip styles='ts-col-24 ts-col-sm-5 ts-pt-2 ts-pt-sm-0 ts-justify-content__center ts-align-items__center' FORMATTED_PRICE_TOURPRODUCT={elem.FORMATTED_PRICE_TOURPRODUCT} FORMATTED_PRICE_TOURSERVICE={elem.FORMATTED_PRICE_TOURSERVICE}>
                                                                        <ul className="ts-d-flex ts-flex-direction__column ts-justify-content__center ts-align-items__center text-danger">
                                                                            <li className="price"><h4
                                                                                className={"ts-p-0 ts-m-0"}>{elem.FORMATTED_PRICE}</h4>
                                                                            </li>
                                                                            <li className="price-usd">{elem.FORMATTED_PRICE_USD}</li>

                                                                        </ul>
                                                                    </Tooltip>

                                                                </Suspense>

                                                                <div
                                                                    className="ts-col-24 ts-col-lg-4 ts-pt-2 ts-pt-lg-0  ts-justify-content__center ts-align-items__center">
                                                                    <a className="btn-secondary add2cart btn btn-primary btn-lg ts-px-2"
                                                                       href={`/booking/?add2basket=${elem.ADD2BASKET}&sessid=${session}`} > Бронировать </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                }
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        )
                    }
                </div>
            )



  }
}