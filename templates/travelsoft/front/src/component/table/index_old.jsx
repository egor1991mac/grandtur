
import React, { Component, lazy, Suspense } from 'react';
import './style.scss';
const Dialog = lazy(() => import('../dialog/index.js'));




export default class TicketResult extends Component {
  render() {
      const {data,people,session} = this.props;

    return (
        <div className="ts-wrap offer-box ">
            {   
                Object.keys(data).map(item=>
                    <div key={ item } className="ts-row tour-package">
                        <div className="ts-col-lg-4 ts-justify-content__center ">
                            { item }
                        </div>
                            <div className="ts-col-lg-20 " >
                                    {
                                        data[item].map(elem=>
                                            <div key={elem.ID} className="ts-wrap offer-item  ts-py-1">
                                                <div className="ts-row ts-py-1 ">

                                                        <Suspense fallback={<div>...Loading...</div>}>
                                                            <div className="ts-col-4  ts-align-items__center ts-pl-lg-0 ts-justify-content__center">
                                                                <Dialog {...elem}>

                                                                    <img className="img_small" src={elem.PICTURE_PATH} alt={elem.NAME}/>

                                                                </Dialog>
                                                            </div>
                                                        </Suspense>

                                                    <div className="ts-col-auto">
                                                        <ul>
                                                            <li>
                                                                <Suspense fallback={<div>...Loading...</div>}>   
                                                                    <Dialog {...elem}>
                                                                        <h5 class="ts-p-0 ts-m-0 text-danger">Номер: { elem.NAME }</h5>
                                                                    </Dialog>
                                                                </Suspense>
                                                            </li>
                                                            <li>Питание: {elem.FOOD.NAME}</li>
                                                            <li>Отдых с: {elem.DATE_FROM} по {elem.DATE_TO} </li>
                                                            <li>Взрослых: {people}</li>
                                                        </ul>
                                                    </div>

                                                        <ul className="ts-col-6 ts-justify-content__center ts-align-items__center text-danger">
                                                            <li className="price"><h4 className={"ts-p-0 ts-m-0"}>{elem.FORMATTED_PRICE}</h4></li>
                                                            <li className="price-usd">{elem.FORMATTED_PRICE_TOURPRODUCT}</li>
                                                          
                                                        </ul>
                                                    <div className="ts-col-4 ts-justify-content__center ts-align-items__center">
                                                        <a className="btn-secondary add2cart btn btn-primary btn-lg ts-px-2" href={`/booking/?add2basket=${elem.ADD2BASKET}&sessid=${session}`}> Бронировать </a>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                        )
                                    }

                                </div>



                    </div>  
                )
                
            }
              
        </div>
        )        
    }
}

