import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import React, { Component, lazy, Suspense } from 'react';


const Slider = lazy(() => import('../slider/index.js'));
const Slider2 = lazy(() => import('../slider/index_old.js'));


class AlertDialog extends React.Component {
  constructor(props){
      super(props);
      this.state = {
        open: false,
      };
    
      this.handleClickOpen = () => {
        this.setState({ open: true });
      };
    
      this.handleClose = () => {
        this.setState({ open: false });
      };
    
  }
    
  render() {
      const {NAME,DESCRIPTION,PICTURES} = this.props;
    return (
   <>
        <button className="ts-border-none ts-p-0 ts-width-100 text-align-left" onClick={this.handleClickOpen}>
          {
              this.props.children
          }
        </button>
        <Dialog
            maxWidth={'md'}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >

          <DialogContent>
              <div className="ts-wrap">

              <div className="ts-row">
                  <div className="ts-col-12">
                      <Suspense fallback={<div>...Loading...</div>}>
                          <Slider2 IMG={PICTURES} />
                      </Suspense>
                  </div>

                  <div className="ts-col-12">
                        <h2>{NAME}</h2>
                        { DESCRIPTION }
                  </div>
              </div>
              </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Закрыть
            </Button>
            
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

export default AlertDialog;