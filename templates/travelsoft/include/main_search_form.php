<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

global $CACHE_MANAGER;

\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/select2.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/icomoon_/style.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select2.min.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/booking/main-search-form.js");

/**
 * /local/php_interface/include/functions.php
 */
$arResult = ["ITEMS" => \getMainSearchFormsIblockItems(HOTELS_ID_IBLOCK)];
//dm($arResult, false, false, false);
?>
<template id="children-age-template">
    <div class="select-age-box form-group">
        <label>Возраст ребенка {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control styled">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option> 
        </select>
    </div>
</template>

    <div class="intro__search">
        <div class="container">
            <div class="search-hotels shadow-sm">
                <ul class="search-hotels__tabs nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#sea-vacation" role="tab" aria-controls="sea-vacation" aria-selected="false"><span class="d-none d-md-block">Туры с отдыхом на море</span><i class="icon icon-building d-md-none mx-1 text-secondary"></i></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tours" role="tab" aria-controls="tours" aria-selected="true"><span class="d-none d-md-block">Экскурсионные туры</span><i class="icon icon-airplane d-md-none mx-1 text-secondary"></i></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#hotels" role="tab" aria-controls="hotels" aria-selected="false"><span class="d-none d-md-block">Размещение</span><i class="icon icon-building d-md-none mx-1 text-secondary"></i></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#transfers" role="tab" aria-controls="transfers" aria-selected="true"><span class="d-none d-md-block">Проезд</span><i class="icon icon-airplane d-md-none mx-1 text-secondary"></i><i class="icon icon-building d-md-none mx-1 text-secondary"></i></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="sea-vacation" role="tabpanel">
                        <?
                        $APPLICATION->IncludeComponent(
                            "travelsoft:booking.search_form", "sea-vacation", array(
                                "ACTION_URL" => "/bus-tours/"
                            )
                        );
                        ?>
                    </div>
                    <div class="tab-pane" id="tours" role="tabpanel">
                        <?
                        $APPLICATION->IncludeComponent(
                            "travelsoft:booking.search_form", "tours", array(
                                "ACTION_URL" => "/bus-tours/"
                            )
                        );
                        ?>
                    </div>
                    <div class="tab-pane" id="hotels" role="tabpanel">
                        <?
                        $APPLICATION->IncludeComponent(
                            "travelsoft:booking.search_form", "hotels", array(
                                "ACTION_URL" => "/hotels/"
                            )
                        );
                        ?>
                    </div>
                    <div class="tab-pane" id="transfers" role="tabpanel">
                        <?
                        $APPLICATION->IncludeComponent(
                            "travelsoft:booking.search_form", "transfer", []
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
