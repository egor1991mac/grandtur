<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($APPLICATION->GetDirProperty("MAIN_PAGE") == "Y"): ?>

<?else:?>

    <? if ($APPLICATION->GetDirProperty("SHOW_LEFT_SIDEBAR") == "Y"): ?>
							</div>
						</section>
					</div>
    <?endif?>

<? endif; ?>

</div>
<button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
</div>
</main>
<footer class="page-footer py-4">
    <div class="container  mb-lg-4">
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."include/footer_content.php"
                ));?>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:search.title",
                                    "main",
                                    Array(
                                        "CATEGORY_0" => array("main", "iblock_agencies", "iblock_about_company", "iblock_geography_and_product"),
                                        "CATEGORY_0_TITLE" => "",
                                        "CATEGORY_0_iblock_about_company" => array("14"),
                                        "CATEGORY_0_iblock_agencies" => array("1", "2"),
                                        "CATEGORY_0_iblock_geography_and_product" => array("4", "7", "10", "11", "13"),
                                        "CATEGORY_0_main" => array(""),
                                        "CHECK_DATES" => "Y",
                                        "CONTAINER_ID" => "title-search",
                                        "INPUT_ID" => "title-search-input",
                                        "NUM_CATEGORIES" => "1",
                                        "ORDER" => "date",
                                        "PAGE" => "/search/",
                                        "SHOW_INPUT" => "Y",
                                        "SHOW_OTHERS" => "N",
                                        "TOP_COUNT" => "5",
                                        "USE_LANGUAGE_GUESS" => "Y"
                                    )
                                );?>
            </div>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "bottom.menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "subbottom",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "2",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "N",
                                        "ROOT_MENU_TYPE" => "bottom",
                                        "USE_EXT" => "Y"
                                    )
                                );?>
			<?/*
            <div class="col-12 col-sm-6 col-lg-3">
                <h3 class="h3 text-uppercase">Агентствам</h3>
                <ul class="page-footer__support list-styled">
                    <li class="mb-2"><a href="#" target="_blank">Регистрация</a></li>
                    <li class="mb-2"><a href="#" target="_blank">Личный кабинет</a></li>
                    <li class="mb-2"><a href="#" target="_blank">Условия работы</a></li>
                    <li class="mb-2"><a href="#" target="_blank">Бонусная программа</a></li>
                </ul>
            </div>
			*/?>
            <div class="col-12 col-sm-12 col-lg-3">
                <h3 class="h3 text-uppercase">получить лучшие предложения</h3>
			<?$APPLICATION->IncludeComponent(
				"asd:subscribe.quick.form", 
				"subscribe", 
				array(
					"FORMAT" => "html",
					"INC_JQUERY" => "N",
					"NOT_CONFIRM" => "Y",
					"RUBRICS" => array(
						0 => "1",
					),
					"SHOW_RUBRICS" => "N",
					"COMPONENT_TEMPLATE" => "subscribe"
				),
				false
			);?>
            </div>
        </div>
    </div>
    <div class="page-footer__copyright text-center">
        <div class="container">
            <div class="hr"></div>Грандтур. All rights reserved.
			<br><a href="/travelsoft/"><img src="/images/logo_travelsoft.jpg" alt="ТрэвелСофт"></a>
        </div>
    </div>
</footer>
<div class="modal-map modal" id="modalMap" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header align-items-center py-2">
                <h4 class="modal-title"><a class="d-flex align-items-center" href=""><i class="icon mr-2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.5 22.1"><path d="M0.3,10.3C0.3,10.3,0.3,10.3,0.3,10.3c-0.4,0.5-0.4,1.2,0,1.6l10,10c0,0,0,0,0,0c0.4,0.4,1.1,0.4,1.6,0 c0.4-0.4,0.4-1.1,0-1.6l-8-8h26.6c0.6,0,1.1-0.5,1.1-1.1c0,0,0,0,0,0c0-0.6-0.5-1.1-1.1-1.1H3.8l8-8c0.4-0.4,0.4-1.1,0-1.6 c0,0,0,0,0,0c-0.4-0.4-1.1-0.4-1.6,0L0.3,10.3z"/></svg></i><span class="title">Hotel</span></a></h4>
                <button class="btn btn-secondary btn--round px-4" type="button" data-dismiss="modal">map close
                </button>
            </div>
            <div class="map-contain" id="map"></div>
        </div>
    </div>
</div>
<? $APPLICATION->ShowPanel() ?>

<? $oAsset->addJs(SITE_TEMPLATE_PATH . "/front/dist/index_bundle.js") ?>
<? //$oAsset->addJs(SITE_TEMPLATE_PATH . "/front2/build//static/js/main.0695db86.chunk.js") ?>

<?// $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/React.js") ?>
<?// $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/ReactDom.js") ?>
<?// $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/redux.js") ?>

</body>
</html>
