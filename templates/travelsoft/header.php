<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$oAsset = Bitrix\Main\Page\Asset::getInstance();
CJSCore::Init(); ?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<head>
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowHead() ?>
    <link rel="icon" href="/favicon.ico"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Looking for something amazing?">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="True">
    <!-- Edge and IE-->
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Add to homescreen for Chrome on Android-->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Looking for something amazing?">
    <link rel="icon" sizes="192x192" href="<?=SITE_TEMPLATE_PATH?>/img/touch/chrome-touch-icon-192x192.png">
    <!-- Add to homescreen for Safari on iOS-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Your Travel World">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/img/touch/apple-touch-icon.png">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.min.js"></script>
    <!-- Google Fonts-->
    <? $oAsset->addCss("https://fonts.googleapis.com/css?family=Open+Sans:400,400i,500,600,700%7CPoppins:400,500,600,700") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/jquery-ui.structure.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/jquery-ui.theme.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/flatpickr.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/fontawesome-stars-o.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/swiper.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/jquery.mCustomScrollbar.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/select2.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/nouislider.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/blueimp-gallery.min.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/style.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/custom.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/ts-theme.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/proslider.css") ?>

<!--    --><?// $oAsset->addJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyD3gFKhVywUkygSxQEBdGVrI5-ZRrdjueA") ?>
<!--    --><?// $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/maps.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/moment.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/wNumb.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/nouislider.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery-ui.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/barba.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/sticky-kit.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/velocity.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/velocity.ui.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.waypoints.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/popper.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/bootstrap.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/imagesloaded.pkgd.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/masonry.pkgd.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/isotope.pkgd.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/ofi.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jarallax.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jarallax-video.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jarallax-element.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.mCustomScrollbar.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/swiper.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/flatpickr/flatpickr.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/flatpickr/rangePlugin.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/select2.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/select2/en.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.mask.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/validator.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.barrating.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/libs/jquery.blueimp-gallery.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap-datepicker.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/script.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/myscript.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/front/dist/config.js") ?>

    <? //$oAsset->addJs(SITE_TEMPLATE_PATH . "/front/dist/index_bundle.js") ?>


<!--    --><?// $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/demo-switcher.js") ?>

    <!--    <link id="cssRtl" rel="stylesheet" href="#">-->
</head>
<body class="load">
<div id="root"></div>
<header class="page-header">
    <div class="panel-fixed">
        <div class="page-header__top bg-white js-header-top js-top-panel px-2">
            <div class="container">

                <div class="row justify-content-center justify-content-between">
                    <div class="col-12 col-md col-lg-2 d-flex justify-content-center justify-content-md-start">
                        <a class="navbar-logo" href="/">
                            <img class="img-fluid" src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="Грандтур"/>
                        </a>
                    </div>

                    <div class="col col-lg-10 py-2">

                        <div class="row">
                            <div class="col-12 d-none d-md-flex justify-content-between justify-content-md-end align-items-center">

                                <a style="margin-right:20px" class="page-header__call d-flex align-items-center text-nowrap" href="tel:375296667001"><i class="icon icon-call js-tooltip-call mr-3" data-toggle="tooltip" data-placement="bottom" title="+375(232) 33-33-10"></i>
                                    <div class="page-header__call-right">
                                        <div class="title fz-small">г. Гомель, ул. Советская, 61</div>
                                        <div class="phone fw-bold">+375 (29) 666-70-01</div>
                                    </div></a>
                                <a style="margin-right:20px" class="page-header__call d-flex align-items-center text-nowrap" href="tel:375296567001">
                                    <div class="page-header__call-right">
                                        <div class="title fz-small">Агенствам</div>
                                        <div class="phone fw-bold">+375 29 656-70-01</div>
                                    </div></a>
								<a href="http://old.grandtour.by"><b>Старая версия сайта</b></a>
                                <div class="page-header__top-search  d-lg-block mx-4" id="navbarSearch">
                                    <div class="page-header__incoming"><a href="/holidays-in-belarus/">INCOMING</a></div>
                                </div>
                                <div class="">
                                    <ul class="nav nav-panel">
                                        <li class="nav-item d-flex"><a class="nav-link btn js-toggle-account" href="/personal/profile/"><i class="text-gray icon icon-user mr-2"></i><b>Личный кабинет</b></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <nav class="w-100 main-nav d-flex collapse d-lg-block" id="navPanel">
                            <div class="main-nav__container d-flex justify-content-end">

                                    <?/*<div class="d-lg-none">
                                        <div class="main-nav__search">
                                            <?$APPLICATION->IncludeComponent(
                                                "bitrix:search.title",
                                                "main",
                                                Array(
                                                    "CATEGORY_0" => array("main", "iblock_agencies", "iblock_about_company", "iblock_geography_and_product"),
                                                    "CATEGORY_0_TITLE" => "",
                                                    "CATEGORY_0_iblock_about_company" => array("14"),
                                                    "CATEGORY_0_iblock_agencies" => array("1", "2"),
                                                    "CATEGORY_0_iblock_geography_and_product" => array("4", "7", "10", "11", "13"),
                                                    "CATEGORY_0_main" => array(""),
                                                    "CHECK_DATES" => "Y",
                                                    "CONTAINER_ID" => "title-search",
                                                    "INPUT_ID" => "title-search-input",
                                                    "NUM_CATEGORIES" => "1",
                                                    "ORDER" => "date",
                                                    "PAGE" => "/search/",
                                                    "SHOW_INPUT" => "Y",
                                                    "SHOW_OTHERS" => "N",
                                                    "TOP_COUNT" => "5",
                                                    "USE_LANGUAGE_GUESS" => "Y"
                                                )
                                            );?>
                                        </div>
                                        <!--<ul class="nav nav-panel">
                                            <li class="nav-item d-flex"><a class="nav-link btn btn-light js-toggle-account" href="#modalAccount" data-toggle="modal" role="button" data-account="login"><i class="text-gray icon icon-user mr-2"></i><span>log in</span></a>
                                            </li>
                                            <li class="nav-item d-flex"><a class="nav-link btn btn-light js-toggle-account" href="#modalAccount" data-toggle="modal" role="button" data-account="regist"><i class="text-gray icon icon-login mr-2"></i><span>sign up</span></a>
                                            </li>
                                        </ul>-->
                                    </div>*/?>
                                    <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                            "MAX_LEVEL" => "2",	// Уровень вложенности меню
                                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                            "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                            "COMPONENT_TEMPLATE" => "horizontal_multilevel"
                                        ),
                                        false
                                    );?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn-toggle btn btn-light d-lg-none btn-toggle-nav" type="button" data-toggle="collapse" data-target="#navPanel"><span class="icon-bar"></span></button>
</header>
<main class="page-main">
    <? if ($APPLICATION->GetDirProperty("MAIN_PAGE") == "Y"): ?>
    <section class="intro d-flex flex-column load">
        <? $APPLICATION->IncludeComponent("travelsoft:travelsoft.news.list", "main_slider", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
            "DISPLAY_DATE" => "N",	// Выводить дату элемента
            "DISPLAY_NAME" => "N",	// Выводить название элемента
            "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "FIELD_CODE" => array(	// Поля
                0 => "",
                1 => "",
            ),
            "FILE_404" => "",	// Страница для показа (по умолчанию /404.php)
            "FILTER_NAME" => "",	// Фильтр
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "IBLOCK_ID" => "9",	// Код информационного блока
            "IBLOCK_TYPE" => "geography_and_product",	// Тип информационного блока (используется только для проверки)
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
            "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "9",	// Количество новостей на странице
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "PROPERTY_CODE" => array(	// Свойства
                0 => "PREVIEW_TEXT",
                1 => "PICTURES",
                2 => "",
            ),
            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
            "SET_STATUS_404" => "Y",	// Устанавливать статус 404
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "SHOW_404" => "Y",	// Показ специальной страницы
            "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
            "COMPONENT_TEMPLATE" => ".default",
            "AFP_ID" => "",	// ID элемента(ов)(если несколько значений, то указывать через запятую)
        ),
            false
        ); ?>
        <div class="intro__content d-flex flex-column justify-content-end js-intro-content">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include/main_search_form.php", [], ["MODE" => "HTML"]);?>
        </div>
    </section>
    <div class="page-content">
            <?$GLOBALS["arFilterHomeSliderTours"] = array("PROPERTY_SHOW_ON_MAIN_VALUE"=>"Y","PROPERTY_SPECIAL_OFFER_VALUE"=>"Y");?>
            <? $APPLICATION->IncludeComponent(
	"travelsoft:travelsoft.news.list", 
	"main_hotels", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILE_404" => "",
		"FILTER_NAME" => "arFilterHomeSliderTours",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "11",
		"IBLOCK_TYPE" => "geography_and_product",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "DEPARTURE",
			1 => "PRICE",
			2 => "CURRENCY",
			3 => "TOWN",
			4 => "PICTURES",
			5 => "HOTEL",
			6 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"SHOW_404" => "Y",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "main_hotels",
		"AFP_ID" => "",
		"AFP_156" => array(
		),
		"AFP_157" => array(
		),
		"AFP_158" => array(
		),
		"AFP_162" => array(
		),
		"AFP_163" => array(
		),
		"AFP_300" => array(
		),
		"AFP_164" => array(
		),
		"AFP_MIN_167" => "",
		"AFP_MAX_167" => "",
		"AFP_169" => array(
		),
		"AFP_170" => array(
		),
		"AFP_171" => array(
		),
		"AFP_172" => array(
		),
		"AFP_174" => array(
		),
		"AFP_176" => array(
		),
		"AFP_MIN_178" => "",
		"AFP_MAX_178" => "",
		"AFP_MIN_179" => "",
		"AFP_MAX_179" => "",
		"AFP_180" => array(
		),
		"AFP_188" => array(
		),
		"AFP_189" => array(
		),
		"AFP_190" => array(
		),
		"AFP_191" => array(
		),
		"AFP_192" => array(
		),
		"AFP_193" => array(
		),
		"AFP_194" => array(
		),
		"AFP_195" => array(
		),
		"AFP_MIN_203" => "",
		"AFP_MAX_203" => "",
		"AFP_204" => array(
		),
		"AFP_MIN_209" => "",
		"AFP_MAX_209" => "",
		"AFP_296" => array(
		)
	),
	false
); ?>
        <div class="container">


<?else:?>
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
            "PATH" => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
            "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
        ),
            false
        ); ?>

        <div class="page-content">
            <div class="container">

            <? if ($APPLICATION->GetDirProperty("SHOW_TITLE") != "N"): ?><h1><?= $APPLICATION->ShowTitle(false); ?></h1><?endif?>

            <? if ($APPLICATION->GetDirProperty("SHOW_LEFT_SIDEBAR") == "Y"): ?>
                <div class="row">
                    <div class="col-lg-3 d-none d-lg-block">
                        <? if ($APPLICATION->GetDirProperty("SHOW_MENU") == "Y"): ?>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "left_menu",
                                array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "1",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "COMPONENT_TEMPLATE" => "left_menu"
                                ),
                                false
                            ); ?>
                        <?endif?>
						<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
                    </div>
                    <div class="col-12 col-lg-9">
						<section class="card" id="typography">
							<div class="card-header">
					  			<h1 class="mb-0"><?= $APPLICATION->ShowTitle(false); ?></h1>
							</div>
							<div class="card-body">
            <?endif?>

<? endif; ?>