<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

#######################
# СОРТИРОВКА
#######################

$arSortBy = array(
    "name" => array(
        "realfield" => "NAME",
        "order_def" => "asc"
    ),
    /*"price" => array(
        "realfield" => "PROPERTY_SORT_BY_PRICE",
        "order_def" => "asc",
        "callback" => array(
            "name" => "prepareSortByPrice",
            "params" => array($arParams)
        )
    ),*/
    "sort" => array(
        "realfield" => "SORT",
        "order_def" => "asc"
    ),
    /*"rating" => array(
        "realfield" => "PROPERTY_RATING",
        "order_def" => "asc"
    )*/
);

$arParams["SORT_PARAMETERS"] = null;

foreach ($arSortBy as $name => $arp) {
    if ($name == $_REQUEST["sort_by"]) {
        $arParams["SORT_PARAMETERS"][] = array("name" => $name, "order" => $_REQUEST["order"] == "asc" ? "desc" : "asc", "selected" => true);
    } else {
        $arParams["SORT_PARAMETERS"][] = array("name" => $name, "order" => $arp["order_def"], "selected" => false);
    }
}
##################################
?>
<?if ($arParams["SORT_PARAMETERS"]) :?>
<form>
    <label class="result-filter-label"><?= GetMessage("SORT_TITLE")?> :</label>
    <div class="selection-bar">
        <?foreach ($arParams["SORT_PARAMETERS"] as $arp):?>
            <div class="select-wrapper">
                <div class="sort-select select float-left">
                    <?
                    $arrow = "<i class=\"fa fa-long-arrow-up\" aria-hidden=\"true\"></i> <i class=\"fa fa-long-arrow-down\" aria-hidden=\"true\"></i>";
                    if ($arp["selected"]) {
                        $arrow = $arp["order"] == "asc" ? "<i class=\"fa fa-long-arrow-up\" aria-hidden=\"true\"></i>" : "<i class=\"fa fa-long-arrow-down\" aria-hidden=\"true\"></i>";
                    }
                    ?>
                    <a class="sorting" rel="nofollow" href="<?= $APPLICATION->GetCurPageParam("sort_by=" . $arp["name"] . "&" . "order=" . $arp["order"], array("sort_by", "order"), false)?>"><?= GetMessage($arp["name"])?></a> <?= $arrow?>
                </div>
            </div>
        <?endforeach?>
    </div>
</form>
<?endif?>