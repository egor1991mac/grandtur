<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/booking/main-search-form-detail.js");
?>
<? /* if ($APPLICATION->GetDirProperty("show_sidebar_detail_page") == "Y"): */ ?><!--
<div class="col-md-3">
    <aside class="sidebar-left">
<? /* $APPLICATION->IncludeComponent(
  "bitrix:menu",
  "left.menu",
  Array(
  "ALLOW_MULTI_SELECT" => "N",
  "CHILD_MENU_TYPE" => "left",
  "DELAY" => "N",
  "MAX_LEVEL" => "1",
  "MENU_CACHE_GET_VARS" => array(""),
  "MENU_CACHE_TIME" => "3600000",
  "MENU_CACHE_TYPE" => "A",
  "MENU_CACHE_USE_GROUPS" => "N",
  "ROOT_MENU_TYPE" => "left",
  "USE_EXT" => "Y"
  )
  ); */ ?>
    </aside>
</div>
<div class="col-md-9">
--><? /* endif */ ?>
<? Bitrix\Main\Loader::includeModule("travelsoft.travelbooking")?>

<?$this->SetViewTarget('page_url');?>
    <?echo "tour-view-main";?>
<?$this->EndViewTarget();?>

<?/* ob_start() */?><!--
<?/*
$APPLICATION->IncludeComponent(
        "travelsoft:booking.offers", "d3p.simple", Array(
    "SERVICE_TYPE" => "placements",
    "BOOKING_URL" => "/booking/",
    "SERVICES_ID" => \travelsoft\booking\Utils::getServiceIdByCode("placements", $arResult["VARIABLES"]["ELEMENT_CODE"])
        )
);
*/?>
<?/* $GLOBALS["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"] = ob_get_clean(); */?>

<?/* ob_start() */?>
<?/*
$APPLICATION->IncludeComponent(
        "travelsoft:booking.offers", "d3p.simple", Array(
    "SERVICE_TYPE" => "packageTour",
    "BOOKING_URL" => "/booking/",
    "SERVICES_ID" => \travelsoft\booking\Utils::getPackageToursIdByPlacementId(\travelsoft\booking\Utils::getServiceIdByCode("placements", $arResult["VARIABLES"]["ELEMENT_CODE"]))
        )
);
*/?>
<?/* $GLOBALS["SEARCH_PACKAGE_OFFERS_RESULT_HTML"] = ob_get_clean(); */?>

<?/* ob_start() */?>
<?/*
$APPLICATION->IncludeComponent(
        "travelsoft:booking.search_form", "d3p.inner", Array(
            "HIGHLIGHT_FOR" => array(
                "CALENDAR_FROM" => array(
                    "SERVICE_TYPE" => "travel",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_from]']",
                    "SERVICES_ID" => \travelsoft\booking\Utils::getTransferIdList(\travelsoft\booking\Utils::getServiceIdByCode("placements", $arResult["VARIABLES"]["ELEMENT_CODE"])),
                ),
                "CALENDAR_TO" => array(
                    "SERVICE_TYPE" => "travelback",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_to]']",
                    "SERVICES_ID" => \travelsoft\booking\Utils::getTransferIdList(\travelsoft\booking\Utils::getServiceIdByCode("placements", $arResult["VARIABLES"]["ELEMENT_CODE"]))
                )
            )
        )
);
*/?>
--><?/* $GLOBALS["SEARCH_FORM_HTML"] = ob_get_clean(); */?>

<?
########################### search offers content ##############################
$GLOBALS = \array_merge($GLOBALS, \travelsoft\booking\Utils::getHTMLSearchOffersFormAndOffersListOnDetailPage([
    "service_type" => function () use ($arParams, $arResult) {
        if ($arParams["DYNAMICALLY_DETECT_SEARCH_OFFERS_PARAMETERS"]) {
            $arElement = \CIBlockElement::GetList(false, ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]], false, false, ["ID", "PROPERTY_TOURTYPE"])->Fetch();
            if (isset($arElement["ID"])) {

                if (!in_array(SEA_TOUR_TYPE_ID, $arElement["PROPERTY_TOURTYPE_VALUE"])) {
                    return "excursiontour";
                }
            }
            return "packagetour";
        } else {
            return $arParams["SERVICE_TYPE"] ?: "placements";
        }
    },
    "search_form_component_template" => "inner",
    "offers_component_template" => "simple_exc",
    "booking_url" => "/bronirovanie/",
    "element_code" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "highlight_dates_parameters" => function () use ($arParams, $arResult) {

        if ($arParams["DYNAMICALLY_DETECT_SEARCH_OFFERS_PARAMETERS"]) {

            $arElement = \CIBlockElement::GetList(false, ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]], false, false, ["ID", "PROPERTY_TOURTYPE"])->Fetch();
            if (isset($arElement["ID"])) {
                if (!in_array(SEA_TOUR_TYPE_ID, $arElement["PROPERTY_TOURTYPE_VALUE"])) {
                    return [
                        "CALENDAR_FROM" => [
                            "SERVICE_TYPE" => "excursiontour",
                            "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_from]']"
                        ]
                    ];
                }
            }
            return [
                "CALENDAR_FROM" => [
                    "SERVICE_TYPE" => "transfer",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_from]']"
                ],
                "CALENDAR_TO" => [
                    "SERVICE_TYPE" => "transferback",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_to]']"
                ]
            ];
        } else {
            return isset($arParams["HIGHLIGHT_FOR"]) && is_array($arParams["HIGHLIGHT_FOR"]) ? $arParams["HIGHLIGHT_FOR"] : [];
        }

    }
]));
################################################################################
?>

<template id="children-age-template">
    <div class="select-age-box form-group">
        <label>Возраст ребенка {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control styled">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <? /* <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option> */ ?>
        </select>
    </div>
</template>

<? $template = isset($arParams["TEMPLATE_DETAIL"]) && !empty($arParams["TEMPLATE_DETAIL"]) ? $arParams["TEMPLATE_DETAIL"] : "placement"; ?>
<?
$ElementID = $APPLICATION->IncludeComponent(
        "bitrix:news.detail", $template, Array(
    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
    "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
    "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
    "META_KEYWORDS" => $arParams["META_KEYWORDS"],
    "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
    "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
    "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
    "SET_TITLE" => $arParams["SET_TITLE"],
    "MESSAGE_404" => $arParams["MESSAGE_404"],
    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
    "SHOW_404" => $arParams["SHOW_404"],
    "FILE_404" => $arParams["FILE_404"],
    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
    "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
    "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
    "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
    "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
    "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
    "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
    "CHECK_DATES" => $arParams["CHECK_DATES"],
    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
    "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
    "USE_SHARE" => $arParams["USE_SHARE"],
    "SHARE_HIDE" => $arParams["SHARE_HIDE"],
    "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
    "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
    "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
    "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
    "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
    'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
    "SEARCH_FORM_HASH" => md5($GLOBALS["SEARCH_FORM_HTML"]),
    "SEARCH_OFFERS_RESULT_HTML" => md5($GLOBALS["SEARCH_OFFERS_RESULT_HTML"]),
    //"SEARCH_PACKAGE_OFFERS_RESULT_HTML" => md5($GLOBALS["SEARCH_PACKAGE_OFFERS_RESULT_HTML"])
        ), $component
);
?>
</div></div></div></div></div>
<section class="contact style-1 page-contact-form padding-top padding-bottom" style="margin-top: 50px;">
    <div class="container">
        <div class="wrapper-contact-form">
            <?$APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.feedback",
                "tour",
                Array(
                    "EMAIL_TO" => "info@travelsoft.by",
                    "EVENT_MESSAGE_ID" => array("7"),
                    "IBLOCK_ID" => "34",
                    "IBLOCK_TYPE" => "request",
                    "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                    "PROPERTY_CODES" => array("289", "290", "291"),
                    "PROPERTY_CODES_REQUIRED" => array("289", "290", "291"),
                    "REQUIRED_FIELDS" => array("NAME", "EMAIL", "MESSAGE"),
                    "TEXT_DESCRIPTION" => "Хотите задать вопрос по этому туру?",
                    "TEXT_TITLE" => "ФОРМА ОБРАТНОЙ СВЯЗИ",
                    "USE_CAPTCHA" => "N",
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_SHADOW" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "ELEMENT_ID" => $ElementID
                )
            );?>
            <div data-wow-delay="0.5s" class="wrapper-form-images wow fadeInRight" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/background/bg-banner-contact-form.jpg" alt="" class="img-responsive">
            </div>
        </div>
    </div>
</section>
<div><div><div><div><div>

<? /* if ($APPLICATION->GetDirProperty("show_sidebar_detail_page") == "Y"): */ ?><!--
    </div>
--><? /* endif */ ?>

<!--<p><a href="<? /* =$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"] */ ?>"><? /* =GetMessage("T_NEWS_DETAIL_BACK") */ ?></a></p>-->
<? if ($arParams["USE_RATING"] == "Y" && $ElementID): ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:iblock.vote", "", Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_ID" => $ElementID,
        "MAX_VOTE" => $arParams["MAX_VOTE"],
        "VOTE_NAMES" => $arParams["VOTE_NAMES"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
            ), $component
    );
    ?>
<? endif ?>
<?
if ($arParams["USE_CATEGORIES"] == "Y" && $ElementID):
    global $arCategoryFilter;
    $obCache = new CPHPCache;
    $strCacheID = $componentPath . LANG . $arParams["IBLOCK_ID"] . $ElementID . $arParams["CATEGORY_CODE"];
    if (($tzOffset = CTimeZone::GetOffset()) <> 0)
        $strCacheID .= "_" . $tzOffset;
    if ($arParams["CACHE_TYPE"] == "N" || $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N")
        $CACHE_TIME = 0;
    else
        $CACHE_TIME = $arParams["CACHE_TIME"];
    if ($obCache->StartDataCache($CACHE_TIME, $strCacheID, $componentPath)) {
        $rsProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $ElementID, "sort", "asc", array("ACTIVE" => "Y", "CODE" => $arParams["CATEGORY_CODE"]));
        $arCategoryFilter = array();
        while ($arProperty = $rsProperties->Fetch()) {
            if (is_array($arProperty["VALUE"]) && count($arProperty["VALUE"]) > 0) {
                foreach ($arProperty["VALUE"] as $value)
                    $arCategoryFilter[$value] = true;
            } elseif (!is_array($arProperty["VALUE"]) && strlen($arProperty["VALUE"]) > 0)
                $arCategoryFilter[$arProperty["VALUE"]] = true;
        }
        $obCache->EndDataCache($arCategoryFilter);
    }
    else {
        $arCategoryFilter = $obCache->GetVars();
    }
    if (count($arCategoryFilter) > 0):
        $arCategoryFilter = array(
            "PROPERTY_" . $arParams["CATEGORY_CODE"] => array_keys($arCategoryFilter),
            "!" . "ID" => $ElementID,
        );
        ?>
        <hr /><h3><?= GetMessage("CATEGORIES") ?></h3>
        <? foreach ($arParams["CATEGORY_IBLOCK"] as $iblock_id): ?>
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:news.list", $arParams["CATEGORY_THEME_" . $iblock_id], Array(
                "IBLOCK_ID" => $iblock_id,
                "NEWS_COUNT" => $arParams["CATEGORY_ITEMS_COUNT"],
                "SET_TITLE" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "FILTER_NAME" => "arCategoryFilter",
                "CACHE_FILTER" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                    ), $component
            );
            ?>
        <? endforeach ?>
    <? endif ?>
<? endif ?>
<? if ($arParams["USE_REVIEW"] == "Y" && IsModuleInstalled("forum") && $ElementID): ?>
    <hr />
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:forum.topic.reviews", "", Array(
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
        "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
        "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
        "FORUM_ID" => $arParams["FORUM_ID"],
        "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
        "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
        "DATE_TIME_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
        "ELEMENT_ID" => $ElementID,
        "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "URL_TEMPLATES_DETAIL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
            ), $component
    );
    ?>
    <?


endif?>
