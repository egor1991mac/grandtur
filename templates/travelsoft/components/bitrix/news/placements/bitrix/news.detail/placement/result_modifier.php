<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


$arResult["SERVICES_ICON"] = array();
$db_props = CIBlockElement::GetList(Array("sort" => "asc"), Array("IBLOCK_ID"=>SERVICES_IBLOCK_ID, "ACTIVE"=>"Y"), false, false, Array("ID", "NAME","PROPERTY_PICTURES"));
while($ar_props = $db_props->Fetch()){
    $arResult["SERVICES_ICON"][$ar_props["ID"]] = array(
        "ICON" => current(getSrc($ar_props["PROPERTY_PICTURES_VALUE"])),
        "TITLE" => $ar_props["NAME"]
    );
}

$arResult['ROUTE_INFO'] = array();
if ($arResult['DISPLAY_PROPERTIES']["MAP"]["VALUE"] != "") {
    $LATLNG = explode(",", $arResult['DISPLAY_PROPERTIES']["MAP"]["VALUE"] );
    $arResult['ROUTE_INFO'][] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arResult['NAME'],
        "infoWindow" => "<div style='color:red'><b>" .$arResult['NAME']. "</b></div>"
    );
}

/*$zoom = isset($arResult['MAP_SCALE']) && !empty($arResult['MAP_SCALE']) ? $arResult['MAP_SCALE'] : 5;

$cp = $this->__component;

if (is_object($cp))
{
    $cp->arResult['ROUTE_INFO'] = $arResult['ROUTE_INFO'];
    $cp->arResult['MAP_SCALE'] = $zoom;

    $arResult['MAP_SCALE'] = $cp->arResult['MAP_SCALE'];
    $cp->SetResultCacheKeys(array('ROUTE_INFO', 'MAP_SCALE'));

}*/