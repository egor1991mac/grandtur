<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/theia-sticky-sidebar.min.js");
$htmlMapID = "map-canvas";
?>
    <div class="row">
        <div class="col-md-9">
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["CAT_ID"]["VALUE"])):?>
                <?$stars = (int)$arResult["DISPLAY_PROPERTIES"]["CAT_ID"]["VALUE"];?>
                <?if(!empty($stars)):?>
                    <?$html_stars = '';
                    $html_stars .= '<div title="'.$arResult["DISPLAY_PROPERTIES"]["CAT_ID"]["VALUE"].'" class="star-rating">';
                    $html_stars .= '<span class="width-'.($stars*20).'">';
                    $html_stars .=  '<strong class="rating">'.$arItem["DISPLAY_PROPERTIES"]["CAT_ID"]["VALUE"].'</strong></span></div>';
                    ?>
                <?endif?>
            <?endif?>
            <h2 class="title-style-2"><?=$arResult["NAME"]?> <?=$html_stars?></h2>
            <div class="schedule-block">
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TYPE_ID"]["VALUE"])):?>
                    <div class="element">
                        <p class="schedule-title"><?=GetMessage('TYPE_ID')?></p>
                        <span class="schedule-content" style="padding: 0 2px;"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["TYPE_ID"]["DISPLAY_VALUE"])?></span>
                    </div>
                <?endif;?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_ADDRESS"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["HD_ADDRESS_COUNTRY_LANGUAGE"]["VALUE"])):?>
                    <?$address = !empty($arResult["DISPLAY_PROPERTIES"]["HD_ADDRESS_COUNTRY_LANGUAGE"]["VALUE"]) ? $arResult["DISPLAY_PROPERTIES"]["HD_ADDRESS_COUNTRY_LANGUAGE"]["VALUE"] : $arResult["DISPLAY_PROPERTIES"]["HD_ADDRESS"]["DISPLAY_VALUE"]?>
                    <div class="element">
                        <p class="schedule-title"><?=GetMessage('HD_ADDRESS')?></p>
                        <span class="schedule-content"><?=strip_tags($address)?></span>
                    </div>
                <?endif;?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_PHONE"]["VALUE"])):?>
                    <div class="element">
                        <p class="schedule-title"><?=GetMessage('HD_PHONE')?></p>
                        <span class="schedule-content"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["HD_PHONE"]["DISPLAY_VALUE"])?></span>
                    </div>
                <?endif;?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_EMAIL"]["VALUE"])):?>
                    <div class="element">
                        <p class="schedule-title"><?=GetMessage('HD_EMAIL')?></p>
                        <span class="schedule-content"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["HD_EMAIL"]["DISPLAY_VALUE"])?></span>
                    </div>
                <?endif?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_HTTP"]["VALUE"])):?>
                    <div class="element">
                        <p class="schedule-title"><?=GetMessage('HD_HTTP')?></p>
                        <span class="schedule-content"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["HD_HTTP"]["DISPLAY_VALUE"])?></span>
                    </div>
                <?endif?>
            </div>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <div class="text-block">
                        <?=$arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["DISPLAY_VALUE"]?>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"])):?>
                <div class="overview-block clearfix">
                    <div class="warpper-slider-detail" id="detail_photo">
                        <? $APPLICATION->IncludeComponent(
                            "travelsoft:travelsoft.slider",
                            ".default",
                            array(
                                "AUTO_PLAY_BIG" => "N",
                                "AUTO_PLAY_HOVER_PAUSE_BIG" => "Y",
                                "AUTO_PLAY_TIMEOUT_BIG" => "3000",
                                "DATA_SOURCE" => $arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"],
                                "DOT_BIG" => "N",
                                "DOT_SMALL" => "N",
                                "DO_NOT_INC_MAGNIFIC_POPUP" => "N",
                                "DO_NOT_INC_OWL_CAROUSEL" => "N",
                                "HEIGHT" => "471",
                                "INC_JQUERY" => "N",
                                "ITEM_COUNT_BIG" => "1",
                                "ITEM_COUNT_SMALL" => "5",
                                "LAZY_LOAD_BIG" => "Y",
                                "LAZY_LOAD_SMALL" => "Y",
                                "MARGIN_PICTURES_BIG" => "10",
                                "MARGIN_PICTURES_SMALL" => "3",
                                "NAV_BIG" => "Y",
                                "NAV_SMALL" => "Y",
                                "NO_PHOTO_PATH" => NO_PHOTO_PATH_750_420,
                                "WIDTH" => "848",
                                "COMPONENT_TEMPLATE" => ".default"
                            ),
                            false
                        ); ?>
                    </div>
                </div>
            <?endif?>
            <?if ($GLOBALS["SEARCH_FORM_HTML"]):?>
            <div class="cart-header">
                <h4 class="card-title">Бронирование</h4>
                <hr>
            </div>
            <div class="row"><div class="col-md-12"><? echo $GLOBALS["SEARCH_FORM_HTML"]; ?></div></div>
            <?endif?>
                            <?// вывод цен из travelbooking по размещению
            if ($GLOBALS["SEARCH_OFFERS_RESULT_HTML"]):?>
            <div class="row"><div class="col-md-12">
                    <!--<div class="cart-header">
                        <h4 class="card-title">Номера</h4>
                        <hr>
                    </div>-->
                    <?echo $GLOBALS["SEARCH_OFFERS_RESULT_HTML"]; ?>
                </div>
            </div>

            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_TABLE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price"><?=GetMessage('PRICE_TABLE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PRICE_TABLE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
    <div class="overview-block clearfix">
                    

        <?$GLOBALS["arFilterRooms"] = array("PROPERTY_HOTEL"=>$arResult["ID"]);?>
        <?$APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.list",
            "rooms",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AFP_ID" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "rooms",
                "DESCRIPTION_LINK" => "",
                "DESCRIPTION_NAMELINK" => "",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "arFilterRooms",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "38",
                "IBLOCK_TYPE" => "geography_and_product",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "100",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "PICTURES",
                    1 => "DESCRIPTION",
                    2 => "SQUARE",
                    3 => "FACILITIES",
                    4 => "HOTEL",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "TEXT_DESCRIPTION" => "",
                "TEXT_TITLE" => "Номера",
            ),
            false
        );?>

    </div>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCROOM"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_room"><?=GetMessage('HD_DESCROOM')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCROOM"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCMEAL"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_food"><?=GetMessage('HD_DESCMEAL')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCMEAL"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arItem["DISPLAY_PROPERTIES"]["SEARCH"]["VALUE"]) || !empty($arItem["DISPLAY_PROPERTIES"]["HD_DESCSERVICE"]["VALUE"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_services"><?=GetMessage('HD_DESCSERVICE')?></h3>
                    <div class="text-block">
                        <?if(!empty($arItem["DISPLAY_PROPERTIES"]["SEARCH"]["VALUE"]) && !empty($arResult["SERVICES_ICON"])):?>
                            <ul class="item-features item-features-sign">
                                <?foreach ($arItem["DISPLAY_PROPERTIES"]["SEARCH"]["VALUE"] as $s=>$service):?>
                                    <?if(isset($arResult["SERVICES_ICON"][$service]) && !empty($arResult["SERVICES_ICON"][$service]["ICON"])):?>
                                        <li rel="tooltip" data-placement="top" title="<?=$arResult["SERVICES_ICON"][$service]["TITLE"]?>">
                                            <img alt="<?=$arResult["SERVICES_ICON"][$service]["TITLE"]?>" src="<?=$arResult["SERVICES_ICON"][$service]["ICON"]?>">
                                        </li>
                                    <?endif?>
                                <?endforeach;?>
                            </ul>
                            <div style="clear: both"></div>
                        <?endif?>
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCSERVICE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCCHILD"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_serviceschild"><?=GetMessage('HD_DESCCHILD')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCCHILD"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCBEACH"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_beach"><?=GetMessage('HD_DESCBEACH')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCBEACH"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCSPORT"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_sport"><?=GetMessage('HD_DESCSPORT')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCSPORT"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCSHEALTH"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_shealth"><?=GetMessage('HD_DESCSHEALTH')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_DESCSHEALTH"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["MEDICAL_TREATMENT"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_medical"><?=GetMessage('MEDICAL_TREATMENT')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["MEDICAL_TREATMENT"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INFRASTRUCTURE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_infrastructure"><?=GetMessage('INFRASTRUCTURE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["INFRASTRUCTURE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_TEXT"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price"><?=GetMessage('PRICE_TEXT')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PRICE_TEXT"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INCLUDED_IN_PRICE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price"><?=GetMessage('PRICE_INCLUDE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["INCLUDED_IN_PRICE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["ADDITIONAL_PAID"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_additional"><?=GetMessage('ADDITIONAL_PAID')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["ADDITIONAL_PAID"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["CANCELLATION"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_cancel"><?=GetMessage('CANCELLATION')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["CANCELLATION"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_ADDINFORMATION"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_addinfo"><?=GetMessage('HD_ADDINFORMATION')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HD_ADDINFORMATION"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PROEZD"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_proezd"><?=GetMessage('PROEZD')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PROEZD"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["ROUTE_INFO"])):?>
                <? $this->addExternalJs($templateFolder . "/jquery-custom-google-map-lib.js"); ?>
                <?
                $zoom = isset($arResult['MAP_SCALE']) && !empty($arResult['MAP_SCALE']) ? $arResult['MAP_SCALE'] : 16;
                ?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_map"><?=GetMessage('MAP')?></h3>
                    <div id="<?= $htmlMapID ?>" style="width:100%; height:500px;">
                        <script>
                            var cnt_point = '<?= count($arResult['ROUTE_INFO']) ?>';
                            (function (gm) {
                                    // init map and draw route
                                gm.createGoogleMap("<?= $htmlMapID ?>", {center: gm.LatLng(<?= $arResult['ROUTE_INFO'][0]["lat"] ?>, <?= $arResult['ROUTE_INFO'][0]["lng"] ?>), zoom: <?= $zoom?>})
                                    ._addMarker({position: gm.LatLng(<?= $arResult['ROUTE_INFO'][0]["lat"] ?>, <?= $arResult['ROUTE_INFO'][0]["lng"] ?>),
                                        map: gm._map,
                                        title: "<?=$arResult["NAME"]?>",
                                        infoWindow: '<div style="color:red"><b><?=$arResult["NAME"]?></b></div>',
                                        icon: "/local/templates/travelsoft/assets/images/map-marker.png"})
                                })(window.GoogleMapFunctionsContainer);
                        </script>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["FILE"]["VALUE"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3"><?=GetMessage('FILES')?></h3>
                    <div class="text-block">
                        <? if (count($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"]) > 1): ?>
                            <ul>
                                <? foreach ($arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"] as $file): ?>
                                    <li><a href="<?= $file["SRC"] ?>"><?= $file["DESCRIPTION"] ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        <? else: ?>
                            <a href="<?= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["SRC"] ?>"><?= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["DESCRIPTION"] ?></a>
                        <? endif ?>
                    </div>
                </div>
            <?endif?>

        </div>
        <div class="col-md-3 sidebar-widget sidebar" id="sidebar">
            <div class="">
                <div class="categories-widget widget">
                    <div class="content-widget">
                        <ul class="widget-list">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCROOM"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_room" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCROOM')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCMEAL"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_food" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCMEAL')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCSERVICE"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_services" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCSERVICE')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCCHILD"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_serviceschild" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCCHILD')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCBEACH"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_beach" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCBEACH')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCSPORT"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_sport" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCSPORT')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESCSHEALTH"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_shealth" class="link">
                                        <span class="fa-custom category"><?=GetMessage('HD_DESCSHEALTH')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INFRASTRUCTURE"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_infrastructure" class="link">
                                    <span class="fa-custom category"><?=GetMessage('INFRASTRUCTURE')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_TEXT"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["INCLUDED_IN_PRICE"]["VALUE"]["TEXT"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_price" class="link">
                                        <span class="fa-custom category"><?=GetMessage('PRICE_TEXT')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["ROUTE_INFO"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_map" class="link">
                                        <span class="fa-custom category"><?=GetMessage('MAP')?></span>
                                    </a>
                                </li>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"])):?>
                                <li class="single-widget-item">
                                    <a href="#detail_photo" class="link">
                                        <span class="fa-custom category"><?=GetMessage('PHOTO')?></span>
                                    </a>
                                </li>
                            <?endif?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('a.link[href^="#"]').on("click", function () {
            var target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top - 80}, 800);
            return false;
        });
        $(document).ready(function(){

          

        });
    </script>
