<?
$MESS["FROM_PRICE"] = "от";
$MESS["PHOTO"] = "Фотографии";
$MESS["MAP"] = "На карте";
$MESS["DESC"] = "Описание";
$MESS["IMG"] = "Картинка";
$MESS["FOOD"] = "Питание";
$MESS["FILES"] = "Файлы";
$MESS["OFFERS_TITLE"] = "Ценовые предложения";
$MESS["PLACEMENTS_TAB"] = "Проживание";
$MESS["PLACEMENTS_TRAVEL_TAB"] = "Проживание + проезд";
$MESS["TYPE_ID"] = "Тип размещения";
$MESS["HD_ADDRESS"] = "Адрес";
$MESS["HD_PHONE"] = "Телефон";
$MESS["HD_EMAIL"] = "E-mail";
$MESS["HD_HTTP"] = "Сайт";
$MESS["HD_DESCROOM"] = "Номера";
$MESS["HD_DESCMEAL"] = "Питание";
$MESS["HD_DESCSERVICE"] = "Описание услуг";
$MESS["HD_DESCCHILD"] = "Услуги для детейг";
$MESS["HD_DESCBEACH"] = "Пляж";
$MESS["HD_DESCSPORT"] = "Спорт и отдых";
$MESS["HD_DESCSHEALTH"] = "Оздоровление";
$MESS["MEDICAL_TREATMENT"] = "Лечебная база";
$MESS["INFRASTRUCTURE"] = "Инфраструктураа";
$MESS["PRICE_TEXT"] = "Стоимость";
$MESS["PRICE_TABLE"] = "Таблица цен";
$MESS["PRICE_INCLUDE"] = "В стоимость входит";
$MESS["ADDITIONAL_PAID"] = "Дополнительно оплачивается";
$MESS["CANCELLATION"] = "Условия отмены";
$MESS["PROEZD"] = "Проезд";
$MESS["HD_ADDINFORMATION"] = "Дополнительная информация";
$MESS["ROOMS"] = "Описание номеров";
?>