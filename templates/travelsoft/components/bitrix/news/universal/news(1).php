<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($APPLICATION->GetDirProperty("SHOW_TITLE") == "N"): ?><h1><?= $APPLICATION->ShowTitle(false); ?></h1><? endif ?>
<div class="row">
    <aside class="sidebar col-lg-3 order-1 order-lg-0">
        <? if ($arParams["USE_FILTER"] == "Y"): ?>
            <? if (isset($arParams["DEF_PARAMS_FILTER"]) && !empty($arParams['DEF_PARAMS_FILTER'])): ?>
                <?
                $APPLICATION->IncludeComponent("travelsoft:catalog.smart.filter", "filter", Array(
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"], // Учитывать права доступа
                    "CACHE_TIME" => $arParams["CACHE_TIME"], // Время кеширования (сек.)
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"], // Тип кеширования
                    "DISPLAY_ELEMENT_COUNT" => "Y", // Показывать количество
                    "FILTER_NAME" => $arParams["FILTER_NAME"], // Имя выходящего массива для фильтрации
                    "FILTER_VIEW_MODE" => "vertical", // Вид отображения
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"], // Инфоблок
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], // Тип инфоблока
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"], // Имя массива с переменными для построения ссылок в постраничной навигации
                    "SAVE_IN_SESSION" => "N", // Сохранять установки фильтра в сессии пользователя
                    "SECTION_CODE" => "", // Код раздела
                    "SECTION_DESCRIPTION" => "-", // Описание
                    "SECTION_ID" => $_REQUEST["SECTION_ID"], // ID раздела инфоблока
                    "SECTION_TITLE" => "-", // Заголовок
                    "SEF_MODE" => "N", // Включить поддержку ЧПУ
                    "TEMPLATE_THEME" => "blue", // Цветовая тема
                    "XML_EXPORT" => "N", // Включить поддержку Яндекс Островов
                    "DEF_PARAMS_FILTER" => (array) $arParams["DEF_PARAMS_FILTER"]
                        ), false
                );
                ?>

            <? else: ?>
                <?
                $APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "filter", Array(
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"], // Учитывать права доступа
                    "CACHE_TIME" => $arParams["CACHE_TIME"], // Время кеширования (сек.)
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"], // Тип кеширования
                    "DISPLAY_ELEMENT_COUNT" => "Y", // Показывать количество
                    "FILTER_NAME" => $arParams["FILTER_NAME"], // Имя выходящего массива для фильтрации
                    "FILTER_VIEW_MODE" => "vertical", // Вид отображения
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"], // Инфоблок
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], // Тип инфоблока
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"], // Имя массива с переменными для построения ссылок в постраничной навигации
                    "SAVE_IN_SESSION" => "N", // Сохранять установки фильтра в сессии пользователя
                    "SECTION_CODE" => "", // Код раздела
                    "SECTION_DESCRIPTION" => "-", // Описание
                    "SECTION_ID" => $_REQUEST["SECTION_ID"], // ID раздела инфоблока
                    "SECTION_TITLE" => "-", // Заголовок
                    "SEF_MODE" => "N", // Включить поддержку ЧПУ
                    "TEMPLATE_THEME" => "blue", // Цветовая тема
                    "XML_EXPORT" => "N", // Включить поддержку Яндекс Островов
                        ), false
                );
                ?>
            <? endif ?>
        <? endif ?>
        <!--
                <div class="sidebar__content js-sticky-top">
                    <div class="sidebar-filter modal card fade" id="sidebarFilter" tabindex="-1" role="dialog">
                        <button class="btn-toggle btn btn-light" type="button" data-dismiss="modal"><span class="icon-bar"></span></button>
                        <div class="modal-header bg-white">
                            <h4 class="modal-title">Уточнить результат</h4>
                        </div>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <ul class="sidebar-filter__list">
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#price" data-toggle="collapse" role="button"><span>Цена от</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="price">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">50 - 99 €</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">100 - 149 €</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">150 - 199 €</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">200 € +</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#accommodation" data-toggle="collapse" role="button"><span>Тип размещения</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="accommodation">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Гостиница</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">База отдыха</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Пансионат</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Санаторий</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#HotelAttrs" data-toggle="collapse" role="button"><span>Услуги в отеле</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="HotelAttrs">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Wi-Fi</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Паркинг</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Airport Shuttle</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Кондиционер</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Семейные номера</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Ресторан</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Конференц-зал / Банкетный зал</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Домашние животные разрешены</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#" data-toggle="collapse" role="button"><span>Звездный рейтинг</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show d-block" id="starRating">
                                                <div class="sidebar-filter__content">
                                                    <div class="sidebar-filter__stars rating d-flex justify-content-between align-items-center">
                                                        <select class="js-rating-filter" data-current-rating="3 of more">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                        <div class="amount ml-3"><span class="val"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#" data-toggle="collapse" role="button"><span>Пользовательская оценка</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show d-block" id="userRating">
                                                <div class="sidebar-filter__content">
                                                    <div class="sidebar-filter__rating">
                                                        <div class="mb-2" id="userRatingChange"></div>
                                                        <div class="d-flex justify-content-between"><span class="amount" id="userRatingMin"></span><span class="amount" id="userRatingMax">10</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#roomAttrs" data-toggle="collapse" role="button"><span>Оснащение номеров</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="roomAttrs">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Wi-Fi</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Сейф в номере</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Desck</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Телевизор</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Мини-бар</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Кондиционирование воздуха</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Ванная комната</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#mealBoard" data-toggle="collapse" role="button"><span>Питание</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="mealBoard">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Без питания</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Завтрак</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Завтрак + Ужин</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Полный пансион</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Все включено</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sidebar-filter__item">
                                            <h5 class="sidebar-filter__title"><a class="sidebar-filter__toggle d-flex justify-content-between align-items-center" href="#location" data-toggle="collapse" role="button"><span>Месторасположение</span><i class="fa fa-angle-down"></i></a></h5>
                                            <div class="collapse show" id="location">
                                                <div class="sidebar-filter__content">
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">менее 100 м. до пляжа</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">от 100 до 500 м. до плажа</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Первая линия</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Вторая линия</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"/><span class="custom-control-label pointer">Центр города</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        -->
    </aside>

    <?
    // расчет стоимости "от" для списка оъектов
    Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

    $mpf_result = \travelsoft\booking\Utils::getCaluculationsPriceFrom(function () use ($arParams) {

                $arData = [];

                $arFilter = ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"];
                if (isset($GLOBALS[$arParams["FILTER_NAME"]]) && is_array($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($GLOBALS[$arParams["FILTER_NAME"]])) {
                    $arFilter = array_merge($arFilter, $GLOBALS[$arParams["FILTER_NAME"]]);
                }

                $cache = new \travelsoft\booking\adapters\Cache(serialize($arFilter) . time(), "/travelsoft/booking/objects_for_price_from_results/");

                if (empty($arData = $cache->get())) {

                    $arData = $cache->caching(function () use ($arFilter, $arParams) {

                        $arData = [
                            "packageTour" => [],
                            "excursionTour" => [],
                            "placements" => []
                        ];

                        $dbElements = \CIBlockElement::GetList(false, $arFilter, false, false, ["ID", "PROPERTY_TOURTYPE"]);

                        if ($arParams["SERVICE_TYPE"] === "placements") {
                            while ($arElement = $dbElements->Fetch()) {
                                $arData["placements"]["services_id"][] = $arElement["ID"];
                            }
                        } else {
                            while ($arElement = $dbElements->Fetch()) {

                                if (\SEA_TOUR_TYPE_ID === (int) $arElement["PROPERTY_TOURTYPE_VALUE"]) {
                                    
                                    $id = travelsoft\booking\Utils::getPackageTourByDescription($arElement["ID"])["ID"];
                                    if ($id > 0) {
                                        $arData["packageTour"]["services_id"][] = $id;
                                    }
                                    
                                } else {
                                    $arData["excursionTour"]["excursions_id"][] = $arElement["ID"];
                                }
                            }
                        }

                        return $arData;
                    });
                }
                
                return $arData;
            }, function (&$result) use ($arParams) {

                $_result = $ids = [];

                foreach ($result as $service_type => $data) {

                    if (!empty($data)) {

                        if ($service_type === "excursionTour" || $service_type === "packageTour") {

                            if ($service_type === "excursionTour") {
                                $class = "\\travelsoft\\booking\\stores\\ExcursionTour";
                            } elseif ($service_type === "packageTour") {
                                $class = "\\travelsoft\\booking\\stores\\Travel";
                            }

                            foreach ($class::get([
                                "filter" => ["ID" => array_keys($data)],
                                "select" => ["UF_TOUR", "ID"]
                            ]) as $arTour) {
                                $_result[$arTour["UF_TOUR"]] = $data[$arTour["ID"]];
                                $ids[] = $arTour["UF_TOUR"];
                            }
                        } else {

                            $_result = \array_merge($result, $data);
                            $ids = \array_merge($ids, array_keys($data));
                        }
                    }
                }
                
                if (isset($GLOBALS[$arParams["FILTER_NAME"]]) && is_array($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($ids)) {
                    $GLOBALS[$arParams["FILTER_NAME"]]["ID"] = $ids;
                }

                $result = $_result;
            });
            
    ?>


    <div class="col-lg-9 category-content">
        <? $template = $arParams["TEMPLATE_LIST"] ?? ""; ?>
        <?
        $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.news.list", $template, Array(
            "MIN_PRICES" => $mpf_result,
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "NEWS_COUNT" => $arParams["NEWS_COUNT"],
            "SORT_BY1" => $arParams["SORT_BY1"],
            "SORT_ORDER1" => $arParams["SORT_ORDER1"],
            "SORT_BY2" => $arParams["SORT_BY2"],
            "SORT_ORDER2" => $arParams["SORT_ORDER2"],
            "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
            "MESSAGE_404" => $arParams["MESSAGE_404"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "SHOW_404" => $arParams["SHOW_404"],
            "FILE_404" => $arParams["FILE_404"],
            "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
            "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
            "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
            "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
            "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
            "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
            "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
            "CHECK_DATES" => $arParams["CHECK_DATES"],
                ), $component
        );
        ?>

    </div>
</div>


<!--

<nav class="pagination-nav" aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-left"></i></a></li>
                <li class="page-item"><a class="page-link" href="#">1</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">2</a>
                </li>
                <li class="page-item align-self-end d-sm-none"><a class="dotted" href="#">......</a>
                </li>
                <li class="page-item d-none d-sm-block"><a class="page-link" href="#">4</a>
                </li>
                <li class="page-item d-none d-sm-block"><a class="page-link" href="#">5</a>
                </li>
                <li class="page-item d-none d-sm-block"><a class="page-link" href="#">6</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">7</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">8</a>
                </li>
                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </nav>

-->

<!--
<nav class="sortbar navbar shadow-sm d-flex align-items-stretch">
    <div class="sortbar__title d-flex align-items-stretch">
        <button class="navbar-brand d-flex align-items-center pointer js-toggle-sidebar" data-target="#sidebarFilter" data-toggle="modal" type="button"><i class="d-lg-none icon icon-filter"></i><span class="ml-2 d-none d-lg-inline">Сортировать:</span></button>
    </div>
    <div class="sortbar__sort js-sorting d-flex align-items-stretch"><a class="nav-link" href="#">Название</a><a class="nav-link" href="#">Категория</a><a class="nav-link" href="#">Рейтинг</a><a class="nav-link d-flex d-md-none align-items-center" href="#"><span class="mr-2">Sort by</span><i class="icon icon-sort-duble"></i></a></div>
    <div class="sortbar__grid d-flex align-items-stretch ml-md-auto"><a class="nav-link active js-toggle-grid" href="#" data-grid="grid" data-cols="col-12 col-sm-6 col-xl-4 d-flex"><i class="icon icon-grid"></i></a><a class="nav-link js-toggle-grid" href="#" data-grid="list" data-cols="col-12 d-flex"><i class="icon icon-list-mb"></i></a></div>
</nav>
-->