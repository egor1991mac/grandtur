<?
$MESS["T_IBLOCK_DESC_DETAIL_FORM_SEARCH"] = "Путь к результатам поиска";
$MESS["T_IBLOCK_DESC_HIDE_PARAMS_FILTER"] = "Свойства фильтра для скрытия";
$MESS["T_IBLOCK_DESC_DEF_PARAMS_FILTER"] = "Дополнительная фильтрафия";
$MESS["T_IBLOCK_DESC_TEMPLATE_LIST"] = "Шаблон списка элементов";
$MESS["T_IBLOCK_DESC_TEMPLATE_DETAIL"] = "Шаблон детальной страницы";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Выводить дату элемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Выводить изображение для анонса";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Выводить текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Отображать панель соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_HIDE"] = "Не раскрывать панель соц. закладок по умолчанию";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "Шаблон компонента панели соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "Используемые соц. закладки и сети";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "Логин для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
?>