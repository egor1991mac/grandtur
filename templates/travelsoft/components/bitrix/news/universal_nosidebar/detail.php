<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/select2.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/icomoon_/style.min.css");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/select2.min.js");
\Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/booking/main-search-form-detail.js");

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");


########################### search offers content ##############################
$GLOBALS = \array_merge($GLOBALS, \travelsoft\booking\Utils::getHTMLSearchOffersFormAndOffersListOnDetailPage([
    "service_type" => function () use ($arParams, $arResult) {
        if ($arParams["DYNAMICALLY_DETECT_SEARCH_OFFERS_PARAMETERS"]) {
            $arElement = \CIBlockElement::GetList(false, ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]], false, false, ["ID", "PROPERTY_TOURTYPE"])->Fetch();
            
            if (isset($arElement["ID"])) {
                
                if (!is_array($arElement["PROPERTY_TOURTYPE_VALUE"])) {
                    $arElement["PROPERTY_TOURTYPE_VALUE"] = [$arElement["PROPERTY_TOURTYPE_VALUE"]];
                }
                
                if (!in_array(SEA_TOUR_TYPE_ID, $arElement["PROPERTY_TOURTYPE_VALUE"])) {
                    return "excursiontour";
                }
            }
            
            return "packagetour";
        } else {
            return $arParams["SERVICE_TYPE"] ?: "placements";
        }
    },
    "search_form_component_template" => "inner",
    "offers_component_template" => $arParams["TEMPLATE_DETAIL_SEARCH_OFFERS"] ? $arParams["TEMPLATE_DETAIL_SEARCH_OFFERS"] : "simple",
    "booking_url" => "/booking/",
    "element_code" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "highlight_dates_parameters" => function () use ($arParams, $arResult) {

        if ($arParams["DYNAMICALLY_DETECT_SEARCH_OFFERS_PARAMETERS"]) {

            $arElement = \CIBlockElement::GetList(false, ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]], false, false, ["ID", "PROPERTY_TOURTYPE"])->Fetch();
            if (isset($arElement["ID"])) {
                if (!in_array(SEA_TOUR_TYPE_ID, $arElement["PROPERTY_TOURTYPE_VALUE"])) {
                    return [
                        "CALENDAR_FROM" => [
                            "SERVICE_TYPE" => "excursiontour",
                            "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_from]']"
                        ]
                    ];
                }
            }
            return [
                "CALENDAR_FROM" => [
                    "SERVICE_TYPE" => "transfer",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_from]']"
                ],
                "CALENDAR_TO" => [
                    "SERVICE_TYPE" => "transferback",
                    "CALENDAR_CSS_SELECTOR" => "input[name='travelbooking[date_to]']"
                ]
            ];
        } else {
            return isset($arParams["HIGHLIGHT_FOR"]) && is_array($arParams["HIGHLIGHT_FOR"]) ? $arParams["HIGHLIGHT_FOR"] : [];
        }

    }
]));
################################################################################
?>

<template id="children-age-template">
    <div class="select-age-box form-group">
        <label>Возраст ребенка {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control styled">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
        </select>
    </div>
</template>

        <? $template = $arParams["TEMPLATE_DETAIL"] ?? ""; ?>
        <?$ElementID = $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.detail",
            $template,
            Array(
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
                "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "META_KEYWORDS" => $arParams["META_KEYWORDS"],
                "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
                "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
                "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
                "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
                "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
                "CHECK_DATES" => $arParams["CHECK_DATES"],
                "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
                "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                "USE_SHARE" => $arParams["USE_SHARE"],
                "SHARE_HIDE" => $arParams["SHARE_HIDE"],
                "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
                "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
                "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
                'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
                "SEARCH_FORM_HASH" => md5($GLOBALS["SEARCH_FORM_HTML"]),
                "SEARCH_OFFERS_RESULT_HTML" => md5($GLOBALS["SEARCH_OFFERS_RESULT_HTML"]),
            ),
            $component
        );?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"]?>"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a>
		</div>
	</div>
</div>