<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="js-nav-scroll js-sticky-top" id="pageNav">
    <ul class="list-group">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
        <li class="list-group-item p-0 rounded-0 active"><a href="<?=$arItem["LINK"]?>" class="d-block p-3"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
        <li class="list-group-item p-0 rounded-0"><a href="<?=$arItem["LINK"]?>" class="d-block p-3"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>
    </ul>
</nav>
<?endif?>