<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

ShowMessage($arParams["~AUTH_RESULT"]); 


$APPLICATION->IncludeComponent( 
   "bitrix:main.register", 
   "d3p", 
   Array( 
      "USER_PROPERTY_NAME" => "", 
      "SEF_MODE" => "N", 
      "SHOW_FIELDS" => Array("PERSONAL_PHONE", "NAME", "SECOND_NAME", "LAST_NAME"), 
      "REQUIRED_FIELDS" => Array("PERSONAL_PHONE"), 
      "AUTH" => "Y", 
      "USE_BACKURL" => "Y", 
      "SUCCESS_PAGE" => $APPLICATION->GetCurPageParam('',array('backurl')), 
      "SET_TITLE" => "N", 
      "USER_PROPERTY" => Array("UF_BIK", "UF_ACTUAL_ADDRESS", "UF_OKPO", "UF_UNP", "UF_ACCOUNT_CURRENCY",
          "UF_CHECKING_ACCOUNT", "UF_BANK_CODE", "UF_BANK_ADDRESS", "UF_BANK_NAME", "UF_LEGAL_ADDRESS", "UF_LEGAL_NAME") 
   ) 
); 

?><p><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a></p><?

?>