<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>
	<div id="<?echo $CONTAINER_ID?>">
        <form action="<?echo $arParams["PAGE"]?>" class="navbar-search">
            <div class="input-group flex-nowrap">
                <input id="<?echo $INPUT_ID?>" class="form-control pr-0 js-navbar-search-input" type="text" name="q" value="" size="40" placeholder="<?=GetMessage("CT_BST_SEARCH_BUTTON");?>" maxlength="50" autocomplete="off" />
                <div class="input-group-append">
                    <button class="navbar-search__subject btn btn-light" name="s" type="submit"><i class="fa fa-search text-gray"></i></button>
                </div>
            </div>
        </form>
	</div>
<?endif?>

