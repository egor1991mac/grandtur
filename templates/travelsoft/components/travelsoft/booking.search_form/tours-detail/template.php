<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

?>

<div class="sidebar__content js-sticky-top">
    <div class="sidebar__finder card">
        <h4 class="m-0 mb-lg-4"><?=GetMessage('TRAVELBOOKING_TOURS_TITLE_FORM')?></h4>
        <form action="/bus-tours/" method="get" class="collapse show d-lg-block mt-3" data-toggle="validator">
            <input type="hidden" name="s2o" value="Y">
            <input type="hidden" name="searchUrl" value="<?=$arParams["ACTION_URL"]?>">
            <div class="hotel-search-form">
                <div class="row">
                    <div class="col-12 form-group">
                        <label class="label-text"><?= GetMessage("TRAVELBOOKING_TOURS_CITY") ?></label>
                        <span class="form-select select_2">
                        <select name="arrFilter_158" id="cities" class="form-control select2 js-form-select" data-placeholder="<?= GetMessage("TRAVELBOOKING_TOURS_CITY_PLACEHOLDER") ?>">
                            <option></option>
                            <?foreach($arResult["CITIES"] as $arCity):?>
                                <option value="Y" data-val="<?= abs(crc32($arCity["ID"]))?>"><?= $arCity["NAME"]?></option>
                            <?endforeach?>
                        </select>
                    </span>
                    </div>
                    <div class="col-12 form-group">
                        <label class="label-text"><?= GetMessage("TRAVELBOOKING_TOURS") ?></label>
                        <span class="form-select select_2">
                        <select id="tour_id" name="" class="form-control select2 js-form-select" data-placeholder="<?= GetMessage("TRAVELBOOKING_TOURS_PLACEHOLDER") ?>">
                            <option></option>
                            <?/*foreach($arResult["TOURS"] as $arTour):*/?><!--
                                <option value="<?/*= $arTour["DETAIL_PAGE_URL"]*/?>~<?/*= $arTour["ID"]*/?>"><?/*= $arTour["NAME"]*/?></option>
                            --><?/*endforeach*/?>
                        </select>
                    </span>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-12 form-group">
                                <label class="label-text" for="sea_date_from"><?= GetMessage("TRAVELBOOKING_TOURS_DATE_FROM") ?></label>
                                <div class="input-group-append"><i class="icon icon-calendar"></i>
                                    <input data-service-type="excursiontour" data-date-default="<?= $arResult["DATE_FROM"]?>" id="sea_date_from" autocomplete="off" value="<?= $arResult["DATE_FROM"] ?>" class="input-datepicker-form form-control js-input-date flatpickr-input" name="travelbooking[date_from]" type="text" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-12 form-group">
                                <label class="label-text" for="sea_date_to"><?= GetMessage("TRAVELBOOKING_TOURS_DATE_TO") ?></label>
                                <div class="input-group-append"><i class="icon icon-calendar"></i>
                                    <input data-date-default="<?= $arResult["DATE_TO"]?>" id="sea_date_to" autocomplete="off" value="<?= $arResult["DATE_TO"] ?>" class="input-datepicker-form form-control js-input-date" name="travelbooking[date_to]" type="text" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 col-sm-6 form-group col-lg-6">
                            <span class="form-select">
                                <select name="travelbooking[adults]" class="form-control styled">
                                    <option <? if ($arResult["ADULTS"] === 1): ?>selected<? endif ?> value="1">1 <?=GetMessage('TRAVELBOOKING_ADULTS_1')?></option>
                                    <option <? if ($arResult["ADULTS"] === 2): ?>selected<? endif ?> value="2">2 <?=GetMessage('TRAVELBOOKING_ADULTS_2')?></option>
                                    <option <? if ($arResult["ADULTS"] === 3): ?>selected<? endif ?> value="3">3 <?=GetMessage('TRAVELBOOKING_ADULTS_2')?></option>
                                    <option <? if ($arResult["ADULTS"] === 4): ?>selected<? endif ?> value="4">4 <?=GetMessage('TRAVELBOOKING_ADULTS_2')?></option>
                                </select>
                            </span>
                            </div>
                            <div class="col-12 col-sm-6 form-group col-lg-6">
                            <span class="form-select">
                                <select class="form-control styled" name="travelbooking[children]">
                                    <option value="0"><?= GetMessage("TRAVELBOOKING_WITHOUT_CHILDREN") ?></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                                <div class="children-age-box hidden_"><div class="closer">&times;</div></div>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-secondary btn--round mx-auto mt-2 w-100 search-btn" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?>
            </button>
        </form>
    </div>
</div>

<script>

    var objects_for_city = <?=json_encode($arResult["CITIES_SEARCH_OBJECT"])?>;

    console.log(objects_for_city);

    $('body').on("change", "select#cities", function () {

        var $this = $(this);

        var value_id = $(this).find(':selected').data('val');
        var value_name = $this.attr('name');

        $('select#cities').attr("name",value_name + '_' + value_id);

        var form = $this.closest("form");

        var objects = objects_for_city[value_id];

        var option_objects = '<option value="#object_id#">#object_name#</option>';

        var htmlObjects = '<option></option>';

        for (object_ in objects) {

            option_objects_ = option_objects;
            option_objects_ = option_objects_.replace("#object_id#", objects[object_]["DETAIL_PAGE_URL"] + '~' + objects[object_]["ID"]);
            option_objects_ = option_objects_.replace("#object_name#", objects[object_]["NAME"]);
            htmlObjects += option_objects_;

        }

        $('#tour_id').select2('destroy');
        form.find("#tour_id").html(htmlObjects);
        $('#tour_id.select2').select2({
            allowClear: true,
            formatNoMatches: function () {
                return "Ничего не найдено";
            },
            placeholder: $this.data("placeholder")
        });


    });

</script>
