<?php

/*
 * Обработка запроса для получения дат подстветки в календаре
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

header('Content-Type: application/json; charset=' . SITE_CHARSET);

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$response = array();

if (!empty($_POST["services_id"]) && is_array($_POST["services_id"])) {

    $service = explode("~", $_POST["services_id"]);
    $arServices = $service[1];

    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>(int)30,"ACTIVE"=>"Y","ID"=>$arServices), false, false, Array("ID", "PROPERTY_TOURTYPE"));
    while($ar_fields = $res->GetNext())
    {
        if($ar_fields["PROPERTY_TOURTYPE_VALUE"] == SEA_TOUR_TYPE_ID) {
            $service_type = "packageTour";
            $service_id = \travelsoft\booking\stores\PackageTour::get(array(
                "filter" => array(
                    "UF_TOUR" => $arServices),
                "select" => array("ID"),
                "limit" => 1
            ));
        } else {
            $service_type = "excursionTour";
            $service_id = \travelsoft\booking\stores\ExcursionTour::get(array(
                "filter" => array(
                    "UF_TOUR" => $arServices),
                "select" => array("ID"),
                "limit" => 1
            ));
        }
    }

    $arr_quotas = travelsoft\booking\stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $service_id,
                    "UF_SERVICE_TYPE" => $service_type,
                    "UF_STOP" => false,
                    "!UF_QUOTA" => false,
                    ">=UF_DATE" => travelsoft\booking\adapters\Date::createFromTimestamp(time())),
                "select" => array("ID", "UF_DATE", "UF_SERVICE_ID")
    ));

    foreach ($arr_quotas as $arr_quota) {
        $response[] = \travelsoft\booking\Utils::dateConvert($arr_quota["UF_DATE"]->toString());
    }
}

echo json_encode(array_values(array_unique($response)));
