<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arListFields = [
    'Тип тура' => strip_tags($arResult['DISPLAY_PROPERTIES']['TOURTYPE']['DISPLAY_VALUE']),
    'Маршрут' => $arResult['DISPLAY_PROPERTIES']['ROUTE']['DISPLAY_VALUE'],
    'Даты тура' => $arResult['DISPLAY_PROPERTIES']['DEPARTURE']['DISPLAY_VALUE'],
    'Количество ночей' => num2word($arResult['DISPLAY_PROPERTIES']['DAYS']['DISPLAY_VALUE'],array('ночь','ночи','ночей')),
    'Ночные переезды' => $arResult['DISPLAY_PROPERTIES']['NIGHT_SHIFTS']['DISPLAY_VALUE'],
    'Продолжительность тура' => $arResult['DISPLAY_PROPERTIES']['DURATION']['DISPLAY_VALUE']. " км",
    'Длительность тура' => num2word($arResult['DISPLAY_PROPERTIES']['DURATION_TIME']['DISPLAY_VALUE'], array('час', 'часа', 'часов')),
    'Транспорт' => $arResult['DISPLAY_PROPERTIES']['TRANSPORT']['DISPLAY_VALUE'],
    'Питание' => $arResult['DISPLAY_PROPERTIES']['FOOD']['DISPLAY_VALUE'],
];

$arTextFields = [
    'Описание' => $arResult['DISPLAY_PROPERTIES']['HD_DESC']['DISPLAY_VALUE'],
    'Проживание' => $arResult['DISPLAY_PROPERTIES']["HOTEL"]["DISPLAY_VALUE"].'<br>'.$arResult['DISPLAY_PROPERTIES']['HOTEL_DESCRIPTION']['DISPLAY_VALUE'],
    'Питание' => $arResult['DISPLAY_PROPERTIES']['FOOD_DESCRIPTION']['DISPLAY_VALUE'],
    'Таблица цен' => $arResult['DISPLAY_PROPERTIES']['PRICE_TABLE']['DISPLAY_VALUE'],
    'Стоимость тура' => $arResult['DISPLAY_PROPERTIES']['PRICE_TOUR']['DISPLAY_VALUE'],
    'В стоимость тура входит' => $arResult['DISPLAY_PROPERTIES']['PRICE_INCLUDE']['DISPLAY_VALUE'],
    'В стоимость тура не входит' => $arResult['DISPLAY_PROPERTIES']['PRICE_NO_INCLUDE']['DISPLAY_VALUE'],
    'Необходимые документы' => $arResult['DISPLAY_PROPERTIES']['DOCUMENT']['DISPLAY_VALUE'],
    'Медицина' => $arResult['DISPLAY_PROPERTIES']['MEDICINE']['DISPLAY_VALUE'],
    'Дополнительно' => $arResult['DISPLAY_PROPERTIES']['ADDITIONAL']['DISPLAY_VALUE'],
    'Примечание' => $arResult['DISPLAY_PROPERTIES']['NOTE']['DISPLAY_VALUE'],
];
?>
<?
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/MapAdapter.js");
?>

        <div class="barba-container">
            <nav class="hotel-nav">
                <ul class="hotel-nav__menu nav nav-tabs" role="tablist">
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link active" data-toggle="tab" href="#tour" role="tab" aria-controls="tour" aria-selected="false">
                            <span>Информация о туре</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#tourProgramm" role="tab" aria-controls="tourProgramm" aria-selected="true">
                            <span>Программа тура по дням</span>
                        </a>
                    </li>
                    <!--<li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#hotelAccommodation" role="tab" aria-controls="hotelAccommodation" aria-selected="true">
                            <span>Условия проживания</span>
                        </a>
                    </li>-->
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#tourReviews" role="tab" aria-controls="tourReviews" aria-selected="true">
                            <span>Отзывы гостей</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="tab-content">
                <div class="tab-pane active show" id="tour" role="tabpanel">
                    <section>
                        <? if ($APPLICATION->GetDirProperty("SHOW_TITLE") == "N"): ?>
                            <h1 class="fw-md mb-1"><?= $APPLICATION->ShowTitle(false); ?></h1>
                        <?else:?>
                            <h3 class="fw-md mb-1">
                                <?=$arResult['NAME']?>
                            </h3>
                        <?endif?>
                        <ul class="hotel-title mb-3">
                            <li class="mb-2">
                                <?if(!empty($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])):?>
                                    <?=star_rating($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])?>
                                <?endif;?>
                            </li>
                            <li class="d-flex">
                                <div class="media-object"><i class="icon icon-label mr-2 text-primary"></i></div>
                                <div class="local">
                                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'])):?>
                                        <span>
                                            <?=$arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE']?>

                                            <?if(!empty($arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])):?>
                                                ,
                                                <?if(count($arResult['DISPLAY_PROPERTIES']['TOWN']['VALUE']) == 1):?>
                                                    <?=$arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']?>
                                                <?else:?>
                                                    <?=implode2("-",$arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'],true)?>
                                                <?endif?>
                                            <?endif;?>
                                        </span>
                                    <?endif;?>
                                    <?if(isset($arResult["ROUTE_INFO"]) && !empty($arResult["ROUTE_INFO"])):?>
                                        <i class="bullet mx-2"></i>
                                        <a href="#modalMap" data-location='<?=\Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"])?>' data-toggle="modal" data-title="<?=$arResult["NAME"]?>">
                                            Показать на карте
                                        </a>
                                    <?endif;?>
                                </div>
                            </li>
                        </ul>
                        <?$images = getSrc($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 825, 'height' => 500], NO_PHOTO_PATH_825_500)?>
                        <div class="hotel-gallery">
                            <div class="hotel-gallery__carousel swiper-container js-hotel-carousel">
                                <div class="swiper-wrapper">
                                    <?foreach($images as $image):?>
                                        <div class="swiper-slide">
                                            <img class="img-fluid img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                        </div>
                                    <?endforeach;?>
                                </div>
                                <?if(count($images) >  1):?>
                                    <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev" role="button">
                                            <i class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                    <path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/>
                                                    <path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/>
                                                </svg>
                                            </i>
                                        </a>
                                        <a class="hotel-gallery__arrow shadow-sm js-next" role="button">
                                            <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                    <path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/>
                                                    <path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/>
                                                </svg>
                                            </i>
                                        </a>
                                    </div>
                                <?endif;?>
                            </div>
                            <?if(count($images) >  1):?>
                                <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                                    <div class="swiper-wrapper">
                                        <?foreach($images as $image):?>
                                            <div class="swiper-slide">
                                                <a class="hotel-gallery__thumb js-gallery-link" href="<?=$image?>">
                                                    <img class="img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                                </a>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12 d-flex">
                                <div class="hotel__intro">
                                    <section class="pb-4">
                                        <div class="row">
                                            <div class="col-12 col-md-8">
                                                <ul class="booking-card__checklist">
                                                    <?foreach ($arListFields as $name => $value):?>
                                                        <?if(!empty($value)):?>
                                                            <li class="d-flex justify-content-between flex-wrap flex-column flex-sm-row mb-3">
                                                                <div class="fw-sm">
                                                                    <?=$name?>:
                                                                </div>
                                                                <span>
                                                                    <?=$value?>
                                                                </span>
                                                            </li>
                                                        <?endif;?>
                                                    <?endforeach;?>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                    <?foreach ($arTextFields as $name => $value):?>
                                        <?if(!empty($value)):?>

                                            <section class="mb-5">
                                                <h4><?=$name?></h4>
                                                <p><?=$value?></p>
                                            </section>

                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                        <div class="hotel-items">
                            <?// ФОРМА ПОИСКА ЦЕНОВЫХ ПРЕДЛОЖЕНИЙ
                            if ($GLOBALS["SEARCH_FORM_HTML"]):?>
                                <div class="cart-header">
                                    <h4 class="card-title">Бронирование</h4>
                                    <hr>
                                </div>
                                <div class="row"><div class="col-md-12"><? echo $GLOBALS["SEARCH_FORM_HTML"]; ?></div></div>
                            <?endif?>
                            <?// вывод цен из travelbooking по размещению
                            if ($GLOBALS["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"]):?>
                                <div class="row"><div class="col-md-12">
                                        <!--<div class="cart-header">
                                            <h4 class="card-title">Номера</h4>
                                            <hr>
                                        </div>-->
                                        <?echo $GLOBALS["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"]; ?>
                                    </div>
                                </div>

                            <?endif?>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="tourProgramm" role="tabpanel">
                    <section>
                        <h3 class="fw-md">Программа тура по дням</h3>
                        <hr class="mb-4">
                        <div class="hotel-checklist">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]["VALUE"])):?>
                                <?foreach($arResult["DISPLAY_PROPERTIES"]["NDAYS"]['~VALUE'] as $key => $nday):?>

                                    <div class="row">
                                        <div class="col-12 col-sm-4">
                                            <h5 class="title d-flex"><i class="icon icon-calendar mr-2"></i>
                                                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]['DESCRIPTION'][$key])):?>
                                                    <?=$arResult["DISPLAY_PROPERTIES"]["NDAYS"]['DESCRIPTION'][$key];?>
                                                <?else:?>
                                                    <?= "День " . ($key + 1);?>
                                                <?endif;?>
                                            </h5>
                                        </div>
                                        <div class="col-12 col-sm-8">
                                            <p><?=$nday['TEXT']?></p>
                                        </div>
                                    </div>

                                <?endforeach;?>
                            <?endif;?>
                        </div>
                    </section>
                </div>
                <!--<div class="tab-pane" id="hotelAccommodation" role="tabpanel">
                    <section>
                        <h3 class="fw-md">Accommodation conditions</h3>
                        <hr class="mb-4">
                        <div class="hotel-checklist hotel-card">
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-calendar mr-2"></i>Check-in:</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <p>From 15:00 hours</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-calendar mr-2"></i>Check-out:</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <p>15:00 hours</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-info mr-2"></i>Cancellation / prepayment</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <p>Cancellation and prepayment policies vary by room category. <a href="#">Please enter the dates of your stay </a>and read the booking conditions for the requested room.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-family mr-2"></i>Accommodation of children</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <p>All children are welcome.</p>
                                    <p><span class="mark bg-primary-light"><span class="bold">strong Is free!  </span>One child under 2 years stays free of charge in a baby cot.</span></p>
                                    <p>Maximum capacity of extra beds / babycots in a room is 1.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-pets mr-2"></i>Pets</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <p>Pets are allowed on request. This service can be paid.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h5 class="title d-flex"><i class="icon icon-credits mr-2"></i>Accepted for payment</h5>
                                </div>
                                <div class="col-12 col-sm-8">
                                    <ul class="hotel-checklist__payment">
                                        <li class="d-inline-block mr-2 mb-2"><img src="img/american-express.jpg" alt="american-express"></li>
                                        <li class="d-inline-block mr-2 mb-2"><img src="img/visa.jpg" alt="visa"></li>
                                        <li class="d-inline-block mr-2 mb-2"><img src="img/master-card.jpg" alt="master-card"></li>
                                        <li class="d-inline-block mr-2 mb-2"><img src="img/maestro.jpg" alt="maestro"></li>
                                        <li class="d-inline-block mr-2 mb-2"><img src="img/discover.jpg" alt="discover"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="hotel-popular">
                        <h3>Popular hotels in the area</h3>
                        <hr class="mb-4">

                        <?/*// Set in component prolog*/?>
                        <?/*= $arParams['SIMILAR_HOTELS'] */?>

                    </section>
                </div>-->
                <div class="tab-pane" id="tourReviews" role="tabpanel">
                    <section>

                        <?// Set in component prolog?>
                        <?=$arParams['REVIEWS']?>

                    </section>
                </div>
            </div>
        </div>

<script>

    $(document).ready(function () {
        var mapAdapter = new MapAdapter({
            map_id: "map",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });

        $('body').on("show.bs.modal","#modalMap", function (e) {

            var t=$(e.relatedTarget),o=t.data("title"),n=$(this);n.find(".modal-title .title").text(o),loc=t.data('location');

            if(typeof loc === "object" && loc.length > 1) {
                mapAdapter.drawRoute(loc);
            } else {
                mapAdapter.addMarker(loc[0]);
            }

        })

    });


</script>