<?php

if (!empty($arResult['DISPLAY_PROPERTIES']['SERVICES']['VALUE'])) {
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arResult['DISPLAY_PROPERTIES']['SERVICES']['LINK_IBLOCK_ID'], "ID" => $arResult['DISPLAY_PROPERTIES']['SERVICES']['VALUE'], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $arResult["SERVICES"][$arFields['ID']] = [
            'NAME' => $arFields['NAME'],
            'ICON' => '<i class="align-middle text-secondary fa fa-' . $arProps['ICON']['VALUE'] . '" aria-hidden="true"></i>',
        ];
    }
}

$iblockTown = 0;
$town = array();

if (!empty($arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]) && !in_array($arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"],$town)) {
    if($iblockTown == 0){
        $iblockTown = $arResult["PROPERTIES"]["POINT_DEPARTURE"]["LINK_IBLOCK_ID"];
    }
    $town[] = $arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"];
}
if (!empty($arResult["PROPERTIES"]["TOWN"]["VALUE"])) {
    if($iblockTown == 0){
        $iblockTown = $arResult["PROPERTIES"]["TOWN"]["LINK_IBLOCK_ID"];
    }
    foreach ($arResult["PROPERTIES"]["TOWN"]["VALUE"] as $k=>$arTown) {
        if(!in_array($arTown,$town)){
            $town[] = $arTown;
        }
    }
}


if(!empty($town) && $iblockTown != 0) {
    $arResult["TOWN"] = array();
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $iblockTown, "ID" => $town, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "PROPERTY_MAP"));
    while ($ob = $res->GetNext()) {
        $arResult["TOWN"][$ob["ID"]] = array("NAME" => $ob["NAME"], "MAP" => $ob["PROPERTY_MAP_VALUE"]);
    }

    if (!empty($arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"])) {

        $LATLNG = explode(",", $arResult["TOWN"][$arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]["MAP"]);

        $arResult['ROUTE_INFO'][] = array(
            "lat" => $LATLNG[0],
            "lng" => $LATLNG[1],
            "title" => $arResult["TOWN"][$arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]['NAME']
        );
    }

    if (!empty($arResult["PROPERTIES"]["TOWN"]["VALUE"])) {

        foreach ($arResult["PROPERTIES"]["TOWN"]["VALUE"] as $k=>$arTown) {

            if($arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"] != $arTown) {

                $LATLNG = explode(",", $arResult["TOWN"][$arTown]["MAP"]);

                $arResult['ROUTE_INFO'][] = array(
                    "lat" => $LATLNG[0],
                    "lng" => $LATLNG[1],
                    "title" => $arResult["TOWN"][$arTown]['NAME'],
                );
            }

        }

    }

    if (!empty($arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"])) {

        $LATLNG = explode(",", $arResult["TOWN"][$arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]["MAP"]);

        $arResult['ROUTE_INFO'][] = array(
            "lat" => $LATLNG[0],
            "lng" => $LATLNG[1],
            "title" => $arResult["TOWN"][$arResult["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]['NAME']
        );
    }

}