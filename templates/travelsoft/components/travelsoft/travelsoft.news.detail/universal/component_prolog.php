<?
global $APPLICATION;

if (!empty($this->arPrologData["ELEMENT"])) {

    $arFields = $this->arPrologData["ELEMENT"]->GetFields();
    $arProps = $this->arPrologData["ELEMENT"]->GetProperties();

    ob_start();

    $APPLICATION->IncludeComponent("travelsoft:reviews", "reviews", Array(
        "LINK_ELEMENT_ID" => $arFields["ID"],    // ID элемента для отзыва
        "NEED_PREMODERATION" => "N",    // Нужна премодерация отзывов
        "PAGE_SIZE" => "10",    // Количество элементов на странице
        "SHOW_ADD_IMAGE_FIELD" => "Y",    // Добавить возможность загрузки фото
        "SHOW_RATING_FIELD" => "Y",    // Показывать поле для рейтинга
        "SHOW_STATISTICS" => "Y",    // Показывать статистику по отзывам
    ),
        false
    );

    $this->arParams['REVIEWS'] = ob_get_clean();


    ob_start();

    $APPLICATION->IncludeComponent(
        "travelsoft:travelsoft.news.list",
        "rooms",
        array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILE_404" => "",
            "FILTER_NAME" => "",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "6",
            "IBLOCK_TYPE" => "geography_and_product",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "100",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array(
                0 => "HOTEL",
                1 => "PICTURES",
                2 => "AREA_OF_THE_ROOM",
                3 => "PREVIEW_TEXT",
                4 => "DESCRIPTION",
                5 => "",
            ),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "Y",
            "SET_TITLE" => "N",
            "SHOW_404" => "Y",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "COMPONENT_TEMPLATE" => "rooms",
            "AFP_ID" => "",
            "AFP_72" => array(
                0 => $arFields['ID'],
            ),
            "AFP_75" => array()
        ),
        false
    );

    $this->arParams['ROOMS'] = ob_get_clean();

    ob_start();

    if(!empty($arProps['COUNTRY']['VALUE'])) {

        $GLOBALS['arrFilterSimilarHotels'] = [
            '!ID' => $arFields['ID']
        ];
        $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.list",
            "universal_list",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILE_404" => "",
                "FILTER_NAME" => "arrFilterSimilarHotels",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "7",
                "IBLOCK_TYPE" => "geography_and_product",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "9",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "TYPE_ID",
                    1 => "STARS",
                    2 => "CURRENCY_BY",
                    3 => "PRICE_MIN_BY",
                    4 => "PREVIEW_TEXT",
                    5 => "HD_DESC",
                    6 => "TOWN",
                    7 => "PICTURES",
                    8 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "Y",
                "SET_TITLE" => "N",
                "SHOW_404" => "Y",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "COMPONENT_TEMPLATE" => "universal_list",
                "AFP_78" => array(
                    0 => $arProps['COUNTRY']['VALUE'],
                ),
            ),
            false
        );
    }

    $this->arParams['SIMILAR_HOTELS'] = ob_get_clean();
}
