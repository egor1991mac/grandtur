<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="category-content__items row pad-lg-20 row-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['FLAG']['VALUE'], ['width' => 32, 'height' => 32]) ?>

    <div class="col-sm-3 d-flex" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="product bg-white js-grid-item product--list">
            <a class="d-block" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                <img class="img-fluid" src="<?=$images[0]?>" alt="<?=$arItem['NAME']?>"/>
            </a>
            <div class="product__body">
                <h4 class="product__title">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <?=$arItem['NAME']?>
                    </a>
                </h4>
            </div>
        </div>
    </div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<!--<script>

    $(document).ready(function () {
        var mapAdapter = new MapAdapter({
            map_id: "map",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });

        $('body').on("show.bs.modal","#modalMap", function (e) {

            var t=$(e.relatedTarget),o=t.data("title"),n=$(this);n.find(".modal-title .title").text(o),loc=t.data('location');

            if(typeof loc === "object" && loc.length > 1) {
                mapAdapter.drawRoute(loc);
            } else {
                mapAdapter.addMarker(loc[0]);
            }

        })

    });


</script>-->
