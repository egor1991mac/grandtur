<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('travelsoft.currency');
?>
<?if(isset($arParams["TITLE_BLOCK"]) && !empty($arParams["TITLE_BLOCK"])):?>
<div class="row">
    <div class="col-12 text-center">
        <div class="section-header">
            <h2 class="h2"><?=$arParams["TITLE_BLOCK"]?></h2>
        </div>
    </div>
</div>
<?endif?>
<div class="row">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <? $images = getSrc(($arItem['DISPLAY_PROPERTIES']['PICTURE_PREVIEW']['VALUE'] ?? $arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE']), ['width' => 377, 'height' => 262], NO_PHOTO_PATH_377_262, 1) ?>

        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="card card-hotel w-100 mb-2">
                <div class="card-hotel__img">
                    <img src="<?= $images[0] ?>" class="img-cover" alt="#">
                </div>
                <? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])): ?>
                    <div class="card-price">
                        <span class="mr-1">от</span><span class="count text-secondary">
                            <?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])->getResult(); ?>
                        </span>
                    </div>
                <? endif; ?>
                <div class="card-hotel__bottom">
                    <h4 class="h4 mb-1">
                        <?= $arItem['NAME'] ?>
                    </h4>
                    <div class="card-hotel__local d-flex align-items-center">
                        <ul class="flex-wrap">
                            <? if (!empty($arItem['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'])): ?>
                                <li class="mr-4">
                                    <i class="icon icon-label text-secondary mr-1" style="margin-bottom: 2px"></i>
                                    <span style="font-size:12px" class="text-dark">
                                    <?= strip_tags($arItem['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE']) ?>
                                    </span>
                                </li>
                            <? endif; ?>
                            <!--<li class="amout"><i class="icon icon-calendar text-secondary mr-1"></i> Выезд каждый четверг </li>-->
                            <!--<li class="amout"><i class="icon icon-info text-secondary mr-1"></i> Автобус из Гомеля, Минска, Могилева ... </li>-->
                        </ul>
                    </div>
                </div>
                <div class="card-hover">
                    <h3 class="h3 text-uppercase">
                        <?= $arItem['NAME'] ?>
                    </h3>
                    <a class="btn btn-light card-hover__view" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        Подробнее
                    </a>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    <div class="col-12 page-section__more text-center">
        <button class="btn btn-secondary btn-load" href="/bus-tours/" type="button"><a class="text-white decoration-none link-main-page" href="/bus-tours/">Все туры<i class="fa fa-spin"></i></a></button>
    </div>
</div>