<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
    <div class="col-12 text-center">
        <div class="section-header">
            <h2 class="h2">Новости</h2>
        </div>
    </div>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="card card-service w-100 mb-2">
                <div class="card-header">
                    <a class="h4" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <?= $arItem['NAME'] ?>
                    </a>
                </div>
                <div class="card-body">
                    <p>
                        <?= substr2(($arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT'] ?? $arItem['DISPLAY_PROPERTIES']['DESC']['~VALUE']['TEXT']), 300) ?>
                    </p>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    <div class="col-12 page-section__more text-center">
        <button class="btn btn-secondary btn-load" type="button"><a class="text-white decoration-none link-main-page" href="/tourists/blog/">Все новости<i class="fa fa-spin"></i></a></button>
    </div>
</div>

