<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="col-12 col-md-8 order-1 order-md-0">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 510, 'height' => 400], NO_PHOTO_PATH_510_400, 1) ?>

        <div class="hotel-package mb-4" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="hotel-package__row row mb-4">
                <div class="col-6 pr-md-1">
                    <a class="hotel-package__img d-block" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <img class="img-fluid" src="<?= $images[0] ?>" alt="<?= $arItem['NAME'] ?>"/>
                    </a>
                </div>
                <div class="col-6">
                    <h4 class="hotel-package__title d-inline-block">
                        <?= $arItem['NAME'] ?>
                    </h4>
                    <ul class="hotel-package__props">
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['AREA_OF_THE_ROOM']['DISPLAY_VALUE'])): ?>
                            <li class="mb-2">
                        <span class="title mr-1">
                            Площадь номера:
                        </span>
                                <span class="fw-bold">
                            <?= $arItem['DISPLAY_PROPERTIES']['AREA_OF_THE_ROOM']['DISPLAY_VALUE'] ?>
                        </span>
                            </li>
                        <? endif; ?>
                    </ul>
                    <p class="mb-2">
                        <?= substr2(($arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT'] ?? $arItem['DISPLAY_PROPERTIES']['DESCRIPTION']['~VALUE']['TEXT']), 180) ?>
                    </p>
                </div>
            </div>
            <hr class="hr-bottom my-0">
        </div>
    <? endforeach; ?>
</div>
