<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('travelsoft.currency');
?>
<?
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/MapAdapter.js");
?>
<div class="category-content__items row pad-lg-20 row-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 252, 'height' => 190], NO_PHOTO_PATH_252_190, 1) ?>

    <div class="col-12 d-flex" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="product bg-white js-grid-item product--list">
            <a class="product__img-top d-block" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                <img class="img-fluid" src="<?=$images[0]?>" alt="<?=$arItem['NAME']?>"/>
                <div class="product__special_box">
					<?/* if(!empty($arItem['DISPLAY_PROPERTIES']['TYPE_ID']['DISPLAY_VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary"><?=$arItem['DISPLAY_PROPERTIES']['TYPE_ID']['DISPLAY_VALUE']?></span>
                            <!--Специальная цена-->
                        </div>
				<?endif;?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['TOURTYPE']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary"><?=strip_tags($arItem['DISPLAY_PROPERTIES']['TOURTYPE']['DISPLAY_VALUE']);?></span>
                        </div>
                    <?endif; */?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['TOUR_TYPE']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary-orange"><?=GetMessage("TOUR_TYPE")?></span>
                        </div>
                    <?endif;?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['NEW']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary"><?=GetMessage("NEW")?></span>
                        </div>
                    <?endif;?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['LEADER']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary-orange"><?=GetMessage("LEADER")?></span>
                        </div>
                    <?endif;?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['OFFER_DAY']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary-orange"><?=GetMessage("OFFER_DAY")?></span>
                        </div>
                    <?endif;?>
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['SPECIAL_OFFER']['VALUE'])):?>
                        <div class="product__special text-white">
                            <span class="bg-primary-orange"><?=GetMessage("SPECIAL_OFFER")?></span>
                        </div>
                    <?endif;?>
                </div>
            </a>
            <div class="product__body">
                <h4 class="product__title">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <?=$arItem['NAME']?>
                    </a>
                </h4>
                <div class="product__rating mb-2">
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])):?>
                        <?=star_rating($arItem['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])?>
                    <?endif;?>
                </div>
                <ul class="product__stat d-flex flex-wrap justify-content-between align-items-center">
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])):?>
                        <li class="mr-3">
                            <span>
                                            <?=$arItem['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE']?>
											<?if(!empty($arItem['DISPLAY_PROPERTIES']['ROUTE']['VALUE'])):?>
												<?= $arItem["DISPLAY_PROPERTIES"]["ROUTE"]["VALUE"] ?>
                                            <?elseif(!empty($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])):?>
                                                ,
                                                <?if(count($arItem['DISPLAY_PROPERTIES']['TOWN']['VALUE']) == 1):?>
                                                    <?=$arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']?>
                                                <?else:?>
                                                    <?=implode2(" - ",$arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'],true)?>
                                                <?endif?>
                                            <?endif;?>
                            </span>
                            <?if($arItem["ROUTE_INFO"]):?>
                                <i class="bullet"></i><a href="#modalMap" data-toggle="modal" data-location='<?=\Bitrix\Main\Web\Json::encode($arItem["ROUTE_INFO"])?>' data-title="<?=$arItem["NAME"]?>"><?=GetMessage('SHOW_ON_THE_MAP')?></a>
                            <?endif?>
                        </li>
                    <?endif;?>
							<? if (!empty($arItem["DISPLAY_PROPERTIES"]["DEPARTURE_TEXT"]["DISPLAY_VALUE"])): ?>
								<li class="mr-3"><?= $arItem["DISPLAY_PROPERTIES"]["DEPARTURE_TEXT"]["DISPLAY_VALUE"] ?></li>
                            <? elseif (!empty($arItem["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"])): ?>
								<li class="mr-3"> 
								<?$i = 1;
								foreach ($arItem["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"] as $date):?>
								<?=CIBlockFormatProperties::DateFormat("d.m", MakeTimeStamp($date, CSite::GetDateFormat()));?>
									<?if($i==6):?>, <small><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('ALL_DATE')?></a></small><?break 1;?><?endif;?>
											<?if($i < count($arItem["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])):?><?echo ", ";?><?endif?>
										<?$i++;?>
								<?endforeach;?>
										</li>
							<?endif;?>
                </ul>
                <hr/>
                <div class="product__desc">
                    <p>
                        <?= substr2($arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT'] ?? $arItem['DISPLAY_PROPERTIES']['HD_DESC']['~VALUE']['TEXT'], 120) ?>
                    </p>
                </div>
            </div>
            <hr class="d-none d-sm-block"/>
            <div class="product__footer">

                    <?if(isset($arParams["MIN_PRICES"][$arItem["ID"]])):?>
                    <a class="product__btn-book btn btn-secondary" href="<?= $arItem['DETAIL_PAGE_URL']?>"><?echo GetMessage('PRICE_FROM') . " " . \travelsoft\currency\factory\Converter::getInstance()->convert($arParams["MIN_PRICES"][$arItem["ID"]]["result_price"], $arParams["MIN_PRICES"][$arItem["ID"]]["currency"])->getResult();?></a>
                    <?else:?>
						<? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])): ?>
							<ul class="product__price d-flex justify-content-between align-items-center">
								<li class="mr-2"><span><?=GetMessage('PRICE_NIGHT')?></span></li>
								<li>
									<span class="cost">
											<?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])->getResult(); ?>
									</span>
								</li>
							</ul>
							<hr/>
						<? endif; ?>
						<? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])): ?>
							<ul class="product__price d-flex justify-content-between align-items-center">
								<? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE_FOR']['VALUE'])):?>
									<li class="mr-2"><span><?=GetMessage('PRICE')?> <?=$arItem['DISPLAY_PROPERTIES']['PRICE_FOR']['VALUE']?></span></li>
								<?endif?>
								<li>
									<span class="cost">
											<?=GetMessage('PRICE_FROM')?> <?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])->getResult(); ?>
									</span>
								</li>
							</ul>
							<hr/>
						<? endif; ?>
                    <a class="product__btn-book btn btn-secondary" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=GetMessage('READ_MORE')?></a>
                    <?endif?>
            </div>
        </div>
    </div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<script>

    $(document).ready(function () {
        var mapAdapter = new MapAdapter({
            map_id: "map",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });

        $('body').on("show.bs.modal","#modalMap", function (e) {

            var t=$(e.relatedTarget),o=t.data("title"),n=$(this);n.find(".modal-title .title").text(o),loc=t.data('location');

            if(typeof loc === "object" && loc.length > 1) {
                mapAdapter.drawRoute(loc);
            } else {
                mapAdapter.addMarker(loc[0]);
            }

        })

    });


</script>
