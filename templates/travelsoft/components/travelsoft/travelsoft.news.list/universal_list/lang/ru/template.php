<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["SHOW_ON_THE_MAP"] = "Показать на карте";
$MESS["READ_MORE"] = "Подробнее";
$MESS["ORDER"] = "Заказать";
$MESS["PRICE"] = "Цена";
$MESS["PRICE_FROM"] = "от ";
$MESS["ALL_DATE"] = "все даты";
$MESS["PRICE_NIGHT"] = "цена за ночь от";
$MESS["READ_MORE_TOUR"] = "Подробнее о туре";
$MESS["SPECIAL_OFFER"] = "Спецпредложение";
$MESS["OFFER_DAY"] = "Предложение дня";
$MESS["LEADER"] = "Лидер продаж";
$MESS["NEW"] = "Новинка";
$MESS["TOUR_TYPE"] = "Горящий тур";
?>