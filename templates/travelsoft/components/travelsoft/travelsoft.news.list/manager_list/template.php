<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["ITEMS"])):?>

    <div class="card d-none d-lg-flex">
        <p class="fw-bold cl1"><?=GetMessage('MANAGER_BLOCK_TITLE')?></p>
        <ul class="guests-room">

            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 38, 'height' => 38], NO_PHOTO_PATH_252_190, 1) ?>

                <?$info = array();?>
                <?if(!empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])):?>
                    <?$info = $arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE']?>
                <?endif?>
                <?if(!empty($arItem['DISPLAY_PROPERTIES']['SKYPE']['VALUE'])):?>
                    <?$info[] = "Skype: ".$arItem['DISPLAY_PROPERTIES']['SKYPE']['VALUE']?>
                <?endif?>
                <?if(!empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])):?>
                    <?$info[] = $arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?>
                <?endif?>

                <li class="guests-room__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <?if(!empty($info)):?>
                        <div class="guests-room__desc">
                            <?=implode('<br>',$info)?>
                        </div>
                    <?endif?>
                    <div class="guests-room__bottom d-flex align-items-center">
                        <?if(!empty($images[0])):?>
                            <div class="guests-room__avatar d-flex align-items-center pointer">
                                <a class="media-object d-block rounded-circle">
                                    <img class="img-fluid rounded-circle" src="<?=$images[0]?>" alt="<?=$arItem["NAME"]?>">
                                </a>
                            </div>
                        <?endif?>
                        <div class="guests-room__right">
                            <h5 class="mb-1 fw-bold cl2"><?=$arItem["NAME"]?></h5>
                            <?if(!empty($arItem['DISPLAY_PROPERTIES']['POSITION']['VALUE'])):?>
                                <div class="d-flex align-items-center">
                                    <div class="country fz-xs"><?=$arItem['DISPLAY_PROPERTIES']['POSITION']['VALUE']?></div>
                                </div>
                            <?endif?>
                        </div>
                    </div>
                </li>

            <?endforeach;?>
        </ul>
    </div>

<?endif?>