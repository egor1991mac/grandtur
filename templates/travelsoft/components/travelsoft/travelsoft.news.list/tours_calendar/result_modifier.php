<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult["PLACES"] = array();
$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>30), false, false, Array("ID", "NAME", "IBLOCK_ID"));
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $arResult["PLACES"][mb_strtolower($arProps["LETTER"]["VALUE"])] = array(
        "name" => $arFields["NAME"],
        "color" => $arProps["COLOR"]["VALUE_XML_ID"]
    );
}

$results["ITEMS"] = array();

foreach ($arResult["ITEMS"] as $key=>$arItem){
    $timestamp = time();
    $curdate = null;
    foreach ($arItem["PROPERTIES"]["DEPARTURE"]["VALUE"] as $k=>$date) {
        $date_timestamp = MakeTimestamp($date);
        $date_place = mb_strtolower($arItem["PROPERTIES"]["DEPARTURE"]["DESCRIPTION"][$k]);
        if ($date_timestamp > $timestamp) {
            $curdate = $date_timestamp;
            $arItem["CURDATE"] = date("d.m.Y", $curdate);
            $arItem["PLACE"] = !empty($date_place) ? $date_place : '';
            $results["ITEMS"][] = $arItem;
        }
    }
}

usort($results["ITEMS"], function ($a, $b) {
    if (MakeTimestamp($a["CURDATE"]) == MakeTimestamp($b["CURDATE"])) {
        return 0;
    }
    return (MakeTimestamp($a["CURDATE"]) < MakeTimestamp($b["CURDATE"])) ? -1 : 1;
});

$result = array();
foreach ($results["ITEMS"] as $arItem) {
    if(!empty($arItem["CURDATE"])) {
        $month = date('m', strtotime($arItem["CURDATE"]));
        $result[$month][] = $arItem;
    }
}

/*
$today = time();
$mnth[date('m',$today)] = [];
for ($i = 1;$i<=12;$i++){
    $mnth[add_month($today,$i,false)] = [];
}
$result = array();
foreach ($arResult["ITEMS"] as $arItem) {
    if(!empty($arItem["CURDATE"])) {
        foreach ($arItem["CURDATE"] as $d) {
            $month = date('m', strtotime($d));

            $mnth[$month][$arItem["ID"]] = $arItem;
        }
    }
}
foreach ($mnth as $m=>$armnth){
    if(!empty($mnth[$m])){
        $result[$m] = $armnth;
    }
}*/

$arResult["ITEMS"] = $result;