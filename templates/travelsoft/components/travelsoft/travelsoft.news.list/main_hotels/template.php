<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('travelsoft.currency');
?>
<div class="intro__hotels">
    <div class="container-fluid p-0">
        <div class="swiper-container js-intro-hotels">
            <div class="swiper-wrapper">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <?$images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 377, 'height' => 262], NO_PHOTO_PATH_377_262, 1)?>
    <div class="swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="card-intro d-block" style="background-image: url(<?=$images[0]?>);">
            <? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])): ?>
                <div class="card-price">
                    <span class="mr-1">от</span>
                    <span class="count text-secondary">
                        <?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])->getResult(); ?>
                    </span>
                </div>
            <? endif; ?>
            <div class="card-intro__footer">
                <h4 class="h4 f-primary">
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"])):?>
                        <?=strip_tags($arItem["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"])?>
                    <?else:?>
                        <?=$arItem['NAME']?>
                    <?endif?>
                </h4>
                <div class="card-intro__local d-flex align-items-center">
                    <ul class="flex-wrap">
                        <?if(!empty($arItem['DISPLAY_PROPERTIES']['TOWN']['VALUE'])):?>
                            <li class="mr-4"><i class="icon icon-label mr-1"></i>
                                <?=strip_tags($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])?>
                            </li>
                        <?endif;?>
                        <?if(!empty($arItem['DATES'])):?>
                            <li class="amout">
                                <i class="icon icon-calendar mr-1"></i>
                                Выезд <?= implode(', ', $arItem['DATES'])?> ...
                            </li>
                        <?endif;?>
                        <!--<li class="amout"><i class="icon icon-info  mr-1"></i> Автобус из Минска, Витебска, Гомеля ...-->
                        <!--</li>-->
                    </ul>
                </div>
            </div>
            <div class="card-hover">
                <h3 class="h3 text-uppercase">
                    <?=$arItem['NAME']?>
                </h3>
                <a class="btn btn-light btn--round card-hover__view" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                    Подробнее
                </a>
            </div>
        </div>
    </div>
<?endforeach;?>
            </div>
        </div>
        <div class="intro__hotels-controls">
            <button class="btn btn-primary btn-nav btn-nav--left js-prev" type="button"><i class="fa fa-angle-left"></i></button>
            <button class="btn btn-primary btn-nav btn-nav--right js-next" type="button"><i class="fa fa-angle-right"></i></button>
        </div>
    </div>
</div>