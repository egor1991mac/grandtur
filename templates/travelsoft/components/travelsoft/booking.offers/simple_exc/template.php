
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$randString = randString(7);

$this->addExternalCss("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.css");
$this->addExternalJs("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.min.js");
/*$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/1.index_bundle.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/2.index_bundle.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/3.index_bundle.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/4.index_bundle.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/5.index_bundle.js");*/
?>
<div id="result_form">
</div>


    <script>



        let config = {

            ajaxUrl: '<?= $componentPath . "/ajax/search_offers.php" ?>',
            body:{
                travelbooking: '<?= json_encode($arResult["REQUEST"]) ?>',
                service_type: '<?= $arResult["SERVICE_TYPE"] ?>',
                sessid : BX.bitrix_sessid(),
            }
        }
        localStorage.clear();
        localStorage.setItem('config',JSON.stringify(config));


        //http://host2011.vh114.hosterby.com/bus-tours/g-odessa-ozdorovitelnyy-kompleks-pao-odeskabel/?s2o=Y&travelbooking%5Bdate_from%5D=01.07.2019&travelbooking%5Bdate_to%5D=09.07.2019&travelbooking%5Bcity_id%5D=&travelbooking%5Badults%5D=2&travelbooking%5Bchildren%5D=3&travelbooking%5Bchildren_age%5D%5B%5D=3&travelbooking%5Bchildren_age%5D%5B%5D=6&travelbooking%5Bchildren_age%5D%5B%5D=3

        //var data = localStorage.getItem('config');
        //console.log(JSON.parse(data).body);
       /* class API{

            constructor(){
                this.url = '';
                this.data = [];
            }

            _createURL(string){
                let { ajaxUrl, body, sessid } = JSON.parse(string);
                body.travelbooking = JSON.parse(body.travelbooking);
                this.url = `${ajaxUrl}/?${$.param(body)}`;
                return this.url;

        };

        _fetchData(url){
            fetch(url)
                .then(response => response.json())
                .then(response =>this._groupeData(response.data))
                .then(response => this.data = response);
            return this.data;
        };

        _groupeData(data){
            if(data.length != 0  || Object.keys(data).length !=0){

                let newData = {};

                data.forEach(item=>{
                    if(item.CITY == null || item.CITY == undefined){
                        item.CITY.NAME = "CITY";
                    }
                });

                let date = this._uniqData(data.map(item => item.DATE_FROM));
                let city = this._uniqData(data.map(item => item.CITY.NAME));
                console.log(city);
                date.forEach(DATE => {
                    newData[DATE] = {};
                    city.forEach(CITY =>{
                        newData[DATE][CITY] = data.filter(item => item.CITY.NAME  == CITY);
                    })
                })



            }

        };

        _uniqData(data){
          return data.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
            };
        }

        const api = new API();

        api._createURL(localStorage.getItem('config'));
        api._fetchData(api.url);
        */


    </script>
    <?//$this->addExternalJs(SITE_TEMPLATE_PATH . "/front/dist/index_bundle.js") ?>
<script>
<? if ($_REQUEST["s2o"] === "Y"): ?>
     //   Travelsoft.scrolltoInit(document.querySelector(".offer-box"), 200);
<? endif ?>
   // Travelsoft.add2cartInit(<? if ($arResult["IS_AVAIL_BOOKING"]): ?>true<? else: ?>false<? endif; ?>, "<?= GetMessage("TRAVELBOOKING_NOT_AVAIL_BOOKING") ?>");
</script>

