/**
 * Component booking.offers
 * Template "d3p.simple"
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */

if (typeof Travelsoft !== "object") {
    Travelsoft = {};
}

if (typeof Travelsoft.loadOffers === "undefined") {

    Travelsoft.loadOffers = function (parameters) {

        var request = JSON.parse(parameters.request);

        request.service_type = parameters.service_type;

        request.sessid = BX.bitrix_sessid();

        $.get(
                parameters.ajaxUrl, request,
                function (resp) {
                    
                    var template_offers_item = $(parameters.offers_item_template_id_selector).html();
                    var template_modal_box = $(parameters.modal_box_template_id_selector).html();
                    var template_pictures_box = $(parameters.pictures_box_template_id_selector).html();
                    var template_picture_item = $(parameters.picture_item_template_id_selector).html();
                    var template_desctiption_box = $(parameters.description_box_template_id_selector).html();
                    var template_preview_image_box = $(parameters.preview_image_box_template_id_selector).html();
                    var modal_box = "";
                    var pictures_items = "";
                    var pictures_box = "";
                    var description_box = "";
                    var rand_string = "";
                    var preview_image_box = "";
                    $(parameters.insertion_selector).html("");
                    if (!resp.error && $.isArray(resp.data) && resp.data.length) {
                        if (resp.nearest_available && parameters.show_nearest_available_text) {
                            $(parameters.insertion_selector).append(
                                    $(parameters.nearest_available_text_template_id_selector).html());
                        }
                        for (var i = 0; i < resp.data.length; i++) {
                            rand_string = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                            if (resp.data[i].PICTURES.length > 0 || resp.data[i].DESCRIPTION.length > 0) {
                                if (resp.data[i].PICTURES.length > 0) {
                                    pictures_items = "";
                                    for (var k = 0; k < resp.data[i].PICTURES.length; k++) {
                                        pictures_items += template_picture_item
                                                .replace("{{offer_picture_path}}", resp.data[i].PICTURES[k])
                                                .replace("{{offer_name}}", resp.data[i].NAME);
                                    }

                                    pictures_box = template_pictures_box
                                            .replace("{{offer_pictures_items}}", pictures_items)
                                            .replace("{{rand_string}}", rand_string);
                                }

                                if (resp.data[i].DESCRIPTION && resp.data[i].DESCRIPTION.length > 0) {
                                    description_box = template_desctiption_box.replace("{{offer_description}}", resp.data[i].DESCRIPTION);
                                }
                                modal_box = template_modal_box
                                        .replace("{{rand_string}}", rand_string)
                                        .replace("{{rand_string}}", rand_string)
                                        .replace("{{rand_string}}", rand_string)
                                        .replace("{{offer_name}}", resp.data[i].NAME)
                                        .replace("{{offer_pictures_box}}", pictures_box)
                                        .replace("{{offer_description_box}}", description_box);
                            }

                            if (resp.data[i].PICTURE_PATH && resp.data[i].PICTURE_PATH.length > 0) {
                                preview_image_box = template_preview_image_box
                                        .replace("{{offer_image}}", resp.data[i].PICTURE_PATH)
                                        .replace("{{offer_name}}", resp.data[i].NAME);
                            } else {
                                preview_image_box = template_preview_image_box
                                    .replace("{{offer_image}}", '/local/templates/travelsoft/img/no_photo/252_190.jpg')
                                    .replace("{{offer_name}}", resp.data[i].NAME);
                            }

                            $(parameters.insertion_selector).append(
                                    template_offers_item
                                    .replace("{{rand_string}}", rand_string)
                                    .replace("{{offer_id}}", resp.data[i].ID)
                                    .replace("{{offer_preview_image}}", preview_image_box)
                                    .replace("{{offer_name}}", resp.data[i].NAME)
                                    .replace("{{offer_price}}", resp.data[i].FORMATTED_PRICE)
                                    .replace("{{offer_price_usd}}", resp.data[i].FORMATTED_PRICE_USD)
                                    .replace("{{offer_nights}}", resp.data[i].NIGHTS)
                                    .replace("{{offer_food}}", resp.data[i].FOOD && resp.data[i].FOOD.NAME ? resp.data[i].FOOD.NAME : parameters.messages.no_food)
                                    .replace("{{offer_date}}", resp.data[i].DATE_FROM ? resp.data[i].DATE_FROM : "")
                                    .replace("{{offer_date_from_transfer}}", resp.data[i].DATE_FROM_TRANSFER ? resp.data[i].DATE_FROM_TRANSFER : "")
                                    .replace("{{offer_date_back_transfer}}", resp.data[i].DATE_BACK_TRANSFER ? resp.data[i].DATE_BACK_TRANSFER : "")
                                    .replace("{{offer_date_from_placement}}", resp.data[i].DATE_FROM_PLACEMENT ? resp.data[i].DATE_FROM_PLACEMENT : "")
                                    .replace("{{offer_date_to_placement}}", resp.data[i].DATE_TO_PLACEMENT ? resp.data[i].DATE_TO_PLACEMENT : "")
                                    .replace("{{offer_add_to_basket}}", resp.data[i].ADD2BASKET)
                                    .replace("{{offer_modal_box}}", modal_box)
                                    );
                            Travelsoft.setModalShowHandler();
                        }

                    } else {

                        $(parameters.insertion_selector).html(parameters.messages.not_found);
                    }

                }
        );

    };
}

if (typeof Travelsoft.add2cartInit === "undefined") {
    Travelsoft.add2cartAlreadyInit = false;
    Travelsoft.add2cartInit = function (isAvailBooking, message) {
        if (!Travelsoft.add2cartAlreadyInit) {
            $(document).on("click", ".add2cart", function (e) {
                if (!isAvailBooking) {
                    alert(message);
                    e.preventDefault();
                } else {
                    $(this).addClass("hidden").prev("span.loading").removeClass("hidden");
                }
            });
            Travelsoft.add2cartAlreadyInit = true;
        }
    };
}

if (typeof Travelsoft.scrolltoInit === "undefined") {
    Travelsoft.scrollto = function (element, delta) {
        delta = delta || 0;
        $(document).ready(function () {
            $('html, body').animate({scrollTop: $(element).offset().top - delta}, 500);
        });
    };
    Travelsoft.scrolltoAlreadyInit = false;
    Travelsoft.scrolltoInit = function (element, delta) {
        if (!Travelsoft.scrolltoAlreadyInit) {
            Travelsoft.scrollto(element, delta);
            Travelsoft.scrolltoAlreadyInit = true;
        }
    };
}

if (typeof Travelsoft.setModalShowHandler === "undefined") {
    Travelsoft.modalShowHandlerAlreadyInit = false;
    Travelsoft.setModalShowHandler = function () {

        $(document).on("click", ".offers-name-box", function () {

            var $this = $(this);
            var modal = $($this.data("target"));
            if (modal.length) {
                if (modal.data("modal-is-inited") !== "true") {
                    modal.modal();
                    modal.data("modal-is-inited", "true");
                    modal.one("shown.bs.modal", function () {
                        modal.find(".rslides").responsiveSlides({
                            auto: false,
                            pager: false,
                            nav: true,
                            speed: 500
                        });
                    });
                } else {
                    modal.modal("show");
                }
            }
        });
        Travelsoft.modalShowHandlerAlreadyInit = true;
    };
}