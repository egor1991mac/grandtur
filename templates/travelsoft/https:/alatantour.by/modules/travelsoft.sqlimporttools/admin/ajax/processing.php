<?php

@set_time_limit(0);
@ini_set('session.gc_maxlifetime', 2400);

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("BX_SECURITY_SHOW_MESSAGE", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

\Bitrix\Main\Loader::includeModule("travelsoft.sqlimporttools");

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$controller = new \travelsoft\sqlimporttools\ajax\Controller($request);

$controller->dispatch();