<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с инфоблоком стран
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Countries extends Iblock
{
    protected static $storeName = 'countries';

    public function getElementLink(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y"),"select" => array("ID", "DETAIL_PAGE_URL")));
        return (string) $arData[$id]["DETAIL_PAGE_URL"];

    }

}