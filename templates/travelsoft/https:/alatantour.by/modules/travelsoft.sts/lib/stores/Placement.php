<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с таблицей вариантов размещения
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Placement extends Iblock
{
    protected static $storeName = 'placement';

}