<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с инфоблоком звездности отелей
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Stars extends Iblock
{
    protected static $storeName = 'stars';

    public function getSubordinateElements(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y"), "select" => array("ID", "PROPERTY_SUBORDINATE_ELEMENTS")));

        $value = array();
        foreach ($arData as $star) {
            $value[] = $star["PROPERTY_SUBORDINATE_ELEMENTS_VALUE"];
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

    public function getSubordinateElementsNew(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y")));

        $value = array();
        foreach ($arData as $star) {
            if(!empty($star["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"])) {
                $value = array_merge($value,$star["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"]);
            }
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

}