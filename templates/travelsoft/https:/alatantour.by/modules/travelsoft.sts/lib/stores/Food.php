<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с инфоблоком питания
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Food extends Iblock
{
    protected static $storeName = 'food';

    public function getElementDesc(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id)));
        if(!empty($arData[$id]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"])) {
            return (string) $arData[$id]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"];
        }
        else {
            return '';
        }


    }

    public function getElementPropName(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id)));
        if(!empty($arData[$id]["PROPERTIES"]["NAME"]["VALUE"])) {
            return (string) $arData[$id]["PROPERTIES"]["NAME"]["VALUE"];
        }
        else {
            return '';
        }


    }

    public function getSubordinateElements(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y"), "select" => array("ID", "PROPERTY_SUBORDINATE_ELEMENTS")));

        $value = array();
        foreach ($arData as $star) {
            $value[] = $star["PROPERTY_SUBORDINATE_ELEMENTS_VALUE"];
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

    public function getSubordinateElementsNew(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y")));

        $value = array();
        foreach ($arData as $food) {
            if(!empty($food["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"])) {
                $value = array_merge($value,$food["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"]);
            }
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

}