<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей направлений
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationDirectionsResorts extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationdirectionsresorts';

    public function getResortsToDirect(int $cityFrom, int $country): array {

        $arData_ = array();
        $arData = self::get(array("filter" => array("UF_CITYFROM_BX_ID" => $cityFrom, "UF_COUNTRY_BX_ID" => $country), "select" => array("ID","UF_CITY_BX_ID")));

        foreach ($arData as $direct) {
            if(!empty($direct["UF_CITY_BX_ID"])) {
                $arData_ = array_merge($arData_,$direct["UF_CITY_BX_ID"]);
            }
        }

        return (array)$arData_;

    }

}