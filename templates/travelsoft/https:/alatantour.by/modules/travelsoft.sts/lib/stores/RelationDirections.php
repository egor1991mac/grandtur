<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей направлений
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationDirections extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationdirections';
}