<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей типов категорий номеров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RoomCatTypes extends Highloadblock
{
    protected static $storeName = 'roomcattypes';

    public static function getElementName(array $query): string {

        $arData = current(self::get(array("filter" => $query,"select" => array("UF_VALUE"))));
        return (string)$arData["UF_VALUE"];

    }

    public static function getElementNameMod(array $query, int $operator_id): array {

        $arData = self::get(array("filter" => $query,"select" => array("UF_VALUE", "UF_OP_".$operator_id, "ID")));

        $arData_ = array();
        foreach ($arData as $data){
            $arData_[$data["UF_OP_".$operator_id]] = $data["UF_VALUE"];
        }

        return (array)$arData_;

    }

}