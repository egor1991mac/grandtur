<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с ифноблоком типов туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class TourTypes extends Iblock
{
    protected static $storeName = 'tourtypes';

    protected function _getTourTypesName(array $query): string {

        return TourTypes::getElementName(
            $this->getBxId(current(RelationTourTypes::get($query))));

    }

    public function getSubordinateElements(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y"), "select" => array("ID", "PROPERTY_SUBORDINATE_ELEMENTS")));

        $value = array();
        foreach ($arData as $star) {
            $value[] = $star["PROPERTY_SUBORDINATE_ELEMENTS_VALUE"];
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

    public function getSubordinateElementsNew(array $id): array {

        $arData_ = $id;
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y")));

        $value = array();
        foreach ($arData as $tourtype) {
            if(!empty($tourtype["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"])) {
                $value = array_merge($value,$tourtype["PROPERTIES"]["SUBORDINATE_ELEMENTS"]["VALUE"]);
            }
        }

        if(!empty($value)){
            $arData_ = array_merge($arData_,$value);
        }

        return (array)$arData_;

    }

}