<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей стран
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationCountries extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationcountries';

    public static function getBxIdArray(array $query): array {

        $arData = self::get(array("filter" => $query,"select" => array("ID","UF_BX_ID")));

        $arData_ = array();
        foreach ($arData as $data){
            $arData_[$data["ID"]] = $data["UF_BX_ID"];
        }

        return (array)$arData_;


    }

}