<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с таблицей курортов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Resorts extends Iblock
{
    protected static $storeName = 'resorts';
}