<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\_interfaces\Operator;
use travelsoft\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей операторов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Operators extends Highloadblock
{
    protected static $storeName = 'operators';

    public static function getElementName(array $query): string {

        $arData = current(self::get(array("filter" => $query,"select" => array("UF_NAME"))));
        return (string)$arData["UF_NAME"];

    }

}