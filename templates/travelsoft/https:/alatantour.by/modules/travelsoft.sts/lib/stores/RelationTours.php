<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationTours extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationtours';
}