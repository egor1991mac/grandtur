<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей курортов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationResorts extends Highloadblock
{
    protected static $storeName = 'relationresorts';
}