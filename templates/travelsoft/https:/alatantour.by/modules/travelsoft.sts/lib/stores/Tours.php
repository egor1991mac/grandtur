<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с инфоблоком предложений
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Tours extends Iblock
{
    protected static $storeName = 'tours';

    public static function getTours($arMtKey){

        $tourCallback = function ($arFields, &$arProperties){
            $arProperties['HOTEL_DESCRIPTION'] = $arProperties['HOTEL_DESCRIPTION']['~VALUE']['TEXT'];
            $arProperties['DAYS'] = $arProperties['DAYS']['~VALUE'];
        };

        $tours = self::get(array("filter" => array("ACTIVE" => "Y", "PROPERTY_MT_KEY" => $arMtKey), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_PREVIEW_TEXT")), true, $tourCallback);

        $resultTours = [];
        foreach ($tours as $key => $tour){
            if(!empty($tour['PROPERTIES']['MT_KEY']['VALUE']))
            $resultTours[$tour['PROPERTIES']['MT_KEY']['VALUE']] = $tour;
        }

        return $resultTours;
    }
}