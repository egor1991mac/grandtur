<?php

namespace travelsoft\sts;

use travelsoft\sts\stores\Operators;
use travelsoft\sts\stores\RelationCountries;
use travelsoft\sts\Utils;

/**
 * Класс прокси
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class ClientServerProxy
{

    protected $_operator = null;

    protected $_request = null;

    public function __construct(array $request) {

        $this->_request = $request;

        $arOperator = Operators::getById($this->_request['operator_id']);
        if (class_exists($arOperator['UF_CLASS'])) {
            if(!isset($request["getDates"])) {
                $cities = null;
                if (!empty($this->_request["cities"]) && !empty($this->_request["cities"][0])) {
                    $cities = current($this->_request["cities"]);
                }
                if (\travelsoft\sts\Utils::getDirectionOperator($arOperator["UF_CODE"], $this->_request["country"], $this->_request["cityFrom"], $cities)) {
                    $class = $arOperator['UF_CLASS'];
                    $this->_request["address"] = $arOperator['UF_GATEWAY'];
                    $this->_operator = new $class($this->_request);
                } else {
                    throw new \Exception("Нет направлений по оператору.");
                }
            } else {
                $class = $arOperator['UF_CLASS'];
                $this->_request["address"] = $arOperator['UF_GATEWAY'];
                $this->_operator = new $class($this->_request);
            }
        } else {
            throw new \Exception("Такой оператор не обслуживается.");
        }

    }

    //написать функцию отдачи ответа getResult ?

    public function run() : array {

        //TODO кэшировать ответ оператора по запросу
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_id = "main_search_result_sts_items_for_request_" . md5(serialize($this->_request));
        $cache_dir = "/travelsoft/main_search_result_sts_items_for_request_" . $this->_request['operator_id'];

        if ($cache->initCache(3600, $cache_id, $cache_dir)) {
            return $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $result = $this->_operator->sendRequest()->getResult();

            if (!empty($result)) {
                $cache->endDataCache($result);
            } else {
                $cache->abortDataCache();
            }

            return $result;

        }

    }

    public function getDatesResult() {

        return $this->_operator->getDates();

    }

}