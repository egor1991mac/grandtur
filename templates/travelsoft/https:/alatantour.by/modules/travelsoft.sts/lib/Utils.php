<?php

namespace travelsoft\sts;

use travelsoft\sts\Settings;

/**
 * Класс утилит
 * 
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Utils {

    /**
     * Возвращает результат конвертации массива в строку
     * @param array $arFields
     * @return string
     */
    public static function ats(array $arFields): string {
        return base64_encode(gzcompress(serialize($arFields), 9));
    }

    /**
     * Возвращает результат конвертации строки в массив
     * @param string $str
     * @return array
     */
    public static function sta(string $str): array {
        return (array) unserialize(gzuncompress(base64_decode($str)));
    }

    /**
     * @global CMain $APPLICATION
     * @global CUser $USER
     * @var array $arParams
     * @var array $arResult
     * @var CatalogSectionComponent $component
     * @var CBitrixComponentTemplate $this
     * @var string $templateName
     * @var string $componentPath
     */
    public static function _er(string $code, bool $div = false, string $message = '') {

        $text = strlen($message) > 0 ? $message : GetMessage($code);

        if ($div) {
            echo '<div class="error">' . $text . '</div>';
        } else {
            echo '<span class="error">' . $text . '</span>';
        }
    }

    /**
     * Обертка метода self::_er()
     * @param array $arErrors
     * @param string $code
     * @param bool $div
     * @param string $message
     */
    public static function showError(array $arErrors = array(), string $code, bool $div = false, string $message = '') {

        if (in_array($code, $arErrors)) {
            Utils::_er($code, $div);
        }
    }

    /**
     * Склеивает элементы массива в строку
     * @param array $array
     * @return string
     */
    public static function gluingAnArray(array $array, string $delemiter = ' '): string {

        return implode($delemiter, array_filter($array, function ($item) {
            return strlen($item) > 0;
        }));
    }

    /**
     * Возвращает объект даты
     * @param string $date
     * @return \Bitrix\Main\Type\DateTime
     */
    public static function create (string $date) {

        $date = new \Bitrix\Main\Type\DateTime($date);
        return $date->format('d.m.Y');
    }

    /**
     * Возвращает объект даты в периоде
     * @param string $date
     * @return \Bitrix\Main\Type\DateTime
     */
    public static function dateModify (string $date, string $period) {

        $date = new \Bitrix\Main\Type\DateTime($date);
        $date = $date->add($period);
        return $date->format('d.m.Y');
    }

    /**
     * Возвращает объект даты по временной метке unix
     * @param int $timestamp
     * @return \Bitrix\Main\Type\DateTime
     */
    public static function createFromTimetamp (int $timestamp) {

        return \Bitrix\Main\Type\DateTime::createFromTimestamp($timestamp);
    }

    /**
     * Возвращает строку, содержащую JSON представление переменной
     * @param array $data
     * @return string
     */
    public static function jsonEncode (array $data) {

        return \Bitrix\Main\Web\Json::encode($data);
    }

    /**
     * Возвращает декодированую строку JSON
     * @param string $data
     * @return array
     */
    public static function jsonDecode (string $data) {

        return \Bitrix\Main\Web\Json::decode($data);
    }

    /**
     * Возвращает путь к изображению
     * @param int $data
     * @return string
     */
    public static function getPath (int $data, array $params) {

        $file = '';

        if(!empty($params) && !empty($params["width"]) && !empty($params["height"])){

            $file = \CFile::ResizeImageGet($data, $params, BX_RESIZE_IMAGE_EXACT, true);
            return $file["src"];

        }

        if(!$file)
            return Settings::pathNoPhoto();

    }

    /**
     * Список справочников
     * @param int $id
     * @return string
     */
    public static function tablesList(int $id = null): string {

        /* список справочников */
        $arListHL = array(
            Settings::relationcountriesStoreId() => "Страны",
            Settings::relationcitiesStoreId() => "Города / Курорты",
            Settings::relationhotelsStoreId() => "Отели",
            Settings::relationstarsStoreId() => "Звездность",
            Settings::relationfoodStoreId() => "Питание",
            Settings::relationtourtypesStoreId() => "Типы туров",
            Settings::relationplacementStoreId() => "Размещение",
        );

        $hl_selected_null = $id == null ? 'selected' : '';
        $html = '<select name="ENTITY_ID" id="listHL" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">';
        $html .= '<option value '.$hl_selected_null.'>...</option>';

        foreach ($arListHL as $key=>$item) {

            $hl_selected = '';
            if(!empty($id) && $id == $key){
                $hl_selected = 'selected';
            }
            $html .= '<option '.$hl_selected.' value="'.$key.'">'.$item.'</option>';

        }

        $html .= '</select>';

        return $html;
    }

    /**
     * Соотношения ИБ
     * @return array
     */
    public static function correctListIB(): array {

        /* список инфоблоков */
        $arListIB = array(
            Settings::relationcitiesStoreId() => \travelsoft\sts\stores\Cities::get(array("select"=>array("ID","NAME"))),
            Settings::relationcountriesStoreId() => \travelsoft\sts\stores\Countries::get(array("select"=>array("ID","NAME"))),
            Settings::relationhotelsStoreId() => \travelsoft\sts\stores\Hotels::get(array("select"=>array("ID","NAME"))),
            Settings::relationfoodStoreId() => \travelsoft\sts\stores\Food::get(array("select"=>array("ID","NAME"))),
            Settings::relationstarsStoreId() => \travelsoft\sts\stores\Stars::get(array("select"=>array("ID","NAME"))),
            Settings::relationtourtypesStoreId() => \travelsoft\sts\stores\TourTypes::get(array("select"=>array("ID","NAME"))),
            Settings::relationplacementStoreId() => \travelsoft\sts\stores\Placement::get(array("select"=>array("ID","NAME")))
        );

        return $arListIB;
    }

    /**
     * Соотношения HL
     * @return array
     */
    public static function correctListHL(): array {

        /* список таблиц */
        $arListHL = array(
            Settings::relationcountriesStoreId() => \travelsoft\sts\stores\RelationCountries::get(),
            Settings::relationcitiesStoreId() => \travelsoft\sts\stores\RelationCities::get(),
            Settings::relationhotelsStoreId() => \travelsoft\sts\stores\RelationHotels::get(),
            Settings::relationfoodStoreId() => \travelsoft\sts\stores\RelationFood::get(),
            Settings::relationstarsStoreId() => \travelsoft\sts\stores\RelationStars::get(),
            Settings::relationtourtypesStoreId() => \travelsoft\sts\stores\RelationTourTypes::get(),
            Settings::relationplacementStoreId() => \travelsoft\sts\stores\RelationPlacement::get()
        );

        return $arListHL;
    }

    /**
     * Классы таблиц
     * @return array
     */
    public static function tablesHL(int $tableId): string {

        /* список таблиц */
        $arTablesHL = array(
            Settings::relationcitiesStoreId() => '\travelsoft\sts\stores\RelationCities',
            Settings::relationcountriesStoreId() => '\travelsoft\sts\stores\RelationCountries',
            Settings::relationhotelsStoreId() => '\travelsoft\sts\stores\RelationHotels',
            Settings::relationfoodStoreId() => '\travelsoft\sts\stores\RelationFood',
            Settings::relationstarsStoreId() => '\travelsoft\sts\stores\RelationStars',
            Settings::relationtourtypesStoreId() => '\travelsoft\sts\stores\RelationTourTypes',
            Settings::relationplacementStoreId() => '\travelsoft\sts\stores\RelationPlacement'
        );

        return $arTablesHL[$tableId];
    }

    /**
     * Классы ИБ
     * @return array
     */
    public static function tablesIB(int $tableId): string {

        /* список инфоблоков */
        $arTablesIB = array(
            Settings::relationcitiesStoreId() => '\travelsoft\sts\stores\Cities',
            Settings::relationcountriesStoreId() => '\travelsoft\sts\stores\Countries',
            Settings::relationhotelsStoreId() => '\travelsoft\sts\stores\Hotels',
            Settings::relationfoodStoreId() => '\travelsoft\sts\stores\Food',
            Settings::relationstarsStoreId() => '\travelsoft\sts\stores\Stars',
            Settings::relationtourtypesStoreId() => '\travelsoft\sts\stores\TourTypes',
            Settings::relationplacementStoreId() => '\travelsoft\sts\stores\Placement'
        );

        return $arTablesIB[$tableId];
    }

    /**
     * Код таблиц
     * @return array
     */
    public static function codeTablesHL(int $tableId): string {

        /* список таблиц */
        $arTablesHL = array(
            Settings::relationcitiesStoreId() => 'CITIES_HL',
            Settings::relationcountriesStoreId() => 'COUNTRIES_HL',
            Settings::relationhotelsStoreId() => 'HOTELS_HL',
            Settings::relationfoodStoreId() => 'FOOD_HL',
            Settings::relationstarsStoreId() => 'STARS_HL',
            Settings::relationtourtypesStoreId() => 'TOURTYPES_HL',
            Settings::relationplacementStoreId() => 'PLACEMENT_HL',
        );

        return $arTablesHL[$tableId];
    }

    public static function getOperators_ ($filter = array()) {

        $filter_ = array("UF_ACTIVE"=>1);
        $filter = !empty($filter) ? $filter : array();
        if(!empty($filter)){
            $filter_ = array_merge($filter_, $filter);
        }
        $result = \travelsoft\sts\stores\Operators::get(array("filter"=>$filter_));
        return $result;

    }

    /**
     * Является ли запрос запросом от формы редактирования
     * @return bool
     */
    public static function isEditFormRequest(): bool {

        return $_SERVER['REQUEST_METHOD'] === 'POST' && check_bitrix_sessid() && ($_POST['SAVE'] || $_POST['APPLY']);
    }

    /**
     * HTML поля редактирования
     * @param string $label
     * @param string $field
     * @param bool $required
     * @param bool $hide
     * @return string
     */
    public static function getEditFieldHtml(string $label, string $field, bool $required = false, bool $hide = false): string {

        if ($required) {
            $label .= '<span class="required">*</span>';
        }

        $content = '<tr ' . ($hide ? 'style="display:none"' : "") . '>';
        $content .= '<td width="40%">' . $label . '</td>';
        $content .= '<td width="60%">' . $field . '</td>';
        $content .= '</tr>';

        return $content;
    }

    /*
     * Проверка актуализации направления по оператору
     * @param string $code
     * @param int $country_id
     * @param int $cityFrom_id
     * @param int $city_id
     */
    public static function getDirectionOperator(string $code, int $country_id, int $cityFrom_id, int $city_id = null): bool {

        //$city_id = $city_id || null;

        //dm(array($code,$country_id,$cityFrom_id,$city_id));

        if($city_id){

            $city = current(\travelsoft\sts\stores\RelationCities::get(array("filter"=>array("UF_BX_ID" => $city_id),"select"=>array("ID"))));
            if(!empty($city["ID"])){
                $row_id = current(\travelsoft\sts\stores\RelationDirectionsResorts::get(array("filter"=>array("UF_CITYFROM_BX_ID"=>$cityFrom_id, "UF_COUNTRY_BX_ID"=>$country_id, $code=>$city["ID"]),"select"=>array("ID"))));
            }

        } else {

            $country = current(\travelsoft\sts\stores\RelationCountries::get(array("filter"=>array("UF_BX_ID" => $country_id),"select"=>array("ID"))));
            if($country){
                $row_id = current(\travelsoft\sts\stores\RelationDirections::get(array("filter"=>array("UF_CITYFROM_BX_ID"=>$cityFrom_id, $code=>$country),"select"=>array("ID"))));
            }

        }

        return isset($row_id) && !empty($row_id) ? true : false;

    }

    /*
     * Направления по оператору
     * @param int $operator_id
     */
    public static function getArrayDirectionOperator(int $operator_id): array {

        $direction = array();
        $values = \travelsoft\sts\stores\RelationDirections::get(array("filter"=>array("!UF_OP_".$operator_id=>false),"select"=>array("ID","UF_CITYFROM_BX_ID","UF_CITYFROM_ID","UF_OP_".$operator_id)));
        $value_country_ids = array();
        $value_city_ids = array();
        foreach ($values as $val){
            $value_country_ids = array_merge($value_country_ids, $val["UF_OP_".$operator_id]);
            $value_city_ids[] = $val["UF_CITYFROM_ID"];
        }
        $value_country_ids = array_unique($value_country_ids);
        $value_city_ids = array_unique($value_city_ids);

        if(!empty($value_country_ids)){
            $country = \travelsoft\sts\stores\RelationCountries::get(array("filter"=>array("ID" => $value_country_ids),"select"=>array("ID","UF_OP_".$operator_id)));
        }
        if(!empty($value_city_ids)){
            $city = \travelsoft\sts\stores\RelationCities::get(array("filter"=>array("ID" => $value_city_ids),"select"=>array("ID","UF_OP_".$operator_id)));
        }

        if(!empty($country) && !empty($city)){
            foreach ($values as $key=>$value) {
                if(isset($city[$value["UF_CITYFROM_ID"]]["UF_OP_".$operator_id]) && !empty($city[$value["UF_CITYFROM_ID"]]["UF_OP_".$operator_id])) {
                    if(!isset($direction[$city[$value["UF_CITYFROM_ID"]]["UF_OP_".$operator_id]])){
                        $direction[$city[$value["UF_CITYFROM_ID"]]["UF_OP_".$operator_id]] = array();
                    }
                    foreach ($value["UF_OP_".$operator_id] as $val){
                        if(isset($country[$val]["UF_OP_".$operator_id]) && !empty($country[$val]["UF_OP_".$operator_id])){
                            $direction[$city[$value["UF_CITYFROM_ID"]]["UF_OP_".$operator_id]][] = $country[$val]["UF_OP_".$operator_id];
                        }
                    }
                }
            }
        }

        return $direction;

    }

}
