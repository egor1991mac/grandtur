<?php

namespace travelsoft\sts\operators;

use travelsoft\currency\Currency;
use \travelsoft\sts\_interfaces\Operator;
use travelsoft\sts\Settings;
use travelsoft\sts\stores\RelationPlacement;
use \travelsoft\sts\Utils;
use \travelsoft\sts\stores\Food;
use \travelsoft\sts\stores\RelationFood;
use \travelsoft\sts\stores\Stars;
use \travelsoft\sts\stores\RelationStars;
use \travelsoft\sts\stores\Cities;
use \travelsoft\sts\stores\Regions;
use \travelsoft\sts\stores\RelationCities;
use \travelsoft\sts\stores\Countries;
use \travelsoft\sts\stores\RelationCountries;
use \travelsoft\sts\stores\Hotels;
use \travelsoft\sts\stores\RelationHotels;
use \travelsoft\sts\stores\TourTypes;
use \travelsoft\sts\stores\RelationTourTypes;
use \travelsoft\sts\stores\RoomTypes;
use \travelsoft\sts\stores\RoomCatTypes;
use \travelsoft\sts\stores\Placement;
use \travelsoft\sts\stores\Tours;

\Bitrix\Main\Loader::includeModule('travelsoft.currency');

/**
 * Класс для работы с оператором TezTour
 *
 * @author juliya.sharlova
 * @copyright (c) 2018, travelsoft
 */
class TezTour extends Operator
{

    protected $_request = null;
    protected $_result = null;
    protected $_operator_id = null;
    protected $_request_search_form = null;
    protected $_currency = null;
    protected $_currency_iso = null;
    //protected $_search_url = "http://search.tez-tour.com/tariffsearch/";

    public function __construct(array $request)
    {
        //определить параметры. привести к нужному единому виду. записать в свойство $params

        $this->_request_search_form = $request;
        $this->_operator_id = $request["operator_id"];
        $this->_currency_iso = \travelsoft\currency\factory\Currency::getInstance()->ISO;
        $this->_currency = self::getCurrency();

        if(!isset($request["getDates"])) {

            if (!empty($request["stars"])) {

                $res_stars = \travelsoft\sts\stores\RelationStars::get(array("filter"=>array("UF_BX_ID"=>$request["stars"]),"select"=>array("ID","UF_OP_".$this->_operator_id,"UF_VALUE")));

                if(count($res_stars) > 1) {

                    $arStars = array();

                    foreach ($res_stars as $star){
                        $arStar = (int)$star["UF_VALUE"];
                        if($arStar){
                            $arStars[$star["UF_OP_".$this->_operator_id]] = $arStar;
                        }
                    }
                    if(!empty($arStars)){
                        $minStar = array_keys($arStars, min($arStars));
                        $request["stars"] = current($minStar);
                    } else {
                        $meal = current($res_stars);
                        $request["stars"] = $meal["UF_OP_".$this->_operator_id];
                    }
                    $hotelClassBetter = 'true';

                } else {

                    $meal = current($res_stars);
                    $request["stars"] = $meal["UF_OP_".$this->_operator_id];
                    $hotelClassBetter = 'false';
                }

            } else {

                $hotelClass = current(\travelsoft\sts\stores\Dates::get(array("filter"=>array("UF_CITYFROM_ID"=>$request["cityFrom"],"UF_COUNTRY_ID"=>$request["country"],"UF_OPERATOR_ID"=>$this->_operator_id),"select"=>array("ID","UF_HOTEL_CLASS"))));
                if(!empty($hotelClass["UF_HOTEL_CLASS"])){
                    $request["stars"] = $hotelClass["UF_HOTEL_CLASS"];
                } else {
                    $request["stars"] = current(self::searchPropValue(array("2*"), '\travelsoft\sts\stores\RelationStars', "UF_VALUE", array()));
                }
                $hotelClassBetter = 'true';

            }

            $arPlacement = self::searchPlacement((int)$request["adults"],(int)$request["children"],'\travelsoft\sts\stores\Placement');

            $this->_request = array(
                "parameters" => array(
                    "after" => Utils::create($request["dateFrom"]),
                    "before" => Utils::create($request["dateTo"]),
                    "cityId" => current(RelationCities::getPropValue(array("UF_BX_ID" => $request["cityFrom"]), $this->_operator_id)),
                    "countryId" => current(RelationCountries::getPropValue(array("UF_BX_ID" => $request["country"]), $this->_operator_id)), // получить у оператора Id направления
                    "nightsMin" => (int)$request["nightFrom"],
                    "nightsMax" => (int)$request["nightTo"],
                    "accommodationId" => current(RelationPlacement::getPropValue(array("UF_BX_ID" => $arPlacement), $this->_operator_id)),
                    "hotelClassId" => $request["stars"],
                    "hotelClassBetter" => $hotelClassBetter,
                    "xml" => "true",
                    "priceMin" => 0,
                    "priceMax" => 999999,
                    "cy" => isset($this->_currency[$this->_currency_iso]) ? $this->_currency[$this->_currency_iso] : 0,
                    "formatResult" => "false",
                    "locale" => "ru",
                    "groupByHotel" => isset($request["disablegroup"]) && $request["disablegroup"] ? "false" : "true"
                ),
                "url" => $request["address"],
            );

            $this->_request["query"] = 'getResult?'.http_build_query($this->_request["parameters"]);

            if (!empty($request["hotels"])) {

                if(count($request["operators"]) > 1) {
                    $this->_request["query"] .= '&hotelId='.current(self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_BX_ID", array()));
                } elseif (count($request["operators"]) == 1 && in_array($this->_operator_id, $request["operators"])) {

                    if(isset($request["bx"]) && $request["operators"]){
                        $hotels = current(self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_BX_ID", $request["operators"]));
                    } else {
                        $hotels = current(self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_OP_" . $this->_operator_id, $request["operators"]));
                    }
                    if(!empty($hotels)) {
                        $this->_request["query"] .= '&hotelId='.$hotels;
                    }
                }

            }
            if (!empty($request["cities"])) {

                if(!empty($request["cities"][0])) {
                    $cities = self::searchPropValue($request["cities"], '\travelsoft\sts\stores\RelationCities', "UF_BX_ID", array());
                    if(!empty($cities)){
                        $this->_request["query"] .= self::getParamsForSearch("tourId", $cities);
                    }
                }
            }

            if (!empty($request["meals"])) {

                //$meals = \travelsoft\sts\stores\Food::getSubordinateElementsNew($request["meals"]);
                $sortMeals = \travelsoft\sts\stores\Food::get(array("filter"=>array("ID"=>$request["meals"]),"select"=>array("ID","SORT")));
                foreach ($sortMeals as $meal){
                   $meals[$meal["ID"]] = $meal["SORT"];
                }
                $meals_key = array_keys($meals, min($meals));
                if(empty($meals_key)){
                    $meals_key = $request["meals"];
                }
                $res_meals = self::searchPropValue($meals_key, '\travelsoft\sts\stores\RelationFood', "UF_BX_ID", array());

                if(!empty($res_meals)){
                    $this->_request["query"] .= self::getParamsForSearch("rAndBId", $res_meals);
                    if(count($request["meals"]) > 1) {
                        $this->_request["query"] .= '&rAndBBetter=true';
                    }
                }



            }

            if((int)$request["children"] == 1){
                $age1 = isset($request["age1"]) && !empty($request["age1"]) ? (int)$request["age1"] : 0;
                $this->_request["query"] .= '&birthday1='.date("d.m.").((int)date("Y")-$age1);
            } elseif((int)$request["children"] > 1) {
                $age1 = isset($request["age1"]) && !empty($request["age1"]) ? (int)$request["age1"] : 0;
                $age2 = isset($request["age2"]) && !empty($request["age2"]) ? (int)$request["age2"] : 0;
                $this->_request["query"] .= '&birthday1='.date("d.m.").((int)date("Y")-$age1);
                $this->_request["query"] .= '&birthday2='.date("d.m.").((int)date("Y")-$age2);
            }

            /*if(isset($request["price"]) && is_array($request["price"]) && !empty($request["price"])){
                $this->_request["parameters"]["where"]["prices"] = $request["price"];
            }*/

        } else {

            /*даты выгружаются без авторизации*/
            //$aid = \travelsoft\sts\api\TezTour::authData("AlatanTour", "e853pbd");

            /*$this->_request = array(
                "aid" => $aid
            );*/
            $this->_request = array(
                "parameters" => array(
                    "cityId" => $request["cityFrom"],
                    "countryId" => $request["country"]
                )
            );
        }

    }

    public function sendRequest()
    {

        $this->_result = \travelsoft\sts\api\TezTour::sendRequest($this->_request);
        return $this;

    }

    public function getDates()
    {

        $this->_result = \travelsoft\sts\api\TezTour::getDates($this->_request);
        return $this->_result;

    }

    public function getResult(): array
    {

        //обработка  $this->_result
        $arTotalResult = array();

        if($this->_result === ""){
            throw new \Exception("Не удалось отправить запрос");
        }

        //dm($this->_result);

        $xml = simplexml_load_string('<response>'.$this->_result.'</response>');
        $json = json_encode($xml);
        $arResult = Utils::jsonDecode($json, true);

        $arHotels = array();

        if($arResult["success"] == "false"){
            throw new \Exception($arResult["message"]);
        } elseif ($arResult["searchResult"]["success"] == "true" && isset($arResult["searchResult"]["data"]["item"]) && (count($arResult["searchResult"]["data"]["item"]) > 0)) {

            unset($this->_request_search_form["address"]);
            unset($this->_request_search_form["disablegroup"]);
            unset($this->_request_search_form["operators"]);
            unset($this->_request_search_form["operator_id"]);

            $arQuery["stsSearch"] = $this->_request_search_form;

            $query = '?'.http_build_query($arQuery);

            //разруливаем ошибки и если их нет, то массив ответа
            $arInfo_ = array();

            if ($this->_request["parameters"]["groupByHotel"] == "true") {

                foreach ($arResult["searchResult"]["data"]["item"] as $item) {

                    if (!empty($item["hotel"]["id"])) {

                        $addArray = true;

                        $price = (!empty($item["price"]["currencyId"]) && $item["price"]["total"] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["price"]["total"], array_search($item["price"]["currencyId"], $this->_currency))->getResultLikeArray() : '';

                        if (!isset($arHotels[$item["hotel"]["id"]])) {
                            $arHotels[$item["hotel"]["id"]] = $price["price"];
                        } elseif (isset($arHotels[$item["hotel"]["id"]]) && $arHotels[$item["hotel"]["id"]] > $price["price"]) {
                            $arHotels[$item["hotel"]["id"]] = $price["price"];
                        } else {
                            $addArray = false;
                        }

                        $arInfo = array();

                        if ($item["price"]["total"] > 0 && in_array($item["price"]["currencyId"], $this->_currency) && $addArray) {

                            /*поиск по таблицам*/
                            if (!isset($arInfo_["ar_city"][$this->_request["parameters"]["cityId"]])) {
                                $arInfo_["ar_city"][$this->_request["parameters"]["cityId"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["cityId"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_city"][$this->_request["parameters"]["cityId"]])) {
                                    if (!isset($arInfo_["ar_city_bx"][$this->_request["parameters"]["cityId"]])) {
                                        $arInfo_["ar_city_bx"][$this->_request["parameters"]["cityId"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$this->_request["parameters"]["cityId"]], "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE")))));
                                    }
                                }
                            }
                            if (!isset($arInfo_["ar_city"][$item["region"]["id"]])) {
                                $arInfo_["ar_city"][$item["region"]["id"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["region"]["id"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_city"][$item["region"]["id"]])) {
                                    if (!isset($arInfo_["ar_city_bx"][$item["region"]["id"]])) {
                                        $arInfo_["ar_city_bx"][$item["region"]["id"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$item["region"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                    }
                                }
                            }
                            if (!isset($arInfo_["ar_country"][$this->_request["parameters"]["countryId"]])) {
                                $arInfo_["ar_country"][$this->_request["parameters"]["countryId"]] = current(RelationCountries::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["countryId"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_country"][$this->_request["parameters"]["countryId"]])) {
                                    if (!isset($arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]])) {
                                        $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]] = current(Countries::get(array("filter" => array("ID" => $arInfo_["ar_country"][$this->_request["parameters"]["countryId"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                    }
                                }
                            }
                            if (!isset($arInfo_["ar_hotel"][$item["hotel"]["id"]])) {
                                $arInfo_["ar_hotel"][$item["hotel"]["id"]] = current(RelationHotels::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["hotel"]["id"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_hotel"][$item["hotel"]["id"]])) {
                                    if (!isset($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]])) {
                                        $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]] = current(Hotels::get(array("filter" => array("ID" => $arInfo_["ar_hotel"][$item["hotel"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_MAP", "PROPERTY_PREVIEW_TEXT"))));
                                    }
                                }
                            }
                            if (!isset($arInfo_["ar_food"][$item["pansion"]["id"]])) {
                                $arInfo_["ar_food"][$item["pansion"]["id"]] = current(RelationFood::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["pansion"]["id"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_food"][$item["pansion"]["id"]])) {
                                    if (!isset($arInfo_["ar_food_bx"][$item["pansion"]["id"]])) {
                                        $arInfo_["ar_food_bx"][$item["pansion"]["id"]] = current(Food::get(array("filter" => array("ID" => $arInfo_["ar_food"][$item["pansion"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                    }
                                }
                            }
                            if (!isset($arInfo_["ar_placement"][$item["ageGroupType"]["id"]])) {
                                $arInfo_["ar_placement"][$item["ageGroupType"]["id"]] = current(RelationPlacement::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["ageGroupType"]["id"]), $this->_operator_id));
                                if (!empty($arInfo_["ar_placement"][$item["ageGroupType"]["id"]])) {
                                    if (!isset($arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]])) {
                                        $arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]] = current(Placement::get(array("filter" => array("ID" => $arInfo_["ar_placement"][$item["ageGroupType"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                    }
                                }
                            }
                            if (preg_match('/[0-9]/', $item["hotel"]["name"])) {
                                $stars_hotel = explode(" ", trim($item["hotel"]["name"]));
                                $star_hotel = $stars_hotel[count($stars_hotel) - 2] . "*";
                            }
                            if (isset($star_hotel) && !isset($arInfo_["ar_stars"][$star_hotel])) {
                                $arInfo_["ar_stars"][$star_hotel] = current(RelationStars::getElementNameMod(array("UF_VALUE" => $star_hotel), $this->_operator_id));
                            }
                            /*if(!isset($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                                $arInfo_["ar_tourType"][$item["tourType"]["id"]] = current(RelationTourTypes::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"]), $this->_operator_id));
                                if(!empty($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                                    if(!isset($arInfo_["ar_tourTypeName"][$item["tourType"]["id"]])){
                                        $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]] = current(TourTypes::getElementNameMod((array)$arInfo_["ar_tourType"][$item["tourType"]["id"]]));
                                    }
                                }
                            }*/
                            /*if(!isset($arInfo_["ar_typeSearch"][$item["tourType"]["id"]])){
                                $arInfo_["ar_typeSearch"][$item["tourType"]["id"]] = current(RelationTourTypes::get(['filter' => array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"])]));
                            }*/
                            /*поиск по ИБ*/
                            /*if(!isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]])){
                                $arInfo_["ar_tour_bx"][$item["tour"]["id"]] = current(Tours::getTours($item["tour"]["id"]));
                            }*/

                            $pl_id = is_array($item["ageGroupType"]) ? $item["ageGroupType"]["id"] : '';
                            //$rT_id = is_array($item["roomType"]) ? $item["roomType"]["id"] : '';
                            $rCT_id = is_array($item["hotelRoomType"]) ? $item["hotelRoomType"]["id"] : '';
                            $pl_name = is_array($item["ageGroupType"]) ? $item["ageGroupType"]["name"] : $item["ageGroupType"];
                            //$rT_name = is_array($item["roomType"]) ? $item["roomType"]["name"] : $item["roomType"];
                            $rCT_name = is_array($item["hotelRoomType"]) ? $item["hotelRoomType"]["name"] : $item["hotelRoomType"];

                            parse_str($item["bookingUrl"]["bookingUrl"]["url"], $keyOffer);

                            $arTotalResult[$item["hotel"]["id"]] = array(
                                "operator_id" => $this->_operator_id,
                                "resultKey" => $keyOffer["cResId"],
                                "night" => $item["nightCount"],
                                "nightFormatted" => num2word($item["nightCount"], array(GetMessage('NIGHT_'), GetMessage('NIGHT__'), GetMessage('NIGHT_N'))),
                                "nightFormattedList" => num2word($item["nightCount"], array(GetMessage('NIGHT__'),GetMessage('NIGHT_N'),GetMessage('NIGHT_N'))),
                                "adults" => $this->_request_search_form["adults"],
                                "children" => $this->_request_search_form["children"],
                                "tourType" => array(
                                    /*"id" => $item["tourType"]["id"],
                                    "originalName" => $item["tourType"]["name"],
                                    "name" => $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]],*/
                                    "viewName" => GetMessage('TOUR_TYPE'),
                                    "typeSearch" => "hotel"
                                ),
                                "tourDate" => array(
                                    "dateFrom" => Utils::create($item["checkIn"]),
                                    "dateTo" => Utils::dateModify($item["checkIn"], $item["nightCount"] . ' day'), //$date->modify('+'.$item["night"].' day')->format('d.m.Y'),
                                    "dateFromMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::create($item["checkIn"]), \CSite::GetDateFormat())),
                                    "dateToMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::dateModify($item["checkIn"], $item["nightCount"] . ' day'), \CSite::GetDateFormat()))
                                ),
                                "cityFrom" => array(
                                    "id" => $this->_request["parameters"]["cityId"],
                                    "name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["cityId"]]["NAME"],
                                    "from_name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["cityId"]]["PROPERTIES"]["CN_NAME_CHEGO"]["VALUE"],
                                ),
                                "country" => array(
                                    "id" => $this->_request["parameters"]["countryId"],
                                    "name" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["NAME"],
                                    "link" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["DETAIL_PAGE_URL"] ? $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["DETAIL_PAGE_URL"] : ''
                                ),
                                "city" => array(
                                    "id" => $item["region"]["id"],
                                    "originalName" => $item["region"]["name"],
                                    "name" => !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]) ? $arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"] : '',
                                    "link" => !empty($arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"]) && !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]["CODE"]) ? $arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"] . $arInfo_["ar_city_bx"][$item["region"]["id"]]["CODE"] . "/" : "",
                                    "viewName" => !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"]) ? $arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"] : $item["region"]["name"]
                                ),
                                "resort" => array(),
                                "hotel" => array(
                                    "id" => $item["hotel"]["id"],
                                    "mdId" => md5($this->_operator_id . "_" . $item["hotel"]["id"]),
                                    "originalName" => $item["hotel"]["name"],
                                    "id_bx" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel"][$item["hotel"]["id"]] : '',
                                    "requestHotel" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) && $arInfo_["ar_hotel"][$item["hotel"]["id"]] != 0 ? '{hotels: [' . (int)$arInfo_["ar_hotel"][$item["hotel"]["id"]] . ']}' : '{hotels: [' . (int)$item["hotel"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                    "name" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : '',
                                    "star" => !empty($arInfo_["ar_star"][$star_hotel]) ? $arInfo_["ar_star"][$star_hotel] : $star_hotel,
                                    "image" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 299, "height" => 240)) : Settings::pathNoPhoto(),
                                    "link" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["DETAIL_PAGE_URL"].$query : '',
                                    "services" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"] : "",
                                    "viewName" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : $item["hotel"]["name"],
                                    "map" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"] : '',
                                    "previewText" => !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"]) ? substr2($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"], 100) : '',
                                ),
                                /*"tour" => array(
                                    "id" => $item["tour"]["id"],
                                    "mdId" => md5($this->_operator_id."_".$item["tour"]["id"]),
                                    "originalName" => $item["tour"]["name"],
                                    "id_bx" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['ID'] : '',
                                    //"requestTour" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] != 0 ? '{tours: [' . (int)$arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] . ']}' : '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                    "requestTour" => '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                    "name" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : '',
                                    "image" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 299, "height" => 240)) : Settings::pathNoPhoto(),
                                    "link" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["DETAIL_PAGE_URL"] : '',
                                    "viewName" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : $item["tour"]["name"],
                                    "description" => substr2($arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['PREVIEW_TEXT'], 150),
                                    "days" => $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['DAYS'],
                                ),*/
                                "placement" => array(
                                    "id" => $item["ageGroupType"]["id"],
                                    "originalName" => '',
                                    "name" => '',
                                    "viewName" => !empty($arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]]["NAME"]) ? $arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]]["NAME"] : ''
                                ),
                                "roomType" => array(
                                    "id" => '',
                                    "originalName" => '',
                                    "name" => '',
                                    "viewName" => ''
                                ),
                                "roomCatType" => array(
                                    "id" => $rCT_id,
                                    "originalName" => $rCT_name,
                                    "name" => '',
                                    "viewName" => $rCT_name
                                ),
                                "food" => array(
                                    "id" => $item["pansion"]["id"],
                                    "originalName" => $item["pansion"]["name"],
                                    "name" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"] : '',
                                    "fullName" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["NAME"]["VALUE"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["NAME"]["VALUE"] : (!empty($item["pansion"]["description"]) ? $item["pansion"]["description"] : ""),
                                    "desc" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"] : (!empty($item["pansion"]["description"]) ? $item["pansion"]["description"] : ""),
                                    "viewName" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"] : $item["pansion"]["name"]
                                ),
                                /*"quoteHotel" => $item["quoteHotel"],
                                "quoteAvia" => array(
                                    "economy" => array(
                                        "quoteAviaFrom" => $item["quoteAviaFrom"],
                                        "quoteAviaTo" => $item["quoteAviaTo"],
                                    ),
                                    "business" => array(
                                        "quoteAviaFrom" => '',
                                        "quoteAviaTo" => '',
                                    )
                                ),
                                "oilTaxes" => array(),
                                "visa" => array(),*/
                                "defCurrency" => array_search($item["price"]["currencyId"], $this->_currency),
                                "prices" => number_format($item["price"]["total"], 0, '.', ''),
                                "price_for" => "за номер",
                                "priceCurrency" => $price,
                                "priceCurrency_" => (!empty($item["price"]["currencyId"]) && $item["price"]["total"] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["price"]["total"], array_search($item["price"]["currencyId"], $this->_currency))->getResult() : '',
                            );
                            $key = end(array_keys($arTotalResult));

                            $departure_city = $arTotalResult[$key]["cityFrom"]["from_name"] ? 'из ' . $arTotalResult[$key]["cityFrom"]["from_name"] : $arTotalResult[$key]["cityFrom"]["name"];
                            $departure_date = $arTotalResult[$key]["tourDate"]["dateFrom"] ? 'c ' . $arTotalResult[$key]["tourDate"]["dateFrom"] : $arTotalResult[$key]["tourDate"]["dateFrom"];
                            $arTotalResult[$key]["infoFormattedTour"] = array(
                                "departure" => implode(', ', array($departure_city, $departure_date)),
                                "accommodation" => implode(', ', array($arTotalResult[$key]["placement"]["viewName"], $arTotalResult[$key]["roomCatType"]["viewName"])),
                                "food" => GetMessage('FOOD') . $arTotalResult[$key]["food"]["viewName"],
                            );

                        }
                    }

                }

                if(!empty($arTotalResult)){
                    $arTotalResult = array_values($arTotalResult);
                }

            } else {

                foreach ($arResult["searchResult"]["data"]["item"] as $item) {

                    $price = (!empty($item["price"]["currencyId"]) && $item["price"]["total"] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["price"]["total"], array_search($item["price"]["currencyId"], $this->_currency))->getResultLikeArray() : '';

                    $arInfo = array();

                    if ($item["price"]["total"] > 0 && in_array($item["price"]["currencyId"], $this->_currency)) {

                        /*поиск по таблицам*/
                        if (!isset($arInfo_["ar_city"][$this->_request["parameters"]["cityId"]])) {
                            $arInfo_["ar_city"][$this->_request["parameters"]["cityId"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["cityId"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_city"][$this->_request["parameters"]["cityId"]])) {
                                if (!isset($arInfo_["ar_city_bx"][$this->_request["parameters"]["cityId"]])) {
                                    $arInfo_["ar_city_bx"][$this->_request["parameters"]["cityId"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$this->_request["parameters"]["cityId"]], "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE")))));
                                }
                            }
                        }
                        if (!isset($arInfo_["ar_city"][$item["region"]["id"]])) {
                            $arInfo_["ar_city"][$item["region"]["id"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["region"]["id"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_city"][$item["region"]["id"]])) {
                                if (!isset($arInfo_["ar_city_bx"][$item["region"]["id"]])) {
                                    $arInfo_["ar_city_bx"][$item["region"]["id"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$item["region"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                }
                            }
                        }
                        if (!isset($arInfo_["ar_country"][$this->_request["parameters"]["countryId"]])) {
                            $arInfo_["ar_country"][$this->_request["parameters"]["countryId"]] = current(RelationCountries::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["countryId"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_country"][$this->_request["parameters"]["countryId"]])) {
                                if (!isset($arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]])) {
                                    $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]] = current(Countries::get(array("filter" => array("ID" => $arInfo_["ar_country"][$this->_request["parameters"]["countryId"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                }
                            }
                        }
                        if (!isset($arInfo_["ar_hotel"][$item["hotel"]["id"]])) {
                            $arInfo_["ar_hotel"][$item["hotel"]["id"]] = current(RelationHotels::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["hotel"]["id"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_hotel"][$item["hotel"]["id"]])) {
                                if (!isset($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]])) {
                                    $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]] = current(Hotels::get(array("filter" => array("ID" => $arInfo_["ar_hotel"][$item["hotel"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_MAP", "PROPERTY_PREVIEW_TEXT"))));
                                }
                            }
                        }
                        if (!isset($arInfo_["ar_food"][$item["pansion"]["id"]])) {
                            $arInfo_["ar_food"][$item["pansion"]["id"]] = current(RelationFood::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["pansion"]["id"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_food"][$item["pansion"]["id"]])) {
                                if (!isset($arInfo_["ar_food_bx"][$item["pansion"]["id"]])) {
                                    $arInfo_["ar_food_bx"][$item["pansion"]["id"]] = current(Food::get(array("filter" => array("ID" => $arInfo_["ar_food"][$item["pansion"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                }
                            }
                        }
                        if (!isset($arInfo_["ar_placement"][$item["ageGroupType"]["id"]])) {
                            $arInfo_["ar_placement"][$item["ageGroupType"]["id"]] = current(RelationPlacement::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["ageGroupType"]["id"]), $this->_operator_id));
                            if (!empty($arInfo_["ar_placement"][$item["ageGroupType"]["id"]])) {
                                if (!isset($arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]])) {
                                    $arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]] = current(Placement::get(array("filter" => array("ID" => $arInfo_["ar_placement"][$item["ageGroupType"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                                }
                            }
                        }
                        if (preg_match('/[0-9]/', $item["hotel"]["name"])) {
                            $stars_hotel = explode(" ", trim($item["hotel"]["name"]));
                            $star_hotel = $stars_hotel[count($stars_hotel) - 2] . "*";
                        }
                        if (isset($star_hotel) && !isset($arInfo_["ar_stars"][$star_hotel])) {
                            $arInfo_["ar_stars"][$star_hotel] = current(RelationStars::getElementNameMod(array("UF_VALUE" => $star_hotel), $this->_operator_id));
                        }
                        /*if(!isset($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                            $arInfo_["ar_tourType"][$item["tourType"]["id"]] = current(RelationTourTypes::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"]), $this->_operator_id));
                            if(!empty($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                                if(!isset($arInfo_["ar_tourTypeName"][$item["tourType"]["id"]])){
                                    $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]] = current(TourTypes::getElementNameMod((array)$arInfo_["ar_tourType"][$item["tourType"]["id"]]));
                                }
                            }
                        }*/
                        /*if(!isset($arInfo_["ar_typeSearch"][$item["tourType"]["id"]])){
                            $arInfo_["ar_typeSearch"][$item["tourType"]["id"]] = current(RelationTourTypes::get(['filter' => array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"])]));
                        }*/
                        /*поиск по ИБ*/
                        /*if(!isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]])){
                            $arInfo_["ar_tour_bx"][$item["tour"]["id"]] = current(Tours::getTours($item["tour"]["id"]));
                        }*/

                        $pl_id = is_array($item["ageGroupType"]) ? $item["ageGroupType"]["id"] : '';
                        //$rT_id = is_array($item["roomType"]) ? $item["roomType"]["id"] : '';
                        $rCT_id = is_array($item["hotelRoomType"]) ? $item["hotelRoomType"]["id"] : '';
                        $pl_name = is_array($item["ageGroupType"]) ? $item["ageGroupType"]["name"] : $item["ageGroupType"];
                        //$rT_name = is_array($item["roomType"]) ? $item["roomType"]["name"] : $item["roomType"];
                        $rCT_name = is_array($item["hotelRoomType"]) ? $item["hotelRoomType"]["name"] : $item["hotelRoomType"];

                        parse_str($item["bookingUrl"]["bookingUrl"]["url"], $keyOffer);

                        $arTotalResult[] = array(
                            "operator_id" => $this->_operator_id,
                            "resultKey" => $keyOffer["cResId"],
                            "night" => $item["nightCount"],
                            "nightFormatted" => num2word($item["nightCount"], array(GetMessage('NIGHT_'), GetMessage('NIGHT__'), GetMessage('NIGHT_N'))),
                            "nightFormattedList" => num2word($item["nightCount"], array(GetMessage('NIGHT__'),GetMessage('NIGHT_N'),GetMessage('NIGHT_N'))),
                            "adults" => $this->_request_search_form["adults"],
                            "children" => $this->_request_search_form["children"],
                            "tourType" => array(
                                /*"id" => $item["tourType"]["id"],
                                "originalName" => $item["tourType"]["name"],
                                "name" => $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]],*/
                                "viewName" => GetMessage('TOUR_TYPE'),
                                "typeSearch" => "hotel"
                            ),
                            "tourDate" => array(
                                "dateFrom" => Utils::create($item["checkIn"]),
                                "dateTo" => Utils::dateModify($item["checkIn"], $item["nightCount"] . ' day'), //$date->modify('+'.$item["night"].' day')->format('d.m.Y'),
                                "dateFromMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::create($item["checkIn"]), \CSite::GetDateFormat())),
                                "dateToMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::dateModify($item["checkIn"], $item["nightCount"] . ' day'), \CSite::GetDateFormat()))
                            ),
                            "cityFrom" => array(
                                "id" => $this->_request["parameters"]["cityId"],
                                "name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["cityId"]]["NAME"],
                                "from_name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["cityId"]]["PROPERTIES"]["CN_NAME_CHEGO"]["VALUE"],
                            ),
                            "country" => array(
                                "id" => $this->_request["parameters"]["countryId"],
                                "name" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["NAME"],
                                "link" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["DETAIL_PAGE_URL"] ? $arInfo_["ar_country_bx"][$this->_request["parameters"]["countryId"]]["DETAIL_PAGE_URL"] : ''
                            ),
                            "city" => array(
                                "id" => $item["region"]["id"],
                                "originalName" => $item["region"]["name"],
                                "name" => !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]) ? $arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"] : '',
                                "link" => !empty($arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"]) && !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]["CODE"]) ? $arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"] . $arInfo_["ar_city_bx"][$item["region"]["id"]]["CODE"] . "/" : "",
                                "viewName" => !empty($arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"]) ? $arInfo_["ar_city_bx"][$item["region"]["id"]]["NAME"] : $item["region"]["name"]
                            ),
                            "resort" => array(),
                            "hotel" => array(
                                "id" => $item["hotel"]["id"],
                                "mdId" => md5($this->_operator_id . "_" . $item["hotel"]["id"]),
                                "originalName" => $item["hotel"]["name"],
                                "id_bx" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel"][$item["hotel"]["id"]] : '',
                                "requestHotel" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) && $arInfo_["ar_hotel"][$item["hotel"]["id"]] != 0 ? '{hotels: [' . (int)$arInfo_["ar_hotel"][$item["hotel"]["id"]] . ']}' : '{hotels: [' . (int)$item["hotel"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                "name" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : '',
                                "star" => !empty($arInfo_["ar_star"][$star_hotel]) ? $arInfo_["ar_star"][$star_hotel] : $star_hotel,
                                "image" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 299, "height" => 240)) : Settings::pathNoPhoto(),
                                "link" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["DETAIL_PAGE_URL"].$query : '',
                                "services" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"] : "",
                                "viewName" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : $item["hotel"]["name"],
                                "map" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"] : '',
                                "previewText" => !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"]) ? substr2($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"], 100) : '',
                            ),
                            /*"tour" => array(
                                "id" => $item["tour"]["id"],
                                "mdId" => md5($this->_operator_id."_".$item["tour"]["id"]),
                                "originalName" => $item["tour"]["name"],
                                "id_bx" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['ID'] : '',
                                //"requestTour" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] != 0 ? '{tours: [' . (int)$arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] . ']}' : '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                "requestTour" => '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                                "name" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : '',
                                "image" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 299, "height" => 240)) : Settings::pathNoPhoto(),
                                "link" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["DETAIL_PAGE_URL"] : '',
                                "viewName" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : $item["tour"]["name"],
                                "description" => substr2($arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['PREVIEW_TEXT'], 150),
                                "days" => $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['DAYS'],
                            ),*/
                            "placement" => array(
                                "id" => $item["ageGroupType"]["id"],
                                "originalName" => '',
                                "name" => '',
                                "viewName" => !empty($arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]]["NAME"]) ? $arInfo_["ar_placement_bx"][$item["ageGroupType"]["id"]]["NAME"] : ''
                            ),
                            "roomType" => array(
                                "id" => '',
                                "originalName" => '',
                                "name" => '',
                                "viewName" => ''
                            ),
                            "roomCatType" => array(
                                "id" => $rCT_id,
                                "originalName" => $rCT_name,
                                "name" => '',
                                "viewName" => $rCT_name
                            ),
                            "food" => array(
                                "id" => $item["pansion"]["id"],
                                "originalName" => $item["pansion"]["name"],
                                "name" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"] : '',
                                "fullName" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["NAME"]["VALUE"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["NAME"]["VALUE"] : (!empty($item["pansion"]["description"]) ? $item["pansion"]["description"] : ""),
                                "desc" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"]) ? $arInfo_["ar_food_bx"][$item["pansion"]["id"]]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"] : (!empty($item["pansion"]["description"]) ? $item["pansion"]["description"] : ""),
                                "viewName" => !empty($arInfo_["ar_food_bx"][$item["pansion"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"] : $item["pansion"]["name"]
                            ),
                            /*"quoteHotel" => $item["quoteHotel"],
                            "quoteAvia" => array(
                                "economy" => array(
                                    "quoteAviaFrom" => $item["quoteAviaFrom"],
                                    "quoteAviaTo" => $item["quoteAviaTo"],
                                ),
                                "business" => array(
                                    "quoteAviaFrom" => '',
                                    "quoteAviaTo" => '',
                                )
                            ),
                            "oilTaxes" => array(),
                            "visa" => array(),*/
                            "defCurrency" => array_search($item["price"]["currencyId"], $this->_currency),
                            "prices" => number_format($item["price"]["total"], 0, '.', ''),
                            "price_for" => "за номер",
                            "priceCurrency" => $price,
                            "priceCurrency_" => (!empty($item["price"]["currencyId"]) && $item["price"]["total"] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["price"]["total"], array_search($item["price"]["currencyId"], $this->_currency))->getResult() : '',
                        );
                        $key = end(array_keys($arTotalResult));

                        $departure_city = $arTotalResult[$key]["cityFrom"]["from_name"] ? 'из ' . $arTotalResult[$key]["cityFrom"]["from_name"] : $arTotalResult[$key]["cityFrom"]["name"];
                        $departure_date = $arTotalResult[$key]["tourDate"]["dateFrom"] ? 'c ' . $arTotalResult[$key]["tourDate"]["dateFrom"] : $arTotalResult[$key]["tourDate"]["dateFrom"];
                        $arTotalResult[$key]["infoFormattedTour"] = array(
                            "departure" => implode(', ', array($departure_city, $departure_date)),
                            "accommodation" => implode(', ', array($arTotalResult[$key]["placement"]["viewName"], $arTotalResult[$key]["roomCatType"]["viewName"])),
                            "food" => GetMessage('FOOD') . $arTotalResult[$key]["food"]["viewName"],
                        );

                    }

                }

            }

        }

        return $arTotalResult;
        
    }

    public function searchPropValue ($values, $propTable, $propName, $operators): array {

        $arValue = array();
        $operators_ = $operators || null;

        $arValue = call_user_func_array(array($propTable, "getPropValue"), array(array($propName => $values), $this->_operator_id));
        if(empty($arValue)){

            if(!$operators_ && count($operators_) != 1 && !in_array($this->_operator_id,$operators_)){
                throw new \Exception("Нет значения поиска у этого оператора");
            }

        }

        return $arValue;

    }

    public function searchPlacement ($adults, $children, $propBlock): array {

        $arValue = array();
        $arrayMas = array(
            'LOGIC' => 'AND',
            array('PROPERTY_ADULT' => $adults),
            array('PROPERTY_CHILDREN' => $children)
        );

        $arValue = call_user_func_array(array($propBlock, "getBxIdAr"), array(array($arrayMas)));

        return $arValue;

    }

    public function getParamsForSearch ($propKey, $values): string {

        $glue = '&'.$propKey.'=';

        $arValue = $glue.implode($glue, $values);

        return $arValue;

    }

    public function getCurrency (): array {

        $arCurrency = array(
            "BYN" => 533067,
            "USD" => 5561,
            "EUR" => 18864,
            "RUB" => 8390,
            "UAH" => 46688,
            "KZT" => 122196,
            "BGN" => 141410
        );

        return $arCurrency;

    }

}