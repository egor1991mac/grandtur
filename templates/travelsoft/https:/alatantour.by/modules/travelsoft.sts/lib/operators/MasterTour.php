<?php

namespace travelsoft\sts\operators;

use \travelsoft\sts\_interfaces\Operator;
use travelsoft\sts\Settings;
use \travelsoft\sts\Utils;
use \travelsoft\sts\stores\Food;
use \travelsoft\sts\stores\RelationFood;
use \travelsoft\sts\stores\Stars;
use \travelsoft\sts\stores\RelationStars;
use \travelsoft\sts\stores\Cities;
use \travelsoft\sts\stores\Regions;
use \travelsoft\sts\stores\RelationCities;
use \travelsoft\sts\stores\Countries;
use \travelsoft\sts\stores\RelationCountries;
use \travelsoft\sts\stores\Hotels;
use \travelsoft\sts\stores\RelationHotels;
use \travelsoft\sts\stores\TourTypes;
use \travelsoft\sts\stores\RelationTourTypes;
use \travelsoft\sts\stores\RoomTypes;
use \travelsoft\sts\stores\RoomCatTypes;
use \travelsoft\sts\stores\Placement;
use \travelsoft\sts\stores\Tours;

\Bitrix\Main\Loader::includeModule('travelsoft.currency');

/**
 * Класс для работы с оператором Мастер-Тур
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class MasterTour extends Operator
{
    protected $_request = null;
    protected $_result = null;
    protected $_operator_id = null;
    protected $_request_search_form = null;
    protected $_arTypeSearch = [];

    public function __construct(array $request)
    {
        //определить параметры. привести к нужному единому виду. записать в свойство $params

        $this->_request_search_form = $request;
        $this->_operator_id = $request["operator_id"];

        if(!isset($request["getDates"])) {

            if (!empty($request["hotels"])) {

                if(count($request["operators"]) > 1) {
                    $request["hotels"] = self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_BX_ID", array());
                } elseif (count($request["operators"]) == 1 && in_array($this->_operator_id, $request["operators"])) {

                    if(isset($request["bx"]) && $request["operators"]){
                        $hotels = self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_BX_ID", $request["operators"]);
                    } else {
                        $hotels = self::searchPropValue($request["hotels"], '\travelsoft\sts\stores\RelationHotels', "UF_OP_" . $this->_operator_id, $request["operators"]);
                    }
                    if(!empty($hotels)) {
                        $request["hotels"] = $hotels;
                    }
                }

            }
            if (!empty($request["tourTypes"])) {

                if(!empty($request["tourTypes"][0])) {

                    $tourTypes = \travelsoft\sts\stores\RelationTourTypes::get(array("filter" => array("UF_BX_ID" => $request["tourTypes"])), true);

                    $this->_arTypeSearch = array_column($tourTypes, 'UF_TYPE_SEARCH');

                    $tour_type = \travelsoft\sts\stores\TourTypes::getSubordinateElementsNew($request["tourTypes"]);
                    $res_tour_type = self::searchPropValue($tour_type, '\travelsoft\sts\stores\RelationTourTypes', "UF_BX_ID");
                    $request["tourTypes"] = $res_tour_type;

                } else {
                    $request["tourTypes"] = null;
                }

            }

            if (!empty($request["stars"])) {

                $stars = \travelsoft\sts\stores\Stars::getSubordinateElementsNew($request["stars"]);
                $res_stars = self::searchPropValue($stars, '\travelsoft\sts\stores\RelationStars', "UF_BX_ID", array());
                $request["stars"] = $res_stars;

                //$request["stars"] = self::searchPropValue($request["stars"], '\travelsoft\sts\stores\RelationStars', "UF_VALUE");

            }
            if (!empty($request["meals"])) {

                $meals = \travelsoft\sts\stores\Food::getSubordinateElementsNew($request["meals"]);
                $res_meals = self::searchPropValue($meals, '\travelsoft\sts\stores\RelationFood', "UF_BX_ID", array());
                $request["meals"] = $res_meals;

            }

            if (!empty($request["cities"])) {

                if(!empty($request["cities"][0])) {
                    $request["cities"] = self::searchPropValue($request["cities"], '\travelsoft\sts\stores\RelationCities', "UF_BX_ID", array());
                } else {
                    $request["cities"] = [];
                }
            }

            //доработка для выборки город по региону
            if (!empty($request["regions"])) {

                $regions = \travelsoft\sts\stores\Regions::getSubordinateCities((int)$request["cityFrom"],(int)$request["country"],$request["regions"]);
                if(!empty($regions)){
                    $request["cities"] = self::searchPropValue($regions, '\travelsoft\sts\stores\RelationCities', "UF_BX_ID", array());
                } else {
                    $request["cities"] = [];
                }

            }

            $this->_request = array(
                "parameters" => array(
                    "paging" => array(
                        "size" => 100, //TODO: изменить на необходимое количество!!!
                        "page" => 1
                    ),
                    "where" => array(
                        "date" => array(
                            Utils::create($request["dateFrom"]),
                            Utils::create($request["dateTo"])
                        ),
                        "cityFrom" => current(RelationCities::getPropValue(array("UF_BX_ID" => $request["cityFrom"]), $this->_operator_id)),
                        "countryKey" => current(RelationCountries::getPropValue(array("UF_BX_ID" => $request["country"]), $this->_operator_id)), // получить у оператора Id направления
                        "nights" => array(
                            (int)$request["nightFrom"],
                            (int)$request["nightTo"]
                        ),
                        "adults" => (int)$request["adults"],
                        "childs" => (int)$request["children"],
                        "tourTypes" => $request["tourTypes"],
                        "tours" => $request["tours"],
                        "cities" => $request["cities"],
                        "hotels" => $request["hotels"],
                        "stars" => $request["stars"],
                        "meals" => $request["meals"]
                    ),
                    "sort" => array(
                        "price" => "asc"
                    ),
                    "disablegroup" => isset($request["disablegroup"]) && $request["disablegroup"] ? "true" : "false",
                ),
                "address" => $request["address"],
            );

            if((int)$request["children"] == 1){
                $this->_request["parameters"]["where"]["age1"] = isset($request["age1"]) && !empty($request["age1"]) ? (int)$request["age1"] : 0;
            } elseif((int)$request["children"] > 1) {
                $this->_request["parameters"]["where"]["age1"] = isset($request["age1"]) && !empty($request["age1"]) ? (int)$request["age1"] : 0;
                $this->_request["parameters"]["where"]["age2"] = isset($request["age2"]) && !empty($request["age2"]) ? (int)$request["age2"] : 0;
            }

            if(isset($request["price"]) && is_array($request["price"]) && !empty($request["price"])){
                $this->_request["parameters"]["where"]["prices"] = $request["price"];
            }

            /*if(isset($request["priceFrom"]) && !empty($request["priceFrom"]) && isset($request["currency"]) && !empty($request["currency"])){

                $price_to = isset($request["priceTo"]) && !empty($request["priceTo"]) ? $request["priceTo"] : $request["priceFrom"];
                $this->_request["parameters"]["where"]["prices"] = array($request["priceFrom"], $price_to, $request["currency"]);

            } elseif(isset($request["priceTo"]) && !empty($request["priceTo"]) && isset($request["currency"]) && !empty($request["currency"])) {

                $price_from = isset($request["priceFrom"]) && !empty($request["priceFrom"]) ? $request["priceFrom"] : $request["priceTo"];
                $this->_request["parameters"]["where"]["prices"] = array($price_from, $request["priceTo"], $request["currency"]);

            }*/

        } else {
            $this->_request = array(
                "address" => $request["address"]
            );
        }

    }

    public function sendRequest()
    {

        //dm(array($this->_request, $this->_arTypeSearch),false,true,false);
        $this->_result = \travelsoft\sts\api\MasterTour::sendRequest($this->_request, $this->_arTypeSearch);
        return $this;

    }

    public function getDates()
    {

        $this->_result = \travelsoft\sts\api\MasterTour::getDates($this->_request);
        return $this->_result;

    }

    public function getResult(): array
    {

        //обработка  $this->_result
        $arTotalResult = array();

        if($this->_result === ""){
            throw new \Exception("Не удалось отправить запрос");
        }

        $arResult = Utils::jsonDecode($this->_result);

        if(isset($arResult["error"])){
            throw new \Exception($arResult["error"]["message"]);
        } elseif (isset($arResult["result"]["List"]) && ($arResult["result"]["rowCount"] > 0 || $arResult["result"]["recordCount"] > 0)) {

            unset($this->_request_search_form["address"]);
            unset($this->_request_search_form["disablegroup"]);
            unset($this->_request_search_form["operators"]);
            unset($this->_request_search_form["operator_id"]);

            $arQuery["stsSearch"] = $this->_request_search_form;

            $query = '?'.http_build_query($arQuery);

            //разруливаем ошибки и если их нет, то массив ответа
            //dm($arResult["result"]["List"],false,true,false);

            $arInfo_ = array();

            foreach ($arResult["result"]["List"] as $item) {

                $arInfo = array();

                if($item["prices"][$item["defaultRate"]] > 0) {

                    /*поиск по таблицам*/
                    if(!isset($arInfo_["ar_city"][$this->_request["parameters"]["where"]["cityFrom"]])){
                        $arInfo_["ar_city"][$this->_request["parameters"]["where"]["cityFrom"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["where"]["cityFrom"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_city"][$this->_request["parameters"]["where"]["cityFrom"]])) {
                            if(!isset($arInfo_["ar_city_bx"][$this->_request["parameters"]["where"]["cityFrom"]])){
                                $arInfo_["ar_city_bx"][$this->_request["parameters"]["where"]["cityFrom"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$this->_request["parameters"]["where"]["cityFrom"]], "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE")))));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_city"][$item["city"]["id"]])){
                        $arInfo_["ar_city"][$item["city"]["id"]] = current(RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["city"]["id"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_city"][$item["city"]["id"]])) {
                            if (!isset($arInfo_["ar_city_bx"][$item["city"]["id"]])) {
                                $arInfo_["ar_city_bx"][$item["city"]["id"]] = current(Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"][$item["city"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_country"][$this->_request["parameters"]["where"]["countryKey"]])){
                        $arInfo_["ar_country"][$this->_request["parameters"]["where"]["countryKey"]] = current(RelationCountries::getBxIdMod(array("UF_OP_" . $this->_operator_id => $this->_request["parameters"]["where"]["countryKey"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_country"][$this->_request["parameters"]["where"]["countryKey"]])){
                            if(!isset($arInfo_["ar_country_bx"][$this->_request["parameters"]["where"]["countryKey"]])){
                                $arInfo_["ar_country_bx"][$this->_request["parameters"]["where"]["countryKey"]] = current(Countries::get(array("filter" => array("ID" => $arInfo_["ar_country"][$this->_request["parameters"]["where"]["countryKey"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_hotel"][$item["hotel"]["id"]])){
                        $arInfo_["ar_hotel"][$item["hotel"]["id"]] = current(RelationHotels::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["hotel"]["id"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_hotel"][$item["hotel"]["id"]])){
                            if(!isset($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]])){
                                $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]] = current(Hotels::get(array("filter" => array("ID" => $arInfo_["ar_hotel"][$item["hotel"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_MAP", "PROPERTY_PREVIEW_TEXT"))));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_food"][$item["meal"]["id"]])){
                        $arInfo_["ar_food"][$item["meal"]["id"]] = current(RelationFood::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["meal"]["id"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_food"][$item["meal"]["id"]])){
                            if(!isset($arInfo_["ar_food_bx"][$item["meal"]["id"]])){
                                $arInfo_["ar_food_bx"][$item["meal"]["id"]] = current(Food::get(array("filter" => array("ID" => $arInfo_["ar_food"][$item["meal"]["id"]]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_stars"][$item["hotel"]["star"]])){
                        $arInfo_["ar_stars"][$item["hotel"]["star"]] = current(RelationStars::getElementNameMod(array("UF_OP_" . $this->_operator_id => $item["hotel"]["star"]), $this->_operator_id));
                    }
                    if(!isset($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                        $arInfo_["ar_tourType"][$item["tourType"]["id"]] = current(RelationTourTypes::getBxIdMod(array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"]), $this->_operator_id));
                        if(!empty($arInfo_["ar_tourType"][$item["tourType"]["id"]])){
                            if(!isset($arInfo_["ar_tourTypeName"][$item["tourType"]["id"]])){
                                $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]] = current(TourTypes::getElementNameMod((array)$arInfo_["ar_tourType"][$item["tourType"]["id"]]));
                            }
                        }
                    }
                    if(!isset($arInfo_["ar_typeSearch"][$item["tourType"]["id"]])){
                        $arInfo_["ar_typeSearch"][$item["tourType"]["id"]] = current(RelationTourTypes::get(['filter' => array("UF_OP_" . $this->_operator_id => $item["tourType"]["id"])]));
                    }
                    /*поиск по ИБ*/
                    if(!isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]])){
                        $arInfo_["ar_tour_bx"][$item["tour"]["id"]] = current(Tours::getTours($item["tour"]["id"]));
                    }

                    /*if(!in_array($this->_request["parameters"]["where"]["cityFrom"],$arInfo["ar_city"])) $arInfo["ar_city"][] = $this->_request["parameters"]["where"]["cityFrom"];
                    if(!in_array($item["city"]["id"],$arInfo["ar_city"])) $arInfo["ar_city"][] = $item["city"]["id"];
                    if(!in_array($this->_request["parameters"]["where"]["countryKey"],$arInfo["ar_country"])) $arInfo["ar_country"][] = $this->_request["parameters"]["where"]["countryKey"];
                    if(!in_array($item["hotel"]["id"],$arInfo["ar_hotel"])) $arInfo["ar_hotel"][] = $item["hotel"]["id"];
                    if(!in_array($item["meal"]["id"],$arInfo["ar_food"])) $arInfo["ar_food"][] = $item["meal"]["id"];
                    if(!in_array($item["hotel"]["star"],$arInfo["ar_stars"])) $arInfo["ar_stars"][] = $item["hotel"]["star"];
                    if(is_array($item["tourType"])){
                        if(!in_array($item["tourType"]["id"],$arInfo["ar_tourType"])) $arInfo["ar_tourType"][] = $item["tourType"]["id"];
                    } else {
                        if(!in_array($item["tourType"],$arInfo["ar_tourType"])) $arInfo["ar_tourType"][] = $item["tourType"];
                    }
                    if(is_array($item["accm"])){
                        if(!in_array($item["accm"]["id"],$arInfo["ar_placement"])) $arInfo["ar_placement"][] = $item["accm"]["id"];
                    } else {
                        if(!in_array($item["accm"],$arInfo["ar_placement"])) $arInfo["ar_placement"][] = $item["accm"];
                    }
                    if(is_array($item["roomType"])){
                        if(!in_array($item["roomType"]["id"],$arInfo["ar_roomType"])) $arInfo["ar_roomType"][] = $item["roomType"]["id"];
                    } else {
                        if(!in_array($item["roomType"],$arInfo["ar_roomType"])) $arInfo["ar_roomType"][] = $item["roomType"];
                    }
                    if(is_array($item["roomCat"])){
                        if(!in_array($item["roomCat"]["id"],$arInfo["ar_roomCat"])) $arInfo["ar_roomCat"][] = $item["roomCat"]["id"];
                    } else {
                        if(!in_array($item["roomCat"],$arInfo["ar_roomCat"])) $arInfo["ar_roomCat"][] = $item["roomCat"];
                    }
                    if(is_array($item["tour"])){
                        if(!in_array($item["tour"]["id"],$arInfo["ar_tour"])) $arInfo["ar_tour"][] = $item["tour"]["id"];
                    } else {
                        if(!in_array($item["tour"],$arInfo["ar_tour"])) $arInfo["ar_tour"][] = $item["tour"];
                    }

                    if (!empty($arInfo)) {
                        $arInfo_["ar_city"] = !empty($arInfo["ar_city"]) ? RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_city"]), $this->_operator_id) : array();
                        $arInfo_["ar_country"] = !empty($arInfo["ar_country"]) ? RelationCountries::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_country"]), $this->_operator_id) : array();
                        $arInfo_["ar_hotel"] = !empty($arInfo["ar_hotel"]) ? RelationHotels::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_hotel"]), $this->_operator_id) : array();
                        $arInfo_["ar_food"] = !empty($arInfo["ar_food"]) ? RelationFood::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_food"]), $this->_operator_id) : array();
                        $arInfo_["ar_star"] = !empty($arInfo["ar_stars"]) ? RelationStars::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_stars"]), $this->_operator_id) : array();
                        $arInfo_["ar_tourType"] = !empty($arInfo["ar_tourType"]) ? RelationTourTypes::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_tourType"]), $this->_operator_id) : array();
                        $arInfo_["ar_typeSearch"] = !empty($arInfo["ar_tourType"]) ? RelationTourTypes::get(['filter' => array("UF_OP_" . $this->_operator_id => $arInfo["ar_tourType"])]) : array();
                        $arInfo_["ar_tourTypeName"] = !empty($arInfo_["ar_tourType"]) ? TourTypes::getElementNameMod($arInfo_["ar_tourType"]) : array();
                        $arInfo_["ar_placement"] = !empty($arInfo["ar_placement"]) ? Placement::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_placement"]), $this->_operator_id) : array();
                        $arInfo_["ar_roomType"] = !empty($arInfo["ar_roomType"]) ? RoomTypes::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_roomType"]), $this->_operator_id) : array();
                        $arInfo_["ar_roomCat"] = !empty($arInfo["ar_roomCat"]) ? RoomCatTypes::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_roomCat"]), $this->_operator_id) : array();

                        $arInfo_["ar_city_bx"] = !empty($arInfo_["ar_city"]) ? Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();
                        $arInfo_["ar_country_bx"] = !empty($arInfo_["ar_country"]) ? Countries::get(array("filter" => array("ID" => $arInfo_["ar_country"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();
                        $arInfo_["ar_hotel_bx"] = !empty($arInfo_["ar_hotel"]) ? Hotels::get(array("filter" => array("ID" => $arInfo_["ar_hotel"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_MAP", "PROPERTY_PREVIEW_TEXT"))) : array();
                        $arInfo_["ar_food_bx"] = !empty($arInfo_["ar_food"]) ? Food::get(array("filter" => array("ID" => $arInfo_["ar_food"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();
                        $arInfo_["ar_tour_bx"] = !empty($arInfo["ar_tour"]) ? Tours::getTours($arInfo["ar_tour"]) : array();

                    }*/

                    $pl_id = is_array($item["accm"]) ? $item["accm"]["id"] : '';
                    $rT_id = is_array($item["roomType"]) ? $item["roomType"]["id"] : '';
                    $rCT_id = is_array($item["roomCat"]) ? $item["roomCat"]["id"] : '';
                    $pl_name = is_array($item["accm"]) ? $item["accm"]["name"] : $item["accm"];
                    $rT_name = is_array($item["roomType"]) ? $item["roomType"]["name"] : $item["roomType"];
                    $rCT_name = is_array($item["roomCat"]) ? $item["roomCat"]["name"] : $item["roomCat"];

                    $arTotalResult[] = array(
                        "operator_id" => $this->_operator_id,
                        "resultKey" => $item["priceKey"],
                        "night" => $item["night"],
                        "nightFormatted" => num2word($item["night"], array(GetMessage('NIGHT_'),GetMessage('NIGHT__'),GetMessage('NIGHT_N'))),
                        "nightFormattedList" => num2word($item["night"], array(GetMessage('NIGHT__'),GetMessage('NIGHT_N'),GetMessage('NIGHT_N'))),
                        "adults" => $this->_request["parameters"]["where"]["adults"],
                        "children" => $this->_request["parameters"]["where"]["childs"],
                        "tourType" => array(
                            "id" => $item["tourType"]["id"],
                            "originalName" => $item["tourType"]["name"],
                            "name" => $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]],
                            "viewName" => !empty($arInfo_["ar_tourTypeName"][$item["tourType"]["id"]]) ? $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]] : $item["tourType"]["name"],
                            "typeSearch" => !empty($arInfo_["ar_typeSearch"][$item["tourType"]["id"]]['UF_TYPE_SEARCH']) ? $arInfo_["ar_typeSearch"][$item["tourType"]["id"]]['UF_TYPE_SEARCH'] : "hotel"
                        ),
                        "tourDate" => array(
                            "dateFrom" => Utils::create($item["tourDate"]),
                            "dateTo" => Utils::dateModify($item["tourDate"], $item["night"] . ' day'), //$date->modify('+'.$item["night"].' day')->format('d.m.Y'),
                            "dateFromMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::create($item["tourDate"]), \CSite::GetDateFormat())),
                            "dateToMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp(Utils::dateModify($item["tourDate"], $item["night"] . ' day'), \CSite::GetDateFormat()))
                        ),
                        "cityFrom" => array(
                            "id" => $this->_request["parameters"]["where"]["cityFrom"],
                            "name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["where"]["cityFrom"]]["NAME"],
                            "from_name" => $arInfo_["ar_city_bx"][(int)$this->_request["parameters"]["where"]["cityFrom"]]["PROPERTIES"]["CN_NAME_CHEGO"]["VALUE"],
                        ),
                        "country" => array(
                            "id" => $this->_request["parameters"]["where"]["countryKey"],
                            "name" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["where"]["countryKey"]]["NAME"],
                            "link" => $arInfo_["ar_country_bx"][$this->_request["parameters"]["where"]["countryKey"]]["DETAIL_PAGE_URL"] ? $arInfo_["ar_country_bx"][$this->_request["parameters"]["where"]["countryKey"]]["DETAIL_PAGE_URL"] : ''
                        ),
                        "city" => array(
                            "id" => $item["city"]["id"],
                            "originalName" => $item["city"]["name"],
                            "name" => !empty($arInfo_["ar_city_bx"][$item["city"]["id"]]) ? $arInfo_["ar_city_bx"][$item["city"]["id"]]["NAME"] : '',
                            "link" => !empty($arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"]) && !empty($arInfo_["ar_city_bx"][$item["city"]["id"]]["CODE"]) ? $arInfo_["ar_country_bx"][$item["country"]["id"]]["DETAIL_PAGE_URL"] . $arInfo_["ar_city_bx"][$item["city"]["id"]]["CODE"] . "/" : "",
                            "viewName" => !empty($arInfo_["ar_city_bx"][$item["city"]["id"]]["NAME"]) ? $arInfo_["ar_city_bx"][$item["city"]["id"]]["NAME"] : $item["city"]["name"]
                        ),
                        "resort" => array(),
                        "hotel" => array(
                            "id" => $item["hotel"]["id"],
                            "mdId" => md5($this->_operator_id."_".$item["hotel"]["id"]),
                            "originalName" => $item["hotel"]["name"],
                            "id_bx" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel"][$item["hotel"]["id"]] : '',
                            "requestHotel" => !empty($arInfo_["ar_hotel"][$item["hotel"]["id"]]) && $arInfo_["ar_hotel"][$item["hotel"]["id"]] != 0 ? '{hotels: [' . (int)$arInfo_["ar_hotel"][$item["hotel"]["id"]] . ']}' : '{hotels: [' . (int)$item["hotel"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                            "name" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : '',
                            "star" => !empty($arInfo_["ar_star"][$item["hotel"]["star"]]) ? $arInfo_["ar_star"][$item["hotel"]["star"]] : $item["hotel"]["star"],
                            "image" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 222, "height" => 182)) : Settings::pathNoPhoto(),
                            "link" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["DETAIL_PAGE_URL"].$query : '',
                            "services" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"] : "",
                            "viewName" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["NAME"] : $item["hotel"]["name"],
                            "map" => !empty($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"]) ? $arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["PROPERTY_MAP_VALUE"] : '',
                            "previewText" => !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"]) ? substr2($arInfo_["ar_hotel_bx"][$item["hotel"]["id"]]["~PROPERTY_PREVIEW_TEXT_VALUE"]["TEXT"],100) : '',
                        ),
                        "tour" => array(
                            "id" => $item["tour"]["id"],
                            "mdId" => md5($this->_operator_id."_".$item["tour"]["id"]),
                            "originalName" => $item["tour"]["name"],
                            "id_bx" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['ID'] : '',
                            //"requestTour" => isset($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] != 0 ? '{tours: [' . (int)$arInfo_["ar_tour_bx"][$item["tour"]["id"]]["ID"] . ']}' : '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                            "requestTour" => '{tours: [' . (int)$item["tour"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                            "name" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : '',
                            "image" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 222, "height" => 182)) : Settings::pathNoPhoto(),
                            "link" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["DETAIL_PAGE_URL"] : '',
                            "viewName" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]) && !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"]) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]["NAME"] : $item["tour"]["name"],
                            "description" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['PREVIEW_TEXT']["TEXT"]) ? substr2($arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['PREVIEW_TEXT']["TEXT"], 150) : '',
                            "days" => !empty($arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['DAYS']) ? $arInfo_["ar_tour_bx"][$item["tour"]["id"]]['PROPERTIES']['DAYS'] : '',
                        ),
                        "placement" => array(
                            "id" => $pl_id,
                            "originalName" => $pl_name,
                            //"name" => $arInfo_["ar_placement"][$pl_id],
                            "name" => '',
                            //"viewName" => isset($arInfo_["ar_placement"][$pl_id]) && !empty($arInfo_["ar_placement"][$pl_id]) ? $arInfo_["ar_placement"][$pl_id] : $pl_name
                            "viewName" => $pl_name
                        ),
                        "roomType" => array(
                            "id" => $rT_id,
                            "originalName" => $rT_name,
                            //"name" => $arInfo_["ar_roomType"][$rT_id],
                            "name" => '',
                            //"viewName" => isset($arInfo_["ar_roomType"][$rT_id]) && !empty($arInfo_["ar_roomType"][$rT_id]) ? $arInfo_["ar_roomType"][$rT_id] : $rT_name
                            "viewName" => $rT_name
                        ),
                        "roomCatType" => array(
                            "id" => $rCT_id,
                            "originalName" => $rCT_name,
                            //"name" => $arInfo_["ar_roomCat"][$rCT_id],
                            "name" => '',
                            //"viewName" => isset($arInfo_["ar_roomCat"][$rCT_id]) && !empty($arInfo_["ar_roomCat"][$rCT_id]) ? $arInfo_["ar_roomCat"][$rCT_id] : $rCT_name
                            "viewName" => $rCT_name
                        ),
                        "food" => array(
                            "id" => $item["meal"]["id"],
                            "originalName" => $item["meal"]["name"],
                            "name" => !empty($arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"] : '',
                            "fullName" => !empty($arInfo_["ar_food_bx"][$item["meal"]["id"]]["PROPERTIES"]["NAME"]["VALUE"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["PROPERTIES"]["NAME"]["VALUE"] : "",
                            "desc" => !empty($arInfo_["ar_food_bx"][$item["meal"]["id"]]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"] : "",
                            "viewName" => !empty($arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"]) ? $arInfo_["ar_food_bx"][$item["meal"]["id"]]["NAME"] : $item["meal"]["name"]
                        ),
                        "quoteHotel" => $item["quoteHotel"],
                        "quoteAvia" => array(
                            "economy" => array(
                                "quoteAviaFrom" => $item["quoteAviaFrom"],
                                "quoteAviaTo" => $item["quoteAviaTo"],
                            ),
                            "business" => array(
                                "quoteAviaFrom" => '',
                                "quoteAviaTo" => '',
                            )
                        ),
                        "oilTaxes" => array(),
                        "visa" => array(),
                        "defCurrency" => $item["defaultRate"],
                        "prices" => $item["prices"][$item["defaultRate"]],
                        "price_for" => "за номер",
                        "priceCurrency" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResultLikeArray() : '',
                        "priceCurrency_" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\factory\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResult() : '',
                    );
                    $key = end(array_keys($arTotalResult));

                    $departure_city = $arTotalResult[$key]["cityFrom"]["from_name"] ? 'из ' . $arTotalResult[$key]["cityFrom"]["from_name"] : $arTotalResult[$key]["cityFrom"]["name"];
                    $departure_date = $arTotalResult[$key]["tourDate"]["dateFrom"] ? 'c ' . $arTotalResult[$key]["tourDate"]["dateFrom"] : $arTotalResult[$key]["tourDate"]["dateFrom"];
                    $arTotalResult[$key]["infoFormattedTour"] = array(
                        "departure" => implode(', ', array($departure_city, $departure_date)),
                        "accommodation" => implode(', ', array($arTotalResult[$key]["placement"]["viewName"], $arTotalResult[$key]["roomType"]["viewName"], $arTotalResult[$key]["roomCatType"]["viewName"])),
                        "food" => GetMessage('FOOD') . $arTotalResult[$key]["food"]["viewName"],
                    );


                    /*---*/

                    /*if(isset($arInfo_["ar_hotel_bx"]) && !empty($arInfo_["ar_hotel_bx"]) && isset($this->_request_search_form["coastline"]) && !empty($this->_request_search_form["coastline"])){

                        if(isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]) && isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"]) && !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"]) && in_array($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"], $this->_request_search_form["coastline"])) {

                            $arTotalResult[] = array(
                                "operator_id" => $this->_operator_id,
                                "resultKey" => $item["priceKey"],
                                "night" => $item["night"],
                                "nightFormatted" => num2word($item["night"], array(GetMessage('NIGHT_'),GetMessage('NIGHT__'),GetMessage('NIGHT_N'))),
                                "adults" => $this->_request["parameters"]["where"]["adults"],
                                "children" => $this->_request["parameters"]["where"]["childs"],
                                "tourType" => array(
                                    "id" => $item["tourType"]["id"],
                                    "originalName" => $item["tourType"]["name"],
                                ),
                                "tourDate" => array(
                                    "dateFrom" => Utils::create($item["tourDate"]),
                                    "dateTo" => Utils::dateModify($item["tourDate"], $item["night"] . ' day') //$date->modify('+'.$item["night"].' day')->format('d.m.Y'),
                                ),
                                "cityFrom" => array(
                                    "id" => $this->_request["parameters"]["where"]["cityFrom"],
                                ),
                                "country" => array(
                                    "id" => $this->_request["parameters"]["where"]["countryKey"],
                                ),
                                "city" => array(
                                    "id" => $item["city"]["id"],
                                    "originalName" => $item["city"]["name"],
                                ),
                                "resort" => array(),
                                "hotel" => array(
                                    "id" => $item["hotel"]["id"],
                                    "mdId" => md5($this->_operator_id."_".$item["hotel"]["id"]),
                                    "originalName" => $item["hotel"]["name"],
                                    "star" => $item["hotel"]["star"],
                                ),
                                "placement" => array(
                                    "id" => is_array($item["accm"]) ? $item["accm"]["id"] : '',
                                    "originalName" => is_array($item["accm"]) ? $item["accm"]["name"] : $item["accm"],
                                ),
                                "roomType" => array(
                                    "id" => is_array($item["roomType"]) ? $item["roomType"]["id"] : '',
                                    "originalName" => is_array($item["roomType"]) ? $item["roomType"]["name"] : $item["roomType"],
                                ),
                                "roomCatType" => array(
                                    "id" => is_array($item["roomCat"]) ? $item["roomCat"]["id"] : '',
                                    "originalName" => is_array($item["roomCat"]) ? $item["roomCat"]["name"] : $item["roomCat"],
                                ),
                                "food" => array(
                                    "id" => $item["meal"]["id"],
                                    "originalName" => $item["meal"]["name"],
                                ),
                                "quoteHotel" => $item["quoteHotel"],
                                "quoteAvia" => array(
                                    "economy" => array(
                                        "quoteAviaFrom" => $item["quoteAviaFrom"],
                                        "quoteAviaTo" => $item["quoteAviaTo"],
                                    ),
                                    "business" => array(
                                        "quoteAviaFrom" => '',
                                        "quoteAviaTo" => '',
                                    )
                                ),
                                "oilTaxes" => array(),
                                "visa" => array(),
                                "defCurrency" => $item["defaultRate"],
                                "prices" => $item["prices"][$item["defaultRate"]],
                                "price_for" => "за номер",
                                "priceCurrency" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResultLikeArray() : '',
                                "priceCurrency_" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResult() : '',
                            );
                            $key = end(array_keys($arTotalResult));

                        }

                    } elseif(!isset($this->_request_search_form["coastline"])) {

                        $arTotalResult[] = array(
                            "operator_id" => $this->_operator_id,
                            "resultKey" => $item["priceKey"],
                            "night" => $item["night"],
                            "nightFormatted" => num2word($item["night"], array(GetMessage('NIGHT_'),GetMessage('NIGHT__'),GetMessage('NIGHT_N'))),
                            "adults" => $this->_request["parameters"]["where"]["adults"],
                            "children" => $this->_request["parameters"]["where"]["childs"],
                            "tourType" => array(
                                "id" => $item["tourType"]["id"],
                                "originalName" => $item["tourType"]["name"],
                            ),
                            "tourDate" => array(
                                "dateFrom" => Utils::create($item["tourDate"]),
                                "dateTo" => Utils::dateModify($item["tourDate"], $item["night"] . ' day') //$date->modify('+'.$item["night"].' day')->format('d.m.Y'),
                            ),
                            "cityFrom" => array(
                                "id" => $this->_request["parameters"]["where"]["cityFrom"],
                            ),
                            "country" => array(
                                "id" => $this->_request["parameters"]["where"]["countryKey"],
                            ),
                            "city" => array(
                                "id" => $item["city"]["id"],
                                "originalName" => $item["city"]["name"],
                            ),
                            "resort" => array(),
                            "hotel" => array(
                                "id" => $item["hotel"]["id"],
                                "mdId" => md5($this->_operator_id."_".$item["hotel"]["id"]),
                                "originalName" => $item["hotel"]["name"],
                                "star" => $item["hotel"]["star"],
                            ),
                            "placement" => array(
                                "id" => is_array($item["accm"]) ? $item["accm"]["id"] : '',
                                "originalName" => is_array($item["accm"]) ? $item["accm"]["name"] : $item["accm"],
                            ),
                            "roomType" => array(
                                "id" => is_array($item["roomType"]) ? $item["roomType"]["id"] : '',
                                "originalName" => is_array($item["roomType"]) ? $item["roomType"]["name"] : $item["roomType"],
                            ),
                            "roomCatType" => array(
                                "id" => is_array($item["roomCat"]) ? $item["roomCat"]["id"] : '',
                                "originalName" => is_array($item["roomCat"]) ? $item["roomCat"]["name"] : $item["roomCat"],
                            ),
                            "food" => array(
                                "id" => $item["meal"]["id"],
                                "originalName" => $item["meal"]["name"],
                            ),
                            "quoteHotel" => $item["quoteHotel"],
                            "quoteAvia" => array(
                                "economy" => array(
                                    "quoteAviaFrom" => $item["quoteAviaFrom"],
                                    "quoteAviaTo" => $item["quoteAviaTo"],
                                ),
                                "business" => array(
                                    "quoteAviaFrom" => '',
                                    "quoteAviaTo" => '',
                                )
                            ),
                            "oilTaxes" => array(),
                            "visa" => array(),
                            "defCurrency" => $item["defaultRate"],
                            "prices" => $item["prices"][$item["defaultRate"]],
                            "price_for" => "за номер",
                            "priceCurrency" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResultLikeArray() : '',
                            "priceCurrency_" => (!empty($item["defaultRate"]) && $item["prices"][$item["defaultRate"]] > 0) ? \travelsoft\currency\Converter::getInstance()->convert($item["prices"][$item["defaultRate"]], $item["defaultRate"])->getResult() : '',
                        );
                        $key = end(array_keys($arTotalResult));

                    }*/

                }

            }

            //dm($arInfo_,false,true,false);

            /*if(!empty($arTotalResult)) {

                if (!empty($arInfo)) {
                    $arInfo_["ar_city"] = !empty($arInfo["ar_city"]) ? RelationCities::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_city"]), $this->_operator_id) : array();
                    $arInfo_["ar_country"] = !empty($arInfo["ar_country"]) ? RelationCountries::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_country"]), $this->_operator_id) : array();
                    //$arInfo_["ar_hotel"] = !empty($arInfo["ar_hotel"]) ? RelationHotels::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_hotel"]), $this->_operator_id) : array();
                    $arInfo_["ar_food"] = !empty($arInfo["ar_food"]) ? RelationFood::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_food"]), $this->_operator_id) : array();
                    $arInfo_["ar_star"] = !empty($arInfo["ar_stars"]) ? RelationStars::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_stars"]), $this->_operator_id) : array();
                    $arInfo_["ar_tourType"] = !empty($arInfo["ar_tourType"]) ? RelationTourTypes::getBxIdMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_tourType"]), $this->_operator_id) : array();
                    $arInfo_["ar_tourTypeName"] = !empty($arInfo_["ar_tourType"]) ? TourTypes::getElementNameMod($arInfo_["ar_tourType"]) : array();
                    $arInfo_["ar_placement"] = !empty($arInfo["ar_placement"]) ? Placement::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_placement"]), $this->_operator_id) : array();
                    $arInfo_["ar_roomType"] = !empty($arInfo["ar_roomType"]) ? RoomTypes::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_roomType"]), $this->_operator_id) : array();
                    $arInfo_["ar_roomCat"] = !empty($arInfo["ar_roomCat"]) ? RoomCatTypes::getElementNameMod(array("UF_OP_" . $this->_operator_id => $arInfo["ar_roomCat"]), $this->_operator_id) : array();

                    $arInfo_["ar_city_bx"] = !empty($arInfo_["ar_city"]) ? Cities::get(array("filter" => array("ID" => $arInfo_["ar_city"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();
                    $arInfo_["ar_country_bx"] = !empty($arInfo_["ar_country"]) ? Countries::get(array("filter" => array("ID" => $arInfo_["ar_country"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();
                    //$arInfo_["ar_hotel_bx"] = !empty($arInfo_["ar_hotel"]) ? Hotels::get(array("filter" => array("ID" => $arInfo_["ar_hotel"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE", "PROPERTY_MAP"))) : array();
                    $arInfo_["ar_food_bx"] = !empty($arInfo_["ar_food"]) ? Food::get(array("filter" => array("ID" => $arInfo_["ar_food"]), "select" => array("NAME", "ID", "DETAIL_PAGE_URL", "CODE"))) : array();

                }

                foreach ($arTotalResult as $key => $item) {

                    $arTotalResult[$key] = array_merge($arTotalResult[$key], array(
                        "tourDate" => array_merge($item["tourDate"], array(
                            "dateFromMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp($arTotalResult[$key]["tourDate"]["dateFrom"], \CSite::GetDateFormat())),
                            "dateToMod" => \CIBlockFormatProperties::DateFormat("j M", MakeTimeStamp($arTotalResult[$key]["tourDate"]["dateTo"], \CSite::GetDateFormat()))
                        )),
                        "tourType" => array_merge($item["tourType"], array(
                            "name" => $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]],
                            "viewName" => !empty($arInfo_["ar_tourTypeName"][$item["tourType"]["id"]]) ? $arInfo_["ar_tourTypeName"][$item["tourType"]["id"]] : $item["tourType"]["originalName"]
                        )),
                        "cityFrom" => array_merge($item["cityFrom"], array(
                            "name" => $arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["cityFrom"]["id"]]]["NAME"],
                            "from_name" => $arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["cityFrom"]["id"]]]["PROPERTIES"]["CN_NAME_CHEGO"]["VALUE"],
                        )),
                        "country" => array_merge($item["country"], array(
                            "name" => $arInfo_["ar_country_bx"][$arInfo_["ar_country"][$item["country"]["id"]]]["NAME"],
                            "link" => $arInfo_["ar_country_bx"][$arInfo_["ar_country"][$item["country"]["id"]]]["DETAIL_PAGE_URL"] ? $arInfo_["ar_country_bx"][$arInfo_["ar_country"][$item["country"]["id"]]]["DETAIL_PAGE_URL"] : ''
                        )),
                        "city" => array_merge($item["city"], array(
                            "name" => isset($arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]) ? $arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["NAME"] : '',
                            "link" => !empty($arInfo_["ar_country_bx"][$arInfo_["ar_country"][$item["country"]["id"]]]["DETAIL_PAGE_URL"]) && isset($arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["CODE"]) && !empty($arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["CODE"]) ? $arInfo_["ar_country_bx"][$arInfo_["ar_country"][$item["country"]["id"]]]["DETAIL_PAGE_URL"] . $arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["CODE"] . "/" : "",
                            "viewName" => isset($arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]) && !empty($arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["NAME"]) ? $arInfo_["ar_city_bx"][$arInfo_["ar_city"][$item["city"]["id"]]]["NAME"] : $item["city"]["originalName"]
                        )),
                        "hotel" => array_merge($item["hotel"], array(
                            "id_bx" => isset($arInfo_["ar_hotel"][$item["hotel"]["id"]]) ? $arInfo_["ar_hotel"][$item["hotel"]["id"]] : '',
                            "requestHotel" => isset($arInfo_["ar_hotel"][$item["hotel"]["id"]]) && $arInfo_["ar_hotel"][$item["hotel"]["id"]] != 0 ? '{hotels: [' . (int)$arInfo_["ar_hotel"][$item["hotel"]["id"]] . ']}' : '{hotels: [' . (int)$item["hotel"]["id"] . '], operators: [' . (int)$this->_operator_id . ']}',
                            "name" => $arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["NAME"],
                            "star" => isset($arInfo_["ar_star"][$item["hotel"]["star"]]) ? $arInfo_["ar_star"][$item["hotel"]["star"]] : $item["hotel"]["star"],
                            "image" => !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["PICTURES"]["VALUE"]) ? (string)Utils::getPath($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width" => 222, "height" => 182)) : Settings::pathNoPhoto(),
                            "link" => isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]) ? $arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["DETAIL_PAGE_URL"] : '',
                            "services" => isset($arInfo_["ar_hotel"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"]) ? $arInfo_["ar_hotel"][$item["hotel"]["id"]]["PROPERTIES"]["SERVICES"]["VALUE"] : "",
                            "viewName" => isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]) && !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["NAME"]) ? $arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["NAME"] : $item["hotel"]["originalName"],
                            "map" => isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]) && !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTY_MAP_VALUE"]) ? $arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTY_MAP_VALUE"] : '',
                            "coastline" => isset($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"]) && !empty($arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"]) ? $arCoastlineBX[$arInfo_["ar_hotel_bx"][$arInfo_["ar_hotel"][$item["hotel"]["id"]]]["PROPERTIES"]["COASTLINE_"]["VALUE"]] : ''
                        )),
                        "placement" => array_merge($item["placement"], array(
                            "name" => $arInfo_["ar_placement"][$item["placement"]["id"]],
                            "viewName" => isset($arInfo_["ar_placement"][$item["placement"]["id"]]) && !empty($arInfo_["ar_placement"][$item["placement"]["id"]]) ? $arInfo_["ar_placement"][$item["placement"]["id"]] : $item["placement"]["originalName"]
                        )),
                        "roomType" => array_merge($item["roomType"], array(
                            "name" => $arInfo_["ar_roomType"][$item["roomType"]["id"]],
                            "viewName" => isset($arInfo_["ar_roomType"][$item["roomType"]["id"]]) && !empty($arInfo_["ar_roomType"][$item["roomType"]["id"]]) ? $arInfo_["ar_roomType"][$item["roomType"]["id"]] : $item["roomType"]["originalName"]
                        )),
                        "roomCatType" => array_merge($item["roomCatType"], array(
                            "name" => $arInfo_["ar_roomCat"][$item["roomCatType"]["id"]],
                            "viewName" => isset($arInfo_["ar_roomCat"][$item["roomCatType"]["id"]]) && !empty($arInfo_["ar_roomCat"][$item["roomCatType"]["id"]]) ? $arInfo_["ar_roomCat"][$item["roomCatType"]["id"]] : $item["roomCatType"]["originalName"]
                        )),
                        "food" => array_merge($item["food"], array(
                            "name" => $arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["NAME"],
                            "fullName" => isset($arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["PROPERTIES"]["NAME"]["VALUE"]) ? $arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["PROPERTIES"]["NAME"]["VALUE"] : "",
                            "desc" => isset($arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"]) ? $arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"] : "",
                            "viewName" => isset($arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]) && !empty($arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["NAME"]) ? $arInfo_["ar_food_bx"][$arInfo_["ar_food"][$item["food"]["id"]]]["NAME"] : $item["food"]["originalName"]
                        )),
                    ));
                    $departure_city = $arTotalResult[$key]["cityFrom"]["from_name"] ? 'из ' . $arTotalResult[$key]["cityFrom"]["from_name"] : $arTotalResult[$key]["cityFrom"]["name"];
                    $departure_date = $arTotalResult[$key]["tourDate"]["dateFrom"] ? 'c ' . $arTotalResult[$key]["tourDate"]["dateFrom"] : $arTotalResult[$key]["tourDate"]["dateFrom"];
                    $arTotalResult[$key]["infoFormattedTour"] = array(
                        "departure" => implode(', ', array($departure_city, $departure_date)),
                        "accommodation" => implode(', ', array($arTotalResult[$key]["placement"]["viewName"], $arTotalResult[$key]["roomType"]["viewName"], $arTotalResult[$key]["roomCatType"]["viewName"])),
                        "food" => GetMessage('FOOD') . $arTotalResult[$key]["food"]["viewName"],
                    );

                }
            }*/

        }

        return $arTotalResult;

    }

    public function searchPropValue ($values, $propTable, $propName, $operators): array {

        $arValue = array();
        $operators_ = $operators || null;

        $arValue = call_user_func_array(array($propTable, "getPropValue"), array(array($propName => $values), $this->_operator_id));
        if(empty($arValue)){

            if(!$operators_ && count($operators_) != 1 && !in_array($this->_operator_id,$operators_)){
                throw new \Exception("Нет значения поиска у этого оператора");
            }

        }

        return $arValue;

    }

}