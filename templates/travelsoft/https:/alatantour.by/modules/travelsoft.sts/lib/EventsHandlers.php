<?php

namespace travelsoft\sts;

use travelsoft\sts\pages\Settings as STSPageSettings;
use travelsoft\sts\Utils;
use travelsoft\sts\_interfaces\Operator;

/**
 * Класс методов обработки событий
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class EventsHandlers {


    /**
     * Добавляет пункт меню Поиск туров в global меню админ. части
     * @global type $USER
     * @param type $aGlobalMenu
     */
    public static function addGlobalAdminMenuItem(&$arGlobalMenu) {

        global $USER;

        //if (sts\Utils::access()) {
        //if ($USER->GetID() == 1) {

        $currentUserGroups = $USER->GetUserGroupArray();

        $arAllMenuItems = array(
            STSPageSettings::DIRECTIONS_URL => array(
                "text" => "Направления. Страны",
                "url" => STSPageSettings::DIRECTIONS_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::DETAIL_DIRECTIONS_URL),
                "title" => "Направления. Страны",
            ),
            STSPageSettings::DIRECTIONS_RESORTS_URL => array(
                "text" => "Направления. Курорты",
                "url" => STSPageSettings::DIRECTIONS_RESORTS_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::DETAIL_DIRECTIONS_RESORTS_URL),
                "title" => "Направления. Курорты",
            ),
            STSPageSettings::TABLES_LIST_URL => array(
                "text" => "Соответствия",
                "url" => STSPageSettings::TABLES_LIST_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::TABLES_LIST_URL),
                "title" => "Соответствия",
            ),
            STSPageSettings::UPDATE_HOTEL_TABLE_URL => array(
                "text" => "Актуализации таблицы отелей",
                "url" => STSPageSettings::UPDATE_HOTEL_TABLE_URL . "?lang=" . LANGUAGE_ID,
                /*"more_url" => array(STSPageSettings::UPDATE_HOTEL_TABLE_URL),*/
                "title" => "Актцализации таблицы отелей",
            )
        );

        $arGlobalMenu["global_menu_travelsoft_sts"] = array(
            "menu_id" => "travelsoft_sts",
            "text" => "Поиск туров",
            "title" => "Поиск туров",
            "sort" => 500,
            "items_id" => "global_menu_travelsoft_sts",
            "help_section" => "travelsoft_sts",
            "items" => array_values($arAllMenuItems)
        );
        //}
    }


    /**
     * @param Bitrix\Main\Entity\Event $event
     */
    public static function onAfterOperatorAdd($event) {

        global $APPLICATION;

        $arParameters = $event->getParameters();
        $oUserTypeEntity = new \CUserTypeEntity();

        $info = array();

        $sort = 100;

        $arTableHL = array("relationcities","relationcountries","relationresorts","relationhotels","relationtourtypes","relationtours","relationplacement","relationfood","roomcattypes","roomtypes","relationstars");
        foreach ($arTableHL as $hl){

            $class = $hl."StoreId";

            $highBlockID = \travelsoft\sts\Settings::$class();

            $aUserField = array(
                'ENTITY_ID' => 'HLBLOCK_' . $highBlockID,
                'FIELD_NAME' => Operator::$fieldCodeName.$arParameters['id'],
                'USER_TYPE_ID' => 'string',
                'SORT' => $sort,
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(),
                'EDIT_FORM_LABEL' => array(
                    'ru' => $arParameters["fields"]["UF_NAME"],
                ),

            );

            if ($idUserTypeProp = $oUserTypeEntity->Add($aUserField)) {
                $info[] = self::oGetMessage('USER_TYPE_ADDED', array(
                    '#FIELD_NAME#' => $aUserField['FIELD_NAME'],
                    '#ENTITY_ID#' => $aUserField['ENTITY_ID'],
                ));
            } else {
                if (($ex = $APPLICATION->GetException())) {
                    $info[] = self::oGetMessage('USER_TYPE_ADDED_ERROR', array(
                        '#FIELD_NAME#' => $aUserField['FIELD_NAME'],
                        '#ENTITY_ID#' => $aUserField['ENTITY_ID'],
                        '#ERROR#' => $ex->GetString(),
                    ));
                }
            }


        }

        //TODO: дописать запись массива $info в log

        $uf_code_operator = Operator::$fieldCodeName;
        $arOperatorProp = stores\Operators::update($arParameters['id'],Array("UF_CODE" => $uf_code_operator.$arParameters['id']));

    }

    /**
     * @param Bitrix\Main\Entity\Event $event
     */
    public static function onAfterOperatorDelete($event) {

        $arParameters = $event->getParameters();
        $ID = $arParameters['id']['ID'];

        $info = array();

        $oUserTypeEntity = new \CUserTypeEntity();

        $arTableHL = array("relationcities","relationcountries","relationresorts","relationhotels","relationtourtypes","relationtours","relationplacement","relationfood","roomcattypes","roomtypes","relationstars");
        foreach ($arTableHL as $hl) {

            $class = $hl . "StoreId";

            $highBlockID = \travelsoft\sts\Settings::$class();

            $aUserField = array(
                'ENTITY_ID' => 'HLBLOCK_' . $highBlockID,
                'FIELD_NAME' => Operator::$fieldCodeName . $ID
            );

            $resProperty = \CUserTypeEntity::GetList(
                array(),
                array('ENTITY_ID' => $aUserField['ENTITY_ID'], 'FIELD_NAME' => $aUserField['FIELD_NAME'])
            );

            if ($aUserHasField = $resProperty->Fetch()) {
                $idUserTypeProp = $aUserHasField['ID'];

                $oUserTypeEntity->Delete( $idUserTypeProp );
                $info[] = self::oGetMessage('USER_TYPE_DELETED', array(
                    '#FIELD_NAME#' => $aUserHasField['FIELD_NAME'],
                    '#ENTITY_ID#' => $aUserHasField['ENTITY_ID'],
                ));
            }

        }

        //TODO: дописать запись массива $info в log

    }


    function oGetMessage($key, $fields)
    {
        $messages = array(
            'USER_TYPE_ADDED' => 'Пользовательское свойство #FIELD_NAME# [#ENTITY_ID#] успешно добавлено',
            'USER_TYPE_ADDED_ERROR' => 'Ошибка добавления пользовательского свойства #FIELD_NAME# [#ENTITY_ID#]: #ERROR#',
            'USER_TYPE_DELETED' => 'Пользовательское свойство #FIELD_NAME# [#ENTITY_ID#] успешно удалено'
        );

        return isset($messages[$key])
            ? str_replace(array_keys($fields), array_values($fields), $messages[$key])
            : '';
    }

}
