<?php

namespace travelsoft\sts\api;

use travelsoft\sts\Utils;


class TezTour
{

    public static function sendRequest(array $request): string {

        $url = $request["url"];
        if(isset($request["query"])){
            $url .= $request["query"];
        }

        //dm($url);

        return file_get_contents(
            $url,
            false
        );

    }

    /**
     * Авторизация
     * @param string $login
     * @param string $password
     */
    public static function authData (string $login, string $password) {

        $url = 'http://xml.tez-tour.com/xmlgate/auth_data.jsp?j_login_request=1&j_login='.$login.'&j_passwd='.$password;

        $result = self::sendRequest(array(
            "url" => $url
        ));

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $result_ = Utils::jsonDecode($json, true);

        return !empty($result_) && isset($result_["sessionId"]) && !empty($result_["sessionId"]) ? $result_["sessionId"] : '';

    }

    /**
     * Справочник по странам
     * @param array $parameters
     * @return json
     */
    public static function catalogCountries (array $parameters) {

        self::sendRequest(array(
            "url" => $parameters["address"]
        ));

    }

    /**
     * Список перелетов
     * @param array $parameters
     */
    public static function getDates (array $parameters) {

        $parameters["url"] = 'https://search.tez-tour.com/tariffsearch/getFlightDeparture';

        $parameters["query"] = '?'.http_build_query($parameters["parameters"]);

        return self::sendRequest($parameters);

    }


}