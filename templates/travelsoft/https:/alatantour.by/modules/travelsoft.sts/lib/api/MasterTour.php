<?php

namespace travelsoft\sts\api;

use travelsoft\sts\Utils;


class MasterTour
{

    public static function sendRequest(array $request, array $tourTypes): string {

        $metod = 'SearchTours';

        if(!empty($tourTypes)) {
            $type = current($tourTypes);

            if($type == 'tour') {
                $metod = 'SearchTourPrice';
            }

        }

        /*dm(Utils::jsonEncode(
            array(
                "jsonrpc" => "2.0",
                "method"  => $metod,
                "params"  =>  $request["parameters"],
                "id" => rand()
            )
        ),false,true,false);*/

        return file_get_contents(
            $request["address"],
            false,
            stream_context_create(
                array(
                    'ssl' => array("verify_peer" => false),
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => Utils::jsonEncode(
                            array(
                                "jsonrpc" => "2.0",
                                "method"  => $metod,
                                "params"  =>  $request["parameters"],
                                "id" => rand()
                            )
                        ),
                    ),
                )
            )
        );

    }

    public static function getDates(array $request): string {

        return file_get_contents(
            $request["address"],
            false,
            stream_context_create(
                array(
                    'ssl' => array("verify_peer" => false),
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => Utils::jsonEncode(
                            array(
                                "jsonrpc" => "2.0",
                                "method"  => "GetDatesForFiltr",
                                "params"  =>  array(),
                                "id" => rand()
                            )
                        ),
                    ),
                )
            )
        );

    }

}