<?php

namespace travelsoft\sts\adapters;

use travelsoft\sts\Settings;

\Bitrix\Main\Loader::includeModule("iblock");

/**
 * Класс адаптер для bitrix iblock
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
abstract class Iblock {

    /**
     * @var string
     */
    protected static $storeName = null;

    /**
     * Возвращает полученные данные из хранилища в виде массива
     * @param array $query
     * @param callable $callback
     * @return array
     */
    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {

        /*static $data;

        if ($data[md5($query)]) {

            return $data[md5(serialize($query))];

        } else {*/

            if ($query['filter']) {

                $arFilter = $query["filter"];
            }

            $arFilter["IBLOCK_ID"] = self::getStoreId();

            if ($query['order']) {

                $arOrder = $query['order'];
            }

            if ($query['select']) {

                $arSelect = $query['select'];
            }

            if ($query['nav']) {

                $arNav = $query['nav'];
            }

            $dbList = \CIBlockElement::GetList($arOrder, $arFilter, null, $arNav, $arSelect);

            if (!$likeArray) {

                return $dbList;
            }

            $result = array();
            if ($callback) {

                while ($dbElement = $dbList->GetNextElement()) {

                    $arFields = $dbElement->GetFields();
                    if ($arFields["ID"] > 0) {

                        $arProperties = $dbElement->GetProperties();
                        $callback($arFields, $arProperties);
                        $result[$arFields["ID"]] = $arFields;
                        $result[$arFields["ID"]]["PROPERTIES"] = $arProperties;
                    }
                }
            } else {

                while ($dbElement = $dbList->GetNextElement()) {

                    $arFields = $dbElement->GetFields();
                    if ($arFields["ID"] > 0) {

                        $arProperties = $dbElement->GetProperties();
                        $result[$arFields["ID"]] = $arFields;
                        $result[$arFields["ID"]]["PROPERTIES"] = $arProperties;
                    }
                }
            }

            /*$data[md5(serialize($query))] = $result;
        }*/

        return (array) $result;
    }

    /**
     * Обновление записи по id
     * @param int $id
     * @param array $arUpdate
     * @return boolean
     */
    public static function update(int $id, array $arUpdate): bool {

        $ob = new \CIBlockElement;
        return boolval($ob->Update($id, $arUpdate));
    }

    /**
     * Добавляет запись в хранилище
     * @param array $arSave
     * @return int
     */
    public static function add(array $arSave): int {

        $ob = new \CIBlockElement;
        $arSave['IBLOCK_ID'] = self::getStoreId();
        return (int) $ob->Add($arSave);
    }

    /**
     * Удаляет запись из хранилища
     * @param int $id
     */
    public static function delete(int $id): bool {

        $ob = new \CIBlockElement;
        return boolval($ob->Delete($id));
    }

    /**
     * Возвращает поля записи таблицы по id
     * @param int $id
     * @return array
     */
    public static function getById(int $id): array {

        $class = get_called_class();
        
        $result = current($class::get(array("filter" => array("ID" => $id))));
        if (is_array($result) && !empty($result)) {
            
            return $result;
        } else {
            
            return array();
        }
    }

    public static function getBxIdAr(array $query): array {

        $arData = self::get(array("filter" => $query,"select" => array("ID")));

        $arData_ = array();

        if(!empty($arData)){
            foreach ($arData as $val){

                $arData_[] = $val["ID"];

            }
        }

        return $arData_;

    }

    public static function getElementName(int $id): string {

        $arDate = current(self::get(array("filter" => array("ID" => $id), "select" => array("NAME", "ID"))));
        return (string)$arDate["NAME"];

    }

    public static function getElementNamePredlog(int $id): string {

        $arDate = current(self::get(array("filter" => array("ID" => $id), "select" => array("PROPERTY_CN_NAME_CHEGO", "ID"))));
        return (string)$arDate["PROPERTY_CN_NAME_CHEGO_VALUE"];

    }

    public static function getElementNameMod(array $id): array {

        $arDate = self::get(array("filter" => array("ID" => $id), "select" => array("NAME", "ID")));

        $arData_ = array();
        foreach ($arDate as $key=>$data){
            $arData_[$key] = $data["NAME"];
        }
        return (array)$arData_;

    }

    /**
     * @return int
     */
    protected static function getStoreId(): int {

        $class = get_called_class();
        $tableId = $class::$storeName . "StoreId";
        return (int) Settings::$tableId();
    }

}
