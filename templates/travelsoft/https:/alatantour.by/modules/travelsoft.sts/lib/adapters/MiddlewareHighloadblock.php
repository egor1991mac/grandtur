<?php

namespace travelsoft\sts\adapters;

/**
 * Middleware для bitrix highloadblock
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
abstract class MiddlewareHighloadblock extends Highloadblock {

    public static function getBxId(array $query): int {

        $arData = current(self::get(array("filter" => $query,"select" => array("UF_BX_ID"))));

        return (int)$arData["UF_BX_ID"];

    }

    public static function getBxIdMod(array $query, int $operator_id): array {

        $arData = self::get(array("filter" => $query,"select" => array("UF_BX_ID", "UF_OP_".$operator_id, "ID")));

        //dm($arData, false, true, false);
        $arData_ = array();
        foreach ($arData as $data){
            $arData_[$data["UF_OP_".$operator_id]] = $data["UF_BX_ID"];
        }

        return (array)$arData_;

    }

    public static function getPropValue(array $query, int $operator_id) {

        $arValue = array();
        $prop = \travelsoft\sts\_interfaces\Operator::$fieldCodeName;
        //$arData = current(self::get(array("filter" => $query,"select" => array($prop.$operator_id))));
        $arData = self::get(array("filter" => $query,"select" => array('ID', $prop.$operator_id)));

        foreach ($arData as $value) {
            if($value["UF_OP_".$operator_id]) {
                $arValue[] = $value["UF_OP_".$operator_id];
            }
        }

        return $arValue;


    }

}
