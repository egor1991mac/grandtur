<?php

namespace travelsoft\sts;

use Bitrix\Main\Config\Option;

/**
 * Класс настроек модуля поискс туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Settings {

    const SECOND_IN_DAY = 86400;

    /**
     * Возвращает id таблицы операторов
     * @return int
     */
    public static function operatorsStoreId(): int {

        return (int) self::get("OPERATORS_HL");
    }

    /**
     * Возвращает id таблицы городв
     * @return int
     */
    public static function relationcitiesStoreId(): int {

        return (int) self::get("CITIES_HL");
    }

    /**
     * Возвращает id инфоблка городв
     * @return int
     */
    public static function citiesStoreId(): int {

        return (int) self::get("CITIES_IB");
    }

    /**
     * Возвращает id инфоблка регионов
     * @return int
     */
    public static function regionsStoreId(): int {

        return (int) self::get("REGIONS_IB");
    }

    /**
     * Возвращает id таблицы стран
     * @return int
     */
    public static function relationcountriesStoreId(): int {

        return (int) self::get("COUNTRIES_HL");
    }

    /**
     * Возвращает id инфоблока стран
     * @return int
     */
    public static function countriesStoreId(): int {

        return (int) self::get("COUNTRIES_IB");
    }

    /**
     * Возвращает id таблицы курортов
     * @return int
     */
    public static function relationresortsStoreId(): int {

        return (int) self::get("RESORTS_HL");
    }

    /**
     * Возвращает id инфоблока курортов
     * @return int
     */
    public static function resortsStoreId(): int {

        return (int) self::get("RESORTS_IB");
    }

    /**
     * Возвращает id таблицы направлений
     * @return int
     */
    public static function relationdirectionsStoreId(): int {

        return (int) self::get("DIRECTIONS_HL");
    }

    /**
     * Возвращает id таблицы направлений по курортам
     * @return int
     */
    public static function relationdirectionsresortsStoreId(): int {

        return (int) self::get("DIRECTIONS_RESORTS_HL");
    }

    /**
     * Возвращает id таблицы отелей
     * @return int
     */
    public static function relationhotelsStoreId(): int {

        return (int) self::get("HOTELS_HL");
    }

    /**
     * Возвращает id инфоблока отелей
     * @return int
     */
    public static function hotelsStoreId(): int {

        return (int) self::get("HOTELS_IB");
    }

    /**
     * Возвращает id таблицы звездности
     * @return int
     */
    public static function relationstarsStoreId(): int {

        return (int) self::get("STARS_HL");
    }

    /**
     * Возвращает id инфоблока звездности
     * @return int
     */
    public static function starsStoreId(): int {

        return (int) self::get("STARS_IB");
    }

    /**
     * Возвращает id таблицы питания
     * @return int
     */
    public static function relationfoodStoreId(): int {

        return (int) self::get("FOOD_HL");
    }

    /**
     * Возвращает id инфоблока питания
     * @return int
     */
    public static function foodStoreId(): int {

        return (int) self::get("FOOD_IB");
    }

    /**
     * Возвращает id таблицы типов туров
     * @return int
     */
    public static function relationtourtypesStoreId(): int {

        return (int) self::get("TOURTYPES_HL");
    }

    /**
     * Возвращает id инфоблока типов туров
     * @return int
     */
    public static function tourtypesStoreId(): int {

        return (int) self::get("TOURTYPES_IB");
    }

    /**
     * Возвращает id таблицы типов номеров
     * @return int
     */
    public static function roomtypesStoreId(): int {

        return (int) self::get("ROOMTYPES_HL");
    }

    /**
     * Возвращает id таблицы типов категорий номеров
     * @return int
     */
    public static function roomcattypesStoreId(): int {

        return (int) self::get("ROOMCATTYPES_HL");
    }

    /**
     * Возвращает id таблицы вариантов размещений
     * @return int
     */
    public static function relationplacementStoreId(): int {

        return (int) self::get("PLACEMENT_HL");
    }

    /**
     * Возвращает id инфоблока размещений
     * @return int
     */
    public static function placementStoreId(): int {

        return (int) self::get("PLACEMENT_IB");
    }

    /**
     * Возвращает id таблицы дат
     * @return int
     */
    public static function datesStoreId(): int {

        return (int) self::get("DATES_HL");
    }

    /**
     * Возвращает путь к no photo
     * @return string
     */
    public static function pathNoPhoto(): string {

        return (string) self::get("PATH_NOPHOTO");
    }

    /**
     * Возвращает id города вылета по-умолчанию
     * @return int
     */
    public static function getDefaultCityFrom(): int {

        return (int) self::get("CITYFROM_ID");
    }

    /**
     * Возвращает id страны прилета по-умолчанию
     * @return int
     */
    public static function getDefaultCountry(): int {

        return (int) self::get("COUNTRY_ID");
    }

    /**
     * Возвращает id инфоблока туров
     * @return int
     */
    public static function toursStoreId(): int {

        return (int) self::get("TOURS_IB");
    }

    /**
     * Возвращает id таблицы туров
     * @return int
     */
    public static function relationtoursStoreId(): int {

        return (int) self::get("TOURS_HL");
    }

    /**
     * Возвращает дату начала поиска туров по-умолчанию
     * @return array array("dateFrom" => "d.m.Y", "dateTo" => "d.m.Y")
     */
    public static function getDefaultSearchDate(): array {

        $time = time();
        $arData = unserialize(self::get("DATE_RANGE"));
        $timestampFrom = mktime(0, 0, 0, date("m", $time), date("d", $time), date("Y", $time)) + self::SECOND_IN_DAY * $arData[0];
        $timestampTo = $timestampFrom + self::SECOND_IN_DAY * $arData[1];

        return array(
            "dateFrom" => date('d.m.Y', $timestampFrom),
            "dateTo" => date('d.m.Y', $timestampTo)
        );
    }

    /**
     * Возвращает период количества ночей поиска туров по-умолчанию
     * @return array array("nightFrom" => int, "nightTo" => int)
     */
    public static function getDefaultSearchNight(): array {

        $arData = unserialize(self::get("NIGHT_RANGE"));

        return array(
            "nightFrom" => (int)$arData[0],
            "nightTo" => (int)$arData[1]
        );
    }

    /**
     * Возвращает количество взрослых по-умолчанию
     * @return int
     */
    public static function getDefaultAdults(): int {

        return (int) self::get("ADULT");
    }

    /**
     * Возвращает количество детей по-умолчанию
     * @return int
     */
    public static function getDefaultChildren(): int {

        return (int) self::get("CHILD");
    }

    /**
     * @param string $name
     * @return string
     */
    public static function get(string $name): string {

        return (string) Option::get("travelsoft.sts", $name);
    }

}
