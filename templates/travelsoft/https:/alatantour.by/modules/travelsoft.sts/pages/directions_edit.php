<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

Bitrix\Main\Loader::includeModule("travelsoft.sts");
Bitrix\Main\Loader::includeModule('highloadblock');
Bitrix\Main\Loader::includeModule('iblock');

use travelsoft\sts\stores\RelationDirections;
?>
<?
$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."css/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."js/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."js/select2.full.min.js'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?
$sTableID = \travelsoft\sts\pages\Settings::DIRECTIONS_TABLE_ID;

$arResultCitiesIB = \travelsoft\sts\stores\Cities::get(array("select"=>array("ID","NAME")));
$arResultCountriesIB = \travelsoft\sts\stores\Countries::get(array("select"=>array("ID","NAME")));

$arResultItem = array();

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Создание направления';
    if ($ID > 0) {

        $arResultItem = \travelsoft\sts\stores\RelationDirections::getById($ID);

        if (!$arResultItem['ID']) {

            throw new Exception('Элемент направления с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование направления #' . $arResultItem['ID'];
    }

    $APPLICATION->SetTitle($title);

    $url = \travelsoft\sts\pages\Settings::DIRECTIONS_URL . '?lang=' . LANGUAGE_ID;

    global $USER_FIELD_MANAGER;

    if (strlen($_POST['CANCEL']) > 0) {

        LocalRedirect($url);
    }

    $arOperators = \travelsoft\sts\Utils::getOperators_();
    $arOperators_ = array();

    if(!empty($arOperators)) {
        foreach ($arOperators as $operator){
            $arOperators_[$operator["UF_CODE"]] = $operator["UF_NAME"];
        }
    }

    $arErrors = array();

    if (\travelsoft\sts\Utils::isEditFormRequest()) {

        $data = array();

        $arCountriesBox = array();

        $USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_' . \travelsoft\sts\pages\Settings::directionsStoreId(), $data);

        if ($data['UF_CITYFROM_ID'] <= 0) {

            $arErrors[] = 'Не указан город отправления';
            $data['UF_CITYFROM_BX_ID'] = array();

        } else {

            $data['UF_CITYFROM_BX_ID'] = \travelsoft\sts\stores\RelationCities::getBxId(array("ID"=>$data['UF_CITYFROM_ID']));
            if(empty($data['UF_CITYFROM_BX_ID'])) {
                $arErrors[] = 'Системная ошибка. Не указаны города отправления (ID Битрикс).';
            }

        }

        if(!empty($arOperators)) {
            foreach ($arOperators as $operator){

                if(!empty($data[$operator["UF_CODE"]])){
                    $arCountriesBox = array_merge($arCountriesBox, $data[$operator["UF_CODE"]]);
                }

            }
        }

        if(!empty($arCountriesBox)){
            $arCountriesBox = array_unique($arCountriesBox, SORT_NUMERIC);
            $data['UF_COUNTRY_ID'] = $arCountriesBox;
        }

        if (empty($data['UF_COUNTRY_ID'])) {

            $arErrors[] = 'Должна быть указана хотябы одна страна прибытия';
            $data['UF_COUNTRY_BX_ID'] = array();

        } else {

            $data['UF_COUNTRY_BX_ID'] = \travelsoft\sts\stores\RelationCountries::getBxIdArray(array("ID"=>$data['UF_COUNTRY_ID']));
            if(empty($data['UF_COUNTRY_BX_ID'])) {
                $arErrors[] = 'Системная ошибка. Не указаны страны прибытия (ID Битрикс).';
            }

        }

        if (empty($arErrors)) {

            if ($_REQUEST['ID'] > 0) {

                $ID = intval($_REQUEST['ID']);
                $result = \travelsoft\sts\stores\RelationDirections::update($ID, $data);

            } else {

                $result = \travelsoft\sts\stores\RelationDirections::add($data);

            }

            if ($result) {

                LocalRedirect($url);
            }
        }
    }


    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arErrors)) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arErrors),
            "TYPE" => "ERROR"
        ));
    }

    $contentDirectionsFieldsForm = '';
    $arDirectionsFields = $USER_FIELD_MANAGER->getUserFieldsWithReadyData('HLBLOCK_' . \travelsoft\sts\pages\Settings::directionsStoreId(), $arResultItem, LANGUAGE_ID);

    if(!empty($arOperators)){
        $arCountriesHL = \travelsoft\sts\stores\RelationCountries::get(array('select' => array('ID', 'UF_BX_ID')));
    }


    foreach ($arDirectionsFields as $arDirectionsField) {

        if (key_exists($arDirectionsField['FIELD_NAME'], $_POST)) {

            $arDirectionsField['VALUE'] = $_POST[$arDirectionsField['FIELD_NAME']];
        }

        switch ($arDirectionsField['FIELD_NAME']) {

            case 'UF_CITYFROM_ID':

                $option = '';

                $arCitiesHL = \travelsoft\sts\stores\RelationCities::get(array('filter' => array("UF_CITYFROM"=>1),'select' => array('ID', 'UF_BX_ID', 'UF_CITYFROM')));

                $arDirectionsField['EDIT_FORM_LABEL'] = 'Город отправления (выбрать город)';

                if($_REQUEST['ID'] > 0) {

                    $option = '<option value="">Выбрать из списка</option>';

                } else {

                    $option = '<option selected value="">Выбрать из списка</option>';

                }

                if (!empty($arCitiesHL) && isset($arCitiesHL) && !empty($arResultCitiesIB)) {

                    foreach ($arCitiesHL as $city){

                        $selected = '';

                        if(!empty($arDirectionsField['VALUE']) && in_array($city['ID'],(array)$arDirectionsField['VALUE'])){
                            $selected = 'selected';
                        }

                        $option .= '<option '.$selected.' value="' . $city['ID'] . '">' . $arResultCitiesIB[$city['UF_BX_ID']]['NAME'] . '</option>';
                    }

                }

                $contentDirectionsFieldsForm .= \travelsoft\sts\pages\Utils::getEditFieldHtml(
                    $arDirectionsField['EDIT_FORM_LABEL'] . ':', '<select style="width:180px;" id="UF_CITYFROM_ID" class="tour-select" name="' . $arDirectionsField['FIELD_NAME'] . '" tabindex="-1" aria-hidden="true">' . $option . '</select>', true
                );

                break;

            case 'UF_COUNTRY_ID':

                /*$option = '';

                $arCountriesHL = \travelsoft\sts\stores\RelationCountries::get(array('select' => array('ID', 'UF_BX_ID')));

                $arDirectionsField['EDIT_FORM_LABEL'] = 'Страны прибытия (выбрать страны)';

                if (!empty($arCountriesHL) && is_array($arCountriesHL) && !empty($arResultCountriesIB)) {

                    foreach ($arCountriesHL as $country){

                        $selected = '';

                        if(!empty($arDirectionsField['VALUE']) && in_array($country['ID'],(array)$arDirectionsField['VALUE'])){
                            $selected = 'selected';
                        }

                        $option .= '<option '.$selected.' value="' . $country['ID'] . '">' . $arResultCountriesIB[$country['UF_BX_ID']]['NAME'] . '</option>';
                    }

                }

                $contentDirectionsFieldsForm .= \travelsoft\sts\pages\Utils::getEditFieldHtml(
                    $arDirectionsField['EDIT_FORM_LABEL'] . ':', '<select multiple style="width:180px;" id="countries-select-1" class="countries-select" name="' . $arDirectionsField['FIELD_NAME'] . '[]" tabindex="-1" aria-hidden="true">' . $option . '</select>', true
                );*/

                break;

            case 'UF_CITYFROM_BX_ID':
            case 'UF_COUNTRY_BX_ID':
                break;


            default:

                if(isset($arOperators_[$arDirectionsField['FIELD_NAME']])) {

                    $option = '';

                    $arDirectionsField['EDIT_FORM_LABEL'] = $arDirectionsField['EDIT_FORM_LABEL'].'. Страны прибытия (выбрать страны)';

                    if (!empty($arCountriesHL) && is_array($arCountriesHL) && !empty($arResultCountriesIB)) {

                        foreach ($arCountriesHL as $country){

                            $selected = '';

                            if(!empty($arDirectionsField['VALUE']) && in_array($country['ID'],(array)$arDirectionsField['VALUE'])){
                                $selected = 'selected';
                            }

                            $option .= '<option '.$selected.' value="' . $country['ID'] . '">' . $arResultCountriesIB[$country['UF_BX_ID']]['NAME'] . '</option>';
                        }

                    }

                    $contentDirectionsFieldsForm .= \travelsoft\sts\pages\Utils::getEditFieldHtml(
                        $arDirectionsField['EDIT_FORM_LABEL'] . ':', '<select multiple style="width:180px;" id="countries-select-'.$arDirectionsField['ID'].'" class="countries-select" name="' . $arDirectionsField['FIELD_NAME'] . '[]" tabindex="-1" aria-hidden="true">' . $option . '</select>', true
                    );

                } else {

                    $contentDirectionsFieldsForm .= $USER_FIELD_MANAGER->GetEditFormHtml(\travelsoft\sts\Utils::isEditFormRequest(), $_POST[$arDirectionsField['FIELD_NAME']], $arDirectionsField);

                }
        }
    }

    $arTabs = array(
        array(
            "DIV" => "DIRESCTION",
            "TAB" => 'Направление',
            "TITLE" => 'Информация по направлению',
            'FIELDS' => $contentDirectionsFieldsForm,
            //'content' => $contentDirectionsFieldsForm,
    )
    );

    echo '<form enctype="multipart/form-data" action="' . $APPLICATION->GetCurPageParam() . '" method="POST" name="directions_form" id="directions_form">';

    if ($_REQUEST['ID'] > 0) {

        echo '<input name="ID" value="' . intVal($_REQUEST['ID']) . '" type="hidden">';
    }

    echo '<input type="hidden" name="lang" value="' . LANGUAGE_ID . '">';

    echo bitrix_sessid_post();

    $tabControl = new \CAdminTabControl("tabControl", $arTabs);

    $tabControl->Begin();

    foreach ($arTabs as $tab) {

        $tabControl->BeginNextTab();

        echo $tab["FIELDS"];

    }

    $tabControl->Buttons();

    $buttons = array(
        array(
            'class' => 'adm-btn-save',
            'name' => 'SAVE',
            'value' => 'Сохранить'
        ),
        array(
            'name' => 'CANCEL',
            'value' => 'Отменить'
        )
    );

    foreach ($buttons as $button) {

        $class = $button['class'] ? 'class="' . $button['class'] . '"' : '';

        $id = $button['id'] ? 'id="' . $button['class'] . '"' : '';

        $onclick = $button['onclick'] ? 'onclick="' . $button['onclick'] . '"' : '';

        echo '<input ' . $onclick . ' type="submit" name="' . $button['name'] . '" ' . $id . ' value="' . $button['value'] . '" ' . $class . '>';
    }

    $tabControl->End();

    echo '</form>';

    ?>

    <script>
        $(document).ready(function () {

            'use strict';

            var
                /**
                 * инициализируем select2
                 * @param {object} options
                 */
                __initSelect2 = function (options) {

                    // инициализируем select
                    function select2 (obj) {
                        obj.select2({
                            allowClear: false,
                            formatNoMatches: function () {
                                return "Совпадений не найдено";
                            },
                            placeholder: "Выбрать из списка"
                            //minimumResultsForSearch: -1
                        });
                    }

                    select2(options);

                };

            $('#directions_form select').each(function() {
                __initSelect2($(this));
            });

        });
    </script>

    <?


} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
