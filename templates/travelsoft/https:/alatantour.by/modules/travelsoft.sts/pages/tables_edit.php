<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

Bitrix\Main\Loader::includeModule("travelsoft.sts");
Bitrix\Main\Loader::includeModule('highloadblock');
Bitrix\Main\Loader::includeModule('iblock');

use Bitrix\Highloadblock as HL;
use \travelsoft\sts\pages\Utils;
?>
<?
$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."css/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."js/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\sts\pages\Settings::REL_PATH_TO_MODULE."js/select2.full.min.js'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?
$arResultHL = null;

// get entity info
if (isset($_REQUEST['ENTITY_ID']))
{

    $arListIB = \travelsoft\sts\Utils::correctListIB();
    $arListHL = \travelsoft\sts\Utils::correctListHL();
    $arResultIB = $arListIB[$_REQUEST['ENTITY_ID']];
    $arResultHL = $arListHL[$_REQUEST['ENTITY_ID']];
    $arResultTable = \travelsoft\sts\Utils::tablesHL($_REQUEST['ENTITY_ID']);

    $sTableID = $_REQUEST["ENTITY_ID"];

    $sTableCODE = "tbl_".mb_strtolower(\travelsoft\sts\Utils::codeTablesHL($sTableID));

    if (!empty($arResultHL))
    {
        //check rights
        if ($USER->isAdmin())
        {
            $canEdit = $canDelete = true;
        }
        else
        {
            $operations = HL\HighloadBlockRightsTable::getOperationsName($sTableCODE);
            if (!empty($operations))
            {
                $canEdit = in_array('hl_element_write', $operations);
                $canDelete = in_array('hl_element_delete', $operations);
            }
        }
    }
}

$arResultItem = array();

try {

    $ID = intVal($_REQUEST['ID']);

    if ($ID > 0) {

        $arResultItem = $arResultTable::getById($ID);

        if (!$arResultItem['ID']) {

            throw new Exception('Элемент направления с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование объекта #' . $arResultItem['ID'];
    }

    $APPLICATION->SetTitle($title);

    $url = \travelsoft\sts\pages\Settings::TABLES_LIST_URL . '?ENTITY_ID='.$sTableID.'&lang=' . LANGUAGE_ID;

    global $USER_FIELD_MANAGER;

    if (strlen($_POST['CANCEL']) > 0) {

        LocalRedirect($url);
    }

    $arErrors = array();

    $arOperators = \travelsoft\sts\Utils::getOperators_();
    $arOperators_ = array();

    if(!empty($arOperators)) {
        foreach ($arOperators as $operator){
            $arOperators_[$operator["UF_CODE"]] = $operator["UF_NAME"];
        }
    }

    if(\travelsoft\sts\Utils::isEditFormRequest()){

        $data = array();

        $USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_' . $sTableID, $data);

        if ($data['UF_BX_ID'] <= 0) {

            $arErrors[] = 'Системная ошибка. Не указан код объекта (ID Битрикс).';

        }

        if (empty($arErrors)) {

            if ($_REQUEST['ID'] > 0) {

                $ID = intval($_REQUEST['ID']);
                $result = $arResultTable::update($ID, $data);

            } /*else {

                $result = $arResultTable::add($data);

            }*/

            if ($result) {

                LocalRedirect($url);
            }
        }
    }



    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arErrors)) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arErrors),
            "TYPE" => "ERROR"
        ));
    }

    $contentTablesFieldsForm = '';
    $arTablesFields = $USER_FIELD_MANAGER->getUserFieldsWithReadyData('HLBLOCK_' . $sTableID, $arResultItem, LANGUAGE_ID);

    foreach ($arTablesFields as $arTablesField) {

        if (key_exists($arTablesField['FIELD_NAME'], $_POST)) {

            $arTablesField['VALUE'] = $_POST[$arTablesField['FIELD_NAME']];
        }

        switch ($arTablesField['FIELD_NAME']) {

            case 'UF_BX_ID':

                if(!empty($arResultIB) && isset($arResultIB[$arTablesField["VALUE"]])){

                }

                $arTablesField['EDIT_FORM_LABEL'] = 'Объект "'.$arResultIB[$arTablesField["VALUE"]]["NAME"].'"';

                $contentTablesFieldsForm .= $USER_FIELD_MANAGER->GetEditFormHtml(\travelsoft\sts\Utils::isEditFormRequest(), $_POST[$arTablesField['FIELD_NAME']], $arTablesField);

                break;

            case 'UF_TYPE_SEARCH':

                $option = '';

                $arTablesField['EDIT_FORM_LABEL'] = $arTablesField['EDIT_FORM_LABEL'].' (для "Мастер-Тур")';

                $arTypeSearch = array("hotel","tour");

                foreach ($arTypeSearch as $typesearch){

                    $selected = '';

                    if(!empty($arTablesField['VALUE']) && in_array($typesearch,(array)$arTablesField['VALUE'])){
                        $selected = 'selected';
                    }

                    $option .= '<option '.$selected.' value="' . $typesearch . '">' . $typesearch . '</option>';
                }

                $contentTablesFieldsForm .= \travelsoft\sts\Utils::getEditFieldHtml(
                    $arTablesField['EDIT_FORM_LABEL'] . ':', '<select style="width:180px;" id="type_search_select" class="type_search_select-select" name="' . $arTablesField['FIELD_NAME'] . '" tabindex="-1" aria-hidden="true">' . $option . '</select>'
                );

                break;

            case 'UF_CITYFROM':

                $checked_checkbox = '';

                if(!empty($arTablesField['VALUE']) && $arTablesField['VALUE'] == 1){
                    $checked_checkbox = 'checked';
                }

                $checkbox = '<input '.$checked_checkbox.' type="checkbox" name="' . $arTablesField['FIELD_NAME'] . '" value="1" > да<br>';

                $contentTablesFieldsForm .= \travelsoft\sts\Utils::getEditFieldHtml(
                    $arTablesField['EDIT_FORM_LABEL'] . ':', '<div id="cityfrom-checkbox-1">' . $checkbox . '</div>'
                );

                break;

            default:

                $contentTablesFieldsForm .= $USER_FIELD_MANAGER->GetEditFormHtml(\travelsoft\sts\Utils::isEditFormRequest(), $_POST[$arTablesField['FIELD_NAME']], $arTablesField);
        }
    }

    $arTabs = array(
        array(
            "DIV" => "TABLEOBJECT",
            "TAB" => 'Соотвествия объекта',
            "TITLE" => 'Информация объекта по соотвествиям ID систем поиска',
            'FIELDS' => $contentTablesFieldsForm,
            //'content' => $contentDirectionsFieldsForm,
        )
    );

    echo '<form enctype="multipart/form-data" action="' . $APPLICATION->GetCurPageParam() . '" method="POST" name="tables_objects_form" id="tables_objects_form">';

    if ($_REQUEST['ID'] > 0) {

        echo '<input name="ID" value="' . intVal($_REQUEST['ID']) . '" type="hidden">';
    }

    echo '<input type="hidden" name="lang" value="' . LANGUAGE_ID . '">';

    echo bitrix_sessid_post();

    $tabControl = new \CAdminTabControl("tabControl", $arTabs);

    $tabControl->Begin();

    foreach ($arTabs as $tab) {

        $tabControl->BeginNextTab();

        echo $tab["FIELDS"];

    }

    $tabControl->Buttons();

    $buttons = array(
        array(
            'class' => 'adm-btn-save',
            'name' => 'SAVE',
            'value' => 'Сохранить'
        ),
        array(
            'name' => 'CANCEL',
            'value' => 'Отменить'
        )
    );

    foreach ($buttons as $button) {

        $class = $button['class'] ? 'class="' . $button['class'] . '"' : '';

        $id = $button['id'] ? 'id="' . $button['class'] . '"' : '';

        $onclick = $button['onclick'] ? 'onclick="' . $button['onclick'] . '"' : '';

        echo '<input ' . $onclick . ' type="submit" name="' . $button['name'] . '" ' . $id . ' value="' . $button['value'] . '" ' . $class . '>';
    }

    $tabControl->End();

    echo '</form>';

    ?>

    <script>
        $(document).ready(function () {

            'use strict';

            var
                /**
                 * инициализируем select2
                 * @param {object} options
                 */
                __initSelect2 = function (options) {

                    // инициализируем select
                    function select2 (obj) {
                        obj.select2({
                            allowClear: false,
                            formatNoMatches: function () {
                                return "Совпадений не найдено";
                            },
                            placeholder: "Выбрать из списка",
                            minimumResultsForSearch: -1
                        });
                    }

                    select2(options);

                };

            $('#tables_objects_form select').each(function() {
                __initSelect2($(this));
            });

            console.log($('input[name="UF_BX_ID"]'));

            $('#tables_objects_form input[name="UF_BX_ID"]').attr('readonly', 'readonly');

        });
    </script>

    <?


} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
