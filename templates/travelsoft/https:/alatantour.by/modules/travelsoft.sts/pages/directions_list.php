<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

Bitrix\Main\Loader::includeModule("travelsoft.sts");
Bitrix\Main\Loader::includeModule('highloadblock');
Bitrix\Main\Loader::includeModule('iblock');
?>

<?

/*$sTableID = \travelsoft\sts\Settings::relationdirectionsStoreId();*/
$sTableID = \travelsoft\sts\pages\Settings::DIRECTIONS_TABLE_ID;

$arResult = \travelsoft\sts\stores\RelationDirections::get();
$arResultCitiesIB = \travelsoft\sts\stores\Cities::get(array("select"=>array("ID","NAME")));
$arResultCountriesIB = \travelsoft\sts\stores\Countries::get(array("select"=>array("ID","NAME")));

$sort = new CAdminSorting($sTableID, "ID", "DESC");
$list = new CAdminList($sTableID, $sort);


if ($arResultsId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arResultsId = array_keys(\travelsoft\sts\stores\RelationDirections::get(array('select' => array('ID'))));
    }

    foreach ($arResultsId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                \travelsoft\sts\stores\RelationDirections::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array('filter' => array(), "order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
    $sTableID, array('nPageSize' => 20)
));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = (int)count($arResult);

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arResult_ = \travelsoft\sts\stores\RelationDirections::get($getParams);

$dbResult = new CAdminResult($arResult_, $sTableID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CITYFROM_ID",
        "content" => "Город отправления",
        "sort" => "cityfrom",
        "align" => "center",
        "default" => true,
    ),
    array(
        "id" => "UF_COUNTRY_ID",
        "content" => "Страна прибытия",
        "sort" => "country",
        "align" => "center",
        "default" => true,
    ),
));

$arView = array("UF_CITYFROM_BX_ID","UF_COUNTRY_BX_ID");

while ($arResult_ = $dbResult->Fetch()) {

    $row =& $list->AddRow($arResult_["ID"], $arResult_);
    $row->AddViewField("UF_CITYFROM_ID", $arResultCitiesIB[$arResult_["UF_CITYFROM_BX_ID"]]["NAME"]);

    $strName = array();

    if($arResult_["UF_COUNTRY_BX_ID"]){
        foreach ($arResult_["UF_COUNTRY_BX_ID"] as $country_bx){
            $strName[] = $arResultCountriesIB[$country_bx]["NAME"];
        }
        $strName = implode(" / ", $strName);
    } else {
        $strName = implode(" / ", $arResult_["UF_COUNTRY_ID"]);
    }

    $row->AddViewField("UF_COUNTRY_ID", $strName);

    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => 'BX.adminPanel.Redirect([], "' . \travelsoft\sts\pages\Settings::DETAIL_DIRECTIONS_URL . '?ID=' . $arResult_["ID"] . '", event);'
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить направление')) " . \travelsoft\sts\pages\Settings::DIRECTIONS_TABLE_ID . ".GetAdminList('/bitrix/admin/" . \travelsoft\sts\pages\Settings::DIRECTIONS_URL . "?ID=" . $arResult_["ID"] . "&action_button=delete&lang=" . LANGUAGE_ID . "&sessid=" . bitrix_sessid() . "');"
        )
    ));

}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
    'TEXT' => "Создать направление",
    'TITLE' => "Создание направления",
    'LINK' => \travelsoft\sts\pages\Settings::DETAIL_DIRECTIONS_URL.'?lang=' . LANG,
    'ICON' => 'btn_new'
)));

$list->CheckListMode();

?>

<?$APPLICATION->SetTitle("Направления");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>
<?

$list->DisplayList();

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>