<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/prolog.php"); // пролог модуля

Bitrix\Main\Loader::includeModule("travelsoft.sts");
Bitrix\Main\Loader::includeModule('highloadblock');
Bitrix\Main\Loader::includeModule('iblock');

use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Highloadblock as HL;

global $APPLICATION, $USER;

IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("subscribe");
?>

<?

$select_table = null;

$arListIB = \travelsoft\sts\Utils::correctListIB();
$arListHL = \travelsoft\sts\Utils::correctListHL();

if (!empty($_REQUEST["ENTITY_ID"]) && !empty($_GET["ENTITY_ID"])) {

    $select_table = !empty($_REQUEST["ENTITY_ID"]) ? $_REQUEST["ENTITY_ID"] : $_GET["ENTITY_ID"];

    $sTableID = !empty($_REQUEST["ENTITY_ID"]) ? $_REQUEST["ENTITY_ID"] : $_GET["ENTITY_ID"];

} else {

    $first_key = reset(array_keys($arListHL));
    $select_table = $first_key;

    $sTableID = $first_key;

}

$sTableCODE = "tbl_".mb_strtolower(\travelsoft\sts\Utils::codeTablesHL($sTableID));

$arResultIB = $arListIB[$sTableID];
$arResultHL = $arListHL[$sTableID];
$arResultTable = \travelsoft\sts\Utils::tablesHL($sTableID);

$totalCount = $arResultTable::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

$sort = new CAdminSorting($sTableCODE, "ID", "DESC");
$list = new CAdminList($sTableCODE, $sort);

if(!empty($arResultHL)) {

    if ($USER->isAdmin()) {
        $canEdit = $canDelete = true;
    } else {
        $operations = HL\HighloadBlockRightsTable::getOperationsName($sTableCODE);

        if (!empty($operations)) {

            $canEdit = in_array('hl_element_write', $operations);
            $canDelete = in_array('hl_element_delete', $operations);
        }
    }

    $arFilter = array();
    $filter = null;
    $filterFields = array('find_name');
    $filterValues = array();
    $filterTitles = array('UF_BX_ID');

    // инициализируем фильтр
    $filter = $list->InitFilter($filterFields);

    if (!empty($find_name))
    {

        $arResultIBlock = \travelsoft\sts\Utils::tablesIB($sTableID);
        $totalResultIBlock = $arResultIBlock::get(array('filter' => array("NAME"=>'%'.$find_name.'%'),"select"=>array("ID")));
        if(!empty($totalResultIBlock)){
            $ids = array_keys($totalResultIBlock);
        }
        if(isset($ids) && !empty($ids)){
            $filterValues['UF_BX_ID'] = $ids;
        }
    }

    $filter = new CAdminFilter(
        $sTableCODE."_filter_id",
        $filterTitles
    );

    // group actions
    if($list->EditAction() && $canEdit && $POST_RIGHT=="W")
    {

        foreach($FIELDS as $ID=>$arFields)
        {
            if(!$list->IsUpdated($ID))
                continue;

            $arResultTable::update($ID, $arFields);
        }
    }

    if ($arResultsId = $list->GroupAction()) {

        if ($_REQUEST['action_target'] == 'selected') {

            $arResultsId = array_keys($arResultTable::get(array('select' => array('ID'), "filter" => $filterValues)));
        }

        foreach ($arResultsId as $ID) {

            $ID = (int)$ID;

            if (!$ID)
            {
                continue;
            }

            switch ($_REQUEST['action']) {

                case "delete":

                    $arResultTable::delete($ID);

                    break;

            }
        }
    }

    if ($_REQUEST["by"]) {

        $by = $_REQUEST["by"];
    }

    if ($_REQUEST["order"]) {

        $order = $_REQUEST["order"];
    }

    $getParams = array('filter' => $filterValues, "order" => array($by => $order));

    $usePageNavigation = true;
    if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'excel')
    {
        $usePageNavigation = false;
    }
    else {
        $navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
            $sTableCODE, array('nPageSize' => 20, 'sNavID' => $APPLICATION->GetCurPage() . '?ENTITY_ID=' . $ENTITY_ID)
        ));

        $navParams['PAGEN'] = (int)$navParams['PAGEN'];
        $navParams['SIZEN'] = (int)$navParams['SIZEN'];
    }
    if ($usePageNavigation)
    {
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN']*($navParams['PAGEN']-1);
    }
    if ($usePageNavigation) {

        $totalCount = (int) $totalCount['CNT'];

        if ($totalCount > 0) {

            $totalPages = ceil($totalCount / $navParams['SIZEN']);
            if ($navParams['PAGEN'] > $totalPages) {

                $navParams['PAGEN'] = $totalPages;
            }
            $getParams['limit'] = $navParams['SIZEN'];
            $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
        } else {

            $navParams['PAGEN'] = 1;
            $getParams['limit'] = $navParams['SIZEN'];
            $getParams['offset'] = 0;
        }
    }

    $arResult_ = $arResultTable::get($getParams);

    $dbResult = new CAdminResult($arResult_, $sTableCODE);

    if ($usePageNavigation) {

        $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
        $dbResult->NavRecordCount = $totalCount;
        $dbResult->NavPageCount = $totalPages;
        $dbResult->NavPageNomer = $navParams['PAGEN'];
    } else {

        $dbResult->NavStart();
    }

    $list->NavText($dbResult->GetNavPrint('Страницы'));

    $arHead = array(
        array(
            "id" => "ID",
            "content" => "ID",
            "sort" => "ID",
            "align" => "center",
            "default" => true
        ),
        array(
            "id" => "UF_BX_ID",
            "content" => "Наименование",
            "sort" => "bx_id",
            "align" => "center",
            "default" => true,
        ),
    );

    $arOperators = \travelsoft\sts\Utils::getOperators_();
    $arOperators_ = array();

    if(!empty($arOperators)) {
        foreach ($arOperators as $operator){
            $arOperators_[$operator["UF_CODE"]] = $operator["UF_NAME"];
            $arHead = array_merge($arHead, array(array(
                "id" => $operator["UF_CODE"],
                "content" => $operator["UF_NAME"],
                "sort" => mb_strtolower($operator["UF_CODE"]),
                "align" => "center",
                "default" => true,
            )));
        }
    }

    $list->AddHeaders(
        $arHead
    );

    while ($arResult_ = $dbResult->Fetch()) {

        $row =& $list->AddRow($arResult_["ID"], $arResult_);
        $row->AddViewField("UF_BX_ID", $arResultIB[$arResult_["UF_BX_ID"]]["NAME"]);


        if(!empty($arOperators)) {
            foreach ($arOperators as $operator){
                $row->AddViewField("UF_COUNTRY_ID", $arResult_, $row);
                $row->AddInputField($operator["UF_CODE"], array("size"=>20));
            }
        }

        $row->AddActions(array(
            array(
                "ICON" => "edit",
                "DEFAULT" => true,
                "TEXT" => "Изменить",
                "ACTION" => 'BX.adminPanel.Redirect([], "' . \travelsoft\sts\pages\Settings::DETAIL_OBJECT_TABLES_URL . '?ENTITY_ID='.$sTableID.'&ID=' . $arResult_["ID"] . '", event);'
            ),
            array(
                "ICON" => "delete",
                "DEFAULT" => true,
                "TEXT" => "Удалить",
                "ACTION" => "if(confirm('Действительно хотите удалить запись')) " . $sTableCODE . ".GetAdminList('/bitrix/admin/" . \travelsoft\sts\pages\Settings::TABLES_LIST_URL . "?ID=" . $arResult_["ID"] . "&action_button=delete&lang=" . LANGUAGE_ID . "&sessid=" . bitrix_sessid() . "');"
            )
        ));

    }

    $list->AddFooter(array(
        array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
        array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
    ));

    $list->AddGroupActionTable(Array(
        "delete" => "Удалить",
    ));

    $list->CheckListMode();

}

?>

<?$APPLICATION->SetTitle("Соответствия");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>

<style>
    #adm-filter-add-tab-tbl_countries_hl_filter_id, #adm-filter-switcher-tab {
        display: none !important;
    }
    .mb-20 {
        margin-bottom: 20px;
    }
    .pt-4 {
        padding-top: 4px;
    }
</style>

    <table class="mb-20">
        <tbody>
            <tr>
                <td>
                    <form id="table-generator-form" method="GET" action="<?= $APPLICATION->GetCurPage("lang=" . LANG, array('lang')) ?>">
                        <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
                        <?= bitrix_sessid_post(); ?>
                        <div class="form-fields">
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="right">
                                            <b>Справочник: </b>
                                        </td>
                                        <td>
                                            <?= travelsoft\sts\Utils::tablesList($select_table)?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div id="btn-area">
                                <button name="show_table" value="Показать" type="submit" class="sub-part adm-btn-save adm-btn adm-btn-add pt-4">Показать</button>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>

    <form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?ENTITY_ID=<?=$sTableID?>">
        <?
        $filter->Begin();
        ?>
        <tr>
            <td>Наименование</td>
            <td><input type="text" name="find_name" size="47" value="<?echo htmlspecialcharsbx($find_name)?>"><?=ShowFilterLogicHelp()?></td>
        </tr>
        <?
        $filter->Buttons(array("table_id"=>$sTableCODE, "url"=>$APPLICATION->GetCurPage().'?ENTITY_ID='.$sTableID, "form"=>"find_form"));
        $filter->End();
        ?>
    </form>


<?

    $list->DisplayList();

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>