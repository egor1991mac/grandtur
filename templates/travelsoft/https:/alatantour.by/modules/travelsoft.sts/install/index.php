<?php

use Bitrix\Main\ModuleManager;

class travelsoft_sts extends CModule {

    public $MODULE_ID = "travelsoft.sts";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";
    protected $namespaceFolder = "travelsoft";

    function __construct() {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Модуль поиска туров";
        $this->MODULE_DESCRIPTION = "Модуль поиска туров представленных операторов";
        $this->PARTNER_NAME = "juliya.sharlova";
        $this->PARTNER_URI = "";
    }

    public function DoInstall() {
        try {

            if (!ModuleManager::isModuleInstalled("iblock")) {
                throw new Exception("Для работы модуля необходим модуль инфоблока");
            }

            if (!ModuleManager::isModuleInstalled("highloadblock")) {
                throw new Exception("Для работы модуля необходим модуль highloadblock");
            }

            // register module
            ModuleManager::registerModule($this->MODULE_ID);

            return true;
        } catch (Exception $ex) {
            $GLOBALS["APPLICATION"]->ThrowException($ex->getMessage());
            $this->DoUninstall();
            return false;
        }

        return true;
    }

    public function DoUninstall() {

        $this->deleteFiles();

        // delete options
        Bitrix\Main\Config\Option::delete($this->MODULE_ID, array('name' => 'OPERATORS_HL'));
        Bitrix\Main\Config\Option::delete($this->MODULE_ID, array('name' => 'CITIES_HL'));
        Bitrix\Main\Config\Option::delete($this->MODULE_ID, array('name' => 'COUNTRIES_HL'));

        // unregister module
        ModuleManager::UnRegisterModule($this->MODULE_ID);

        return true;
    }

}
