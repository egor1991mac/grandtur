
/**
 * main-search-form-detail.js
 * @author yuliya.sharlova
 * @copyright (c) 2018, travelsoft
 */

(function (window) {

    var document = window.document;
    var bx = window.BX;
    var $ = window.jQuery;

    $(document).ready(function () {

        var childrenAgeTemplate = $("#children-age-template").html();

        var cache = {};

        /**
         * @param {DOMElement} context
         * @returns {undefined}
         */
        function __initSelect2(context) {

            var $context = $(context);

            $context.select2({
                allowClear: true,
                formatNoMatches: function () {
                    return "Ничего не найдено";
                },
                placeholder: $context.data("placeholder")
            });

        }

        /**
         * @param {$} $calendar
         * @returns {undefined}
         */
        function __removeDatepicker ($calendar) {
            $calendar.off("changeDate");
            $calendar.datepicker("remove");
        }

        /**
         * @param {$} $calendar
         * @param {Function} beforeShowDayHandler
         * @returns {undefined}
         */
        function __initDatepicker($calendar, beforeShowDayHandler) {

            var options = {
                todayHighlight: true,
                format: 'dd.mm.yyyy',
                startDate: "today",
                autoclose: true,
                language: "RU",
                weekStart: 1
            };

            if (typeof beforeShowDayHandler === "function") {
                options.beforeShowDay = beforeShowDayHandler;
            }


            $calendar.datepicker(options);
            $calendar.datepicker("setDate", $calendar.data("date-default"));
            if ($calendar.attr("name") === "travelbooking[date_from]") {
                $calendar.on("changeDate", function () {
                    var $date_from = $calendar.closest("form").find("input[name='travelbooking[date_from]']");
                    var $date_to = $calendar.closest("form").find("input[name='travelbooking[date_to]']");
                    $date_from.siblings('label').text($calendar.val());
                    $date_to.datepicker("setDate", $calendar.val());
                    $date_to.datepicker("show");
                });
            } else if ($calendar.attr("name") === "travelbooking[date_to]") {
                $calendar.on("changeDate", function () {
                    var $date_from = $calendar.closest("form").find("input[name='travelbooking[date_from]']");
                    var $date_to = $calendar.closest("form").find("input[name='travelbooking[date_to]']");
                    var arr_date_from = $date_from.val().split(".");
                    var arr_date_to = $calendar.val().split(".");
                    if ((new Date(arr_date_from[2], arr_date_from[1], arr_date_from[0])).getTime()
                            > (new Date(arr_date_to[2], arr_date_to[1], arr_date_to[0])).getTime()) {
                        $date_from.val($calendar.val());
                    }
                    $date_to.siblings('label').text($calendar.val());
                });
            }
        }

        /**
         * @param {$} $calendar
         * @param {Array} arr_dates
         * @returns {undefined}
         */
        function __datesHighlighting($calendar, arr_dates) {

            __removeDatepicker($calendar);

            if ($.isArray(arr_dates) && arr_dates.length) {

                __initDatepicker($calendar, function (date) {

                    var day = date.getDate().toString();

                    var month = (date.getMonth() + 1).toString();

                    var year = date.getFullYear().toString();

                    var d_parts = [];

                    var to_highlight = false;

                    if (Number(day) < 10) {
                        day = `0${day}`;
                    }

                    if (Number(month) < 10) {
                        month = `0${month}`;
                    }

                    for (var i = 0; i < arr_dates.length; i++) {
                        var d_parts = arr_dates[i].split(".");

                        if (d_parts[0] === day && d_parts[1] === month && d_parts[2] === year) {

                            to_highlight = true;
                            break;
                        }
                    }

                    return to_highlight ? {classes: 'travelsoft-highlight-date'} : {classes: ''};

                });

            } else {
                __initDatepicker($calendar);
            }


        }

        /**
         * @param {Object} calendar_data
         * @returns {undefined}
         */
        function __initDatesHighlight(calendar_data) {

            if (!$.isArray(cache[calendar_data.object_id])) {
                $.post("/local/ajax/booking/get-dates-for-highlight.php", {
                    sessid: bx.bitrix_sessid(), service_type: calendar_data.service_type, object_id: calendar_data.object_id}, function (arr_dates) {

                    cache[calendar_data.object_id] = arr_dates;
                    __datesHighlighting(calendar_data.calendar, arr_dates);
                }).fail(function () {
                    __datesHighlighting(calendar_data.calendar, []);
                });
            } else {
                __datesHighlighting(calendar_data.calendar, cache[calendar_data.object_id]);
            }

        }

        $.fn.datepicker.dates.RU = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четвер", "Пятница", "Суббота"],
            daysShort: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
            daysMin: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек."],
            today: "Сегодня",
            clear: "Очистить",
            format: "dd.mm.yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        // init
        $('select[name="travelbooking[children]"]').on("change", function () {

            var $this = $(this).closest("select");
            var children = $this.val();
            var childrenAgeBox = $this.closest("form").find(".children-age-box");
            childrenAgeBox.find(".select-age-box").each(function () {
                $(this).remove();
            });
            childrenAgeBox.addClass("hidden_");
            if (children > 0) {
                for (var i = 1; i <= children; i++) {
                    childrenAgeBox.append(childrenAgeTemplate.replace("{{index}}", i));
                }
                childrenAgeBox.removeClass("hidden_");
            }

        });

        $('.select2').each(function () {
            __initSelect2(this);

            $(this).on("change", function () {

                var $this = $(this);

                var value_parts = [];

                var form = $this.closest("form");

                if($this.attr('id') != 'cities') {
                    if (this.value) {
                        value_parts = this.value.split("~");
                        form.attr("action", value_parts[0] || form.attr("action"));
                        $('.input-datepicker-form').each(function () {
                            // try highlight dates
                            var $this = $(this);
                            var service_type = $this.data("service-type");

                            if (typeof service_type === "string" && service_type !== "") {
                                __initDatesHighlight({
                                    service_type: service_type,
                                    object_id: value_parts[1],
                                    calendar: $this
                                });
                            }
                        });

                    }
                } else {

                    if (this.value) {

                        var search_url = form.find('input[name="searchUrl"]').val();
                        form.append('<input type="hidden" name="set_filter" value="y">');
                        form.attr("action", search_url || form.attr("action"));

                    }

                }
            });

        });


        $('form').on("submit", function (e) {
            var $search_btn = $('.search-btn');
            var select = null;
            $search_btn.prop("disabled", true);

            if ($(this).attr("id") === "transfers-booking-form-search") {
                select = $("select[name=rates]");
                if (!select.val()) {
                    $search_btn.prop("disabled", false);
                    alert("Тариф не выбран");
                } else {
                    $('input[name="travelbooking[rates_id][]"]').val(select.val().replace("~", ""));
                    select.prop("disabled", true);
                }

            } else if (this.action.indexOf("/#") !== -1) {
                $search_btn.prop("disabled", false);
                alert("Объект не выбран");
                e.preventDefault();
            }
        });

        $('.closer').on("click", function () {
            $(this).parent().addClass("hidden_");
        });

        $('.input-datepicker-form').each(function () {
            __initDatepicker($(this));
        });

    });
})(window);

