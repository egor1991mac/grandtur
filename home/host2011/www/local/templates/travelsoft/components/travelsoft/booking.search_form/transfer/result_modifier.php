<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$cache = new travelsoft\booking\adapters\Cache("booking_search_form_transfer_rates", "/travelsoft/booking/rates", 3600000);

if (empty($arResult["TRANSFER_RATES"] = $cache->get())) {
    
    $arResult["TRANSFER_RATES"] = $cache->caching(function () use ($cache) {
       
        $cache->setTagCache("highloadblock_" . travelsoft\booking\Settings::ratesStoreId());
        
        return travelsoft\booking\stores\Rates::get([
            "filter" => ["UF_SERVICES_TYPE" => ["transfer", "transferback"]]
        ]);
    });
    
}