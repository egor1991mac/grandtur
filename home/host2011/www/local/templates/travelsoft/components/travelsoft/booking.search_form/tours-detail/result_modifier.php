<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

/**
 * /local/php_interface/include/functions.php
 */
$arResult["TOURS"] = \getMainSearchFormsIblockItems(TOURS_ID_IBLOCK, ["!=PROPERTY_TOURTYPE" => SEA_TOUR_TYPE_ID]);

$arResult["CITIES"] = $arResult["CITIES_SEARCH_OBJECT"] = array();
$cities_id = array();
foreach ($arResult["TOURS"] as $tour) {

    if(!empty($tour["PROPERTIES"]["TOWN"]["VALUE"])){
        foreach ($tour["PROPERTIES"]["TOWN"]["VALUE"] as $city){
            $cities_id[] = $city;
            $arResult["CITIES_SEARCH_OBJECT"][(int)abs(crc32($city))][] = array(
                "ID" => $tour["ID"],
                "NAME" => $tour["NAME"],
                "DETAIL_PAGE_URL" => $tour["DETAIL_PAGE_URL"]
            );
        }
        $cities_id = array_merge($cities_id,$tour["PROPERTIES"]["TOWN"]["VALUE"]);
    }


}
if(!empty($cities_id)) {
    $cities_id = array_unique($cities_id);
    $arResult["CITIES"] = \getMainSearchFormsIblockItems(CITIES_ID_IBLOCK, ["ID" => $cities_id]);
}
