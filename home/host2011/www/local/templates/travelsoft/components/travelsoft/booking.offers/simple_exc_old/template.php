
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$randString = randString(7);

$this->addExternalCss("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.css");
$this->addExternalJs("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.min.js");

?>
    <div class="offer-box container" id="offers-box-<?= $randString ?>"><?= GetMessage("TRAVELBOOKING_SEARCH_TEXT") ?></div>
    <template id="offers-item-template-<?= $randString ?>">

        <div class="tour-package">
            <div class="tour-package__row row">
                <div class="col-12 col-lg-2 px-0 py-2">
                        <? if ($arResult["SERVICE_TYPE"] === "excursiontour"): ?>
                            <span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TOUR") ?></span> <span class="fw-bold date-tour">{{offer_date}}</span>
                        <? endif; ?>
				</div>
                <div class="col-12 col-lg-6 hotel-items__check text_left ts-d-md-flex py-2">
                    <div class="ts-width-30">
                        <div class="img" style="background: url({{offer_image}})"></div>
                    </div>
                    <div class="ts-width-70">
                    <div class="tour-package__title d-inline-block">{{offer_name}}</div>
                    <ul class="hotel-package__props">
                        <? if (in_array($arResult["SERVICE_TYPE"], ["packageTour", "transfer", "transferback"])): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL") ?></span> <span class="fw-bold">{{offer_date_from_transfer}}</span></li>
                        <? endif ?>
                        <? if (in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback"])): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") ?></span> <span class="fw-bold"><?= $arOffer["DATE_FROM_PLACEMENT"] ?></span></li>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") ?></span> <span class="fw-bold"><?= $arOffer["DATE_TO_PLACEMENT"] ?></span></li>
                        <? endif; ?>
                        <?/* if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): */?><!--
                            <li><span class="title mr-1"><?/*= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") */?></span> <span class="fw-bold">{{offer_date_from_placement}}</span></li>
                            <li><span class="title mr-1"><?/*= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") */?></span> <span class="fw-bold">{{offer_date_to_placement}}</span></li>
                        --><?/* endif; */?>
                        <? if ($arResult["SERVICE_TYPE"] === "packageTour"): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL") ?></span> <span class="fw-bold">{{offer_date_back_transfer}}</span></li>
                        <? endif; ?>
                        <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): ?>
                            <!--<li><span class="title mr-1"><?/*= GetMessage("TRAVELBOOKING_NIGHTS") */?></span> <span class="fw-bold">{{offer_nights}}</span></li>-->
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_FOOD") ?></span> <span class="fw-bold">{{offer_food}}</span></li>
                        <? endif; ?>
							<li><?= (@$_GET["travelbooking"]["adults"] ? GetMessage("ADL") . htmlspecialchars(intval($_GET["travelbooking"]["adults"])) : GetMessage("ADL") . " 2")?> <?= (@$_GET["travelbooking"]["children"] ? ", " . GetMessage("CHILD") . htmlspecialchars(intval($_GET["travelbooking"]["children"])) . " (" . implode(",", array_map(function($item) {return $item . GetMessage("AGE");}, $_GET["travelbooking"]["children_age"])) . ")" : "")?> </li>
                    </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-2 hotel-items__check ts-d-md-flex px-0 py-2">
                    <div class="py-1 js-sticky-top m-auto">
                        <ul class="text-danger">
                            <li class="price">{{offer_price}}
                                <div class="tooltip_">
									<div class="text">{{offer_price_tourproduct}} {{offer_tourservice_price}}</div>
                                </div>
                            </li>
                            <li class="price-usd">{{offer_price_usd}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-2 hotel-items__check ts-d-md-flex  py-2">
                    <div class="py-1 js-sticky-top m-auto">
                        <span class="loading hidden_"></span><a rel="nofollow" class="btn-secondary add2cart btn btn-primary btn-lg" href="<?= $arResult["BOOKING_URL"] ?>?add2basket={{offer_add_to_basket}}&sessid=<?= bitrix_sessid() ?>" role="button"><?= GetMessage("TRAVELBOOKING_BTN") ?></a>
                        
                    </div>
                </div>
            </div>
            {{offer_modal_box}}
        </div>
    </template>
    <template id="nearest-available-text-template-<?= $randString ?>">
        <?= GetMessage("TRAVELBOOKING_NEAREST_AVAILABLE_TEXT") ?>
    </template>

    <template id="modal-box-template-<?= $randString ?>">
        <div class="collapse js-addition">
            <div class="hotel-package__more">
                {{offer_description_box}}
            </div>
        </div>
    </template>

    <template id="pictures-box-template-<?= $randString ?>">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="rslides" id="offer-carousel-{{rand_string}}">
                {{offer_pictures_items}}
            </ul>
        </div>
    </template>
    <template id="picture-item-template-<?= $randString ?>">
        <img class="img-fluid" src="{{offer_picture_path}}" alt="{{offer_name}}">
    </template>
    <template id="description-box-template-<?= $randString ?>">
        <p class="mb-2">{{offer_description}}</p>
    </template>

    <template id="preview-image-box-template-<?= $randString ?>">
        <div class="photo"><img src="{{offer_image}}" alt="{{offer_name}}"></div>
    </template>

    <script>
        Travelsoft.loadOffers({
            ajaxUrl: '<?= $componentPath . "/ajax/search_offers.php" ?>',
            request: '<?= json_encode(array("travelbooking" => $arResult["REQUEST"])) ?>',
            service_type: '<?= $arResult["SERVICE_TYPE"] ?>',
            insertion_selector: "#offers-box-<?= $randString ?>",
            offers_item_template_id_selector: "#offers-item-template-<?= $randString ?>",
            show_nearest_available_text: <? if (isset($_REQUEST["travelbooking"]) && !empty($_REQUEST["travelbooking"])): ?>true<? else: ?>false<? endif ?>,
                    nearest_available_text_template_id_selector: "#nearest-available-text-template-<?= $randString ?>",
                    modal_box_template_id_selector: "#modal-box-template-<?= $randString ?>",
                    pictures_box_template_id_selector: "#pictures-box-template-<?= $randString ?>",
                    picture_item_template_id_selector: "#picture-item-template-<?= $randString ?>",
                    description_box_template_id_selector: "#description-box-template-<?= $randString ?>",
                    preview_image_box_template_id_selector: "#preview-image-box-template-<?= $randString ?>",
                    messages: {
                        not_found: "<?= GetMessage("TRAVELBOOKING_OFFERS_NOT_FOUND", ["#phone#" => ""]) ?>",
                        no_food: "<?= GetMessage("TRAVELBOOKING_WITHOUT_FOOD") ?>"
                    }
                });
    </script>

<script>
<? if ($_REQUEST["s2o"] === "Y"): ?>
        Travelsoft.scrolltoInit(document.querySelector(".offer-box"), 200);
<? endif ?>
    Travelsoft.add2cartInit(<? if ($arResult["IS_AVAIL_BOOKING"]): ?>true<? else: ?>false<? endif; ?>, "<?= GetMessage("TRAVELBOOKING_NOT_AVAIL_BOOKING") ?>");
</script>
