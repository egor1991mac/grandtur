<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(true);

if (!$arResult["ITEMS"]) {
    return;
}
?>

<div class="hotel-card hotel-guests" id="reviews-list">
    <ul class="hotel-guests__list">
    <?
    if (defined("TRAVELSOFT_REVIEWS_AJAX_CALL") && TRAVELSOFT_REVIEWS_AJAX_CALL === TRUE) {
        $APPLICATION->RestartBuffer();
    }
    foreach ($arResult["ITEMS"] as $arItem):
        $h = md5("ts_" . $arItem["ID"]);
        ?>
        <li class="comment">
            <div class="row flex-sm-nowrap">
                <div class="comment__left col-12 col-sm-5 col-md-4 mb-4">
                    <div class="d-flex align-items-center w-100">
                        <div class="comment__avatar rounded-circle">
                            <img class="img-fluid rounded-circle" src="<?= $arItem["USER"]["AVATAR"] ?>" alt="<?= $arItem["USER"]["EMAIL"] ?>"/>
                        </div>
                    </div>
                    <div class="comment__right">
                        <div class="comment__name mb-0"><?= $arItem["USER"]["EMAIL"] ?></div>
                    </div>
                </div>
                <div class="comment__content col-12 col-sm-7 col-md-8">
                    <div class="pr-3">
                        <div class="comment__meta mb-3">
                            <ul class="row justify-content-sm-between align-items-baseline">
                                <li class="comment__level d-flex align-items-center mb-2">
                                    <h4 class="level m-0">
                                        <span id="review-rating-<?= $h ?>" data-stars="<?= $arItem["RATING"] ?>" class="review-rating">
                                            <?= $arItem["RATING"] ?>
                                        </span>
                                    </h4>
                                </li>
                                <li class="comment__date mb-2"><span>Дата отзыва:</span>
                                    <time>
                                        <?= $arItem["DATE_CREATE"] ?>
                                    </time>
                                </li>
                            </ul>
                        </div>
                        <div class="comment__desc">
                            <div class="d-flex">
                                <div class="content">
                                    <p>
                                        <?= $arItem["REVIEW_TEXT"] ?>
                                    </p>
                                    <? if (!empty($arItem["PICTURES"])) : ?>

                                        <a data-fancybox="gallery-<?= $h ?>" id="gallery-<?= md5("ts_" . time() . $arItem["PICTURES"][0]["ID"]) ?>" class="review-gallery" href="<?= $arItem["PICTURES"][0]["SRC"] ?>">Галерея</a>
                                        <span class="hidden">
                                            <? if ($arItem["PICTURES"][1]): ?>
                                                <? for ($i = 1; $i < count($arItem["PICTURES"]); $i++): ?>
                                                    <a data-fancybox="gallery-<?= $h ?>" id="gallery-<?= md5("ts_" . time() . $arItem["PICTURES"][$i]["ID"]) ?>" class="review-gallery" href="<?= $arItem["PICTURES"][$i]["SRC"] ?>"></a>
                                                <? endfor ?>
                                            <? endif ?>
                                        </span>

                                    <? endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <?
    endforeach;
    if (defined("TRAVELSOFT_REVIEWS_AJAX_CALL") && TRAVELSOFT_REVIEWS_AJAX_CALL === TRUE) {
        die;
    }
    ?>

    </ul>
</div>
<?
if (
        isset($arResult["dbList"]->NavPageCount) &&
        isset($arResult["dbList"]->NavPageNomer) &&
        $arResult["dbList"]->NavPageNomer < $arResult["dbList"]->NavPageCount
):
    ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <button class="btn btn-success" id="show-more-reviews">+ Еще</button>
        </div>
    </div>
<? endif ?>

<? $this->addExternalJs($templateFolder . "/_script.min.js", true) ?>
<script>
    window.reviewsListJsParameters = {
        total_stars_count: <?= Bitrix\Main\Config\Option::get("travelsoft.reviews", "MAX_RATING_VALUE") ?>,
        pageCount: "<?= $arResult["dbList"]->NavPageCount ?>",
        page: "<?= $arResult["dbList"]->NavPageNomer ?>",
        messages: {
            showMore: "<?= GetMessage("SHOW_MORE")?>",
            less: "<?= GetMessage("LESS")?>"
        }
    };
</script>