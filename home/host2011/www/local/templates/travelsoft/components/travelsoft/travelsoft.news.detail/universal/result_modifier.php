<?php

if (!empty($arResult['DISPLAY_PROPERTIES']['SERVICES']['VALUE'])) {
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arResult['DISPLAY_PROPERTIES']['SERVICES']['LINK_IBLOCK_ID'], "ID" => $arResult['DISPLAY_PROPERTIES']['SERVICES']['VALUE'], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $arResult["SERVICES"][$arFields['ID']] = [
            'NAME' => $arFields['NAME'],
            'ICON' => '<i class="align-middle text-secondary fa fa-' . $arProps['ICON']['VALUE'] . '" aria-hidden="true"></i>',
        ];
    }
}

if(!empty($arResult['DISPLAY_PROPERTIES']['MAP']['VALUE'])){

    $coordinates = explode(',', $arResult['DISPLAY_PROPERTIES']['MAP']['VALUE']);
    $arResult['ROUTE_INFO'] = [
        'lat' => $coordinates[0],
        'lng' => $coordinates[1],
        'scale' => $arResult['DISPLAY_PROPERTIES']['MAP_SCALE']['DISPLAY_VALUE'] ?? 10,
        'title' => $arResult['NAME']
    ];
}