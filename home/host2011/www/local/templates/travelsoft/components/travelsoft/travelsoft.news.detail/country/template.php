<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arTextFields = [
    'Язык' => $arResult['DISPLAY_PROPERTIES']['LANGUAGE']['DISPLAY_VALUE'],
    'Население' => $arResult['DISPLAY_PROPERTIES']["POPULATION"]["DISPLAY_VALUE"],
    'Климат' => $arResult['DISPLAY_PROPERTIES']["CLIMATE"]["DISPLAY_VALUE"],
    'География' => $arResult['DISPLAY_PROPERTIES']['GEOGRAFY']['DISPLAY_VALUE'],
    'Кухня' => $arResult['DISPLAY_PROPERTIES']['KITCHEN']['DISPLAY_VALUE'],
];
?>
<?
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/MapAdapter.js");
?>

        <div class="barba-container">
            <nav class="hotel-nav">
                <ul class="hotel-nav__menu nav nav-tabs" role="tablist">
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link active" data-toggle="tab" href="#country" role="tab" aria-controls="country" aria-selected="false">
                            <span>Информация</span>
                        </a>
                    </li>
                    <?if($arResult["SHOW_CITIES"]):?>
                        <li class="nav-item d-flex col-12 col-sm p-0">
                            <a class="nav-link" data-toggle="tab" href="#cities" role="tab" aria-controls="cities" aria-selected="true">
                                <span>Города и курорты</span>
                            </a>
                        </li>
                    <?endif?>
                </ul>
            </nav>
            <div class="tab-content">
                <div class="tab-pane active show" id="country" role="tabpanel">
                    <section>
                        <? if ($APPLICATION->GetDirProperty("SHOW_TITLE") == "N"): ?>
                            <h1 class="fw-md mb-1"><?= $APPLICATION->ShowTitle(false); ?></h1>
                        <?else:?>
                            <h3 class="fw-md mb-1">
                                <?=$arResult['NAME']?>
                            </h3>
                        <?endif?>
                        <ul class="hotel-title mb-3">
                            <li class="d-flex">
                                <div class="media-object"><i class="icon icon-label mr-2 text-primary"></i></div>
                                <div class="local">
                                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'])):?>
                                        <span>
                                            <?=$arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE']?>
                                        </span>
                                    <?endif;?>
                                    <?if(isset($arResult["ROUTE_INFO"]) && !empty($arResult["ROUTE_INFO"])):?>
                                        <i class="bullet mx-2"></i>
                                        <a href="#modalMap" data-location='<?=\Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"])?>' data-toggle="modal" data-title="<?=$arResult["NAME"]?>">
                                            Показать на карте
                                        </a>
                                    <?endif;?>
                                </div>
                            </li>
                        </ul>
                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'])):?>
                        <?$images = getSrc($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 825, 'height' => 500], NO_PHOTO_PATH_825_500)?>
                        <div class="hotel-gallery">
                            <div class="hotel-gallery__carousel swiper-container js-hotel-carousel">
                                <div class="swiper-wrapper">
                                    <?foreach($images as $image):?>
                                        <div class="swiper-slide">
                                            <img class="img-fluid img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                        </div>
                                    <?endforeach;?>
                                </div>
                                <?if(count($images) >  1):?>
                                    <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev" role="button">
                                            <i class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                    <path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/>
                                                    <path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/>
                                                </svg>
                                            </i>
                                        </a>
                                        <a class="hotel-gallery__arrow shadow-sm js-next" role="button">
                                            <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                    <path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/>
                                                    <path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/>
                                                </svg>
                                            </i>
                                        </a>
                                    </div>
                                <?endif;?>
                            </div>
                            <?if(count($images) >  1):?>
                                <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                                    <div class="swiper-wrapper">
                                        <?foreach($images as $image):?>
                                            <div class="swiper-slide">
                                                <a class="hotel-gallery__thumb js-gallery-link" href="<?=$image?>">
                                                    <img class="img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                                </a>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                        <?endif?>
                        <hr>
                        <div class="row">
                            <div class="col-12 d-flex">
                                <div class="hotel__intro hotel-card w-100">
                                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['DESCRIPTION_TOP']['DISPLAY_VALUE'])):?>
                                        <section class="mb-5">
                                            <p>
                                                <?=$arResult['DISPLAY_PROPERTIES']['DESCRIPTION_TOP']['DISPLAY_VALUE']?>
                                            </p>
                                        </section>
                                    <?endif?>
                                    <?foreach ($arTextFields as $name => $value):?>
                                        <?if(!empty($value)):?>

                                            <section class="mb-5">
                                                <h4><?=$name?></h4>
                                                <p><?=$value?></p>
                                            </section>

                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <?if($arResult["SHOW_CITIES"]):?>
                    <div class="tab-pane" id="cities" role="tabpanel">
                        <section>
                            <h3 class="fw-md">Города и курорты</h3>
                            <hr class="mb-4">
                            <div class="hotel-card">
                                <?if(!empty($arResult["CITIES"])):?>
                                    <div class="row">
                                        <?foreach($arResult["CITIES"] as $key => $city):?>

                                            <div class="col-sm-4">

                                                <div class="d-block" style="background-image: url(<?=$city["PICTURES"]?>);">
                                                    <div class="card-intro__footer">
                                                        <h4 class="h4 f-primary">
                                                            <a href="<?= $arResult['DETAIL_PAGE_URL'].$city["CODE"] ?>/"><?=$city['NAME']?></a>
                                                        </h4>
                                                    </div>
                                                </div>

                                            </div>

                                        <?endforeach;?>
                                    </div>
                                <?endif;?>
                            </div>
                        </section>
                    </div>
                <?endif?>
            </div>
        </div>

<script>

    $(document).ready(function () {
        var mapAdapter = new MapAdapter({
            map_id: "map",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });

        $('body').on("show.bs.modal","#modalMap", function (e) {

            var t=$(e.relatedTarget),o=t.data("title"),n=$(this);n.find(".modal-title .title").text(o),loc=t.data('location');

            if(typeof loc === "object" && loc.length > 1) {
                mapAdapter.drawRoute(loc);
            } else {
                mapAdapter.addMarker(loc[0]);
            }

        })

    });


</script>