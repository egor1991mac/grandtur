<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

                    <section>
<ul class="blog-post__meta">
<? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
                          <li class="date"><i class="icon icon-calendar"></i>
                            <time datetime="2017-01-18"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></time>
                          </li>
<? endif; ?>
            <? if (!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])): ?>
                          <li><i class="icon icon-label  mr-1"></i>
                            <ol class="blog-post__meta-tags">
                <? foreach ((array)$arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"] as $item): ?>
                    <li><span><?= strip_tags($item) ?></span>
                              </li>
                <? endforeach; ?>
                            </ol>
                          </li>
            <? endif; ?>
            <? if (!empty($arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"])): ?>
                          <li><i class="icon icon-label  mr-1"></i>
                            <ol class="blog-post__meta-tags">
                <? foreach ((array)$arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"] as $item): ?>
                    <li><span><?= strip_tags($item) ?></span>
                              </li>
                <? endforeach; ?>
                            </ol>
                          </li>
            <? endif; ?>
                        </ul>

                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'])):?>
                            <?$images = getSrc($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 825, 'height' => 500], NO_PHOTO_PATH_825_500)?>
                            <div class="hotel-gallery">
                                <div class="hotel-gallery__carousel swiper-container<?if(count($images) >  1):?> js-hotel-carousel<?endif?>">
                                    <div class="swiper-wrapper">
                                        <?foreach($images as $image):?>
                                            <div class="swiper-slide">
                                                <img class="img-fluid img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                    <?if(count($images) >  1):?>
                                        <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev" role="button">
                                                <i class="icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                        <path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/>
                                                        <path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/>
                                                    </svg>
                                                </i>
                                            </a>
                                            <a class="hotel-gallery__arrow shadow-sm js-next" role="button">
                                                <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                        <path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/>
                                                        <path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/>
                                                    </svg>
                                                </i>
                                            </a>
                                        </div>
                                    <?endif;?>
                                </div>
                                <?if(count($images) >  1):?>
                                    <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                                        <div class="swiper-wrapper">
                                            <?foreach($images as $image):?>
                                                <div class="swiper-slide">
                                                    <a class="hotel-gallery__thumb js-gallery-link" href="<?=$image?>">
                                                        <img class="img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                                    </a>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                <?endif;?>
                            </div>
                        <?endif?>
                        <div class="row">
                            <div class="col-12 d-flex">
                                    <section class="pb-4 pt-4">
										<? if (!empty($arResult["DISPLAY_PROPERTIES"]["ABOUT_OFFER"]["DISPLAY_VALUE"])): ?>
											<p><?= $arResult["DISPLAY_PROPERTIES"]["ABOUT_OFFER"]["DISPLAY_VALUE"] ?></p>
										<? endif; ?>
										<? if (!empty($arResult["DISPLAY_PROPERTIES"]["DESC"]["DISPLAY_VALUE"])): ?>
											<p><?= $arResult["DISPLAY_PROPERTIES"]["DESC"]["DISPLAY_VALUE"] ?></p>
										<? endif; ?>
										<? if (!empty($arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["DISPLAY_VALUE"])): ?>
											<h3>Необходимые документы</h3>
											<p><?= $arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["DISPLAY_VALUE"] ?></p>
										<? endif; ?>
										<? if (!empty($arResult["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"])): ?>
											<section id="video">
												<div class="thumb-wrap">
													<iframe width="100%" src="https://www.youtube.com/embed/<?= $arResult["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"] ?>?ecver=1" frameborder="0" allowfullscreen></iframe>
												</div>
											</section>
										<? endif; ?>
										<? if (!empty($arResult["DISPLAY_PROPERTIES"]["VIMEO"]["VALUE"])): ?>
											<section id="vimeo">
												<div class="thumb-wrap">
													<iframe width="100%" src="https://player.vimeo.com/video/<?= $arResult["DISPLAY_PROPERTIES"]["VIMEO"]["VALUE"] ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												</div>
											</section>
										<? endif; ?>
                                    </section>
                            </div>
                        </div>
                    </section>
