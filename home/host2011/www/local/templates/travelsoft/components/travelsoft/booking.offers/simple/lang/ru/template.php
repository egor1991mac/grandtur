<?php
$MESS["TRAVELBOOKING_BTN"] = "Бронировать";
$MESS["TRAVELBOOKING_DATE_TOUR"] = "Дата тура:";
$MESS["TRAVELBOOKING_SEARCH_TEXT"] = "<span style='color:red'>Идет поиск ценовых предложений...</span>";
$MESS["TRAVELBOOKING_OFFERS_NOT_FOUND"] = "<span style='color:red'>Ценовых предложений не найдено. Для уточнения цен и наличия мест свяжитесь  с менеджером #phone#.</span>";
$MESS["TRAVELBOOKING_NEAREST_AVAILABLE_TEXT"] = "По заданным критериям мы не смогли ничего подобрать. Возможно, Вам будут интересны следующие варианты:";
$MESS["TRAVELBOOKING_DATE_FROM_PLACEMENT"] = "Заселение:";
$MESS["TRAVELBOOKING_DATE_TO_PLACEMENT"] = "Выселение:";
$MESS["TRAVELBOOKING_DATE_FROM_TRAVEL"] = "Выезд:";
$MESS["TRAVELBOOKING_DATE_BACK_TRAVEL"] = "Приезд:";
$MESS["TRAVELBOOKING_NIGHTS"] = "Ночей:";
$MESS["TRAVELBOOKING_FOOD"] = "Питание:";
$MESS["TRAVELBOOKING_WITHOUT_FOOD"] = "не входит в стоимость";
$MESS["TRAVELBOOKING_NOT_AVAIL_BOOKING"] = "Бронирование временно недоступно. Бронирование будет доступно после активации Вас в системе как агента.";
$MESS["TRAVELBOOKING_READ_MORE"] = "Подробное описание";

        