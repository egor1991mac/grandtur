<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(true);
if (!$arResult["STATISTICS"]["total_count"]) {
    return;
}
?>

<div class="hotel-card hotel-review">
    <div class="row">

        <div class="hotel-review__total col-12 col-sm-4">
            <ul class="total d-flex mb-1">
                <li class="total__item">
                    <?= $arResult["STATISTICS"]["middle"] ?>
                </li>
                <li class="total__item">5</li>
            </ul>
            <p class="review">
                <?= GetMessage("TOTAL_REVIEWS") ?>:
                <b itemprop="reviewCount"><?= $arResult["STATISTICS"]["total_count"] ?></b>
            </p>
            <hr>
            <!--<p>Guest reviews are written by our customers after their stay at Hotel Ravena</p>-->
        </div>
        <div class="hotel-review__progress col-12 col-sm-8 js-point-progress">
            <? foreach ($arResult["STATISTICS"]["stars"] as $r_stars):
                $isBest = "";
                if ($r_stars["stars"] == Bitrix\Main\Config\Option::get("travelsoft.reviews", "MAX_RATING_VALUE")) {
                    $isBest = 'itemprop="bestRating"';
                }
                $isWorst = "";
                if ($r_stars["stars"] == 1) {
                    $isWorst = 'itemprop="worstRating"';
                }
                ?>
                <div class="item pr-3">
                    <div class="mb-1">
                        <div data-stars="<?= $r_stars["stars"] ?>" class="stars"></div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="progress progress--lg">
                            <div class="progress-bar bg-secondary" role="progressbar"
                                 style="width: <?= $r_stars["percent"] ?>%" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach ?>
        </div>
    </div>
</div>

<? $this->addExternalJs($templateFolder . "/_script.min.js", true) ?>

<script>

    window.reviewsStatisticsJsParameters = {
        total_stars_count: <?= Bitrix\Main\Config\Option::get("travelsoft.reviews", "MAX_RATING_VALUE") ?>
    };

</script>
