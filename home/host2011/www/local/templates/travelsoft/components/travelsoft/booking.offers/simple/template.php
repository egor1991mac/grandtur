<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$randString = randString(7);

$this->addExternalCss("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.css");
$this->addExternalJs("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.min.js");

if ($arResult["OFFERS"]):
    ?>
    <div class="offer-box container hello_world_1" id="offers-box-<?= $randString ?>">

        <div class="row">
            <div class="col-md-12 nearest-available-text order-1 order-md-0">

        <? if ($arResult["NEAREST_AVAILABLE"]): ?>

            <?= GetMessage("TRAVELBOOKING_NEAREST_AVAILABLE_TEXT") ?>

        <? endif ?>
        <?
        foreach ($arResult["OFFERS"] as $arOffer):
            $show_popup = (isset($arOffer["PICTURES"]) && !empty($arOffer["PICTURES"])) || strlen($arOffer["DESCRIPTION"]) > 0;
            $offer_rand_string = randString(5);
            ?>

            <div class="hotel-package mb-4">
                <div class="hotel-package__row row mb-4">
                    <div class="col-4 pr-md-1">
                        <a class="hotel-package__img d-block" href="javascript:void(0)">
                            <? if (isset($arOffer["PICTURE_PATH"]) && strlen($arOffer["PICTURE_PATH"]) > 0): ?>
                                <img class="img-fluid" src="<?= $arOffer["PICTURE_PATH"] ?>" alt="<?= $arOffer["NAME"] ?>">
                            <?else:?>
                                <img class="img-fluid" src="<?= NO_PHOTO_PATH_252_190 ?>" alt="<?= $arOffer["NAME"] ?>">
                            <? endif ?>
                        </a>
                    </div>
                    <div class="col-4">
                        <h4 class="hotel-package__title d-inline-block"><?= $arOffer["NAME"] ?></h4>
                        <ul class="hotel-package__props">
                            <? if (in_array($arResult["SERVICE_TYPE"], ["packageTour", "transfer", "transferback"])): ?>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL") ?></span> <span class="fw-bold"><?= $arOffer["DATE_FROM_TRANSFER"] ?></span></li>
                            <? endif ?>
                            <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): ?>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") ?></span> <span class="fw-bold"><?= $arOffer["DATE_FROM_PLACEMENT"] ?></span></li>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") ?></span> <span class="fw-bold"><?= $arOffer["DATE_TO_PLACEMENT"] ?></span></li>
                            <? endif; ?>
                            <? if ($arResult["SERVICE_TYPE"] === "packageTour"): ?>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL") ?></span> <span class="fw-bold"><?= $arOffer["DATE_BACK_TRANSFER"] ?></span></li>
                            <? endif; ?>
                            <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): ?>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_NIGHTS") ?></span> <span class="fw-bold"><?= $arOffer["NIGHTS"] ?></span></li>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_FOOD") ?></span> <span class="fw-bold"><?= ($arOffer["FOOD"]["NAME"] ? $arOffer["FOOD"]["NAME"] : GetMessage("TRAVELBOOKING_WITHOUT_FOOD")) ?></span></li>
                            <? endif; ?>
                            <? if ($arResult["SERVICE_TYPE"] === "excursionTour"): ?>
                                <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TOUR") ?></span> <span class="fw-bold"><?= $arOffer["DATE_FROM"] ?></span></li>
                            <? endif; ?>
                        </ul>
                        <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more" type="button"><?=GetMessage('TRAVELBOOKING_READ_MORE')?></button>
                    </div>
                    <div class="col-3 col-md-3 hotel-items__check">
                        <div class="py-3 js-sticky-top">
                            <ul class="text-danger mb-4">
                                <li class="price"><?= $arOffer["FORMATTED_PRICE"] ?></li>
                                <li class="price-usd"><?= $arOffer["FORMATTED_PRICE_USD"] ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-3 col-md-3 hotel-items__check">
                        <div class="py-3 js-sticky-top">
                            <p class="mb-2 offers-booking-btn-box"><span class="loading hidden_"></span><a rel="nofollow" class="btn-secondary btn--round btn-order add2cart btn btn-primary btn-lg" href="<?= $arResult["BOOKING_URL"] ?>?add2basket=<?= $arOffer["ADD2BASKET"] ?>&sessid=<?= bitrix_sessid() ?>" role="button"><?= GetMessage("TRAVELBOOKING_BTN") ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="collapse js-addition">
                    <div class="hotel-package__more">
                        <? if (strlen($arOffer["DESCRIPTION"]) > 0): ?>
                            <p class="mb-2"><?= $arOffer["DESCRIPTION"] ?></p>
                        <? endif ?>
                    </div>
                </div>
                <hr class="hr-bottom my-0">
            </div>

        <? endforeach; ?>

            </div>
        </div>
    </div>
    <script>Travelsoft.setModalShowHandler();</script>
<? else: ?>
    <div class="offer-box container" id="offers-box-<?= $randString ?>"><?= GetMessage("TRAVELBOOKING_SEARCH_TEXT") ?></div>
    <template id="offers-item-template-<?= $randString ?>">

        <div class="hotel-package mb-4">
            <div class="hotel-package__row row mb-4">
                <div class="col-4 pr-md-1">
                    <a rel="nofollow" class="hotel-package__img d-block" href="javascript:void(0)">
                        {{offer_preview_image}}
                    </a>
                </div>
                <div class="col-4">
                    <h4 class="hotel-package__title d-inline-block">{{offer_name}}</h4>
                    <ul class="hotel-package__props">
                        <? if (in_array($arResult["SERVICE_TYPE"], ["packageTour", "transfer", "transferback"])): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL") ?></span> <span class="fw-bold">{{offer_date_from_transfer}}</span></li>
                        <? endif ?>
                        <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") ?></span> <span class="fw-bold">{{offer_date_from_placement}}</span></li>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") ?></span> <span class="fw-bold">{{offer_date_to_placement}}</span></li>
                        <? endif; ?>
                        <? if ($arResult["SERVICE_TYPE"] === "packageTour"): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL") ?></span> <span class="fw-bold">{{offer_date_back_transfer}}</span></li>
                        <? endif; ?>
                        <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursionTour"])): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_NIGHTS") ?></span> <span class="fw-bold">{{offer_nights}}</span></li>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_FOOD") ?></span> <span class="fw-bold">{{offer_food}}</span></li>
                        <? endif; ?>
                        <? if ($arResult["SERVICE_TYPE"] === "excursionTour"): ?>
                            <li><span class="title mr-1"><?= GetMessage("TRAVELBOOKING_DATE_TOUR") ?></span> <span class="fw-bold">{{offer_date}}</span></li>
                        <? endif; ?>
                    </ul>
                    <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more" type="button"><?=GetMessage('TRAVELBOOKING_READ_MORE')?></button>
                </div>
                <div class="col-4 col-md-4 hotel-items__check">
                    <div class="py-3 js-sticky-top">
                        <ul class="text-danger mb-4">
                            <li class="price">{{offer_price}}</li>
                            <li class="price-usd">{{offer_price_usd}}</li>
                        </ul>
                        <p class="mb-2 offers-booking-btn-box"><span class="loading hidden_"></span><a rel="nofollow" class="btn-secondary btn--round btn-order add2cart btn btn-primary btn-lg" href="<?= $arResult["BOOKING_URL"] ?>?add2basket={{offer_add_to_basket}}&sessid=<?= bitrix_sessid() ?>" role="button"><?= GetMessage("TRAVELBOOKING_BTN") ?></a>
                        </p>
                    </div>
                </div>

            </div>
            {{offer_modal_box}}
            <hr class="hr-bottom my-0">
        </div>
    </template>
    <template id="nearest-available-text-template-<?= $randString ?>">
        <?= GetMessage("TRAVELBOOKING_NEAREST_AVAILABLE_TEXT") ?>
    </template>

    <template id="modal-box-template-<?= $randString ?>">
        <div class="collapse js-addition">
            <div class="hotel-package__more">
                {{offer_description_box}}
            </div>
        </div>
    </template>

    <template id="pictures-box-template-<?= $randString ?>">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="rslides" id="offer-carousel-{{rand_string}}">
                {{offer_pictures_items}}
            </ul>
        </div>
    </template>
    <template id="picture-item-template-<?= $randString ?>">
        <img class="img-fluid" src="{{offer_picture_path}}" alt="{{offer_name}}">
    </template>
    <template id="description-box-template-<?= $randString ?>">
        <p class="mb-2">{{offer_description}}</p>
    </template>

    <template id="preview-image-box-template-<?= $randString ?>">
        <div class="photo"><img src="{{offer_image}}" alt="{{offer_name}}"></div>
    </template>

    <script>
        Travelsoft.loadOffers({
            ajaxUrl: '<?= $componentPath . "/ajax/search_offers.php" ?>',
            request: '<?= json_encode(array("travelbooking" => $arResult["REQUEST"])) ?>',
            service_type: '<?= $arResult["SERVICE_TYPE"] ?>',
            insertion_selector: "#offers-box-<?= $randString ?>",
            offers_item_template_id_selector: "#offers-item-template-<?= $randString ?>",
            show_nearest_available_text: <? if (isset($_REQUEST["travelbooking"]) && !empty($_REQUEST["travelbooking"])): ?>true<? else: ?>false<? endif ?>,
                    nearest_available_text_template_id_selector: "#nearest-available-text-template-<?= $randString ?>",
                    modal_box_template_id_selector: "#modal-box-template-<?= $randString ?>",
                    pictures_box_template_id_selector: "#pictures-box-template-<?= $randString ?>",
                    picture_item_template_id_selector: "#picture-item-template-<?= $randString ?>",
                    description_box_template_id_selector: "#description-box-template-<?= $randString ?>",
                    preview_image_box_template_id_selector: "#preview-image-box-template-<?= $randString ?>",
                    messages: {
                        not_found: "<?= GetMessage("TRAVELBOOKING_OFFERS_NOT_FOUND", ["#phone#" => ""]) ?>",
                        no_food: "<?= GetMessage("TRAVELBOOKING_WITHOUT_FOOD") ?>"
                    }
                });
    </script>
<? endif; ?>
<script>
<? if ($_REQUEST["s2o"] === "Y"): ?>
        Travelsoft.scrolltoInit(document.querySelector(".offer-box"), 200);
<? endif ?>
    Travelsoft.add2cartInit(<? if ($arResult["IS_AVAIL_BOOKING"]): ?>true<? else: ?>false<? endif; ?>, "<?= GetMessage("TRAVELBOOKING_NOT_AVAIL_BOOKING") ?>");
</script>
