
/**
 * Component booking.search_form
 * Template "d3p.detail"
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */

(function (window) {

    var document = window.document;
    var bx = window.BX;
    var $ = window.jQuery;

    $(document).ready(function () {

        var childrenAgeTemplate = $("#children-age-template").html();

        function __removeDatepicker ($calendar) {
            $calendar.off("changeDate");
            $calendar.datepicker("remove");
        }

        function __initDatepickerInner($calendar, beforeShowDayHandler) {

            var options = {
                todayHighlight: true,
                format: 'dd.mm.yyyy',
                weekStart: 1,
                startDate: "today",
                autoclose: true,
                language: "RU"
            };

            if (typeof beforeShowDayHandler === "function") {
                options.beforeShowDay = beforeShowDayHandler;
            }

            $calendar.datepicker(options);
            $calendar.datepicker("setDate", $calendar.data("date-default"));

            if ($calendar.attr("name") === "travelbooking[date_from]") {
                $calendar.on("changeDate", function () {
                    var $date_from = $calendar.closest("form").find("input[name='travelbooking[date_from]']");
                    var $date_to = $calendar.closest("form").find("input[name='travelbooking[date_to]']");
                    $date_from.siblings('label').text($calendar.val());
                    $date_to.datepicker("setDate", $calendar.val());
                    $date_to.datepicker("show");
                });
            } else if ($calendar.attr("name") === "travelbooking[date_to]") {
                $calendar.on("changeDate", function () {
                    var $date_from = $calendar.closest("form").find("input[name='travelbooking[date_from]']");
                    var $date_to = $calendar.closest("form").find("input[name='travelbooking[date_to]']");
                    var arr_date_from = $date_from.val().split(".");
                    var arr_date_to = $calendar.val().split(".");
                    if ((new Date(arr_date_from[2], arr_date_from[1], arr_date_from[0])).getTime()
                        > (new Date(arr_date_to[2], arr_date_to[1], arr_date_to[0])).getTime()) {
                        $date_from.val($calendar.val());
                    }
                    $date_to.siblings('label').text($calendar.val());
                });
            }

        }

        function __datesHighlightInner(calendar_data) {

            var $calendar = $("input[name='travelbooking[date_from]']");
            __removeDatepicker($calendar);
            $.post("/local/templates/travelsoft/components/travelsoft/booking.search_form/tours-detail/ajax/get_dates_for_highlight_all_tours.php", {
                sessid: bx.bitrix_sessid(), services_id: [calendar_data]}, function (arr_dates) {
console.log(arr_dates);
                if ($.isArray(arr_dates) && arr_dates.length) {

                    __initDatepickerInner($calendar, function (date) {

                        var day = date.getDate().toString();

                        var month = (date.getMonth() + 1).toString();

                        var year = date.getFullYear().toString();

                        var d_parts = [];

                        var to_highlight = false;

                        if (Number(day) < 10) {
                            day = `0${day}`;
                        }

                        if (Number(month) < 10) {
                            month = `0${month}`;
                        }

                        for (var i = 0; i < arr_dates.length; i++) {
                            var d_parts = arr_dates[i].split(".");

                            if (d_parts[0] === day && d_parts[1] === month && d_parts[2] === year) {

                                console.log(d_parts, 1);

                                to_highlight = true;
                                //break;
                            }

                        }
                        return to_highlight ? {classes: 'travelsoft-highlight-date'} : {classes: ''};

                    });

                } else {
                    __initDatepickerInner($calendar);
                }

            }).fail(function () {
                __initDatepickerInner($calendar);
            });

        }

        $.fn.datepicker.dates.RU = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четвер", "Пятница", "Суббота"],
            daysShort: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
            daysMin: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек."],
            today: "Сегодня",
            clear: "Очистить",
            format: "dd.mm.yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        $("select[name='travelbooking[children]']").on("change", function () {

            var $this = $(this).closest("select");
            var children = $this.val();
            var childrenAgeBox = $this.closest("form").find(".children-age-box");
            childrenAgeBox.find(".select-age-box").each(function () {
                $(this).remove();
            });
            childrenAgeBox.addClass("hidden_");
            if (children > 0) {
                for (var i = 1; i <= children; i++) {
                    childrenAgeBox.append(childrenAgeTemplate.replace("{{index}}", i));
                }
                childrenAgeBox.removeClass("hidden_");
            }

        });

        $(".closer").on("click", function () {
            $(this).parent().addClass("hidden_");
        });

        $(".search-btn").on("click", function () {
            $(this).addClass("hidden_").prev("span.loading").removeClass("hidden_");
        });

        $(".input-datepicker-inner-form").each(function () {
            __initDatepickerInner($(this));
        });

        /*$('body').on("click", "label.date", function () {
            var for_date = $(this).attr('for');
            var calendar_ = $("#inner-offers-search-form").find('#'+for_date);
            calendar_.datepicker("show");
            console.log(calendar_);
            //$('#'+for_date).trigger('click');
        });*/

        $("select#tour_id").on("change", function () {

            var $this = $(this);
            var services = $this.val();
            __datesHighlightInner(services);

        });

    });
})(window);

