<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="intro__bg js-intro-bg">
    <div class="over"></div>
    <div class="swiper-container js-intro-slider-bg">
        <div class="swiper-wrapper">

            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 1920, 'height' => 1330], NO_PHOTO_PATH_1920_1330, 1) ?>

                <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <img class="img-cover" src="<?= $images[0] ?>" alt="<?= $arItem['NAME'] ?>">
                </div>

            <? endforeach; ?>
        </div>
    </div>
</div>
<div class="intro__desc js-intro-slider-desc swiper-container">
    <div class="swiper-wrapper align-items-center">

        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <div class="swiper-slide">
                <div class="container w-100 text-center">
                    <div class="row">
                        <div class="col-12 col-lg-10 mx-auto">

                            <h4 class="h4 intro__title prlx-scroll">
                                <?= $arItem['NAME'] ?>
                            </h4>

                            <? if (!empty($arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']["DISPLAY_VALUE"])): ?>
                                <p class="prlx-scroll">
                                    <?= $arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']["DISPLAY_VALUE"] ?>
                                </p>
                            <? endif; ?>

                        </div>
                    </div>
                </div>
            </div>

        <? endforeach; ?>

    </div>
    <button class="intro__btn-scroll js-intro-btn-jump" type="button"><span></span></button>
</div>