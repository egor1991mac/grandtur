<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

?>
<form id="transfers-booking-form-search" class="search-hotels__form" action="/transfers/" method="get">
    <input type="hidden" name="s2o" value="Y">
    <input type="hidden" name="travelbooking[tickets_buy]" value="1">
    <input type="hidden" name="travelbooking[rates_id][]" value="">
    <div class="hotel-search-form">
        <div class="row">

            <div class="form-group col-12 col-md-6 col-lg-6 col-xl-4 flex-between flex-wrap">
                <label class="label-text label-on-input"><?= GetMessage("TRAVELBOOKING_TRANSFERS") ?></label>
                <div class="d-flex select_2 w-100">
                    <span class="select-local d-flex">
                        <select id="rates" name="rates" class="form-control select-2 js-select-locality" data-placeholder="<?= GetMessage("TRAVELBOOKING_TRANSFERS_PLACEHOLDER") ?>">
                            <option></option>
                            <?foreach($arResult["TRANSFER_RATES"] as $arRate):?>
                                <option value="~<?= $arRate["ID"]?>"><?= $arRate["UF_NAME"]?></option>
                            <?endforeach?>
                        </select>
                    </span>
                </div>
            </div>
            <div class="form-group col-12 col-md-6 col-lg-3 col-xl-3 d-flex justify-content-center flex-wrap">
                <div class="form-group-date text-nowrap text-center flex-around flex-wrap">
                    <div class="d-inline-block">
                        <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_TRANSFERS_DATE_FROM") ?></label>
                        <div class="input-date-group position-relative"><i class="mr-2 icon icon-calendar text-secondary"></i>
                            <input id="transfer_date" data-service-type="rates" data-date-default="<?= $arResult["DATE_FROM"]?>" autocomplete="off" value="<?= $arResult["DATE_FROM"] ?>" class="input-datepicker-form form-control js-input-date hidden" name="travelbooking[date]" type="text" />
                            <label class="form-control date" for="transfer_date"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 col-md-6 col-lg-3 col-xl-3 d-flex justify-content-center">
                <div class="d-flex flex-wrap w-50">
                    <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_ADULTS") ?></label>
                    <div class="d-flex w-100">
                        <select name="travelbooking[adults]" class="form-control styled select-size">
                            <option <? if ($arResult["ADULTS"] === 1): ?>selected<? endif ?> value="1">1</option>
                            <option <? if ($arResult["ADULTS"] === 2): ?>selected<? endif ?> value="2">2</option>
                            <option <? if ($arResult["ADULTS"] === 3): ?>selected<? endif ?> value="3">3</option>
                            <option <? if ($arResult["ADULTS"] === 4): ?>selected<? endif ?> value="4">4</option>
                        </select>
                    </div>
                </div><i class="bullet empty dot-style-older"></i>
                <div class="d-flex flex-wrap w-50">
                    <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_CHILDREN") ?></label>
                    <div class="d-flex w-100">
                        <select class="form-control styled select-size" name="travelbooking[children]">
                            <option value="0"><?= GetMessage("TRAVELBOOKING_WITHOUT_CHILDREN") ?></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <div class="children-age-box hidden_"><div class="closer">&times;</div></div>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 col-xl-2 d-flex">
                <button class="search-btn btn btn-secondary btn-default align-self-center sea_id" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?>
                </button>
            </div>
        </div>
    </div>
</form>
