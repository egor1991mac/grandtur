<?php

if ($arParams["SERVICE_TYPE"] === "packagetour" && $arParams["OBJECT_CODE"]) {

    $exc = CIBlockElement::GetList(false, ["CODE" => $arParams["OBJECT_CODE"]], FALSE, FALSE)->GetNextElement();

    if ($exc) {;
        $props = $exc->getProperties();
        
        if (!empty(@$props["NIGHT_COUNT"]["VALUE"])) {
            $arResult["NIGHTS_COUNT"] = $props["NIGHT_COUNT"]["VALUE"];
            sort($arResult["NIGHTS_COUNT"]);
        }
    }
}
