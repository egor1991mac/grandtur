<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["PROPERTIES"]["MAP"]["VALUE"])) {

    $LATLNG = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);

    $arResult['ROUTE_INFO'][] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arResult["NAME"],
    );

}

$arResult["SHOW_CITIES"] = false;
$arResult["CITIES"] = array();
$cities_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>CITIES_ID_IBLOCK, "ACTIVE"=>"Y", "PROPERTY_COUNTRY"=>$arResult["ID"]), false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL","CODE"));
if($cities_db->SelectedRowsCount() >= 1){
    $arResult["SHOW_CITIES"] = true;
    while($ob = $cities_db->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        $arResult["CITIES"][$arFields["ID"]] = array(
            "NAME" => $arFields["NAME"],
            "CODE" => $arFields["CODE"],
            "DETAIL_PAGE_URL" => $arFields["DETAIL_PAGE_URL"],
            "PICTURES" => current(getSrc($arProps['PICTURES']['VALUE'], ['width' => 252, 'height' => 190], NO_PHOTO_PATH_252_190))
        );
    }
}