<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

?>
Ведуться работы
<form action="#" method="get" class="search-hotels__form" data-toggle="validator">
    <input type="hidden" name="s2o" value="Y">
    <input type="hidden" name="arrFilter_169_2889884971" value="Y">
    <input type="hidden" name="searchUrl" value="<?= $arParams["ACTION_URL"] ?>">
    <div class="hotel-search-form hello world">
        <div class="row">
            <div class="form-group col-12 col-md-12 col-lg-6 col-xl-5 flex-between flex-wrap">
                <div class="d-flex flex-wrap width-40">
                    <label class="label-text label-on-input w-100"><?= GetMessage("TRAVELBOOKING_TOURS_CITY") ?></label>
                    <div class="d-flex select_2 w-100">
						<span class="select-local d-flex">
                            <select name="arrFilter_158" id="cities" class="form-control select-2 js-select-locality"
                                    data-placeholder="<?= GetMessage("TRAVELBOOKING_TOURS_CITY_PLACEHOLDER") ?>">
                                <option></option>
                                <? foreach ($arResult["CITIES"] as $arCity): ?>
                                    <option value="Y"
                                            data-val="<?= abs(crc32($arCity["ID"])) ?>"><?= $arCity["NAME"] ?></option>
                                <? endforeach ?>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="d-flex flex-wrap width-55">
                    <label class="label-text label-on-input w-100"><?= GetMessage("TRAVELBOOKING_TOURS") ?></label>
                    <div class="d-flex select_2 w-100">
                        <span class="select-local d-flex ">
                            <select id="sea_id" name="" class="form-control select-2 js-select-locality"
                                    data-placeholder="<?= GetMessage("TRAVELBOOKING_TOURS_PLACEHOLDER") ?>">
                                <option></option>
                                <? /*foreach($arResult["TOURS"] as $arTour):*/ ?><!--
                                    <option value="<? /*= $arTour["DETAIL_PAGE_URL"]*/ ?>~<? /*= $arTour["ID"]*/ ?>"><? /*= $arTour["NAME"]*/ ?></option>
                                --><? /*endforeach*/ ?>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3 d-flex justify-content-center flex-wrap">
                <div class="form-group-date text-nowrap text-center flex-around flex-wrap">
                    <div class="d-flex flex-wrap w-50 justify-content-center">
                        <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_DATE_FROM") ?></label>
                        <div class="input-date-group position-relative"><i
                                    class="mr-2 icon icon-calendar text-secondary"></i>
                            <input id="sea_date_from" data-service-type="transfer"
                                   data-date-default="<?= $arResult["DATE_FROM"] ?>" autocomplete="off"
                                   value="<?= $arResult["DATE_FROM"] ?>"
                                   class="input-datepicker-form form-control js-input-date hidden"
                                   name="travelbooking[date_from]" type="text"/>
                            <label class="form-control date" for="sea_date_from"></label>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap w-50 justify-content-center ts-d-none">
                        <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_DATE_TO") ?></label>

                        <?
                        $adults = $_GET["travelbooking"]["adults"] > 0 ? intVal($_GET["travelbooking"]["adults"]) : $arResult["ADULTS"];
                        $arr_nights = [];

                        if (isset($arResult["NIGHTS_COUNT"]) && !empty($arResult["NIGHTS_COUNT"])) {
                            $arr_nights = $arResult["NIGHTS_COUNT"];
                        } else {
                            $arr_nights = [3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14];
                        }
                        ?>

                        <select name="night" class="form-control styled select-size">
                            <? foreach ($arr_nights as $night): ?>
                                <option <? if ($night == @$_GET["night"]): ?>selected<? endif ?>
                                        value="<?= $night ?>"><?= $night ?></option>
                            <? endforeach ?>

                        </select>

                    </div>
                    <div class="ts-d-none">
                        <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_DATE_TO") ?></label>
                        <div class="input-date-group position-relative"><i
                                    class="mr-2 icon icon-calendar text-secondary"></i>
                            <input id="sea_date_to" data-service-type="transferback"
                                   data-date-default="<?= $arResult["DATE_TO"] ?>" autocomplete="off"
                                   value="<?= $arResult["DATE_TO"] ?>"
                                   class="input-datepicker-form form-control js-input-date hidden"
                                   name="travelbooking[date_to]" type="text"/>
                            <label class="form-control date" for="sea_date_to"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 col-md-6 col-lg-2 col-xl-2 d-flex justify-content-center">
                <div class="d-flex flex-wrap w-50">
                    <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_ADULTS") ?></label>
                    <div class="d-flex w-100">
                        <select name="travelbooking[adults]" class="form-control styled select-size">
                            <option <? if ($arResult["ADULTS"] === 1): ?>selected<? endif ?> value="1">1</option>
                            <option <? if ($arResult["ADULTS"] === 2): ?>selected<? endif ?> value="2">2</option>
                            <option <? if ($arResult["ADULTS"] === 3): ?>selected<? endif ?> value="3">3</option>
                            <option <? if ($arResult["ADULTS"] === 4): ?>selected<? endif ?> value="4">4</option>
                        </select>
                    </div>
                </div>
                <i class="bullet empty dot-style-older"></i>
                <div class="d-flex flex-wrap w-50">
                    <label class="label-text w-100"><?= GetMessage("TRAVELBOOKING_CHILDREN") ?></label>
                    <div class="d-flex w-100">
                        <select class="form-control styled select-size" name="travelbooking[children]">
                            <option value="0"><?= GetMessage("TRAVELBOOKING_WITHOUT_CHILDREN") ?></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <div class="children-age-box hidden_">
                            <div class="closer">&times;</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 col-xl-2 d-flex">
                <button class="search-btn btn btn-secondary btn-default align-self-center sea_id"
                        type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?>
                </button>
            </div>
        </div>
    </div>
</form>

<script>

    var objects_for_city_sea = <?=json_encode($arResult["CITIES_SEARCH_OBJECT"])?>;

    var dateTo;
    var selected;
    var result;

    setTimeout(function(e){
        dateTo = document.querySelector('[name="travelbooking[date_from]"]').value;
        selected = parseInt($('[name="night"] :selected').val()) + 1;
        result = moment(dateTo,'DD.MM.YYYY').add(parseInt(selected),'days').format('DD.MM.YYYY');
        $('[name="travelbooking[date_to]"]').val(result);

    },1000);

    $('[name="night"]').change(function(){
        dateTo = document.querySelector('[name="travelbooking[date_from]"]').value;
        selected = parseInt($('[name="night"] :selected').val()) + 1;
        result = moment(dateTo,'DD.MM.YYYY').add(parseInt(selected),'days').format('DD.MM.YYYY');
        $('[name="travelbooking[date_to]"]').val(result);
        console.log( $('[name="travelbooking[date_to]"]').val());
    });
    document.addEventListener('click',function (e) {
        console.log(e.target);
    });
    $('body').on("change", "select#cities", function () {

        var $this = $(this);

        var value_id = $(this).find(':selected').data('val');
        var value_name = $this.attr('name');

        $('select#cities').attr("name", value_name + '_' + value_id);

        var form = $this.closest("form");

        var objects = objects_for_city_sea[value_id];

        var option_objects = '<option value="#object_id#">#object_name#</option>';

        var htmlObjects = '<option></option>';

        for (object_ in objects) {

            option_objects_ = option_objects;
            option_objects_ = option_objects_.replace("#object_id#", objects[object_]["DETAIL_PAGE_URL"] + '~' + objects[object_]["ID"]);
            option_objects_ = option_objects_.replace("#object_name#", objects[object_]["NAME"]);
            htmlObjects += option_objects_;

        }

        $('#sea_id').select2('destroy');
        form.find("#sea_id").html(htmlObjects);
        $('#sea_id.select-2').select2({
            allowClear: true,
            formatNoMatches: function () {
                return "Ничего не найдено";
            },
            placeholder: $this.data("placeholder")
        });



    });

</script>