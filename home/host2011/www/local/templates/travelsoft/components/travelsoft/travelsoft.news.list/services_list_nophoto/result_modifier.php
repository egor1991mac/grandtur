<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$iblockTown = 0;
$town = array();
foreach ($arResult["ITEMS"] as $key=>$arItem) {

    if (!empty($arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]) && !in_array($arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"],$town)) {
        if($iblockTown == 0){
            $iblockTown = $arItem["PROPERTIES"]["POINT_DEPARTURE"]["LINK_IBLOCK_ID"];
        }
        $town[] = $arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"];
    }
    if (!empty($arItem["PROPERTIES"]["TOWN"]["VALUE"])) {
        if($iblockTown == 0){
            $iblockTown = $arItem["PROPERTIES"]["TOWN"]["LINK_IBLOCK_ID"];
        }
        foreach ($arItem["PROPERTIES"]["TOWN"]["VALUE"] as $k=>$arTown) {
            if(!in_array($arTown,$town)){
                $town[] = $arTown;
            }
        }
    }

}

if(!empty($town) && $iblockTown != 0) {
    $arResult["TOWN"] = array();
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $iblockTown, "ID" => $town, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "PROPERTY_MAP"));
    while ($ob = $res->GetNext()) {
        $arResult["TOWN"][$ob["ID"]] = array("NAME" => $ob["NAME"], "MAP" => $ob["PROPERTY_MAP_VALUE"]);
    }

    foreach ($arResult["ITEMS"] as $key=>$arItem) {

        if (!empty($arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"])) {

            $LATLNG = explode(",", $arResult["TOWN"][$arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]["MAP"]);

            $arResult["ITEMS"][$key]['ROUTE_INFO'][] = array(
                "lat" => $LATLNG[0],
                "lng" => $LATLNG[1],
                "title" => $arResult["TOWN"][$arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"]]['NAME']
            );
        }

        if (!empty($arItem["PROPERTIES"]["TOWN"]["VALUE"])) {

            foreach ($arItem["PROPERTIES"]["TOWN"]["VALUE"] as $k=>$arTown) {

                if($arItem["PROPERTIES"]["POINT_DEPARTURE"]["VALUE"] != $arTown) {

                    $LATLNG = explode(",", $arResult["TOWN"][$arTown["PROPERTIES"]["MAP"]["VALUE"]]["MAP"]);

                    $arResult["ITEMS"][$key]['ROUTE_INFO'][] = array(
                        "lat" => $LATLNG[0],
                        "lng" => $LATLNG[1],
                        "title" => $arResult["TOWN"][$arTown["PROPERTIES"]["MAP"]["VALUE"]]['NAME'],
                    );
                }

            }

        }

    }

}