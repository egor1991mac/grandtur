<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arListFields = [
    'Адрес' => $arResult['DISPLAY_PROPERTIES']['HD_ADDRESS']['DISPLAY_VALUE'],
    'Адрес на языке страны' => $arResult['DISPLAY_PROPERTIES']['HD_ADDRESS_COUNTRY_LANGUAGE']['DISPLAY_VALUE'],
    'Телефон' => $arResult['DISPLAY_PROPERTIES']['HD_PHONE']['DISPLAY_VALUE'],
    'Факс' => $arResult['DISPLAY_PROPERTIES']['HD_FAX']['DISPLAY_VALUE'],
    'E-mail' => $arResult['DISPLAY_PROPERTIES']['HD_EMAIL']['DISPLAY_VALUE'],
    'Сайт' => $arResult['DISPLAY_PROPERTIES']['HD_HTTP']['DISPLAY_VALUE'],
    'Регион' => $arResult['DISPLAY_PROPERTIES']['REGIONS']['DISPLAY_VALUE'],
    'Тип путевки' => $arResult['DISPLAY_PROPERTIES']['TYPE_PERMIT']['DISPLAY_VALUE'],
    'Спортивный комплекс' => $arResult['DISPLAY_PROPERTIES']['SPORTS_COMPLEX']['DISPLAY_VALUE'],
];

$arTextFields = [
    'Описание' => $arResult['DISPLAY_PROPERTIES']['HD_DESC']['DISPLAY_VALUE'],
    'Таблица цен' => $arResult['DISPLAY_PROPERTIES']['PRICE_TABLE']['DISPLAY_VALUE'],
    'Медицинский профиль' => $arResult['DISPLAY_PROPERTIES']['MEDPROFIL']['DISPLAY_VALUE'],
    'Национальный парк' => $arResult['DISPLAY_PROPERTIES']['NATIONAL_PARK']['DISPLAY_VALUE'],
    'Конференц-зал' => $arResult['DISPLAY_PROPERTIES']['CONFERENCE_HALL']['DISPLAY_VALUE'],
    'Условия отмены' => $arResult['DISPLAY_PROPERTIES']['CANCELLATION']['DISPLAY_VALUE'],
    'Проезд' => $arResult['DISPLAY_PROPERTIES']['PROEZD']['DISPLAY_VALUE'],
    'Описание номеров' => $arResult['DISPLAY_PROPERTIES']['HD_DESCROOM']['DISPLAY_VALUE'],
    'Описание питания' => $arResult['DISPLAY_PROPERTIES']['HD_DESCMEAL']['DISPLAY_VALUE'],
    'Описание услуг' => $arResult['DISPLAY_PROPERTIES']['HD_DESCSERVICE']['DISPLAY_VALUE'],
    'Спорт и отдых' => $arResult['DISPLAY_PROPERTIES']['HD_DESCSPORT']['DISPLAY_VALUE'],
    'Оздоровление' => $arResult['DISPLAY_PROPERTIES']['HD_DESCSHEALTH']['DISPLAY_VALUE'],
    'Услуги для детей' => $arResult['DISPLAY_PROPERTIES']['HD_DESCCHILD']['DISPLAY_VALUE'],
    'Пляж' => $arResult['DISPLAY_PROPERTIES']['HD_DESCBEACH']['DISPLAY_VALUE'],
    'Лечебная база' => $arResult['DISPLAY_PROPERTIES']['MEDICAL_TREATMENT']['DISPLAY_VALUE'],
    'Дополнительная информация' => $arResult['DISPLAY_PROPERTIES']['HD_ADDINFORMATION']['DISPLAY_VALUE'],
    'Инфраструктура' => $arResult['DISPLAY_PROPERTIES']['INFRASTRUCTURE']['DISPLAY_VALUE'],
    'В стоимость входит' => $arResult['DISPLAY_PROPERTIES']['INCLUDED_IN_PRICE']['DISPLAY_VALUE'],
    'Дополнительно оплачивается' => $arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PAID']['DISPLAY_VALUE'],
];

$arTextFieldsAccommodation = [
    'Заселение' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['CHECK_IN']['DISPLAY_VALUE'],"ICON"=>"icon-calendar"),
    'Выселение' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['CHECK_OUT']['DISPLAY_VALUE'],"ICON"=>"icon-calendar"),
    'Условия отмены' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['CANCELLATION']['DISPLAY_VALUE'],"ICON"=>"icon-info"),
    'Дополнительно оплачивается' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PAID']['DISPLAY_VALUE'],"ICON"=>"icon-info"),
    'Размещение детей' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['ACCOMMODATION_CHILDREN']['DISPLAY_VALUE'],"ICON"=>"icon-family"),
    'Домашние питомцы' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['PETS']['DISPLAY_VALUE'],"ICON"=>"icon-pets"),
    'К оплате принимаются' => array("TEXT"=>$arResult['DISPLAY_PROPERTIES']['ACCEPTED_PAYMENT']['DISPLAY_VALUE'],"ICON"=>"icon-credits"),
];


?>

        <div class="barba-container">
            <nav class="hotel-nav">
                <ul class="hotel-nav__menu nav nav-tabs" role="tablist">
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link active" data-toggle="tab" href="#hotel" role="tab" aria-controls="hotel" aria-selected="false">
                            <span>Информация об отеле и номерах</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#hotelServices" role="tab" aria-controls="hotelServices" aria-selected="true">
                            <span>Оснащение и услуги</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#hotelAccommodation" role="tab" aria-controls="hotelAccommodation" aria-selected="true">
                            <span>Условия проживания</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col-12 col-sm p-0">
                        <a class="nav-link" data-toggle="tab" href="#hotelReviews" role="tab" aria-controls="hotelReviews" aria-selected="true">
                            <span>Отзывы гостей</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="tab-content">
                <div class="tab-pane active show" id="hotel" role="tabpanel">
                    <section>
                        <? if ($APPLICATION->GetDirProperty("SHOW_TITLE") == "N"): ?>
                            <h1 class="fw-md mb-1"><?= $APPLICATION->ShowTitle(false); ?></h1>
                        <?else:?>
                            <h3 class="fw-md mb-1">
                                <?=$arResult['NAME']?>
                            </h3>
                        <?endif?>
                        <ul class="hotel-title mb-3">
                            <li class="mb-2">
                                <?if(!empty($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])):?>
                                    <?=star_rating($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])?>
                                <?endif;?>
                            </li>
                            <li class="d-flex">
                                <div class="media-object"><i class="icon icon-label mr-2 text-primary"></i></div>
                                <div class="local">
                                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'])):?>
                                        <span>
                                            <?=$arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE']?>

                                            <?if(!empty($arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])):?>
                                                <!-- TODO: доделать city url -->
                                                , <?=$arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']?>
                                            <?endif;?>
                                        </span>
                                    <?endif;?>
                                    <i class="bullet mx-2"></i>
                                    <a href="#modalMap" data-toggle="modal" data-title="Ravena Best Hotel">
                                        Показать на карте
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'])):?>
                            <?$images = getSrc($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 825, 'height' => 500], NO_PHOTO_PATH_825_500)?>
                            <div class="hotel-gallery">
                                <div class="hotel-gallery__carousel swiper-container<?if(count($images) >  1):?> js-hotel-carousel<?endif?>">
                                    <div class="swiper-wrapper">
                                        <?foreach($images as $image):?>
                                            <div class="swiper-slide">
                                                <img class="img-fluid img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                    <?if(count($images) >  1):?>
                                        <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev" role="button">
                                                <i class="icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                        <path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/>
                                                        <path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/>
                                                    </svg>
                                                </i>
                                            </a>
                                            <a class="hotel-gallery__arrow shadow-sm js-next" role="button">
                                                <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                                        <path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/>
                                                        <path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/>
                                                    </svg>
                                                </i>
                                            </a>
                                        </div>
                                    <?endif;?>
                                </div>
                                <?if(count($images) >  1):?>
                                    <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                                        <div class="swiper-wrapper">
                                            <?foreach($images as $image):?>
                                                <div class="swiper-slide">
                                                    <a class="hotel-gallery__thumb js-gallery-link" href="<?=$image?>">
                                                        <img class="img-cover" src="<?=$image?>" alt="<?$arResult['NAME']?>"/>
                                                    </a>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                <?endif;?>
                            </div>
                        <?endif?>
                        <hr>
                        <div class="row">
                            <div class="col-12 d-flex">
                                <div class="hotel__intro">
                                    <section class="pb-1">
                                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['CAT_ID']['DISPLAY_VALUE'])):?>
                                            <h5 class="d-flex align-items-center text-primary mb-3"><i class="icon icon-badge mr-2"></i>
                                                <?=$arResult['DISPLAY_PROPERTIES']['CAT_ID']['DISPLAY_VALUE']?>
                                            </h5>
                                        <?endif;?>
                                        <div class="row">
                                            <div class="col-12 col-md-8">
                                                <ul class="booking-card__checklist">
                                                    <?foreach ($arListFields as $name => $value):?>
                                                        <?if(!empty($value)):?>
                                                            <li class="justify-content-between flex-wrap flex-column flex-sm-row mb-1">
                                                                <div class="fw-sm-element">
                                                                    <?=$name?>:
                                                                <span>
                                                                    <?=$value?>
                                                                </span>
                                                                </div>
                                                            </li>
                                                        <?endif;?>
                                                    <?endforeach;?>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                    <?foreach ($arTextFields as $name => $value):?>
                                        <?if(!empty($value)):?>

                                            <section class="mb-1">
                                                <h4><?=$name?></h4>
                                                <p><?=$value?></p>
                                            </section>

                                        <?endif;?>
                                    <?endforeach;?>
                                        <section class="mb-5">
                                            <?dm($arResult['DISPLAY_PROPERTIES']['VIDEO']['DISPLAY_VALUE']);?>
                                            <?dm($arResult['DISPLAY_PROPERTIES']['FILE']['DISPLAY_VALUE']);?>
                                        </section>
                                </div>
                            </div>
                        </div>
                        <div class="hotel-items">
                        <?// ФОРМА ПОИСКА ЦЕНОВЫХ ПРЕДЛОЖЕНИЙ
                        if ($GLOBALS["SEARCH_FORM_HTML"]):?>
                            <div class="cart-header">
                                <h4 class="card-title">Бронирование</h4>
                                <hr>
                            </div>
                            <div class="row"><div class="col-md-12"><? echo $GLOBALS["SEARCH_FORM_HTML"]; ?></div></div>
                        <?endif?>
                        <?// вывод цен из travelbooking по размещению
                        if ($GLOBALS["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"]):?>
                            <div class="row"><div class="col-md-12">
                                    <!--<div class="cart-header">
                                        <h4 class="card-title">Номера</h4>
                                        <hr>
                                    </div>-->
                                    <?echo $GLOBALS["SEARCH_PLACEMENTS_OFFERS_RESULT_HTML"]; ?>
                                </div>
                            </div>

                        <?endif?>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="hotelServices" role="tabpanel">
                    <section>
                        <h3 class="fw-md">Услуги в отеле</h3>
                        <hr class="mb-4">
                        <div class="">
                            <?if(!empty($arResult["SERVICES"])):?>
                                <ul class="hotel-facilities d-flex flex-wrap flex-column flex-md-row mb-3">
                                    <?foreach ($arResult["SERVICES"] as $service):?>
                                        <li class="mr-2">
                                            <?=$service['ICON']?>
                                            <span class="align-middle mr-2"><?=$service['NAME']?></span>
                                            <i class="bullet d-none d-md-inline-block"></i>
                                        </li>
                                    <?endforeach;?>
                                </ul>
                            <?endif;?>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="hotelAccommodation" role="tabpanel">
                    <section>
                        <h3 class="fw-md">Условия проживания</h3>
                        <hr class="mb-4">
                        <div class="hotel-checklist">
                            <?foreach ($arTextFieldsAccommodation as $k=>$value):?>
                                <div class="row">
                                    <div class="col-12 col-sm-4">
                                        <h5 class="title d-flex"><i class="icon <?=$value["ICON"]?> mr-2"></i><?=$k?>:</h5>
                                    </div>
                                    <div class="col-12 col-sm-8">
                                        <?if($value["ICON"] == "icon-credits"):?>
                                            <ul class="hotel-checklist__payment">
                                                <?foreach ($value["TEXT"] as $icon):?>
                                                    <li class="d-inline-block mr-2 mb-2"><img src="<?=SITE_TEMPLATE_PATH?>/img/<?=$icon?>.jpg" alt="<?=$icon?>"></li>
                                                <?endforeach;?>
                                            </ul>
                                        <?else:?>
                                            <p><?=$value["TEXT"]?></p>
                                        <?endif?>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </section>
                    <section class="hotel-popular">
                        <h3>Популярные отели в этом районе</h3>
                        <hr class="mb-4">

                        <?// Set in component prolog?>
                        <?= $arParams['SIMILAR_HOTELS'] ?>

                    </section>
                </div>
                <div class="tab-pane" id="hotelReviews" role="tabpanel">
                    <section>

                        <?// Set in component prolog?>
                        <?=$arParams['REVIEWS']?>

                    </section>
                </div>
            </div>
        </div>
