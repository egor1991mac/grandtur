<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

/**
 * /local/php_interface/include/functions.php
 */
$arResult["PLACEMENTS"] = \getMainSearchFormsIblockItems(HOTELS_ID_IBLOCK);

$arResult["CITIES"] = $arResult["CITIES_SEARCH_OBJECT"] = array();
$cities_id = array();
foreach ($arResult["PLACEMENTS"] as $placement) {

    if(!empty($placement["PROPERTIES"]["TOWN"]["VALUE"])){

            $cities_id[] = $placement["PROPERTIES"]["TOWN"]["VALUE"];
            $arResult["CITIES_SEARCH_OBJECT"][(int)abs(crc32($placement["PROPERTIES"]["TOWN"]["VALUE"]))][] = array(
                "ID" => $placement["ID"],
                "NAME" => $placement["NAME"],
                "DETAIL_PAGE_URL" => $placement["DETAIL_PAGE_URL"]
            );

    }

}

if(!empty($cities_id)) {
    $cities_id = array_unique($cities_id);
    $arResult["CITIES"] = \getMainSearchFormsIblockItems(CITIES_ID_IBLOCK, ["ID" => $cities_id]);
}