<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$url_parameters_to_delete = array(
    "travelbooking[services_id][]",
    "travelbooking[date_from]",
    "travelbooking[date]",
    "travelbooking[date_to]",
    "travelbooking[adults]",
    "travelbooking[children]",
    "travelbooking[children_age][]",
);
?>
<div class="offers-search-form hello world">
    <form id="inner-offers-search-form"
          action="<?= $APPLICATION->GetCurPageParam("", $url_parameters_to_delete, false) ?>" method="get"
          class="booking-item-dates-change mb40 hotel-card hotel-status">
        <input type="hidden" name="s2o" value="Y">
        <div class="row">

            <div class="col-12 text-center text-xl-left col-lg-11">
                <div class="row">

                    <? if ($arParams["SERVICE_TYPE"] !== "excursiontour"): ?>
                        <div class="input-daterange form-grop mb-0 col-12 col-md-6">
                            <div class="row">

                                <div class="form-group-icon-left form-grop mb-0 col-12 col-md-6 my-0">
                                    <label class="text-label text-white"><?= GetMessage("TRAVELBOOKING_DATE_FROM") ?></label>
                                    <div class="input-date-group position-relative"><i
                                                class="mr-2 icon icon-calendar text-secondary"></i>
                                        <input id="inner_date_from" autocomplete="off"
                                               data-date-default="<?= (strlen($_GET["travelbooking"]["date_from"]) ? htmlspecialchars($_GET["travelbooking"]["date_from"]) : $arResult["DATE_FROM"]) ?>"
                                               class="input-datepicker-inner-form form-control js-input-date hidden"
                                               name="travelbooking[date_from]" type="text"/>
                                        <label class="form-control date border-none date_from"
                                               for="inner_date_from"></label>
                                    </div>
                                </div>
                                <div class="form-grop mb-0 col-12 col-md-6">
                                    <div class="orm-group- form-group-select-plus">
                                        <label class="text-label text-white "> Ночей </label>
                                        <?
                                        $adults = $_GET["travelbooking"]["adults"] > 0 ? intVal($_GET["travelbooking"]["adults"]) : $arResult["ADULTS"];
                                        $arr_nights = [];

                                        if (isset($arResult["NIGHTS_COUNT"]) && !empty($arResult["NIGHTS_COUNT"])) {
                                            $arr_nights = $arResult["NIGHTS_COUNT"];
                                        } else {
                                            $arr_nights = [3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14];
                                        }
                                        ?>
                                        <span class="position-relative">
                                        <select name="night"
                                                class="form-control px-1 ts-height ts-border-radius bg-light-gray">
                                            <? foreach ($arr_nights as $night): ?>
                                                <option
                                                    <? if ($night == @$_GET["night"]): ?>selected<? endif ?> value="<?= $night ?>"><?= $night ?></option>
                                            <? endforeach ?>


                                        </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group-icon-left form-grop mb-0 col-12 col-md-6" style="display:none">
                                    <label class="text-label text-white"><?= GetMessage("TRAVELBOOKING_DATE_TO") ?></label>
                                    <div class="input-date-group position-relative"><i
                                                class="mr-2 icon icon-calendar text-secondary"></i>
                                        <input type="text" name="travelbooking[date_to]" value="30.01.2019">
                                    </div><!--<span>13 ночей</span>-->
                                </div>
                            </div>
                        </div>
                    <? else: ?>
                        <div class="input-daterange col-6 px-0">
                            <div class="row">
                                <div class="form-group-icon-left form-grop mb-0 col-12 col-md-6">
                                    <label class="text-label  text-white"><?= GetMessage("TRAVELBOOKING_TOUR_DATE") ?></label>
                                    <div class="input-date-group position-relative  bg-light-gray ts-height ts-border-radius">
                                        <i class="mr-2 icon icon-calendar text-secondary"></i>
                                        <input id="inner_date_from" autocomplete="off"
                                               data-date-default="<?= (strlen($_GET["travelbooking"]["date_from"]) ? htmlspecialchars($_GET["travelbooking"]["date_from"]) : $arResult["DATE_FROM"]) ?>"
                                               class="input-datepicker-inner-form form-control js-input-date hidden"
                                               name="travelbooking[date_from]" type="text"/>
                                        <label class="form-control date border-none" for="inner_date_from"></label>
                                    </div>
                                </div>
                                <div class="form-group-icon-left form-grop mb-0 col-12 col-md-6">
                                    <label class="text-label text-white"><?= GetMessage("TRAVELBOOKING_DATE_TO") ?></label>
                                    <div class="input-date-group position-relative  bg-light-gray ts-height ts-border-radius">
                                        <i class="mr-2 icon icon-calendar text-secondary"></i>
                                        <input id="inner_date_to" autocomplete="off"
                                               data-date-default="<?= (strlen($_GET["travelbooking"]["date_to"]) ? htmlspecialchars($_GET["travelbooking"]["date_to"]) : $arResult["DATE_TO"]) ?>"
                                               class="input-datepicker-inner-form form-control js-input-date hidden"
                                               name="travelbooking[date_to]" type="text"/>
                                        <label class="form-control date border-none" for="inner_date_to"></label>
                                    </div><!--<span>13 ночей</span>-->
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                    <? $col = 3;
                    if (isset($arResult['CITIES']) && !empty($arResult['CITIES'])): $col = 2 ?>
                        <div class="form-grop mb-0 col-12 col-sm-6 col-md-2">
                            <div class="orm-group- form-group-select-plus">
                                <label class="text-label text-white "><?= GetMessage("TRAVELBOOKING_CITIES") ?></label>
                                <? $city_id = isset($_GET["travelbooking"]["city_id"]) && $_GET["travelbooking"]["city_id"] > 0 ? intVal($_GET["travelbooking"]["city_id"]) : 0; ?>
                                <span class="position-relative">
                                <select name="travelbooking[city_id]"
                                        class="form-control px-1 ts-height ts-border-radius bg-light-gray">
                                    <option value="">...</option>
                                    <? foreach ($arResult['CITIES'] as $arCity): ?>
                                        <option value="<?= $arCity['ID'] ?>"
                                                <? if ($city_id == $arCity["ID"]): ?>selected<? endif ?>><?= $arCity["NAME"] ?></option>
                                    <? endforeach ?>
                                </select>
                            </span>
                            </div>
                        </div>
                    <? endif ?>
                    <div class="form-grop mb-0 col-12 col-sm-3 col-md-2">
                        <div class="orm-group- form-group-select-plus">
                            <label class="text-label text-white "><?= GetMessage("TRAVELBOOKING_ADULTS") ?></label>
                            <? $adults = $_GET["travelbooking"]["adults"] > 0 ? intVal($_GET["travelbooking"]["adults"]) : $arResult["ADULTS"]; ?>
                            <span class="position-relative">
                                <select name="travelbooking[adults]"
                                        class="form-control px-1 ts-height ts-border-radius bg-light-gray">
                                    <option <? if ($adults === 1): ?>selected<? endif ?> value="1">1</option>
                                    <option <? if ($adults === 2): ?>selected<? endif ?> value="2">2</option>
                                    <option <? if ($adults === 3): ?>selected<? endif ?> value="3">3</option>
                                    <option <? if ($adults === 4): ?>selected<? endif ?> value="4">4</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="form-grop mb-0 col-12 col-sm-3 col-md-2">
                        <div class="form-group-select-plus">
                            <label class="text-label text-white"><?= GetMessage("TRAVELBOOKING_CHILDREN") ?></label>
                            <span class="position-relative">
                                <select class="form-control px-1  ts-border-radius bg-light-gray"
                                        name="travelbooking[children]">
                                    <option <? if ((int)$_GET["travelbooking"]["children"] === 0): ?>selected<? endif ?> value="0"><?= GetMessage("TRAVELBOOKING_WITHOUT_CHILDREN") ?></option>
                                    <option <? if ((int)$_GET["travelbooking"]["children"] === 1): ?>selected<? endif ?> value="1">1</option>
                                    <option <? if ((int)$_GET["travelbooking"]["children"] === 2): ?>selected<? endif ?> value="2">2</option>
                                    <option <? if ((int)$_GET["travelbooking"]["children"] === 3): ?>selected<? endif ?> value="3">3</option>
                                    <option <? if ((int)$_GET["travelbooking"]["children"] === 4): ?>selected<? endif ?> value="4">4</option>
                                </select>
                            </span>
                            <div class="children-age-box hidden_">
                                <div class="closer">&times;</div>
                                <? if (is_array($_GET["travelbooking"]["children_age"]) && !empty($_GET["travelbooking"]["children_age"])): ?>
                                    <? foreach ($_GET["travelbooking"]["children_age"] as $age): ?>
                                        <input class="select-age-box" type="hidden" name="travelbooking[children_age][]"
                                               value="<?= htmlspecialchars($age) ?>">
                                    <? endforeach ?>
                                <? endif ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-auto mx-auto text-center ts-d-flex px-0 pt-2">
                <span class="loading hidden_"></span>
                <button class="btn-secondary btn-order add2cart btn btn-primary btn-lg ts-margin-bottom mt-auto"
                        style="height:36px" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?>
                </button>
            </div>

        </div>

    </form>
    <span class="next-dates"></span>
</div>
<template id="children-age-template">
    <div class="select-age-box form-group">
        <label>Возраст ребёнка {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
        </select>
    </div>
</template>
<script>
    var jsInnerSearchFormHighlightFor = <?= json_encode($arParams["HIGHLIGHT_FOR"]) ?>;


</script>