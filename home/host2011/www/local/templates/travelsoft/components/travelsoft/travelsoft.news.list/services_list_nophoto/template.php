<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="category-content__items row pad-lg-20 row-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <? $images = getSrc($arItem['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 252, 'height' => 190], NO_PHOTO_PATH_252_190, 1) ?>

    <div class="col-12 d-flex" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="product bg-white js-grid-item product--list">
            <div class="product__body">
                <div class="product__title">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <?=$arItem['NAME']?>
                    </a>
                </div>
                <div class="product__rating mb-2">
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])):?>
                        <?=star_rating($arItem['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])?>
                    <?endif;?>
                </div>
                <ul class="product__stat d-flex flex-wrap justify-content-between align-items-center">
                    <?if(!empty($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])):?>
                        <li class="mr-3">
                            <span>
                                <?if(count($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']) == 1):?>
                                    <?=strip_tags($arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']);?>
                                <?else:?>
                                    <?=implode2("-",$arItem['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE']);?>
                                <?endif?>
                            </span>
                            <?if($arItem["ROUTE_INFO"]):?>
                                <i class="bullet"></i><a href="#modalMap" data-toggle="modal" data-location='<?=\Bitrix\Main\Web\Json::encode($arItem["ROUTE_INFO"])?>' data-title="<?=$arItem["NAME"]?>"><?=GetMessage('SHOW_ON_THE_MAP')?></a>
                            <?endif?>
                        </li>
                    <?endif;?>
                </ul>
                <hr/>
                <div class="product__desc">
                    <p>
                        <?= substr2($arItem['DISPLAY_PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT'], 180) ?>
                    </p>
                </div>
            </div>
            <hr class="d-none d-sm-block"/>
            <div class="product__footer">
                <? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])): ?>
                    <ul class="product__price d-flex justify-content-between align-items-center">
                        <li class="mr-2"><span>Цена за ночь от</span></li>
                        <li>
                            <span class="cost">
                                    <?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE_MIN_BY']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY_BY']['VALUE'])->getResult(); ?>
                            </span>
                        </li>
                    </ul>
                    <hr/>
                <? endif; ?>
                <? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE']) && !empty($arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])): ?>
                    <ul class="product__price d-flex justify-content-between align-items-center">
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['PRICE_FOR']['VALUE'])):?>
                            <li class="mr-2"><span><?=GetMessage('PRICE')?> <?=$arItem['DISPLAY_PROPERTIES']['PRICE_FOR']['VALUE']?> <?=GetMessage('PRICE_FROM')?></span></li>
                        <?endif?>
                        <li>
                            <span class="cost">
                                    <?= \travelsoft\currency\factory\Converter::getInstance()->convert($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE'], $arItem['DISPLAY_PROPERTIES']['CURRENCY']['VALUE'])->getResult(); ?>
                            </span>
                        </li>
                    </ul>
                    <hr/>
                <? endif; ?>
                <a class="product__btn-book btn btn-secondary btn--round" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=GetMessage('READ_MORE')?></a>
            </div>
        </div>
    </div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

