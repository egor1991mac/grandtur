<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arListFields = [
    'Тип тура' => strip_tags($arResult['DISPLAY_PROPERTIES']['TOURTYPE']['DISPLAY_VALUE']),
    'Маршрут' => $arResult['DISPLAY_PROPERTIES']['ROUTE']['DISPLAY_VALUE'],
    'Даты тура' => $arResult['DISPLAY_PROPERTIES']['DEPARTURE']['DISPLAY_VALUE'],
    'Количество ночей' => num2word($arResult['DISPLAY_PROPERTIES']['DAYS']['DISPLAY_VALUE'], array('ночь', 'ночи', 'ночей')),
    'Ночные переезды' => $arResult['DISPLAY_PROPERTIES']['NIGHT_SHIFTS']['DISPLAY_VALUE'],
    'Продолжительность тура' => $arResult['DISPLAY_PROPERTIES']['DURATION']['DISPLAY_VALUE'] . " км",
    'Длительность тура' => num2word($arResult['DISPLAY_PROPERTIES']['DURATION_TIME']['DISPLAY_VALUE'], array('час', 'часа', 'часов')),
    'Транспорт' => $arResult['DISPLAY_PROPERTIES']['TRANSPORT']['DISPLAY_VALUE'],
    'Питание' => $arResult['DISPLAY_PROPERTIES']['FOOD']['DISPLAY_VALUE'],
];

$arTextFields = [
    'Описание' => $arResult['DISPLAY_PROPERTIES']['HD_DESC']['DISPLAY_VALUE'],
    'Проживание' => $arResult['DISPLAY_PROPERTIES']["HOTEL"]["DISPLAY_VALUE"] . '<br>' . $arResult['DISPLAY_PROPERTIES']['HOTEL_DESCRIPTION']['DISPLAY_VALUE'],
    'Питание' => $arResult['DISPLAY_PROPERTIES']['FOOD_DESCRIPTION']['DISPLAY_VALUE'],
    'Таблица цен' => $arResult['DISPLAY_PROPERTIES']['PRICE_TABLE']['DISPLAY_VALUE'],
    'Стоимость тура' => $arResult['DISPLAY_PROPERTIES']['PRICE_TOUR']['DISPLAY_VALUE'],
    'В стоимость тура входит' => $arResult['DISPLAY_PROPERTIES']['PRICE_INCLUDE']['DISPLAY_VALUE'],
    'В стоимость тура не входит' => $arResult['DISPLAY_PROPERTIES']['PRICE_NO_INCLUDE']['DISPLAY_VALUE'],
    'Необходимые документы' => $arResult['DISPLAY_PROPERTIES']['DOCUMENT']['DISPLAY_VALUE'],
    'Медицина' => $arResult['DISPLAY_PROPERTIES']['MEDICINE']['DISPLAY_VALUE'],
    'Дополнительно' => $arResult['DISPLAY_PROPERTIES']['ADDITIONAL']['DISPLAY_VALUE'],
    'Примечание' => $arResult['DISPLAY_PROPERTIES']['NOTE']['DISPLAY_VALUE'],
];
?>
<?
$this->addExternalJs(SITE_TEMPLATE_PATH . "/js/MapAdapter.js");
?>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <? if ($APPLICATION->GetDirProperty("SHOW_TITLE") == "N"): ?>
                <h1 class="fw-md mb-1"><?= $APPLICATION->ShowTitle(false); ?></h1>
            <? endif ?>
            <ul class="hotel-title mb-3">
                <li class="mb-2">
                    <? if (!empty($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE'])): ?>
                        <?= star_rating($arResult['DISPLAY_PROPERTIES']['STARS']['DISPLAY_VALUE']) ?>
                    <? endif; ?>
                </li>
                <li class="d-flex">
                    <div class="media-object"><i class="icon icon-label mr-2 text-primary"></i></div>
                    <div class="local">
                        <? if (!empty($arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'])): ?>
                            <span>
                                            <?= $arResult['DISPLAY_PROPERTIES']['COUNTRY']['DISPLAY_VALUE'] ?>
                                <? if (!empty($arResult['DISPLAY_PROPERTIES']['ROUTE']['VALUE'])): ?>
                                    <?= $arResult["DISPLAY_PROPERTIES"]["ROUTE"]["VALUE"] ?>
                                <? elseif (!empty($arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'])): ?>
                                    ,
                                    <? if (count($arResult['DISPLAY_PROPERTIES']['TOWN']['VALUE']) == 1): ?>
                                        <?= $arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'] ?>
                                    <? else: ?>
                                        <?= implode2("-", $arResult['DISPLAY_PROPERTIES']['TOWN']['DISPLAY_VALUE'], true) ?>
                                    <? endif ?>
                                <? endif; ?>
                                        </span>
                        <? endif; ?>
                        <? if (isset($arResult["ROUTE_INFO"]) && !empty($arResult["ROUTE_INFO"])): ?>
                            <i class="bullet mx-2"></i>
                            <a href="#modalMap"
                               data-location='<?= \Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"]) ?>'
                               data-toggle="modal" data-title="<?= $arResult["NAME"] ?>">
                                <?= GetMessage('SHOW_ON_THE_MAP') ?>
                            </a>
                        <? endif; ?>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <div id="map2" style="width: 100%; height: 315px"></div>
            <div class="d-inline-block align-top mr-5 mb-2 mt-3">
                <div class="fw-bold"><?= $arResult["PROPERTIES"]["DEPARTURE"]["NAME"] ?></div>
                <ul>
                    <? if (!empty($arResult["PROPERTIES"]["DEPARTURE_TEXT"]["VALUE"])): ?>
                        <li>
                            <?= $arResult["PROPERTIES"]["DEPARTURE_TEXT"]["VALUE"] ?>
                        </li>
                    <? elseif (!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])): ?>
                        <li>

                            <? foreach ($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"] as $date): ?>
                                <?= CIBlockFormatProperties::DateFormat("d.m", MakeTimeStamp($date, CSite::GetDateFormat())); ?>
                                <? if ($i < count($arItem["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])): ?><? echo ", "; ?><? endif ?>
                            <? endforeach; ?>
                        </li>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE_EXC_TEXT"]["VALUE"])): ?>
                        <div class="fw-bold"><?= $arResult["DISPLAY_PROPERTIES"]["DEPARTURE_EXC_TEXT"]["NAME"] ?></div>
                        <li>
                            <?= strip_tags($arResult["DISPLAY_PROPERTIES"]["DEPARTURE_EXC_TEXT"]["DISPLAY_VALUE"]) ?>
                        </li>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["FOOD"]["VALUE"])): ?>
                        <div class="fw-bold"><?= $arResult["DISPLAY_PROPERTIES"]["FOOD"]["NAME"] ?></div>
                        <li><?= strip_tags($arResult["DISPLAY_PROPERTIES"]["FOOD"]["DISPLAY_VALUE"]) ?></li>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["VALUE"])): ?>
                        <div class="fw-bold">
                            <? if (count($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["VALUE"]) > 1): ?>
                                <? $j = 1; ?>
                                <? foreach ($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["DISPLAY_VALUE"] as $tour): ?>
                                    <?= strip_tags($tour) ?><? if (count($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["DISPLAY_VALUE"]) > $j): ?><? echo ", " ?><? endif ?>
                                    <? $j++ ?>
                                <? endforeach; ?>
                            <? else: ?>
                                <?= strip_tags($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["DISPLAY_VALUE"]) ?>
                            <? endif ?>
                        </div>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"])): ?>
                        <li><?= $arResult["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"] ?>
                            <span><?= $arResult["DISPLAY_PROPERTIES"]["DAYS"]["NAME"] ?></span></li>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["NIGHT_SHIFTS"]["VALUE"])): ?>
                        <li><?= $arResult["DISPLAY_PROPERTIES"]["NIGHT_SHIFTS"]["VALUE"] ?></li>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DURATION"]["VALUE"])): ?>
                        <li>
                            <span><?= $arResult["DISPLAY_PROPERTIES"]["DURATION"]["NAME"] ?></span>: <?= $arResult["DISPLAY_PROPERTIES"]["DURATION"]["VALUE"] ?>
                        </li>
                    <? endif ?>
                    <ul>
            </div>
        </div>
        <div class="col-lg-6">
            <? $images = getSrc($arResult['DISPLAY_PROPERTIES']['PICTURES']['VALUE'], ['width' => 510, 'height' => 314], NO_PHOTO_PATH_825_500) ?>
            <div class="hotel-gallery">
                <div class="hotel-gallery__carousel swiper-container js-hotel-carousel">
                    <div class="swiper-wrapper">
                        <? foreach ($images as $image): ?>
                            <div class="swiper-slide">
                                <img class="img-fluid img-cover" src="<?= $image ?>" alt="<? $arResult['NAME'] ?>"/>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <? if (count($images) > 1): ?>
                        <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev"
                                                                role="button">
                                <i class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                        <path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/>
                                        <path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/>
                                    </svg>
                                </i>
                            </a>
                            <a class="hotel-gallery__arrow shadow-sm js-next" role="button">
                                <i class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44">
                                        <path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/>
                                        <path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    <? endif; ?>
                </div>
                <? if (count($images) > 1): ?>
                    <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                        <div class="swiper-wrapper">
                            <? foreach ($images as $image): ?>
                                <div class="swiper-slide">
                                    <a class="hotel-gallery__thumb js-gallery-link" href="<?= $image ?>">
                                        <img class="img-cover" src="<?= $image ?>" alt="<? $arResult['NAME'] ?>"/>
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <div class="w-100 px-0">
            <div class="hotel-items px-0">
                <? // ФОРМА ПОИСКА ЦЕНОВЫХ ПРЕДЛОЖЕНИЙ
                if ($GLOBALS["SEARCH_FORM_HTML"]):?>
                    <div class="row">
                        <div class="col-md-12"><? echo $GLOBALS["SEARCH_FORM_HTML"]; ?></div>
                    </div>
                <? endif ?>
                <? // вывод цен из travelbooking по размещению
                if ($GLOBALS["SEARCH_OFFERS_RESULT_HTML"]):?>
                    <div class="row">
                        <div class="col-md-12">
                            <? echo $GLOBALS["SEARCH_OFFERS_RESULT_HTML"]; ?>
                        </div>
                    </div>

                <? endif ?>
            </div>
        </div>
        <div class="col-lg-9">
            <? if (!empty($arResult["PROPERTIES"]["HOTEL"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white">
                    <div class="sidebar__card-title">
                        <h4 class="card-title"><?= $arResult["DISPLAY_PROPERTIES"]["HOTEL"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <? $GLOBALS["arFilterHotels"] = array("ID" => $arResult["PROPERTIES"]["HOTEL"]["VALUE"]); ?>
                    <? $APPLICATION->IncludeComponent(
                        "travelsoft:travelsoft.news.list",
                        "universal_list",
                        Array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AFP_125" => array(),
                            "AFP_156" => array(),
                            "AFP_157" => array(),
                            "AFP_158" => array(),
                            "AFP_162" => array(),
                            "AFP_163" => array(),
                            "AFP_164" => array(),
                            "AFP_169" => array(),
                            "AFP_170" => array(),
                            "AFP_171" => array(),
                            "AFP_172" => array(),
                            "AFP_174" => array(),
                            "AFP_176" => array(),
                            "AFP_180" => array(),
                            "AFP_188" => array(),
                            "AFP_189" => array(),
                            "AFP_190" => array(),
                            "AFP_191" => array(),
                            "AFP_192" => array(),
                            "AFP_193" => array(),
                            "AFP_194" => array(),
                            "AFP_195" => array(),
                            "AFP_204" => array(),
                            "AFP_276" => array(),
                            "AFP_283" => array(),
                            "AFP_296" => array(),
                            "AFP_300" => array(),
                            "AFP_78" => array(),
                            "AFP_79" => array(),
                            "AFP_80" => array(),
                            "AFP_81" => array(),
                            "AFP_91" => array(),
                            "AFP_ID" => "",
                            "AFP_MAX_104" => "",
                            "AFP_MAX_167" => "",
                            "AFP_MAX_178" => "",
                            "AFP_MAX_179" => "",
                            "AFP_MAX_203" => "",
                            "AFP_MAX_209" => "",
                            "AFP_MAX_90" => "",
                            "AFP_MIN_104" => "",
                            "AFP_MIN_167" => "",
                            "AFP_MIN_178" => "",
                            "AFP_MIN_179" => "",
                            "AFP_MIN_203" => "",
                            "AFP_MIN_209" => "",
                            "AFP_MIN_90" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "N",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("", ""),
                            "FILE_404" => "",
                            "FILTER_NAME" => "arFilterHotels",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "7",
                            "IBLOCK_TYPE" => "geography_and_product",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "9",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(0 => "COUNTRY", 1 => "TOWN", 2 => "TYPE_ID", 3 => "CAT_ID", 4 => "STARS", 5 => "CURRENCY_BY", 6 => "PRICE_MIN_BY", 7 => "PREVIEW_TEXT", 8 => "HD_DESC", 9 => "PICTURES", 10 => "",),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "Y",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "Y",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "ASC"
                        )
                    ); ?>
                </section>
            <? endif; ?>
            <? if (!empty($arResult["PROPERTIES"]["HD_DESC"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white">
                    <div class="sidebar__card-title">
                        <h4 class="card-title"><?= $arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <p><?= $arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["DISPLAY_VALUE"] ?></p>
                </section>
            <? endif; ?>
            <section class="card" id="pager">
                <ul class="list-group list-group-flush">
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]["VALUE"])): ?>
                        <? foreach ($arResult["DISPLAY_PROPERTIES"]["NDAYS"]['~VALUE'] as $key => $nday): ?>
                            <li class="card-header">
                                <h5 class="mb-0 mt-0">
                                    <b><?= ($key + 1) . " День"; ?></b> <? if (!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]['DESCRIPTION'][$key])): ?>
                                        <?= $arResult["DISPLAY_PROPERTIES"]["NDAYS"]['DESCRIPTION'][$key]; ?>
                                    <? endif; ?></h5>
                            </li>
                            <li class="list-group-item">
                                <p><?= $nday['TEXT'] ?></p>
                            </li>
                        <? endforeach; ?>
                    <? endif; ?>
                </ul>
            </section>

        </div>
        <div class="col-lg-3 sidebar">
            <?= $arParams['MANAGERS'] ?>
            <!--<div class="card d-none d-lg-flex">
              <p class="fw-bold cl1">МЕНЕДЖЕРЫ ТУРА</p>
              <ul class="guests-room">
                <li class="guests-room__item">
                  <div class="guests-room__desc">+375 (17) 255 255 7<br>+375 (29) 134 255 1<br>Skype: halinamaroz<br>france@bel-orientir.ru</div>
                  <div class="guests-room__bottom d-flex align-items-center">
                      <div class="guests-room__avatar d-flex align-items-center pointer"><a class="media-object d-block rounded-circle" href="hotel-reviews.html"><img class="img-fluid rounded-circle" src="/images/avatar-2.jpg" alt="#"></a></div>
                    <div class="guests-room__right">
                      <h5 class="mb-1 fw-bold cl2">Галина Мороз</h5>
                      <div class="d-flex align-items-center">
                        <div class="country fz-xs">Специалист по продаже</div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>-->
            <? if (!empty($arResult["PROPERTIES"]["PRICE_INCLUDE"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white border-green">
                    <div class="sidebar__card-title">
                        <h4 class="card-title text-green"><?= $arResult["DISPLAY_PROPERTIES"]["PRICE_INCLUDE"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <p><?= $arResult["DISPLAY_PROPERTIES"]["PRICE_INCLUDE"]["DISPLAY_VALUE"] ?></p>
                </section>
            <? endif; ?>
            <? if (!empty($arResult["PROPERTIES"]["PRICE_NO_INCLUDE"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white bg-white border-red">
                    <div class="sidebar__card-title">
                        <h4 class="card-title text-red"><?= $arResult["DISPLAY_PROPERTIES"]["PRICE_NO_INCLUDE"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <p><?= $arResult["DISPLAY_PROPERTIES"]["PRICE_NO_INCLUDE"]["DISPLAY_VALUE"] ?></p>
                </section>
            <? endif; ?>
            <? if (!empty($arResult["PROPERTIES"]["DOCUMENT"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white">
                    <div class="sidebar__card-title">
                        <h4 class="card-title"><?= $arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <p><?= $arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["DISPLAY_VALUE"] ?></p>
                </section>
            <? endif; ?>
            <? if (!empty($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"])): ?>
                <section class="sidebar__card card hotel-card bg-white">
                    <div class="sidebar__card-title">
                        <h4 class="card-title"><?= $arResult["DISPLAY_PROPERTIES"]["FILES"]["NAME"] ?></h4>
                        <hr class="my-3">
                    </div>
                    <ul class="booking-card__checklist">
                        <? if (count($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"]) > 1): ?>

                            <? foreach ($arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"] as $file): ?>
                                <li><i class="icon icon-check-button mr-3 text-primary"></i><a
                                            href="<?= $file["SRC"] ?>"><?= $file["DESCRIPTION"] ?></a></li>
                            <? endforeach; ?>

                        <? else: ?>

                            <li><i class="icon icon-check-button mr-3 text-primary"></i><a
                                        href="<?= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["SRC"] ?>"><?= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["DESCRIPTION"] ?></a>
                            </li>

                        <? endif ?>
                    </ul>
                </section>
            <? endif ?>
        </div>
        <div class="col-lg-12">
            <? // Set in component prolog?>
            <?= $arParams['REVIEWS'] ?>
        </div>
    </div>
</div>
</div>
<script>

    $(document).ready(function () {

        var route = <?=json_encode($arResult['ROUTE_INFO'])?>;

        var mapAdapter = new MapAdapter({
            map_id: "map",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });
        var mapAdapter2 = new MapAdapter({
            map_id: "map2",
            center: {
                lat: 53.53,
                lng: 27.34
            },
            object: "ymaps",
            zoom: 15
        });

        if (route !== null) {
            if (typeof route === "object" && route.length > 1) {
                mapAdapter2.drawRoute(route);
            } else {
                mapAdapter2.addMarker(route[0]);
            }
        }

        $('body').on("show.bs.modal", "#modalMap", function (e) {

            var t = $(e.relatedTarget), o = t.data("title"), n = $(this);
            n.find(".modal-title .title").text(o), loc = t.data('location');

            if (typeof loc === "object" && loc.length > 1) {
                mapAdapter.drawRoute(loc);
            } else {
                mapAdapter.addMarker(loc[0]);
            }

        })

    });


</script>
