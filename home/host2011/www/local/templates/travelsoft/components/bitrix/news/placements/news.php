<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$this->SetViewTarget('page_url');?>
    <?echo "hotel-result-main";?>
<?$this->EndViewTarget();?>

<?$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js//moment.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/moment_locales.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.travelsoft.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/booking/main-search-form-detail.js");
?>


<?

?>


<div class="result-body">
    <div class="row">


<? if ($arParams["USE_RSS"] == "Y"): ?>
    <?
    if (method_exists($APPLICATION, 'addheadstring'))
        $APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" href="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" />');
    ?>
    <a href="<?= $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] ?>" title="rss" target="_self"><img alt="RSS" src="<?= $templateFolder ?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<? endif ?>

<? if ($arParams["USE_SEARCH"] == "Y"): ?>
    <?= GetMessage("SEARCH_LABEL") ?><?
    $APPLICATION->IncludeComponent(
            "bitrix:search.form", "flat", Array(
        "PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"]
            ), $component
    );
    ?>
    <br />
<? endif ?>
    <? if ($arParams["USE_FILTER"] == "Y"): ?>
        <?$this->SetViewTarget("smartfilter");?>
        <?/* $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.catalog.smart.filter",
            ".default",
            array(
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "DISPLAY_ELEMENT_COUNT" => "Y",
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "FILTER_VIEW_MODE" => "vertical",
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "PAGER_PARAMS_NAME" => "arrPager",
                "POPUP_POSITION" => "right",
                "SAVE_IN_SESSION" => "N",
                "SECTION_CODE" => "",
                "SECTION_DESCRIPTION" => "-",
                "SECTION_ID" => "",
                "SECTION_TITLE" => "-",
                "SEF_MODE" => "N",
                "TEMPLATE_THEME" => "",
                "XML_EXPORT" => "N",
                "COMPONENT_TEMPLATE" => ".default"
            ),
            false
        ); */?>
        <?/* $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "sitefilter",
            array(
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "DISPLAY_ELEMENT_COUNT" => "Y",
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "FILTER_VIEW_MODE" => "vertical",
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "IBLOCK_TYPE" => "tourpoduct",
                "PAGER_PARAMS_NAME" => "arrPager",
                "POPUP_POSITION" => "right",
                "SAVE_IN_SESSION" => "N",
                "SECTION_CODE" => "",
                "SECTION_DESCRIPTION" => "-",
                "SECTION_ID" => "",
                "SECTION_TITLE" => "-",
                "SEF_MODE" => "N",
                "TEMPLATE_THEME" => "",
                "XML_EXPORT" => "N",
                "COMPONENT_TEMPLATE" => "sitefilter"
            ),
            false
        ); */?>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.smart.filter", "filter", array(
                //"bitrix:catalog.smart.filter", ".default", array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => "",
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => "",
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => "vertical",
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => "Y",
                "TEMPLATE_THEME" => "",
                'CONVERT_CURRENCY' => "",
                'CURRENCY_ID' => "",
                "SEF_MODE" => "N",
                "SEF_RULE" => "",
                "SMART_FILTER_PATH" => "",
                "PAGER_PARAMS_NAME" => "",
                "INSTANT_RELOAD" => "N",
                //"SEF_FOLDER" => $arResult["FOLDER"],
            ), $component, array('HIDE_ICONS' => 'Y')
            );
            ?>
        <?$this->EndViewTarget();?>
    <?endif?>
    <div class="col-md-<? if ($arParams["USE_FILTER"] == "Y"): ?>8<? else: ?>12<? endif ?> main-right">

        <?
        // расчет стоимости "от" для списка оъектов
        Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        $mpf_result = array();

        if($arParams["IBLOCK_ID"] != IBLOCK_PLACEMENTS) {

            $mpf_result = \travelsoft\booking\Utils::getCaluculationsPriceFrom(function () use ($arParams) {

                $arData = [];

                $arFilter = ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"];
                if (isset($GLOBALS[$arParams["FILTER_NAME"]]) && is_array($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($GLOBALS[$arParams["FILTER_NAME"]])) {
                    $arFilter = array_merge($arFilter, $GLOBALS[$arParams["FILTER_NAME"]]);
                }

                $cache = new \travelsoft\booking\adapters\Cache(serialize($arFilter) . time(), "/travelsoft/booking/objects_for_price_from_results/");

                if (empty($arData = $cache->get())) {

                    $arData = $cache->caching(function () use ($arFilter, $arParams) {

                        $arData = [
                            "packagetour" => [],
                            "excursiontour" => [],
                            "placements" => []
                        ];

                        $dbElements = \CIBlockElement::GetList(false, $arFilter, false, false, ["ID", "PROPERTY_TOURTYPE"]);

                        if ($arParams["SERVICE_TYPE"] === "placements") {
                            while ($arElement = $dbElements->Fetch()) {
                                $arData["placements"]["services_id"][] = $arElement["ID"];
                            }
                        } else {
                            while ($arElement = $dbElements->Fetch()) {

                                if (\SEA_TOUR_TYPE_ID === (int)$arElement["PROPERTY_TOURTYPE_VALUE"]) {

                                    $id = travelsoft\booking\Utils::getPackageTourByDescription($arElement["ID"])["ID"];
                                    if ($id > 0) {
                                        $arData["packagetour"]["services_id"][] = $id;
                                    }

                                } else {
                                    $arData["excursiontour"]["excursions_id"][] = $arElement["ID"];
                                }
                            }
                        }

                        return $arData;
                    });
                }

                return $arData;
            }, function (&$result) use ($arParams) {

                $_result = $ids = [];

                foreach ($result as $service_type => $data) {

                    if (!empty($data)) {

                        if ($service_type === "excursiontour" || $service_type === "packagetour") {

                            if ($service_type === "excursiontour") {
                                $class = "\\travelsoft\\booking\\stores\\ExcursionTour";
                            } elseif ($service_type === "packagetour") {
                                $class = "\\travelsoft\\booking\\stores\\Toursdesc";
                            }

                            foreach ($class::get([
                                "filter" => ["ID" => array_keys($data)],
                                "select" => ["UF_TOUR", "ID"]
                            ]) as $arTour) {
                                $_result[$arTour["UF_TOUR"]] = $data[$arTour["ID"]];
                                $ids[] = $arTour["UF_TOUR"];
                            }
                        } else {

                            $_result = \array_merge($result, $data);
                            $ids = \array_merge($ids, array_keys($data));
                        }
                    }
                }

                if (isset($GLOBALS[$arParams["FILTER_NAME"]]) && is_array($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($GLOBALS[$arParams["FILTER_NAME"]]) && !empty($ids)) {
                    //$GLOBALS[$arParams["FILTER_NAME"]]["ID"] = $ids;
                }

                $result = $_result;
            });
        }

        ?>

        <? $template = isset($arParams["TEMPLATE_LIST"]) && !empty($arParams["TEMPLATE_LIST"]) ? $arParams["TEMPLATE_LIST"] : "placements-list"; ?>
        <?
        $ajaxId = "__travelsoft_2563a459969351ba97ae5ef0f8913e2e";
        travelsoft\Ajax::start($ajaxId, true);
        ?>
        <?
        $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.news.list", $template, Array(
            "MIN_PRICES" => $mpf_result,
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "NEWS_COUNT" => $arParams["NEWS_COUNT"],
            "SORT_BY1" => $arParams["SORT_BY1"],
            "SORT_ORDER1" => $arParams["SORT_ORDER1"],
            "SORT_BY2" => $arParams["SORT_BY2"],
            "SORT_ORDER2" => $arParams["SORT_ORDER2"],
            "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
            "MESSAGE_404" => $arParams["MESSAGE_404"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "SHOW_404" => $arParams["SHOW_404"],
            "FILE_404" => $arParams["FILE_404"],
            "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
            "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
            "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
            "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
            "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
            "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
            "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
            "CHECK_DATES" => $arParams["CHECK_DATES"],
            "AR_SORT_BY" => $arSortBy,
                ), $component
        );
        ?>
        <? travelsoft\Ajax::end($ajaxId, true); ?>
    </div>
    <? if ($arParams["USE_FILTER"] == "Y"): ?>
        <?$templ_search = isset($arParams['TEMPLATE_SEARCH']) && !empty($arParams['TEMPLATE_SEARCH']) ? $arParams['TEMPLATE_SEARCH'] : "d3p.main_";?>
        <div class="col-md-4 sidebar-widget">
            <div>
                <?
                $APPLICATION->IncludeComponent(
                    "travelsoft:booking.search_form", $templ_search, Array(
                    )
                );
                ?>
            </div>
            <div>
                <?$APPLICATION->ShowViewContent('smartfilter');?>
            </div>
        </div>
    <? endif ?>

    </div>
</div>

<?if($arParams["IBLOCK_ID"] == IBLOCK_PLACEMENTS):?>
    <?$APPLICATION->IncludeComponent(
        "travelsoft:travelsoft.news.list",
        "offers_list_minblock",
        array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "COMPONENT_TEMPLATE" => "offers_list_minblock",
            "DESCRIPTION_LINK" => "/tury/",
            "DESCRIPTION_NAMELINK" => "",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_NAME" => "arFilterOffers",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "13",
            "IBLOCK_TYPE" => "geography_and_product",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "8",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Туры",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array(
                0 => "PRICE",
                1 => "CURRENCY",
                2 => "HD_DESC",
                3 => "NOTE",
                4 => "PICTURES",
                5 => "PICTURE_PREVIEW",
                6 => "PRICE_FOR",
            ),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "TEXT_DESCRIPTION" => "",
            "TEXT_TITLE" => "cпециальные предложения",
            "AFP_ID" => "",
            "AFP_74" => array(
            ),
            "AFP_75" => array(
            ),
            "AFP_76" => array(
            ),
            "AFP_80" => array(
            ),
            "AFP_294" => array(
            ),
            "AFP_81" => array(
            ),
            "AFP_MIN_84" => "",
            "AFP_MAX_84" => "",
            "AFP_MIN_85" => "",
            "AFP_MAX_85" => "",
            "AFP_86" => array(
            ),
            "AFP_87" => array(
            ),
            "AFP_MIN_90" => "",
            "AFP_MAX_90" => "",
            "AFP_93" => array(
            ),
            "AFP_MIN_94" => "",
            "AFP_MAX_94" => "",
            "AFP_MIN_95" => "",
            "AFP_MAX_95" => "",
            "AFP_96" => array(
            ),
            "AFP_295" => array(
            ),
            "AFP_102" => array(
            ),
            "AFP_103" => array(
            ),
            "AFP_104" => array(
            ),
            "AFP_269" => array(
            ),
            "AFP_113" => array(
                0 => "44",
            ),
            "AFP_MIN_112" => "",
            "AFP_MAX_112" => "",
            "AFP_MIN_110" => "",
            "AFP_MAX_110" => "",
            "AFP_111" => array(
            ),
            "AFP_MIN_116" => "",
            "AFP_MAX_116" => "",
            "AFP_342" => array(
            ),
            "AFP_343" => array(
                0 => "101",
            )
        ),
        false
    );?>
<?endif?>

<script>

    $('.booking-filters-text').on('click', function (e) {
        $(this).hide();
        $('.booking-filters').css("display","block");
    });

</script>

<style>
    .main-right {
        float: right;
    }
    .hidden {
        display: none!important;
    }
</style>
