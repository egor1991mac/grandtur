<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

if(!empty($arResult["ITEMS"]["252"]["VALUES"])) {
    $arIdServices = Array();
    foreach ($arResult["ITEMS"]["252"]["VALUES"] as $k => $item){
        $arIdServices[$k] = $k;
    }
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => SERVICES_ID_IBLOCK, "ID" => $arIdServices, "ACTIVE" => "Y", "PROPERTY_SHOW_VALUE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult["ITEMS"]["252"]["VALUES"][$arFields["ID"]]["SHOW"] = "Y";
    }
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

if(array_key_exists("PROPERTY_TYPE", $GLOBALS[$arParams["FILTER_NAME"]])){
    foreach ($arResult["ITEMS"] as $key=>$val){
        if($val["CODE"] == "TYPE"){
            unset($arResult["ITEMS"][$key]);
        }
    }
}

