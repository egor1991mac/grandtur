<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<nav class="pagination-nav" aria-label="Page navigation example">
    <ul class="pagination">
    <?
    $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
    $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
    ?>
        <?$StartPage = true;?>
        <?$EndPage = true;?>
        <li class="page-item">
            <?if($arResult["NavPageNomer"] == "1"):?>
                <a class="page-link">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            <?else:?>
                <a class="page-link" href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            <?endif;?>
        </li>
        <?for($page = 1; $page <= $arResult["NavPageCount"]; $page++):?>
            <?if($arResult["NavPageNomer"] == $page):?>
                <li class="page-item active"><a class="page-link"><?=$page?></a></li>
            <?else:?>
                <?if($page == "1"):?>
                    <li class="page-item">
                        <a class="page-link" href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>">
                            <?=$page?>
                        </a>
                    </li>
                <?elseif($page == $arResult["NavPageCount"]):?>
                    <li class="page-item">
                        <a class="page-link" href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>">
                            <?=$page?>
                        </a>
                    </li>
                <?elseif(($arResult["nStartPage"] > $page) && (($arResult["nStartPage"] - 2) != "1")):?>
                    <?if($StartPage):?>
                        <li class="page-item"><a class="page-link">...</a></li>
                        <?$StartPage = false;?>
                    <?endif;?>
                <?elseif(($arResult["nEndPage"] < $page) && (($arResult["nEndPage"] + 2) != $arResult["NavPageCount"])):?>
                    <?if($EndPage):?>
                        <li class="page-item"><a class="page-link">...</a></li>
                        <?$EndPage = false;?>
                    <?endif;?>
                <?else:?>
                    <li class="page-item">
                        <a class="page-link" href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>">
                            <?=$page?>
                        </a>
                    </li>
                <?endif;?>
            <?endif;?>
        <?endfor;?>
        <li class="page-item">
            <?if($arResult["NavPageNomer"] == $arResult["NavPageCount"]):?>
                <a class="page-link">
                    <i class="fa fa-angle-double-right"></i>
                </a>
            <?else:?>
                <a class="page-link" href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
                    <i class="fa fa-angle-double-right"></i>
                </a>
            <?endif;?>
        </li>
    </ul>
</nav>