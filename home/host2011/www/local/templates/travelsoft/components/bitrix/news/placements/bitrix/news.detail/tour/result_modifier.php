<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$TOWN_CODE = "TOWN";
$MAP_CODE = "PROPERTY_MAP_VALUE";
$EXURSIONS_CODE = "EXURSIONS";

$POINT_DEPARTURE_CODE = "POINT_DEPARTURE";

$arCities = array();
$db_cities = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['PROPERTIES'][$TOWN_CODE]["LINK_IBLOCK_ID"], "ACTIVE" => "Y"), false, false, Array("ID", "NAME", "PROPERTY_MAP", "PROPERTY_POINT_DEPARTURE"));
while ($ar_fields = $db_cities->GetNext()) {
    $arCities[$ar_fields["ID"]] = $ar_fields;
}

if ($arCities[$arResult['PROPERTIES'][$POINT_DEPARTURE_CODE]["VALUE"]][$MAP_CODE] != "") {
    $LATLNG = explode(",", $arCities[$arResult['PROPERTIES'][$POINT_DEPARTURE_CODE]["VALUE"]][$MAP_CODE]);
    $point_departure = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arCities[$arResult['PROPERTIES'][$POINT_DEPARTURE_CODE]["VALUE"]]['NAME'],
        "infoWindow" => "<div style='color:red'><b>" . $arCities[$arResult['PROPERTIES'][$POINT_DEPARTURE_CODE]["VALUE"]]['NAME'] . "</b></div>"
    );
    $arResult['ROUTE_INFO'][] = $point_departure;
}

$cnt = 0;
$cnt = count($arResult['PROPERTIES'][$TOWN_CODE]["VALUE"]) + 2;
foreach ($arResult['PROPERTIES'][$TOWN_CODE]["VALUE"] as $maps) {

    if ($arCities[$maps][$MAP_CODE] != "") {
        $LATLNG = explode(",", $arCities[$maps][$MAP_CODE]);
        $arResult['ROUTE_INFO'][] = array(
            "lat" => $LATLNG[0],
            "lng" => $LATLNG[1],
            "title" => $arCities[$maps]['NAME'],
            "infoWindow" => "<div style='color:red'><b>" . $arCities[$maps]['NAME'] . "</b></div>"
        );
    }

}

if(isset($point_departure) && !empty($point_departure))
    $arResult['ROUTE_INFO'][] = $point_departure;


if(!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])) {

    $date = array();

    if (is_array($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])) {
        $date_ = date("d.m.Y");
        foreach ($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"] as $t => $item) {

            if (strtotime($date_) < strtotime($item)) {
                $date[] = CIBlockFormatProperties::DateFormat("d.m", MakeTimeStamp($item, CSite::GetDateFormat()));
            }
            /*if (count($date) == 4)
                break;*/

        }
    } else {
        $date[] = $arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"];
    }

    $arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DATES"] = $date;
}

/*

$cp = $this->__component;

if (is_object($cp))
{
    $cp->arResult['ROUTE_INFO'] = $arResult['ROUTE_INFO'];
    $cp->arResult['MAP_SCALE'] = $zoom;

    $arResult['MAP_SCALE'] = $cp->arResult['MAP_SCALE'];
    $cp->SetResultCacheKeys(array('ROUTE_INFO', 'MAP_SCALE'));

}*/