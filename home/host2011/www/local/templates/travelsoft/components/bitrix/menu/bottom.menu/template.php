<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
$previousLevel = 0;
foreach($arResult as $k=>$arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
            <div class="<?if($k == 0):?>col-12 col-sm-6 col-lg-3<?else:?>col-12 col-sm-6 col-lg-3<?endif;?>">
                <h3 class="h3 text-uppercase"><?=$arItem["TEXT"]?></h3>
                <ul class="page-footer__support list-styled">
		<?else:?>
            <div class="<?if($k == 0):?>col-12 col-sm-6 col-lg-3<?else:?>col-12 col-sm-6 col-lg-3<?endif;?>">
                <h3 class="h3 text-uppercase"><?=$arItem["TEXT"]?></h3>
                <ul class="page-footer__support list-styled">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <div class="<?if($k == 0):?>col-12 col-sm-6 col-lg-3<?else:?>col-12 col-sm-6 col-lg-3<?endif;?>">
                        <h3 class="h3 text-uppercase"><?=$arItem["TEXT"]?></h3>
                </div>
			<?else:?>
				<li class="mb-2"><a class="link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="mb-2"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li class="mb-2"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div>", ($previousLevel-1) );?>
<?endif?>

<?endif?>