<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<ul class="main-nav__list nav d-flex align-items-center justify-content-between w-100" id="navMenu">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

        <li class="nav-item dropdown">
            <a class="nav-link fw-bold text-uppercase <?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>">
                <span><?=$arItem["TEXT"]?></span><i class="caret fa fa-angle-down ml-2"></i>
            </a>
            <ul class="dropdown-menu">

	<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <li class="nav-item">
                    <a class="nav-link fw-bold text-uppercase <?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
                </li>
           <?else:?>
                <li class="nav-item">
                    <a class="nav-link <?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
                </li>
			<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
                <li class="nav-item d-flex d-md-none border-none pt-5">
                    <a class="nav-link btn" href="/personal/profile/"><i class="text-gray icon icon-user mr-2"></i><span>Личный кабинет</span></a>
                </li>
</ul>
<?endif?>


<?/*

            <li class="nav-item dropdown"><a class="nav-link fw-bold text-uppercase" href="#"><span>Автобусные туры</span><i class="caret fa fa-angle-down ml-2"></i></a>
                <ul class="dropdown-menu">
                    <li class="nav-item"><a class="nav-link" href="index.html"><span>Поиск</span></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="index-account.html"><span>Спецпредложения</span></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="index-video.html"><span>Виды отдыха</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="hotel.html"><span>Гостиницы</span></a>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="hotel.html"><span>Страны</span></a>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="hotel.html"><span>Туристам</span></a>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="flights.html"><span>Агентствам</span></a>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="category.html"><span>Отдых в Беларуси</span></a>
            </li>
            <li class="nav-item"><a class="nav-link fw-bold text-uppercase" href="category.html"><span>Контакты</span></a>
            </li>
*/?>

