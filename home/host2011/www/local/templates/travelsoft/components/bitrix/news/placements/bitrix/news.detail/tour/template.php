<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/theia-sticky-sidebar.min.js");
$htmlMapID = "map-canvas";
?>

<div class="row">
    <div class="col-md-9">
        <?if(!empty($arResult['PROPERTIES']['ROUTE']['VALUE'])):?>
			<h2 class="title-style-2"><?= $arResult["PROPERTIES"]["ROUTE"]["VALUE"] ?></h2>
        <?endif;?>
        <div class="schedule-block">
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DATES"])):?>
                <div class="element">
                    <p class="schedule-title"><?=GetMessage('DEPARTURE')?></p>
                    <span class="schedule-content" style="padding: 0 2px;"><?=implode(", ", $arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DATES"])?></span>
                </div>
            <?endif;?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"])):?>
                <div class="element">
                    <p class="schedule-title"><?=GetMessage('DAYS')?></p>
                    <span class="schedule-content"><?=num2word($arResult["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"]+1,array(GetMessage('DAY1'),GetMessage('DAY2'),GetMessage('DAY_ALL')))?></span>
                </div>
            <?endif;?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NIGHT_SHIFTS"]["VALUE"])):?>
                <div class="element">
                    <p class="schedule-title"><?=GetMessage('NIGHT_SHIFTS')?></p>
                    <span class="schedule-content"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["NIGHT_SHIFTS"]["VALUE"])?></span>
                </div>
            <?endif;?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TOURTYPE"]["VALUE"])):?>
                <div class="element">
                    <p class="schedule-title"><?=GetMessage('TOURTYPE')?></p>
                    <span class="schedule-content"><?if(count($arResult['DISPLAY_PROPERTIES']['TOURTYPE']['VALUE']) == 1):?>
                                                    <?=strip_tags($arResult['DISPLAY_PROPERTIES']['TOURTYPE']['DISPLAY_VALUE'])?>
                                                <?else:?>
													<?=implode("<br>", array_map(function($item){
														return strip_tags($item);
													}, $arResult['DISPLAY_PROPERTIES']['TOURTYPE']['DISPLAY_VALUE']))?>
                                                <?endif?></span>
                </div>
            <?endif?>
        </div>
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["VALUE"]["TEXT"])):?>
            <div class="overview-block clearfix">
                <div class="text-block">
                    <?=$arResult["DISPLAY_PROPERTIES"]["HD_DESC"]["DISPLAY_VALUE"]?>
                </div>
            </div>
        <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_TABLE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price"><?=GetMessage('PRICE_TABLE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PRICE_TABLE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"])):?>
            <div class="overview-block clearfix">
                <div class="warpper-slider-detail" id="detail_photo">
                    <? $APPLICATION->IncludeComponent(
                        "travelsoft:travelsoft.slider",
                        ".default",
                        array(
                            "AUTO_PLAY_BIG" => "N",
                            "AUTO_PLAY_HOVER_PAUSE_BIG" => "Y",
                            "AUTO_PLAY_TIMEOUT_BIG" => "3000",
                            "DATA_SOURCE" => $arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"],
                            "DOT_BIG" => "N",
                            "DOT_SMALL" => "N",
                            "DO_NOT_INC_MAGNIFIC_POPUP" => "N",
                            "DO_NOT_INC_OWL_CAROUSEL" => "N",
                            "HEIGHT" => "471",
                            "INC_JQUERY" => "N",
                            "ITEM_COUNT_BIG" => "1",
                            "ITEM_COUNT_SMALL" => "5",
                            "LAZY_LOAD_BIG" => "Y",
                            "LAZY_LOAD_SMALL" => "Y",
                            "MARGIN_PICTURES_BIG" => "10",
                            "MARGIN_PICTURES_SMALL" => "3",
                            "NAV_BIG" => "Y",
                            "NAV_SMALL" => "Y",
                            "NO_PHOTO_PATH" => NO_PHOTO_PATH_750_420,
                            "WIDTH" => "848",
                            "COMPONENT_TEMPLATE" => ".default"
                        ),
                        false
                    ); ?>
                </div>
            </div>
        <?endif?>
        <?if ($GLOBALS["SEARCH_FORM_HTML"]):?>
            <div class="cart-header">
                <h4 class="card-title">Бронирование</h4>
                <hr>
            </div>
            <div class="row"><div class="col-md-12"><? echo $GLOBALS["SEARCH_FORM_HTML"]; ?></div></div>
        <?endif?>
        <?// вывод цен из travelbooking по размещению
        if ($GLOBALS["SEARCH_OFFERS_RESULT_HTML"]):?>
            <div class="row"><div class="col-md-12">
                    <!--<div class="cart-header">
                        <h4 class="card-title">Номера</h4>
                        <hr>
                    </div>-->
                    <?echo $GLOBALS["SEARCH_OFFERS_RESULT_HTML"]; ?>
                </div>
            </div>

        <?endif?>

        <?// вывод цен из travelbooking
        if ($GLOBALS["SEARCH_PACKAGE_OFFERS_RESULT_HTML"]):?>
            <div class="row"><div class="col-md-12">
                    <!--<div class="cart-header">
                        <h4 class="card-title">Номера</h4>
                        <hr>
                    </div>-->
                    <?echo $GLOBALS["SEARCH_PACKAGE_OFFERS_RESULT_HTML"]; ?>
                </div>
            </div>

        <?endif?>
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]["VALUE"])):?>
            <?$ndays = (array)$arResult["DISPLAY_PROPERTIES"]["NDAYS"]["DISPLAY_VALUE"];?>
            <div class="journey-block">
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_ndays"><?=GetMessage('NDAYS')?></h3>
                    <div class="timeline-container">
                        <div class="timeline">
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["NDAYS"]["DISPLAY_VALUE"] as $key=>$nday):?>
                                <div class="timeline-block">
                                    <div class="timeline-title">
                                        <span><?=GetMessage('DAY')?> <?=$key+1?></span>
                                    </div>
                                    <div class="timeline-content medium-margin-top">
                                        <div class="row">
                                            <div class="timeline-point">
                                                <i class="fa fa-circle-o"></i>
                                            </div>
                                            <div class="timeline-custom-col content-col">
                                                <div class="timeline-location-block">
                                                    <p class="location-name"><?=$arResult["DISPLAY_PROPERTIES"]["NDAYS"]["DESCRIPTION"][$key]?>
                                                        <i class="fa fa-map-marker icon-marker"></i>
                                                    </p>
                                                    <p class="description"><?=$nday?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        <?endif?>
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["HOTEL_DESCRIPTION"]["VALUE"]["TEXT"])):?>
            <div class="overview-block clearfix">
                <h3 class="title-style-3" id="detail_hotel"><?=GetMessage('HOTEL')?></h3>
                <div class="text-block">
					<?if(is_array($arResult["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"])):?>
					<p><?=implode("<br>",$arResult["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"])?></p>
					<?else:?>
                    <p><?=$arResult["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"]?></p>
					<?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HOTEL_DESCRIPTION"]["VALUE"]["TEXT"])):?>
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["HOTEL_DESCRIPTION"]["DISPLAY_VALUE"]?></p>
                    <?endif?>
                </div>
            </div>
        <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_INCLUDE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price_inc"><?=GetMessage('PRICE_INCLUDE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PRICE_INCLUDE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_NO_INCLUDE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3" id="detail_price_no_inc"><?=GetMessage('PRICE_NO_INCLUDE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["PRICE_NO_INCLUDE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
			<?if(!empty($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"])):?>
			<div class="overview-block clearfix" id="detail_files">
				<h3 class="title-style-3" id="detail_price_inc"><?=$arResult["DISPLAY_PROPERTIES"]["FILES"]["NAME"]?></h3>
				<div class="text-block">
				<ul class="booking-card__checklist">
					<?if(count($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"]) > 1):?>

						<?foreach ($arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"] as $file):?>
							<li><i class="icon icon-check-button mr-3 text-primary"></i><a href="<?=$file["SRC"]?>" ><?=$file["DESCRIPTION"]?></a></li>
						<?endforeach;?>

					<?else:?>

						<li><i class="icon icon-check-button mr-3 text-primary"></i><a href="<?=$arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["SRC"]?>" ><?if(!empty($arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["DESCRIPTION"])):?><?=$arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["DESCRIPTION"]?><?else:?><?=$arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["ORIGINAL_NAME"]?><?endif?></a></li>

					<?endif?>
				</ul>
				</div>
			</div>
			<?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["VISA"]["VALUE"])):?>
                <div class="overview-block clearfix"  id="detail_visa">
                    <h3 class="title-style-3" id="detail_document"><?=GetMessage('DOCUMENT')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["DISPLAY_VALUE"]?></p>
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["VISA"]["VALUE"])):?>
								<ul class="booking-card__checklist">
												<li><?if(count($arResult['DISPLAY_PROPERTIES']['VISA']['VALUE']) == 1):?>
                                                    <?=$arResult['DISPLAY_PROPERTIES']['VISA']['DISPLAY_VALUE']?></li>
                                                <?else:?><li>
													<?=implode("</li><li>", array_map(function($item){
														return $item;
														}, $arResult['DISPLAY_PROPERTIES']['VISA']['DISPLAY_VALUE']))?></li>
                                                <?endif?>
								</ul>
							<?endif?>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NOTE"]["VALUE"]["TEXT"])):?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3"><?=GetMessage('NOTE')?></h3>
                    <div class="text-block">
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["NOTE"]["DISPLAY_VALUE"]?></p>
                    </div>
                </div>
            <?endif?>
            <?if(!empty($arResult["ROUTE_INFO"])):?>
                <? $this->addExternalJs($templateFolder . "/jquery-custom-google-map-lib.js"); ?>
                <?
                $zoom = isset($arResult['MAP_SCALE']) && !empty($arResult['MAP_SCALE']) ? $arResult['MAP_SCALE'] : 5;
                ?>
                <div class="overview-block clearfix">
                    <h3 class="title-style-3"><?=GetMessage('MAP')?></h3>
                    <div id="<?= $htmlMapID ?>" style="width:100%; height:500px;">
                        <script>
                            var cnt_point = '<?= count($arResult['ROUTE_INFO']) ?>';
                            if (cnt_point < 2) {
                                (function (gm) {
                                    // init map and draw route
                                    gm.createGoogleMap("<?= $htmlMapID ?>", {center: gm.LatLng(<?= $arResult['ROUTE_INFO'][0]["lat"] ?>, <?= $arResult['ROUTE_INFO'][0]["lng"] ?>), zoom: <?= $zoom?>})
                                })(window.GoogleMapFunctionsContainer)
                            } else {
                                (function (gm) {
                                    // init map and draw route
                                    gm.createGoogleMap("<?= $htmlMapID ?>", {center: gm.LatLng(0, 0), zoom: <?= $zoom ?>})
                                        .drawRoute(<?= \Bitrix\Main\Web\Json::encode($arResult['ROUTE_INFO']) ?>);
                                })(window.GoogleMapFunctionsContainer)
                            }
                        </script>
                    </div>
                </div>
            <?endif?>

    </div>
    <div class="col-md-3 sidebar-widget sidebar" id="sidebar">
        <div class="theiaStickySidebar">
            <div class="categories-widget widget">
                <div class="content-widget">
                    <ul class="widget-list">
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_TABLE"]["VALUE"]["TEXT"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_price" class="link">
                                    <span class="fa-custom category"><?=GetMessage('PRICE_TABLE')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_photo" class="link">
                                    <span class="fa-custom category"><?=GetMessage('PHOTO')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["NDAYS"]["VALUE"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_ndays" class="link">
                                    <span class="fa-custom category"><?=GetMessage('NDAYS')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["HOTEL_DESCRIPTION"]["VALUE"]["TEXT"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_hotel" class="link">
                                    <span class="fa-custom category"><?=GetMessage('HOTEL')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_INCLUDE"]["VALUE"]["TEXT"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_price_inc" class="link">
                                    <span class="fa-custom category"><?=GetMessage('PRICE_INCLUDE')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_NO_INCLUDE"]["VALUE"]["TEXT"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_price_no_inc" class="link">
                                    <span class="fa-custom category"><?=GetMessage('PRICE_NO_INCLUDE')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_files" class="link">
                                    <span class="fa-custom category"><?=GetMessage('FILES')?></span>
                                </a>
                            </li>
                        <?endif?>
                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCUMENT"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["VISA"]["VALUE"])):?>
                            <li class="single-widget-item">
                                <a href="#detail_document" class="link">
                                    <span class="fa-custom category"><?=GetMessage('DOCUMENT')?></span>
                                </a>
                            </li>
                        <?endif?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

    <?/*if(!empty($arResult["DISPLAY_PROPERTIES"]["FILE"]["VALUE"])):*/?><!--
        <div class="col-md-12">
            <h4><?/*=GetMessage('FILES')*/?></h4>
            <h4><?/*=$arResult["DISPLAY_PROPERTIES"]["HD_ADDINFORMATION"]["NAME"]*/?></h4>
            <?/* if (count($arResult["DISPLAY_PROPERTIES"]["FILES"]["VALUE"]) > 1): */?>
                <ul>
                    <?/* foreach ($arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"] as $file): */?>
                        <li><a href="<?/*= $file["SRC"] */?>"><?/*= $file["DESCRIPTION"] */?></a></li>
                    <?/* endforeach; */?>
                </ul>
            <?/* else: */?>
                <a href="<?/*= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["SRC"] */?>"><?/*= $arResult["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["DESCRIPTION"] */?></a>
            <?/* endif */?>
        </div>
    --><?/*endif*/?>

<script>
    $('a.link[href^="#"]').on("click", function () {
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top - 80}, 800);
        return false;
    });
    $(document).ready(function(){

        $('#sidebar').theiaStickySidebar({
            additionalMarginTop: 30
        });

    });
</script>