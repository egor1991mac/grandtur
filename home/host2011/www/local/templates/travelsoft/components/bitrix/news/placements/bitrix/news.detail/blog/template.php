<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$htmlMapID = "map-canvas";
?>

    <article class="post">
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"])):?>
            <header class="post-header">
                <div class="fotorama" data-allowfullscreen="true">
                    <?$images = getSrc($arResult["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"],array("width"=>848,"height"=>353));?>
                    <?foreach ($images as $key=>$img):?>
                        <img src="<?=$img?>" alt="<?=GetMessage('IMG')?> - <?=$key+1?>" title="<?=$arResult["NAME"]?> . <?=GetMessage('IMG')?> - <?=$key+1?>" />
                    <?endforeach?>
                </div>
            </header>
        <?endif?>
        <div class="post-inner">
            <h4 class="post-title text-darken"><?=$arResult["NAME"]?></h4>
            <ul class="post-meta">
                <?if(!empty($arResult["DISPLAY_ACTIVE_FROM"])):?>
                    <li><i class="fa fa-calendar"></i><a><?= $arResult["DISPLAY_ACTIVE_FROM"]?></a></li>
                <?endif;?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"])):?>
                    <?$point = array();
                    if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"])){ $point[] = strip_tags($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"]);}
                    if(!empty($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"])){ $point[] = strip_tags($arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"]);}
                    ?>
                    <li><i class="fa fa-map-marker"></i><a><?=implode(", ", $point)?></a></li>
                <?endif;?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TYPE"]["VALUE"])):?>\
                    <li><i class="fa fa-tags"></i><a><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["TYPE"]["DISPLAY_VALUE"])?></a></li>
                <?endif;?>
            </ul>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])):?>
                <h5><?=GetMessage('PRICE')?>:</h5> <p class="inl"><span class="booking-item-price"><?=$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?></span> <?=$arResult["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"]?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PRICE_FOR"]["VALUE"])):?> (<?=$arResult["DISPLAY_PROPERTIES"]["PRICE_FOR"]["VALUE"]?>)<?endif?></p><br>
            <?endif?>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TOUR"]["VALUE"])):?>
                <h5><?=GetMessage('TOUR')?>:</h5> <p class="inl"><?=$arResult["DISPLAY_PROPERTIES"]["TOUR"]["DISPLAY_VALUE"]?></p><br>
            <?endif?>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC"]["VALUE"]["TEXT"])):?>
                <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["DESC"]["~VALUE"]["TEXT"]?>
                </p>
            <?endif?>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["ABOUT_OFFER"]["VALUE"]["TEXT"])):?>
                <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["ABOUT_OFFER"]["~VALUE"]["TEXT"]?>
                </p>
            <?endif?>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["FEATURES"]["VALUE"]["TEXT"])):?>
                <h4><?=$arResult["DISPLAY_PROPERTIES"]["FEATURES"]["NAME"]?></h4>
                <p>
                    <?=$arResult["DISPLAY_PROPERTIES"]["FEATURES"]["~VALUE"]["TEXT"]?>
                </p>
            <?endif?>

            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"])):?>
                <iframe src="https://www.youtube.com/embed/<?=$arResult["DISPLAY_PROPERTIES"]["VIDEO"]["VALUE"]?>" width="848" height="353"></iframe>
            <?endif?>

        </div>
    </article>