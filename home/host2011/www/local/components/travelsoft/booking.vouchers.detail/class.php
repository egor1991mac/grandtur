<?php

/**
 * Компонент детального просмотра путевки
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftBookingVouchersDetail extends CBitrixComponent {

    /**
     * Подключает component_prolog.php, если определен в шаблоне компонента
     */
    public function includeComponentProlog() {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        if (file_exists($file_path))
            require $file_path;
        else {

            $file_path = $dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if (file_exists($file_path))
                require $file_path;
            else {
                $file_path = $dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if (file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if (file_exists($file_path))
                        require $file_path;
                }
            }
        }
    }

    /**
     * component body
     */
    public function executeComponent() {

        $this->includeComponentProlog();

        \Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        try {

            $this->arParams["VOUCHER_ID"] = intVal($this->arParams["VOUCHER_ID"]);
            if ($this->arParams["VOUCHER_ID"] <= 0) {
                throw new Exception("Unknown voucher id");
            }

            $this->arResult["VOUCHER"] = travelsoft\booking\stores\Vouchers::getById($this->arParams["VOUCHER_ID"]);

            if (empty($this->arResult["VOUCHER"])) {
                throw new Exception("Voucher with id = '" . $this->arParams["VOUCHER_ID"] . "' not found");
            }

            $this->arResult["IS_AGENT"] = travelsoft\booking\adapters\User::isAgent();

            $this->arResult["DOCS_FOR_AGENTS"] = array();

            $this->arResult["IS_ADMIN"] = travelsoft\booking\adapters\User::isAdmin();
            if ($this->arResult["IS_ADMIN"]) {
                $this->arResult["STATUSES"] = \travelsoft\booking\stores\Statuses::get();
            }

            if (!$this->arResult["IS_ADMIN"] &&
                    $this->arResult["VOUCHER"]["UF_CLIENT"] != travelsoft\booking\adapters\User::id()) {
                $GLOBALS["APPLICATION"]->AuthForm();
            }

            if (check_bitrix_sessid() && $_REQUEST["cancellation_request"] === "Y") {
                \travelsoft\booking\stores\Vouchers::update($this->arResult["VOUCHER"]["ID"], array(
                    "UF_STATUS" => travelsoft\booking\Settings::cancellationRequestStatus()));
                LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("voucher_id=" . $this->arResult["VOUCHER"]["ID"], array('sessid', 'voucher_id', 'cancellation_request'), false));
            }

            if (check_bitrix_sessid() && $_REQUEST["change_status_request"] === "Y" &&
                    $this->arResult["IS_ADMIN"] && isset($this->arResult["STATUSES"][$_REQUEST["status"]])) {
                \travelsoft\booking\stores\Vouchers::update($this->arResult["VOUCHER"]["ID"], array(
                    "UF_STATUS" => IntVal($_REQUEST["status"])));
                LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("voucher_id=" . $this->arResult["VOUCHER"]["ID"], array('sessid', 'voucher_id', 'change_status_request', 'status'), false));
            }

            $arr_tourists = array();
            $this->arResult["CONVERTER"] = new travelsoft\booking\adapters\CurrencyConverter;
            foreach ($this->arResult["VOUCHER"]["BOOKINGS_DETAIL"] as &$arr_booking) {
                $arr_tourists = array_merge($arr_tourists, (array) $arr_booking["UF_TOURISTS"]);
                $arr_booking["UF_ADULTS"] = intVal($arr_booking["UF_ADULTS"]);
                $arr_booking["UF_CHILDREN"] = intVal($arr_booking["UF_CHILDREN"]);
                if ($arr_booking["UF_EXCUR_TOUR_RATE"]) {
                    $arr_booking["TOUR_NAME"] = travelsoft\booking\stores\ExcursionTour::nameById($arr_booking["UF_SERVICE"]);
                }
                if ($arr_booking["UF_PLACEMENT_RATE"] > 0) {
                    $arr_booking["RATE_NAME"] = current(travelsoft\booking\stores\Rates::get(array(
                                        "filter" => array("ID" => $arr_booking["UF_PLACEMENT_RATE"]),
                                        "select" => array("ID", "UF_NAME")
                            )))["UF_NAME"];
                }
                if ($arr_booking["UF_TRANSFER"] > 0) {
                    $arr_booking["TRANSFER_NAME"] = \travelsoft\booking\stores\Buses::nameById((int) $arr_booking["UF_TRANSFER"]) . "[" . \travelsoft\booking\stores\Rates::nameById((int) $arr_booking["UF_TRANS_RATE"]) . "]";
                }
                if ($arr_booking["UF_ROOM"] > 0) {
                    $arr_booking["ROOM_NAME"] = \travelsoft\booking\stores\Rooms::nameById((int) $arr_booking["UF_ROOM"]);
                }
                $arr_booking["PLACEMENT_NAME"] = $this->_getPlacementName($arr_booking);
                if ($arr_booking["UF_DATE_FROM"]) {
                    $arr_booking["UF_DATE_FROM"] = $arr_booking["UF_DATE_FROM"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
                }
                if ($arr_booking["UF_DATE_TO"]) {
                    $arr_booking["UF_DATE_TO"] = $arr_booking["UF_DATE_TO"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
                }
                if ($arr_booking["UF_DATE_FROM_TRANS"]) {
                    $arr_booking["UF_DATE_FROM_TRANS"] = $arr_booking["UF_DATE_FROM_TRANS"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
                }
                if ($arr_booking["UF_DATE_BACK_TRANS"]) {

                    if ($arr_booking["UF_SERVICE_TYPE"] === "packagetour") {

                        $travel_days_back = intVal(travelsoft\booking\stores\PackageTour::getById($arr_booking["UF_SERVICE"], array("UF_TRAVEL_DAYS_BACK"))["UF_TRAVEL_DAYS_BACK"]);

                        $arr_booking["UF_DATE_BACK_TRANS"] = date(travelsoft\booking\Settings::DATE_FORMAT, $arr_booking["UF_DATE_BACK_TRANS"]->getTimestamp() + (86400 * $travel_days_back));
                    } else {
                        $arr_booking["UF_DATE_BACK_TRANS"] = $arr_booking["UF_DATE_BACK_TRANS"]->format(travelsoft\booking\Settings::DATE_FORMAT);
                    }
                }
                if ($arr_booking["UF_DATE_FROM_PLACE"]) {
                    $arr_booking["UF_DATE_FROM_PLACE"] = $arr_booking["UF_DATE_FROM_PLACE"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
                }
                if ($arr_booking["UF_DATE_TO_PLACE"]) {
                    $arr_booking["UF_DATE_TO_PLACE"] = $arr_booking["UF_DATE_TO_PLACE"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
                }
            }
            $this->arResult["VOUCHER"]["STATUS"] = current(\travelsoft\booking\stores\Statuses::get(array(
                        "filter" => array("ID" => $this->arResult["VOUCHER"]["UF_STATUS"]),
                        "select" => array("ID", "UF_NAME")
            )));
            $this->arResult["TOURISTS"] = array();
            if (!empty($arr_tourists)) {
                $this->arResult["TOURISTS"] = \travelsoft\booking\stores\Tourists::namesListByIdList($arr_tourists);
                $this->arResult["VOUCHER"]["TOURISTS"] = \travelsoft\booking\stores\Tourists::get(array(
                            "filter" => array("ID" => $arr_tourists)
                ));
            }

            if ($this->arResult["IS_ADMIN"]) {
                if ((!empty($this->arParams["DOCS_FOR_AGENTS"]) && is_array($this->arParams["DOCS_FOR_AGENTS"])) ||
                        (!empty($this->arParams["DOCS_FOR_CLIENTS"]) && is_array($this->arParams["DOCS_FOR_CLIENTS"]))) {
                    $this->arResult["DOCS_FOR_PRINT"] = \travelsoft\booking\stores\Documents::get(array(
                                "filter" => array("ID" => array_merge((array) $this->arParams["DOCS_FOR_AGENTS"], (array) $this->arParams["DOCS_FOR_CLIENTS"]))
                    ));
                }
            } elseif ($this->arResult["IS_AGENT"]) {

                if (!empty($this->arParams["DOCS_FOR_AGENTS"]) && is_array($this->arParams["DOCS_FOR_AGENTS"])) {
                    $this->arResult["DOCS_FOR_PRINT"] = \travelsoft\booking\stores\Documents::get(array(
                                "filter" => array("ID" => $this->arParams["DOCS_FOR_AGENTS"])
                    ));
                }
            } else {

                if (!empty($this->arParams["DOCS_FOR_CLIENTS"]) && is_array($this->arParams["DOCS_FOR_CLIENTS"])) {
                    $this->arResult["DOCS_FOR_PRINT"] = \travelsoft\booking\stores\Documents::get(array(
                                "filter" => array("ID" => $this->arParams["DOCS_FOR_CLIENTS"])
                    ));
                }
            }

            if ($_REQUEST["print"] > 0 && isset($this->arResult["DOCS_FOR_PRINT"][$_REQUEST["print"]])) {
                $this->_printing();
            }

            $this->arResult["SHOW_OK_BOOKING_MESSAGE"] = false;
            if ($_SESSION["COME_FROM_BOOKING_PAGE"]) {
                unset($_SESSION["COME_FROM_BOOKING_PAGE"]);
                $this->arResult["SHOW_OK_BOOKING_MESSAGE"] = true;
            }

            $this->IncludeComponentTemplate();
        } catch (\Exception $e) {
            echo $e->getMessage();
            (new \travelsoft\booking\Logger)->write("Component booking.vouchers.detail: " . $e->getMessage());
            ShowError("System error.");
        }
    }

    protected function _printing() {
        $GLOBALS["APPLICATION"]->RestartBuffer();
        if ($this->arResult["DOCS_FOR_PRINT"][$_REQUEST["print"]]["UF_TEMPLATE"] <= 0) {
            ShowError("Missing document template.");
        } elseif (!class_exists($this->arResult["DOCS_FOR_PRINT"][$_REQUEST["print"]]["UF_CLASS"])) {
            ShowError("Missing class for processing.");
        } elseif ($this->arResult["IS_ADMIN"] || $this->arResult["VOUCHER"]["UF_CLIENT"] == \travelsoft\booking\adapters\User::id()) {

            $tpl = CFile::GetFileArray($this->arResult["DOCS_FOR_PRINT"][$_REQUEST["print"]]["UF_TEMPLATE"]);

            $this->arResult["DOCS_FOR_PRINT"][$_REQUEST["print"]]['UF_CLASS']::create($this->arResult["VOUCHER"], $tpl['SRC'], "pdf");
            echo "<script>window.close()</script>";
        } else {

            ShowError("Have no permission for print.");
        }
        die();
    }

    protected function _getPlacementName(array $arr_booking) {

        switch ($arr_booking["UF_SERVICE_TYPE"]) {

            case "placements":
                if ($arr_booking["UF_SERVICE"] > 0) {
                    return $this->_getPlacementNameById((int) $arr_booking["UF_SERVICE"]);
                }
                break;
            case "packagetour":
                if ($arr_booking["UF_PLACEMENT"] > 0) {
                    return $this->_getPlacementNameById((int) $arr_booking["UF_PLACEMENT"]);
                }
                break;
        }
    }

    protected function _getPlacementNameById(int $id) {
        return (string) current(travelsoft\booking\stores\Placements::get(array(
                            "filter" => array("ID" => $id),
                            "select" => array("ID", "NAME")
                )))["NAME"];
    }

}
