<?php

/*
 * Обработка запроса на поиск ценовых предложений
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

header('Content-Type: application/json; charset=' . SITE_CHARSET);

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

try {

    CBitrixComponent::includeComponentClass("travelsoft:booking.offers");

    $component = new TravelsoftBookingOffers();

    $component->arParams["SERVICE_TYPE"] = $_GET["service_type"];

    $arr_result = $component->prepareParameters()->getOffers();
    
    if ((!isset($arr_result["offers"]) || empty($arr_result["offers"])) && !in_array($component->arParams["SERVICE_TYPE"], array("transfer", "transferback"))) {
        $arr_result = $component->getNearestAvailableOffers();
    }

    echo json_encode(array(
        "error" => false,
        "data" => (array)$arr_result["offers"],
        "nearest_available" => (bool)$arr_result["nearest_available"]
    ));
} catch (Exception $ex) {
    
    (new \travelsoft\booking\Logger)->write("Component booking.offers: " . $ex->getMessage());
    echo json_encode(array("error" => true, "data" => array(), "nearest_available" => false));
}

die;
