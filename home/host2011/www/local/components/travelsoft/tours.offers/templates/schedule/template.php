<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<? if (!empty($arResult["OFFERS"]) && !empty($arResult["COUNTRIES"])): ?>
    <div class="row" style="border-bottom: 1px solid rgb(227, 227, 227); margin-bottom: 15px; padding-bottom: 15px;">
        <div class="col-md-6">
            <div class="country-list">
                <span><a data-id="all" href="#all" class="anchor"><?=GetMessage("ALL_COUNTRIES")?></a></span>
                <? foreach ($arResult["COUNTRIES"] as $k => $country): ?>
                    <span><a data-id="<?= $k ?>" href="#country-<?= $k ?>" class="anchor"><?= $country["NAME"] ?></a></span>
                <? endforeach; ?>
            </div>
        </div>
        <div class="col-md-6" id="legend-area">
            <span class="legend-part"><b><?=GetMessage('LEGEND')?></b></span> <span class="legend-part"><b class="red">01.01</b> - <b><?=GetMessage('NO_PLACES')?></b>, </span> <span class="legend-part"><b class="yellow">01.01</b> - <b><?=GetMessage('FEW_PLACES')?></b>, </span><span class="legend-part"><b class="green">01.01</b> - <b><?=GetMessage('PLACES_ARE')?></b></span>
        </div>
    </div>
    <div class="schedule-body availabily-wrapper">
        <ul class="availabily-list">
            <? if (!empty($arResult["MONTH"])): ?>
                <li class="availabily-heading bg-white clearfix">
                    <div class="availabily-name" style="text-align: center"><?=GetMessage('NAME_TITLE')?></div>
                    <div class="availabily-other price-from"><?=GetMessage('PRICE_TITLE')?></div>
                    <? foreach ($arResult["MONTH"] as $month): ?>
                        <div class="availabily-other first"><?= $month ?></div>
                    <? endforeach; ?>
                    <div class="availabily-other last"><?=GetMessage('DATE_TITLE')?></div>
                </li>
            <? endif ?>
            <? foreach ($arResult["COUNTRIES"] as $key => $countryName): ?>
                <li id="country-<?= $key ?>" class="group-<?= $key ?> availabily-content country clearfix">
                    <div style="width: 100%;text-align: center"><span><?= $countryName["NAME"] ?></span></div>
                </li>
                <? foreach ($countryName['OFFERS'] as $item): ?>

                    <li class="group-<?= $key ?> availabily-content clearfix parent-schedule-cells">
                        <div class="schedule-cell availabily-name">
                            <? if (!empty($item["TOUR_TYPE"])): ?><span class="span-tour-type red"><?=GetMessage('TOUR_TYPE')?></span><? endif; ?>
                            <? if (!empty($item["NEW"])): ?><span class="span-tour-type"><?=GetMessage('NEW')?></span><? endif; ?>
                            <? if (!empty($item["FREE"])): ?><span class="span-tour-type green"><?=GetMessage('FREE')?></span><? endif; ?>
                            <? if (!empty($item["LEADER"])): ?><span class="span-tour-type"><?=GetMessage('LEADER')?></span><? endif; ?>
                            <? if (!empty($item["OFFER_DAY"])): ?><span class="span-tour-type green"><?=GetMessage('OFFER_DAY')?></span><? endif; ?>
                            <? if (!empty($item["SPECIAL_OFFER"])): ?><span class="span-tour-type red"><?=GetMessage('SPECIAL_OFFER')?></span><? endif; ?>
                            <a <? if (!empty($item["DETAIL_PAGE_URL"])): ?>target="_blank" href="<?= $item["DETAIL_PAGE_URL"] ?>"<? endif ?>>
                                <h4><?= $item["NAME"] ?></h4>
                            </a>
                            <? if (strlen($item['ROUTE']) > 0): ?>
                                <h6><i class="icon icon-label mr-2 text-secondary"></i> <?= $item['ROUTE'] ?></h6>
                            <? endif ?>
                            <? if (!empty($item["DETAIL_PAGE_URL"])): ?><p><a target="_blank" class="read_more" href="<?= $item["DETAIL_PAGE_URL"] ?>"><?=GetMessage('READ_MORE')?></a></p><? endif ?>
                        </div>
                        <div class="schedule-cell availabily-other price-from">
                            <span class="price-container">
                                <?
                                if (!empty($item["PRICE"]) && !empty($arResult["CURRENCY"])):?>
                                    <span class="offer-price"><i class="cost"><?=$item["PRICE"]?></i> <i class="currency"><?=$arResult["CURRENCY"]?></i></span>
                                <? else: ?>
                                    <? echo " - "; ?>
                                <? endif; ?>
                            </span>
                        </div>
                        <? if (!empty($item["DATES_"])): ?>
                            <? foreach ($arResult["MONTH"] as $m => $month): ?>
                                <div class="schedule-cell availabily-other first">
                                    <? if (isset($item["DATES_"][$m])): sort($item["DATES_"][$m]) ?>
                                        <? foreach ($item["DATES_"][$m] as $date): ?>
                                            <span class="<? if ($date["quota"] == 0): ?>red<? elseif ($date["quota"] <= 7 && $date["quota"] != 0): ?>yellow<? else: ?>green<? endif ?>"><?= $date["date_dm"] ?></span><br>
                                        <? endforeach ?>
                                    <? else: ?>
                                        <span><? echo " - "; ?></span>
                                    <? endif; ?>


                                </div>
                            <? endforeach; ?>
                        <? endif ?>

                        <div class="schedule-cell availabily-other last">
                            <? if (!empty($item["DATES_"])): ?>
                                <? foreach ($arResult["MONTH"] as $m => $month): ?>
                                    <? if (isset($item["DATES_"][$m])): sort($item["DATES"][$m]) ?>
                                        <? foreach ($item["DATES_"][$m] as $date): ?>
                                            <span class="<? if ($date["quota"] == 0): ?>red<? elseif ($date["quota"] <= 7): ?>yellow<? else: ?>green<? endif ?>"><?= $date["date_dm"] ?></span><br>
                                        <? endforeach ?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif ?>
                        </div>
                    </li>
                <? endforeach; ?>
            <? endforeach; ?>
        </ul>
    </div>
<? endif ?>
<script>
    /**
     * Smooth scroll to anchor
     */
    jQuery('a.anchor[href^="#"]').on("click", function (ev) {

        ev.preventDefault();

        var $this = jQuery(this);
        var id = $this.data('id');

        if ($this.hasClass('active')) {
            return false;
        }

        jQuery('a.anchor[href^="#"]').removeClass('active');

        $this.addClass('active');

        if (id === 'all') {

            jQuery('li[class^="group-"]').show();
            return true;
        }

        jQuery('li[class^="group-"]').hide();

        jQuery('li[class^="group-' + id + '"]').show();

        return true;
    });

    // ВЫРАВНИВАНИЕ ЯЧЕЕК ТАБЛИЦЫ ПО ВЫСОТЕ
    jQuery('li.parent-schedule-cells').each(function () {

        var maxHeight = 0;
        var cells = jQuery(this).find('.schedule-cell');
        cells.each(function () {

            var height = jQuery(this).outerHeight();
            if (height > maxHeight) {
                maxHeight = height;
            }
        });

        cells.height(maxHeight);
    });
</script>

