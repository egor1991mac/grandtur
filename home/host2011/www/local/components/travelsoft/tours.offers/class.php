<?php

/**
 * Класс туристических предложений
 *
 * @author juliya.sharlova
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftToursOffers extends CBitrixComponent {

    public $_tourTypes = null;
    public $_today_date = null;
    public $_period_date = null;
    public $_default_request = null;

    /**
     * Обработка входных параметров
     * @throws \Exception
     */
    public function prepareParameters() {

        Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        if (empty($this->arParams['IBLOCK_ID'])) {

            throw new \Exception('Укажите ID инфоблока предложений.');
        }
    }

    /**
     *  Вывод предложений
     */
    public function getCountriesList() {

        $cache_id = 'main_search_countries_for_schedule ' + md5(serialize($this->arParams));
        $cache_dir = "/travelsoft/countries_getlist_for_schedule";

        $obCache = new CPHPCache;
        if ($obCache->InitCache(360000000, $cache_id, $cache_dir)) {
            $arCountries = $obCache->GetVars();
        } elseif (CModule::IncludeModule("iblock") && $obCache->StartDataCache()) {
            $arCountries = array();

            $db_countries = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => COUNTRIES_ID_IBLOCK, "ACTIVE" => "Y"), false, false, Array("ID", "NAME"));

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);

            while ($ar_fields = $db_countries->GetNext()) {

                $CACHE_MANAGER->RegisterTag("countries_iblock_id_for_schedule_" . $ar_fields["ID"]);
                $arCountries[$ar_fields["ID"]] = array(
                    "ID" => $ar_fields["ID"],
                    "NAME" => $ar_fields["NAME"]
                );
            }

            $CACHE_MANAGER->RegisterTag("iblock_id_countries_for_schedule");
            $CACHE_MANAGER->EndTagCache();

            $obCache->EndDataCache($arCountries);
        } else {
            $arCountries = array();
        }

        return $arCountries;
    }

    /**
     *  Вывод предложений
     */
    public function getTours() {

        $cache_id = 'main_search_tours_for_schedule ' + md5(serialize($this->arParams));
        $cache_dir = "/travelsoft/tours_getlist_for_schedule";

        $obCache = new CPHPCache;
        if ($obCache->InitCache(360000000, $cache_id, $cache_dir)) {
            $arTours = $obCache->GetVars();
        } elseif (CModule::IncludeModule("iblock") && $obCache->StartDataCache()) {
            $arTours = array();

            $arSelect = Array("ID", "PROPERTY_" . $this->arParams["PROPERTY_CODE"], "PROPERTY_COUNTRY", "NAME", "DETAIL_PAGE_URL");

            if (!empty($this->arParams["PROPERTY_LIST"])) {
                foreach ($this->arParams["PROPERTY_LIST"] as $prop) {
                    $arSelectProp[] = 'PROPERTY_' . $prop;
                }
                $arSelect = array_merge($arSelect, $arSelectProp);
            }

            $db_tours = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y"), false, false, $arSelect);

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);

            while ($ar_fields = $db_tours->GetNext()) {
                if (!empty($ar_fields["PROPERTY_" . $this->arParams["PROPERTY_CODE"] . "_VALUE"]) && !empty($ar_fields["PROPERTY_COUNTRY_VALUE"])) {

                    $CACHE_MANAGER->RegisterTag("tours_iblock_id_for_schedule_" . $ar_fields["ID"]);

                    $arTours[$ar_fields["ID"]] = array(
                        "ID" => $ar_fields["ID"],
                        "NAME" => $ar_fields["NAME"],
                        "DETAIL_PAGE_URL" => $ar_fields["DETAIL_PAGE_URL"],
                        "COUNTRY" => $ar_fields["PROPERTY_COUNTRY_VALUE"],
                        "TOURTYPE" => $this->_tourTypes[$ar_fields["PROPERTY_" . $this->arParams["PROPERTY_CODE"] . "_VALUE"]] ?? ''
                    );

                    if (!empty($this->arParams["PROPERTY_LIST"])) {
                        foreach ($this->arParams["PROPERTY_LIST"] as $prop) {
                            if (!empty($prop))
                                $arTours[$ar_fields["ID"]][$prop] = $ar_fields["PROPERTY_" . $prop . "_VALUE"];
                        }
                    }
                }
            }

            $CACHE_MANAGER->RegisterTag("iblock_id_tours_for_schedule");
            $CACHE_MANAGER->EndTagCache();

            $obCache->EndDataCache($arTours);
        }
        else {
            $arTours = array();
        }

        return $arTours;
    }

    public function add_month($time, $month) {
        $d = date('j', $time);  // день
        $m = date('n', $time);  // месяц
        $y = date('Y', $time);  // год
        // Прибавить месяц
        $m = $m + $month;
        if ($m > 12) {
            $y++;
            $m = $m - 12;
        }

        // Это последний день месяца?
        if ($d == date('t', $time)) {
            $d = 31;
        }
        // Открутить дату до последнего дня месяца
        if (!checkdate($m, $d, $y)) {
            $d = date('t', mktime(0, 0, 0, $m, 1, $y));
        }
        // Вернуть новую дату в TIMESTAMP
        return mktime(0, 0, 0, $m, $d, $y);
    }

    /**
     * Получение минимальной цены на дату пакетного тура
     */
    public function getMinPriceForDatePackageTour($price_duration, $pkg_id) {

        $arMinPriceForOffer = array();

        $_GET["travelbooking"] = $this->_default_request;
        $_GET['travelbooking']["services_id"] = [$pkg_id];

        foreach ($price_duration as $offer_min_price) {

            $d = $_GET['travelbooking']["date_from"] = date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($offer_min_price["UF_DATE"]) - 86400);
            $_GET['travelbooking']["date_to"] = date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($offer_min_price["UF_DATE"]) + (86400 * ($offer_min_price["UF_DURATION"])));
            
            $arr_prices = \travelsoft\booking\Utils::getMinCalculationResultForPackageTour();
            $currencies = $arr_min_prices = [];
            if (!empty($arr_prices)) {
                foreach ($arr_prices as $arr_price_data) {
                    $arr_min_prices[$d][] = $arr_price_data["result_price"];
                    $currencies[$d] = $arr_price_data["currency"];
                }
                 
                $arMinPriceForOffer[$d] = array(
                    "date" => $d,
                    "date_dm" => date("d.m", strtotime($d)),
                    "price" => min($arr_min_prices[$d]),
                    "quota" => 10,
                    "currency" => $currencies[$d]
                );
            }
//            $arRoomsMinPrices = \travelsoft\booking\Utils::getMinCalculationResultForRooms();
//            $currencies = [];
//            if (!empty($arRoomsMinPrices)) {
//                foreach ($arRoomsMinPrices as $room_min_price) {
//                    $arRoomsMinPriceForDate[$offer_min_price["UF_DATE"]][] = $room_min_price["result_price"];
//                    $currencies[$offer_min_price["UF_DATE"]] = $room_min_price["currency"];
//                }
//
//                $arMinPriceForOffer[$offer_min_price["UF_DATE"]] = array(
//                    "date" => $offer_min_price["UF_DATE"],
//                    "date_dm" => date("d.m", strtotime($offer_min_price["UF_DATE"])),
//                    "price" => min($arRoomsMinPriceForDate[$offer_min_price["UF_DATE"]]),
//                    "quota" => 10,
//                    "currency" => $currencies[$offer_min_price["UF_DATE"]]
//                );
//            }
        }

        return $arMinPriceForOffer;
    }

    /**
     * Получение минимальной цены предложения
     */
    public function getMinPriceForOffers($arMinPriceForDate) {

        $dates = array();

        foreach ($arMinPriceForDate as $date) {
            $price_ = \travelsoft\currency\factory\Converter::getInstance()->convert($date["price"], $date["currency"])->getResultLikeArray();
            $dates[] = number_format($price_["price"], 2, ".", " ");
        }

        return min($dates);
    }

    /**
     * Получение минимальной цены на дату экскурсионного тура
     */
    public function getMinPriceForDateExcursionTour($d_from, $d_to, $tours_id) {

        $arTour = $tours_id;

        $period = new DatePeriod(
                new DateTime($d_from), new DateInterval('P1D'), new DateTime($d_to)
        );

        $_GET["travelbooking"] = $this->_default_request;
        $_GET['travelbooking']["excursions_id"] = (array) $tours_id;

        $_GET['travelbooking']["date_from"] = $d_from;
        $_GET['travelbooking']["date_to"] = $d_to;

        $arExTourMinPrices = \travelsoft\booking\Utils::getMinCalculationResultForExcursionTour();
        //dm($arExTourMinPrices);
        $arMinPriceForOffer = array();

        $arExTourDates = travelsoft\booking\stores\Quotas::get(array(
                    "filter" => array(
                        "UF_SERVICE_TYPE" => "excursiontour",
                        "UF_STOP" => false,
                        "!UF_QUOTA" => false,
                        "><UF_DATE" => array(
                            $d_from,
                            $d_to
                        )
                    ),
                    "order" => array("UF_DATE" => "ASC"),
                    "select" => array("ID", "UF_SOLD_NUMBER", "UF_QUOTA", "UF_DATE", "UF_SERVICE_ID")
        ));

        foreach ($arExTourDates as $exDate) {

            $eDate = \travelsoft\booking\Utils::dateConvert($exDate["UF_DATE"]->toString());
            if (isset($arTour[$exDate["UF_SERVICE_ID"]])) {
                $arMinPriceForOffer[$arTour[$exDate["UF_SERVICE_ID"]]]["DATES"][] = array(
                    "date" => $eDate,
                    "date_dm" => date("d.m", strtotime($eDate)),
                    "quota" => $exDate["UF_QUOTA"] - $exDate["UF_SOLD_NUMBER"]
                );
            }
        }

        //dm($arMinPriceForOffer);

        /* if(!empty($arExTourMinPrices)) {
          foreach ($arExTourMinPrices as $key=>$tour_min_price){

          $quota_service = \travelsoft\booking\stores\Quotas::get(array(
          "filter" => array(
          "UF_SERVICE_ID" => $key,
          "UF_SERVICE_TYPE" => "excursiontour",
          "UF_DATE" => $tour_min_price["date"]
          ),
          "order" => array("UF_DATE" => "ASC"),
          "select" => array("ID", "UF_SOLD_NUMBER", "UF_QUOTA")
          ));

          $quota_service_ = current($quota_service);

          $price_ = \travelsoft\currency\factory\Converter::getInstance()->convert($tour_min_price["result_price"], $tour_min_price["currency"])->getResultLikeArray();
          $dates[$arTour[$key]][] = $price_["price"];

          $arMinPriceForOffer[$arTour[$key]]["DATES"][$tour_min_price["date"]] = array(
          "date" => $tour_min_price["date"],
          "date_dm" => date("d.m", strtotime($tour_min_price["date"])),
          "price" => $tour_min_price["result_price"],
          "quota" => $quota_service_["UF_QUOTA"] - $quota_service_["UF_SOLD_NUMBER"],
          "currency" => $tour_min_price["currency"]
          );

          }

          } */

        if (!empty($arExTourMinPrices)) {
            foreach ($arExTourMinPrices as $key => $tour_min_price) {

                $price_ = \travelsoft\currency\factory\Converter::getInstance()->convert($tour_min_price["result_price"], $tour_min_price["currency"])->getResultLikeArray();
                $dates[$arTour[$key]][] = $price_["price"];
            }
        }


        /* foreach ($period as $key => $value) {
          $_GET['travelbooking']["date_from"] = $value->format(\travelsoft\booking\Settings::DATE_FORMAT);
          $_GET['travelbooking']["date_to"] = $value->format(\travelsoft\booking\Settings::DATE_FORMAT);
          $arExTourMinPrices = \travelsoft\booking\Utils::getMinCalculationResultForExcursionTour();

          if(!empty($arExTourMinPrices)) {
          foreach ($arExTourMinPrices as $key=>$tour_min_price){

          $quota_service = \travelsoft\booking\stores\Quotas::get(array(
          "filter" => array(
          "UF_SERVICE_ID" => $key,
          "UF_SERVICE_TYPE" => "excursiontour",
          "UF_DATE" => $tour_min_price["date"]
          ),
          "order" => array("UF_DATE" => "ASC"),
          "select" => array("ID", "UF_SOLD_NUMBER", "UF_QUOTA")
          ));
          $quota_service = current($quota_service);

          $dates[$arTour[$key]][]  = $tour_min_price["result_price"];
          $arMinPriceForOffer[$arTour[$key]]["DATES"][$tour_min_price["date"]] = array(
          "date" => $tour_min_price["date"],
          "date_dm" => date("d.m", strtotime($tour_min_price["date"])),
          "price" => $tour_min_price["result_price"],
          "quota" => $quota_service["UF_QUOTA"] - $quota_service["UF_SOLD_NUMBER"],
          );
          }

          }

          } */

        if (!empty($dates)) {
            foreach ($dates as $k => $date) {
                $arMinPriceForOffer[$k]["PRICE"] = number_format(min($dates[$k]), 2, ".", " ");
            }
        }

        return $arMinPriceForOffer;
    }

    /**
     * Устанавливает arResult
     */
    public function setDataOffers() {

        $this->arResult["OFFERS"] = array();
        $arOffers = array();

        $result_iblock = $this->getTours();

        $arMinPriceForDateOffer = array();
        $arMinPriceForOffer = array();

        $tours_id = array();

        if (!empty($result_iblock)) {

            foreach ($result_iblock as $key => $tour) {

                switch ($tour["TOURTYPE"]) {

                    case "excursiontour":

                        $tour_ = \travelsoft\booking\Utils::getExcursionTourByDescription($tour["ID"]);

                        if (!empty($tour_)) {

                            //if(isset($tour_[0]) && !empty($tour_[0]) || !isset($tour_[0])) {
                            if (isset($tour_) && !empty($tour_) || !isset($tour_)) {

                                if (!empty($tour_["UF_TOUR"])) {

                                    $tours_id[$tour_["ID"]] = $tour_["UF_TOUR"];
                                }
                            }
                        }

                        break;

                    case "packagetour":

                        $tour_ = \travelsoft\booking\Utils::getPackageTourByDescription($tour["ID"]);

                        if (!empty($tour_)) {

                            //if(isset($tour_[0]) && !empty($tour_[0]) || !isset($tour_[0])){
                            if (isset($tour_) && !empty($tour_) || !isset($tour_)) {

                                if (!empty($tour_["UF_PLACEMENT"])) {

                                    $rooms = \travelsoft\booking\Utils::getRoomsByPlacement($tour_["UF_PLACEMENT"]);
                                    if (!empty($rooms)) {

                                        $rooms_id = array();
                                        $min_price_duration = array();

                                        foreach ($rooms as $room) {
                                            $duration_room = \travelsoft\booking\stores\Duration::get(array(
                                                        "filter" => array(
                                                            "UF_SERVICE_ID" => $room["ID"],
                                                            "UF_SERVICE_TYPE" => "rooms",
                                                            "><UF_DATE" => array(
                                                                $this->_today_date, $this->_period_date
                                                            )
                                                        ),
                                                        "order" => array("UF_DATE" => "ASC"),
                                                        "select" => array("ID", "UF_DATE", "UF_DURATION")
                                            ));
                                            $rooms_id[] = $room["ID"];

                                            foreach ($duration_room as $value) {

                                                $date_arrival = \travelsoft\booking\Utils::dateConvert($value["UF_DATE"]);

                                                if (!empty($date_arrival)) {

                                                    if (!isset($min_price_duration[$date_arrival])) {
                                                        $min_price_duration[$date_arrival] = array(
                                                            "ID" => $value["ID"],
                                                            "UF_DATE" => $date_arrival,
                                                            "UF_DURATION" => current($value["UF_DURATION"])
                                                        );
                                                    }
                                                    if ($value["UF_DURATION"] < $min_price_duration[$date_arrival]["UF_DURATION"]) {
                                                        $min_price_duration[$date_arrival] = array(
                                                            "ID" => $value["ID"],
                                                            "UF_DATE" => $date_arrival,
                                                            "UF_DURATION" => current($value["UF_DURATION"])
                                                        );
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (!empty($min_price_duration)) {

                                        $arMinPriceForOffer[$tour["ID"]]["DATES"] = $this->getMinPriceForDatePackageTour($min_price_duration, $tour_["ID"]);
                                        $arMinPriceForOffer[$tour["ID"]]["PRICE"] = $this->getMinPriceForOffers($arMinPriceForOffer[$tour["ID"]]["DATES"]);
                                    }
                                }
                            }
                        }

                        break;
                }
            }

            if (!empty($tours_id)) {

                $arExTour = $this->getMinPriceForDateExcursionTour($this->_today_date, $this->_period_date, $tours_id);

//dm($arMinPriceForOffer);
                if (!empty($arExTour)) {
                    $arOffers_ = $arMinPriceForOffer + $arExTour;
                }
            }

            foreach ($arOffers_ as $key => $offer) {
                $this->arResult["OFFERS"][$key] = $result_iblock[$key];
                $this->arResult["OFFERS"][$key] = $this->arResult["OFFERS"][$key] + $offer;

                if (!empty($result_iblock[$key]["COUNTRY"]))
                    $this->arResult["COUNTRIES"][$result_iblock[$key]["COUNTRY"]] = $this->countries[$result_iblock[$key]["COUNTRY"]];
            }
        }
    }

    /**
     * component body
     */
    public function executeComponent() {

        Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");
        Bitrix\Main\Loader::includeModule('travelsoft.currency');
        $this->_tourTypes = array("43" => "excursiontour", "45" => "packagetour");
        $this->_today_date = date(\travelsoft\booking\Settings::DATE_FORMAT, time());
        $this->_period_date = date(\travelsoft\booking\Settings::DATE_FORMAT, $this->add_month(strtotime($this->_today_date), 6));
        $this->_default_request = travelsoft\booking\Utils::makeCommonSearchRequest();
        $this->_default_request["adults"] = 1;
        $this->arResult["CURRENCY"] = \travelsoft\currency\stores\Currencies::getISObyId(\travelsoft\booking\adapters\CurrencyConverter::getCurrentCourseId());

        try {

            $this->prepareParameters();
            $this->countries = $this->getCountriesList();

            $this->setDataOffers();

            $this->IncludeComponentTemplate();
        } catch (\Exception $e) {

            ShowError($e->getMessage());
        }
    }

}
