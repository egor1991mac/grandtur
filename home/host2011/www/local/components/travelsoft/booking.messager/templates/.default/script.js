/* 
 * Voucher messages
 */

$(document).ready(function () {

    $('#send-message').on('click', function () {

        var _this = this;

        var $this = $(_this);

        var textarea = $('textarea[name=message]');

        var message = textarea.val().trim().replace(/<[^>]*>/g, '');

        if (!message) {
            alert('Необходимо ввести сообщение.');
            return;
        }

        $.ajax({
            method: 'post',
            url: $this.data('ajax-url'),
            dataType: 'json',
            data: {
                sessid: $this.data('sessid'),
                action: 'add-message',
                message: message,
                voucher_id: $this.data('voucher-id')
            },
            beforeSend: function () {
                _this.disabled = true;
                BX.showWait();
            },
            complete: function () {
                _this.disabled = false;
                BX.closeWait();
            },
            success: function (resp) {

                if (resp.error) {
                    console.log(resp.message);
                    return;
                }

                $("#tech-message").remove();

                $('#messages-container').append(`
                    <div class="message-container">

                        <div class="user-container"><b><i>${resp.data.user.FULL_NAME_WITH_EMAIL} &nbsp;&nbsp;&nbsp; ${resp.data.date}</i></b></div>

                        <div id="message-${resp.message_id}" class="message">${message}</div>

                    </div>
                `);
            }
        });

        textarea.val('');
    });

});