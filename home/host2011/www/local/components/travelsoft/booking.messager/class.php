<?php

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

/**
 * Компонент сообщений по путевке
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftVouchersMessager extends CBitrixComponent {

    public function executeComponent() {

        try {

            $this->arParams['VOUCHER_ID'] = intVal($this->arParams['VOUCHER_ID']);

            if ($this->arParams['VOUCHER_ID'] <= 0) {

                throw new Exception('Booking not found.');
            }

            $this->arResult['MESSAGES'] = [];

            foreach (\array_reverse(\array_values(\travelsoft\booking\stores\Messages::get([
                                'order' => ['UF_DATE' => 'DESC'],
                                'filter' => ['UF_VOUCHER_ID' => $this->arParams['VOUCHER_ID']],
                                'limit' => 20
            ]))) as $message) {
                
                $this->arResult['MESSAGES'][] = [
                    'ID' => $message['ID'],
                    'MESSAGE' => strip_tags(trim($message['UF_MESSAGE'])),
                    'USER' => \travelsoft\booking\stores\Users::getById($message['UF_USER_ID']),
                    'DATE' => $message['UF_DATE']->toString()
                ];
            }
            
            \CJSCore::Init();
            
            $this->includeComponentTemplate();
        } catch (Exception $ex) {
            \ShowError($ex->getMessage());
        }
    }

}
