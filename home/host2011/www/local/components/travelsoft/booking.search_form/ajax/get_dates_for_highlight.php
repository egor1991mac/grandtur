<?php

/*
 * Обработка запроса для получения дат подстветки в календаре
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

header('Content-Type: application/json; charset=' . SITE_CHARSET);

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$response = array();

if (strlen($_POST["service_type"]) && !empty($_POST["services_id"]) && is_array($_POST["services_id"])) {

    $arr_quotas = travelsoft\booking\stores\Quotas::get(array(
        "filter" => array(
            "UF_SERVICE_ID" => $_POST["services_id"],
            "UF_SERVICE_TYPE" => $_POST["service_type"],
            "UF_STOP" => false,
            "!UF_QUOTA" => false,
            ">UF_DATE" => travelsoft\booking\adapters\Date::createFromTimestamp(time())),
        "select" => array("ID", "UF_DATE", "UF_SERVICE_ID")
    ));
    if ($_POST["service_type"] === "transferback") {

        $arr_travelback_days_grouped_by_transferback = array();
        foreach (\travelsoft\booking\Utils::getPackageTours(NULL, NULL, $_POST["services_id"]) as $arr_package_tour) {
            $arr_travelback_days_grouped_by_transferback[$arr_package_tour["UF_TRANSFER_BACK"]] = $arr_package_tour["UF_TRAVEL_DAYS_BACK"];
        }

        foreach ($arr_quotas as $arr_quota) {
            if (isset($arr_travelback_days_grouped_by_transferback[$arr_quota["UF_SERVICE_ID"]])) {
                $response[] = date(travelsoft\booking\Settings::DATE_FORMAT, $arr_quota["UF_DATE"]->getTimestamp() + (86400*$arr_travelback_days_grouped_by_transferback[$arr_quota["UF_SERVICE_ID"]]));
            }

        }
    } else {
        foreach ($arr_quotas as $arr_quota) {
            $response[] = \travelsoft\booking\Utils::dateConvert($arr_quota["UF_DATE"]->toString());
        }
    }
    
}

echo json_encode(array_values(array_unique($response)));
