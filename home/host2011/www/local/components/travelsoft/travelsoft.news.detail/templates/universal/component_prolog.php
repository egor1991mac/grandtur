<?
global $APPLICATION;
$picturesID = Array();
$picturesDescription = Array();

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arParams["ELEMENT_CODE"], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
if ($ob = $res->GetNextElement()) {
    $arProps = $ob->GetProperties();
    $picturesID = $arProps["PICTURES"]["VALUE"];
    $picturesDescription = $arProps["PICTURES"]["DESCRIPTION"];
}

if (!empty($picturesID) && !empty($picturesDescription)) {
    ob_start();
    $APPLICATION->IncludeComponent(
        "travelsoft:travelsoft.slider",
        "",
        Array(
            "AUTO_PLAY_BIG" => "N",
            "AUTO_PLAY_HOVER_PAUSE_BIG" => "Y",
            "AUTO_PLAY_TIMEOUT_BIG" => "3000",
            "DATA_DESCRIPTION" => $picturesDescription,
            "DATA_SOURCE" => $picturesID,
            "DOT_BIG" => "N",
            "DOT_SMALL" => "N",
            "DO_NOT_INC_MAGNIFIC_POPUP" => "N",
            "DO_NOT_INC_OWL_CAROUSEL" => "N",
            "HEIGHT" => "480",
            "INC_JQUERY" => "N",
            "ITEM_COUNT_BIG" => "1",
            "ITEM_COUNT_SMALL" => "6",
            "LAZY_LOAD_BIG" => "Y",
            "LAZY_LOAD_SMALL" => "Y",
            "MARGIN_PICTURES_BIG" => "10",
            "MARGIN_PICTURES_SMALL" => "10",
            "NAV_BIG" => "Y",
            "NAV_SMALL" => "Y",
            "NO_PHOTO_PATH" => NO_PHOTO_1170_480,
            "WIDTH" => "1170"
        )
    );
    $this->arParams["SLIDER"] = ob_get_clean();
}
?>
