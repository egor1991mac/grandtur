<?php

/**
 * id iblock cities
 */
define("CITIES_ID_IBLOCK", 5);

/**
 * id iblock countries
 */
define("COUNTRIES_ID_IBLOCK", 10);

/**
 * id iblock hotels
 */
define("HOTELS_ID_IBLOCK", 7);

/**
 * id iblock tours
 */
define("TOURS_ID_IBLOCK", 11);

/**
 * id sea tour type
 */
define("SEA_TOUR_TYPE_ID", 45);

/**
 * Путь к noPhoto
 */
define("NO_PHOTO_PATH_377_262", "/local/templates/travelsoft/img/no_photo/377_262.jpg");
define("NO_PHOTO_PATH_348_323", "/local/templates/travelsoft/img/no_photo/348_323.jpg");
define("NO_PHOTO_PATH_252_190", "/local/templates/travelsoft/img/no_photo/252_190.jpg");

