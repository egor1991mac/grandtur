<?
$interface_include_path = '/local/php_interface/include';

/* Вспомогательные классы */
\Bitrix\Main\Loader::registerAutoLoadClasses(
        null, 
        array(
                "travelsoft\Ajax" => $interface_include_path ."/classes/ajax.php",
            )
    );
