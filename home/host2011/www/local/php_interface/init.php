<?php

//определение дополнительных констант
if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/const.php'))
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/const.php';

//библиотека функций
if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/functions.php'))
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/functions.php';

//подключение и настройка классов
if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/class_includer.php'))
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/class_includer.php';