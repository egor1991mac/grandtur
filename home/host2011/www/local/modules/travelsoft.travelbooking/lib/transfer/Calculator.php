<?php

namespace travelsoft\booking\transfer;

use travelsoft\booking\CalculationTypes;

/**
 * Калькулятор стоимости
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Calculator extends \travelsoft\booking\abstraction\Calculator {

    /**
     * @var array
     */
    protected $_prices_data = null;

    /**
     * @var array
     */
    protected $_tourservices = null;

    /**
     * @var array
     */
    protected $_markup = null;

    /**
     * @var array
     */
    protected $_discount = null;
    
    /**
     * @var array
     */
    protected $_ts_discount = null;

    /**
     * @var \travelsoft\booking\Request 
     */
    protected $_request = null;

    /**
     * @param array $prices_data
     * массив сгруппированный данных по услугам
     * для расчета цен
     * [
     *      service_id => [
     *          rate_id => [
     *              date => [
     *                  "prices" => [
     *                      price_type_id => [
     *                          "gross" => цена брутто
     *                          "netto" => цена нетто
     *                      ]
     *                  ],
     *                  "seating" => [
     *                      "adults" => кол. взрослых,
     *                      "children" => кол. детей
     *                      "children_age" => возраст детей
     *                  ],
     *                  "price_types" => массив данных по типам цен из БД
     *                  "calc_types" => массив данных по типам расчета из БД
     *                  "currency" => валюта
     *              ]
     *          ]
     *      ]
     * ],
     * @param array $tourservices
     * @param array $markup
     * @param array $discount
     * @param array $ts_discount
     * @param \travelsoft\booking\Request $request
     */
    public function __construct(array $prices_data, array $tourservices, array $markup, array $discount, array $ts_discount,\travelsoft\booking\Request $request = null) {
        parent::__construct();
        $this->_prices_data = $prices_data;
        $this->_tourservices = $tourservices;
        $this->_markup = $markup;
        $this->_discount = $discount;
        $this->_ts_discount = $ts_discount;
        $this->_request = $request;
    }

    /**
     * @return $this
     */
    public function calculating() {

        $this->_result = $localResult = $localData = array();
        if (!empty($this->_prices_data)) {

            foreach ($this->_prices_data as $service_id => $arr_services_data) {
                foreach ($arr_services_data as $rate_id => $_data) {
                    foreach ($_data as $date => $data) {
                        $localData = array(
                            "prices" => null,
                            "gross" => null,
                            "netto" => null,
                            "seating" => $data["seating"],
                            "max_age" => null,
                            "min_age" => null
                        );

                        foreach ($data["prices"] as $price_type_id => $arr_prices) {

                            $localData["prices"] = $arr_prices;
                            $localData["min_age"] = $data["price_types"][$price_type_id]["UF_MIN_AGE"];
                            $localData["max_age"] = $data["price_types"][$price_type_id]["UF_MAX_AGE"];
                            $calculation_method = $data["calc_types"][$data["price_types"][$price_type_id]["UF_CALC_TYPE"]];
                            $localData = CalculationTypes::$calculation_method($localData);
                        }

                        if ($localData["gross"] > 0 &&
                                intVal($localData["seating"]["adults"]) === 0 &&
                                intVal($localData["seating"]["children"]) === 0
                        ) {
                            $this->_result[$service_id][$rate_id][$date] = array(
                                "result_price" => $localData["gross"],
                                "gross" => $localData["gross"],
                                "netto" => $localData["netto"] ?: $localData["gross"],
                                "discount_price" => 0.00,
                                "tourservice_for_adults" => 0.00,
                                "tourservice_for_children" => 0.00,
                                "tourservice_currency" => "",
                                "markup_price" => 0.00,
                                "markup_currency" => "",
                                "currency" => $data["currency"],
                                "date" => $date,
                            );
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function min() {


        $arr_min_result = array();

        foreach ($this->_result as $service_id => $arr_data_grouped_by_rates) {
            foreach ($arr_data_grouped_by_rates as $arr_data_grouped_by_dates) {
                $min = exp(10);
                $arr_min = array();
                foreach ($arr_data_grouped_by_dates as $arr_data) {

                    $price = $this->_converter->convert($arr_data["result_price"], $arr_data["currency"]);

                    if ($min > $price) {
                        $arr_min = $arr_data;
                        $min = $price;
                    }
                }
                if (!empty($arr_min)) {
                    $arr_min_result[$service_id] = $arr_min;
                }
            }
        }

        $this->_result = $arr_min_result;
        return $this;
    }

    /**
     * @return $this
     */
    public function addTourservice() {
         $tmp_data = [];
        
        foreach ($this->_result as $service_id => &$arr_data_grouped_by_rates) {

            foreach ($arr_data_grouped_by_rates as $rate_id => &$arr_data_grouped_by_dates) {

                $arr_tourservice = $this->_tourservices[$service_id];

                foreach ($arr_data_grouped_by_dates as $date => $arr_data) {

                    if (!empty($arr_tourservice)) {
                        foreach ($arr_tourservice as $city_id => $arr_ts) {
                            $arr_data["city_id"] = $city_id;
                            $arr_data["tourservice_for_adults"] = $arr_ts["for_adults"] * $arr_ts["adults"];
                            $arr_data["tourservice_for_children"] = $this->_calculateChilrenTouservice($arr_ts, $this->_request);
                            $arr_data["tourservice_currency"] = $arr_ts["currency"];
                            $arr_data["tourservice_discount_price"] = $arr_ts["tourservice_for_adults"] + $arr_ts["tourservice_for_children"];
$arr_data["tourservice_currency"] = $arr_ts["currency"];
                            $toursevice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                            
                            $res_price = $arr_data["result_price"];
                            if ($toursevice_price > 0) {
                                $res_price += $this->_converter->convert($toursevice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                            }
                            $res_data_tmp = $arr_data;
                            $res_data_tmp["result_price"] = $res_price;
                            $tmp_data[$service_id][$rate_id . "~" . $city_id][$date] = $res_data_tmp;
                        }
                    } else {
                        $tmp_data[$service_id][$rate_id][$date] = $arr_data;
                    }
                }
            }
        }
        $this->_result = $tmp_data;
        return $this;
    }

    /**
     * @return $this
     */
    public function addMarkup() {

        foreach ($this->_result as $service_id => &$arr_data_grouped_by_rates) {
            foreach ($arr_data_grouped_by_rates as &$arr_data_grouped_by_dates) {
                foreach ($arr_data_grouped_by_dates as &$arr_data) {

                    if (isset($this->_markup[$service_id])) {

                        $arr_data["markup_price"] = $this->_markup[$service_id]["price"];
                        $arr_data["markup_currency"] = $this->_markup[$service_id]["currency"];

                        if ($arr_data["markup_price"] > 0) {
                            $arr_data["result_price"] += $this->_converter->convert($arr_data["markup_price"], $arr_data["markup_currency"], $arr_data["currency"]);
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function applyDiscount() {
        if (!empty($this->_discount)) {

            if ($this->_discount["UF_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * ($this->_request->adults + $this->_request->children);
                            $arr_data["result_price"] = $arr_data["result_price"] - $this->_discount["UF_VALUE"];
                        }
                    }
                }
            } elseif ($this->_discount["UF_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * $arr_data["gross"] / 100;
                            $arr_data["result_price"] = $arr_data["result_price"] - ($this->_discount["UF_VALUE"] * $arr_data["result_price"] / 100);
                        }
                    }
                }
            }
        }

        return $this;
    }
    
    /**
     * @return $this
     */
    public function applyTSDiscount() {
        if (!empty($this->_ts_discount)) {

            if ($this->_ts_discount["UF_TS_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["tourservice_discount_price"] = ($this->_request->adults + $this->_request->children)*$this->_ts_discount["UF_TS_VALUE"];
                            if ($arr_data["tourservice_discount_price"] > 0 ) {
                                $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                            }
                        }
                    }
                }
            } elseif ($this->_ts_discount["UF_TS_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["tourservice_discount_price"] = $this->_ts_discount["UF_TS_VALUE"] * ($arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"]) / 100;
                            if ($arr_data["tourservice_discount_price"] > 0 ) {
                                $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

}
