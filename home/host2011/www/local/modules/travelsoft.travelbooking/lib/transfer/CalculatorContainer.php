<?php

namespace travelsoft\booking\transfer;

/**
 * Класс контейнер для калькуляторов по проезду
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorContainer extends \travelsoft\booking\abstraction\CalculatorContainer{
        
    /**
     * @param \travelsoft\booking\travel\Calculator $calculator
     * @param string $key
     */
    public function add (\travelsoft\booking\transfer\Calculator $calculator, string $key) {
        $this->_container[$key] = $calculator;
    }
}
