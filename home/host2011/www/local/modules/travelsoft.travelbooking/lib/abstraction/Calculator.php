<?php

namespace travelsoft\booking\abstraction;

/**
 * Интерфейс для класса калькулятор
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
abstract class Calculator {
    
    /**
     * @var array
     */
    protected $_result = array();
    
    /**
     * @var \travelsoft\booking\adapters\CurrencyConverter
     */
    protected $_converter = null;
    
    abstract public function calculating ();
    abstract public function min ();
    abstract public function addTourservice ();
    abstract public function addMarkup ();
    abstract public function applyDiscount ();
    
    public function __construct() {
        $this->_converter = new \travelsoft\booking\adapters\CurrencyConverter;
    }
    
    /**
     * @return array
     */
    public function get () : array {
        return $this->_result;
    }
    
    /**
     * @param array $tourservice
     * @param \travelsoft\booking\Request $request
     * @return float
     */
    protected function _calculateChilrenTouservice(array $tourservice, \travelsoft\booking\Request $request) {

        $tourservice_price = 0.00;

        foreach ($request->children_age as $age) {

            foreach ($tourservice["max_age"] as $k => $max_age) {
                if ($max_age >= $age && $tourservice["min_age"][$k] <= $age) {
                    $tourservice_price += $tourservice["for_children"][$k]; 
                }
            }
        }
        
        return $tourservice_price;
    }
}
