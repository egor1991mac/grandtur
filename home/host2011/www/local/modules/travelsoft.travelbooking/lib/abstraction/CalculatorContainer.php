<?php

namespace travelsoft\booking\abstraction;

/**
 * Класс контейра для калькуляторов
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
abstract class CalculatorContainer {
    
    protected $_container = null;
    
    /**
     * @return array
     */
    public function get () : array {
        return (array)$this->_container;
    }
}
