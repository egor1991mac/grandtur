<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с типами расчета
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TouristsUtils extends Utils {

    public static function getTouristsFilter() {
        if (strlen($_REQUEST['CANCEL']) > 0) {
            \LocalRedirect($GLOBALS['APPLICATION']->GetCurPageParam("", array(
                        'UF_NEED_VISA',
                        'CANCEL'
            )));
        }

        $filter = array();
        if (in_array($_REQUEST['UF_NEED_VISA'], array(0, 1))) {
            $filter['UF_NEED_VISA'] = $_REQUEST['UF_NEED_VISA'];
        }

        return $filter;
    }

    /**
     * Возвращает список полей по туристу
     * @param string $firtsKey
     * @return array
     */
    public static function getTouristFields(string $firtsKey = null): array {

        $arFields = array(
            'MAIN_TOURIST_DATA' => array(
                'UF_USER_ID', 'UF_NAME', 'UF_LAT_NAME', 'UF_LAST_NAME', 'UF_LAT_LAST_NAME', 'UF_SECOND_NAME',
                'UF_PASS_SERIES', 'UF_PASS_NUMBER', 'UF_PASS_ISSUED_BY', 'UF_PASS_DATE_ISSUE',
                'UF_BIRTHDATE', 'UF_MALE'
            )
        );

        if (isset($arFields[$firtsKey])) {

            return $arFields[$firtsKey];
        } else {

            return $arFields;
        }
    }

    /**
     * Возвращает HTML для формы редактирования туриста
     * @param array $data
     * @param type $tabName
     * @return string
     */
    public static function getTouristFieldsContent(array $data = null, $tabName): string {

        $arFields = self::getTouristFields($tabName);

        $arUserFields = array_filter(
                \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::touristsStoreId(), $data, LANGUAGE_ID), function ($arItem) use ($arFields) {
            return in_array($arItem['FIELD_NAME'], $arFields);
        });

        $arRequired = array(
            'UF_NAME', 'UF_LAST_NAME', 'UF_SECOND_NAME',
            'UF_PASS_SERIES', 'UF_PASS_NUMBER', 'UF_BIRTHDATE', 'UF_PASS_DATE_ISSUE', 'UF_PASS_ISSUED_BY');

        $isFormRequest = self::isEditFormRequest();
        $content = "";
        foreach ($arUserFields as $arUserField) {

            if ($arUserField['FIELD_NAME'] == 'UF_USER_ID') {

                if ($data['UF_USER_ID'] > 0) {
                    $content .= self::getEditFieldHtml(
                                    '', '<input type="hidden" value="' . $arUserField["VALUE"] . '" name="' . $arUserField['FIELD_NAME'] . '">', false, true);
                }
                continue;
            }

            if (in_array($arUserField['FIELD_NAME'], $arRequired)) {
                $value = $arUserField['VALUE'];
                if ($_POST[$arUserField['FIELD_NAME']]) {

                    $value = $_POST[$arUserField['FIELD_NAME']];
                }

                $content .= self::getEditFieldHtml(
                                $arUserField['EDIT_FORM_LABEL'], '<input type="text" value="' . $value . '" name="' . $arUserField['FIELD_NAME'] . '">', true);
                continue;
            }
            $arUserField['ENTITY_VALUE_ID'] = 2;
            $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
        }
        
        return $content;
    }

    /**
     * Обработка формы редактирования данных по туристу
     * @param bool $useRedirectIfOk
     * @return array
     */
    public static function processingTouristEditForm(bool $useRedirectIfOk = true): array {

        $url = Settings::TOURISTS_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $data = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::touristsStoreId(), array());

            if (strlen($data['UF_NAME']) <= 0) {

                $arErrors[] = 'Не указано имя туриста';
            }

            if (strlen($data['UF_LAST_NAME']) <= 0) {

                $arErrors[] = 'Не указана фамилия туриста';
            }
            
            if (strlen($data['UF_SECOND_NAME']) <= 0) {

                $arErrors[] = 'Не указано отчество туриста';
            }

            if (strlen($data['UF_PASS_SERIES']) <= 0) {

                $arErrors[] = 'Не указан серия паспорта';
            }

            if (strlen($data['UF_PASS_NUMBER']) <= 0) {

                $arErrors[] = 'Не указан номер паспорта туриста';
            }
            
            if (strlen($data['UF_BIRTHDATE']) <= 0) {

                $arErrors[] = 'Не указана дата рождения туриста';
            }
            
            if (strlen($data['UF_PASS_ISSUED_BY']) <= 0) {

                $arErrors[] = 'Не указано кем выдан паспорт';
            }
            
            if (strlen($data['UF_PASS_DATE_ISSUE']) <= 0) {

                $arErrors[] = 'Не указана дата выдачи паспорта';
            }
            
            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Tourists::update($ID, $data);
                } else {
                    $data["UF_USER_ID"] = $GLOBALS["USER"]->GetID();
                    $result = \travelsoft\booking\stores\Tourists::add($data);
                }

                if ($result && $useRedirectIfOk) {

                    LocalRedirect($url);
                }
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }
}
