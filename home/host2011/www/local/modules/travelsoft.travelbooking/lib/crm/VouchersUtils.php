<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с путевками
 *
 * @author dimabresky
 */
class VouchersUtils extends Utils {
    
    public static function getFilter() {
        if (strlen($_REQUEST['CANCEL']) > 0) {
            \LocalRedirect($GLOBALS['APPLICATION']->GetCurPageParam("", array(
                        'TOUR_ID',
                        'CLIENT',
                        'AGENT',
                        'MANAGER',
                        'DATE_CREATE',
                        'DATE_FROM',
                        'CANCEL'
            )));
        }

        $filter = array();
        if (!empty(@$_GET['TOUR_ID'])) {
            $parts = \explode("_", $_GET['TOUR_ID']);
            $service_type = $parts[0];
            $service_id = $parts[1];
            $bookings_id_by_tours = \array_keys(\travelsoft\booking\stores\Bookings::get(['filter' => ['UF_SERVICE' => $service_id, "UF_SERVICE_TYPE" => $service_type]]));

            if (empty($bookings_id_by_tours)) {
                $bookings_id_by_tours = [-1];
            }
        }
        
        if (strlen(@$_GET['DATE_FROM']) > 0) {
            
            $bookings_id_by_date_from = \array_keys(\travelsoft\booking\stores\Bookings::get(['filter' => ['UF_DATE_FROM' => \travelsoft\booking\adapters\Date::create($_GET['DATE_FROM'])]]));
            if (empty($bookings_id_by_date_from)) {
                $bookings_id_by_date_from = [-1];
            }
        }
        
        if (isset($bookings_id_by_date_from)) {
            $filter['ID'] = $bookings_id_by_date_from;
        }
        
        if (isset($bookings_id_by_tours)) {
            if (!isset($filter['ID'])) {
                $filter['ID'] = $bookings_id_by_tours;
            } else {
                $filter['ID'] = \array_merge($bookings_id_by_tours, $filter['ID']);
            }
            
        }
        
        if (@$_GET['CLIENT'] > 0) {
            @$filter['UF_CLIENT'][] = $_GET['CLIENT'];
        }
        
        if (@$_GET['AGENT'] > 0) {
            @$filter['UF_CLIENT'][] = $_GET['AGENT'];
        }
        
        if (strlen(@$_GET['DATE_CREATE']) > 0) {
            $filter['><UF_DATE_CREATE'] = [
                \travelsoft\booking\adapters\Date::createFromTimestamp(\strtotime($_GET['DATE_CREATE'] . " 00:00:00")),
                \travelsoft\booking\adapters\Date::createFromTimestamp(\strtotime($_GET['DATE_CREATE'] . " 23:59:59"))
            ];
        }
                

        return $filter;
    }
    
    /**
     * Подготовка строки таблицы путевок
     * @param \CAdminListRow $row
     */
    public static function prepareRowForVochersTable(\CAdminListRow &$row, array $arVoucher) {

        if ($arVoucher["UF_CLIENT"] > 0) {
            $arUser = current(\travelsoft\booking\stores\Users::get(array("filter" => array("ID" => $arVoucher["UF_CLIENT"]), "select" => array(
                            "ID", "NAME", "LAST_NAME", "SECOND_NAME", "UF_LEGAL_NAME", "EMAIL", "PERSONAL_PHONE"))));
            $name = "";

            if ($arUser["ID"] > 0) {

                if (\travelsoft\booking\adapters\User::isAgentById($arVoucher["UF_CLIENT"])) {
                    $name = $arUser["UF_LEGAL_NAME"] . "(" . $arUser["PERSONAL_PHONE"] . ")";
                } else {
                    $name = $arUser["FULL_NAME_WITH_EMAIL"] . "(" . $arUser["PERSONAL_PHONE"] . ")";
                }

                $row->AddViewField("UF_СLIENT", $name);
            }
        }

        if ($arVoucher["UF_MANAGER"] > 0) {

            $arUser = current(\travelsoft\booking\stores\Users::get(array("filter" => array("ID" => $arVoucher["UF_MANAGER"]), "select" => array(
                            "ID", "NAME", "LAST_NAME", "SECOND_NAME", "EMAIL"))));
            $name = "";
            if ($arUser["ID"] > 0) {

                $row->AddViewField("UF_MANAGER", $arUser["FULL_NAME_WITH_EMAIL"]);
            }
        }
        
        $booking = current($arVoucher['BOOKINGS_DETAIL']);
        if ($booking['UF_SERVICE_NAME']) {
            $row->AddViewField("NAME", $booking['UF_SERVICE_NAME']);
        }
        
        if ($booking['UF_DATE_FROM']) {
            $row->AddViewField("UF_DATE_FROM", $booking['UF_DATE_FROM']->toString());
        }
        
        $newVoucherLabel = '';
        if ($arVoucher['UF_STATUS'] == \travelsoft\booking\Settings::defStatus()) {

            $newVoucherLabel = '<span class="new-order-label">new</span> ';
        }

        $row->AddViewField("ID", $newVoucherLabel . '<a target="__blank" href="' . Settings::VOUCHER_EDIT_URL . '?ID=' . $arVoucher['ID'] . '&lang=' . LANG . '">' . $arVoucher["ID"] . '</a>');

        $row->AddActions(array(
            array(
                "ICON" => "edit",
                "DEFAULT" => true,
                "TEXT" => "Изменить",
                "ACTION" => 'BX.adminPanel.Redirect([], "' . Settings::BOOKING_EDIT_URL . '?ID=' . $arVoucher["ID"] . '", event);'
            ),
            array(
                "ICON" => "delete",
                "DEFAULT" => true,
                "TEXT" => "Удалить",
                "ACTION" => "if(confirm('Действительно хотите удалить бронь')) " . Settings::VOUCHERS_LIST_URL . ".GetAdminList('/bitrix/admin/" . Settings::VOUCHERS_LIST_URL . "?ID=" . $arVoucher['ID'] . "&action_button=delete&lang=" . LANGUAGE_ID . "&sessid=" . bitrix_sessid() . "');"
            )
        ));
    }

    /**
     * Обработки формы добавления/редактирования путевки
     * @return array
     */
    public static function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::VOUCHERS_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arBookingSave = array();

            if (Validator::numericNotEmpty((int) $_POST["UF_COST"], "Стоимость", $arErrors)) {
                $arBookingSave["UF_COST"] = $_POST["UF_COST"];
                $arBookingSave["UF_CURRENCY"] = $_POST["UF_CURRENCY"];
            }

            $arBookingSave["UF_NETTO"] = $_POST["UF_NETTO"];
            $arBookingSave["UF_DISCOUNT"] = $_POST["UF_DISCOUNT"];
            $arBookingSave["UF_TS_DISCOUNT"] = $_POST["UF_TS_DISCOUNT"];
            $arBookingSave["UF_TS_PRICE"] = $_POST["UF_TS_PRICE"];
            $arBookingSave["UF_TS_CURRENCY"] = $_POST["UF_TS_CURRENCY"];
            $arBookingSave["UF_MARKUP_PRICE"] = $_POST["UF_MARKUP_PRICE"];
            $arBookingSave["UF_MARKUP_CURRENCY"] = $_POST["UF_MARKUP_CURRENCY"];
            $arBookingSave["UF_FOOD"] = $_POST["UF_FOOD"];

            $arVoucherSave = array();

            if (Validator::numericNotEmpty((int) $_POST["UF_CLIENT"], "Клиент", $arErrors)) {
                $arVoucherSave["UF_CLIENT"] = $_POST["UF_CLIENT"];
            }

            if (Validator::numericNotEmpty((int) $_POST["UF_STATUS"], "Статус", $arErrors)) {
                $arVoucherSave["UF_STATUS"] = $_POST["UF_STATUS"];
            }

            $arVoucherSave["UF_DATE_DOCS"] = $_POST["UF_DATE_DOCS"];
            $arVoucherSave["UF_DATE_EMBASSY"] = $_POST["UF_DATE_EMBASSY"];
            $arVoucherSave["UF_MANAGER"] = $_POST["UF_MANAGER"];
            $arVoucherSave["UF_COMMENT"] = $_POST["UF_COMMENT"];
            $arVoucherSave["UF_FILES"] = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::vouchersStoreId(), array())['UF_FILES'];

            if ($_REQUEST['ID'] > 0) {

                if (empty($arErrors)) {
                    $ID = intVal($_REQUEST['ID']);
                    $bid = current(\travelsoft\booking\stores\Vouchers::get(array("filter" => array("ID" => $ID), "select" => array("UF_BOOKINGS", "ID"))))["UF_BOOKINGS"][0];

                    \travelsoft\booking\stores\Bookings::update($bid, $arBookingSave);
                    $result = \travelsoft\booking\stores\Vouchers::update($ID, $arVoucherSave);
                }
            } else {

                if (Validator::strIsEmpty((string) $_POST["UF_SERVICE_TYPE"], "Тип услуги", $arErrors)) {
                    $arBookingSave["UF_SERVICE_TYPE"] = $_POST["UF_SERVICE_TYPE"];

                    if (Validator::numericNotEmpty((int) $_POST["UF_SERVICE"], "Услуга", $arErrors)) {
                        $arBookingSave["UF_SERVICE"] = $_POST["UF_SERVICE"];
                        if ($arBookingSave["UF_SERVICE_TYPE"] !== "excursiontour" && !Validator::isTransferServiceType($arBookingSave["UF_SERVICE_TYPE"]) && Validator::numericNotEmpty((int) $_POST["UF_ROOM"], "Номер", $arErrors)) {
                            $arBookingSave["UF_ROOM"] = $_POST["UF_ROOM"];
                        }
                    }

                    if (!Validator::isTransferServiceType($arBookingSave["UF_SERVICE_TYPE"]) && $arBookingSave["UF_SERVICE_TYPE"] !== "excursiontour") {

                        if (strlen($_POST["UF_DATE_FROM_PLACE"]) > 0) {
                            $arBookingSave["UF_DATE_FROM_PLACE"] = $_POST["UF_DATE_FROM_PLACE"];
                        } else {
                            $arErrors[] = "Нет данных по дате заселения";
                        }

                        if (strlen($_POST["UF_DATE_TO_PLACE"]) > 0) {
                            $arBookingSave["UF_DATE_TO_PLACE"] = $_POST["UF_DATE_TO_PLACE"];
                        } else {
                            $arErrors[] = "Нет данных по дате выселения";
                        }

                        if ($_POST["UF_PLACEMENT_RATE"] > 0) {
                            $arBookingSave["UF_PLACEMENT_RATE"] = $_POST["UF_PLACEMENT_RATE"];
                        } else {
                            $arErrors[] = "Нет данных по тарифу размещения.";
                        }

                        if (Validator::checkDate((string) $_POST["UF_DATE_TO"], "Дата по", $arErrors)) {
                            $arBookingSave["UF_DATE_TO"] = $_POST["UF_DATE_TO"];
                        }
                    }

                    if ($_POST["UF_SERVICE_TYPE"] === "packagetour") {

                        if ($_POST["UF_PLACEMENT"] > 0) {

                            $arBookingSave["UF_PLACEMENT"] = $_POST["UF_PLACEMENT"];
                        } else {
                            $arErrors[] = "Нет данных по размещению.";
                        }

                        if ($_POST["UF_TRANSFER_BACK"] <= 0 || $_POST["UF_TRANSFER"] <= 0) {
                            $arErrors[] = "Нет данных по проезду.";
                        }

                        if ($_POST["UF_TRANSFER"] > 0) {
                            $arBookingSave["UF_TRANSFER"] = $_POST["UF_TRANSFER"];
                            if (strlen($_POST["UF_DATE_FROM_TRANS"]) > 0) {
                                $arBookingSave["UF_DATE_FROM_TRANS"] = $_POST["UF_DATE_FROM_TRANS"];
                            } else {
                                $arErrors[] = "Нет данных по дате выезда.";
                            }
                        }

                        if ($_POST["UF_TRANS_RATE"] > 0) {
                            $arBookingSave["UF_TRANS_RATE"] = $_POST["UF_TRANS_RATE"];
                        }

                        if ($_POST["UF_TRANSFER_BACK"] > 0) {
                            $arBookingSave["UF_TRANSFER_BACK"] = $_POST["UF_TRANSFER_BACK"];
                            if (strlen($_POST["UF_DATE_BACK_TRANS"]) > 0) {
                                $arBookingSave["UF_DATE_BACK_TRANS"] = $_POST["UF_DATE_BACK_TRANS"];
                            } else {
                                $arErrors[] = "Нет данных по дате выезда обратно.";
                            }
                        }

                        if ($_POST["UF_TRANS_BACK_RATE"] > 0) {
                            $arBookingSave["UF_TRANS_BACK_RATE"] = $_POST["UF_TRANS_BACK_RATE"];
                        }

                        if ($_POST["UF_TRANS_BACK_RATE"] <= 0 || $_POST["UF_TRANS_RATE"] <= 0) {
                            $arErrors[] = "Нет данных по тарифу проезда.";
                        }

                        if (Validator::numericNotEmpty((int) $_POST["UF_SERVICE"], "Услуга", $arErrors)) {
                            $arBookingSave["UF_SERVICE"] = $_POST["UF_SERVICE"];
                            if (Validator::numericNotEmpty((int) $_POST["UF_ROOM"], "Номер", $arErrors)) {
                                $arBookingSave["UF_ROOM"] = $_POST["UF_ROOM"];
                            }
                        }
                    }

                    if (Validator::isTransferServiceType($_POST["UF_SERVICE_TYPE"])) {
                        if ($_POST["UF_TRANS_RATE"] > 0) {
                            $arBookingSave["UF_TRANS_RATE"] = $_POST["UF_TRANS_RATE"];
                        } else {
                            $arErrors[] = "Нет данных по тарифу проезда.";
                        }
                    }

                    if ($_POST["UF_SERVICE_TYPE"] === "excursiontour") {
                        if ($_POST["UF_EXCUR_TOUR_RATE"] > 0) {
                            $arBookingSave["UF_EXCUR_TOUR_RATE"] = $_POST["UF_EXCUR_TOUR_RATE"];
                        } else {
                            $arErrors[] = "Нет данных по тарифу экскурсионного тура.";
                        }
                    }
                }

                if (Validator::numericNotEmpty((int) $_POST["UF_ADULTS"], "Кол-во взрослых", $arErrors)) {
                    $arBookingSave["UF_ADULTS"] = (int) $_POST["UF_ADULTS"];
                }

                if (Validator::numericMoreOrEqualZero((int) $_POST["UF_CHILDREN"], "Кол-во детей", $arErrors)) {
                    $arBookingSave["UF_CHILDREN"] = (int) $_POST["UF_CHILDREN"];
                    if ($arBookingSave["UF_CHILDREN"] > 0 && Validator::arrayIsEmpty((array) $_POST["UF_CHILDREN_AGE"], "Возраст детей", $arErrors)) {
                        if (count($_POST["UF_CHILDREN_AGE"]) !== $arBookingSave["UF_CHILDREN"]) {
                            $arErrors[] = "Указанное количество возрастов не соответствует указанному количеству детей";
                        } else {
                            $arBookingSave["UF_CHILDREN_AGE"] = $_POST["UF_CHILDREN_AGE"];
                        }
                    }
                }
                $arBookingSave["UF_TOURISTS"] = array_filter((array) $_POST["UF_TOURISTS"], function ($it) {
                    return $it > 0;
                });
                if ($arBookingSave["UF_ADULTS"] + $arBookingSave["UF_CHILDREN"] !== count($arBookingSave["UF_TOURISTS"])) {

                    $arErrors[] = "Количество туристов не соответствует указанному количеству взрослых и детей";
                }

                if (Validator::checkDate((string) $_POST["UF_DATE_FROM"], "Дата с", $arErrors)) {
                    $arBookingSave["UF_DATE_FROM"] = $_POST["UF_DATE_FROM"];
                }

                if (empty($arErrors)) {

                    $arr_service_name = array();
                    if ($arBookingSave["UF_SERVICE_TYPE"] === "transfer") {
                        $arBookingSave["UF_TRANSFER"] = $arBookingSave["UF_SERVICE"];
                        $arBookingSave["UF_DATE_FROM_TRANS"] = $arBookingSave["UF_DATE_FROM"];
                        $arr_service_name[] = \travelsoft\booking\stores\Buses::nameById((int) $arBookingSave["UF_TRANSFER"]) . "[" . \travelsoft\booking\stores\Rates::nameById($arBookingSave["UF_TRANS_RATE"]) . "]";
                    } elseif ($arBookingSave["UF_SERVICE_TYPE"] === "transferback") {
                        $arBookingSave["UF_TRANSFER_BACK"] = $arBookingSave["UF_SERVICE"];
                        $arBookingSave["UF_DATE_BACK_TRANS"] = $arBookingSave["UF_DATE_FROM"];
                        $arr_service_name[] = \travelsoft\booking\stores\Buses::nameById((int) $arBookingSave["UF_DATE_BACK_TRANS"]) . "[" . \travelsoft\booking\stores\Rates::nameById($arBookingSave["UF_TRANS_RATE"]) . "]";
                    } elseif ($arBookingSave["UF_SERVICE_TYPE"] === "excursiontour") {
                        $arr_service_name[] = \travelsoft\booking\stores\ExcursionTour::nameById((int) $arBookingSave["UF_SERVICE"]);
                    } else {

                        if ($arBookingSave["UF_TRANSFER"] > 0) {
                            $arr_service_name[] = \travelsoft\booking\stores\Buses::nameById((int) $arBookingSave["UF_TRANSFER"]);
                        }
                        if ($arBookingSave["UF_PLACEMENT"] > 0) {
                            $arr_service_name[] = \travelsoft\booking\stores\Placements::nameById((int) $arBookingSave["UF_PLACEMENT"]) . "[" . \travelsoft\booking\stores\Rooms::nameById((int) $arBookingSave["UF_ROOM"]) . "]";
                        } else {

                            $arr_service_name[] = \travelsoft\booking\stores\Buses::nameById((int) $arBookingSave["UF_SERVICE"]) . "[" . \travelsoft\booking\stores\Rooms::nameById((int) $arBookingSave["UF_ROOM"]) . "]";
                        }
                    }
                    $arBookingSave["UF_SERVICE_NAME"] = implode(" + ", $arr_service_name);
                    $bid = \travelsoft\booking\stores\Bookings::add($arBookingSave);
                    if ($bid > 0) {
                        $arVoucherSave["UF_BOOKINGS"] = array($bid);
                        $arVoucherSave["UF_DATE_CREATE"] = date(\travelsoft\booking\Settings::DATE_FORMAT . ' H:i:s');
                        $result = \travelsoft\booking\stores\Vouchers::add($arVoucherSave);
                    } else {
                        $arErrors[] = "Возникла ошибка при попытке сохранения. Повторите попытку позже.";
                    }
                }
            }


            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $IS_NEW = $_REQUEST["ID"] <= 0;

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::vouchersStoreId(), $arData);

        $content = "";

        if (!$IS_NEW && $arUserFields["UF_STATUS"]["VALUE"] == \travelsoft\booking\Settings::cancellationStatus()) {

            if (!empty($arUserFields["UF_BOOKINGS"]["VALUE"])) {
                // just for one service
                $arr_booking = \travelsoft\booking\stores\Bookings::getById($arUserFields["UF_BOOKINGS"]["VALUE"][0]);
            }

            $content .= self::getEditFieldHtml("Создана:", $arUserFields["UF_DATE_CREATE"]["VALUE"]);

            $content .= self::getEditFieldHtml("Тип услуги:", \travelsoft\booking\Settings::getServicesTypesWithout(array("rooms"))[$arr_booking["UF_SERVICE_TYPE"]]["name"]);

            $content .= self::getEditFieldHtml("Услуга:", $arr_booking["UF_SERVICE_NAME"], false);

            $content .= self::getEditFieldHtml("Дата с:", $arr_booking["UF_DATE_FROM"]->toString(), false, false);

            if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"])) {
                $content .= self::getEditFieldHtml("Дата по:", $arr_booking["UF_DATE_TO"]->toString(), false, false);
            }


            $content .= self::getEditFieldHtml("Кол-во взрослых:", (int) $arr_booking["UF_ADULTS"], false, false);

            $content .= self::getEditFieldHtml("Кол-во детей:", (int) $arr_booking["UF_CHILDREN"], false, false);

            if ($arr_booking["UF_CHILDREN"] > 0) {

                $content .= self::getEditFieldHtml("Возраст детей:", (string) implode(", ", $arr_booking["UF_CHILDREN_AGE"]), false, false);
            } else {

                $content .= self::getEditFieldHtml("Возраст детей:", "0", false, false);
            }

            $content .= self::getEditFieldHtml("Полная стоимость:", $arr_booking["TOTAL_COST_AC_FORMATTED"]);

            $content .= self::getEditFieldHtml("Цена брутто:", $arr_booking["UF_COST"] . " " . $arr_booking["UF_CURRENCY"], false);

            $content .= self::getEditFieldHtml('Цена нетто:', $arr_booking["UF_NETTO"] . " " . $arr_booking["UF_CURRENCY"]);

            $content .= self::getEditFieldHtml('Стоимость транспортных расходов:', $arr_booking["UF_TS_PRICE"] . " " . $arr_booking["UF_TS_CURRENCY"], false);

            $content .= self::getEditFieldHtml('Наценка:', $arr_booking["UF_MARKUP_PRICE"] . " " . $arr_booking["UF_MARKUP_CURRENCY"], false);

            $content .= self::getEditFieldHtml('Комиссия турпродукта (в валюте цены брутто):', $arr_booking["UF_DISCOUNT"], false);
            
            $content .= self::getEditFieldHtml('Комиссия транспортных расходов (в валюте транспортных расходов):', $arr_booking["UF_TS_DISCOUNT"], false);
            
            if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"])) {
                $content .= self::getEditFieldHtml('Питание:', $arr_booking["UF_FOOD"] ?: '-', false);
            }


            $arr_torists_names = array();
            foreach (\travelsoft\booking\stores\Tourists::get(array(
                "filter" => array("ID" => $arr_booking["UF_TOURISTS"]),
                "select" => array("ID", "UF_NAME", "UF_LAST_NAME", "UF_SECOND_NAME")
            )) as $arr_tourist) {

                $arr_torists_names[] = $arr_tourist["FULL_NAME"];
            }

            $content .= self::getEditFieldHtml('Туристы:', implode(', ', $arr_torists_names), false);

            $arr_users = \travelsoft\booking\stores\Users::get(array("ID" => array($arUserFields["UF_CLIENT"]["VALUE"], $arUserFields["UF_MANAGER"]["VALUE"])));

            $content .= self::getEditFieldHtml('Клиент или Агент:', $arr_users[$arUserFields["UF_CLIENT"]["VALUE"]]["FULL_NAME_WITH_EMAIL"] . '&nbsp; <a href="javascript: CRMUtils.editClient(' . $arUserFields["UF_CLIENT"]["VALUE"] . ')">редактировать</a>');

            $content .= self::getEditFieldHtml('Менеджер:', $arr_users[$arUserFields["UF_MANAGER"]["VALUE"]]["FULL_NAME_WITH_EMAIL"] ?: '-');

            $arr_status = \travelsoft\booking\stores\Statuses::getById($arUserFields["UF_STATUS"]["VALUE"]);

            $content .= self::getEditFieldHtml('Статус:', $arr_status["UF_NAME"]);

            $content .= self::getEditFieldHtml('Комментарий:', $arUserFields["UF_COMMENT"]["VALUE"]);

            $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml(false, $arUserFields["UF_FILES"]);

            return $content;
        }

        foreach ($arUserFields as $arUserField) {

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            switch ($arUserField["FIELD_NAME"]) {

                case "UF_DATE_CREATE":
                    if ($arUserField["VALUE"]) {
                        $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ':', $arUserField["VALUE"]);
                    }
                    break;

                case "UF_BOOKINGS":

                    if (!empty($arUserFields["UF_BOOKINGS"]["VALUE"])) {
                        // just for one service
                        $arr_booking = \travelsoft\booking\stores\Bookings::getById($arUserFields["UF_BOOKINGS"]["VALUE"][0]);
                    }

                    if (strlen($_POST["UF_SERVICE_TYPE"]) > 0) {
                        $arr_booking["UF_SERVICE_TYPE"] = $_POST["UF_SERVICE_TYPE"];

                        if (strlen($_POST["UF_DATE_FROM_PLACE"]) > 0) {
                            $arr_booking["UF_DATE_FROM_PLACE"] = $_POST["UF_DATE_FROM_PLACE"];
                        }

                        if (strlen($_POST["UF_DATE_TO_PLACE"]) > 0) {
                            $arr_booking["UF_DATE_TO_PLACE"] = $_POST["UF_DATE_TO_PLACE"];
                        }

                        if ($_POST["UF_ROOM"] > 0) {
                            $arr_booking["UF_ROOM"] = $_POST["UF_ROOM"];
                        }

                        if ($arr_booking["UF_SERVICE_TYPE"] === "packagetour") {

                            if ($_POST["UF_PLACEMENT"] > 0) {
                                $arr_booking["UF_PLACEMENT"] = $_POST["UF_PLACEMENT"];
                            }

                            if ($_POST["UF_PLACEMENT_RATE"] > 0) {
                                $arr_booking["UF_PLACEMENT_RATE"] = $_POST["UF_PLACEMENT_RATE"];
                            }

                            if ($_POST["UF_TRANSFER"] > 0) {
                                $arr_booking["UF_TRANSFER"] = $_POST["UF_TRANSFER"];
                            }

                            if (strlen($_POST["UF_DATE_FROM_TRANS"]) > 0) {
                                $arr_booking["UF_DATE_FROM_TRANS"] = $_POST["UF_DATE_FROM_TRANS"];
                            }

                            if ($_POST["UF_TRANSFER_BACK"] > 0) {
                                $arr_booking["UF_TRANSFER_BACK"] = $_POST["UF_TRANSFER_BACK"];
                            }

                            if ($_POST["UF_TRANS_BACK_RATE"] > 0) {
                                $arr_booking["UF_TRANS_BACK_RATE"] = $_POST["UF_TRANS_BACK_RATE"];
                            }

                            if (strlen($_POST["UF_DATE_BACK_TRANS"]) > 0) {
                                $arr_booking["UF_DATE_BACK_TRANS"] = $_POST["UF_DATE_BACK_TRANS"];
                            }
                        }
                    } elseif (!isset($arr_booking["UF_SERVICE_TYPE"])) {
                        $arr_booking["UF_SERVICE_TYPE"] = "";
                    }

                    if (Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"])) {
                        if ($_POST["UF_TRANS_RATE"] > 0) {
                            $arr_booking["UF_TRANS_RATE"] = $_POST["UF_TRANS_RATE"];
                        }
                    }

                    if ($arr_booking["UF_SERVICE_TYPE"] === "excursiontour") {
                        if ($_POST["UF_EXCUR_TOUR_RATE"] > 0) {
                            $arr_booking["UF_EXCUR_TOUR_RATE"] = $_POST["UF_EXCUR_TOUR_RATE"];
                        }
                    }

                    if ($_POST["UF_SERVICE"] > 0) {
                        $arr_booking["UF_SERVICE"] = (int) $_POST["UF_SERVICE"];
                    } elseif (!isset($arr_booking["UF_SERVICE"])) {
                        $arr_booking["UF_SERVICE"] = "";
                    }

                    if ($_POST["UF_ADULTS"] > 0) {
                        $arr_booking["UF_ADULTS"] = $_POST["UF_ADULTS"];
                    } elseif (!isset($arr_booking["UF_ADULTS"])) {
                        $arr_booking["UF_ADULTS"] = "";
                    }

                    if ($_POST["UF_CHILDREN"] > 0) {
                        $arr_booking["UF_CHILDREN"] = $_POST["UF_CHILDREN"];
                    } elseif (!isset($arr_booking["UF_CHILDREN"])) {
                        $arr_booking["UF_CHILDREN"] = "";
                    }

                    if (is_array($_POST["UF_CHILDREN_AGE"]) && !empty($_POST["UF_CHILDREN_AGE"])) {
                        $arr_booking["UF_CHILDREN_AGE"] = $_POST["UF_CHILDREN_AGE"];
                    } elseif (!isset($arr_booking["UF_CHILDREN_AGE"])) {
                        $arr_booking["UF_CHILDREN_AGE"] = array();
                    }

                    if (strlen($_POST["UF_DATE_FROM"]) > 0) {
                        $arr_booking["UF_DATE_FROM"] = $_POST["UF_DATE_FROM"];
                    } elseif (!isset($arr_booking["UF_DATE_FROM"])) {
                        $arr_booking["UF_DATE_FROM"] = "";
                    }


                    if (strlen($_POST["UF_DATE_TO"]) > 0) {
                        $arr_booking["UF_DATE_TO"] = $_POST["UF_DATE_TO"];
                    } elseif (!isset($arr_booking["UF_DATE_TO"])) {
                        $arr_booking["UF_DATE_TO"] = "";
                    }


                    if ($_POST["UF_COST"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_COST"] = $_POST["UF_COST"];
                    } elseif (!isset($arr_booking["UF_COST"])) {
                        $arr_booking["UF_COST"] = "";
                    }
                    if (strlen($_POST["UF_CURRENCY"]) > 0) {
                        $arr_booking["UF_CURRENCY"] = $_POST["UF_CURRENCY"];
                    } elseif (!isset($arr_booking["UF_CURRENCY"])) {
                        $arr_booking["UF_CURRENCY"] = "";
                    }


                    if ($_POST["UF_NETTO"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_NETTO"] = $_POST["UF_NETTO"];
                    } elseif (!isset($arr_booking["UF_NETTO"])) {
                        $arr_booking["UF_NETTO"] = "";
                    }


                    if ($_POST["UF_TS_PRICE"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_TS_PRICE"] = $_POST["UF_TS_PRICE"];
                    } elseif (!isset($arr_booking["UF_TS_PRICE"])) {
                        $arr_booking["UF_TS_PRICE"] = "";
                    }
                    if (strlen($_POST["UF_TS_CURRENCY"]) > 0) {
                        $arr_booking["UF_TS_CURRENCY"] = $_POST["UF_TS_CURRENCY"];
                    } elseif (!isset($arr_booking["UF_TS_CURRENCY"])) {
                        $arr_booking["UF_TS_CURRENCY"] = "";
                    }


                    if ($_POST["UF_MARKUP_PRICE"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_MARKUP_PRICE"] = $_POST["UF_MARKUP_PRICE"];
                    } elseif (!isset($arr_booking["UF_MARKUP_PRICE"])) {
                        $arr_booking["UF_MARKUP_PRICE"] = "";
                    }
                    if (strlen($_POST["UF_MARKUP_CURRENCY"]) > 0) {
                        $arr_booking["UF_MARKUP_CURRENCY"] = $_POST["UF_MARKUP_CURRENCY"];
                    } elseif (!isset($arr_booking["UF_MARKUP_CURRENCY"])) {
                        $arr_booking["UF_MARKUP_CURRENCY"] = "";
                    }


                    if ($_POST["UF_DISCOUNT"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_DISCOUNT"] = $_POST["UF_DISCOUNT"];
                    } elseif (!isset($arr_booking["UF_DISCOUNT"])) {
                        $arr_booking["UF_DISCOUNT"] = "";
                    }
                    
                    if ($_POST["UF_TS_DISCOUNT"] > \travelsoft\booking\Settings::FLOAT_NULL) {
                        $arr_booking["UF_TS_DISCOUNT"] = $_POST["UF_TS_DISCOUNT"];
                    } elseif (!isset($arr_booking["UF_TS_DISCOUNT"])) {
                        $arr_booking["UF_TS_DISCOUNT"] = "";
                    }

                    if (is_array($_POST["UF_TOURISTS"]) && !empty($_POST["UF_TOURISTS"])) {
                        sort($_POST["UF_TOURISTS"]);
                        $arr_booking["UF_TOURISTS"] = $_POST["UF_TOURISTS"];
                    } elseif (!isset($arr_booking["UF_TOURISTS"])) {
                        $arr_booking["UF_TOURISTS"] = array();
                    }

                    if (isset($_POST["UF_FOOD"]) && strlen($_POST["UF_FOOD"]) > 0) {
                        $arr_booking["UF_FOOD"] = $_POST["UF_FOOD"];
                    }

                    if ($arr_booking["ID"] > 0) {

                        $content .= self::getEditFieldHtml("Тип услуги:", \travelsoft\booking\Settings::getServicesTypesWithout(array("rooms"))[$arr_booking["UF_SERVICE_TYPE"]]["name"]);

                        $content .= self::getEditFieldHtml("Услуга:", $arr_booking["UF_SERVICE_NAME"], false);

                        $content .= self::getEditFieldHtml("Дата с:", $arr_booking["UF_DATE_FROM"]->toString(), false, false);

                        if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"]) && $arr_booking["UF_SERVICE_TYPE"] !== "excursiontour") {
                            $content .= self::getEditFieldHtml("Дата по:", $arr_booking["UF_DATE_TO"]->toString(), false, false);
                        }

                        $content .= self::getEditFieldHtml("Кол-во взрослых:", (int) $arr_booking["UF_ADULTS"], false, false);

                        $content .= self::getEditFieldHtml("Кол-во детей:", (int) $arr_booking["UF_CHILDREN"], false, false);

                        if ($arr_booking["UF_CHILDREN"] > 0) {

                            $content .= self::getEditFieldHtml("Возраст детей:", (string) implode(", ", $arr_booking["UF_CHILDREN_AGE"]), false, false);
                        } else {
                            $content .= self::getEditFieldHtml("Возраст детей:", "0", false, false);
                        }

                        $content .= self::getEditFieldHtml("Полная стоимость:", "<b>" . $arr_booking["TOTAL_COST_AC_FORMATTED"] . "</b>");

                        $content .= self::getEditFieldHtml("", '<a href="javascript:void(0)" id="calculation">Расчитать предварительную стоимость</a>', false, $arr_booking["ID"] > 0) . self::getEditFieldHtml(
                                        'Цена брутто:', '<input name="UF_COST" value="' . $arr_booking["UF_COST"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_CURRENCY', $arr_booking['UF_CURRENCY']), true);
                        $content .= self::getEditFieldHtml(
                                        'Цена нетто:', '<input name="UF_NETTO" value="' . $arr_booking["UF_NETTO"] . '" type="text">');
                        $content .= self::getEditFieldHtml(
                                        'Стоимость транспортных расходов:', '<input name="UF_TS_PRICE" value="' . $arr_booking["UF_TS_PRICE"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_TS_CURRENCY', $arr_booking['UF_TS_CURRENCY']), false);
                        $content .= self::getEditFieldHtml(
                                        'Наценка:', '<input name="UF_MARKUP_PRICE" value="' . $arr_booking["UF_MARKUP_PRICE"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_MARKUP_CURRENCY', $arr_booking['UF_MARKUP_CURRENCY']), false);
                        $content .= self::getEditFieldHtml(
                                        'Комиссия тупродукта (в валюте цены брутто):', '<input name="UF_DISCOUNT" value="' . $arr_booking["UF_DISCOUNT"] . '" type="text">', false);
                        $content .= self::getEditFieldHtml(
                                        'Комиссия транспортных расходов (в валюте транспортных расходов):', '<input name="UF_TS_DISCOUNT" value="' . $arr_booking["UF_TS_DISCOUNT"] . '" type="text">', false);

                        if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"]) && $arr_booking["UF_SERVICE_TYPE"] !== "excursiontour") {
                            $content .= self::getEditFieldHtml(
                                            'Питание:', '<input id="UF_FOOD" name="UF_FOOD" value="' . $arr_booking["UF_FOOD"] . '" type="text">', false);
                        }


                        $table_tourists = '<table id="tourists-add-table"><tbody>';

                        $arr_tourists = \travelsoft\booking\stores\Tourists::get(
                                        array(
                                            "select" => array("ID", "UF_NAME", "UF_LAST_NAME", "UF_SECOND_NAME")
                        ));

                        $select = "";
                        if (!empty($arr_booking["UF_TOURISTS"])) {
                            $cnt = 0;
                            foreach ($arr_booking["UF_TOURISTS"] as $tid) {
                                $options = '<option value="">...</option>';
                                foreach ($arr_tourists as $arTourist) {
                                    $selected = "";
                                    if ($tid == $arTourist["ID"]) {
                                        $selected = 'selected=""';
                                    }
                                    $options .= '<option ' . $selected . ' value="' . $arTourist["ID"] . '">' . $arTourist["FULL_NAME"] . '</option>';
                                }
                                $cnt++;
                                $select .= '<tr><td><select id="tourist-select-' . $cnt . '" style="width:180px;" class="select2" name="' . 'UF_TOURISTS[]">' . $options . '</select> &nbsp; <a target="_blank" href="/bitrix/admin/travelsoft_crm_booking_tourist_edit.php?ID=' . $tid . '&lang=ru">подробнее...</a></td></tr>';
                            }
                        } else {
                            $options = '<option value="">...</option>';
                            foreach ($arr_tourists as $arTourist) {
                                $selected = "";
                                if (in_array($arTourist["ID"], $arUserField['VALUE'])) {
                                    $selected = 'selected=""';
                                }
                                $options .= '<option ' . $selected . ' value="' . $arTourist["ID"] . '">' . $arTourist["FULL_NAME"] . '</option>';
                            }
                            $select .= '<tr><td><select id="tourist-select-1" style="width:180px;" class="select2" name="' . 'UF_TOURISTS[]">' . $options . '</select> или <a href="javascript: CRMUtils.addTourist(\'tourist-select-1\')">Добавить нового</a></td></tr>';
                        }

                        $table_tourists .= $select . '</tbody><tfoot><tr><td><input value="+ Еще" type="button" onclick="CRMUtils.addTouristField()"></td></tr></tfoot></table>';

                        $content .= self::getEditFieldHtml('Туристы:', $table_tourists, true);
                    } else {

                        $content .= self::getEditFieldHtml(
                                        "Тип услуги:", \SelectBoxFromArray("UF_SERVICE_TYPE", self::getReferencesSelectData(\travelsoft\booking\Settings::getServicesTypesWithout(array("rooms")), "name", "type"), $arr_booking["UF_SERVICE_TYPE"], "", ' id="UF_SERVICE_TYPE"'), true);

                        switch ($arr_booking["UF_SERVICE_TYPE"]) {

                            case "packagetour":

                                if ($arr_booking["UF_SERVICE"] > 0) {
                                    $content .= self::getEditFieldHtml(
                                                    "Услуга:", self::getPackageTourSelectView("UF_SERVICE", $arr_booking["UF_SERVICE"]), true);

                                    if ($arr_booking["UF_PLACEMENT"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_PLACEMENT" id="UF_PLACEMENT" name="UF_PLACEMENT" value="' . $arr_booking["UF_PLACEMENT"] . '" type="hidden">', false, true);

                                        $content .= self::getEditFieldHtml(
                                                        "Номер:", self::getRoomsSelectView("UF_ROOM", $arr_booking["UF_ROOM"], false, (int) $arr_booking["UF_PLACEMENT"]), true
                                        );
                                    }


                                    if ($arr_booking["UF_PLACEMENT_RATE"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_PLACEMENT_RATE" name="UF_PLACEMENT_RATE" value="' . $arr_booking["UF_PLACEMENT_RATE"] . '" type="hidden">', false, true);
                                    }

                                    if (strlen($arr_booking["UF_DATE_FROM_PLACE"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_FROM_PLACE" name="UF_DATE_FROM_PLACE" value="' . $arr_booking["UF_DATE_FROM_PLACE"] . '" type="hidden">', false, true);
                                    }

                                    if (strlen($arr_booking["UF_DATE_TO_PLACE"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_TO_PLACE" name="UF_DATE_TO_PLACE" value="' . $arr_booking["UF_DATE_TO_PLACE"] . '" type="hidden">', false, true);
                                    }

                                    if ($arr_booking["UF_TRANS_RATE"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_TRANS_RATE" name="UF_TRANS_RATE" value="' . $arr_booking["UF_TRANS_RATE"] . '" type="hidden">', false, true);
                                    }

                                    if (strlen($arr_booking["UF_DATE_FROM_TRANS"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_FROM_TRANS" name="UF_DATE_FROM_TRANS" value="' . $arr_booking["UF_DATE_FROM_TRANS"] . '" type="hidden">', false, true);
                                    }

                                    if ($arr_booking["UF_TRANSFER"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_TRANSFER" name="UF_TRANSFER" value="' . $arr_booking["UF_TRANSFER"] . '" type="hidden">', false, true);
                                    }

                                    if ($arr_booking["UF_TRANSFER_BACK"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_TRANSFER_BACK" name="UF_TRANSFER_BACK" value="' . $arr_booking["UF_TRANSFER_BACK"] . '" type="hidden">', false, true);
                                    }

                                    if ($arr_booking["UF_TRANS_BACK_RATE"] > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_TRANS_BACK_RATE" name="UF_TRANS_BACK_RATE" value="' . $arr_booking["UF_TRANS_BACK_RATE"] . '" type="hidden">', false, true);
                                    }

                                    if (strlen($arr_booking["UF_DATE_BACK_TRANS"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_BACK_TRANS" name="UF_DATE_BACK_TRANS" value="' . $arr_booking["UF_DATE_BACK_TRANS"] . '" type="hidden">', false, true);
                                    }
                                } else {
                                    $content .= self::getEditFieldHtml(
                                                    "Услуга:", self::getPackageTourSelectView("UF_SERVICE", ""), true);
                                }

                                break;

                            case "placements":

                                if ($arr_booking["UF_SERVICE"] > 0) {
                                    $content .= self::getEditFieldHtml(
                                                    "Услуга:", self::getPlacementsSelectView("UF_SERVICE", $arr_booking["UF_SERVICE"]), true);


                                    $content .= self::getEditFieldHtml(
                                                    "Номер:", self::getRoomsSelectView("UF_ROOM", $arr_booking["UF_ROOM"] ?: "", false, (int) $arr_booking["UF_SERVICE"]), true
                                    );


                                    if (strlen($arr_booking["UF_DATE_FROM_PLACE"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_FROM_PLACE" name="UF_DATE_FROM_PLACE" value="' . $arr_booking["UF_DATE_FROM_PLACE"] . '" type="hidden">', false, true);
                                    }

                                    if (strlen($arr_booking["UF_DATE_TO_PLACE"]) > 0) {
                                        $content .= self::getEditFieldHtml(
                                                        "", '<input id="UF_DATE_TO_PLACE" name="UF_DATE_TO_PLACE" value="' . $arr_booking["UF_DATE_TO_PLACE"] . '" type="hidden">', false, true);
                                    }
                                } else {
                                    $content .= self::getEditFieldHtml(
                                                    "Услуга:", self::getPlacementsSelectView("UF_SERVICE", ""), true);
                                }

                                break;

                            case "transfer":
                            case "transferback":

                                $content .= self::getEditFieldHtml(
                                                "Услуга:", self::getTransferSelectView("UF_SERVICE", $arr_booking["UF_SERVICE"]), true);

                                if ($arr_booking["UF_TRANS_RATE"] > 0) {
                                    $content .= self::getEditFieldHtml(
                                                    "", '<input id="UF_TRANS_RATE" name="UF_TRANS_RATE" value="' . $arr_booking["UF_TRANS_RATE"] . '" type="hidden">', false, true);
                                }
                                break;

                            case "excursiontour":
                                $content .= self::getEditFieldHtml(
                                                "Услуга:", self::getExcursionTourSelectView("UF_SERVICE", $arr_booking["UF_SERVICE"]), true);

                                if ($arr_booking["UF_EXCUR_TOUR_RATE"] > 0) {
                                    $content .= self::getEditFieldHtml(
                                                    "", '<input id="UF_EXCUR_TOUR_RATE" name="UF_EXCUR_TOUR_RATE" value="' . $arr_booking["UF_EXCUR_TOUR_RATE"] . '" type="hidden">', false, true);
                                }
                                break;
                        }

                        $content .= self::getEditFieldHtml(
                                        'Кол-во взрослых:', '<input id="UF_ADULTS" name="UF_ADULTS" value="' . $arr_booking["UF_ADULTS"] . '" type="text">', true);
                        $content .= self::getEditFieldHtml(
                                        'Кол-во детей:', '<input id="UF_CHILDREN" name="UF_CHILDREN" value="' . $arr_booking["UF_CHILDREN"] . '" type="text">');


                        $table_ages = '<table id="ages-add-table"><tbody>';

                        if (!empty($arr_booking["UF_CHILDREN_AGE"])) {
                            foreach ($arr_booking["UF_CHILDREN_AGE"] as $age) {
                                $table_ages .= '<tr><td><input name="UF_CHILDREN_AGE[]" value="' . $age . '" type="text"></td></tr>';
                            }
                        } else {
                            $table_ages .= '<tr><td><input name="UF_CHILDREN_AGE[]" value="" type="text"></td></tr>';
                        }

                        $table_ages .= '</tbody><tfoot><tr><td><input class="adm-btn-save" value="+ Еще" type="button" onclick="CRMUtils.addAgeField()"><td></tr></tfoot></table>';

                        $content .= self::getEditFieldHtml("Возраст детей:", $table_ages);

                        $content .= self::getEditFieldHtml(
                                        'Дата с:', \CAdminCalendar::CalendarDate("UF_DATE_FROM", $arr_booking["UF_DATE_FROM"]), true);

                        if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"])) {
                            $content .= self::getEditFieldHtml(
                                            'Дата по:', \CAdminCalendar::CalendarDate("UF_DATE_TO", $arr_booking["UF_DATE_TO"]), true);
                        }

                        $disabled = ' disabled="" ';
                        if (isset($_POST["VARIAN_IS_CHOOSED"])) {
                            $disabled = "";
                            $content .= self::getEditFieldHtml("", '<input ' . $disabled . ' id="VARIAN_IS_CHOOSED" name="VARIAN_IS_CHOOSED" value="1" type="hidden">', false, true);
                        }

                        $content .= self::getEditFieldHtml("", '<input class="adm-btn-save" value="Подобрать варианты" type="button" onclick="CRMUtils.findVariants()">', false, $arr_booking["ID"] > 0) . self::getEditFieldHtml(
                                        'Цена брутто:', '<input ' . $disabled . ' id="UF_COST" name="UF_COST" value="' . $arr_booking["UF_COST"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_CURRENCY', $arr_booking['UF_CURRENCY']), true);
                        $content .= self::getEditFieldHtml(
                                        'Цена нетто:', '<input ' . $disabled . ' id="UF_NETTO" name="UF_NETTO" value="' . $arr_booking["UF_NETTO"] . '" type="text">');
                        $content .= self::getEditFieldHtml(
                                        'Стоимость транспортных расходов:', '<input' . $disabled . ' id="UF_TS_PRICE" name="UF_TS_PRICE" value="' . $arr_booking["UF_TS_PRICE"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_TS_CURRENCY', $arr_booking['UF_TS_CURRENCY']), false);
                        $content .= self::getEditFieldHtml(
                                        'Наценка:', '<input ' . $disabled . ' id="UF_MARKUP_PRICE" name="UF_MARKUP_PRICE" value="' . $arr_booking["UF_MARKUP_PRICE"] . '" type="text"> &nbsp;' . self::getCurrencySelectForBookingEdit('UF_MARKUP_CURRENCY', $arr_booking['UF_MARKUP_CURRENCY']), false);
                        $content .= self::getEditFieldHtml(
                                        'Комиссия турпродукта (в валюте цены брутто):', '<input ' . $disabled . ' id="UF_DISCOUNT" name="UF_DISCOUNT" value="' . $arr_booking["UF_DISCOUNT"] . '" type="text">', false);
                        $content .= self::getEditFieldHtml(
                                        'Комиссия транспортных расходов (в валюте транспортных расходов):', '<input ' . $disabled . ' id="UF_TS_DISCOUNT" name="UF_TS_DISCOUNT" value="' . $arr_booking["UF_TS_DISCOUNT"] . '" type="text">', false);
                        
                        if (!Validator::isTransferServiceType($arr_booking["UF_SERVICE_TYPE"]) && $arr_booking["UF_SERVICE_TYPE"] !== "excursiontour") {
                            $content .= self::getEditFieldHtml(
                                            'Питание:', '<input id="UF_FOOD" name="UF_FOOD" value="' . $arr_booking["UF_FOOD"] . '" type="text">', false);
                        }
                        $table_tourists = '<table id="tourists-add-table"><tbody>';

                        $arr_tourists = \travelsoft\booking\stores\Tourists::get(
                                        array(
                                            "select" => array("ID", "UF_NAME", "UF_LAST_NAME", "UF_SECOND_NAME")
                        ));

                        $select = "";
                        if (!empty($arr_booking["UF_TOURISTS"])) {
                            $cnt = 0;
                            foreach ($arr_booking["UF_TOURISTS"] as $tid) {
                                $options = '<option value="">...</option>';
                                foreach ($arr_tourists as $arTourist) {
                                    $selected = "";
                                    if ($tid == $arTourist["ID"]) {
                                        $selected = 'selected=""';
                                    }
                                    $options .= '<option ' . $selected . ' value="' . $arTourist["ID"] . '">' . $arTourist["FULL_NAME"] . '</option>';
                                }
                                $cnt++;
                                $select .= '<tr><td><select id="tourist-select-' . $cnt . '" style="width:180px;" class="select2" name="' . 'UF_TOURISTS[]">' . $options . '</select> &nbsp; <a href="javascript: CRMUtils.addTourist(\'tourist-select-' . $cnt . '\')">добавить нового</a></td></tr>';
                            }
                        } else {
                            $options = '<option value="">...</option>';
                            foreach ($arr_tourists as $arTourist) {
                                $selected = "";
                                if (in_array($arTourist["ID"], $arUserField['VALUE'])) {
                                    $selected = 'selected=""';
                                }
                                $options .= '<option ' . $selected . ' value="' . $arTourist["ID"] . '">' . $arTourist["FULL_NAME"] . '</option>';
                            }
                            $select .= '<tr><td><select id="tourist-select-1" style="width:180px;" class="select2" name="' . 'UF_TOURISTS[]">' . $options . '</select> &nbsp; <a href="javascript: CRMUtils.addTourist(\'tourist-select-1\')">добавить нового</a></td></tr>';
                        }

                        $table_tourists .= $select . '</tbody><tfoot><tr><td><input class="adm-btn-save" value="+ Еще" type="button" onclick="CRMUtils.addTouristField()"></td></tr></tfoot></table>';

                        $content .= self::getEditFieldHtml('Туристы:', $table_tourists, true);
                    }

                    break;

                case "UF_STATUS":

                    if ($IS_NEW) {
                        $arr_statuses = array();
                        foreach (VouchersUtils::getStatuses() as $arr_status) {
                            if ($arr_status["ID"] != \travelsoft\booking\Settings::cancellationStatus() &&
                                    $arr_status["ID"] != \travelsoft\booking\Settings::cancellationRequestStatus()) {
                                $arr_statuses[$arr_status["ID"]] = $arr_status;
                            }
                        }
                        $statuses_html = \SelectBoxFromArray("UF_STATUS", self::getReferencesSelectData($arr_statuses, "UF_NAME", "ID"), $arUserField["VALUE"], "", 'style="width: 300px" id="UF_STATUS" class="select2"');
                    } else {
                        $statuses_html = VouchersUtils::getStatusesSelectView("UF_STATUS", $arUserField["VALUE"]);
                    }

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ':', $statuses_html, true);
                    break;
                case "UF_CLIENT":

                    $content .= self::getEditFieldHtml('Клиент или Агент:', VouchersUtils::getClientsSelectView("UF_CLIENT", $arUserField["VALUE"]) . (!$IS_NEW ? ' &nbsp; <a href="javascript: CRMUtils.editClient(' . $arUserField["VALUE"] . ')">редактировать</a>' : '&nbsp; <a href="javascript: CRMUtils.editClient()">добавить нового клиента</a>'), true);
                    break;

                case "UF_MANAGER":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ':', VouchersUtils::getManagersSelectView("UF_MANAGER", $arUserField["VALUE"] > 0 ? $arUserField["VALUE"] : \travelsoft\booking\adapters\User::id()), false);

                    break;

                case "UF_COMMENT":

                    $content .= self::getEditFieldHtml($arUserField['EDIT_FORM_LABEL'] . ':', '<textarea rows="10" cols="35" name="UF_COMMENT">' . $arUserField["VALUE"] . '</textarea>', false);
                    break;

                default :
                    $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml(self::isEditFormRequest(), $arUserField);
            }
        }
        return $content;
    }

    /**
     * Возвращает select для валюты
     * @staticvar array $arCurrencies
     * @param string $code
     * @param string $currentValue
     * @return string
     */
    public static function getCurrencySelectForBookingEdit(string $code, string $currentValue): string {

        return self::getCurrencySelect($code, $currentValue);
    }

    /**
     * @param int $voucher_id
     * @return string
     */
    public static function getDocumentsForPrintContent(int $voucher_id) {
        if ($voucher_id) {
            $content = self::getEditFieldHtml("Шаблон документа:", \SelectBoxFromArray("DOCTPL", self::getReferencesSelectData(\travelsoft\booking\stores\Documents::get(array("select" => array('ID', 'UF_NAME'))), "UF_NAME", "ID"), '', "", 'onchange="CRMUtils.buildDocLink(' . $voucher_id . ')"', false, "find_form"));
            $content .= self::getEditFieldHtml("Формат документа:", \SelectBoxFromArray("DOCFORMAT", self::getReferencesSelectData(array(
                                        array("name" => "docx", "value" => "docx"),
                                        array("name" => "pdf", "value" => "pdf")
                                            ), "name", "value"), '', "", 'onchange="CRMUtils.buildDocLink(' . $voucher_id . ')"', false, "voucher_form"));
            $content .= self::getEditFieldHtml("", '<span id="link-container"></span>');
            return $content;
        }

        return "Необходимо создать путевку";
    }

    /**
     * Возвращает HTML просмотра истории платежей на странице редактирования заказа
     * @param int $voucher_id
     * @return string
     */
    public static function getPaymentHistoryContent(int $voucher_id) {

        $content = '';

        $tbody = '';

        $dbHistories = \travelsoft\booking\stores\PaymentHistory::get(array('filter' => array('UF_ORDER_ID' => $voucher_id)), false);

        $arCashDesks = $arCreaters = $arPaymentTypes = array();

        while ($arPaymentHistory = $dbHistories->fetch()) {

            if ($arPaymentHistory['UF_CASH_DESK_ID'] > 0) {

                if (!isset($arCashDesks[$arPaymentHistory['UF_CASH_DESK_ID']])) {
                    $arCashDesks[$arPaymentHistory['UF_CASH_DESK_ID']] = current(stores\CashDesks::get(array('filter' => array('ID' => $arPaymentHistory['UF_CASH_DESK_ID']), 'select' => array('ID', 'UF_NAME'))));
                }
            }

            if ($arPaymentHistory['UF_PAYMENT_TYPE'] > 0) {

                if (!isset($arPaymentTypes[$arPaymentHistory['UF_PAYMENT_TYPE']])) {
                    $arPaymentTypes[$arPaymentHistory['UF_PAYMENT_TYPE']] = current(\travelsoft\booking\stores\PaymentsTypes::get(array('filter' => array('ID' => $arPaymentHistory['UF_PAYMENT_TYPE']), 'select' => array('ID', 'UF_NAME'))));
                }
            }

            if ($arPaymentHistory['UF_CREATER'] > 0) {

                if (!isset($arCreaters[$arPaymentHistory['UF_CREATER']])) {
                    $arCreaters[$arPaymentHistory['UF_CREATER']] = current(\travelsoft\booking\stores\Users::get(array('filter' => array('ID' => $arPaymentHistory['UF_CREATER']), 'select' => array('ID', 'NAME', 'SECOND_NAME', 'LAST_NAME', 'EMAIL'))));
                }
            }

            $tbody .= '<tr>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arPaymentHistory['UF_PRICE'] . '</td>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arPaymentHistory['UF_CURRENCY'] . '</td>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arCashDesks[$arPaymentHistory['UF_CASH_DESK_ID']]['UF_NAME'] . '</td>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arPaymentTypes[$arPaymentHistory['UF_PAYMENT_TYPE']]['UF_NAME'] . '</td>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arCreaters[$arPaymentHistory['UF_CREATER']]['FULL_NAME_WITH_EMAIL'] . '</td>'
                    . '<td style="text-align: left; border-bottom: 1px solid #000">' . $arPaymentHistory['UF_DATE_CREATE'] . '</td>'
                    . '</tr>';
        }

        if (strlen($tbody) > 0) {

            $content .= '<tr>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Сумма</b></td>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Валюта</b></td>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Касса</b></td>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Тип платежа</b></td>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Создатель</b></td>';
            $content .= '<td style="text-align: left; border-bottom: 1px solid #000"><b>Дата создания</b></td>';
            $content .= '</tr>';
            $content .= $tbody;

            $arVoucher = \travelsoft\booking\stores\Vouchers::getById($voucher_id);

            $content .= '<tr><td style="text-align: right; padding-top: 50px;" colspan="6">';
            $content .= '<table>';
            $content .= '<tr><td></td>';
            $content .= '<td style="padding: 10px"><b>Цена</b></td>';
            $content .= '<td style="padding: 10px"><b>Оплачено</b></td>';
            $content .= '<td style="padding: 10px"><b>К оплате</b></td></tr>';
            $content .= '<tr><td style="padding: 10px">Стоимость</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]["COST_WITHOUT_TS_FORMATTED"] . '</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]["COST_PAID_FORMATTED"] . '</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]["COST_TO_PAY_FORMATTED"] . '</td>';
            $content .= '</td></tr>';
            $content .= '<tr><td style="padding: 10px">Транспортные расходы</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]["TS_BYN_FORMATTED"] . '</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]["TS_PAID_BYN_FORMATTED"] . '</td>';
            $content .= '<td style="padding: 10px">' . $arVoucher["PAYMENT_HISTORY"]['TS_TO_PAY_BYN_FORMATTED'] . '</td>';
            $content .= '</td></tr>';
            $content .= '</table>';
            $content .= '</td></tr>';
        } else {

            $content .= '<tr><td colspan="6"><b>Платежей не поступило</b></td></tr>';
        }

        return $content;
    }

    /**
     * Возвращает HTML таба сообщений по путевке
     * @param int $voucher_id
     */
    public static function getMessagesContent(int $voucher_id) {

        global $APPLICATION;

        $APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/messages.css?" . randString(7) . "'>");

        $APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/messages.js?" . randString(7) . "'></script>");

        $content = '<tr><td>';

        $arMessages = \array_reverse(\array_values(\travelsoft\booking\stores\Messages::get([
                            'order' => ['UF_DATE' => 'DESC'],
                            'filter' => ['UF_VOUCHER_ID' => $voucher_id],
                            'limit' => 20
        ])));

        $content .= '<div id="messages-container">';

        if (empty($arMessages)) {

            $content .= '<i id="tech-message"><b>Нет сообщений по путевке</b></i>';
        } else {

            foreach ($arMessages as $arMessage) {

                $user = \travelsoft\booking\stores\Users::getById($arMessage['UF_USER_ID']);

                $content .= '<div class="message-container">';

                $content .= '<div class="user-container"><b><i>' . $user['FULL_NAME_WITH_EMAIL'] . ' &nbsp;&nbsp;&nbsp; ' . $arMessage['UF_DATE']->toString() . '</b></i></div>';

                $content .= '<div id="message-' . $arMessage['ID'] . '" class="message">' . strip_tags(trim($arMessage['UF_MESSAGE'])) . '</div>';

                $content .= '</div>';
            }
        }

        $content .= '</div>';

        $content .= '<div id="add-message-container">';

        $content .= '<textarea rows="10" name="message"></textarea><br>';
        $content .= '<input data-voucher-id="' . $voucher_id . '" data-sessid="' . bitrix_sessid() . '" data-ajax-url="' . Settings::REL_PATH_TO_MODULE_AJAX . "/messages.php" . '" id="send-message" type="button" class="adm-btn-save" value="Отправить">';

        $content .= '</div>';

        $content .= '</td></tr>';

        return $content;
    }

}
