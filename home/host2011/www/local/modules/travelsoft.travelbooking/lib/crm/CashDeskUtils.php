<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с кассами
 *
 * @author dimabresky
 */
class CashDeskUtils extends Utils {

    /**
     * Возвращает результат обработки формы добавления/редактирования касс
     * @return array
     */
    public static function processingCashDeskEditForm(): array {

        $url = Settings::CASH_DESKS_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            
            $data = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::cashDesksStoreId(), array());

            Validator::stringLessThenTwo($data['UF_NAME'], 'Название', $arErrors);

            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);
                    $result = \travelsoft\booking\crm\stores\CashDesks::update($ID, $data);
                } else {

                    $data['UF_CREATER'] = $GLOBALS['USER']->GetID();
                    $result = \travelsoft\booking\crm\stores\CashDesks::add($data);
                }

                if ($result) {

                    LocalRedirect($url);
                }
            }
        }


        return array('errors' => $arErrors, 'result' => $result);
    }

    /**
     * Возвращает HTML полей для формы редактирования кассы
     * @param array $data
     * @return string
     */
    public static function getCashDeskEditFieldsContent(array $data = null): string {
        
        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::cashDesksStoreId(), $data);

        $isFormRequest = self::isEditFormRequest();

        $content = '';

        foreach ($arUserFields as $arUserField) {

            if ($arUserField['FIELD_NAME'] == 'UF_CREATER') {
                if ($arUserField['VALUE'] > 0) {

                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ":", \travelsoft\booking\stores\Users::getFullUserNameWithEmailByFields(\travelsoft\booking\stores\Users::getById($arUserField['VALUE'])));
                }
                continue;
            }


            $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
        }

        return $content;
    }

}
