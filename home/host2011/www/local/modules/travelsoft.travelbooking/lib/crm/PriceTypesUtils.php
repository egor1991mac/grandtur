<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с типами цен
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class PriceTypesUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::PRICE_TYPES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::priceTypesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::priceTypesStoreId(), array());

            foreach ($arSave as $field => $value) {

                if ($field === "UF_NAME") {
                    Validator::stringLessThenTwo($value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_CALC_TYPE") {
                    Validator::numericNotEmpty((int)$value, $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }

            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\PriceTypes::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\PriceTypes::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::priceTypesStoreId(), $arData);

        $fields = array();

        foreach ($arUserFields as $arUserField) {

            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField['FIELD_NAME'] === "UF_CALC_TYPE") {
                $formField["view"] = self::getCalculationTypesSelectView($arUserField['FIELD_NAME'], $arUserField['VALUE']);
                $formField["required"] = true;
            } else {
                
                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = $arUserField['FIELD_NAME'] !== "UF_MIN_AGE" && $arUserField['FIELD_NAME'] !== "UF_MAX_AGE";
            }

            $fields[] = $formField;
        }
        return $fields;
    }

}
