<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с тарифами
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class RatesUtils extends Utils {

    /**
     * @param bool $useRedirectIfOk
     * @return array
     */
    static public function processingEditForm(bool $useRedirectIfOk = true) {

        $url = \travelsoft\booking\crm\Settings::RATES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::ratesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::ratesStoreId(), array());

            $arFields = array_keys($arUserFieldsData);

            foreach ($arFields as $field) {

                if ($field === "UF_NAME") {
                    Validator::stringLessThenTwo($arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_SERVICES_TYPE") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_CURRENCY") {
                    Validator::issetKeyArray((string) $arSave[$field], (new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_PRICE_TYPES") {
                    Validator::arrayIsEmpty((array)$arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_DURATION") {
                    
                    if ($arSave['UF_BY_PERIOD'] > 0) {
                        
                        if ($arSave[$field] <= 0) {
                            Validator::numericNotEmpty(intVal($arSave[$field]), $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                        }
                    }
                    continue;
                }
            }

            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Rates::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Rates::add($arSave);
                }
            }

            if ($result && $useRedirectIfOk) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    /**
     * @param array $arData
     * @param array $fieldsToHidden
     * [
     *      "{field name}" => "{value}"
     * ]
     * @return array
     */
    public static function getEditFormFields(array $arData = null, array $fieldsToHidden = null) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::ratesStoreId(), $arData);

        $fields = array();

        $type = "placements";
        foreach ($arUserFields as $arUserField) {
            
            if ($arUserField["FIELD_NAME"] === "UF_CITY") {
                continue;
            }
            
            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if (isset($fieldsToHidden[$arUserField['FIELD_NAME']])) {

                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $fieldsToHidden[$arUserField['FIELD_NAME']] . '" type="hidden">';

                $formField["hide"] = true;

                if ($arUserField['FIELD_NAME'] === "UF_SERVICES_TYPE") {
                    $arUserFields['UF_SERVICES_TYPE']['VALUE'] = $fieldsToHidden[$arUserField['FIELD_NAME']]; // для отображения полей на максимальное количество человек
                }
            } else {

                if ($arUserField['FIELD_NAME'] === "UF_SERVICES_TYPE") {


                    if (strlen($arUserField['VALUE']) && $arUserField['VALUE'] !== "placements") {
                        $type = $arUserField['VALUE'];
                    }

                    $serviceTypes = \travelsoft\booking\Settings::getServicesTypesWithout(array("packagetour", "individual"));

                    $formField["view"] = \SelectBoxFromArray("UF_SERVICES_TYPE", self::getReferencesSelectData($serviceTypes, "name", "type"), $type, "", 'data-services-target="select[name=\'UF_SERVICES[]\']" id="UF_SERVICES_TYPE" class="select2"');


                    $formField["required"] = true;
                } elseif ($arUserField['FIELD_NAME'] === "UF_SERVICES") {

                    $method = "get" . ucfirst($type) . "SelectView";
                    $formField["view"] = self::$method("UF_SERVICES[]", $arUserField["VALUE"], true);

                    foreach (\travelsoft\booking\crm\Utils::getBookingServices(
                            array_keys(\travelsoft\booking\Settings::getServicesTypesWithout(array("packagetour")))) as $type => $arr) {

                        $jsBookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
                    }

                    $formField["view"] .= "<script>var jsBookingServices = " . json_encode($jsBookingServices) . "</script>";

                    $formField["required"] = false;
                } elseif ($arUserField['FIELD_NAME'] === "UF_CURRENCY") {

                    $formField["view"] = \SelectBoxFromArray("UF_CURRENCY", self::getReferencesSelectData((new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), "TITLE", "ISO"), $arUserField["VALUE"], "", 'id="UF_CURRENCY" class="select2"');

                    $formField["required"] = true;
                } elseif ($arUserField['FIELD_NAME'] === "UF_PRICE_TYPES") {

                    $formField["view"] = \SelectBoxMFromArray("UF_PRICE_TYPES[]", self::getReferencesSelectData(self::getPriceTypes(), "UF_NAME", "ID"), $arUserField["VALUE"], "", "", "", 'id="UF_PRICE_TYPES" class="select2"');

                    $formField["required"] = true;
                } elseif ($arUserField['FIELD_NAME'] === "UF_FOOD") {

                    $formField["view"] = self::getFoodSelectView("UF_FOOD", $arUserField["VALUE"], false);
                } elseif ($arUserField['FIELD_NAME'] === "UF_MAX_ADULTS" || $arUserField['FIELD_NAME'] === "UF_MAX_CHILDREN" || $arUserField['FIELD_NAME'] === "UF_MIN_CHILDREN" || $arUserField['FIELD_NAME'] === "UF_MIN_ADULTS") {

                    $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                    $formField["hide"] = $arUserFields['UF_SERVICES_TYPE']['VALUE'] !== 'excursiontour';
                } elseif ($arUserField['FIELD_NAME'] === "UF_BY_PERIOD") {
                    
                    $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="0" type="hidden"><input name="' . $arUserField["FIELD_NAME"] . '" value="1" '.(intVal($arUserField["VALUE"]) > 0 ? "checked" : "").' type="checkbox">';
                    $formField["hide"] = $arUserFields['UF_SERVICES_TYPE']['VALUE'] != '' && $arUserFields['UF_SERVICES_TYPE']['VALUE'] !== 'placements' && $arUserFields['UF_SERVICES_TYPE']['VALUE'] !== 'rooms';
                } elseif ($arUserField['FIELD_NAME'] === "UF_DURATION") {
                    
                    $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="'.$arUserField["VALUE"].'" type="text">';
                    $formField["hide"] = intVal($arUserFields["UF_BY_PERIOD"]["VALUE"]) <= 0 && intVal(@$_POST['UF_BY_PERIOD']) <= 0;
                } elseif ($arUserField['FIELD_NAME'] === "UF_CITY") {
                    
                    //$formField["view"] = self::getCitiesSelectView($arUserField['FIELD_NAME'], $arUserField['VALUE']);
                    //$formField['hide'] = !in_array($arUserFields["UF_SERVICES_TYPE"]["VALUE"], ['excursiontour', 'transfer', 'transferback']);
                } else {

                    $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                    $formField["required"] = true;
                }
            }

            $fields[] = $formField;
        }

        return $fields;
    }

    /**
     * Устанавливает в массив информацию по размещению
     * @param array $arPlacements
     * @param int $id
     */
    public static function setPlacement(&$arPlacements, $id) {
        if (!isset($arPlacements[$id])) {
            $arPlacements[$id] = current(\travelsoft\booking\stores\Placements::get(array(
                        "filter" => array("ID" => $id),
                        "select" => array("ID", "NAME")
            )));
        }
    }

}
