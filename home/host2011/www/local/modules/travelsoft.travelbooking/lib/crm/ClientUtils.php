<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы c клиентами
 *
 * @author dimabresky
 */
class ClientUtils extends Utils {

    /**
     * Обработка формы добавления/редактирования клиента
     * @return array
     */
    public static function processingEditForm() {

        $arErrors = array();

        $arr_save = array();
        
        $is_old = isset($_REQUEST["ID"]) && $_REQUEST["ID"] > 0;

        if (self::isEditFormRequest()) {

            if (Validator::checkEmail($_POST["EMAIL"], "Email", $arErrors)) {
                $arr_save["EMAIL"] = $_POST["EMAIL"];
                $arr_save["LOGIN"] = $_POST["EMAIL"];
            }

            if ($is_old) {

                if (isset($_POST["PASSWORD"]) && strlen($_POST["PASSWORD"]) > 0 && Validator::checkPassword((string)$_POST["PASSWORD"], (string)$_POST["CONFIRM_PASSWORD"], $arErrors)) {
                    $arr_save["PASSWORD"] = $_POST["PASSWORD"];
                    $arr_save["CONFIRM_PASSWORD"] = $_POST["CONFIRM_PASSWORD"];
                }
                
            } else {

                if (Validator::checkPassword((string)$_POST["PASSWORD"], (string)$_POST["CONFIRM_PASSWORD"], $arErrors)) {
                    $arr_save["PASSWORD"] = $_POST["PASSWORD"];
                    $arr_save["CONFIRM_PASSWORD"] = $_POST["CONFIRM_PASSWORD"];
                }
            }

            if (Validator::stringLessThenTwo($_POST["NAME"], "Имя", $arErrors)) {
                $arr_save["NAME"] = $_POST["NAME"];
            }

            if (Validator::stringLessThenTwo($_POST["SECOND_NAME"], "Отчество", $arErrors)) {
                $arr_save["SECOND_NAME"] = $_POST["SECOND_NAME"];
            }

            if (Validator::stringLessThenTwo($_POST["LAST_NAME"], "Фамилия", $arErrors)) {
                $arr_save["LAST_NAME"] = $_POST["LAST_NAME"];
            }

            if (Validator::checkDate($_POST["PERSONAL_BIRTHDAY"], "Дата рождения", $arErrors)) {
                $arr_save["PERSONAL_BIRTHDAY"] = $_POST["PERSONAL_BIRTHDAY"];
            }

            if (Validator::checkPhone($_POST["PERSONAL_PHONE"], "Телефон", $arErrors)) {
                $arr_save["PERSONAL_PHONE"] = $_POST["PERSONAL_PHONE"];
            }

            if (Validator::strIsEmpty($_POST["UF_PASS_SERIES"], "Серия паспорта", $arErrors)) {
                $arr_save["UF_PASS_SERIES"] = $_POST["UF_PASS_SERIES"];
            }

            if (Validator::strIsEmpty($_POST["UF_PASS_NUMBER"], "Номер паспорта", $arErrors)) {
                $arr_save["UF_PASS_NUMBER"] = $_POST["UF_PASS_NUMBER"];
            }
            
            if (Validator::strIsEmpty($_POST["UF_PASS_DATE_ISSUE"], "Дата выдачи паспорта", $arErrors)) {
                $arr_save["UF_PASS_DATE_ISSUE"] = $_POST["UF_PASS_DATE_ISSUE"];
            }
            
            if (Validator::strIsEmpty($_POST["UF_PASS_ISSUED_BY"], "Кем выдан паспорт", $arErrors)) {
                $arr_save["UF_PASS_ISSUED_BY"] = $_POST["UF_PASS_ISSUED_BY"];
            }

            if (Validator::strIsEmpty($_POST["UF_ADDRESS"], "Адрес", $arErrors)) {
                $arr_save["UF_ADDRESS"] = $_POST["UF_ADDRESS"];
            }
            
            if (empty($arErrors)) {
                if ($is_old) {
                    $ID = (int)$_REQUEST["ID"];
                    $result = \travelsoft\booking\stores\Users::update($ID, $arr_save);
                } else {
                    
                    if (!\travelsoft\booking\adapters\User::existsByEmail($arr_save["EMAIL"])) {
                         $arr_save["GROUP_ID"] = array(\travelsoft\booking\Settings::clientsUGroup());
                         $result = \travelsoft\booking\stores\Users::add($arr_save);
                         if ($result > 0) {
                             \travelsoft\booking\adapters\User::sendUserInfo((int)$result);
                         }
                    } else {
                        $arErrors[] = "Пользователь с таким Email существует";
                    }
                }
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getFieldsContent(array $arr_client = array()) {

        if (strlen($_POST["NAME"]) > 0) {
            $arr_client["NAME"] = $_POST["NAME"];
        }
        if (strlen($_POST["SECOND_NAME"]) > 0) {
            $arr_client["SECOND_NAME"] = $_POST["SECOND_NAME"];
        }
        if (strlen($_POST["LAST_NAME"]) > 0) {
            $arr_client["LAST_NAME"] = $_POST["LAST_NAME"];
        }
        if (strlen($_POST["EMAIL"]) > 0) {
            $arr_client["EMAIL"] = $_POST["EMAIL"];
        }
        if (strlen($_POST["PERSONAL_BIRTHDAY"]) > 0) {
            $arr_client["PERSONAL_BIRTHDAY"] = $_POST["PERSONAL_BIRTHDAY"];
        }
        if (strlen($_POST["UF_PASS_SERIES"]) > 0) {
            $arr_client["UF_PASS_SERIES"] = $_POST["UF_PASS_SERIES"];
        }
        if (strlen($_POST["UF_PASS_NUMBER"]) > 0) {
            $arr_client["UF_PASS_NUMBER"] = $_POST["UF_PASS_NUMBER"];
        }
        if (strlen($_POST["UF_PASS_DATE_ISSUE"]) > 0) {
            $arr_client["UF_PASS_DATE_ISSUE"] = $_POST["UF_PASS_DATE_ISSUE"];
        }
        if (strlen($_POST["UF_PASS_ISSUED_BY"]) > 0) {
            $arr_client["UF_PASS_ISSUED_BY"] = $_POST["UF_PASS_ISSUED_BY"];
        }
        if (strlen($_POST["UF_ADDRESS"]) > 0) {
            $arr_client["UF_ADDRESS"] = $_POST["UF_ADDRESS"];
        }
        if (strlen($_POST["PERSONAL_PHONE"]) > 0) {
            $arr_client["PERSONAL_PHONE"] = $_POST["PERSONAL_PHONE"];
        }

        $content = "";
        if (!isset($_REQUEST["ID"])) {
            $arr_tourists = \travelsoft\booking\stores\Tourists::get(array(
                        "select" => array("ID", "UF_NAME", "UF_SECOND_NAME", "UF_LAST_NAME", "UF_BIRTHDATE", "UF_PASS_SERIES", "UF_PASS_NUMBER", "UF_PASS_ISSUED_BY", "UF_PASS_DATE_ISSUE")
            ));

            $content .= self::getEditFieldHtml("Выбрать туриста в качестве шаблона:", \SelectBoxFromArray("TOURISTS", self::getReferencesSelectData($arr_tourists, "FULL_NAME", "ID"), "", "", "", false, "client_form") . "<script>var jsTouristsStorage = " . json_encode($arr_tourists) . "</script>", false);
        }

        $content .= self::getEditFieldHtml("Имя:", '<input id="NAME" name="NAME" type="text" value="' . htmlspecialcharsbx($arr_client["NAME"]) . '">', true);
        $content .= self::getEditFieldHtml("Отчество:", '<input id="SECOND_NAME" name="SECOND_NAME" type="text" value="' . htmlspecialcharsbx($arr_client["SECOND_NAME"]) . '">', true);
        $content .= self::getEditFieldHtml("Фамилия:", '<input id="LAST_NAME" name="LAST_NAME" type="text" value="' . htmlspecialcharsbx($arr_client["LAST_NAME"]) . '">', true);
        $content .= self::getEditFieldHtml("Email:", '<input name="EMAIL" value="' . htmlspecialcharsbx($arr_client["EMAIL"]) . '" type="text">', true);
        $content .= self::getEditFieldHtml("Пароль:", '<input name="PASSWORD" value="' . htmlspecialcharsbx($arr_client["PASSWORD"]) . '" type="password">', !($_REQUEST["ID"] > 0));
        $content .= self::getEditFieldHtml("Подтверждение пароля:", '<input name="CONFIRM_PASSWORD" value="' . htmlspecialcharsbx($arr_client["CONFIRM_PASSWORD"]) . '" type="password">', !($_REQUEST["ID"] > 0));

        $content .= self::getEditFieldHtml("Дата рождения", \CAdminCalendar::CalendarDate("PERSONAL_BIRTHDAY", $arr_client["PERSONAL_BIRTHDAY"]), true);
        $content .= self::getEditFieldHtml("Телефон:", '<input id="PERSONAL_PHONE" name="PERSONAL_PHONE" type="text" value="' . htmlspecialcharsbx($arr_client["PERSONAL_PHONE"]) . '">', true);
        $content .= self::getEditFieldHtml("Серия паспорта:", '<input id="UF_PASS_SERIES" name="UF_PASS_SERIES" type="text" value="' . htmlspecialcharsbx($arr_client["UF_PASS_SERIES"]) . '">', true);
        $content .= self::getEditFieldHtml("Номер паспорта:", '<input id="UF_PASS_NUMBER" name="UF_PASS_NUMBER" type="text" value="' . htmlspecialcharsbx($arr_client["UF_PASS_NUMBER"]) . '">', true);
        $content .= self::getEditFieldHtml("Дата выдачи паспорта:",  \CAdminCalendar::CalendarDate("UF_PASS_DATE_ISSUE", $arr_client["UF_PASS_DATE_ISSUE"]), true);
        $content .= self::getEditFieldHtml("Кем выдан паспорт:", '<input id="UF_PASS_ISSUED_BY" name="UF_PASS_ISSUED_BY" type="text" value="' . htmlspecialcharsbx($arr_client["UF_PASS_ISSUED_BY"]) . '">', true);
        $content .= self::getEditFieldHtml("Адрес проживания:", '<input id="UF_ADDRESS" name="UF_ADDRESS" type="text" value="' . htmlspecialcharsbx($arr_client["UF_ADDRESS"]) . '">', true);

        return $content;
    }
    
}
