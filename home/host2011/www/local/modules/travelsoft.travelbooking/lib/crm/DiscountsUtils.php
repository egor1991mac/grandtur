<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы со скидками
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class DiscountsUtils extends Utils {

    /**
     * Обработка формы добавления скидки
     * @global \travelsoft\booking\crm\type $USER_FIELD_MANAGER
     * @return array
     */
    public static function processingDiscountEditForm() {

        global $USER_FIELD_MANAGER;

        $url = Settings::DISCOUNTS_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            $data = array();

            $USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::discountsStoreId(), $data);

            if (!strlen($data['UF_SERVICE_TYPE'])) {

                $arErrors[] = 'Укажите тип услуги';
            }

            if ($data['UF_VALUE'] <= 0 && $data["UF_TS_VALUE"] <= 0) {

                $arErrors[] = 'Укажите размер Комиссии';
            }

            if (!strlen($data['UF_DTYPE'])) {

                $arErrors[] = 'Укажите тип Комиссии';
            }

            if (!strlen($data['UF_VARIANT']) && !strlen($data['UF_TS_VARIANT'])) {

                $arErrors[] = 'Укажите вариант расчета Комиссии';
            }

            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);

                    $result = \travelsoft\booking\stores\Discounts::update($ID, $data);
                } else {

                    $result = \travelsoft\booking\stores\Discounts::add($data);
                }

                if ($result) {

                    LocalRedirect($url);
                }
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getDiscountsFieldsContent(array $arDiscounts = array()) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::discountsStoreId(), $arDiscounts);

        $isFormRequest = self::isEditFormRequest();

        $content = '';

        foreach ($arUserFields as $arUserField) {

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            switch ($arUserField['FIELD_NAME']) {

                case "UF_SERVICE_TYPE":
                    $serviceTypes = \travelsoft\booking\Settings::getServicesTypes();
                    
                    $service_type = "placements";
                    if (strlen($arUserField['VALUE'])) {
                        $service_type = $arUserField['VALUE'];
                    }
                    
                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', \SelectBoxFromArray("UF_SERVICE_TYPE", self::getReferencesSelectData($serviceTypes, "name", "type"), $arUserField["VALUE"], "", 'data-services-target="select[name=\'UF_SERVICES[]\']" id="UF_SERVICES_TYPE" class="select2"'), true
                    );

                    break;

                case "UF_VARIANT":
                case "UF_TS_VARIANT":
                    $option = '<option selected="" value="">Выбрать из списка</option>';

                    $variants = array(
                        array("ID" => "F", "NAME" => "фиксированная"),
                        array("ID" => "P", "NAME" => "процент")
                    );

                    foreach ($variants as $variant) {

                        $option .= '<option ' . ($arUserField['VALUE'] == $variant['ID'] ? 'selected=""' : '') . ' value="' . $variant['ID'] . '">' . $variant['NAME'] . '</option>';
                    }

                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', '<select style="width:180px;" class="select2" name="' . $arUserField['FIELD_NAME'] . '">' . $option . '</select>'
                    );

                    break;

                case "UF_DTYPE":

                    $option = '<option selected="" value="">Выбрать из списка</option>';

                    $types = array(
                        array("ID" => "A", "NAME" => "Комиссия для агенств"),
                        array("ID" => "PR", "NAME" => "Промо скидка")
                    );

                    foreach ($types as $type) {

                        $option .= '<option ' . ($arUserField['VALUE'] == $type['ID'] ? 'selected=""' : '') . ' value="' . $type['ID'] . '">' . $type['NAME'] . '</option>';
                    }

                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', '<select style="width:180px;" class="select2" name="' . $arUserField['FIELD_NAME'] . '">' . $option . '</select>'
                    );

                    break;

                case 'UF_SERVICES':
                    $method = "get" . ucfirst($service_type) . "SelectView";
                    
                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', self::$method("UF_SERVICES[]", $arUserField["VALUE"], true), true
                    );

                    foreach (\travelsoft\booking\crm\Utils::getBookingServices(
                            array_keys(\travelsoft\booking\Settings::getServicesTypes())) as $type => $arr) {
                        $jsBookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
                    }
                    
                    $content .= "<script>var jsBookingServices = " . json_encode($jsBookingServices) . "</script>";

                    break;

                case 'UF_AGENT':

                    $option = '<option selected="" value="">Выбрать из списка</option>';

                    $arUsers = \travelsoft\booking\stores\Users::get(array('filter' => array('GROUPS_ID' => array(\travelsoft\booking\Settings::agentsUGroup())), 'select' => array('ID', 'UF_LEGAL_NAME')));
                    foreach ($arUsers as $arUser) {
                        $option .= '<option ' . ($arUserField['VALUE'] > 0 && $arUserField['VALUE'] == $arUser['ID'] ? 'selected=""' : '') . ' value="' . $arUser['ID'] . '">' . $arUser['UF_LEGAL_NAME'] . '</option>';
                    }

                    $content .= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', '<select style="width:180px;" class="select2" name="' . $arUserField['FIELD_NAME'] . '">' . $option . '</select>');

                    break;
                
                case "UF_VALUE":
                case "UF_TS_VALUE":
                    
                    $content.= self::getEditFieldHtml(
                                    $arUserField['EDIT_FORM_LABEL'] . ':', '<input type="text" name="' . $arUserField['FIELD_NAME'] . '" value="'.$arUserField['VALUE'].'">');
                    
                    break;
                    
                default:
                    
                    if ($arUserField["FIELD_NAME"] === "UF_SORT" && $arUserField["VALUE"] <= 0) {
                        $arUserField["VALUE"] = 100;
                    }
                    
                    $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
            }
        }

        return $content;
    }
    
    /**
     * Возвращает фильтр для выборки списка скидок
     * @return array
     */
    public static function getDiscountsFilter(): array {

        if (strlen($_REQUEST['CANCEL']) > 0) {
            \LocalRedirect($GLOBALS['APPLICATION']->GetCurPageParam("", array(
                        'UF_LP_FROM',
                        'UF_LP_TO',
                        'UF_TD_FROM',
                        'UF_TD_TO',
                        'UF_AGENT',
                        'UF_VARIANT',
                        'UF_DTYPE',
                        'CANCEL'
            )));
        }

        $filter = array();

        if (strlen($_REQUEST['UF_LP_FROM'])) {

            $filter['UF_LP_FROM'] = \travelsoft\booking\adapters\Date::create($_REQUEST['UF_LP_FROM']);
        }

        if (strlen($_REQUEST['UF_LP_TO'])) {

            $filter['UF_LP_TO'] = \travelsoft\booking\adapters\Date::create($_REQUEST['UF_LP_TO']);
        }

        $intFields = array(
            'UF_AGENT',
            'UF_DTYPE',
            'UF_VARIANT'
        );

        foreach ($intFields as $field) {
            if (strlen($_REQUEST[$field]) > 0) {
                $filter[$field] = $_REQUEST[$field];
            }
        }

        return $filter;
    }

}
