<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с туруслугой
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TourservicesUtils extends Utils {
    
    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::TOURSERVICES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }
        
        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::tourservicesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::tourservicesStoreId(), array());
            
            $arFields = array_keys($arUserFieldsData);
            
            foreach ($arFields as $field) {

                if ($field === "UF_SERVICE_TYPE") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_CURRENCY") {
                    Validator::issetKeyArray((string) $arSave[$field], (new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_ADULTS_PRICE") {
                    Validator::numericMoreOrEqualZero($arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_CHILDREN_PRICE") {
                    
                    $tmp_prices = $tmp_min_age = $tmp_max_age = [];
                    
                    foreach ($arSave[$field] as $k => $v) {
                        if ($v > 0) {
                            
                            $tmp_prices[] = $v;
                            $tmp_max_age[] = $arSave["UF_MAX_AGE"][$k] <= 0 ? 0 : intVal($arSave["UF_MAX_AGE"][$k]);
                            $tmp_min_age[] = $arSave["UF_MIN_AGE"][$k] <= 0 ? 0 : intVal($arSave["UF_MIN_AGE"][$k]);                            
                        }
                    }
//                    var_dump($tmp_prices);die;
                    if (!empty($tmp_prices)) {
                        $arSave["UF_CHILDREN_PRICE"] = $tmp_prices;
                        $arSave["UF_MAX_AGE"] = $tmp_max_age;
                        $arSave["UF_MIN_AGE"] = $tmp_min_age;
                    } else {
                        Validator::numericMoreOrEqualZero("", $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    }
                    
                    continue;
                }
            }
            
            if (!isset($arSave["UF_SERVICES"])) {
                $arSave["UF_SERVICES"] = array();
            }
            
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Tourservices::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Tourservices::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }
    
    public static function getEditFormFields($arData) {
        
        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::tourservicesStoreId(), $arData);

        $fields = array();
        
        $type = "placements";
        foreach ($arUserFields as $arUserField) {
            
            if (in_array($arUserField['FIELD_NAME'], ["UF_MIN_AGE", "UF_MAX_AGE"])) {
                
               continue; 
            }
            
            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField["FIELD_NAME"] === "UF_SERVICE_TYPE") {
                
                if (strlen($arUserField['VALUE']) && $arUserField['VALUE'] !== "placements") {
                    $type = $arUserField['VALUE'];
                }

                $serviceTypes = \travelsoft\booking\Settings::getServicesTypes();
                unset($serviceTypes["rooms"]);

                $formField["view"] = \SelectBoxFromArray("UF_SERVICE_TYPE", self::getReferencesSelectData($serviceTypes, "name", "type"), $type, "", 'data-services-target="select[name=\'UF_SERVICES[]\']" id="UF_SERVICE_TYPE" class="select2"');

                $formField["required"] = true;
            } elseif ($arUserField["FIELD_NAME"] === "UF_SERVICES") {

                $method = "get" . ucfirst($type) . "SelectView";
                
                $formField["view"] = self::$method("UF_SERVICES[]", $arUserField["VALUE"], true);
                
                foreach (\travelsoft\booking\crm\Utils::getBookingServices(
                        array_keys(\travelsoft\booking\Settings::getServicesTypesWithout(array("rooms")))) as $type => $arr) {
                    
                    $jsBookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
                }
                
                $formField["view"] .= "<script>var jsBookingServices = " . json_encode($jsBookingServices) . "</script>";
                $formField["required"] = false;
            } elseif ($arUserField['FIELD_NAME'] === "UF_CURRENCY") {

                $formField["view"] = \SelectBoxFromArray("UF_CURRENCY", self::getReferencesSelectData((new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), "TITLE", "ISO"), $arUserField["VALUE"], "", 'id="UF_CURRENCY" class="select2"');

                $formField["required"] = true;
            }  elseif ($arUserField['FIELD_NAME'] === "UF_CHILDREN_PRICE") {
                    
                    $table_tpl = "<table id=\"children-prices\">"
                                . "<thead>"
                                    . "<tr>"
                                        . "<th>стоимость</th>"
                                        . "<th>возраст от</th>"
                                        . "<th>возраст до</th>"
                                    . "</tr>"
                                . "</thead>"
                                . "<tbody>"
                                    . "{{trs-tpl}}"
                                . "</tbody>"
                                . "<tfoot>"
                                    . "<tr><td style=\"text-align: right\" colspan=\"3\"><input id=\"add-children-price-btn\" type=\"button\" class=\"adm-btn-save\" value=\"+ Еще\"></td></tr>"
                                . "</tfoot>"
                            . "</table>";
                    
                    $trs_tpl = "<tr>"
                            . "<td><input style=\"width: 100px\" name=\"UF_CHILDREN_PRICE[]\" value=\"{{price-value}}\" type=\"text\"></td>"
                            . "<td><input style=\"width: 100px\" name=\"UF_MIN_AGE[]\" value=\"{{age-min-value}}\" type=\"text\"></td>"
                            . "<td><input style=\"width: 100px\" name=\"UF_MAX_AGE[]\" value=\"{{age-max-value}}\" type=\"text\"></td>"
                            . "</tr>";
                    
                    $tr = [];
                    if (!empty($arUserField["VALUE"])) {
                        foreach ($arUserField["VALUE"] as $k => $v) {
                            $tr[] = \str_replace(["{{price-value}}", "{{age-min-value}}", "{{age-max-value}}"], [$v, $arUserFields["UF_MIN_AGE"]["VALUE"][$k], $arUserFields["UF_MAX_AGE"]["VALUE"][$k]], $trs_tpl);
                        }
                    }
                    
                    $tr[] = \str_replace(["{{price-value}}", "{{age-min-value}}", "{{age-max-value}}"], ["", "", ""], $trs_tpl);
                    
                    $formField["view"] = \str_replace(["{{trs-tpl}}"], implode("", $tr), $table_tpl);
                    $formField["required"] = true;
                } elseif ($arUserField['FIELD_NAME'] === "UF_CITY") {
                    
                    $formField["view"] = self::getCitiesSelectView($arUserField['FIELD_NAME'], $arUserField['VALUE']);
                    $formField["required"] = false;
                } else {

                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = true;
            }

            $fields[] = $formField;
        }
        return $fields;
    }

}
