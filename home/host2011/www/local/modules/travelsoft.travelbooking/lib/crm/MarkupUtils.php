<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с номерным фондом
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class MarkupUtils extends Utils {
    
    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::MARKUP_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }
        
        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::markupStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::markupStoreId(), array());
            
            $arFields = array_keys($arUserFieldsData);
            
            foreach ($arFields as $field) {

                if ($field === "UF_SERVICE_TYPE") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }

                if ($field === "UF_CURRENCY") {
                    Validator::issetKeyArray((string) $arSave[$field], (new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
                
                if ($field === "UF_PRICE") {
                    Validator::numericMoreOrEqualZero($arSave[$field],$arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }
                        
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Markup::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Markup::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }
    
    public static function getEditFormFields($arData) {
        
        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::markupStoreId(), $arData);

        $fields = array();
        
        $type = "placements";
        foreach ($arUserFields as $arUserField) {

            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            if ($arUserField["FIELD_NAME"] === "UF_SERVICE_TYPE") {
                
                if (strlen($arUserField['VALUE']) && $arUserField['VALUE'] !== "placements") {
                    $type = $arUserField['VALUE'];
                }

                $serviceTypes = \travelsoft\booking\Settings::getServicesTypes();
                unset($serviceTypes["rooms"]);

                $formField["view"] = \SelectBoxFromArray("UF_SERVICE_TYPE", self::getReferencesSelectData($serviceTypes, "name", "type"), $type, "", 'data-services-target="select[name=\'UF_SERVICES[]\']" id="UF_SERVICE_TYPE" class="select2"');

                $formField["required"] = true;
            } elseif ($arUserField["FIELD_NAME"] === "UF_SERVICES") {

                $method = "get" . ucfirst($type) . "SelectView";
                $formField["view"] = self::$method("UF_SERVICES[]", $arUserField["VALUE"], true);
                
                foreach (\travelsoft\booking\crm\Utils::getBookingServices(
                        array_keys(\travelsoft\booking\Settings::getServicesTypesWithout(array("rooms")))) as $type => $arr) {
                    $jsBookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
                }
                
                $formField["view"] .= "<script>var jsBookingServices = " . json_encode($jsBookingServices) . "</script>";
                $formField["required"] = false;
            } elseif ($arUserField['FIELD_NAME'] === "UF_CURRENCY") {

                $formField["view"] = \SelectBoxFromArray("UF_CURRENCY", self::getReferencesSelectData((new \travelsoft\booking\adapters\CurrencyConverter)->getAcceptableISO(), "TITLE", "ISO"), $arUserField["VALUE"], "", 'id="UF_CURRENCY" class="select2"');

                $formField["required"] = true;
            } else {

                $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
                $formField["required"] = true;
            }

            $fields[] = $formField;
        }
        return $fields;
    }

}
