<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с добавлением цен
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class AddPricesUtils extends Utils {

    /**
     * Обрабатывает запрос от формы изменения цен и квот и возвращает результат
     * @param array $request
     * @return array
     */
    public static function processPriceAndQuotasFormRequest(array $request): array {

        $req = $request;

        $response = array();

        $service_id = (int) $request["service_id"];

        $service_type = (string) $request["service_type"];

        if ($service_id > 0 && strlen($service_type)) {

            # обработка квот
            if ($req['quotas']) {

                foreach ($req['quotas'] as $timestamp => $value) {

                    $response['quotas'][$timestamp] = self::_processQuotas($timestamp, intVal($value), $service_id, $service_type);
                }
            }

            # обработка квот на билеты
            if ($req['transfer_quotas']) {

                foreach ($req['transfer_quotas'] as $timestamp => $value) {

                    $response['transfer_quotas'][$timestamp] = self::_processTransferQuotas($timestamp, intVal($value), $service_id, $service_type);
                }
            }

            if ($req['duration']) {

                foreach ($req['duration'] as $timestamp => $value) {

                    $response['duration'][$timestamp] = self::_processDuration($timestamp, $value, $service_id, $service_type);
                }
            }

            # обработка stop sale
            if ($req['stop_sale']) {

                foreach ($req['stop_sale'] as $timestamp => $value) {

                    $response['stop_sale'][$timestamp] = self::_processStopSale($timestamp, boolval($value), $service_id, $service_type);
                }
            }

            # обработка цен
            if ($req['prices']) {

                foreach ($req['prices']["gross"] as $rate_id => $arr_prices) {
                    foreach ($arr_prices as $ptid => $value) {

                        foreach ($value as $timestamp => $vvalue) {

                            if ($vvalue === "0") {
                                $vvalue = \travelsoft\booking\Settings::FLOAT_NULL;
                            }

                            $response['prices']["gross"][$rate_id][$ptid][$timestamp] = self::_processGrossPrices($timestamp, intVal($rate_id), $ptid, (float) $vvalue, (int) $service_id, $service_type);
                        }
                    }
                }

                foreach ($req['prices']["netto"] as $rate_id => $arr_prices) {
                    foreach ($arr_prices as $ptid => $value) {

                        foreach ($value as $timestamp => $vvalue) {

                            if ($vvalue === "0") {
                                $vvalue = \travelsoft\booking\Settings::FLOAT_NULL;
                            }

                            $response['prices']["netto"][$rate_id][$ptid][$timestamp] = self::_processNettoPrices($timestamp, intVal($rate_id), $ptid, (float) $vvalue, (int) $service_id, $service_type);
                        }
                    }
                }
            }

            # обработка запроса на массовое редактирование
            if ($req['mass_edit']) {

                $me = $req['mass_edit'];

                if (!empty($me['unix_dates'])) {

                    if ($me['quotas']) {

                        foreach ($me['unix_dates'] as $timestamp) {

                            $response['quotas'][$timestamp] = self::_processQuotas($timestamp, intVal($me['quotas']['value']), $service_id, $service_type);
                        }
                    }

                    if ($me['transfer_quotas']) {

                        foreach ($me['unix_dates'] as $timestamp) {

                            $response['transfer_quotas'][$timestamp] = self::_processTransferQuotas($timestamp, intVal($me['transfer_quotas']['value']), $service_id, $service_type);
                        }
                    }

                    if ($me['duration']) {

                        foreach ($me['unix_dates'] as $timestamp) {

                            $response['duration'][$timestamp] = self::_processDuration($timestamp, $me['duration']['value'], $service_id, $service_type);
                        }
                    }

                    if ($me['stop_sale']) {

                        foreach ($me['unix_dates'] as $timestamp) {

                            $response['stop_sale'][$timestamp] = self::_processStopSale($timestamp, boolval($me['stop_sale']['value']), $service_id, $service_type);
                        }
                    }

                    if ($me['prices']) {

                        foreach ($me['unix_dates'] as $timestamp) {
                            foreach ($me['prices']['gross'] as $rate_id => $arr) {
                                foreach ($arr as $ptid => $value) {

                                    if ($value === "0") {
                                        $value = \travelsoft\booking\Settings::FLOAT_NULL;
                                    }

                                    $response['prices']['gross'][$rate_id][$ptid][$timestamp] = self::_processGrossPrices($timestamp, intVal($rate_id), intVal($ptid), (float) $value, (int) $service_id, $service_type);
                                }
                            }
                        }

                        foreach ($me['unix_dates'] as $timestamp) {
                            foreach ($me['prices']['netto'] as $rate_id => $arr) {
                                foreach ($arr as $ptid => $value) {

                                    if ($value === "0") {
                                        $value = \travelsoft\booking\Settings::FLOAT_NULL;
                                    }

                                    $response['prices']['netto'][$rate_id][$ptid][$timestamp] = self::_processNettoPrices($timestamp, intVal($rate_id), intVal($ptid), (float) $value, (int) $service_id, $service_type);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $response;
    }

    /**
     * Обрабатывает запрос на изменение квот на билеты и возвращает результат
     * @param string $timestamp
     * @param int $value
     * @param int $service_id
     * @param string $service_type
     * @return array
     */
    public static function _processTransferQuotas(string $timestamp, int $value, int $service_id, string $service_type): array {

        $result = $dbQuota = $dbTransferQuota = array();

        $dbTransferQuota = current(\travelsoft\booking\stores\TransferQuotas::get(array(
                    'filter' => array(
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type
                    ),
                    'select' => array('ID', 'UF_SOLD_NUMBER')
        )));

        $dbQuota = current(\travelsoft\booking\stores\Quotas::get(array(
                    'filter' => array(
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type
                    ),
                    'select' => array('ID', 'UF_SOLD_NUMBER', 'UF_QUOTA')
        )));

        $onsale = 0;
        $sold = 0;

        if (isset($dbQuota["ID"])) {

            $onsale = $dbQuota["UF_QUOTA"] - $dbQuota["UF_SOLD_NUMBER"];
            $sold = $dbQuota["UF_SOLD_NUMBER"];
        }

        if ($dbTransferQuota['ID'] > 0) {

            if ($value >= 0) {

                \travelsoft\booking\stores\TransferQuotas::update($dbTransferQuota['ID'], array('UF_QUOTA' => $value));

                $onsale_transfer = $value - $dbTransferQuota['UF_SOLD_NUMBER'];

                $result = array(
                    'quota_value' => $value,
                    'sold_value' => $sold . "/" . $dbTransferQuota["UF_SOLD_NUMBER"],
                    'onsale_value' => $onsale . "/" . $onsale_transfer
                );
            }
        } else {

            if ($value >= 0) {

                \travelsoft\booking\stores\TransferQuotas::add(array(
                    'UF_QUOTA' => $value,
                    'UF_SOLD_NUMBER' => 0,
                    'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                    'UF_SERVICE_ID' => $service_id,
                    'UF_SERVICE_TYPE' => $service_type,
                ));

                $result = array(
                    'quota_value' => $value,
                    'sold_value' => $sold . "/0",
                    'onsale_value' => $onsale . "/0"
                );
            }
        }

        return $result;
    }

    /**
     * Обрабатывает запрос на изменение квот и возвращает результат
     * @param string $timestamp
     * @param int $value
     * @param int $service_id
     * @param string $service_type
     * @return array
     */
    public static function _processQuotas(string $timestamp, int $value, int $service_id, string $service_type): array {

        $inc_transfer_quotas = $service_type === "transfer" || $service_type === "transfer_back";

        $result = $dbQuota = $dbTransferQuota = array();

        $dbQuota = current(\travelsoft\booking\stores\Quotas::get(array(
                    'filter' => array(
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type
                    ),
                    'select' => array('ID', 'UF_SOLD_NUMBER')
        )));

        if ($inc_transfer_quotas) {
            $dbTransferQuota = current(\travelsoft\booking\stores\TransferQuotas::get(array(
                        'filter' => array(
                            'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                            'UF_SERVICE_ID' => $service_id,
                            'UF_SERVICE_TYPE' => $service_type
                        ),
                        'select' => array('ID', 'UF_SOLD_NUMBER')
            )));

            $onsale_transfer = $sold_transfer = 0;
            if (isset($dbTransferQuota["ID"])) {

                $sold_transfer = $dbTransferQuota["UF_SOLD_NUMBER"];
                $onsale_transfer = $dbTransferQuota["UF_QUOTA"] - $sold_transfer;
            }
        }

        if ($dbQuota['ID'] > 0) {

            if ($value >= 0) {

                \travelsoft\booking\stores\Quotas::update($dbQuota['ID'], array('UF_QUOTA' => $value));
                $onsale = $value - $dbQuota['UF_SOLD_NUMBER'];
                $result = array(
                    'quota_value' => $value,
                    'sold_value' => $dbQuota['UF_SOLD_NUMBER'] . ($inc_transfer_quotas ? "/" . $sold_transfer : ""),
                    'onsale_value' => $onsale .  ($inc_transfer_quotas ? "/" . $onsale_transfer : "")
                );
            }
        } else {

            if ($value >= 0) {

                \travelsoft\booking\stores\Quotas::add(array(
                    'UF_QUOTA' => $value,
                    'UF_SOLD_NUMBER' => 0,
                    'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                    'UF_SERVICE_ID' => $service_id,
                    'UF_SERVICE_TYPE' => $service_type,
                ));

                $result = array(
                    'quota_value' => $value,
                    'sold_value' => 0 . ($inc_transfer_quotas ? "/". $sold_transfer : ""),
                    'onsale_value' => 0 . ($inc_transfer_quotas ? "/" . $onsale_transfer : "")
                );
            }
        }

        return $result;
    }

    /**
     * Обрабатывает запрос на изменение квот и возвращает результат
     * @param string $timestamp
     * @param string $value
     * @param int $service_id
     * @param string $service_type
     * @return string
     */
    public static function _processDuration(string $timestamp, string $value, int $service_id, string $service_type): string {

        $result = "";

        $dbDuration = current(\travelsoft\booking\stores\Duration::get(array(
                    'filter' => array(
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type
                    ),
                    'select' => array('ID', 'UF_DURATION')
        )));

        if ($dbDuration['ID'] > 0) {

            if (!strlen($value)) {

                \travelsoft\booking\stores\Duration::delete($dbDuration['ID']);
                $result = "";
            } else {
                $arr_values = self::_preparingDurataion($value);
                \travelsoft\booking\stores\Duration::update($dbDuration['ID'], array('UF_DURATION' => $arr_values));
                $result = $value;
            }
        } else {

            if (strlen($value)) {
                $arr_values = self::_preparingDurataion($value);
                \travelsoft\booking\stores\Duration::add(array(
                    'UF_DURATION' => $arr_values,
                    'UF_SERVICE_ID' => $service_id,
                    'UF_SERVICE_TYPE' => $service_type,
                    'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp)
                ));

                $result = $value;
            }
        }

        return $result;
    }

    /**
     * Обрабатывает запрос на изменение stop sale и возвращает результат
     * @param string $timestamp
     * @param bool $value
     * @param int $service_id
     * @param string $service_type
     * @return int
     */
    public static function _processStopSale(string $timestamp, bool $value, int $service_id, string $service_type): int {

        $result = 0;

        $dbQuota = current(\travelsoft\booking\stores\Quotas::get(array(
                    'filter' => array(
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type
                    ),
                    'select' => array('ID')
        )));

        if ($dbQuota['ID'] > 0) {

            if ($value) {

                \travelsoft\booking\stores\Quotas::update($dbQuota['ID'], array('UF_STOP' => 1));
                $result = 1;
            } else {

                \travelsoft\booking\stores\Quotas::update($dbQuota['ID'], array('UF_STOP' => 0));
                $result = 0;
            }
        }

        return $result;
    }

    /**
     * Обрабатывает запрос на изменение цен брутто и возвращает результат
     * @param string $timestamp
     * @param int $rate_id
     * @param int $ptid
     * @param float $value
     * @param int $service_id
     * @param string $service_type
     * @return float
     */
    public static function _processGrossPrices(string $timestamp, int $rate_id, int $ptid, float $value, int $service_id, string $service_type): float {

        $result = 0;

        $dbPrice = current(\travelsoft\booking\stores\Prices::get(array(
                    'filter' => array(
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type,
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_PRICE_TYPE_ID' => $ptid,
                        'UF_RATE_ID' => $rate_id
                    ),
                    'select' => array('ID', 'UF_GROSS', 'UF_NETTO')
        )));

        if ($dbPrice['ID']) {

            if ($value <= 0 && $dbPrice['UF_NETTO'] <= 0) {

                \travelsoft\booking\stores\Prices::delete($dbPrice['ID']);
                $result = 0;
            } else {

                \travelsoft\booking\stores\Prices::update($dbPrice['ID'], array('UF_GROSS' => $value));
                $result = $value;
            }
        } else {

            if ($value > 0) {

                \travelsoft\booking\stores\Prices::add(array(
                    'UF_SERVICE_ID' => $service_id,
                    'UF_SERVICE_TYPE' => $service_type,
                    'UF_PRICE_TYPE_ID' => $ptid,
                    'UF_RATE_ID' => $rate_id,
                    'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                    'UF_GROSS' => $value
                ));
                $result = $value;
            }
        }

        return $result;
    }

    /**
     * Обрабатывает запрос на изменение цен нетто и возвращает результат
     * @param string $timestamp
     * @param int $rate_id
     * @param int $ptid
     * @param float $value
     * @param int $service_id
     * @param string $service_type
     * @return float
     */
    public static function _processNettoPrices(string $timestamp, int $rate_id, int $ptid, float $value, int $service_id, string $service_type): float {

        $result = 0;

        $dbPrice = current(\travelsoft\booking\stores\Prices::get(array(
                    'filter' => array(
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type,
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                        'UF_PRICE_TYPE_ID' => $ptid,
                        'UF_RATE_ID' => $rate_id
                    ),
                    'select' => array('ID', 'UF_NETTO', 'UF_GROSS')
        )));

        if ($dbPrice['ID']) {

            if ($value <= 0 && $dbPrice['UF_GROSS'] <= 0) {

                \travelsoft\booking\stores\Prices::delete($dbPrice['ID']);
                $result = 0;
            } else {

                \travelsoft\booking\stores\Prices::update($dbPrice['ID'], array('UF_NETTO' => $value));
                $result = $value;
            }
        } else {

            if ($value > 0) {

                \travelsoft\booking\stores\Prices::add(array(
                    'UF_SERVICE_ID' => $service_id,
                    'UF_SERVICE_TYPE' => $service_type,
                    'UF_PRICE_TYPE_ID' => $ptid,
                    'UF_RATE_ID' => $rate_id,
                    'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamp),
                    'UF_NETTO' => $value
                ));
                $result = $value;
            }
        }

        return $result;
    }

    /**
     * Обработка запроса формы настроек
     * @param array $request
     * @param array $userSettings
     * @return array
     */
    public static function processOptionsFromRequest(array $request): array {

        $errors = null;

        if (!$_REQUEST["service_type"]) {
            $errors[] = "Выберите тип объекта для заполнения цен";
        }

        if (!$_REQUEST["service_id"]) {
            $errors[] = "Выберите объект для заполнения цен";
        }

        if (!strlen($_REQUEST["date_from"])) {
            $errors[] = "Выберите 'дату с' для заполнения цен";
        }

        if (!strlen($_REQUEST["date_to"])) {
            $errors[] = "Выберите 'дату по' для заполнения цен";
        }

        $result = array(
            "errors" => $errors,
            "html" => null
        );
        if (empty($errors)) {
            $result = self::getPriceAndQuotasTableAsHtml(
                            (int) $_REQUEST["service_id"], (string) $_REQUEST["service_type"], (string) $_REQUEST["date_from"], (string) $_REQUEST["date_to"]);
        }

        return $result;
    }

    /**
     * Возвращает html таблицы цен и квот
     * @param int $service_id
     * @param string $service_type
     * @param string $date_from
     * @param string $date_to
     * @return array
     */
    public static function getPriceAndQuotasTableAsHtml(int $service_id, string $service_type, string $date_from, string $date_to): array {

        $arrFilterRate = array("UF_SERVICES_TYPE" => $service_type, "UF_SERVICES" => array($service_id));
        if ($service_type === "rooms") {
            $arRoom = current(\travelsoft\booking\stores\Rooms::get(array("filter" => array("ID" => $service_id))));
            if ($arRoom["ID"] > 0 && $arRoom["UF_PLACEMENT"] > 0) {
                $arrFilterRate = array(array(
                        "LOGIC" => "OR",
                        array(
                            "UF_SERVICES_TYPE" => "placements",
                            "UF_SERVICES" => null
                        ),
                        array(
                            "UF_SERVICES_TYPE" => "placements",
                            "UF_SERVICES" => array($arRoom["UF_PLACEMENT"])
                        ),
                        array(
                            "UF_SERVICES_TYPE" => $service_type,
                            "UF_SERVICES" => array($service_id)
                        )
                ));
            }
        }

        $arr_rates = \travelsoft\booking\stores\Rates::get(array(
                    "filter" => $arrFilterRate
        ));

        if (!empty($arr_rates)) {
            $header = array(
                array(
                    "id" => "id",
                    "align" => "center",
                    "content" => '<input type="hidden" value="' . $service_id . '" name="prices_and_quotas[service_id]"><input type="hidden" value="' . $service_type . '" name="prices_and_quotas[service_type]">',
                    "default" => true
            ));

            $timestamps = $dates = array();

            $timestamp = (int) strtotime($date_from);
            $date_to_timestamp = (int) strtotime($date_to);

            $step = 86400;

            while ($timestamp <= $date_to_timestamp) {

                $timestamps[] = $timestamp;
                $dates[] = date(\travelsoft\booking\Settings::DATE_FORMAT, $timestamp);
                $timestamp += $step;
            }


            foreach ($dates as $key => $date) {

                $header[] = array(
                    "id" => $timestamps[$key],
                    "align" => "center",
                    "content" => $date,
                    "default" => true
                );
            }

            $tableId = "add_pq";
            $list = new \CAdminList($tableId, null);

            $list->AddHeaders($header);

            self::_setQuotasSoldStopSaleRows($timestamps, $list, $service_id, $service_type);
            if ($service_type !== "transfer" && $service_type !== "transferback" && $service_type !== "excursiontour") {
                self::_setDurationRows($timestamps, $list, $service_id, $service_type);
            }

            foreach ($arr_rates as $arr_rate) {
                $row = $list->AddRow();
                $row->AddViewField("id", '<span class="rate-name">' . $arr_rate["UF_NAME"] . "<br>(" . $arr_rate["UF_CURRENCY"] . ")" . ($arr_rate["UF_BY_PERIOD"] > 0 ? "<br><small style='color: red'><i>цены вносятся за период ".$arr_rate['UF_DURATION']." ночей !!!</i></small>" : "") . '</span>');
                self::_setPriceTypesRow($timestamps, $list, $service_id, $service_type, (int) $arr_rate["ID"], $arr_rate["UF_PRICE_TYPES"]);
            }

            ob_start();

            $list->DisplayList();

            $html = preg_replace('#(onsubmit=\"(.*)\;\")#', 'onsubmit="return Travelsoft.priceAndQuotasFormAjaxSubmit(this)"', preg_replace('#\?mode=frame#', '', ob_get_contents()));

            ob_end_clean();

            return array("errors" => null, "html" => $html);
        } else {
            return array("errors" => array('Не найдено тарифов для данного объекта. Пожалуйста, <a target="_blank" href="'.Settings::RATES_LIST_URL.'">добавьте тарифы</a>.'), "html" => null);
        }
    }

    /**
     * @param array $timestamps
     * @param \CAdminList $list
     * @param int $service_id
     * @param string $service_type
     * @param int $rate_id
     * @param array $pricetypes_id
     */
    public static function _setPriceTypesRow(array $timestamps, & $list, int $service_id, string $service_type, int $rate_id, array $pricetypes_id) {

        $priceTypes = \travelsoft\booking\stores\PriceTypes::get(array(
                    "filter" => array(
                        "ID" => $pricetypes_id
                    )
        ));

        $prices = array();
        foreach (\travelsoft\booking\stores\Prices::get(array('filter' => array(
                'UF_SERVICE_ID' => $service_id,
                'UF_SERVICE_TYPE' => $service_type,
                'UF_RATE_ID' => $rate_id,
                '><UF_DATE' => array(
                    \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[0]),
                    \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[count($timestamps) - 1])
                )
    ))) as $price) {
            $prices[$price['UF_PRICE_TYPE_ID']][$price['UF_DATE']->getTimestamp()] = $price;
        }

        foreach ($priceTypes as $priceType) {

            $formGrossElements = $formNettoElements = self::_getMassEditHiddenFormElements($timestamps, $service_id, $service_type);

            $formGrossElements[] = array(
                'label' => $priceType["UF_NAME"] . " (цена брутто)",
                'element' => 'input',
                'type' => 'text',
                'value' => '',
                'name' => 'prices_and_quotas[mass_edit][prices][gross][' . $rate_id . '][' . $priceType['ID'] . ']'
            );

            $formNettoElements[] = array(
                'label' => $priceType["UF_NAME"] . " (цена нетто)",
                'element' => 'input',
                'type' => 'text',
                'value' => '',
                'name' => 'prices_and_quotas[mass_edit][prices][netto][' . $rate_id . '][' . $priceType['ID'] . ']'
            );

            $rowGrossData["id"] = "<b>" . $priceType["UF_NAME"] . "</b><br><span style='color: red'>(цена брутто)</span><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formGrossElements, 'prices-gross-mass-edit-' . $rate_id . "-" . $priceType['ID']), 'prices-gross-mass-edit-' . $rate_id . "-" . $priceType['ID']) . ")'>Изменить</a>]";
            $rowNettoData["id"] = "<b>" . $priceType["UF_NAME"] . "</b><br><span style='color: red'>(цена нетто)</span><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formNettoElements, 'prices-netto-mass-edit-' . $rate_id . "-" . $priceType['ID']), 'prices-netto-mass-edit-' . $rate_id . "-" . $priceType['ID']) . ")'>Изменить</a>]";

            foreach ($timestamps as $timestamp) {

                $rowGrossData[$timestamp] = $prices[$priceType['ID']][$timestamp]['UF_GROSS'] > 0 ? ($prices[$priceType['ID']][$timestamp]['UF_GROSS'] == \travelsoft\booking\Settings::FLOAT_NULL ? 0 : (float) $prices[$priceType['ID']][$timestamp]['UF_GROSS']) : null;
                $rowNettoData[$timestamp] = $prices[$priceType['ID']][$timestamp]['UF_NETTO'] > 0 ? ($prices[$priceType['ID']][$timestamp]['UF_NETTO'] == \travelsoft\booking\Settings::FLOAT_NULL ? 0 : (float) $prices[$priceType['ID']][$timestamp]['UF_NETTO']) : null;
            }

            self::_setViewField($list->addRow($rowGrossData["id"], $rowGrossData), $rowGrossData, '<input data-group="#key#" class="paint" onblur="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" type="text" name="prices_and_quotas[prices][gross][' . $rate_id . '][' . $priceType['ID'] . '][#key#]" value="#value#" size="6">');
            self::_setViewField($list->addRow($rowNettoData["id"], $rowNettoData), $rowNettoData, '<input data-group="#key#" class="paint" onblur="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" type="text" name="prices_and_quotas[prices][netto][' . $rate_id . '][' . $priceType['ID'] . '][#key#]" value="#value#" size="6">');
        }
    }

    /**
     * Возвращает контент формы массового редактирования
     * @global \CMain $APPLICATION
     * @param array $formElements
     * @param string $formid
     * @return string
     */
    public static function _getMassEditPopupFormHtml(array $formElements, string $formid): string {

        global $APPLICATION;


        $content = '<form id="' . $formid . '" method="post" action="' . $APPLICATION->GetCurPage("lang=" . LANG, array('lang')) . '">';

        $content .= '<input name="sessid" value="' . bitrix_sessid() . '" type="hidden">';

        foreach ($formElements as $formElement) {

            switch ($formElement['element']) {

                case 'input':

                    if ($formElement['label']) {

                        $content .= '<label for="' . $formElement['name'] . '"><b>' . $formElement['label'] . '</b>: </label>';
                    }
                    $content .
                            $content .= '<input ' . ($formElement['checked'] ? 'checked=""' : '') . ' type="' . $formElement['type'] . '" value="' . $formElement['value'] . '" name="' . $formElement['name'] . '">';
                    break;

                case 'select':

                    if ($formElement['label']) {

                        $content .= '<label for="' . $formElement['name'] . '"><b>' . $formElement['label'] . '</b>: </label>';
                    }

                    $content .= '<select name="' . $formElement['name'] . '">';

                    foreach ($formElement['value'] as $params) {

                        $content .= '<option value="' . $params['value'] . '" ' . ($params['selected'] ? 'selected=""' : '') . '>' . $params['title'] . '</option>';
                    }

                    $content .= '</selected>';

                    break;
            }
        }

        $content .= '</form>';

        return $content;
    }

    /**
     * @param array $timestamps
     * @param \CAdminList $list
     * @param int $service_id
     * @param string $service_type
     */
    public static function _setDurationRows(array $timestamps, & $list, int $service_id, string $service_type) {

        $durations = array();
        foreach (\travelsoft\booking\stores\Duration::get(
                array('filter' => array(
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type,
                        '><UF_DATE' => array(
                            \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[0]),
                            \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[count($timestamps) - 1])
                        )))
        ) as $duration) {
            $durations[$duration['UF_DATE']->getTimestamp()] = $duration;
        }

        $durationRowData = array();

        $formElements = self::_getMassEditHiddenFormElements($timestamps, $service_id, $service_type);

        $formElements[] = array(
            'label' => "Продолжительность (ночей)",
            'element' => 'input',
            'type' => 'text',
            'value' => '',
            'name' => 'prices_and_quotas[mass_edit][duration][value]'
        );

        $durationRowData["id"] = "<b>Продолжительность (ночей)</b><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formElements, 'duration-mass-edit'), 'duration-mass-edit') . ")'>Изменить</a>]";

        foreach ($timestamps as $timestamp) {

            $durationRowData[$timestamp] = !empty($durations[$timestamp]['UF_DURATION']) ? implode(",", $durations[$timestamp]['UF_DURATION']) : null;
        }

        self::_setViewField($list->addRow($durationRowData["id"], $durationRowData), $durationRowData, '<input data-group="#key#" class="paint" onblur="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" type="text" name="prices_and_quotas[duration][#key#]" value="#value#" size="3">');
    }

    /**
     * @param array $timestamps
     * @param \CAdminList $list
     * @param int $service_id
     * @param string $service_type
     */
    public static function _setQuotasSoldStopSaleRows(array $timestamps, & $list, int $service_id, string $service_type) {

        $inc_transfer_quota = $service_type === "transfer" || $service_type === "transferback";

        $quotas = array();

        foreach (\travelsoft\booking\stores\Quotas::get(
                array('filter' => array(
                        'UF_SERVICE_ID' => $service_id,
                        'UF_SERVICE_TYPE' => $service_type,
                        '><UF_DATE' => array(
                            \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[0]),
                            \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[count($timestamps) - 1])
                        )))
        ) as $quota) {
            $quotas[$quota["UF_DATE"]->getTimestamp()] = $quota;
        }

        $quotasRowData = $stopSalesRowData = $soldRowData = $onSaleRowData = array();

        $formElements = self::_getMassEditHiddenFormElements($timestamps, $service_id, $service_type);

        $formElements[] = array(
            'label' => "Квоты",
            'element' => 'input',
            'type' => 'text',
            'value' => '',
            'name' => 'prices_and_quotas[mass_edit][quotas][value]'
        );

        $quotasRowData["id"] = "<b>Квоты</b><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formElements, 'quotas-mass-edit'), 'quotas-mass-edit') . ")'>Изменить</a>]";

        if ($inc_transfer_quota) {

            $transfer_quotas = array();

            foreach (\travelsoft\booking\stores\TransferQuotas::get(
                    array('filter' => array(
                            'UF_SERVICE_ID' => $service_id,
                            'UF_SERVICE_TYPE' => $service_type,
                            '><UF_DATE' => array(
                                \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[0]),
                                \travelsoft\booking\adapters\Date::createFromTimestamp((int) $timestamps[count($timestamps) - 1])
                            )))
            ) as $quota) {

                $transfer_quotas[$quota["UF_DATE"]->getTimestamp()] = $quota;
            }

            $transferQuotasRowData = array();

            array_pop($formElements);
            $formElements[] = array(
                'label' => "Квоты, выделяемые под продажу билетов",
                'element' => 'input',
                'type' => 'text',
                'value' => '',
                'name' => 'prices_and_quotas[mass_edit][transfer_quotas][value]'
            );

            $transferQuotasRowData["id"] = "<b>Квоты под продажу билетов</b><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formElements, 'transfer-quotas-mass-edit'), 'transfer-quotas-mass-edit') . ")'>Изменить</a>]";
        }

        array_pop($formElements);
        $formElements[] = array(
            'label' => "Stop sale",
            'element' => 'select',
            'value' => array(
                array(
                    'value' => 0,
                    'title' => 'Нет'
                ),
                array(
                    'value' => 1,
                    'title' => 'Да'
                )
            ),
            'name' => 'prices_and_quotas[mass_edit][stop_sale][value]'
        );

        $stopSalesRowData["id"] = "<b>Stop sale</b><br>[<a href='javascript: Travelsoft.initPopupForm(" . self::_getMassEditPopupJsonSettings(self::_getMassEditPopupFormHtml($formElements, 'stop-sale-mass-edit'), 'stop-sale-mass-edit') . ")'>Изменить</a>]";

        if ($inc_transfer_quota) {
            $soldRowData["id"] = "Кол-во проданых мест / из них билетов";
            $onSaleRowData["id"] = "В продаже мест / из них билетов";
        } else {
            $soldRowData["id"] = "Кол-во проданых";
            $onSaleRowData["id"] = "В продаже";
        }



        if ($inc_transfer_quota) {
            foreach ($timestamps as $timestamp) {

                $quotasRowData[$timestamp] = !is_null($quotas[$timestamp]['UF_QUOTA']) && $quotas[$timestamp]['UF_QUOTA'] >= 0 ? intVal($quotas[$timestamp]['UF_QUOTA']) : null;
                $transferQuotasRowData[$timestamp] = !is_null($transfer_quotas[$timestamp]['UF_QUOTA']) && $transfer_quotas[$timestamp]['UF_QUOTA'] >= 0 ? intVal($transfer_quotas[$timestamp]['UF_QUOTA']) : null;
                $stopSalesRowData[$timestamp] = boolval($quotas[$timestamp]['UF_STOP']);

                $sold_quota = $quotas[$timestamp]['UF_SOLD_NUMBER'] > 0 ? intVal($quotas[$timestamp]['UF_SOLD_NUMBER']) : 0;
                $sold_transfer_quota = $transfer_quotas[$timestamp]['UF_SOLD_NUMBER'] > 0 ? intVal($transfer_quotas[$timestamp]['UF_SOLD_NUMBER']) : 0;
                $soldRowData[$timestamp] = $sold_quota . "/" . $sold_transfer_quota;

                $on_sale_quota = $quotasRowData[$timestamp] - $sold_quota;
                if ($on_sale_quota < 0) {
                    $on_sale_quota = 0;
                }
                $on_sale_transfer_quota = $transferQuotasRowData[$timestamp] - $sold_transfer_quota;
                if ($on_sale_transfer_quota < 0) {
                    $on_sale_transfer_quota = 0;
                }
                $onSaleRowData[$timestamp] = $on_sale_quota . "/" . $on_sale_transfer_quota;
            }
        } else {
            foreach ($timestamps as $timestamp) {

                $quotasRowData[$timestamp] = !is_null($quotas[$timestamp]['UF_QUOTA']) && $quotas[$timestamp]['UF_QUOTA'] >= 0 ? intVal($quotas[$timestamp]['UF_QUOTA']) : null;
                $stopSalesRowData[$timestamp] = boolval($quotas[$timestamp]['UF_STOP']);
                $soldRowData[$timestamp] = $quotas[$timestamp]['UF_SOLD_NUMBER'] > 0 ? intVal($quotas[$timestamp]['UF_SOLD_NUMBER']) : 0;
                $onSaleRowData[$timestamp] = $quotasRowData[$timestamp] - $soldRowData[$timestamp];
                if ($onSaleRowData[$timestamp] < 0) {

                    $onSaleRowData[$timestamp] = 0;
                }
            }
        }

        self::_setViewField($list->addRow($quotasRowData["id"], $quotasRowData), $quotasRowData, '<input data-group="#key#" class="paint" onblur="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" type="text" name="prices_and_quotas[quotas][#key#]" value="#value#" size="3">');

        if ($inc_transfer_quota) {
            self::_setViewField($list->addRow($transferQuotasRowData["id"], $transferQuotasRowData), $transferQuotasRowData, '<input data-group="#key#" class="paint" onblur="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" type="text" name="prices_and_quotas[transfer_quotas][#key#]" value="#value#" size="3">');
        }
        self::_setViewField($list->addRow($soldRowData["id"], $soldRowData), $soldRowData, '<span data-group="#key#" class="paint" id="sold-#key#">#value#</span>');
        self::_setViewField($list->addRow($onSaleRowData["id"], $onSaleRowData), $onSaleRowData, '<span data-group="#key#" class="paint" id="on-sale-#key#">#value#</span>');
        self::_setViewField($list->addRow($stopSalesRowData["id"], $stopSalesRowData), $stopSalesRowData, '<select data-group="#key#" class="paint" onchange="Travelsoft.triggerPriceAndQuotasFormAjaxSubmit(this);" name="prices_and_quotas[stop_sale][#key#]"><option #selected-no# value="0">Нет</option><option #selected-yes# value="1">Да</option></select>');
    }

    /**
     * Возвращает массив скрытых полей формы массового редактирования цен и квот
     * @param array $timestamps
     * @param int $service_id
     * @return array
     */
    public static function _getMassEditHiddenFormElements(array $timestamps, int $service_id, string $service_type): array {

        foreach ($timestamps as $timestamp) {

            $formElements[] = array(
                'element' => 'input',
                'type' => 'hidden',
                'value' => $timestamp,
                'name' => 'prices_and_quotas[mass_edit][unix_dates][]'
            );
        }

        $formElements[] = array(
            'element' => 'input',
            'type' => 'hidden',
            'name' => 'prices_and_quotas[service_id]',
            'value' => $service_id
        );

        $formElements[] = array(
            'element' => 'input',
            'type' => 'hidden',
            'name' => 'prices_and_quotas[service_type]',
            'value' => $service_type
        );

        return $formElements;
    }

    public static function _getMassEditPopupJsonSettings(string $content, string $formid): string {

        $massEditPopupJsonSettings = \Bitrix\Main\Web\Json::encode(array(
                    'id' => $formid,
                    'title' => 'Форма массового редактирования',
                    'content' => $content,
                    'height' => 100,
                    'buttons' => array(
                        '<input onclick="return Travelsoft.massEditFormAjaxSubmit(\'' . $formid . '\')" type="button" name="savebtn" value="Сохранить" id="savebtn" class="adm-btn-save">',
                        '[code]BX.CDialog.prototype.btnCancel[code]',
                    )
        ));

        # clear [code]
        $massEditPopupJsonSettings = str_replace('"[code]', '', $massEditPopupJsonSettings);
        $massEditPopupJsonSettings = str_replace('[code]"', '', $massEditPopupJsonSettings);

        return $massEditPopupJsonSettings;
    }

    /**
     * @param \CAdminListRow $row
     * @param array $rowData
     * @param string $template
     */
    public static function _setViewField(&$row, array $rowData, string $template) {

        $row->AddViewField("id", $rowData["id"]);

        unset($rowData["id"]);

        foreach ($rowData as $key => $value) {

            $selected_no = 'selected=""';
            $selected_yes = '';
            if ($value) {

                $selected_no = '';
                $selected_yes = 'selected=""';
            }

            $checked = '';
            if ($value) {
                $checked = 'checked=""';
            }

            $row->AddViewField($key, str_replace(array("#key#", "#value#", "#checked#", "#selected-no#", "#selected-yes#"), array($key, $value, $checked, $selected_no, $selected_yes), $template));
        }
    }
    
    /**
     * 
     * @param string $duration
     * @return array|null
     */
    public static function _preparingDurataion(string $duration) {

        $result = null;

        if ($duration) {
            $expDuration = explode(",", str_replace(" ", "", $duration));
            for ($i = 0, $cnt = count($expDuration); $i < $cnt; $i++) {

                if (strpos($expDuration[$i], "-") !== false) {

                    $tmpRes = explode("-", $expDuration[$i]);
                    if ($tmpRes[1] - $tmpRes[0] >= 1) {
                        for ($j = $tmpRes[0]; $j <= $tmpRes[1]; $j++) {
                            $result[] = (int) $j;
                        }
                    }
                    continue;
                }
                $result[] = (int) $expDuration[$i];
            }

            $result = array_unique($result);
            sort($result);
        }
        return $result;
    }

}
