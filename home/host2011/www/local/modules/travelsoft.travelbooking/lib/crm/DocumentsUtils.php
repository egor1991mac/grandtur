<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с документами
 *
 * @author dimabresky
 * @copyright (c) 2017, dimabresky
 */
class DocumentsUtils extends Utils {

    /**
     * Возвращает HTML полей для формы редактирования документов
     * @param array $data
     * @return string
     */
    public static function getDocumentFieldsContent(array $data): string {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::documentsStoreId(), $data);

        $isFormRequest = self::isEditFormRequest();

        $content = '';

        foreach ($arUserFields as $arUserField) {

            $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
        }

        return $content;
    }
    
    /**
     * @param int $createContent
     * @return string
     */
    public static function getDocumentsForPrintContent(bool $createContent = false) {

        if ($createContent) {
            $content = self::getEditFieldHtml("Шаблон документа:", \SelectBoxFromArray("DOCTPL", self::getReferencesSelectData(\travelsoft\booking\stores\Documents::get(array("select" => array('ID', 'UF_NAME'))), "UF_NAME", "ID"), '', "", 'onchange="CRMUtils.buildDocLink(' . $orderId . ')"', false, "find_form"));
            $content .= self::getEditFieldHtml("Формат документа:", \SelectBoxFromArray("DOCFORMAT", self::getReferencesSelectData(array(
                                        array("name" => "docx", "value" => "docx"),
                                        array("name" => "pdf", "value" => "pdf")
                                            ), "name", "value"), '', "", 'onchange="CRMUtils.buildDocLink(' . $orderId . ')"', false, "find_form"));
            $content .= self::getEditFieldHtml("", '<span id="link-container"></span>');
            return $content;
        }

        return "Необходимо создать путевку";
    }

    /**
     * Обработка формы добавления.редактирования документа
     * @global \travelsoft\booking\crm\type $USER_FIELD_MANAGER
     * @return type
     */
    public static function processingDocumentEditForm() {

        $url = Settings::DOCUMENTS_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            
            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::documentsStoreId(), array());

            Validator::stringLessThenTwo($arSave['UF_NAME'], 'Название', $arErrors);

            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Documents::update($ID, $arSave);
                } else {

                    $result = \travelsoft\booking\stores\Documents::add($arSave);
                }

                if ($result) {

                    LocalRedirect($url);
                }
            }
        }


        return array('errors' => $arErrors, 'result' => $result);
    }
}
