<?php

namespace travelsoft\booking\packagetour;

/**
 * Калькулятор стоимости
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Calculator extends \travelsoft\booking\abstraction\Calculator {

    /**
     * @var \travelsoft\booking\placements\CalculatorContainer
     */
    protected $_placements_calculator_container = null;

    /**
     * @var \travelsoft\booking\travel\CalculatorContainer
     */
    protected $_travel_calculator_container = null;

    /**
     * @var \travelsoft\booking\travelback\CalculatorContainer
     */
    protected $_travelback_calculator_container = null;

    /*
     * @var \travelsoft\booking\adapters\CurrencyConverter()
     */
    protected $_converter = null;

    /**
     * @var array
     */
    protected $_grouped_package_tours_id = null;

    /**
     * @var array
     */
    protected $_tourservices = null;

    /**
     * @var array
     */
    protected $_markup = null;

    /**
     * @var array 
     */
    protected $_discount = null;
    
    /**
     * @var array 
     */
    protected $_ts_discount = null;
    
    protected $_request = null;

    /**
     * @param \travelsoft\booking\placements\CalculatorContainer $placements_calculator_container
     * @param \travelsoft\booking\travel\CalculatorContainer $transfer_calculator_container
     * @param \travelsoft\booking\travelback\CalculatorContainer $transferback_calculator_container
     * @param array $grouped_package_tours_id
     * @param array $tourservices
     * @param array $markup
     * @param array $discount
     * @param array $ts_discount
     * @param array $arr_grouped_travel_days
     */
    public function __construct(
    \travelsoft\booking\placements\CalculatorContainer $placements_calculator_container, \travelsoft\booking\transfer\CalculatorContainer $transfer_calculator_container, \travelsoft\booking\transferback\CalculatorContainer $transferback_calculator_container, array $grouped_package_tours_id, array $tourservices, array $markup, array $discount, array $ts_discount, array $arr_grouped_travel_days, $request = null) {
        parent::__construct();
        $this->_placements_calculator_container = $placements_calculator_container;
        $this->_transfer_calculator_container = $transfer_calculator_container;
        $this->_transferback_calculator_container = $transferback_calculator_container;
        $this->_grouped_package_tours_id = $grouped_package_tours_id;
        $this->_tourservices = $tourservices;
        $this->_markup = $markup;
        $this->_discount = $discount;
        $this->_ts_discount = $ts_discount;
        $this->_arr_grouped_travel_days = $arr_grouped_travel_days;
        $this->_request = $request;
    }

    /**
     * @return $this
     */
    public function calculating() {

        $arr_placements_calculators = $this->_placements_calculator_container->get();

        $arr_transfer_calculators = $this->_transfer_calculator_container->get();

        $arr_transferback_calculators = $this->_transferback_calculator_container->get();

        foreach ($this->_grouped_package_tours_id as $k => $ar_) {

            $placements_calculation = $arr_placements_calculators[$k]->calculating()->get();
            $transfer_calculation = $arr_transfer_calculators[$k]->calculating()->get();
            $transferback_calculation = $arr_transferback_calculators[$k]->calculating()->get();

            foreach ($ar_ as $placement_id => $ar__) {

                foreach ($ar__ as $transfer_id => $ar___) {

                    foreach ($ar___ as $transferback_id => $package_tour_id) {

                        if (
                                isset($placements_calculation[$placement_id]) && !empty($placements_calculation[$placement_id]) &&
                                isset($transfer_calculation[$transfer_id]) && !empty($transfer_calculation[$transfer_id]) &&
                                isset($transferback_calculation[$transferback_id]) && !empty($transferback_calculation[$transferback_id])
                        ) {


                            foreach ($placements_calculation[$placement_id] as $room_id => $arr_data_grouped_by_rates) {

                                foreach ($arr_data_grouped_by_rates as $room_rate_id => $arr_rooms_price_data) {

                                    foreach ($transfer_calculation[$transfer_id] as $transfer_rate_id => $arr_transfer_price_data) {

                                        $arr_transfer_price_data__ = current($arr_transfer_price_data);

                                        foreach ($transferback_calculation[$transferback_id] as $transferback_rate_id => $arr_transferback_price_data) {

                                            $arr_transferback_price_data__ = current($arr_transferback_price_data);

                                            $gross = $result_price = $this->_converter->convert($arr_transfer_price_data__["gross"], $arr_transferback_price_data__["currency"], $arr_rooms_price_data["currency"]) +
                                                    + $this->_converter->convert($arr_transferback_price_data__["gross"], $arr_transferback_price_data__["currency"], $arr_rooms_price_data["currency"]) + $arr_rooms_price_data["gross"];

                                            $price_item = array(
                                                "placement_id" => $placement_id,
                                                "room_id" => $room_id,
                                                "transfer_id" => $transfer_id,
                                                "transferback_id" => $transferback_id,
                                                "transfer_rate_id" => $transfer_rate_id,
                                                "transferback_rate_id" => $transferback_rate_id,
                                                "placement_rate_id" => $room_rate_id,
                                                "gross" => $gross,
                                                "netto" => 0.00,
                                                "discount_price" => 0.00,
                                                "tourservice_for_adults" => 0.00,
                                                "tourservice_for_children" => 0.00,
                                                "tourservice_currency" => "",
                                                "markup_price" => 0.00,
                                                "markup_currency" => "",
                                                "currency" => $arr_rooms_price_data["currency"],
                                                "result_price" => $result_price,
                                                "date_from_transfer" => $arr_transfer_price_data__["date"],
                                                "date_back_transfer" => $arr_transferback_price_data__["date"],
                                                "date_from" => $arr_rooms_price_data["date_from"],
                                                "date_to" => $arr_rooms_price_data["date_to"],
                                                "travel_days" => $this->_arr_grouped_travel_days[$k]["travel_days"],
                                                "travel_days_back" => $this->_arr_grouped_travel_days[$k]["travel_days_back"],
                                            );

                                            $price_item["netto"] = $this->_converter->convert($arr_transfer_price_data__["netto"], $arr_transfer_price_data__["currency"], $arr_rooms_price_data["currency"]) +
                                                    + $this->_converter->convert($arr_transferback_price_data__["netto"], $arr_transferback_price_data__["currency"], $arr_rooms_price_data["currency"]) + $arr_rooms_price_data["netto"];


                                            $this->_result[$package_tour_id][] = $price_item;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function min() {

        $arr_min_result = array();
        foreach ($this->_result as $service_id => $arr_data_grouped_by_rates) {
            $min = exp(10);
            $arr_min = array();

            foreach ($arr_data_grouped_by_rates as $arr_data) {

                $price = $this->_converter->convert($arr_data["result_price"], $arr_data["currency"]);

                if ($min > $price) {
                    $arr_min = $arr_data;
                    $min = $price;
                }
            }
            if (!empty($arr_min)) {
                $arr_min_result[$service_id] = $arr_min;
            }
        }

        $this->_result = $arr_min_result;
        return $this;
    }

    /**
     * @return $this
     */
    public function addTourservice() {

        $tmp_data = [];

        foreach ($this->_result as $service_id => $arr_grouped_data) {

            $arr_tourservice = $this->_tourservices[$service_id];

            if (!empty($arr_tourservice)) {

                foreach ($arr_tourservice as $city_id => $arr_ts) {

                    $tourservice_for_adults = $arr_ts["for_adults"] * $arr_ts["adults"];
                    $tourservice_for_children = $this->_calculateChilrenTouservice($arr_ts, $this->_request);
                    $tourservice_currency = $arr_ts["currency"];
                    $tourservice_discount_price = $arr_ts["discount"];

                    $tourservice_price = $tourservice_for_adults + $tourservice_for_children;

                    foreach ($arr_grouped_data as $arr_data) {

                        ///////
                        $arr_data["tourservice_currency"] = $arr_ts["currency"];
                        $arr_data["tourservice_for_adults"] = $tourservice_for_adults;
                        $arr_data["tourservice_for_children"] = $tourservice_for_children;
                        $arr_data["tourservice_currency"] = $tourservice_currency;
                        $arr_data["tourservice_discount_price"] = $tourservice_discount_price;

                        if ($tourservice_price > 0) {
                            $arr_data["result_price"] += $this->_converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }

                        $arr_data["city_id"] = $city_id;

                        $tmp_data[$service_id][] = $arr_data;
                    }
                }
            } else {
                $tmp_data[$service_id] = $arr_grouped_data;
            }
        }

        $this->_result = $tmp_data;

        return $this;
    }

    /**
     * @return $this
     */
    public function addMarkup() {

        foreach ($this->_result as $service_id => &$arr_grouped_data) {
            foreach ($arr_grouped_data as &$arr_data) {
                if (isset($this->_markup[$service_id])) {

                    $arr_data["markup_price"] = $this->_markup[$service_id]["price"];
                    $arr_data["markup_currency"] = $this->_markup[$service_id]["currency"];

                    if ($arr_data["markup_price"] > 0) {
                        $arr_data["result_price"] += $this->_converter->convert($arr_data["markup_price"], $arr_data["markup_currency"], $arr_data["currency"]);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function applyDiscount() {

        if (!empty($this->_discount)) {

            if ($this->_discount["UF_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data) {
                    foreach ($arr_grouped_data as &$arr_data) {

                        $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * ($this->_request->adults + $this->_request->children);
                        $arr_data["result_price"] = $arr_data["result_price"] - $this->_discount["UF_VALUE"];
                    }
                }
            } elseif ($this->_discount["UF_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data) {
                    foreach ($arr_grouped_data as &$arr_data) {

                        $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * $arr_data["gross"] / 100;
                        $arr_data["result_price"] = $arr_data["result_price"] - ($this->_discount["UF_VALUE"] * $arr_data["result_price"] / 100);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function applyTSDiscount() {

        if (!empty($this->_discount)) {

            if ($this->_discount["UF_TS_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data) {
                    foreach ($arr_grouped_data as &$arr_data) {

                        $arr_data["tourservice_discount_price"] = ($this->_request->adults + $this->_request->children) * $this->_ts_discount["UF_TS_VALUE"];
                        if ($arr_data["tourservice_discount_price"] > 0) {
                            $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                    }
                }
            } elseif ($this->_discount["UF_TS_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data) {
                    foreach ($arr_grouped_data as &$arr_data) {

                        $arr_data["tourservice_discount_price"] = $this->_ts_discount["UF_TS_VALUE"] * ($arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"]) / 100;
                        if ($arr_data["tourservice_discount_price"] > 0) {
                            $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                    }
                }
            }
        }

        return $this;
    }

}
