<?php

namespace travelsoft\booking\packagetour;

/**
 * Класс-фабрика Calculator
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorFactory {

    public static function create(\travelsoft\booking\Request $request) {

        $arr_filter = $arr_placements = $arr_transfer = $arr_transferback = $arr_transfer_rates = $arr_transfer_back_rates = $arr_grouped_service_id_by_md5hash = $arr_grouped_travel_days = array();

        if (!empty($request->services_id)) {
            $arr_filter["ID"] = $request->services_id;
        } else {
            if (!empty($request->placements_id)) {
                $arr_filter["UF_PLACEMENT"] = $request->placements_id;
            }
            if (!empty($request->transfers_id)) {
                $arr_filter["UF_TRANSFER"] = $request->transfers_id;
            }
            if (!empty($request->transfersback_id)) {
                $arr_filter["UF_TRANSFER_BACK"] = $request->transfersback_id;
            }
            if (!empty($request->transfersrate_id)) {
                $arr_filter["UF_TRANS_RATE"] = $request->transfersrate_id;
            }
            if (!empty($request->transfersback_rates_id)) {
                $arr_filter["UF_TRANS_BACK_RATE"] = $request->transfersback_rates_id;
            }
        }

        $arPackageTours = \travelsoft\booking\stores\PackageTour::get(array(
                    "filter" => $arr_filter
        ));
        
        foreach ($arPackageTours as $id => $arPackageTour) {

            $k = md5($arPackageTour["UF_TRAVEL_DAYS"] . $arPackageTour["UF_TRAVEL_DAYS_BACK"]);

            $arr_grouped_travel_days[$k] = array("travel_days" => $arPackageTour["UF_TRAVEL_DAYS"], "travel_days_back" => $arPackageTour["UF_TRAVEL_DAYS_BACK"]);
            $arr_placements[$k][] = $arPackageTour["UF_PLACEMENT"];
            $arr_transfer[$k][] = $arPackageTour["UF_TRANSFER"];
            $arr_transferback[$k][] = $arPackageTour["UF_TRANSFER_BACK"];
            $arr_transfer_rates = \array_merge($arr_transfer_rates, $arPackageTour["UF_TRANS_RATE"]);
            $arr_transfer_back_rates = \array_merge($arr_transfer_back_rates, $arPackageTour["UF_TRANS_BACK_RATE"]);
            $arr_grouped_service_id_by_md5hash[$k][$arPackageTour["UF_PLACEMENT"]][$arPackageTour["UF_TRANSFER"]][$arPackageTour["UF_TRANSFER_BACK"]] = $id;
        }

        if (!empty($arr_grouped_travel_days)) {
            
            $arPackageToursId = array_keys($arPackageTours);
            
            // поиск туруслуг
            $tourservices = \travelsoft\booking\Utils::findTourservices($arPackageToursId, "packagetour", $request->adults, $request->children, $request->city_id);

            // поиск наценок
            $markup = array();

            $arr_markup = \travelsoft\booking\stores\Markup::get(array(
                        "filter" => array(
                            "UF_SERVICES" => $arPackageToursId,
                            "UF_SERVICE_TYPE" => "packagetour"),
                        "limit" => 1
            ));

            $arr_total_markup = current(\travelsoft\booking\stores\Markup::get(array(
                        "filter" => array(
                            "UF_SERVICES" => false,
                            "UF_SERVICE_TYPE" => "packagetour"),
                        "limit" => 1
            )));

            if (!empty($arr_markup) || !empty($arr_total_markup)) {

                foreach ($arPackageToursId as $service_id) {
                    $key = false;
                    foreach ($arr_markup as $arrr_markup) {
                        $key = array_search($service_id, $arrr_markup["UF_SERVICES"]);
                        if ($key !== false) {
                            $markup[$service_id] = array(
                                "price" => (float) $arrr_markup["UF_PRICE"],
                                "currency" => (string) $arrr_markup["UF_CURRENCY"],
                            );
                            break;
                        }
                    }
                    if ($key === false && !empty($arr_total_markup)) {
                        $markup[$service_id] = array(
                            "price" => (float) $arr_total_markup["UF_PRICE"],
                            "currency" => (string) $arr_total_markup["UF_CURRENCY"]
                        );
                    }
                }
            }

            // поиск скидок
            $discount = array();
            $time = \travelsoft\booking\adapters\Date::createFromTimestamp(time());
            if (\travelsoft\booking\adapters\User::isAgent()) {

                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arPackageToursId,
                                "UF_SERVICE_TYPE" => "packagetour",
                                "UF_DTYPE" => "A",
                                "!UF_VALUE" => false,
                                "UF_AGENT" => \travelsoft\booking\adapters\User::id(),
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));

                if (empty($arr_discount)) {

                    $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                "filter" => array(
                                    "UF_SERVICES" => $arPackageToursId,
                                    "UF_SERVICE_TYPE" => "packagetour",
                                    "UF_DTYPE" => "A",
                                    "!UF_VALUE" => false,
                                    "<=UF_LP_FROM" => $time,
                                    ">=UF_LP_TO" => $time
                                ),
                                "order" => array(
                                    "UF_SORT" => "ASC",
                                    "ID" => "DESC"),
                                "limit" => 1)));

                    if (empty($arr_discount)) {

                        $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                    "filter" => array(
                                        "UF_SERVICES" => $arPackageToursId,
                                        "UF_SERVICE_TYPE" => "packagetour",
                                        "UF_DTYPE" => "PR",
                                        "<=UF_LP_FROM" => $time,
                                        ">=UF_LP_TO" => $time
                                    ),
                                    "order" => array(
                                        "UF_SORT" => "ASC",
                                        "ID" => "DESC"),
                                    "limit" => 1)));
                    }
                }
            } else {
                
                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arPackageToursId,
                                "UF_SERVICE_TYPE" => "packagetour",
                                "UF_DTYPE" => "PR",
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));
            }

            if (!empty($arr_discount)) {
                $discount = $arr_discount;
            }
            
            // поиск скидок на туруслугу
            $ts_discount = $arr_discount = array();
            if (\travelsoft\booking\adapters\User::isAgent()) {

                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arPackageToursId,
                                "UF_SERVICE_TYPE" => "packagetour",
                                "UF_DTYPE" => "A",
                                "!UF_TS_VALUE" => false,
                                "UF_AGENT" => \travelsoft\booking\adapters\User::id(),
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));

                if (empty($arr_discount)) {

                    $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                "filter" => array(
                                    "UF_SERVICES" => $arPackageToursId,
                                    "UF_SERVICE_TYPE" => "packagetour",
                                    "UF_DTYPE" => "A",
                                    "!UF_TS_VALUE" => false,
                                    "<=UF_LP_FROM" => $time,
                                    ">=UF_LP_TO" => $time
                                ),
                                "order" => array(
                                    "UF_SORT" => "ASC",
                                    "ID" => "DESC"),
                                "limit" => 1)));

                    if (empty($arr_discount)) {

                        $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                    "filter" => array(
                                        "UF_SERVICES" => $arPackageToursId,
                                        "UF_SERVICE_TYPE" => "packagetour",
                                        "UF_DTYPE" => "PR",
                                        "!UF_TS_VALUE" => FALSE,
                                        "<=UF_LP_FROM" => $time,
                                        ">=UF_LP_TO" => $time
                                    ),
                                    "order" => array(
                                        "UF_SORT" => "ASC",
                                        "ID" => "DESC"),
                                    "limit" => 1)));
                    }
                }
            } else {
                
                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arPackageToursId,
                                "UF_SERVICE_TYPE" => "packagetour",
                                "UF_DTYPE" => "PR",
                                "!UF_TS_VALUE" => FALSE,
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));
            }

            if (!empty($arr_discount)) {
                $ts_discount = $arr_discount;
            }

            $transfer_calculator_container = new \travelsoft\booking\transfer\CalculatorContainer;
            $placements_calculator_container = new \travelsoft\booking\placements\CalculatorContainer;
            $transferback_calculator_container = new \travelsoft\booking\transferback\CalculatorContainer;

            foreach ($arr_grouped_travel_days as $k => $arr) {

                $date_from = date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($request->date_from) + (86400 * $arr["travel_days"]));
                $date_to = date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($request->date_to) - (86400 * $arr["travel_days_back"]));

                $placements_calculator_container->add(\travelsoft\booking\placements\CalculatorFactory::create(\travelsoft\booking\placements\RequestFactory::create(array(
                                    "services_id" => !empty($arr_placements[$k]) ? $arr_placements[$k] : array(-1),
                                    "rooms_id" => (array) $request->rooms_id,
                                    "rates_id" => (array) $request->placements_rates_id,
                                    "date_from" => $date_from,
                                    "date_to" => $date_to,
                                    "adults" => $request->adults,
                                    "children" => $request->children,
                                    "children_age" => $request->children_age
                        ))), $k);

                $transfer_calculator_container->add(\travelsoft\booking\transfer\CalculatorFactory::create(\travelsoft\booking\transfer\RequestFactory::create(array(
                                    "services_id" => !empty($arr_transfer[$k]) ? $arr_transfer[$k] : array(-1),
                                    "rates_id" => array_unique($arr_transfer_rates),
                                    "date_from" => $request->date_from,
                                    "date_to" => $request->date_from,
                                    "adults" => $request->adults,
                                    "children" => $request->children,
                                    "children_age" => $request->children_age,
                                    "tickets_buy" => false,
                                    "city_id" => $request->city_id
                        ))), $k);

                $transferback_calculator_container->add(\travelsoft\booking\transferback\CalculatorFactory::create(\travelsoft\booking\transferback\RequestFactory::create(array(
                                    "services_id" => !empty($arr_transferback[$k]) ? $arr_transferback[$k] : array(-1),
                                    "rates_id" => array_unique($arr_transfer_back_rates),
                                    "date_from" => $request->date_to,
                                    "date_to" => $request->date_to,
                                    "adults" => $request->adults,
                                    "children" => $request->children,
                                    "children_age" => $request->children_age,
                                    "tickets_buy" => false,
                        ))), $k);
            }
            
            return new Calculator(
                    $placements_calculator_container, $transfer_calculator_container, $transferback_calculator_container, $arr_grouped_service_id_by_md5hash, $tourservices, $markup, $discount, $ts_discount, $arr_grouped_travel_days, $request
            );
        }

        return new Calculator(
                new \travelsoft\booking\placements\CalculatorContainer, new \travelsoft\booking\transfer\CalculatorContainer, new \travelsoft\booking\transferback\CalculatorContainer, array(), array(), array(), array(), array(), array(), null
        );
    }

}
