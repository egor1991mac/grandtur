<?php

namespace travelsoft\booking\rooms;

use travelsoft\booking\Request;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory{

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request): Request {

        $oRequest = parent::create($request);

        return $oRequest;
    }

}
