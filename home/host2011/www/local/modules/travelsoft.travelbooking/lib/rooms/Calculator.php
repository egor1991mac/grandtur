<?php

namespace travelsoft\booking\rooms;

use travelsoft\booking\CalculationTypes;

/**
 * Калькулятор стоимости
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Calculator extends \travelsoft\booking\abstraction\Calculator {

    /**
     * @var null
     */
    protected $_prices_data = null;
    
    /**
     * @param array $prices_data
     * массив сгруппированный данных по услугам
     * для расчета цен
     * [
     *      service_id => [
     *          rate_id => [
     *              "date_from" => дата с
     *              "date_to" => дата по
     *              "prices" => [
     *                  price_type_id => [
     *                      [
     *                          "gross" => цена брутто
     *                          "netto" => цена нетто
     *                      ]
     *                  ]
     *              ],
     *              "seating" => [
     *                  "main_places" => [
     *                      "adults" => количество взрослых на основном месте
     *                      "children" => количество детей на основном месте
     *                      "children_age" => возраст детей
     *                  ],
     *                  "additional_places" => [
     *                      "adults" => количество взрослых на доп. месте
     *                      "children" => количество детей на доп. месте
     *                      "children_age" => возраст детей
     *                  ],
     *                  "without_places" => [
     *                      "adults" => количество взрослых без места
     *                      "children" => количество детей без места
     *                      "children_age" => возраст детей
     *                  ]
     *              ],
     *              "price_types" => массив данных по типам цен из БД
     *              "calc_types" => массив данных по типам расчета из БД
     *              "currency" => валюта
     *          ]
     *      ]
     * ]
     */
    public function __construct(array $prices_data) {
        parent::__construct();
        $this->_prices_data = $prices_data;
    }

    /**
     * @return $this
     */
    public function calculating() {

        $this->_result = $localResult = $localData = array();
        if (!empty($this->_prices_data)) {
            
            foreach ($this->_prices_data as $service_id => $arr_services_data) {
                $localResult = array();
                
                // получаем тарифы с расчетом за период
                $rates_by_period = \travelsoft\booking\stores\Rates::get([
                            'filter' => ['ID' => \array_keys($arr_services_data), 'UF_BY_PERIOD' => 1],
                            'select' => ['ID']
                ]);
                
                foreach ($arr_services_data as $rate_id => $data) {
                    
                    $localData = array(
                        "prices" => null,
                        "gross" => null,
                        "netto" => null,
                        "seating" => $data["seating"],
                        "max_age" => null,
                        "min_age" => null,
                        "price_by_period" => isset($rates_by_period[$rate_id])
                    );
                    
                    foreach ($data["prices"] as $price_type_id => $arr_prices) {

                        $localData["prices"] = $arr_prices;
                        
                        $localData["min_age"] = $data["price_types"][$price_type_id]["UF_MIN_AGE"];
                        $localData["max_age"] = $data["price_types"][$price_type_id]["UF_MAX_AGE"];
                        $calculation_method = $data["calc_types"][$data["price_types"][$price_type_id]["UF_CALC_TYPE"]];

                        if ($calculation_method && $calculation_method !== "") {
                            $localData = CalculationTypes::$calculation_method($localData);
                        }
                    }
                    
                    if ($localData["gross"] > 0 &&
                            intVal($localData["seating"]["main_places"]["adults"]) === 0 &&
                            intVal($localData["seating"]["main_places"]["children"]) === 0 &&
                            intVal($localData["seating"]["additional_places"]["adults"]) === 0 &&
                            intVal($localData["seating"]["additional_places"]["children"]) === 0 &&
                            intVal($localData["seating"]["without_places"]["adults"]) === 0 &&
                            intVal($localData["seating"]["without_places"]["children"]) === 0
                    ) {

                        $this->_result[$service_id][$rate_id] = array(
                            "result_price" => $localData["gross"],
                            "gross" => $localData["gross"],
                            "netto" => $localData["netto"] ?: $localData["gross"],
                            "discount_price" => 0.00,
                            "tourservice_for_adults" => 0.00,
                            "tourservice_for_children" => 0.00,
                            "tourservice_currency" => "",
                            "markup_price" => 0.00,
                            "markup_currency" => "",
                            "currency" => $data["currency"],
                            "date_from" => $data["date_from"],
                            "date_to" => date(\travelsoft\booking\Settings::DATE_FORMAT, strtotime($data["date_to"]) + 86400)
                        );
                    }
                    
                }
            } 
        }
        
        return $this;
    }

    /**
     * @return $this
     */
    public function min() {

        $arr_min_result = array();

        foreach ($this->_result as $room_id => $arr_data_grouped_by_rates) {
            $min = exp(10);
            $arr_min = array();
            foreach ($arr_data_grouped_by_rates as $arr_data) {

                $price = $this->_converter->convert($arr_data["result_price"], $arr_data["currency"]);

                if ($min > $price) {
                    $arr_min = $arr_data;
                    $min = $price;
                }
            }
            if (!empty($arr_min)) {
                $arr_min_result[$room_id] = $arr_min;
            }
        }


        $this->_result = $arr_min_result;

        return $this;
    }

    /**
     * @return $this
     */
    public function addTourservice() {
        return $this;
    }
    
    /**
     * @return $this
     */
    public function addMarkup() {
        return $this;
    }
    
    /**
     * @return $this
     */
    public function applyDiscount() {
        return $this;
    }
    
    /**
     * @return $this
     */
    public function applyTSDiscount() {
        return $this;
    }
}
