<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей скидок
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Discounts extends Highloadblock {

    protected static $storeName = 'discounts';
    
    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {
        
        $stid = parent::getTable();
        
        return parent::get($query, $likeArray, function ($item) use ($callback, $stid) {
            
            if (is_callable($callback)) { $callback($item); }
            
            $readyData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . $stid, $item);
            
            $item["UF_PTYPE"] = $readyData["UF_PTYPE"]["XML_ID"];
            $item["UF_DTYPE"] = $readyData["UF_DTYPE"]["XML_ID"];
            $item["UF_VARIANT"] = $readyData["UF_VARIANT"]["XML_ID"];
            
        });
        
    }
    
}
