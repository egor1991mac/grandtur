<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей истории платежей
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class PaymentHistory extends Highloadblock {

    protected static $storeName = 'paymentHistory';

    /**
     * Возвращает информацию по последнему платежу по заказу
     * @param int $orderId
     * @return array
     */
    public static function getLastPaymentByOrderId(int $orderId): array {

        return (array) current(self::get(array('order' => array('ID' => 'DESC'), 'filter' => array('UF_ORDER_ID' => $orderId),
                            'limit' => 1)));
    }

    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {


        return parent::get($query, $likeArray, function (&$arPaymentHistory) use ($callback) {

                    if ($callback) {
                        $callback($arPaymentHistory);
                    }
                    
                    if (isset($arPaymentHistory["UF_COURSE_INFO"]) && strlen($arPaymentHistory["UF_COURSE_INFO"])) {
                        
                        $arr_course_info = \travelsoft\booking\Utils::sta($arPaymentHistory["UF_COURSE_INFO"]);
                        $arPaymentHistory["CONVERTER"] = new \travelsoft\booking\adapters\CurrencyConverter($arr_course_info["COURSE_ID"], $arr_course_info["COMMISSIONS"]);
                    }
                    
                });
    }

}
