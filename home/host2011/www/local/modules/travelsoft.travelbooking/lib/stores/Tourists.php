<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей статусов заказа
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Tourists extends Highloadblock {

    protected static $storeName = 'tourists';

    /**
     * @param array $query
     * @param boolean $likeArray
     * @param \travelsoft\booking\stores\callable $callback
     * @return array
     */
    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {
        return parent::get($query, $likeArray, function (&$item) use ($callback) {

                    if ($callback) {
                        $callback($item);
                    }

                    if (isset($item["UF_BIRTHDATE"])) {
                        $item["BIRTHDATE"] = $item["UF_BIRTHDATE"]->toString();
                    }

                    if (isset($item["UF_PASS_DATE_ISSUE"])) {
                        $item["PASS_DATE_ISSUE"] = $item["UF_PASS_DATE_ISSUE"]->toString();
                    }

                    $item['FULL_NAME'] = "";
                    $item['FULL_LAT_NAME'] = "";
                    $item['SN_PASSPORT'] = "";
                    $item['ADDRESS'] = "";

                    if (strlen($item['UF_LAST_NAME']) > 0) {
                        $item['FULL_NAME'] .= $item['UF_LAST_NAME'];
                        $item['SHORT_NAME'] .= $item['UF_LAST_NAME'];
                    }

                    if (strlen($item['UF_NAME']) > 0) {
                        $item['FULL_NAME'] .= ' ' . $item['UF_NAME'];
                        $item['SHORT_NAME'] .= ' ' . substr($item['UF_NAME'], 0, 1) . ".";
                    }

                    if (strlen($item['UF_SECOND_NAME']) > 0) {
                        $item['FULL_NAME'] .= ' ' . $item['UF_SECOND_NAME'];
                        $item['SHORT_NAME'] .= ' ' . substr($item['UF_SECOND_NAME'], 0, 1) . ".";
                    }

                    if (strlen($item['UF_LAT_LAST_NAME'])) {
                        $item['FULL_NAME_LAT'] .= $item['UF_LAT_LAST_NAME'];
                    }
                    if (strlen($item['FULL_NAME_LAT']) > 0 && strlen($item['UF_LAT_NAME'])) {
                        $item['FULL_NAME_LAT'] .= ' ' . $item['UF_LAT_NAME'];
                    }



                    $item['UF_PASS_SERIES'] = trim($item['UF_PASS_SERIES']);
                    $item['UF_PASS_NUMBER'] = trim($item['UF_PASS_NUMBER']);
                    if (strlen($item['UF_PASS_SERIES']) > 0) {

                        $item['SN_PASSPORT'] .= $item['UF_PASS_SERIES'];
                        if (strlen($item['SN_PASSPORT']) > 0) {
                            $item['SN_PASSPORT'] .= $item['UF_PASS_NUMBER'];
                        }
                    }
                });
    }

    /**
     * Список имен туристов строкой по списку id
     * @param array $arr_tourists_id_list
     * @return string
     */
    public static function namesListByIdList(array $arr_tourists_id_list): string {

        $arr_tourists_name = array();
        foreach (self::get(array(
            "filter" => array("ID" => array_unique($arr_tourists_id_list))
        )) as $arr_tourist) {

            $arr_tourists_name[] = $arr_tourist["FULL_NAME"];
        }

        return !empty($arr_tourists_name) ? implode(", ", $arr_tourists_name) : "";
    }

}
