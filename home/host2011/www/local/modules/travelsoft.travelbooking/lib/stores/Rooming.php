<?php
namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Список туристов
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Rooming extends Highloadblock{
    
    protected static $storeName = 'rooming';
}
