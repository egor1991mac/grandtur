<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Iblock;

/**
 * Класс для работы с таблицей стран
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Countries extends Iblock{

    protected static $storeName = 'countries';
}
