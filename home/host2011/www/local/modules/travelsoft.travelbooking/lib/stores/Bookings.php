<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей бронировок
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Bookings extends Highloadblock {

    protected static $storeName = 'bookings';

    public static function get(array $query = array(), bool $likeArray = true, callable $callback = null) {
        
        $converter = new \travelsoft\booking\adapters\CurrencyConverter;
        $AC = \travelsoft\booking\Settings::getAccountingCurrency();
        
        return parent::get($query, $likeArray, function (&$arBooking) use ($callback, $converter, $AC) {

                    if ($callback) {
                        $callback($arBooking);
                    }
                    
                    // туруслуга в валюте учета
                    $arBooking["TS_COST_AC"] = 0.00;
                    // отформатированная туруслуга
                    $arBooking["TS_COST_FORMATTED"] = 0.00;
                    // отформатированная туруслуга в валюте учета
                    $arBooking["TS_COST_AC_FORMATTED"] = "";
                    if ($arBooking["UF_TS_PRICE"] > 0) {
                        $arBooking["TS_COST_FORMATTED"] = $converter->convertWithFormatting($arBooking["UF_TS_PRICE"], $arBooking["UF_TS_CURRENCY"], $arBooking["UF_TS_CURRENCY"]);
                        $arBooking["TS_COST_AC"] = $converter->convert($arBooking["UF_TS_PRICE"], $arBooking["UF_TS_CURRENCY"], $AC);
                        $arBooking["TS_COST_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["TS_COST_AC"], $AC, $AC);
                    }
                    
                    // стоимость в валюте учета
                    $arBooking["COST_AC"] = $converter->convert($arBooking["UF_COST"], $arBooking["UF_CURRENCY"], $AC);
                    
                    // отфармотированная стоимость в валюте учета
                    $arBooking["COST_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["COST_AC"], $AC, $AC);
                    
                    // скидка в валюте учета
                    $arBooking["DISCOUNT_AC"] = 0.00;
                    // отформатированная скидка в валюте учета
                    $arBooking["DISCOUNT_AC_FORMATTED"] = 0.00;
                    if ($arBooking["UF_DISCOUNT"] > 0) {
                        $arBooking["DISCOUNT_AC"] = $converter->convert($arBooking["UF_DISCOUNT"], $arBooking["UF_CURRENCY"], $AC);
                        $arBooking["DISCOUNT_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["DISCOUNT_AC"], $AC, $AC);
                    }
                    
                    // наценка в валюте учета
                    $arBooking["MARKUP_AC"] = 0.00;
                    // отформатированная наценка в валюте учета
                    $arBooking["MARKUP_AC_FORMATTED"] = 0.00;
                    if ($arBooking["UF_MARKUP_PRICE"] > 0) {
                        $arBooking["MARKUP_AC"] = $converter->convert($arBooking["UF_MARKUP_PRICE"], $arBooking["UF_MARKUP_CURRENCY"], $AC);
                        $arBooking["MARKUP_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["MARKUP_AC"], $AC, $AC);
                    }
                    
                    // стоимость без учета скидки в валюте счета
                    $arBooking["TOTAL_COST_WITHOUT_DISCOUNT_AC"] = $converter->convert($arBooking["COST_AC"] + $arBooking["TS_COST_AC"] + $arBooking["MARKUP_AC"], $AC, $AC);
                    // отформатированная стоимость без учета скидки в валюте счета
                    $arBooking["TOTAL_COST_WITHOUT_DISCOUNT_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["TOTAL_COST_WITHOUT_DISCOUNT_AC"], $AC, $AC);
                    
                    // общая стоимость в валюте учета
                    $arBooking["TOTAL_COST_AC"] = $converter->convert($arBooking["TOTAL_COST_WITHOUT_DISCOUNT_AC"] - $arBooking["DISCOUNT_AC"], $AC, $AC);
                    // отформатированная общая стоимость в валюте учета
                    $arBooking["TOTAL_COST_AC_FORMATTED"] = $converter->convertWithFormatting($arBooking["TOTAL_COST_AC"], $AC, $AC);
                    
                    // валюта учета
                    $arBooking["ACURRENCY"] = $AC;
                    
                });
    }

    /**
     * Возвращает максимальный id списка заказов
     * @return int
     */
    public static function getLastId() {
        $result = parent::get(array('select' => array(new \Bitrix\Main\Entity\ExpressionField('MAX_ID', 'max(ID)'))), false)->fetch();
        return intVal($result['MAX_ID']);
    }
    
}
