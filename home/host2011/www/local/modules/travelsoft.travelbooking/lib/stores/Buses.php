<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей автобусная база
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Buses extends Highloadblock {

    protected static $storeName = 'buses';
    
    public static function nameById (int $id) : string {
        
        return (string)self::getById($id, array("ID", "UF_NAME"))["UF_NAME"];
    }
}
