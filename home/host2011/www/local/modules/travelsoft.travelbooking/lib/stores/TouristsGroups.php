<?php
namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы со списком групп туристов
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TouristsGroups extends Highloadblock{
    
    protected static $storeName = 'touristsGroups';
}
