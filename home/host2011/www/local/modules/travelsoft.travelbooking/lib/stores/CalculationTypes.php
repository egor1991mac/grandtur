<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей типов расчёта
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class CalculationTypes extends Highloadblock {

    protected static $storeName = 'calculationTypes';

}
