<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Iblock;

/**
 * Класс для работы с таблицей городов
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Cities extends Iblock{

    protected static $storeName = 'cities';
}
