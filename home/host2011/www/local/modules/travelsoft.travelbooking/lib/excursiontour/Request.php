<?php

namespace travelsoft\booking\excursiontour;

/**
 * Класс запроса на поиск проездов
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\Request{
        
    /**
     * @var array
     */
    public $excursions_id = null;
    
    /**
     * @var int
     */
    public $city_id = null;
}
