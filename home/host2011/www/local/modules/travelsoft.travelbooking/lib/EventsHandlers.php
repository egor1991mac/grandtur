<?php

namespace travelsoft\booking;

use travelsoft\booking\crm\Settings as CRMSettings;
use travelsoft\booking\Utils;

/**
 * Класс методов обработки событий
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class EventsHandlers {

    /**
     * Добавляет пункт меню CRM в global меню админ. части
     * @global type $USER
     * @param type $aGlobalMenu
     */
    public static function addGlobalAdminMenuItem(&$arGlobalMenu) {

        global $USER;

        if (crm\Utils::access()) {

            $currentUserGroups = $USER->GetUserGroupArray();

            $arAllMenuItems = [
                [
                    "text" => "Работа менеджеров",
                    "section" => "manager_work",
//                    "url" => CRMSettings::VOUCHERS_LIST_URL . "?lang=" . LANGUAGE_ID,
//                    "more_url" => [
//                        CRMSettings::VOUCHERS_LIST_URL, 
//                        CRMSettings::TOURISTS_GROUPS_LIST_URL,
//                        CRMSettings::TOURISTS_LIST_URL
//                    ],
                    "items_id" => "manager_work",
                    "title" => "Работа менеджеров",
                    "items" => [
                        [
                            "text" => "Заказы",
                            "url" => CRMSettings::VOUCHERS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::VOUCHER_EDIT_URL),
                            "title" => "Заказы",
                        ],
                        [
                            "text" => "Список групп туристов",
                            "url" => CRMSettings::TOURISTS_GROUPS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => [],
                            "title" => "Список групп туристов",
                        ],
                        [
                            "text" => "Туристы",
                            "url" => CRMSettings::TOURISTS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::TOURIST_EDIT_URL),
                            "title" => "Туристы",
                        ]
                    ],
                ],
                [
                    "text" => "Финансы",
                    "section" => "finance",
                    "items_id" => "finance",
//                    "url" => CRMSettings::CASH_DESKS_LIST_URL . "?lang=" . LANGUAGE_ID,
//                    "more_url" => [
//                        CRMSettings::CASH_DESKS_LIST_URL,
//                        CRMSettings::PAYMENTS_TYPES_LIST_URL,
//                        CRMSettings::PAYMENT_HISTORY_LIST_URL,
//                    ],
                    "title" => "Финансы",
                    "items" => [
                        [
                            "text" => "Кассы",
                            "url" => CRMSettings::CASH_DESKS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::CASH_DESK_EDIT_URL),
                            "title" => "Кассы",
                        ],
                        [
                            "text" => "Типы оплаты",
                            "url" => CRMSettings::PAYMENTS_TYPES_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::PAYMENT_TYPE_EDIT_URL),
                            "title" => "Типы оплаты",
                        ],
                        [
                            "text" => "Кассовый учет",
                            "url" => CRMSettings::PAYMENT_HISTORY_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::PAYMENT_HISTORY_EDIT_URL),
                            "title" => "Кассовый учет",
                        ]
                    ]
                ],
                [
                    "text" => "Формирование продукта",
                    "section" => "product",
                    "items_id" => "product",
//                    "url" => CRMSettings::EXCURSION_TOUR_LIST_URL . "?lang=" . LANGUAGE_ID,
//                    "more_url" => [
//                        CRMSettings::PACKAGE_TOUR_LIST_URL,
//                        CRMSettings::EXCURSION_TOUR_LIST_URL,
//                    ],
                    "title" => "Формирование продукта",
                    "items" => [
                        [
                            "text" => "Экскурсионные туры",
                            "url" => CRMSettings::EXCURSION_TOUR_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(
                                CRMSettings::EXCURSION_TOUR_EDIT_URL,
                                //CRMSettings::EXCURSION_TOUR_LOADER_URL,
                            ),
                            "title" => "Экскурсионные туры",
                        ],
                        [
                            "text" => "Пакетные туры",
                            "url" => CRMSettings::PACKAGE_TOUR_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::PACKAGE_TOUR_EDIT_URL),
                            "title" => "Пакетные туры",
                        ],
                    ]
                ],
                [
                    "text" => "Формирование цен",
                    "section" => "prices",
                    "items_id" => "prices",
//                    "url" => CRMSettings::ADD_PRICES_URL . "?lang=" . LANGUAGE_ID,
//                    "more_url" => [
//                        CRMSettings::ADD_PRICES_URL,
//                        CRMSettings::RATES_LIST_URL,
//                        CRMSettings::PRICE_TYPES_LIST_URL,
//                        CRMSettings::DISCOUNTS_LIST_URL,
//                        CRMSettings::MARKUP_LIST_URL,
//                        CRMSettings::TOURSERVICES_LIST_URL,
//                    ],
                    "title" => "Формирование цен",
                    "items" => [
                        [
                            "text" => "Цены и наличие мест",
                            "url" => CRMSettings::ADD_PRICES_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(),
                            "title" => "Цены и наличие мест",
                        ],
                        [
                            "text" => "Тарифы",
                            "url" => CRMSettings::RATES_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::RATES_EDIT_URL),
                            "title" => "Тарифы"
                        ],
                        [
                            "text" => "Типы цен",
                            "url" => CRMSettings::PRICE_TYPES_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::PRICE_TYPES_EDIT_URL),
                            "title" => "Типы цен",
                        ],
                        [
                            "text" => "Комиссии",
                            "url" => CRMSettings::DISCOUNTS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::DISCOUNT_EDIT_URL),
                            "title" => "Комиссии",
                        ],
                        [
                            "text" => "Наценки",
                            "url" => CRMSettings::MARKUP_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::MARKUP_EDIT_URL),
                            "title" => "Наценки",
                        ],
                        [
                            "text" => "Транспортные расходы",
                            "url" => CRMSettings::TOURSERVICES_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::TOURSERVICE_EDIT_URL),
                            "title" => "Транспортные расходы",
                        ]
                    ],
                ],
                [
                    "text" => "Справочники",
                    "section" => "lists",
                    "items_id" => "lists",
//                        "url" => CRMSettings::ADD_PRICES_URL . "?lang=" . LANGUAGE_ID,
//                        "more_url" => [
//                            CRMSettings::ROOMS_LIST_URL,
//                            CRMSettings::BUSES_LIST_URL,
//                            CRMSettings::MANAGERS_LIST_URL,
//                            CRMSettings::AGENTS_LIST_URL,
//                            CRMSettings::DOCUMENTS_URL,
//                        ],
                    "title" => "Формирование цен",
                    "items" => [
                        [
                            "text" => "Номерной фонд",
                            "url" => CRMSettings::ROOMS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::ROOM_EDIT_URL),
                            "title" => "Номерной фонд",
                        ],
                        [
                            "text" => "Автобусная база",
                            "url" => CRMSettings::BUSES_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::BUS_EDIT_URL),
                            "title" => "Автобусная база",
                        ],
                        /*[
                            "text" => "Менеджеры",
                            "url" => CRMSettings::MANAGERS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::MANAGERS_EDIT_URL),
                            "title" => "Менеджеры",
                        ],
                        [
                            "text" => "Агенты",
                            "url" => CRMSettings::AGENTS_LIST_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::AGENT_EDIT_URL),
                            "title" => "Агенты",
                        ],*/
                        [
                            "text" => "Документы",
                            "url" => CRMSettings::DOCUMENTS_URL . "?lang=" . LANGUAGE_ID,
                            "more_url" => array(CRMSettings::DOCUMENT_EDIT_URL),
                            "title" => "Документы",
                        ]
                    ]
                ]
            ];

            /* $arAllMenuItems = array(
              CRMSettings::ROOMS_LIST_URL => array(
              "text" => "Номерной фонд",
              "url" => CRMSettings::ROOMS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::ROOM_EDIT_URL),
              "title" => "Номерной фонд",
              ),
              CRMSettings::BUSES_LIST_URL => array(
              "text" => "Автобусная база",
              "url" => CRMSettings::BUSES_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::BUS_EDIT_URL),
              "title" => "Автобусная база",
              ),
              CRMSettings::AGENTS_LIST_URL => array(
              "text" => "Агенты",
              "url" => CRMSettings::AGENTS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::AGENT_EDIT_URL),
              "title" => "Агенты",
              ),
              CRMSettings::MANAGERS_LIST_URL => array(
              "text" => "Менеджеры",
              "url" => CRMSettings::MANAGERS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::MANAGERS_EDIT_URL),
              "title" => "Менеджеры",
              ),
              CRMSettings::EXCURSION_TOUR_LIST_URL => array(
              "text" => "Экскурсионные туры",
              "url" => CRMSettings::EXCURSION_TOUR_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(
              CRMSettings::EXCURSION_TOUR_EDIT_URL,
              CRMSettings::EXCURSION_TOUR_LOADER_URL,
              ),
              "title" => "Экскурсионные туры",
              ),
              CRMSettings::ADD_PRICES_URL => array(
              "text" => "Цены и наличие мест",
              "url" => CRMSettings::ADD_PRICES_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(),
              "title" => "Цены и наличие мест",
              ),
              CRMSettings::VOUCHERS_LIST_URL => array(
              "text" => "Список путевок",
              "url" => CRMSettings::VOUCHERS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::VOUCHER_EDIT_URL),
              "title" => "Список путевок",
              ),
              CRMSettings::TOURISTS_GROUPS_LIST_URL => array(
              "text" => "Список групп туристов",
              "url" => CRMSettings::TOURISTS_GROUPS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::TOURIST_GROUP_EDIT_URL),
              "title" => "Список групп туристов",
              ),
              CRMSettings::TOURISTS_LIST_URL => array(
              "text" => "Туристы",
              "url" => CRMSettings::TOURISTS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::TOURIST_EDIT_URL),
              "title" => "Туристы",
              ),
              CRMSettings::PAYMENT_HISTORY_LIST_URL => array(
              "text" => "История платежей",
              "url" => CRMSettings::PAYMENT_HISTORY_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::PAYMENT_HISTORY_EDIT_URL),
              "title" => "История платежей",
              ),
              CRMSettings::CASH_DESKS_LIST_URL => array(
              "text" => "Кассы",
              "url" => CRMSettings::CASH_DESKS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::CASH_DESK_EDIT_URL),
              "title" => "Кассы",
              ),
              CRMSettings::PAYMENTS_TYPES_LIST_URL => array(
              "text" => "Типы оплаты",
              "url" => CRMSettings::PAYMENTS_TYPES_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::PAYMENT_TYPE_EDIT_URL),
              "title" => "Типы оплаты",
              ),
              CRMSettings::DISCOUNTS_LIST_URL => array(
              "text" => "Скидки",
              "url" => CRMSettings::DISCOUNTS_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::DISCOUNT_EDIT_URL),
              "title" => "Скидки",
              ),
              CRMSettings::PRICE_TYPES_LIST_URL => array(
              "text" => "Типы цен",
              "url" => CRMSettings::PRICE_TYPES_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::PRICE_TYPES_EDIT_URL),
              "title" => "Типы цен",
              ),
              CRMSettings::RATES_LIST_URL => array(
              "text" => "Тарифы",
              "url" => CRMSettings::RATES_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::RATES_EDIT_URL, CRMSettings::RATES_LIST_URL),
              "title" => "Тарифы",
              ),
              CRMSettings::PACKAGE_TOUR_LIST_URL => array(
              "text" => "Пакетные туры",
              "url" => CRMSettings::PACKAGE_TOUR_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::PACKAGE_TOUR_EDIT_URL),
              "title" => "Пакетные туры",
              ),
              CRMSettings::TOURSERVICES_LIST_URL => array(
              "text" => "Туруслуга",
              "url" => CRMSettings::TOURSERVICES_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::TOURSERVICE_EDIT_URL),
              "title" => "Туруслуга",
              ),
              CRMSettings::MARKUP_LIST_URL => array(
              "text" => "Наценки",
              "url" => CRMSettings::MARKUP_LIST_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::MARKUP_EDIT_URL),
              "title" => "Наценки",
              )
              );

              $arMenuItems = array();

              if ($USER->IsAdmin()) {

              $arMenuItems = $arAllMenuItems;
              $arMenuItems[CRMSettings::DOCUMENTS_URL] = array(
              "text" => "Документы",
              "url" => CRMSettings::DOCUMENTS_URL . "?lang=" . LANGUAGE_ID,
              "more_url" => array(CRMSettings::DOCUMENT_EDIT_URL),
              "title" => "Документы",
              );
              }


              if (in_array(Settings::managersUGroup(), $currentUserGroups)) {

              $arMenuItems[CRMSettings::ROOMS_LIST_URL] = $arAllMenuItems[CRMSettings::ROOMS_LIST_URL];
              $arMenuItems[CRMSettings::BUSES_LIST_URL] = $arAllMenuItems[CRMSettings::BUSES_LIST_URL];
              $arMenuItems[CRMSettings::EXCURSION_TOUR_LIST_URL] = $arAllMenuItems[CRMSettings::EXCURSION_TOUR_LIST_URL];
              $arMenuItems[CRMSettings::ADD_PRICES_URL] = $arAllMenuItems[CRMSettings::ADD_PRICES_URL];
              $arMenuItems[CRMSettings::VOUCHERS_LIST_URL] = $arAllMenuItems[CRMSettings::VOUCHERS_LIST_URL];
              $arMenuItems[CRMSettings::CASH_DESKS_LIST_URL] = $arAllMenuItems[CRMSettings::CASH_DESKS_LIST_URL];
              $arMenuItems[CRMSettings::PAYMENTS_TYPES_LIST_URL] = $arAllMenuItems[CRMSettings::PAYMENTS_TYPES_LIST_URL];
              $arMenuItems[CRMSettings::TOURISTS_LIST_URL] = $arAllMenuItems[CRMSettings::TOURISTS_LIST_URL];
              $arMenuItems[CRMSettings::DISCOUNTS_LIST_URL] = $arAllMenuItems[CRMSettings::DISCOUNTS_LIST_URL];
              $arMenuItems[CRMSettings::PRICE_TYPES_LIST_URL] = $arAllMenuItems[CRMSettings::PRICE_TYPES_LIST_URL];
              $arMenuItems[CRMSettings::RATES_LIST_URL] = $arAllMenuItems[CRMSettings::RATES_LIST_URL];
              $arMenuItems[CRMSettings::PACKAGE_TOUR_LIST_URL] = $arAllMenuItems[CRMSettings::PACKAGE_TOUR_LIST_URL];
              $arMenuItems[CRMSettings::TOURSERVICES_LIST_URL] = $arAllMenuItems[CRMSettings::TOURSERVICES_LIST_URL];
              $arMenuItems[CRMSettings::MARKUP_LIST_URL] = $arAllMenuItems[CRMSettings::MARKUP_LIST_URL];
              } */

            $arGlobalMenu["global_menu_travelsoft_crm"] = array(
                "menu_id" => "travelsoft_booking_crm",
                "text" => "TS: Operator",
                "title" => "TS: Operator",
                "sort" => 500,
                "items_id" => "global_menu_travelsoft_booking_crm",
                "help_section" => "travelsoft_booking_crm",
                "items" => $arAllMenuItems
            );
        }
    }

    /**
     * @param int $id
     */
    public static function onAfterVoucherAdd($id) {
        $arr_voucher = stores\Vouchers::getById($id);
        if (isset($arr_voucher["UF_BOOKINGS"]) && !empty($arr_voucher["UF_BOOKINGS"])) {
            
            foreach (stores\Bookings::get(array(
                "filter" => array("ID" => $arr_voucher["UF_BOOKINGS"])
            )) as $arr_booking) {
                
                if ($arr_booking["UF_SERVICE"] > 0 && strlen($arr_booking["UF_SERVICE_TYPE"])) {
                    $method = "increase" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSold";
                    if (method_exists("\\travelsoft\\booking\\Utils", $method)) {
                        Utils::$method($arr_booking);
                    }
                    
                    // списываем количество проданных билетов
                    $ticketsMethod = "increase" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSoldTickets";
                    if (method_exists("\\travelsoft\\booking\\Utils", $ticketsMethod)) {
                        Utils::$ticketsMethod($arr_booking);
                    }
                }
            }
            if (isset($arr_voucher["UF_CLIENT"]) && $arr_voucher["UF_CLIENT"] > 0) {

                $arr_client = stores\Users::getById((int) $arr_voucher["UF_CLIENT"], array("ID", "EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_PHONE", "UF_LEGAL_NAME"));


                adapters\Mail::sendNewBookingNotificationForClient(array(
                    "EMAIL_TO" => $arr_client["EMAIL"],
                    "VOUCHER_ID" => $arr_voucher["ID"]
                        )
                );

                $arr_booking = current($arr_voucher["BOOKINGS_DETAIL"]);

                $placement_id = (int) $arr_booking["UF_PLACEMENT"];
                if (!$placement_id) {
                    $placement_id = (int) $arr_booking["UF_SERVICE"];
                }

                adapters\Mail::sendNewBookingNotificationForManager(array(
                    "EMAIL_TO" => Settings::emailForNotification(),
                    "VOUCHER_ID" => $arr_voucher["ID"],
                    "CLIENT" => strlen($arr_client["UF_LEGAL_NAME"]) > 0 ? $arr_client["UF_LEGAL_NAME"] : $arr_client["FULL_NAME_WITH_EMAIL"],
                    "TRANSFER" => stores\Buses::nameById((int) $arr_booking["UF_TRANSFER"]) ?: "-",
                    "PLACEMENT" => stores\Placements::nameById($placement_id) ?: "-",
                    "ROOM" => stores\Rooms::nameById((int) $arr_booking["UF_ROOM"]) ?: "-",
                    "RATE" => stores\Rooms::nameById((int) $arr_booking["UF_PLACEMENT_RATE"]) ?: "-",
                    "DATE_FROM_TRANSFER" => $arr_booking["UF_DATE_FROM_TRANS"] ? $arr_booking["UF_DATE_FROM_TRANS"]->toString() : "-",
                    "DATE_BACK_TRANSFER" => $arr_booking["UF_DATE_BACK_TRANS"] ? $arr_booking["UF_DATE_BACK_TRANS"]->toString() : "-",
                    "DATE_FROM_PLACEMENT" => $arr_booking["UF_DATE_FROM_PLACE"] ? $arr_booking["UF_DATE_FROM_PLACE"]->toString() : "-",
                    "DATE_TO_PLACEMENT" => $arr_booking["UF_DATE_TO_PLACE"] ? $arr_booking["UF_DATE_TO_PLACE"]->toString() : "-",
                    "ADULTS" => $arr_booking["UF_ADULTS"] ?: "-",
                    "CHILDREN" => $arr_booking["UF_CHILDREN"] ?: "-",
                    "CHILDREN_AGE" => !empty($arr_booking["UF_CHILDREN_AGE"]) ? implode(", ", $arr_booking["UF_CHILDREN_AGE"]) : "-",
                    "TOURISTS" => stores\Tourists::namesListByIdList($arr_booking["UF_TOURISTS"]),
                    "PHONE" => $arr_client["PERSONAL_PHONE"] ?: "-"
                ));
            }
        }
    }

    /**
     * @param array $parameters
     * @param array $fields
     */
    public static function onBeforeVoucherUpdate($parameters, $fields) {

        $arVoucher = \travelsoft\booking\stores\Vouchers::getById((int) $parameters['ID']);
        if ($arVoucher['UF_STATUS'] != $fields['UF_STATUS']) {
            $GLOBALS["TRAVELBOOKING_STATUS_CHANGED"] = true;
        }
        if (
                $fields['UF_STATUS'] == \travelsoft\booking\Settings::cancellationStatus() &&
                $GLOBALS["TRAVELBOOKING_STATUS_CHANGED"]
        ) {

            // ФЛАГ НА АННУЛЯЦИЮ
            $GLOBALS['TRAVELBOOKING_NEED_CANCELLATION'] = true;
        }
        
        $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_EMBASSY'] = time();
        if (!empty($arVoucher['UF_DATE_EMBASSY'])) {
            $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_EMBASSY'] = $arVoucher['UF_DATE_EMBASSY']->getTimestamp();
        }
        
        $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_DOCS'] = time();
        if (!empty($arVoucher['UF_DATE_DOCS'])) {
            $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_DOCS'] = $arVoucher['UF_DATE_DOCS']->getTimestamp();
        }
    }

    /**
     * @param array $parameters
     */
    public static function onAfterVoucherUpdate($parameters) {

        $arVoucher = stores\Vouchers::getById($parameters['ID']);

        // аннулирование и увеличение количества проданных
        if (isset($GLOBALS['TRAVELBOOKING_NEED_CANCELLATION'])) {

            if ($GLOBALS['TRAVELBOOKING_NEED_CANCELLATION']) {
                if (!empty($arVoucher["UF_BOOKINGS"])) {

                    foreach (stores\Bookings::get(array(
                        "filter" => array("ID" => $arVoucher["UF_BOOKINGS"])
                    )) as $arr_booking) {
                        if ($arr_booking["UF_SERVICE"] > 0 && strlen($arr_booking["UF_SERVICE_TYPE"])) {
                            $method = "decrease" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSold";
                            if (method_exists("\\travelsoft\\booking\\Utils", $method)) {
                                Utils::$method($arr_booking);
                            }

                            $tiketsMethod = "decrease" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSoldTickets";
                            if (method_exists("\\travelsoft\\booking\\Utils", $tiketsMethod)) {
                                Utils::$tiketsMethod($arr_booking);
                            }
                        }
                    }
                }
            }
            unset($GLOBALS['TRAVELBOOKING_NEED_CANCELLATION']);
        }

        if (isset($GLOBALS["TRAVELBOOKING_STATUS_CHANGED"]) &&
                $GLOBALS["TRAVELBOOKING_STATUS_CHANGED"]) {
            // отправка писем о смене статуса
            $status_name = stores\Statuses::nameById($arVoucher["UF_STATUS"]);
            adapters\Mail::sendChangeStatusNotification(array(
                "EMAIL_TO" => stores\Users::getById($arVoucher["UF_CLIENT"])["EMAIL"],
                "VOUCHER_ID" => $arVoucher["ID"],
                "STATUS" => $status_name
            ));

            adapters\Mail::sendChangeStatusNotification(array(
                "EMAIL_TO" => Settings::emailForNotification(),
                "VOUCHER_ID" => $arVoucher["ID"],
                "STATUS" => $status_name
            ));
            unset($GLOBALS["TRAVELBOOKING_STATUS_CHANGED"]);
        }
        
        if (!empty($arVoucher['UF_DATE_EMBASSY']) && $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_EMBASSY'] !== $arVoucher['UF_DATE_EMBASSY']->getTimestamp()) {
            
            adapters\Mail::sendEmbassyDate([
                "TOUR_NAME" => current($arVoucher['BOOKINGS_DETAIL'])['UF_SERVICE_NAME'],
                "DATE" => $arVoucher['UF_DATE_EMBASSY']->toString(),
                "EMAIL_TO" => stores\Users::getById($arVoucher['UF_CLIENT'])['EMAIL']
            ]);
        }
        
        if (!empty($arVoucher['UF_DATE_DOCS']) && $GLOBALS['TRAVELBOOKING_TIMESTAMP_DATE_DOCS'] !== $arVoucher['UF_DATE_EMBASSY']->getTimestamp()) {
            
            adapters\Mail::sendDocumentsDate([
                "TOUR_NAME" => current($arVoucher['BOOKINGS_DETAIL'])['UF_SERVICE_NAME'],
                "DATE" => $arVoucher['UF_DATE_DOCS']->toString(),
                "EMAIL_TO" => stores\Users::getById($arVoucher['UF_CLIENT'])['EMAIL']
            ]);
        }
    }

    /**
     * @param array $parameters
     */
    public static function onBeforeVoucherDelete($parameters) {

        $GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE'] = \travelsoft\booking\stores\Vouchers::getById((int) $parameters["ID"]);
        
    }

    public static function onAfterVoucherDelete() {

        if (
                $GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE']['ID'] > 0
        ) {

            if ($GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE']['UF_STATUS'] != \travelsoft\booking\Settings::cancellationStatus()) {
                // возвращаем количество проданных квот при удалении, при условии что путевка не аннулирована
                foreach (stores\Bookings::get(array(
                    "filter" => array("ID" => $GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE']["UF_BOOKINGS"])
                )) as $arr_booking) {
                    if ($arr_booking["UF_SERVICE"] > 0 && strlen($arr_booking["UF_SERVICE_TYPE"])) {
                        $method = "decrease" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSold";
                        if (method_exists("\\travelsoft\\booking\\Utils", $method)) {
                            Utils::$method($arr_booking);
                        }
                        // возвращаем количество проданных билетов
                        $ticketsMethod = "decrease" . ucfirst($arr_booking["UF_SERVICE_TYPE"]) . "NumberOfSoldTickets";
                        if (method_exists("\\travelsoft\\booking\\Utils", $ticketsMethod)) {
                            Utils::$ticketsMethod($arr_booking);
                        }
                    }
                }
            }

            // очищаем списки бронировок
            foreach ($GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE']["UF_BOOKINGS"] as $bid) {
                stores\Bookings::delete($bid);
            }
        }
        unset($GLOBALS['TRAVELBOOKING_VOUCHER_FIELDS_BEFORE_DELETE']);
    }

    /**
     * @param type $arFields
     */
    public static function onAfterUserRegister(&$arFields) {

        if ($arFields['USER_ID'] > 0) {
            if ((string) $_POST['is_agent'] === 'Y') {
                adapters\Mail::sendAgentRegisterNotification(array("EMAIL_TO" => $arFields["EMAIL"], "ID" => $arFields['USER_ID']));
                adapters\Mail::sendAgentRegisterNotificationForManager(array("EMAIL_TO" => Settings::emailForNotification(), "ID" => $arFields['USER_ID']));

                stores\Users::update($arFields['USER_ID'], array("UF_AGENT_WAITING" => 1));
            }

            (new \CEvent)->SendImmediate("USER_INFO", $arFields['LID'], $arFields);
        }
    }

    /**
     * @param type $arFields
     */
    public static function onBeforeUserUpdate($arFields) {
        $arUser = stores\Users::getById((int) $arFields["ID"], array("UF_AGENT_WAITING"));
        // запоминаем состояние поля UF_AGENT_WAITING
        $GLOBALS["TRAVELBOOKING_UF_AGENT_WAITING"] = (int) $arUser["UF_AGENT_WAITING"];
    }

    /**
     * @param type $arFields
     */
    public static function onAfterUserUpdate($arFields) {

        if (key_exists("TRAVELBOOKING_UF_AGENT_WAITING", $GLOBALS)) {
            // если агент активирован
            if ($GLOBALS["TRAVELBOOKING_UF_AGENT_WAITING"] === 1 && (int) $arFields["UF_AGENT_WAITING"] === 0) {

                adapters\Mail::sendAgentActivated(array("EMAIL_TO" => $arFields["EMAIL"]));
            }
            unset($GLOBALS["TRAVELBOOKING_UF_AGENT_WAITING"]);
        }
    }

}
