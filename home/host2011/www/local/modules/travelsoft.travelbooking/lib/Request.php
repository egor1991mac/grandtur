<?php

namespace travelsoft\booking;

/**
 * Класс запроса на поиск туров
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request {
    
    /**
     * @var array
     */
    public $services_id = null;
    
    /**
     * @var array
     */
    public $rates_id = null;
    
    /**
     * @var string
     */
    public $date_from = null;
    
    /**
     * @var string
     */
    public $date_to = null;
    
    /**
     * @var int
     */
    public $adults = null;
    
    /**
     * @var int
     */
    public $children = null;
    
    /**
     * @var array
     */
    public $children_age = null;
}
