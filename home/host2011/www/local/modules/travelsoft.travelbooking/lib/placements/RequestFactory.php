<?php

namespace travelsoft\booking\placements;

use travelsoft\booking\Request;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory{

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request): Request {

        $oRequest = parent::create($request);
        
        if (is_array($request["rooms_id"]) && !empty($request["rooms_id"])) {

            $oRequest->rooms_id = array_map(function ($id) {
                return intVal($id);
            }, $request["rooms_id"]);
        }

        return $oRequest;
    }

}
