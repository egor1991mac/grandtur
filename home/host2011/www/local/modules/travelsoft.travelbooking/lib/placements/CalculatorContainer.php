<?php

namespace travelsoft\booking\placements;

/**
 * Класс контейнер для калькуляторов по проживанию
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorContainer extends \travelsoft\booking\abstraction\CalculatorContainer {

    /**
     * @param \travelsoft\booking\placements\Calculator $calculator
     * @param string $key
     */
    public function add (\travelsoft\booking\placements\Calculator $calculator, string $key) {
        $this->_container[$key] = $calculator;
    }
}
