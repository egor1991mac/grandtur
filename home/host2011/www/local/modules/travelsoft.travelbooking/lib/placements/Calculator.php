<?php

namespace travelsoft\booking\placements;

/**
 * Калькулятор стоимости
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Calculator extends \travelsoft\booking\abstraction\Calculator {

    /**
     * @var \travelsoft\booking\rooms\Calculator
     */
    protected $_calculator = null;

    /**
     * @var array
     */
    protected $_groupedPlacementsByRooms = null;

    /**
     * @var array
     */
    protected $_tourservices = null;

    /**
     * @var array 
     */
    protected $_discount = null;
    
    /**
     * @var array 
     */
    protected $_ts_discount = null;
    
    /**
     * @var \travelsoft\booking\Request 
     */
    protected $_request = null;
    

    /**
     * @param \travelsoft\booking\rooms\Calculator $calculator
     * @param array $groupedPlacementsByRooms
     * @param array $tourservices
     * @param array $markup
     * @param array $discount
     * @param array $ts_discount
     * @param \travelsoft\booking\Request $request
     */
    public function __construct(
    \travelsoft\booking\rooms\Calculator $calculator, array $groupedPlacementsByRooms, array $tourservices, array $markup, array $discount, array $ts_discount, \travelsoft\booking\Request $request = null) {

        parent::__construct();
        $this->_calculator = $calculator;
        $this->_groupedPlacementsByRooms = $groupedPlacementsByRooms;
        $this->_tourservices = $tourservices;
        $this->_markup = $markup;
        $this->_discount = $discount;
        $this->_ts_discount = $ts_discount;
        $this->_request = $request;
    }

    /**
     * @return $this
     */
    public function calculating() {

        $this->_result = array();

        $arr_prices_grouped_by_rooms = $this->_calculator->calculating()->get();

        if (!empty($arr_prices_grouped_by_rooms)) {

            foreach ($this->_groupedPlacementsByRooms as $room_id => $placement_id) {
                if (isset($arr_prices_grouped_by_rooms[$room_id])) {
                    $this->_result[$placement_id][$room_id] = $arr_prices_grouped_by_rooms[$room_id];
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function min() {

        $arr_min_result = array();
        foreach ($this->_result as $service_id => $arr_data_grouped_by_services) {

            $min = exp(10);
            $arr_min = array();
            foreach ($arr_data_grouped_by_services as $arr_data_grouped_by_rates) {
                foreach ($arr_data_grouped_by_rates as $arr_data) {

                    $price = $this->_converter->convert($arr_data["result_price"], $arr_data["currency"]);

                    if ($min > $price) {
                        $arr_min = $arr_data;
                        $min = $price;
                    }
                }
            }
            if (!empty($arr_min)) {
                $arr_min_result[$service_id] = $arr_min;
            }
        }

        $this->_result = $arr_min_result;

        return $this;
    }

    /**
     * @return $this
     */
    public function addTourservice() {

        foreach ($this->_result as $service_id => &$arr_data_grouped_by_services) {
            foreach ($arr_data_grouped_by_services as &$arr_data_grouped_by_rates) {

                $arRates = \travelsoft\booking\stores\Rates::get([
                            "filter" => ["ID" => \array_values(\array_keys($arr_data_grouped_by_rates))],
                            "select" => ["ID", "UF_CITY"]
                ]);

                foreach ($arr_data_grouped_by_rates as $rate_id => &$arr_data) {

                    $tourservice_for_adults = 0.00;
                    $tourservice_for_children = 0.00;
                    $tourservice_currency = "BYN";
                    $tourservice_discount_price = 0.00;
                    $tourservice_price = 0.00;
                    $arr_tourservice = $this->_tourservices[$service_id][0];
                    if (!empty($arr_tourservice)) {

                        $tourservice_for_adults = $arr_tourservice["for_adults"] * $arr_tourservice["adults"];
                        $tourservice_for_children = $this->_calculateChilrenTouservice($arr_tourservice, $this->_request);
                        $tourservice_currency = $arr_tourservice["currency"];
                        $tourservice_discount_price = $arr_tourservice["discount"];

                        $tourservice_price = $tourservice_for_adults + $tourservice_for_children;
                    }
                    $arr_data["tourservice_currency"] = $arr_tourservice["currency"];
                    $arr_data["tourservice_for_adults"] = $tourservice_for_adults;
                    $arr_data["tourservice_for_children"] = $tourservice_for_children;
                    $arr_data["tourservice_currency"] = $tourservice_currency;
                    $arr_data["tourservice_discount_price"] = $tourservice_discount_price;

                    if ($tourservice_price > 0) {
                        $arr_data["result_price"] += $this->_converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addMarkup() {

        foreach ($this->_result as $service_id => &$arr_data_grouped_by_services) {
            foreach ($arr_data_grouped_by_services as &$arr_data_grouped_by_rates) {
                foreach ($arr_data_grouped_by_rates as &$arr_data) {

                    $arr_data["markup_price"] = 0.00;
                    $arr_data["markup_currency"] = "";

                    if (isset($this->_markup[$service_id])) {

                        $arr_data["markup_price"] = $this->_markup[$service_id]["price"];
                        $arr_data["markup_currency"] = $this->_markup[$service_id]["currency"];

                        if ($arr_data["markup_price"] > 0) {
                            $arr_data["result_price"] += $this->_converter->convert($arr_data["markup_price"], $arr_data["markup_currency"], $arr_data["currency"]);
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function applyDiscount() {
        if (!empty($this->_discount)) {

            if ($this->_discount["UF_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * ($this->_request->adults + $this->_request->children);
                            $arr_data["result_price"] = $arr_data["result_price"] - $this->_discount["UF_VALUE"];
                        }
                    }
                }
            } elseif ($this->_discount["UF_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["discount_price"] = $this->_discount["UF_VALUE"] * $arr_data["gross"] / 100;
                            $arr_data["result_price"] = $arr_data["result_price"] - ($this->_discount["UF_VALUE"] * $arr_data["result_price"] / 100);
                        }
                    }
                }
            }
        }

        return $this;
    }
    
    /**
     * @return $this
     */
    public function applyTSDiscount() {
        if (!empty($this->_ts_discount)) {

            if ($this->_ts_discount["UF_TS_VARIANT"] === "F") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["tourservice_discount_price"] = ($this->_request->adults + $this->_request->children)*$this->_ts_discount["UF_TS_VALUE"];
                            if ($arr_data["tourservice_discount_price"] > 0 ) {
                                $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                            }
                        }
                    }
                }
            } elseif ($this->_ts_discount["UF_TS_VARIANT"] === "P") {
                foreach ($this->_result as $service_id => &$arr_grouped_data_by_rates) {
                    foreach ($arr_grouped_data_by_rates as &$arr_grouped_data) {
                        foreach ($arr_grouped_data as &$arr_data) {

                            $arr_data["tourservice_discount_price"] = $this->_ts_discount["UF_TS_VALUE"] * ($arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"]) / 100;
                            if ($arr_data["tourservice_discount_price"] > 0 ) {
                                $arr_data["result_price"] -= $this->_converter->convert($arr_data["tourservice_discount_price"], $arr_data["tourservice_currency"], $arr_data["currency"]);
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

}
