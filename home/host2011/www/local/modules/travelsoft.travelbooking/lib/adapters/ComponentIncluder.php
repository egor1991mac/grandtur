<?php

namespace travelsoft\booking\adapters;

/**
 * Адаптер подключения компонент
 *
 * @author dimabresky
 */
class ComponentIncluder {
    
    public static function includeComponent(array $parameters = []) {
        
        global $APPLICATION;
        $APPLICATION->IncludeComponent($parameters["component_name"], $parameters["component_template"], $parameters["component_parameters"]);
    }
    
}
