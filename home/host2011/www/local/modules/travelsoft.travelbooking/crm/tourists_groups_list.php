<?
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @global CUser $USER */
require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/tourists-groups-list.css?" . randString(7) . "'>");
$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/select2.min.css'>");
$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/jquery-ui.min.css'>");

$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/jquery-ui.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/moment.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/tourists-groups-list.js?" . randString(7) . "'></script>");

$APPLICATION->SetTitle("Список групп туристов");

/**
 * @param array $dbTours
 * @param string $placement_field
 * @param string $type
 * @param array $countries
 * @return array
 */
function __setToursData($dbTours, $placement_field, $type, &$countries) {

    $tours = $placements = $rooms = $placements_id = [];

    foreach ($dbTours as $tour) {

        if (!empty($tour[$placement_field])) {
            if (is_array($tour[$placement_field])) {
                $placements_id = \array_merge($placements_id, $tour[$placement_field]);
            } else {
                $placements_id[] = $tour[$placement_field];
            }
        }
    }

    if (!empty($placements_id)) {
        $placements = travelsoft\booking\stores\Placements::get([
                    "filter" => ["ID" => \array_values(\array_unique($placements_id))],
                    "select" => ["ID", "NAME"]
        ]);
        foreach (\travelsoft\booking\stores\Rooms::get([
            "filter" => ["UF_PLACEMENT" => $placements_id]
        ]) as $room) {
            $rooms[$room["UF_PLACEMENT"]][] = $room;
        }
    }

    foreach ($dbTours as $tour) {

        $tourdesc = travelsoft\booking\stores\Toursdesc::getById($tour['UF_TOUR']);

        $countries_id = $tourdesc['PROPERTIES'][travelsoft\booking\Settings::getCountryPropertyCode()]['VALUE'];

        if (!empty($countries_id)) {

            $countries = \array_merge($countries, travelsoft\booking\stores\Countries::get([
                        'filter' => ['ID' => !is_array($countries_id) ? [$countries_id] : $countries_id]
            ]));
        }

        $tours[$type . "_" . $tour["ID"]] = [
            "ID" => $tour["ID"],
            "COUNTRIES_ID" => $countries_id,
            "NAME" => $tour["UF_NAME"],
            "HOTELS" => []
        ];

        if (!empty($placements)) {
            if (is_array($tour[$placement_field])) {
                foreach ($tour[$placement_field] as $id) {

                    $tours[$type . "_" . $tour["ID"]]["HOTELS"][$id] = @$placements[$id];
                    $tours[$type . "_" . $tour["ID"]]["HOTELS"][$id]["ROOMS"] = @$rooms[$id];
                }
            } else {
                $tours[$type . "_" . $tour["ID"]]["HOTELS"][$tour[$placement_field]] = @$placements[$tour[$placement_field]];
                $tours[$type . "_" . $tour["ID"]]["HOTELS"][$tour[$placement_field]]["ROOMS"] = @$rooms[$tour[$placement_field]];
            }
        }
    }

    return $tours;
}

$countries = $dates = $grouped_tours_data = [];

// тянем бронировки c датой >= чем сегодня и 
// формируем сгруппированный массив id туров по типам и массив дат
foreach (travelsoft\booking\stores\Bookings::get([
    "filter" => [
        ">=UF_DATE_FROM" => \travelsoft\booking\adapters\Date::create(date('d.m.Y')),
        "UF_SERVICE_TYPE" => ['excursiontour', 'packagetour']
    ]
]) as $booking) {

    $grouped_tours_data[$booking['UF_SERVICE_TYPE']][] = $booking['UF_SERVICE'];
    $dates[$booking['UF_SERVICE_TYPE']."_".$booking['UF_SERVICE']][] = $booking['UF_DATE_FROM']->toString();
}

$tours = \array_merge(
        __setToursData(\travelsoft\booking\stores\ExcursionTour::get(['filter' => ['ID' => $grouped_tours_data['excursiontour']]]), "UF_PLACEMENTS", "excursiontour", $countries), __setToursData(\travelsoft\booking\stores\PackageTour::get(['filter' => ['ID' => $grouped_tours_data['packagetour']]], true, function (&$item) {

                    $item["UF_NAME"] = $item["NAME"];
                    if (!empty($item["UF_TOUR"])) {
                        $tour = travelsoft\booking\stores\Toursdesc::getById($item["UF_TOUR"], ["ID", "NAME"]);
                        if (@$tour["ID"] > 0) {
                            $item["UF_NAME"] = $tour["NAME"];
                        }
                    }
                }), "UF_PLACEMENT", "packagetour", $countries)
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if (empty($tours)) {
    CAdminMessage::ShowMessage('Актуальные путевки по турам не найдены.');
} else {
    ?>
    <form id="tourists-groups-generator" method="POST" action="<?= \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_AJAX ?>/tourists-groups-list.php">
        <fieldset>
            <?= bitrix_sessid_post() ?>
            <input name="action" value="form-generation" type="hidden">
            <h2>Форма генерации списка <span data-target=".fields-area" id="form-collapser" data-title="(развернуть)">(свернуть)</span></h2>
            <div class="fields-area">
                <div class="overlay"></div>
                <div class="form-grousp">
                    <div class="inline-form-group">
                        <label><b>Страны</b>:</label>
                        <select name="country" class="select-2">
                            <option></option>
                            <? foreach ($countries as $id => $country): ?>
                                <option value="<?= $id ?>"><?= $country["NAME"] ?></option>
                            <? endforeach ?>
                        </select>
                    </div>
                    <div class="inline-form-group">
                        <label><b>Тур</b>:</label>
                        <select disabled="" name="tour" class="select-2">
                            <option></option>
                            <? foreach ($tours as $custom_id => $tour): ?>
                                <option value="<?= $custom_id ?>"><?= $tour["NAME"] ?></option>
                            <? endforeach ?>
                        </select>
                    </div>
                    <div class="inline-form-group">
                        <label><b>Дата тура</b>:</label>
                        <input autocomplete="off" name="date_from" type="text" value="" disabled="">
                        
                    </div>
                    <div class="inline-form-group">
                        <label><b>Отели</b>:</label>
                        <select multiple="" name="hotels[]" disabled="" class="select-2">
                            <option>Все отели</option>
                        </select>
                    </div>
                    <div class="inline-form-group">
                        <label><b>Группа</b>:</label>
                        <select name="group" class="select-2">
                            <option value="all">Все группы</option>
                            <? for ($i = 1; $i <= 5; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <? endfor ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="inline-form-groups">
                    <div class="inline-form-group">
                        <input checked="" type="checkbox" name="show_tour_name" value="Y"><label class="inline"><b>Название тура</b></label>
                    </div>
                    <div class="inline-form-group">
                        <input disabled=""  checked="" type="checkbox" name="show_fio" value="Y"><label class="inline"><b>ФИО</b></label>
                    </div>
                    <div class="inline-form-group">
                        <input checked="" type="checkbox" name="show_fio_lat" value="Y"><label class="inline"><b>ФИ латиницей</b></label>
                    </div>

                    <div class="inline-form-group">
                        <input checked="" type="checkbox" name="show_passport" value="Y"><label class="inline"><b>Паспорт</b></label>
                    </div>
                    <div class="inline-form-group">
                        <input checked="" type="checkbox" name="show_birthday" value="Y"><label class="inline"><b>Дата рождения</b></label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="btn-area"><input type="submit" class="adm-btn-save" value="Показать" name="show"></div>
            </div>
        </fieldset>
    </form>
    <div id="tourists-groups-table"></div>
    <script>
        window.__tsconfig = {
            countries: <?= \json_encode($countries) ?>,
            dates: <?= \json_encode($dates) ?>,
            tours: <?= \json_encode($tours) ?>,
            ajax_url: "<?= \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_AJAX ?>/tourists-groups-list.php"
        };
    </script>
    <?
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
