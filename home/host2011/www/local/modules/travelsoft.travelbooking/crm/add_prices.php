<?
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @global CUser $USER */
require_once 'header.php';

$APPLICATION->SetTitle("Цены и наличие мест");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
//
$APPLICATION->AddHeadString("<link rel='stylesheet' href='/local/modules/travelsoft.travelbooking/crm/css/jquery-ui.min.css?" . randString(7) . "'>");
$APPLICATION->AddHeadString("<link rel='stylesheet' href='/local/modules/travelsoft.travelbooking/crm/css/select2.min.css'>");
$APPLICATION->AddHeadString("<link rel='stylesheet' href='/local/modules/travelsoft.travelbooking/crm/css/styles.css?" . randString(7) . "'>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/jquery-ui.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/common.js?" . randString(7) . "'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/add_prices.js?" . randString(7) . "'></script>");

if (check_bitrix_sessid() && strlen($_REQUEST["show_table"]) > 0) {

//    # обработка запроса формы натсроек таблицы цен
    $optionsFormResponse = \travelsoft\booking\crm\AddPricesUtils::processOptionsFromRequest((array) $_REQUEST);
}


if ($optionsFormResponse["errors"]) {
    CAdminMessage::ShowMessage(array(
        "MESSAGE" => implode('<br>', $optionsFormResponse["errors"]),
        "TYPE" => "ERROR",
        "HTML" => true
    ));
}

$serviceTypes = \travelsoft\booking\Settings::getServicesTypesWithout(array("placements", "packagetour"));
$bookingServices = travelsoft\booking\crm\Utils::getBookingServices(array_keys($serviceTypes));
foreach (travelsoft\booking\crm\Utils::getBookingServices(array_keys($serviceTypes)) as $type => $arr) {
    $bookingServices[$type] = \travelsoft\booking\crm\Utils::getReferencesSelectData($arr, "NAME", "ID");
}

$selectViewMethod = isset($serviceTypes[$_REQUEST["service_type"]]) ? "get" . ucfirst($_REQUEST["service_type"]) . "SelectView" : "get" . ucfirst(array_keys($serviceTypes)[0]) . "SelectView";
?>
<table>
    <tbody>
        <tr>
            <td>
                <form id="table-generator-form" method="GET" action="<?= $APPLICATION->GetCurPage("lang=" . LANG, array('lang')) ?>">
                    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
                    <?= bitrix_sessid_post(); ?>
                    <div class="form-fields">
                        <table>
                            <tbody>
                                <tr>
                                    <td align="right">
                                        <b>Тип объекта: </b>
                                    </td>
                                    <td>
                                        <?= \SelectBoxFromArray("service_type", travelsoft\booking\crm\Utils::getReferencesSelectData($serviceTypes, "name", "type"), $_REQUEST["service_type"], "", ' data-services-target="#service_id" id="service_type" class="select2"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Объект: </b></td>
                                    <td><?= travelsoft\booking\crm\Utils::$selectViewMethod("service_id", $_REQUEST["service_id"]) ?></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Дата с: </b></td>
                                    <td><input autocomplete="off" id="date_from" name="date_from" value="<?= htmlspecialcharsbx($_REQUEST["date_from"]) ?>" class="__ui_datepicker"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Дата по: </b></td>
                                    <td><input  autocomplete="off" id="date_to" name="date_to" value="<?= htmlspecialcharsbx($_REQUEST["date_to"]) ?>" class="__ui_datepicker"></td>
                                </tr>

                            </tbody>
                        </table>
                        <div id="btn-area"><button name="show_table" value="Показать" type="submit" class="sub-part adm-btn-save">Показать</button></span>

                        </div>

                </form>
            </td>
        </tr>
    </tbody>
</table>
<div id="add-prices"><div id="table-hidder"></div><?= ($optionsFormResponse['html'] ? $optionsFormResponse['html'] : "") ?></div>

<script>
    var jsBookingServices = <?= \Bitrix\Main\Web\Json::encode($bookingServices); ?>;
    var floatNull = <?= travelsoft\booking\Settings::FLOAT_NULL?>;
</script>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
