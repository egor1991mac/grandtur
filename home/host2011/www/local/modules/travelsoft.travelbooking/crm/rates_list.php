<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Rates;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$sort = new CAdminSorting(Settings::RATES_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::RATES_HTML_TABLE_ID, $sort);

if ($arRatesId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arRatesId = array_keys(Rates::get(array('select' => array('ID'))));
    }

    foreach ($arRatesId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Rates::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::RATES_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Rates::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arRates = Rates::get($getParams);

$dbResult = new CAdminResult($arRates, Settings::RATES_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_NAME",
        "content" => "Название",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICES_TYPE",
        "content" => "Тип услуг",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICES",
        "content" => "Услуги",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CURRENCY",
        "content" => "Валюта",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_PRICE_TYPES",
        "content" => "Типы цен",
        "align" => "center",
        "default" => true
    ),
));

$arPriceTypes = \travelsoft\booking\crm\Utils::getPriceTypes();
$arTransferService = $arExcursionTourService = $arPlacements = $arRooms = array();
while ($arRate = $dbResult->Fetch()) {

    $row = &$list->AddRow($arRate["ID"], $arRate);
    
     if (!empty($arRate["UF_PRICE_TYPES"])) {
        $arr_str = array();
        foreach ($arRate["UF_PRICE_TYPES"] as $ptid) {
            if (isset($ptid, $arPriceTypes)) {
                $arr_str[] = $arPriceTypes[$ptid]["UF_NAME"];
            }
        }

        $row->AddViewField("UF_PRICE_TYPES", implode(", ", $arr_str));
    }
    
    $arr_str = array();
    if ($arRate["UF_SERVICES_TYPE"] === "placements") {
        
        $row->AddViewField("UF_SERVICES_TYPE", \travelsoft\booking\Settings::getServicesTypes()["placements"]["name"]);
        
        if (!empty($arRate["UF_SERVICES"])) {
            
            foreach ($arRate["UF_SERVICES"] as $service_id) {
                
                \travelsoft\booking\crm\RatesUtils::setPlacement($arPlacements, $service_id);
                
                $arr_str[] = $arPlacements[$service_id]["NAME"];
            }
        }
        
        if (!empty($arr_str)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arr_str));
        } else {
            $row->AddViewField("UF_SERVICES", "Для всех проживаний");
        }
        
    } elseif ($arRate["UF_SERVICES_TYPE"] === "transfer" || $arRate["UF_SERVICES_TYPE"] === "transferback") {
        
        $row->AddViewField("UF_SERVICES_TYPE", \travelsoft\booking\Settings::getServicesTypes()[$arRate["UF_SERVICES_TYPE"]]["name"]);
        
        if (!empty($arRate["UF_SERVICES"])) {
            
            foreach ($arRate["UF_SERVICES"] as $service_id) {
                
                if (!isset($arTravelService[$service_id])) {
                    $arTravelService[$service_id] = current(travelsoft\booking\stores\Buses::get(array(
                        "filter" => array("ID" => $service_id),
                        "select" => array("ID", "UF_NAME")
                    )));
                    
                }
                $arr_str[] = $arTravelService[$service_id]["UF_NAME"];
                
            }
        }
        
        if (!empty($arr_str)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arr_str));
        } else {
            $row->AddViewField("UF_SERVICES", "Для всех проездов");
        }
    } elseif ($arRate["UF_SERVICES_TYPE"] === "excursiontour") {
        
        $row->AddViewField("UF_SERVICES_TYPE", \travelsoft\booking\Settings::getServicesTypes()["excursiontour"]["name"]);
        
        if (!empty($arRate["UF_SERVICES"])) {
            
            foreach ($arRate["UF_SERVICES"] as $service_id) {
                
                if (!isset($arExcursionTourService[$service_id])) {
                    $arExcursionTourService[$service_id] = current(travelsoft\booking\stores\ExcursionTour::get(array(
                        "filter" => array("ID" => $service_id),
                        "select" => array("ID", "UF_NAME")
                    )));
                    
                }
                $arr_str[] = $arExcursionTourService[$service_id]["UF_NAME"];
                
            }
        }
        
        if (!empty($arr_str)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arr_str));
        } else {
            $row->AddViewField("UF_SERVICES", "Для всех проездов");
        }
    } elseif ($arRate["UF_SERVICES_TYPE"] === "rooms") {
        
        $row->AddViewField("UF_SERVICES_TYPE", \travelsoft\booking\Settings::getServicesTypes()["rooms"]["name"]);
        
        if (!empty($arRate["UF_SERVICES"])) {
            
            foreach ($arRate["UF_SERVICES"] as $service_id) {
                
                if (!isset($arRooms[$service_id])) {
                    $arRooms[$service_id] = current(travelsoft\booking\stores\Rooms::get(array(
                        "filter" => array("ID" => $service_id),
                        "select" => array("ID", "UF_NAME", "UF_PLACEMENT")
                    )));
                    
                    \travelsoft\booking\crm\RatesUtils::setPlacement($arPlacements, $arRooms[$service_id]["UF_PLACEMENT"]);
                    
                    $arRooms[$service_id]["UF_NAME"] .= "[".$arPlacements[$arRooms[$service_id]["UF_PLACEMENT"]]["NAME"]."]";
                }
                $arr_str[] = $arRooms[$service_id]["UF_NAME"];
                
            }
        }
        
        if (!empty($arr_str)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arr_str));
        } else {
            $row->AddViewField("UF_SERVICES", "Для всех проездов");
        }
    }
    
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect("travelsoft_crm_booking_rates_edit.php?ID=" . $arRate["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить тариф')) " . $list->ActionDoGroup($arRate["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать тариф",
        'TITLE' => "Создание тарифа",
        'LINK' => 'travelsoft_crm_booking_rates_edit.php?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список тарифов");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
