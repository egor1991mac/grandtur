<?php

require 'header.php';

if ($_SERVER["REQUEST_METHOD"] === "POST" && check_bitrix_sessid()) {
    
    # обработка запроса на сохранение цен и квот
    $priceAndQuotasFormResponse = travelsoft\booking\crm\AddPricesUtils::processPriceAndQuotasFormRequest((array) $_REQUEST['prices_and_quotas']);

    if (!empty($priceAndQuotasFormResponse)) {

        travelsoft\booking\crm\Utils::sendJsonResponse(\Bitrix\Main\Web\Json::encode(array('error' => null, 'result' => $priceAndQuotasFormResponse)));
    }
} else {
    
    travelsoft\booking\crm\Utils::sendJsonResponse(\Bitrix\Main\Web\Json::encode(array('error' => 'bad request')));
}
