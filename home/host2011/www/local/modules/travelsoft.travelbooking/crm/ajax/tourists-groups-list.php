<?php
/*
 * Tourists groups list ajax processing
 */

require_once 'header.php';

switch ($request->get('action')) {

    case 'form-generation':

        __runAction($request, function () use ($request) {

            \extract(__getVars($request));

            $bookings = travelsoft\booking\stores\Bookings::get([
                        "filter" => [
                            "UF_DATE_FROM" => $date,
                            "UF_SERVICE" => $tour_id,
                        ]
            ]);

            $tourists = $tourists_id = [];

            foreach ($bookings as $booking) {

                if ($booking["UF_TOURISTS"]) {

                    $tourists_id = \array_merge($tourists_id, $booking['UF_TOURISTS']);
                }
            }

            if (!empty($tourists_id)) {

                $tourists = \travelsoft\booking\stores\Tourists::get([
                            "filter" => ["ID" => array_unique($tourists_id)]
                ]);

                $filter = [
                    "UF_TOURISTS" => $tourists_id,
                    "UF_SERVICE" => $tour_id,
                    "UF_SERVICE_TYPE" => $type,
                    "UF_DATE" => $date
                ];

                if ($request->get('group') > 0) {
                    $filter["UF_NUMBER"] = $request->get('group');
                }

                $groups = travelsoft\booking\stores\TouristsGroups::get([
                            "filter" => $filter
                ]);

                if ($request->get('group') > 0) {
                    if (empty($groups)) {
                        \travelsoft\booking\Utils::sendJsonResponse(\json_encode(["error" => 'Туристов по выбранной группе не найдено.']));
                    } else {
                        // фильтруем туристов в соответсвии с группой, которую хотим видеть
                        $tmp_tourists = [];
                        foreach ($groups as $group) {
                            foreach ($group['UF_TOURISTS'] as $tourist_id) {
                                if (!isset($tmp_tourists[$tourist_id])) {
                                    $tmp_tourists[$tourist_id] = $tourists[$tourist_id];
                                }
                            }
                        }
                        $tourists = $tmp_tourists;
                    }
                }

                if (!empty($tourists)) {
                    $rooming = $placements = $rooms = $tour = $touristsGroupsData = [];

                    // получение отелей по туру
                    if ($type === "excursiontour") {

                        $tour = travelsoft\booking\stores\ExcursionTour::getById($tour_id);

                        $filter = ["ID" => !empty($tour["UF_PLACEMENTS"]) ? $tour["UF_PLACEMENTS"] : -1];
                        if (!empty($request->get('hotels'))) {
                            $filter['ID'] = $request->get('hotels');
                        }

                        $placements = travelsoft\booking\stores\Placements::get([
                                    "filter" => ["ID" => !empty($tour["UF_PLACEMENTS"]) ? $tour["UF_PLACEMENTS"] : -1]
                        ]);
                    } elseif ($type === "packagetour") {

                        $tour = travelsoft\booking\stores\PackageTour::getById($tour_id);

                        $filter = ["ID" => !empty($tour["UF_PLACEMENTS"]) ? $tour["UF_PLACEMENTS"] : -1];
                        if (!empty($request->get('hotels'))) {
                            $filter['ID'] = $request->get('hotels');
                        }

                        $placements = travelsoft\booking\stores\Placements::get([
                                    "filter" => ["ID" => $tour['UF_PLACEMENT']]
                        ]);
                    }

                    if (!empty($request->get('hotels')) && empty($placements)) {
                        \travelsoft\booking\Utils::sendJsonResponse(\json_encode(["error" => 'Туристов по выбранному размещению не найдено.']));
                    }

                    // получение информации по номерам
                    if (!empty($placements)) {

                        $rooms = \travelsoft\booking\stores\Rooms::get([
                                    "filter" => ["UF_PLACEMENT" => \array_keys($placements)]
                        ]);
                    }

                    if (empty($groups)) {
                        // если группы раннее не создавались, то для всех туристов
                        // создаем группу #1

                        \travelsoft\booking\stores\TouristsGroups::add([
                            "UF_TOURISTS" => $tourists_id,
                            "UF_SERVICE" => $tour_id,
                            "UF_SERVICE_TYPE" => $type,
                            "UF_DATE" => $date,
                            "UF_NUMBER" => 1
                        ]);
                    } else {

                        foreach ($groups as $group) {

                            foreach ($group['UF_TOURISTS'] as $id) {

                                $tourists[$id]['GROUP'] = $group['UF_NUMBER'];
                            }
                        }

                        $def_group_tourists_id = [];

                        foreach ($tourists as &$tourist) {

                            if (!isset($tourist['GROUP'])) {

                                $tourist['GROUP'] = 1;
                                $def_group_tourists_id[] = $tourist['ID'];
                            }
                        }

                        // всех туристов у которых не стоит группа
                        // определяем в группу 1
                        if (!empty($def_group_tourists_id)) {
                            \travelsoft\booking\stores\TouristsGroups::add([
                                "UF_TOURISTS" => $def_group_tourists_id,
                                "UF_SERVICE" => $tour_id,
                                "UF_SERVICE_TYPE" => $type,
                                "UF_DATE" => $date,
                                "UF_NUMBER" => 1
                            ]);
                        }

                        $rooming = \travelsoft\booking\stores\Rooming::get([
                                    "filter" => [
                                        "UF_TOURISTS" => \array_values(\array_keys($tourists)),
                                        "UF_SERVICE" => $tour_id,
                                        "UF_SERVICE_TYPE" => $type,
                                        "UF_DATE" => $date,
                                    ]
                        ]);
                    }

                    // формируем основные данные для вывода
                    if (!empty($rooming)) {

                        foreach ($rooming as $r) {

                            $touristsGroupsData[] = [
                                "ROOM_ID" => $r["UF_ROOM"],
                                "ROOM_NUMBER" => $r["UF_ROOM_NUMBER"],
                                "TOURISTS_ID" => $r['UF_TOURISTS']
                            ];
                        }
                    } else {

                        $touristsGroupsData[] = [
                            "ROOM_ID" => -1,
                            "ROOM_NUMBER" => 0,
                            "TOURISTS_ID" => array_keys($tourists)
                        ];
                    }

                    if (!empty($touristsGroupsData)):
                        $GLOBALS["APPLICATION"]->RestartBuffer();
                        ob_start();
                        $colspan = 4;
                        ?>
                        <div class="excel-btn-area"><button class="excel-btn"></button></div>
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>Edit</th>
                                    <? if ($request->get('show_tour_name') === "Y"): ?>
                                        <th>Название тура</th>
                                        <?
                                        $colspan++;
                                    endif
                                    ?>
                                    <th>ФИО</th>
                                    <? if ($request->get('show_fio_lat') === "Y"): ?>
                                        <th>ФИ латиницей</th>
                                        <?
                                        $colspan++;
                                    endif
                                    ?>
                                    <? if ($request->get('show_passport') === "Y"): ?>
                                        <th>Паспорт</th>
                                        <?
                                        $colspan++;
                                    endif
                                    ?>
                                    <? if ($request->get('show_birthday') === "Y"): ?>
                                        <th>Дата рождения</th>
                                        <?
                                        $colspan++;
                                    endif
                                    ?>
                                    <th>Группа</th>
                                    <th>Размещение</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?
                                foreach ($touristsGroupsData as $data) {
                                    $rooms_rowspan = count($data['TOURISTS_ID']);
                                    $room_name = 'Не назначено.';
                                    if ($data['ROOM_ID'] > 0) {
                                        $room_name = "#" . $data["ROOM_NUMBER"] . ": " . $rooms[$data['ROOM_ID']]['UF_NAME'] . "[" . $placements[$rooms[$data['ROOM_ID']]['UF_PLACEMENT']]['NAME'] . "]";
                                    }
                                    foreach ($data['TOURISTS_ID'] as $id) {
                                        if (isset($tourists[$id])) {
                                            __showTr($request, $tourists[$id], $tour, $rooms_rowspan, $room_name);
                                            $rooms_rowspan = 0;
                                            unset($tourists[$id]);
                                        }
                                    }
                                }
                                if (!empty($tourists)) {
                                    $rooms_rowspan = count($tourists);
                                    $room_name = 'Не назначено.';
                                    foreach ($tourists as $tourist) {
                                        __showTr($request, $tourist, $tour, $rooms_rowspan, $room_name);
                                        $rooms_rowspan = 0;
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="<?= $colspan ?>">
                                        <input type='button' class="adm-btn-save" value="Редактировать" name="show_edit">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <?
                        \travelsoft\booking\Utils::sendJsonResponse(\json_encode(["result" => ob_get_clean(), "rooms" => $rooms]));

                    endif;
                }
            }
        });

        break;

    case "edit-tourists-groups":

        __runAction($request, function () use ($request) {
            if (
                    !empty($request->get('tourists_id')) && is_array($request->get('tourists_id')) &&
                    $request->get('group') > 0
            ) {

                \extract(__getVars($request));

                // чистим группы от туристов
                travelsoft\booking\stores\TouristsGroups::get([
                    "filter" => [
                        "UF_TOURISTS" => $request->get('tourists_id'),
                        "UF_SERVICE" => $tour_id,
                        "UF_SERVICE_TYPE" => $type,
                        "UF_DATE" => $date
                    ]
                        ], true, function ($group) use ($request) {

                    foreach ($request->get('tourists_id') as $tourist_id) {

                        if (in_array($tourist_id, $group['UF_TOURISTS'])) {
                            unset($group['UF_TOURISTS'][\array_search($tourist_id, $group['UF_TOURISTS'])]);
                        }
                    }

                    travelsoft\booking\stores\TouristsGroups::update($group['ID'], [
                        'UF_TOURISTS' => $group['UF_TOURISTS']
                    ]);
                });
                // добавляем туристов к группе из запроса
                $group = current(travelsoft\booking\stores\TouristsGroups::get([
                            'filter' => [
                                "UF_NUMBER" => $request->get('group'),
                                "UF_SERVICE" => $tour_id,
                                "UF_SERVICE_TYPE" => $type,
                                "UF_DATE" => $date
                            ]
                ]));
                if (@$group['ID'] > 0) {
                    foreach ($request->get('tourists_id') as $tourist_id) {
                        $group['UF_TOURISTS'][] = $tourist_id;
                    }
                    travelsoft\booking\stores\TouristsGroups::update($group['ID'], [
                        'UF_TOURISTS' => \array_values(\array_unique($group['UF_TOURISTS']))
                    ]);
                } else {
                    travelsoft\booking\stores\TouristsGroups::add([
                        "UF_NUMBER" => $request->get('group'),
                        "UF_SERVICE" => $tour_id,
                        "UF_SERVICE_TYPE" => $type,
                        "UF_DATE" => $date,
                        "UF_TOURISTS" => $request->get('tourists_id')
                    ]);
                }
                if ($request->getPost('room_id') > 0 && $request->getPost('room_number') > 0) {
                    // чистим руминг от туристов
                    travelsoft\booking\stores\Rooming::get([
                        "filter" => [
                            "UF_TOURISTS" => $request->get('tourists_id'),
                            "UF_SERVICE" => $tour_id,
                            "UF_SERVICE_TYPE" => $type,
                            "UF_DATE" => $date
                        ]
                            ], true, function ($rooming) use ($request) {

                        foreach ($request->get('tourists_id') as $tourist_id) {

                            if (in_array($tourist_id, $rooming['UF_TOURISTS'])) {
                                unset($rooming['UF_TOURISTS'][\array_search($tourist_id, $rooming['UF_TOURISTS'])]);
                            }
                        }

                        travelsoft\booking\stores\Rooming::update($rooming['ID'], [
                            'UF_TOURISTS' => $rooming['UF_TOURISTS']
                        ]);
                    });

                    // добавляем туристов к румингу из запроса
                    $rooming = current(\travelsoft\booking\stores\Rooming::get([
                                'filter' => [
                                    'UF_ROOM_NUMBER' => $request->get('room_number'),
                                    'UF_ROOM' => $request->get('room_id'),
                                    "UF_SERVICE" => $tour_id,
                                    "UF_SERVICE_TYPE" => $type,
                                    "UF_DATE" => $date,
                                ]
                    ]));

                    if (@$rooming['ID'] > 0) {
                        foreach ($request->get('tourists_id') as $tourist_id) {
                            $rooming['UF_TOURISTS'][] = $tourist_id;
                        }
                        \travelsoft\booking\stores\Rooming::update($rooming['ID'], [
                            'UF_TOURISTS' => \array_values(\array_unique($rooming['UF_TOURISTS']))
                        ]);
                    } else {
                        \travelsoft\booking\stores\Rooming::add([
                            'UF_ROOM_NUMBER' => $request->get('room_number'),
                            'UF_ROOM' => $request->get('room_id'),
                            "UF_SERVICE" => $tour_id,
                            "UF_SERVICE_TYPE" => $type,
                            "UF_DATE" => $date,
                            "UF_TOURISTS" => $request->get('tourists_id')
                        ]);
                    }
                }

                \travelsoft\booking\Utils::sendJsonResponse(\json_encode(['success' => true]));
            }

            __sendBadResponse();
        });

        break;
}

/**
 * @param \Bitrix\Main\HttpRequest $request
 * @param array $tourist
 * @param array $tour
 * @param array $room_rowspan
 * @param array $room_name
 */
function __showTr($request, $tourist, $tour, $room_rowspan, $room_name) {
    ?><tr>
        <td class="edit-td"><input data-tourist-id="<?= $tourist['ID'] ?>" name="to_edit" type="checkbox" value='Y'></td>
            <? if ($request->get('show_tour_name') === "Y"): ?>
            <td><?= $tour['UF_NAME'] ?></td>
        <? endif ?>
        <td><?= $tourist['FULL_NAME'] ?></td>
        <? if ($request->get('show_fio_lat') === "Y"): ?>
            <td><?= $tourist['FULL_NAME_LAT'] ?></td>
        <? endif ?>
        <? if ($request->get('show_passport') === "Y"): ?>
            <td><?= $tourist['SN_PASSPORT'] ?></td>
        <? endif ?>
        <? if ($request->get('show_birthday') === "Y"): ?>
            <td><?= $tourist['BIRTHDATE'] ?></td>
        <? endif ?>
        <td><?= $tourist['GROUP'] ?></td>
        <? if ($room_rowspan > 0): ?>
            <td rowspan="<?= $room_rowspan ?>"><?= $room_name ?></td>
        <? endif ?>
    </tr><?
}

/**
 * @param \Bitrix\Main\HttpRequest
 * @return array
 */
function __getVars($request) {

    static $vars = null;

    if (!$vars) {
        $request_tour_field_parts = explode("_", $request->get('tour'));
        $vars = [
            'type' => $request_tour_field_parts[0],
            'tour_id' => $request_tour_field_parts[1],
            'types' => \travelsoft\booking\Settings::getServicesTypes(),
            'date' => \travelsoft\booking\adapters\Date::create($request->get('date_from')),
            'timestamp' => \strtotime($request->get('date_from'))
        ];
    }

    return $vars;
}

/**
 * @param \Bitrix\Main\HttpRequest
 * @param callable
 */
function __runAction($request, $callback) {

    \extract(__getVars($request));

    if (isset($types[$type]) && $tour_id > 0 && $timestamp >= time()) {
        $callback();
    }

    __sendBadResponse();
}

function __sendBadResponse() {

    \travelsoft\booking\Utils::sendJsonResponse(\json_encode(["error" => "Bad request."]));
}
