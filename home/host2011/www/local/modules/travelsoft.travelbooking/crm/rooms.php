<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Rooms;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS."/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/common.js?".randString(7)."'></script>");

$sort = new CAdminSorting(Settings::ROOMS_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::ROOMS_HTML_TABLE_ID, $sort);

if ($arRoomsId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arRoomsId = array_keys(Rooms::get(array('select' => array('ID'))));
    }

    foreach ($arRoomsId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Rooms::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("filter" => travelsoft\booking\crm\RoomsUtils::getFilterList(), "order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::ROOMS_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Rooms::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arRooms = Rooms::get($getParams);

$dbResult = new CAdminResult($arRooms, Settings::ROOMS_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID комнаты",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_NAME",
        "content" => "Название",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_DESCRIPTION",
        "content" => "Описание",
        "sort" => "UF_DESCRIPTION",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_PLACEMENT",
        "content" => "Размещение",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_MAIN_PLACES",
        "content" => "Количество основных мест",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_ADD_PLACES",
        "content" => "Количество дополнительных мест",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_PICTURES",
        "content" => "Наличие фото",
        "align" => "center",
        "default" => true
    ),
));

$arPlacements = array();
while ($arRoom = $dbResult->Fetch()) {

    $row = &$list->AddRow($arRoom["ID"], $arRoom);
    if ($arRoom["UF_PLACEMENT"] > 0) {

        if (!isset($arPlacements[$arRoom["UF_PLACEMENT"]])) {

            $arPlacements[$arRoom["UF_PLACEMENT"]] = current(\travelsoft\booking\stores\Placements::get(
                            array("filter" => array("ID" => $arRoom["UF_PLACEMENT"]), "order" => array("ID", "NAME"))
            ));
        }

        $row->AddViewField("UF_PLACEMENT", $arPlacements[$arRoom["UF_PLACEMENT"]]["NAME"]);
    }
    if (!empty($arRoom["UF_PICTURES"])) {
        $row->AddViewField("UF_PICTURES", "Да");
    } else {
        $row->AddViewField("UF_PICTURES", "Нет");
    }
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect("travelsoft_crm_booking_room_edit.php?ID=" . $arRoom["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить номер')) " . $list->ActionDoGroup($arRoom["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать номер",
        'TITLE' => "Создание номера",
        'LINK' => 'travelsoft_crm_booking_room_edit.php?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список номеров");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<?

travelsoft\booking\crm\Utils::showFilterForm(
        array(
            'table_id' => Settings::ROOMS_HTML_TABLE_ID,
            'form_elements' => array(
                array(
                    "label" => "Проживание",
                    "view" => \travelsoft\booking\crm\Utils::getPlacementsSelectView("UF_PLACEMENT", $_GET["UF_PLACEMENT"])
                )
            )
        )
);

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
