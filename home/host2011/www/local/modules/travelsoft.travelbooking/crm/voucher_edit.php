<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/common.js?" . randString(7) . "'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/voucher_edit.js?" . randString(7) . "'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление путевки';
    if ($ID > 0) {

        $arVouchers = current(\travelsoft\booking\stores\Vouchers::get(array('filter' => array('ID' => $ID))));

        if (!$arVouchers['ID']) {

            throw new Exception('Путевка с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование путевки #' . $arVouchers['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\VouchersUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    $arTabs = array(
        array(
            "DIV" => "VOUCHER",
            "TAB" => 'Поля для заполнения путевки',
            "CONTENT" => travelsoft\booking\crm\VouchersUtils::getEditFormFields($arVouchers)
        )
    );
    if ($ID > 0) {
        $arTabs[] = array(
            "DIV" => "DOCUMENTS",
            "TAB" => "Формирование документов",
            "TITLE" => "Выбор документа для формирования",
            "CONTENT" => travelsoft\booking\crm\VouchersUtils::getDocumentsForPrintContent($ID)
        );
        $arTabs[] = array(
            "DIV" => "PAYMENT_HISTORY",
            "TAB" => 'Платежи',
            "TITLE" => 'История платежей',
            'CONTENT' => travelsoft\booking\crm\VouchersUtils::getPaymentHistoryContent($ID)
        );
        $arTabs[] = array(
            "DIV" => "MESSAGES",
            "TAB" => 'Сообщения',
            "TITLE" => 'Сообщения по путевке',
            'CONTENT' => travelsoft\booking\crm\VouchersUtils::getMessagesContent($ID)
        );
    }

    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'voucher_form',
        'id' => 'voucher_form',
        'tabs' => $arTabs,
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

