
window.CRMUtils = {

    // добавление/редактирование клиента
    editClient: function (client_id) {
        window.jsUtils.OpenWindow(`/local/modules/travelsoft.travelbooking/crm/client_edit_window.php${client_id > 0 ? `?ID=${client_id}` : ``}`, 700, 600);
    },

    // добавление нового туриста
    addTourist: function (domNodeLink) {

        window.touristChildWindowData = {

            touristSelect: $("#" + domNodeLink),

            initSelect2: function () {

                $("#" + domNodeLink).select2("destroy");
                $("#" + domNodeLink).select2();
            }
        };

        window.jsUtils.OpenWindow('/local/modules/travelsoft.travelbooking/crm/tourist_edit_window.php', 700, 600);
    },

    // добаление полей добавления туриста
    addTouristField: function () {

        var timestamp = new Date().getTime();

        $("#tourists-add-table").find('tbody').append(`<tr><td id="tourists-select-container-${timestamp}">Загрузка списка туристов...</td></tr>`);
        $.ajax({
            url: "/local/modules/travelsoft.travelbooking/crm/ajax/get_tourists.php",
            dataType: "json",
            success: function (tourists) {

                $(`#tourists-select-container-${timestamp}`).html(`
                    <select style="width:180px;" id="tourist-select-${timestamp}" class="tourist-select" name="UF_TOURISTS[]">
                    ${(function (tourists) {
                    var html = `<option value="">...</option>`;
                    for (var id in tourists) {
                        html += `<option value="${id}">${tourists[id].FULL_NAME}</option>`;
                    }
                    return html;
                })(tourists)}
                    </select> &nbsp; <a href="javascript: CRMUtils.addTourist(\'tourist-select-${timestamp}\')">добавить нового</a>
                `);

                $(`#tourist-select-${timestamp}`).select2();
            }
        });
    },

    // добаление полей добавления возраста детей
    addAgeField: function () {

        $("#ages-add-table").find('tbody').append(`<tr><td><input name="UF_CHILDREN_AGE[]" value="" type="text"></td></tr>`);

    },

    findVariants: function () {

        var request_data_pool = {
            service_type: $("#UF_SERVICE_TYPE").val(),
            service: $("#UF_SERVICE").val(),
            adults: $("#UF_ADULTS").val(),
            children: $("#UF_CHILDREN").val() || 0,
            children_age: (function () {
                var ages = [];
                $("input[name='UF_CHILDREN_AGE[]'").each(function () {
                    var age = $(this).val();
                    if (age) {
                        ages.push(age);
                    }
                });
                return ages;
            })()
        };

        var request_str = "";

        var errors = [];

        // проверка и формирование данных перед поиском цен
        switch (request_data_pool.service_type) {

            case "placements":
            case "packagetour":

                request_data_pool.room = $("#UF_ROOM").val();
                request_data_pool.date_from = $("input[name='UF_DATE_FROM']").val();
                request_data_pool.date_to = $("input[name='UF_DATE_TO']").val();

                if (!request_data_pool.service || !request_data_pool.service.length) {
                    errors.push("Укажите услугу.");
                }
                if (!request_data_pool.room || request_data_pool.room < 0) {
                    errors.push("Укажите номер.");
                }
                if (!request_data_pool.adults || request_data_pool.adults < 0) {
                    errors.push("Укажите количество взрослых.");
                }
                if (request_data_pool.children > 0 && request_data_pool.children_age.length !== Number(request_data_pool.children)) {
                    errors.push("Укажите возраст всех детей.");
                }
                if (!request_data_pool.date_from || !request_data_pool.date_from.length) {
                    errors.push("Укажите дату с");
                }
                if (!request_data_pool.date_to || !request_data_pool.date_to.length) {
                    errors.push("Укажите дату тура по");
                }
                if (errors.length) {
                    alert(errors.join("\n"));
                    return;
                }

                request_str = [`service_type=${request_data_pool.service_type}`, `travelbooking[services_id][]=${request_data_pool.service}`, `travelbooking[rooms_id][]=${request_data_pool.room}`, `travelbooking[adults]=${request_data_pool.adults}`, `travelbooking[children]=${request_data_pool.children}`, `${(function () {
                        request_data_pool.children_age.sort();
                        return $.map(request_data_pool.children_age, function (age) {
                            return "travelbooking[children_age][]=" + age;
                        }).join("&");
                    })()}`, `travelbooking[date_from]=${request_data_pool.date_from}`, `travelbooking[date_to]=${request_data_pool.date_to}`].join("&");

                break;
            case "transfer":
            case "transferback":
            case "excursiontour":
                
                request_data_pool.date_from = $("input[name='UF_DATE_FROM']").val();

                if (!request_data_pool.service || !request_data_pool.service.length) {
                    errors.push("Укажите услугу.");
                }

                if (!request_data_pool.adults || request_data_pool.adults < 0) {
                    errors.push("Укажите количество взрослых.");
                }
                if (request_data_pool.children > 0 && request_data_pool.children_age.length !== Number(request_data_pool.children)) {
                    errors.push("Укажите возраст всех детей.");
                }
                if (!request_data_pool.date_from || !request_data_pool.date_from.length) {
                    errors.push("Укажите дату тура с");
                }
                if (errors.length) {
                    alert(errors.join("\n"));
                    return;
                }
                
                request_str = [`service_type=${request_data_pool.service_type}`, `travelbooking[services_id][]=${request_data_pool.service}`, `travelbooking[adults]=${request_data_pool.adults}`, `travelbooking[children]=${request_data_pool.children}`, `${(function () {
                        request_data_pool.children_age.sort();
                        return $.map(request_data_pool.children_age, function (age) {
                            return "travelbooking[children_age][]=" + age;
                        }).join("&");
                    })()}`, `travelbooking[date]=${request_data_pool.date_from}`].join("&");
                
                break;

            default:
                alert("Укажите тип услуги.");
                return;
        }

        window.jsUtils.OpenWindow(`/local/modules/travelsoft.travelbooking/crm/booking_variants_window.php?${request_str}`, 800, 600);
    },

    buildDocLink: function (orderId) {

        var linkContainer = document.getElementById('link-container');
        var doctplid = document.getElementById('DOCTPL').value;
        var docformat = document.getElementById('DOCFORMAT').value;
        if (doctplid.length > 0 && docformat.length > 0) {
            linkContainer.innerHTML = '<a class="adm-btn-save" target="__blank" href="travelsoft_crm_booking_make_doc.php?VOUCHER_ID=' + orderId + '&DOC_TPL_ID=' + doctplid + '&DOCFORMAT=' + docformat + '">Скачать документ</a>';
            return;
        }

        linkContainer.innerHTML = '';
    }
};

$(document).ready(function () {

    'use strict';

    var selectsOfServiceHandlersOfChange = {

        placements: function ($this) {

            removeServicesDependencyFields(["#UF_ROOM"]);
            if ($this.val() > 0) {
                $.get("/local/modules/travelsoft.travelbooking/crm/ajax/get_rooms_by_placement_id.php",
                        {id: $this.val()}, function (resp) {

                    if ($.isArray(resp) && resp.length) {

                        renderServiceTableItem($this, "Номер", "UF_ROOM", resp);
                    }
                });
            }

        },

        packagetour: function ($this) {
            removeServicesDependencyFields(["#UF_PLACEMENT", "#UF_TRANSFER", "#UF_TRANSFER_BACK", "#UF_ROOM"]);
            if ($this.val() > 0) {
                $.get("/local/modules/travelsoft.travelbooking/crm/ajax/get_rooms_by_package_tour_id.php",
                        {id: $this.val()}, function (resp) {

                    renderServiceHiddenTableItem($this, "UF_PLACEMENT", resp.placement);
                    renderServiceHiddenTableItem($this, "UF_TRANSFER", resp.transfer);
                    renderServiceHiddenTableItem($this, "UF_TRANSFER_BACK", resp.transferback);
                    renderServiceTableItem($this, "Номер", "UF_ROOM", resp.rooms);
                });
            }
        }

    };

    function renderServiceTableItem($__, title, name, services) {

        $__.closest("tr").after(`
                        <tr>
                            <td width="40%" class="adm-detail-content-cell-l">${title}:</td>
                            <td width="60%" class="adm-detail-content-cell-r">
                                <select id="${name}" class="select2" name="${name}">
                                    <option selected="" value="">...</option>
                                    ${(function (services) {
            var html = "";
            arrayIterator(services, function (item) {
                html += `<option value="${item.id}">${item.name}</option>`;
            });
            return html;
        })(services)}
                            </td>
                        </tr>
                    `);
        $(".select2").select2();
    }
    
    function renderCalendarTableItem ($__, title, name, value) {
        
        $__.closest("tr").after(`
                                    <tr>
                                        <td width="40%" class="adm-detail-content-cell-l">${title}:<span class="required">*</span></td>
                                        <td width="60%" class="adm-detail-content-cell-r">
                                            <div class="adm-input-wrap adm-input-wrap-calendar">
                                                <input class="adm-input adm-input-calendar" size="13" type="text" name="${name}" id="${name}" value="${value || ""}">
                                                <span class="adm-calendar-icon" title="Нажмите для выбора даты" onclick="BX.calendar({node:this, field:'${name}', form: '', bTime: false, bHideTime: false});"></span>
                                            </div>
                                        </td>
                                    </tr>
                                `);
    }
    
    function renderServiceTextTableItem ($__, title, name, value) {
        $__.closest("tr").after(`
                        <tr>
                            <td width="40%" class="adm-detail-content-cell-l">${title}</td>
                            <td width="60%" class="adm-detail-content-cell-r">
                                <input type="text" name="${name}" id="${name}" value="${value || ""}">
                            </td>
                        </tr>
                    `);
    }
    
    function renderServiceHiddenTableItem($__, name, sid) {
        $__.closest("tr").after(`
                        <tr style="display:none">
                            <td width="40%" class="adm-detail-content-cell-l"></td>
                            <td width="60%" class="adm-detail-content-cell-r">
                                <input type="hidden" name="${name}" id="${name}" value="${sid}">
                            </td>
                        </tr>
                    `);
    }

    function arrayIterator(arr, callback) {
        for (var i = 0; i < arr.length; i++) {
            callback(arr[i]);
        }
    }

    function removeServicesDependencyFields(arr_selectors) {
        if (!$.isArray(arr_selectors)) {
            arr_selectors = ["#UF_SERVICE", "#UF_ROOM", "#UF_PLACEMENT", "#UF_TRANSFER", "#UF_TRANSFER_BACK", "input[name='UF_DATE_TO']", "#UF_FOOD"];
        }
        arrayIterator(arr_selectors, function (item) {
            var $item = $(item);
            if ($item.length) {

                if ($item.get(0).nodeName === "SELECT") {
                    $item.select2("destroy");
                }

                $item.closest("tr").remove();
            }
        });
    }

    $("#UF_SERVICE_TYPE").on("change", function () {

        var $this = $(this);
        var serviceType = $this.val();

        removeServicesDependencyFields();
        if (serviceType !== "") {

            $.get("/local/modules/travelsoft.travelbooking/crm/ajax/get_services_by_service_type.php",
                    {service_type: serviceType}, function (resp) {

                if ($.isArray(resp) && resp.length) {
                    
                    renderServiceTableItem($this, "Услуга", "UF_SERVICE", resp);
                    if (serviceType !== "transfer" && serviceType !== "transferback" && serviceType !== "excursiontour") {
                        renderCalendarTableItem($("input[name='UF_DATE_FROM']"), "Дата по", "UF_DATE_TO");
                        renderServiceTextTableItem($("#UF_DISCOUNT"), "Питание", "UF_FOOD");
                    }
                }
            });
            
        }

    });

    $(document).on("change", "#UF_SERVICE", function () {
        
        var service_type = $("#UF_SERVICE_TYPE").val();
        
        if (selectsOfServiceHandlersOfChange[service_type]) {
            selectsOfServiceHandlersOfChange[service_type]($(this));
        }
    });

    $(document).on("change", "#UF_PLACEMENT", function () {

        selectsOfServiceHandlersOfChange.placements($(this));

    });

    $('.select2').select2();

});