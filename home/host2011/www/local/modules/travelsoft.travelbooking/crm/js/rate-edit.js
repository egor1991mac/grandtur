/* 
 * Rate edit page
 */

$(document).ready(function () {

    var store = {

        fields: {
            service_type: $("select[name=UF_SERVICES_TYPE]"),
            duration: $('input[name=UF_DURATION]'),
            by_period: $('input[name=UF_BY_PERIOD][type=checkbox]'),
            hiddenByPeriod: $('input[name=UF_BY_PERIOD]'),
//            city: $('select[name=UF_CITY]'),
            max_adults: $('input[name=UF_MAX_ADULTS]'),
            max_children: $('input[name=UF_MAX_CHILDREN]'),
            min_adults: $('input[name=UF_MIN_ADULTS]'),
            min_children: $('input[name=UF_MIN_CHILDREN]')
        },

        events: {
            checkboxByPeriodPressed: function () {

                store.utils.toggle(store.fields.duration.closest('tr'), store.fields.by_period.is(":checked"));
            },
            serviceTypeSelected: function () {


                store.utils.clearServiceTypeRelations();

                store.utils.toggle(store.fields.hiddenByPeriod.closest('tr'), store.fields.service_type.val() === "placements" || store.fields.service_type.val() === "rooms");

                store.utils.commonProcessingExcurionsAndTransferRelations();
            }
        },

        utils: {
            clearServiceTypeRelations: function ($context) {

                store.fields.duration.val('');
                store.fields.by_period.prop('checked', false);
                store.fields.by_period.trigger('change');

//                store.fields.city.val('').trigger('change');
            },
            commonProcessingExcurionsAndTransferRelations: function () {

                var isExcurtiontour = store.fields.service_type.val() === "excursiontour";

                var isTransfer = store.fields.service_type.val() === "transfer" || store.fields.service_type.val() === "transferback";

                store.utils.toggle(store.fields.max_adults.closest('tr'), isExcurtiontour);
                store.utils.toggle(store.fields.max_children.closest('tr'), isExcurtiontour);
                store.utils.toggle(store.fields.min_adults.closest('tr'), isExcurtiontour);
                store.utils.toggle(store.fields.min_children.closest('tr'), isExcurtiontour);
//                store.utils.toggle(store.fields.city.closest('tr'), isExcurtiontour || isTransfer);
            },
            toggle: function ($context, toShow) {

                if (toShow) {
                    $context.show();
                } else {
                    $context.hide();
                }

            }

        }

    };


    store.fields.service_type.on('change', function () {

        store.events.serviceTypeSelected();
    });

    store.fields.by_period.on('change', function () {

        store.events.checkboxByPeriodPressed();
    });

});


