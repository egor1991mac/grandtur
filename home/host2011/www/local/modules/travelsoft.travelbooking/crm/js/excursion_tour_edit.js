
$(document).ready(function () {
    
    'use strict';
    
    $(".select2").on("change", function () {
        if (this.id === "UF_PLACEMENTS[]") {
            return;
        }
        var $this = $(this);
        $("input[name=UF_NAME]").val($this.find(`option[value=${$this.val()}]`).text());
    });
});
