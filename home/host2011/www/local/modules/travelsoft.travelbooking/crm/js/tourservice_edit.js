
$(document).ready(function () {
    
    'use strict';
    
    $('#add-children-price-btn').on('click', function () {
        
        var $thead = $("#children-prices tbody");
        
        $thead.append(`
            <tr>
                <td><input style="width: 100px" name="UF_CHILDREN_PRICE[]" value="" type="text"></td>
                <td><input style="width: 100px" name="UF_MIN_AGE[]" value="" type="text"></td>
                <td><input style="width: 100px" name="UF_MAX_AGE[]" value="" type="text"></td>
            </tr>
        `);
        
    });
});
