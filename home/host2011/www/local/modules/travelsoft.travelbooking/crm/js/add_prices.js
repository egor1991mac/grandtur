
$(document).ready(function () {

    "use strict";
    
    var floatNull = window.floatNull || 0.0000000001;
    
    /**
     * Объект данных, который будет отправлен при инициализации отправки формы
     * используется для подмены данных формы более точными (для ускарения обработки на сервере)
     * @type Object
     */
    var data_instead_of_the_form_data_must_be_send = null;
    
    var tableFormHidder = $("#table-hidder");
    
    /**
     * @param {DOMElement} initiator
     * @returns {undefined}
     */
    var setDataInsteadOfTheFormDataMustBeSend = function (initiator) {
        
        var form = $(initiator).closest("form");
        var arr_data = [];
        
        arr_data.push(initiator.name + "=" + initiator.value);
        arr_data.push("sessid=" + form.find("input[name=sessid]").val());
        arr_data.push("prices_and_quotas[service_id]=" + form.find("input[name='prices_and_quotas[service_id]']").val());
        arr_data.push("prices_and_quotas[service_type]=" + form.find("input[name='prices_and_quotas[service_type]']").val());
        data_instead_of_the_form_data_must_be_send = arr_data.join("&");
    };
    
    /**
     * Инициирует отправку формы
     * @param {Object} initiator
     * @returns {undefined}
     */
    var triggerFormAjaxSubmit = function (initiator) {

        $(initiator).closest('form').trigger('submit');
    };

    /**
     * Отпрака формы ajax'ом
     * @param {$} form
     * @param {Function} beforeSend
     * @param {Function} complete
     * @param {Function} success
     * @returns {undefined}
     */
    var formAjaxSubmit = function (form, beforeSend, complete, success) {
        
        var options = {

            url: "/local/modules/travelsoft.travelbooking/crm/ajax/processing_add_prices_form.php",
            method: form.attr('method'),
            data: data_instead_of_the_form_data_must_be_send ? data_instead_of_the_form_data_must_be_send : form.serialize(),
            processData: false
        };
        
        data_instead_of_the_form_data_must_be_send = null;

        if (typeof beforeSend === 'function') {

            options.beforeSend = beforeSend;
        }

        if (typeof complete === 'function') {

            options.complete = complete;
        }

        if (typeof success === 'function') {

            options.success = success;
        }

        $.ajax(options);
    };


    var processPriceAndQuotasResponse = function (data) {

        try {
            
            if (data.error) {
                throw new Error(data.error);
            }
            
            data = data.result;
            
            if (typeof data.quotas === 'object') {

                for (var uxd in data.quotas) {

                    $('input[name="prices_and_quotas[quotas][' + uxd + ']"]').val(data.quotas[uxd].quota_value >= 0 ? data.quotas[uxd].quota_value : '');
                    $('#sold-' + uxd).text(data.quotas[uxd].sold_value || 0);
                    $('#on-sale-' + uxd).text(data.quotas[uxd].onsale_value || 0);
                }
            }
            
            if (typeof data.transfer_quotas === 'object') {

                for (var uxd in data.transfer_quotas) {

                    $('input[name="prices_and_quotas[transfer_quotas][' + uxd + ']"]').val(data.transfer_quotas[uxd].quota_value >= 0 ? data.transfer_quotas[uxd].quota_value : '');
                    $('#sold-' + uxd).text(data.transfer_quotas[uxd].sold_value || 0);
                    $('#on-sale-' + uxd).text(data.transfer_quotas[uxd].onsale_value || 0);
                }
            }

            if (typeof data.stop_sale === 'object') {

                for (var uxd in data.stop_sale) {

                    $('select[name="prices_and_quotas[stop_sale][' + uxd + ']"]').val(data.stop_sale[uxd] ? 1 : 0);
                }
            }

            if (typeof data.duration === 'object') {

                for (var uxd in data.duration) {

                    $('input[name="prices_and_quotas[duration][' + uxd + ']"]').val(data.duration[uxd].length ? data.duration[uxd] : '');
                }
            }

            if (typeof data.prices === 'object') {
                
                for (var rate_id in data.prices.gross) {

                    for (var ptid in data.prices.gross[rate_id]) {

                        for (uxd in data.prices.gross[rate_id][ptid]) {
                            
                            $('input[name="prices_and_quotas[prices][gross]['+ rate_id +'][' + ptid + '][' + uxd + ']"]').val(data.prices.gross[rate_id][ptid][uxd] > 0 ? (data.prices.gross[rate_id][ptid][uxd] === floatNull ? 0 : data.prices.gross[rate_id][ptid][uxd]) : '');
                        }
                    }
                }
                
                for (var rate_id in data.prices.netto) {

                    for (var ptid in data.prices.netto[rate_id]) {

                        for (uxd in data.prices.netto[rate_id][ptid]) {
                            
                            $('input[name="prices_and_quotas[prices][netto]['+ rate_id +'][' + ptid + '][' + uxd + ']"]').val(data.prices.netto[rate_id][ptid][uxd] > 0 ? (data.prices.netto[rate_id][ptid][uxd] === floatNull ? 0 : data.prices.netto[rate_id][ptid][uxd]) : '');
                        }
                    }
                }
            }
            
            paintTable();
        } catch (error) {

            alert(error.message);
        }
    };
    
    var paintTable = function () {
        
        var colors = {
            red: "indianred",
            green: "lightgreen"
        };
        
        var color = null;
        
        var paintGroups = {};
        
        $(".paint").each(function () {
            var $this = $(this);
            if (typeof paintGroups[$this.data("group")] === 'undefined') {
                paintGroups[$this.data("group")] = [];
            }
            paintGroups[$this.data("group")].push($this);
        });
        
        for (var group in paintGroups) {
            color = null;
            for (var i = 0; i < paintGroups[group].length; i++) {
                
                if (typeof paintGroups[group][i] === 'undefined') {
                    continue;
                }
                
                if (paintGroups[group][i].attr("name") === "prices_and_quotas[quotas]["+group+"]") {
                    if (paintGroups[group][i].val() === "0") {
                        color = colors.red;
                        break;
                    } else if (paintGroups[group][i].val() > 0) {
                        color = colors.green;
                    }
                }
                
                else if (paintGroups[group][i].attr("name") === "prices_and_quotas[stop_sale]["+group+"]") {
                    if (paintGroups[group][i].val() === "1") {
                        color = colors.red;
                        break;
                    } else if (paintGroups[group][i].val() === "0") {
                        color = colors.green;
                    }
                }
            }
            
            if (color) {
                for (var i = 0; i < paintGroups[group].length; i++) {
                    
                if (typeof paintGroups[group][i] === 'undefined') {
                    continue;
                }
                    paintGroups[group][i].parent("td").css({"background-color": color});
                }
            }
        }
    };
    
    var $tableGeneratorForm = $("#table-generator-form");

    window.Travelsoft = {

        /**
         * @type Object
         */
        _popupFroms: {},

        /**
         * Инициализирует показ попап формы массового редактирования цен и квот
         * @param {Object} popup
         * @returns {undefined}
         */
        initPopupForm: function (popup) {

            if (typeof Travelsoft._popupFroms[popup.id] === 'undefined') {

                Travelsoft._popupFroms[popup.id] = new BX.CDialog(popup);
            }

            Travelsoft._popupFroms[popup.id].Show();
        },

        /**
         * Инициирует отправку формы заполнения цен и квот
         * @param {Object} initiator
         * @returns {undefined}
         */
        triggerPriceAndQuotasFormAjaxSubmit: function (initiator) {
            setDataInsteadOfTheFormDataMustBeSend(initiator);
            triggerFormAjaxSubmit(initiator);
        },

        /**
         * Отправка формы заполнения цен и квот
         * @param {Object} form
         * @returns {Boolean}
         */
        priceAndQuotasFormAjaxSubmit: function (form) {

            formAjaxSubmit($(form), function () {
                tableFormHidder.show();
            }, function () {
                tableFormHidder.hide();
            }, processPriceAndQuotasResponse);

            return false;
        },

        /**
         * Отправка формы массового редактирования цен и квот
         * @param {String} formid
         * @returns {Boolean}
         */
        massEditFormAjaxSubmit: function (formid) {

            formAjaxSubmit($('#' + formid), null, function () {
                BX.WindowManager.Get().Close();
            }, processPriceAndQuotasResponse);
            return false;
        }

    };

    $tableGeneratorForm.on("submit", function (e) {

        var errors = [];

        if ($("#service_type").val() <= 0) {
            errors.push("Выберите тип услуги для заполнения цен");
        }

        if ($("#service_id").val() <= 0) {
            errors.push("Выберите услугу для заполнения цен");
        }

        if (!$("#date_from").val()) {
            errors.push("Выберите 'дату с' для заполнения цен");
        }

        if (!$("#date_to").val()) {
            errors.push("Выберите 'дату по' для заполнения цен");
        }

        if (errors.length) {
            alert(errors.join("\n"));
            e.preventDefault();
        }
    });
    
    paintTable();
    tableFormHidder.css({height: tableFormHidder.parent().outerHeight() + "px"});
});
