<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Discounts;
use travelsoft\booking\crm\Utils;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='/local/modules/travelsoft.travelbooking/crm/css/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/common.js?".randString(7)."'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление комиссии';
    if ($ID > 0) {

        $arDiscounts = Discounts::getById($ID);

        if (!$arDiscounts['ID']) {

            throw new Exception('Комиссия с ID="' . $ID . '" не найдена');
        }

        $title = 'Редактирование комиссии #' . $arDiscounts['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\DiscountsUtils::processingDiscountEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<?
    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    $arTabs = array(
        array(
            "DIV" => "DISCOUNT",
            "TAB" => 'Комиссия',
            "TITLE" => 'Комиссия',
            'CONTENT' => travelsoft\booking\crm\DiscountsUtils::getDiscountsFieldsContent((array) $arDiscounts)
    ));

    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'discount_form',
        'id' => 'discount_form',
        'tabs' => $arTabs,
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

