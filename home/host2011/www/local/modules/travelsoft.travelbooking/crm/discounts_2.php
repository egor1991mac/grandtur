<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Discounts;
use travelsoft\booking\crm\Settings;
use Bitrix\Main\Entity\ExpressionField;

require_once 'header.php';

$TABLE_ID = Settings::DISCOUNTS_LIST_HTML_TABLE_ID;

$sort = new CAdminSorting($TABLE_ID, "ID", "DESC");
$list = new CAdminList($TABLE_ID, $sort);

if ($arDiscountsId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arDiscountsId = array_keys(Discounts::get(array('select' => array('ID'))));
    }

    foreach ($arDiscountsId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Discounts::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("filter" => \travelsoft\booking\crm\DiscountsUtils::getDiscountsFilter(), "order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        $TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {
    
    $arTotalCount = Discounts::get(array('filter' => \travelsoft\booking\crm\DiscountsUtils::getDiscountsFilter(), 'select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $arTotalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arDiscounts = Discounts::get($getParams);

$dbResult = new CAdminResult($arDiscounts, $TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID комиссии",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_LP_FROM",
        "content" => "Срок действия комиссии с",
        "sort" => "UF_LP_FROM",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_LP_TO",
        "content" => "Срок действия комиссии по",
        "sort" => "UF_LP_TO",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICE_TYPE",
        "content" => "Тип услуги",
        "sort" => "UF_SERVICE_TYPE",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICES",
        "content" => "Услуги",
        "sort" => "UF_SERVICES",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_AGENT",
        "content" => "Агент",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SORT",
        "content" => "Сортировка",
        "sort" => "UF_SORT",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_VALUE",
        "content" => "Размер комиссии",
        "sort" => "UF_VALUE",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_DTYPE",
        "content" => "Тип комиссии",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_VARIANT",
        "content" => "% или фиксированная",
        "sort" => "UF_VARIANT",
        "align" => "center",
        "default" => true
    )
));

$DTYPE = array("A" => "Скидка для агенств", "PR" => "Промо скидка");

$variants = array(
    "F" => "фиксированная",
    "P" => "процент"
);

$serviceTypes = \travelsoft\booking\Settings::getServicesTypes();

while ($arResult = $dbResult->Fetch()) {

    $row = &$list->AddRow($arResult["ID"], $arResult);

    $row->AddViewField("UF_DTYPE", $DTYPE[$arResult["UF_DTYPE"]]);
    $row->AddViewField("UF_VARIANT", $variants[$arResult["UF_VARIANT"]]);
    
    if ($arResult["UF_AGENT"] > 0) {
        $arAgent = current(travelsoft\booking\stores\Users::get(array('filter' => array('ID' => $arResult["UF_AGENT"]), 'select' => array('ID', 'UF_LEGAL_NAME'))));
        $row->AddViewField("UF_AGENT", $arAgent["UF_LEGAL_NAME"]);
    }
    
    if (strlen($arResult["UF_SERVICE_TYPE"]) > 0) {
        $row->AddViewField("UF_SERVICE_TYPE", $serviceTypes[$arResult["UF_SERVICE_TYPE"]]["name"]);
    } else {
        $row->AddViewField("UF_SERVICE_TYPE", "Все");
    }
    
    if (!empty($arResult["UF_SERVICES"])) {
        $arServicesNames = array();
        
        if (!isset($object[$arResult["UF_SERVICE_TYPE"]])) {
            $object[$arResult["UF_SERVICE_TYPE"]] = current(travelsoft\booking\crm\Utils::getBookingServices(
                    array($arResult["UF_SERVICE_TYPE"])));
        }
        
        foreach ($arResult["UF_SERVICES"] as $service_id) {
            
            if (strlen($object[$arResult["UF_SERVICE_TYPE"]][$service_id]["NAME"]) > 0) {
                $arServicesNames[] = $object[$arResult["UF_SERVICE_TYPE"]][$service_id]["NAME"];
            } elseif (strlen($object[$arResult["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"]) > 0) {
                $arServicesNames[] = $object[$arResult["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"];
            }
        }
        
        if (!empty($arServicesNames)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arServicesNames));
        }
    } else {
        $row->AddViewField("UF_SERVICES", "Все");
    }
    
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect(Settings::DISCOUNT_EDIT_URL . "?ID=" . $arResult["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить комиссии')) " . $list->ActionDoGroup($arResult["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Добавить комиссии",
        'TITLE' => "Добавить комиссии",
        'LINK' => Settings::DISCOUNT_EDIT_URL . '?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список скидок");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arr_services_id = $arr_agents_id = -1;
foreach (Discounts::get(array("select" => array("ID", "UF_SERVICES"))) as $arr_discount) {
    if (!empty($arr_discount["UF_SERVICES"])) {
        $arr_services_id[] = $arr_discount["UF_SERVICE_ID"];
    }
    if ($arr_discount["UF_AGENT"] > 0) {
        $arr_agents_id[] = $arr_discount["UF_AGENT"];
    }
}

$arFilterSelectAgentsData = \travelsoft\booking\crm\Utils::getReferencesSelectData(
                travelsoft\booking\stores\Users::get(array('filter' => array("ID" => $arr_agents_id), 'select' => array("ID", "UF_LEGAL_NAME"))), 'UF_LEGAL_NAME', 'ID');

$arFilterSelectVariantsData = \travelsoft\booking\crm\Utils::getReferencesSelectData(array(
            array("ID" => "F", "NAME" => "фиксированная"),
            array("ID" => "P", "NAME" => "процент")
                ), "NAME", "ID");

$arFilterSelectDTypeData = \travelsoft\booking\crm\Utils::getReferencesSelectData(array(
            array("ID" => "A", "NAME" => "Комиссия для агенств"),
            array("ID" => "PR", "NAME" => "Промо скидка")
                ), "NAME", "ID");

\travelsoft\booking\crm\Utils::showFilterForm(
        array(
            'table_id' => Settings::PAYMENT_HISTORY_HTML_TABLE_ID,
            'form_elements' => array(
                array(
                    'label' => 'Срок действия комиссии с',
                    'view' => CAdminCalendar::CalendarDate('UF_LP_FROM', $_GET['UF_LP_FROM'], 19, true)
                ),
                array(
                    'label' => 'Срок действия комиссии по',
                    'view' => CAdminCalendar::CalendarDate('UF_LP_TO', $_GET['UF_LP_TO'], 19, true)
                ),
                array(
                    'label' => 'Агент',
                    'view' => SelectBoxFromArray("UF_AGENT", $arFilterSelectAgentsData, $_GET['UF_AGENT'], "", 'class="adm-filter-select"', false, "find_form")
                ),
                array(
                    'label' => '% или фиксированная',
                    'view' => SelectBoxFromArray("UF_VARIANT", $arFilterSelectVariantsData, $_GET['UF_VARIANT'], "", 'class="adm-filter-select"', false, "find_form")
                ),
                array(
                    'label' => 'Тип скидки',
                    'view' => SelectBoxFromArray("UF_DTYPE", $arFilterSelectDTypeData, $_GET['UF_DTYPE'], "", 'class="adm-filter-select"', false, "find_form")
                ),
            )
        )
);

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
