<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arTemplateParameters = array(

        "SHOW_PLACEMENTS_TAB" => array(
            "PARENT" => "BASE",
            "NAME" => "Показывать вкладку Проживание",
            "TYPE" => "CHECKBOX",
            "REFRESH" => "Y"
        ),
        "SHOW_PACKAGE_TOUR_TAB" => array(
            "PARENT" => "BASE",
            "NAME" => "Показывать вкладку Туры",
            "TYPE" => "CHECKBOX",
            "REFRESH" => "Y"
        ),
        "SHOW_TRAVEL_TAB" => array(
            "PARENT" => "BASE",
            "NAME" => "Показывать вкладку Проезд",
            "TYPE" => "CHECKBOX",
            "REFRESH" => "Y"
        )
    
);

if (
        $arCurrentValues["SHOW_PLACEMENTS_TAB"] === "Y" 
        || $arCurrentValues["SHOW_PACKAGE_TOUR_TAB"] === "Y"
        || $arCurrentValues["SHOW_TRAVEL_TAB"] === "Y" 
) {
    
    if ($arCurrentValues["SHOW_PLACEMENTS_TAB"] === "Y") {
        $arTemplateParameters["PLACEMENTS_SEARCH_PAGE"] = array(
            "PARENT" => "BASE",
            "NAME" => "Страница поиска для проживаний",
            "TYPE" => "STRING",
        );
    }
    
    if ($arCurrentValues["SHOW_PACKAGE_TOUR_TAB"] === "Y") {
        $arTemplateParameters["PACKAGE_TOUR_SEARCH_PAGE"] = array(
            "PARENT" => "BASE",
            "NAME" => "Страница поиска для туров",
            "TYPE" => "STRING",
        );
    }
    
    if ($arCurrentValues["SHOW_TRAVEL_TAB"] === "Y") {
        $arTemplateParameters["TRAVEL_SEARCH_PAGE"] = array(
            "PARENT" => "BASE",
            "NAME" => "Страница поиска для проезда",
            "TYPE" => "STRING",
        );
    }
    
    $arTemplateParameters["TAB_ACTIVE"] = array(
        "PARENT" => "BASE",
        "NAME" => "Активная вкладка по-умолчанию",
        "TYPE" => "LIST",
        "VALUES" => array(
            "PLACEMENTS" => "Проживание",
            "TRAVEL" => "Проезд",
            "PACKAGE_TOUR" => "Туры"
        )
    );
       
}

?>