<?php
$MESS["TRAVELBOOKING_DATE_FROM"] = "Дата тура";
$MESS["TRAVELBOOKING_ADULTS"] = "Взрослых";
$MESS["TRAVELBOOKING_CHILDREN"] = "Детей";
$MESS["TRAVELBOOKING_WITHOUT_CHILDREN"] = "Без детей";
$MESS["TRAVELBOOKING_SEARCH_BTN"] = "Поиск";
$MESS["TRAVELBOOKING_DETAIL_SEARCH_FORM_TITLE"] = "Форма поиска предложений";

