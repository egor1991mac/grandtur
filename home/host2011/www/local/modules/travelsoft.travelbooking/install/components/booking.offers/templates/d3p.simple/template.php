<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);

$randString = randString(7);

$this->addExternalCss("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.css");
$this->addExternalJs("/local/modules/travelsoft.travelbooking/plugins/ResponsiveSlides/responsiveslides.min.js");

if ($arResult["OFFERS"]):
    ?>
    <div class="offer-box container" id="offers-box-<?= $randString ?>">
        <? if ($arResult["NEAREST_AVAILABLE"]): ?>

            <div class="row">
                <div class="col-md-12 nearest-available-text">
                    <?= GetMessage("TRAVELBOOKING_NEAREST_AVAILABLE_TEXT") ?>
                </div>
            </div>

        <? endif ?>
        <?
        foreach ($arResult["OFFERS"] as $arOffer):
            $show_popup = (isset($arOffer["PICTURES"]) && !empty($arOffer["PICTURES"])) || strlen($arOffer["DESCRIPTION"]) > 0;
            $offer_rand_string = randString(5);
            ?>
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row offer-row">
                        <div <? if ($show_popup): ?>data-target="#offer-modal-<?= $offer_rand_string ?>"<? endif ?> class="col-md-4 offers-name-box">
                            <div class="offer-name <? if (in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>mt-26<? endif; ?>"><a rel="nofollow" href="javascript:void(0)"><?= $arOffer["NAME"] ?></a></div>
                            <? if (isset($arOffer["PICTURE_PATH"]) && strlen($arOffer["PICTURE_PATH"]) > 0): ?>
                                <div class="photo"><img src="<?= $arOffer["PICTURE_PATH"] ?>" alt="<?= $arOffer["NAME"] ?>"></div>
                            <? endif ?>
                        </div>
                        <div class="col-md-4">
                            <ul class="offer-details">
                                <? if (in_array($arResult["SERVICE_TYPE"], ["packagetour", "transfer", "transferback"])): ?>
                                    <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL") ?> <?= $arOffer["DATE_FROM_TRANSFER"] ?></li>
                                <? endif ?>
                                <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>
                                    <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") ?> <?= $arOffer["DATE_FROM_PLACEMENT"] ?></li>
                                    <li><?= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") ?> <?= $arOffer["DATE_TO_PLACEMENT"] ?></li>
                                <? endif; ?>
                                <? if ($arResult["SERVICE_TYPE"] === "packagetour"): ?>    
                                    <li><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL") ?> <?= $arOffer["DATE_BACK_TRANSFER"] ?></li>
                                <? endif; ?>
                                <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>
                                    <li><?= GetMessage("TRAVELBOOKING_NIGHTS") ?> <?= $arOffer["NIGHTS"] ?></li>
                                    <li><?= GetMessage("TRAVELBOOKING_FOOD") ?> <?= ($arOffer["FOOD"]["NAME"] ? $arOffer["FOOD"]["NAME"] : GetMessage("TRAVELBOOKING_WITHOUT_FOOD")) ?></li>
                                <? endif; ?>
                                <? if ($arResult["SERVICE_TYPE"] === "excursiontour"): ?>    
                                    <li><?= GetMessage("TRAVELBOOKING_DATE_TOUR") ?> <?= $arOffer["DATE_FROM"] ?></li>
                                <? endif; ?>
                            </ul>
                        </div>
                        <div class="col-md-2 offers-price-box">
                            <span class="offer-price"><?= $arOffer["FORMATTED_PRICE"] ?></span><br>
                            <small class="offer-price-usd"><?= $arOffer["FORMATTED_PRICE_USD"] ?></small>
                        </div>
                        <div class="col-md-2 offers-booking-btn-box text-center">
                            <span class="loading hidden"></span>
                            <a rel="nofollow" href="<?= $arResult["BOOKING_URL"] ?>?add2basket=<?= $arOffer["ADD2BASKET"] ?>&sessid=<?= bitrix_sessid() ?>" class="add2cart btn btn-primary btn-lg"><?= GetMessage("TRAVELBOOKING_BTN") ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <? if ($show_popup): ?>
                <div class="modal fade" id="offer-modal-<?= $offer_rand_string ?>" tabindex="-1" role="dialog" aria-labelledby="offer-lable-<?= $offer_rand_string ?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="offer-lable-<?= $offer_rand_string ?>"><?= $arOffer["NAME"] ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <? if (!empty($arOffer["PICTURES"])): ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <ul class="rslides" id="offer-carousel-<?= $offer_rand_string ?>">
                                                <? foreach ($arOffer["PICTURES"] as $img_path): ?>
                                                    <li><img src="<?= $img_path ?>" alt="<?= $arOffer["NAME"] ?>"></li>
                                                <? endforeach ?>
                                            </ul>
                                        </div>
                                    <? endif ?>
                                    <? if (strlen($arOffer["DESCRIPTION"]) > 0): ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="offer-description"><?= $arOffer["DESCRIPTION"] ?></div>
                                        </div>
                                    <? endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif ?>
        <? endforeach; ?>
    </div>
    <script>Travelsoft.setModalShowHandler();</script>
<? else: ?>
    <div class="offer-box container" id="offers-box-<?= $randString ?>"><?= GetMessage("TRAVELBOOKING_SEARCH_TEXT") ?></div>
    <template id="offers-item-template-<?= $randString ?>">

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row offer-row">
                    <div data-target="#offer-modal-{{rand_string}}" class="col-md-4 offers-name-box">
                        <div class="offer-name <? if (in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>mt-26<? endif; ?>"><a rel="nofollow" href="javascript:void(0)">{{offer_name}}</a></div>
                        {{offer_preview_image}}
                    </div>
                    <div class="col-md-4">
                        <ul class="offer-details">
                            <? if (in_array($arResult["SERVICE_TYPE"], ["packagetour", "transfer", "transferback"])): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL") ?> {{offer_date_from_transfer}}</li>
                            <? endif ?>
                            <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_PLACEMENT") ?> {{offer_date_from_placement}}</li>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_TO_PLACEMENT") ?> {{offer_date_to_placement}}</li>
                            <? endif; ?>
                            <? if ($arResult["SERVICE_TYPE"] === "packagetour"): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL") ?> {{offer_date_back_transfer}}</li>
                            <? endif ?>
                            <? if (!in_array($arResult["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])): ?>
                                <li><?= GetMessage("TRAVELBOOKING_NIGHTS") ?> {{offer_nights}}</li>
                                <li><?= GetMessage("TRAVELBOOKING_FOOD") ?> {{offer_food}}</li>
                            <? endif ?>
                            <? if ($arResult["SERVICE_TYPE"] === "excursiontour"): ?>    
                                <li><?= GetMessage("TRAVELBOOKING_DATE_TOUR") ?> {{offer_date}}</li>
                            <? endif; ?>
                        </ul>
                    </div>
                    <div class="col-md-2 offers-price-box">
                        <span class="offer-price">{{offer_price}}</span><br>
                        <small class="offer-price-usd">{{offer_price_usd}}</small>
                    </div>
                    <div class="col-md-2 offers-booking-btn-box">
                        <span class="loading hidden"></span>
                        <a rel="nofollow" href="<?= $arResult["BOOKING_URL"] ?>?add2basket={{offer_add_to_basket}}&sessid=<?= bitrix_sessid() ?>" class="add2cart btn btn-primary btn-lg"><?= GetMessage("TRAVELBOOKING_BTN") ?></a>
                    </div>
                </div>
            </div>
        </div>
        {{offer_modal_box}}
    </template>
    <template id="nearest-available-text-template-<?= $randString ?>">
        <div class="row">
            <div class="col-md-12 nearest-available-text">
                <?= GetMessage("TRAVELBOOKING_NEAREST_AVAILABLE_TEXT") ?>
            </div>
        </div>
    </template>

    <template id="modal-box-template-<?= $randString ?>">
        <div class="modal fade" id="offer-modal-{{rand_string}}" tabindex="-1" role="dialog" aria-labelledby="offer-lable-{{rand_string}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="offer-lable-{{rand_string}}">{{offer_name}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            {{offer_pictures_box}}
                            {{offer_description_box}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>

    <template id="pictures-box-template-<?= $randString ?>">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="rslides" id="offer-carousel-{{rand_string}}">
                {{offer_pictures_items}}
            </ul>
        </div>
    </template>
    <template id="picture-item-template-<?= $randString ?>">
        <li>
            <img src="{{offer_picture_path}}" alt="{{offer_name}}">
        </li>
    </template>
    <template id="description-box-template-<?= $randString ?>">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="offer-description">{{offer_description}}</div>
        </div>
    </template>

    <template id="preview-image-box-template-<?= $randString ?>">
        <div class="photo"><img src="{{offer_image}}" alt="{{offer_name}}"></div>
    </template>

    <script>
        Travelsoft.loadOffers({
            ajaxUrl: '<?= $componentPath . "/ajax/search_offers.php" ?>',
            request: '<?= json_encode(array("travelbooking" => $arResult["REQUEST"])) ?>',
            service_type: '<?= $arResult["SERVICE_TYPE"] ?>',
            insertion_selector: "#offers-box-<?= $randString ?>",
            offers_item_template_id_selector: "#offers-item-template-<?= $randString ?>",
            show_nearest_available_text: <? if (isset($_REQUEST["travelbooking"]) && !empty($_REQUEST["travelbooking"])): ?>true<? else: ?>false<? endif ?>,
                    nearest_available_text_template_id_selector: "#nearest-available-text-template-<?= $randString ?>",
                    modal_box_template_id_selector: "#modal-box-template-<?= $randString ?>",
                    pictures_box_template_id_selector: "#pictures-box-template-<?= $randString ?>",
                    picture_item_template_id_selector: "#picture-item-template-<?= $randString ?>",
                    description_box_template_id_selector: "#description-box-template-<?= $randString ?>",
                    preview_image_box_template_id_selector: "#preview-image-box-template-<?= $randString ?>",
                    messages: {
                        not_found: "<?= GetMessage("TRAVELBOOKING_OFFERS_NOT_FOUND", ["#phone#" => ""]) ?>",
                        no_food: "<?= GetMessage("TRAVELBOOKING_WITHOUT_FOOD") ?>"
                    }
                });
    </script>
<? endif; ?>
<script>
<? if ($_REQUEST["s2o"] === "Y"): ?>
        Travelsoft.scrolltoInit(document.querySelector(".offer-box"), 200);
<? endif ?>
    Travelsoft.add2cartInit(<? if ($arResult["IS_AVAIL_BOOKING"]): ?>true<? else: ?>false<? endif; ?>, "<?= GetMessage("TRAVELBOOKING_NOT_AVAIL_BOOKING") ?>");
</script>
