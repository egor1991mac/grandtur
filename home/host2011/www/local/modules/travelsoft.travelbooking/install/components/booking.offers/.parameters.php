<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "SERVICE_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => "Тип услуги для поиска цен",
            "TYPE" => "LIST",
            "VALUES" => array(
                "placements" => "Проживание",
                "packagetour" => "Пакетный тур",
                "travel" => "Проезд"
            )
        ),
        "SERVICES_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID услуг(и)",
            "TYPE" => "STRING"
        ),
        "BOOKING_URL" => array(
            "PARENT" => "BASE",
            "NAME" => "URL страницы бронирования",
            "TYPE" => "STRING"
        )
    )
);

?>