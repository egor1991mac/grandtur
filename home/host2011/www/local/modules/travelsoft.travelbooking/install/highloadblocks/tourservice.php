<?php



return array(
    "table" => "ts_tourservice",
    "table_data" => array(
        "NAME" => "TSTOURSERVICES",
        "ERR" => "Ошибка при создании highloadblock'a туруслуга",
        "LANGS" => array(
            "ru" => 'Таблица Туруслуга',
            "en" => "Tourservice"
        ),
        "OPTION_PARAMETER" => "TOURSERVICE_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_TYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип услуги" ',
                'en' => 'An error in completing the field "Service type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICES",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Service',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Service',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Service',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Услуги" ',
                'en' => 'An error in completing the field "Service"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ADULTS_PRICE",
            "USER_TYPE_ID" => 'double',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'PRECISION' => 2,
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Стоимость для взрослых',
                'en' => 'Cost for adults',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Стоимость для взрослых',
                'en' => 'Cost for adults',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Стоимость для взрослых',
                'en' => 'Cost for adults',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Стоимость для взрослых" ',
                'en' => 'An error in completing the field "Cost for adults"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CHILDREN_PRICE",
            "USER_TYPE_ID" => 'double',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'PRECISION' => 2,
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Стоимость для детей',
                'en' => 'Cost for children',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Стоимость для детей',
                'en' => 'Cost for children',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Стоимость для детей',
                'en' => 'Cost for children',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Стоимость для детей" ',
                'en' => 'An error in completing the field "Cost for children"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CURRENCY",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'PRECISION' => 2,
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Валюта" ',
                'en' => 'An error in completing the field "Currency"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
