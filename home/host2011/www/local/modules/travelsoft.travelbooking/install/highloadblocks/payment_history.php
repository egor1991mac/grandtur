<?php



return array(
    "table" => "ts_payment_history",
    "table_data" => array(
        "NAME" => "TSPAYMENTHISTORY",
        "ERR" => "Ошибка при создании highloadblock'a истории платежей",
        "LANGS" => array(
            "ru" => 'Таблица История платежей',
            "en" => "Payment history"
        ),
        "OPTION_PARAMETER" => "PAYMENT_HISTORY_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_COURSE_INFO",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Курс на момент оплаты',
                'en' => 'Course',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Курс на момент оплаты',
                'en' => 'Course',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Курс на момент оплаты',
                'en' => 'Course',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Курс на момент оплаты" ',
                'en' => 'An error in completing the field "Course"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE_CREATE",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата создания" ',
                'en' => 'An error in completing the field "Date create"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CREATER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Создатель" ',
                'en' => 'An error in completing the field "Creater"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ORDER_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID путевки" ',
                'en' => 'An error in completing the field "Voucher id"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PAYMENT_TYPE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип платежа',
                'en' => 'payment type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип платежа',
                'en' => 'payment type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип платежа',
                'en' => 'payment type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип платежа" ',
                'en' => 'An error in completing the field "payment type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CASH_DESK_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Касса',
                'en' => 'Cash desk',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Касса',
                'en' => 'Cash desk',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Касса',
                'en' => 'Cash desk',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Касса" ',
                'en' => 'An error in completing the field "Cash desk"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CURRENCY",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Валюта',
                'en' => 'Currency',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Валюта" ',
                'en' => 'An error in completing the field "Currency"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PRICE",
            "USER_TYPE_ID" => 'double',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                "PRECISION" => 2,
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Стоимость',
                'en' => 'Price',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Стоимость',
                'en' => 'Price',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Стоимость',
                'en' => 'Price',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Стоимость" ',
                'en' => 'An error in completing the field "Price"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
