<?php

return array(
    "table" => "tsmessages",
    "table_data" => array(
        "NAME" => "TSMESSAGES",
        "ERR" => "Ошибка при создании highloadblock'a bookings",
        "LANGS" => array(
            "ru" => 'Таблица сообщения по путевки',
            "en" => "Vouchers messages"
        ),
        "OPTION_PARAMETER" => "VOUCHERS_MESSAGES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_USER_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID пользователя',
                'en' => 'User id',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID пользователя',
                'en' => 'User id',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID пользователя',
                'en' => 'User id',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID пользователя" ',
                'en' => 'An error in completing the field "User id"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата сообщения',
                'en' => 'Message date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата сообщения',
                'en' => 'Message date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата сообщения',
                'en' => 'Message date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата сообщения" ',
                'en' => 'An error in completing the field "Message date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MESSAGE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Сообщение',
                'en' => 'Message',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Сообщение',
                'en' => 'Message',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Сообщение',
                'en' => 'Message',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Сообщение" ',
                'en' => 'An error in completing the field "Message"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_VOUCHER_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID путевки',
                'en' => 'Voucher id',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID путевки" ',
                'en' => 'An error in completing the field "Voucher id"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        )
    )
);
