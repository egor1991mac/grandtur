<?php


return array(
    "table" => "ts_rooms",
    "table_data" => array(
        "NAME" => "TSROOMS",
        "ERR" => "Ошибка при создании highloadblock'a комнаты",
        "LANGS" => array(
            "ru" => 'Таблица Комнаты',
            "en" => "Rooms"
        ),
        "OPTION_PARAMETER" => "ROOMS_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DESCRIPTION",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Описание',
                'en' => 'Description',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Описание',
                'en' => 'Description',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Описание',
                'en' => 'Description',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Описание" ',
                'en' => 'An error in completing the field "Description"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PLACEMENT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Проживание" ',
                'en' => 'An error in completing the field "Placement"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MAIN_PLACES",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Основных мест',
                'en' => 'Main places',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Основных мест',
                'en' => 'Main places',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Основных мест',
                'en' => 'Main places',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Основных мест" ',
                'en' => 'An error in completing the field "Main places"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ADD_PLACES",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дополнительных мест',
                'en' => 'Additional places',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дополнительных мест',
                'en' => 'Additional places',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дополнительных мест',
                'en' => 'Additional places',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дополнительных мест" ',
                'en' => 'An error in completing the field "Additional places"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PICTURES",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Фото',
                'en' => 'Photo',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Фото" ',
                'en' => 'An error in completing the field "Photo"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TOTAL_COST_CALC",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                "DEFAULT_VALUE" => 0,
                'LABEL' => array(0 => "основные и доп. места", 1 => "основные места"),
                "DISPLAY" => "DROPDOWN",
                'DEFAULT_VALUE' => "",
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Общая стоимость номера включает',
                'en' => 'Total cost calculation by',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Общая стоимость номера включает',
                'en' => 'Total cost calculation by',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Общая стоимость номера включает',
                'en' => 'Total cost calculation by',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Общая стоимость номера включает" ',
                'en' => 'An error in completing the field "Total cost calculation by"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CAPACITY_CALC",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                "DEFAULT_VALUE" => 0,
                'LABEL' => array(0 => "основным местам", 1 => "основным + доп. местам"),
                "DISPLAY" => "DROPDOWN",
                'DEFAULT_VALUE' => "",
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Общая вместимость номера считается по',
                'en' => 'Total capacity calculation by',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Общая вместимость номера считается по',
                'en' => 'Total capacity calculation by',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Общая вместимость номера считается по',
                'en' => 'Total capacity calculation by',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Общая вместимость номера считается по" ',
                'en' => 'An error in completing the field "Total capacity calculation by"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
