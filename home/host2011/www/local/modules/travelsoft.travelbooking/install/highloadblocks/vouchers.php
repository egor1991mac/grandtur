<?php



return array(
    "table" => "ts_vouchers",
    "table_data" => array(
        "NAME" => "TSVOUCHERS",
        "ERR" => "Ошибка при создании highloadblock'a путевки",
        "LANGS" => array(
            "ru" => 'Таблица Путевок',
            "en" => "Vouchers"
        ),
        "OPTION_PARAMETER" => "VOUCHERS_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_BOOKINGS",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Список бронировок',
                'en' => 'Bookings list',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Список бронировок',
                'en' => 'Bookings list',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Список бронировок',
                'en' => 'Bookings list',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Список бронировок" ',
                'en' => 'An error in completing the field "Bookings list"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_STATUS",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 400,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Статус',
                'en' => 'Status',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Статус',
                'en' => 'Status',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Статус',
                'en' => 'Status',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Статус" ',
                'en' => 'An error in completing the field "Status"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
            
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CLIENT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 200,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Клиент',
                'en' => 'Client',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Клиент',
                'en' => 'Client',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Клиент',
                'en' => 'Client',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Клиент" ',
                'en' => 'An error in completing the field "Client"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MANAGER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 300,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Менеджер',
                'en' => 'Manager',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Менеджер',
                'en' => 'Manager',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Менеджер',
                'en' => 'Manager',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Менеджер" ',
                'en' => 'An error in completing the field "Manager"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE_CREATE",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата создания',
                'en' => 'Date create',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата создания" ',
                'en' => 'An error in completing the field "Date create"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
            
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_COMMENT",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 500,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Комментарий',
                'en' => 'Comment',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Комментарий',
                'en' => 'Comment',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Комментарий',
                'en' => 'Comment',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Комментарий" ',
                'en' => 'An error in completing the field "Comment"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_FILES",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 600,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Документы',
                'en' => 'Documents',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Документы',
                'en' => 'Documents',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Документы',
                'en' => 'Documents',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Документы" ',
                'en' => 'An error in completing the field "Documents"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE_EMBASSY",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 700,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата записи в посольство',
                'en' => 'Embassy date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата записи в посольство',
                'en' => 'Embassy date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата записи в посольство',
                'en' => 'Embassy date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата записи в посольство" ',
                'en' => 'An error in completing the field "Embassy date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
            
        ),
         array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE_DOCS",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 700,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата выдачи документов',
                'en' => 'Documents date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата выдачи документов',
                'en' => 'Documents date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Дата выдачи документов',
                'en' => 'Documents date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата выдачи документов" ',
                'en' => 'An error in completing the field "Documents date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
            
        )
    )
);
