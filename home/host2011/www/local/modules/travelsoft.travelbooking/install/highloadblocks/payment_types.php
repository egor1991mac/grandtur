<?php



return array(
    "table" => "ts_payment_types",
    "table_data" => array(
        "NAME" => "TSPAYMENTTYPES",
        "ERR" => "Ошибка при создании highloadblock'a типы платежей",
        "LANGS" => array(
            "ru" => 'Таблица Типы платежей',
            "en" => "Payment types"
        ),
        "OPTION_PARAMETER" => "PAYMENT_TYPES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PAYMENT_HANDLER",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Обработчик платежной системы',
                'en' => 'Payment handler',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Обработчик платежной системы',
                'en' => 'Payment handler',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Обработчик платежной системы',
                'en' => 'Payment handler',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Обработчик платежной системы" ',
                'en' => 'An error in completing the field "Payment handler"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
