<?php



return array(
    "table" => "ts_discounts",
    "table_data" => array(
        "NAME" => "TSDISCOUNTS",
        "ERR" => "Ошибка при создании highloadblock'a Конструктор скидок",
        "LANGS" => array(
            "ru" => 'Таблица Конструсктор скидок',
            "en" => "Discounts"
        ),
        "OPTION_PARAMETER" => "DISCOUNTS_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_VARIANT",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 900,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => '% или фиксированная(в валюте тура)',
                'en' => '% or fix',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => '% или фиксированная(в валюте тура)',
                'en' => '% or fix',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => '% или фиксированная(в валюте тура)',
                'en' => '% or fix',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "% или фиксированная(в валюте тура)" ',
                'en' => 'An error in completing the field "% or fix"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DTYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 700,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип скидки',
                'en' => 'Discount type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип скидки',
                'en' => 'Discount type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип скидки',
                'en' => 'Discount type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип скидки" ',
                'en' => 'An error in completing the field "Discount type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_VALUE",
            "USER_TYPE_ID" => 'double',
            "XML_ID" => "",
            "SORT" => 1000,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                "PRECISION" => 2,
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Значение',
                'en' => 'Value',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Значение',
                'en' => 'Value',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Значение',
                'en' => 'Value',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Значение" ',
                'en' => 'An error in completing the field "Value"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SORT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 1100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sort',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sort',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sort',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Сортировка" ',
                'en' => 'An error in completing the field "Sort"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_TYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип услуги" ',
                'en' => 'An error in completing the field "Service type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICES",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 200,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Services',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Services',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Услуги',
                'en' => 'Services',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Услуги" ',
                'en' => 'An error in completing the field "Services"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_AGENT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 800,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Агент',
                'en' => 'Agent',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Агент',
                'en' => 'Agent',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Агент',
                'en' => 'Agent',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Агент" ',
                'en' => 'An error in completing the field "Agent"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_LP_TO",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 600,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Скидка действует по',
                'en' => 'Discount date to',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Скидка действует по',
                'en' => 'Discount date to',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Скидка действует по',
                'en' => 'Discount date to',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Скидка действует по" ',
                'en' => 'An error in completing the field "Discount date to"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_LP_FROM",
            "USER_TYPE_ID" => 'datetime',
            "XML_ID" => "",
            "SORT" => 500,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Скидка действует с',
                'en' => 'Discount date from',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Скидка действует с',
                'en' => 'Discount date from',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Скидка действует с',
                'en' => 'Discount date from',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Скидка действует с" ',
                'en' => 'An error in completing the field "Discount date from"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
