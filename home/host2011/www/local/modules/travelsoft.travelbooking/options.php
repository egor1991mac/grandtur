<?php
if (!$USER->isAdmin())
    return;

$mid = "travelsoft.travelbooking";

\Bitrix\Main\Loader::includeModule("highloadblock");
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule($mid);

global $APPLICATION;

function renderOptions($arOptions, $mid) {

    foreach ($arOptions as $name => $arValues) {

        $cur_opt_val = htmlspecialcharsbx(Bitrix\Main\Config\Option::get($mid, $name));
        $name = htmlspecialcharsbx($name);

        $options .= '<tr>';
        $options .= '<td width="40%">';
        $options .= '<label for="' . $name . '">' . $arValues['DESC'] . ':</label>';
        $options .= '</td>';
        $options .= '<td width="60%">';
        if ($arValues['TYPE'] == 'select') {

            $options .= '<select id="' . $name . '" name="' . $name . '">';
            foreach ($arValues['VALUES'] as $key => $value) {
                $options .= '<option ' . ($cur_opt_val == $key ? 'selected' : '') . ' value="' . $key . '">' . $value . '</option>';
            }
            $options .= '</select>';
        } elseif ($arValues['TYPE'] == 'text') {

            $options .= '<input type="text" name="' . $name . '" value="' . $cur_opt_val . '">';
        }
        $options .= '</td>';
        $options .= '</tr>';
    }
    echo $options;
}

function loadFiles($dirName) {

    $directory = __DIR__ . "/" . $dirName;
    return array_diff(scandir($directory), array('..', '.'));
}

$dbHLList = Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            "order" => array("ID" => "ASC")
        ))->fetchAll();

foreach ($dbHLList as $arHL) {
    $arHLS[$arHL["ID"]] = $arHL["NAME"];
}

$dbIBList = CIBlock::GetList(
                array(), array("ACTIVE" => "Y")
);
while ($arIB = $dbIBList->Fetch()) {
    $arIBS[$arIB["ID"]] = $arIB["NAME"];
}

$dbStatuses = travelsoft\booking\stores\Statuses::get();
foreach ($dbStatuses as $arStatus) {
    $arStatuses[$arStatus['ID']] = $arStatus['UF_NAME'];
}

$dbPriceTypes = travelsoft\booking\stores\PriceTypes::get();
foreach ($dbPriceTypes as $arPriceType) {
    $arPriceTypes[$arPriceType['ID']] = $arPriceType['UF_NAME'];
}

$dbMails = CEventMessage::GetList($by = "site_id", $order = "desc", array('TYPE_ID' => "TRAVELSOFT_TRAVELBOOKING"));
while ($arMail = $dbMails->Fetch()) {
    $arMails[$arMail['ID']] = $arMail['SUBJECT'] . "(" . $arMail['ID'] . ")";
}

$dbGroupsList = Bitrix\Main\GroupTable::getList(array("select" => array("ID", "NAME")))->fetchAll();

for ($i = 0, $cnt = count($dbGroupsList); $i < $cnt; $i++) {
    $arGroups[$dbGroupsList[$i]["ID"]] = $dbGroupsList[$i]["NAME"];
}

$arr_currencies = array();
foreach (travelsoft\booking\adapters\CurrencyConverter::getAcceptableISO() as $arr_currency) {
    $arr_currencies[$arr_currency["ISO"]] = $arr_currency["ISO"];
}

$main_options = array(
    'STORES' => array(
        "COUNTRIES_STORAGE_ID" => array("DESC" => "Инфоблок с описанием стран", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "CITIES_STORAGE_ID" => array("DESC" => "Инфоблок с описанием городов", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "TRAVEL_STORAGE_ID" => array("DESC" => "Инфоблок с описанием туров", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "PLACEMENTS_STORAGE_ID" => array("DESC" => "Инфоблок с описанием размещений", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "FOOD_STORAGE_ID" => array("DESC" => "Инфоблок с описанием питания", "VALUES" => $arIBS, 'TYPE' => 'select'),
    ),
    'USER_GROUPS' => array(
        'MANAGER_GROUP_ID' => array("DESC" => "Группа пользователей для менеджеров(имеют доступ к CRM)", "VALUES" => $arGroups, 'TYPE' => 'select'),
        'AGENT_GROUP_ID' => array("DESC" => "Группа пользователей для агентов", "VALUES" => $arGroups, 'TYPE' => 'select'),
        'CLIENT_GROUP_ID' => array("DESC" => "Группа пользователей для клиентов", "VALUES" => $arGroups, 'TYPE' => 'select'),
        'CASHER_GROUP_ID' => array("DESC" => "Группа пользователей для кассиров(имеют доступ к CRM)", "VALUES" => $arGroups, 'TYPE' => 'select')
    ),
    'EMAILS' => array(
    ),
    'TOTAL_OPTS' => array(
        'SINGLE_OCCUPANCY_PRICE_TYPE_ID' => array('DESC' => "Тип цены для одноместного размещения", "VALUES" => $arPriceTypes, 'TYPE' => 'select'),
        'STATUS_ID_FOR_ORDER_CREATION' => array('DESC' => "При создании заказа устанавливать статус", "VALUES" => $arStatuses, 'TYPE' => 'select'),
        'STATUS_ID_FOR_ORDER_CANCELLATION' => array('DESC' => "Статус аннуляции путевки", "VALUES" => $arStatuses, 'TYPE' => 'select'),
        'STATUS_ID_FOR_ORDER_REQUEST_CANCELLATION' => array('DESC' => "Статус запроса на аннуляцию путевки", "VALUES" => $arStatuses, 'TYPE' => 'select'),
        'ACCOUNTING_CURRENCY' => array('DESC' => "Валюта учета", "VALUES" => $arr_currencies, 'TYPE' => 'select'),
        'VOUCHER_CURRENCY' => array('DESC' => "Валюта путевки", "VALUES" => $arr_currencies, 'TYPE' => 'select'),
        "EMAIL_FOR_NOTIFICATION" => array("DESC" => "Email для приема уведомлений модуля", 'TYPE' => 'text'),
        "PROPERTY_COUNTRY_CODE" => array("DESC" => "Код свойства связи описания тура со страной", 'TYPE' => 'text'),
        "PROPERTY_CITY_CODE" => array("DESC" => "Код свойства связи описания тура с городом", 'TYPE' => 'text')
    )
);

$highloadblocksFiles = loadFiles("install/highloadblocks");
foreach ($highloadblocksFiles as $file) {

    $arr = include "install/highloadblocks/" . $file;

    $main_options["STORES"][$arr["table_data"]["OPTION_PARAMETER"]] = array("DESC" => $arr["table_data"]["LANGS"]["ru"], "VALUES" => $arHLS, 'TYPE' => 'select');
}

$emailsFiles = loadFiles("install/emails");
foreach ($emailsFiles as $file) {

    $arr = include "install/emails/" . $file;

    $main_options["EMAILS"][$arr["OPTION_PARAMETER"]] = array("DESC" => $arr["OPTION_NAME"], "VALUES" => $arMails, 'TYPE' => 'select');
}

$tabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => "Хранение данных",
        "ICON" => "",
        "TITLE" => "Укажите необходимые инфоблоки, highloadblock'и"
    ),
    array(
        "DIV" => "edit2",
        "TAB" => "Группы пользователей",
        "ICON" => "",
        "TITLE" => "Укажите гуппы пользователей"
    ),
    array(
        "DIV" => "edit3",
        "TAB" => "Почтовые шаблоны",
        "ICON" => "",
        "TITLE" => "Укажите id почтовых шаблонов"
    ),
    array(
        "DIV" => "edit4",
        "TAB" => "Общие настройки",
        "ICON" => "",
        "TITLE" => "Укажите параметры работы с заказами"
    )
);

$o_tab = new CAdminTabControl("TravelsoftTabControl", $tabs);
if ($REQUEST_METHOD == "POST" && strlen($save . $reset) > 0 && check_bitrix_sessid()) {

    if (strlen($reset) > 0) {
        foreach ($main_options as $arBlockOption) {

            foreach (\array_keys($arBlockOption) as $name) {
                \Bitrix\Main\Config\Option::delete($mid, array('name' => $name));
            }
        }
    } else {
        foreach ($main_options as $arBlockOption) {

            foreach ($arBlockOption as $name => $arValues) {
                if (isset($_REQUEST[$name])) {
                    \Bitrix\Main\Config\Option::set($mid, $name, $_REQUEST[$name]);
                }
            }
        }
    }

    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&" . $o_tab->ActiveTabParam());
}
$o_tab->Begin();
?>

<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
<?
foreach ($main_options as $arOption) {
    $o_tab->BeginNextTab();
    renderOptions($arOption, $mid);
}
$o_tab->Buttons();
?>
    <input type="submit" name="save" value="Сохранить" title="Сохранить" class="adm-btn-save">
    <input type="submit" name="reset" title="Сбросить" OnClick="return confirm('Сбросить')" value="Сбросить">
<?= bitrix_sessid_post(); ?>
    <? $o_tab->End(); ?>
</form>
