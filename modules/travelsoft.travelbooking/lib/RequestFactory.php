<?php

namespace travelsoft\booking;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory {

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request): Request {

        $oRequest = new Request;

        if (is_array($request["services_id"]) && !empty($request["services_id"])) {

            $oRequest->services_id = \array_values(\array_map(function ($id) {
                        return intVal($id);
                    }, $request["services_id"]));
        }

        if (strlen($request["date_from"])) {
            $oRequest->date_from = Utils::dateConvert($request["date_from"]);
        }

        if (strlen($request["date_to"])) {
            $oRequest->date_to = Utils::dateConvert($request["date_to"]);
        }

        $oRequest->rates_id = \array_values(\array_map(function ($id) {
                    return intVal($id);
                }, $request["rates_id"]));

        $oRequest->adults = intVal($request["adults"]);

        $oRequest->children = intVal($request["children"]);

        $oRequest->children_age = (array) $request["children_age"];

        return $oRequest;
    }

}
