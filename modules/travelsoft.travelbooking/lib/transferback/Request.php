<?php

namespace travelsoft\booking\transferback;

/**
 * Класс запроса на поиск проездов
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\RequestFactory{
    
    /**
     * @var boolean
     */
    public $tickets_buy = true;
    
    /**
     * @var int
     */
    public $city_id = null;
}
