<?php

namespace travelsoft\booking\transferback;

/**
 * Класс контейнер для калькуляторов по проезду обратно
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorContainer extends \travelsoft\booking\abstraction\CalculatorContainer{
    
    /**
     * @param \travelsoft\booking\transferback\Calculator $calculator
     * @param string $key
     */
    public function add (\travelsoft\booking\transferback\Calculator $calculator, string $key) {
        $this->_container[$key] = $calculator;
    }
}
