<?php

namespace travelsoft\booking\transferback;

/**
 * Класс-фабрика Calculator
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorFactory {

    public static function create(\travelsoft\booking\Request $request) {
        
        if (!empty($request->services_id)) {

            $arr_services_id = $arr_dates = array();
            $people_count = $request->adults + $request->children;
            if ($request->tickets_buy) {

                $arr_tickets_quotas = \travelsoft\booking\stores\TransferQuotas::get(array(
                            "filter" => array(
                                "UF_SERVICE_ID" => $request->services_id,
                                "UF_SERVICE_TYPE" => "transferback",
                                "!=UF_QUOTA" => 0,
                                "><UF_DATE" => [\travelsoft\booking\adapters\Date::create($request->date_from), \travelsoft\booking\adapters\Date::create($request->date_to)]
                            )
                ));

                if (!empty($arr_tickets_quotas)) {

                    foreach (\travelsoft\booking\stores\Quotas::get(array(
                        "filter" => array(
                            "UF_SERVICE_ID" => $request->services_id,
                            "UF_SERVICE_TYPE" => "transferback",
                            "UF_STOP" => 0,
                            "><UF_DATE" => [\travelsoft\booking\adapters\Date::create($request->date_from), \travelsoft\booking\adapters\Date::create($request->date_to)]
                        )
                    )) as $arr_quota) {

                        $tickets_quota_in_sale = 0;
                        foreach ($arr_tickets_quotas as $arr_tickets_quota) {

                            if ($arr_tickets_quota["UF_SERVICE_ID"] === $arr_quota["UF_SERVICE_ID"] &&
                                    $arr_tickets_quota["UF_SERVICE_TYPE"] === $arr_quota["UF_SERVICE_TYPE"] &&
                                    $arr_tickets_quota["UF_DATE"]->getTimestamp() === $arr_quota["UF_DATE"]->getTimestamp()) {

                                $tickets_quota_in_sale = $arr_tickets_quota["UF_QUOTA"] - $arr_tickets_quota["UF_SOLD_NUMBER"];
                                break;
                            }
                        }

                        if ($tickets_quota_in_sale > 0) {
                            $inSale = $arr_quota["UF_QUOTA"] - $arr_quota["UF_SOLD_NUMBER"];
                            if ($inSale > 0 && $inSale >= $people_count && $inSale >= $tickets_quota_in_sale) {
                                $arr_services_id[] = $arr_quota["UF_SERVICE_ID"];
                                $arr_dates[] = $arr_quota["UF_DATE"];
                            }
                        }
                    }
                }
            } else {
                foreach (\travelsoft\booking\stores\Quotas::get(array(
                    "filter" => array(
                        "UF_SERVICE_ID" => $request->services_id,
                        "UF_SERVICE_TYPE" => "transferback",
                        "UF_STOP" => 0,
                        "><UF_DATE" => [\travelsoft\booking\adapters\Date::create($request->date_from), \travelsoft\booking\adapters\Date::create($request->date_to)]
                    )
                )) as $arr_quota) {

                    $inSale = $arr_quota["UF_QUOTA"] - $arr_quota["UF_SOLD_NUMBER"];
                    if ($inSale > 0 && $inSale >= $people_count) {
                        $arr_services_id[] = $arr_quota["UF_SERVICE_ID"];
                        $arr_dates[] = $arr_quota["UF_DATE"];
                    }
                }
            }

            if (!empty($arr_services_id)) {

                rsort($request->children_age);
                foreach ($arr_services_id as $service_id) {
                    $seating_data[$service_id] = array(
                        "adults" => $request->adults,
                        "children" => $request->children,
                        "children_age" => $request->children_age
                    );
                }

                $arr_price_types_id = $arr_rates_id = $prices_data = array();

                $arr_filter = array(
                    "UF_SERVICE_ID" => $arr_services_id,
                    "UF_SERVICE_TYPE" => "transferback",
                    "!UF_GROSS" => false,
                    "UF_DATE" => $arr_dates
                );

                if (!empty($request->rates_id)) {
                    $arr_filter["UF_RATE_ID"] = $request->rates_id;
                }

                // получаем данные для расчета цен
                foreach (\travelsoft\booking\stores\Prices::get(array(
                    "filter" => $arr_filter
                )) as $arr) {

                    if (!@$prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]][$arr["UF_DATE"]->toString()]) {
                        $prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]][$arr["UF_DATE"]->toString()] = array(
                            "prices" => null,
                            "date" => $arr["UF_DATE"]->toString(),
                            "seating" => $seating_data[$arr["UF_SERVICE_ID"]],
                            "price_types" => null,
                            "calc_types" => null,
                            "currency" => null
                        );
                    }

                    $prices_data[$arr["UF_SERVICE_ID"]][$arr["UF_RATE_ID"]][$arr["UF_DATE"]->toString()]["prices"][$arr["UF_PRICE_TYPE_ID"]] = array(
                        "gross" => $arr["UF_GROSS"],
                        "netto" => $arr["UF_NETTO"]
                    );

                    $arr_rates_id[] = $arr["UF_RATE_ID"];
                    $arr_price_types_id[] = $arr["UF_PRICE_TYPE_ID"];
                }

                if (!empty($prices_data)) {

                    // получение данных по валюте и фильтрация по городам выезда
                    $arr_filter_rates = ["ID" => array_unique($arr_rates_id), '!UF_CURRENCY' => false];
                    if ($request->city_id > 0) {
                        $arr_filter_rates["UF_CITY"] = $request->city_id;
                    }
                    $arr_rates = \travelsoft\booking\stores\Rates::get(array(
                                "filter" => $arr_filter_rates,
                                "select" => array("UF_CURRENCY", "ID")
                    ));

                    // получение данных по типам цен
                    $arr_price_types = \travelsoft\booking\stores\PriceTypes::get(array(
                                "filter" => array(
                                    "ID" => array_unique($arr_price_types_id)
                                ),
                                "select" => array("UF_MAX_AGE", "UF_MIN_AGE", "UF_CALC_TYPE", "ID")
                    ));

                    foreach ($prices_data as $service_id => $arr_) {
                        foreach ($arr_ as $rate_id => $arr___) {
                            foreach ($arr___ as $date => $arr__) {
                                if (isset($arr_rates[$rate_id])) {
                                    $prices_data[$service_id][$rate_id][$date]["currency"] = $arr_rates[$rate_id]["UF_CURRENCY"];
                                } else {
                                    unset($prices_data[$service_id][$rate_id]);
                                    continue;
                                }
                                foreach (array_keys($arr__["prices"]) as $pt_id) {
                                    if (isset($arr_price_types[$pt_id])) {
                                        $prices_data[$service_id][$rate_id][$date]["price_types"][$pt_id] = $arr_price_types[$pt_id];
                                        $arr_calc_types_id[] = $arr_price_types[$pt_id]["UF_CALC_TYPE"];
                                    }
                                }
                            }
                        }
                    }

                    if ($arr_calc_types_id) {
                        $arr_calc_types = \travelsoft\booking\stores\CalculationTypes::get(array(
                                    "filter" => array("ID" => array_unique($arr_calc_types_id))
                        ));

                        foreach ($prices_data as $service_id => $arr_) {
                            foreach ($arr_ as $rate_id => $arR) {
                                foreach ($arR as $date => $arr__) {
                                    foreach ($arr__["price_types"] as $arr___) {
                                        $prices_data[$service_id][$rate_id][$date]["calc_types"][$arr___["UF_CALC_TYPE"]] = $arr_calc_types[$arr___["UF_CALC_TYPE"]]["UF_METHOD"];
                                    }
                                }
                            }
                        }
                    }

                    // поиск туруслуг
                    $tourservices = \travelsoft\booking\Utils::findTourservices($arr_services_id, "transferback", $request->adults, $request->children, $request->city_id);

                    // поиск наценок
                    $markup = array();

                    $arr_markup = \travelsoft\booking\stores\Markup::get(array(
                                "filter" => array(
                                    "UF_SERVICES" => !empty($arr_services_id) ? $arr_services_id : array(-1),
                                    "UF_SERVICE_TYPE" => "transferback"),
                                "limit" => 1
                    ));

                    $arr_total_markup = current(\travelsoft\booking\stores\Markup::get(array(
                                "filter" => array(
                                    "UF_SERVICES" => false,
                                    "UF_SERVICE_TYPE" => "transferback"),
                                "limit" => 1
                    )));

                    if (!empty($arr_markup) || !empty($arr_total_markup)) {

                        foreach ($arr_services_id as $service_id) {
                            $key = false;
                            foreach ($arr_markup as $arrr_markup) {
                                $key = array_search($service_id, $arrr_markup["UF_SERVICES"]);
                                if ($key !== false) {
                                    $markup[$service_id] = array(
                                        "price" => (float) $arrr_markup["UF_PRICE"],
                                        "currency" => (string) $arrr_markup["UF_CURRENCY"],
                                    );
                                    break;
                                }
                            }
                            if ($key === false && !empty($arr_total_markup)) {
                                $markup[$service_id] = array(
                                    "price" => (float) $arr_total_markup["UF_PRICE"],
                                    "currency" => (string) $arr_total_markup["UF_CURRENCY"]
                                );
                            }
                        }
                    }

                    // поиск скидок
                    $discount = array();
                    $time = \travelsoft\booking\adapters\Date::createFromTimestamp(time());
                    if (\travelsoft\booking\adapters\User::isAgent()) {

                        $arr_discount = (array) current(\travelsoft\booking\stores\Discounts::get(array(
                                            "filter" => array(
                                                "UF_SERVICES" => $arr_services_id,
                                                "UF_SERVICE_TYPE" => "transferback",
                                                "UF_DTYPE" => "A",
                                                "!UF_VALUE" => false,
                                                "UF_AGENT" => \travelsoft\booking\adapters\User::id(),
                                                "<=UF_LP_FROM" => $time,
                                                ">=UF_LP_TO" => $time
                                            ),
                                            "order" => array(
                                                "UF_SORT" => "ASC",
                                                "ID" => "DESC"),
                                            "limit" => 1)));

                        if (empty($arr_discount)) {

                            $arr_discount = (array) current(\travelsoft\booking\stores\Discounts::get(array(
                                                "filter" => array(
                                                    "UF_SERVICES" => $arr_services_id,
                                                    "UF_SERVICE_TYPE" => "transferback",
                                                    "UF_DTYPE" => "A",
                                                    "!UF_VALUE" => false,
                                                    "<=UF_LP_FROM" => $time,
                                                    ">=UF_LP_TO" => $time
                                                ),
                                                "order" => array(
                                                    "UF_SORT" => "ASC",
                                                    "ID" => "DESC"),
                                                "limit" => 1)));

                            if (empty($arr_discount)) {

                                $arr_discount = (array) current(\travelsoft\booking\stores\Discounts::get(array(
                                                    "filter" => array(
                                                        "UF_SERVICES" => $arr_services_id,
                                                        "UF_SERVICE_TYPE" => "transferback",
                                                        "UF_DTYPE" => "PR",
                                                        "<=UF_LP_FROM" => $time,
                                                        ">=UF_LP_TO" => $time
                                                    ),
                                                    "order" => array(
                                                        "UF_SORT" => "ASC",
                                                        "ID" => "DESC"),
                                                    "limit" => 1)));
                            }
                        }
                    } else {

                        $arr_discount = (array) current(\travelsoft\booking\stores\Discounts::get(array(
                                            "filter" => array(
                                                "UF_SERVICES" => $arr_services_id,
                                                "UF_SERVICE_TYPE" => "transferback",
                                                "UF_DTYPE" => "PR",
                                                "<=UF_LP_FROM" => $time,
                                                ">=UF_LP_TO" => $time
                                            ),
                                            "order" => array(
                                                "UF_SORT" => "ASC",
                                                "ID" => "DESC"),
                                            "limit" => 1)));
                    }

                    if (!empty($arr_discount)) {
                        $discount = $arr_discount;
                    }

                    return new Calculator($prices_data, $tourservices, $markup, $discount, $request);
                }
            }
        }

        return new Calculator(array(), array(), array(), array());
    }

}
