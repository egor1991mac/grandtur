<?php

namespace travelsoft\booking\excursiontour;

use travelsoft\booking\Request;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory {

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request) : Request {
        
        $oRequest = parent::create($request);

        if (is_array($request["excursions_id"]) && !empty($request["excursions_id"])) {

            $oRequest->excursions_id = \array_values(array_map(function ($id) {
                return intVal($id);
            }, $request["excursions_id"]));
        }
        
        if ($request['city_id'] > 0) {
            $oRequest->city_id = $request['city_id'];
        }
        
        return $oRequest;
    }

}
