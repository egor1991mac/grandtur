<?php

namespace travelsoft\booking;

use Bitrix\Main\Config\Option;

/**
 * Класс настроек модуля бронирования
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Settings {

    const DATE_FORMAT = "d.m.Y";
    const FLOAT_NULL = 0.0000000001;
    const MAIL_EVENT_TYPE = "TRAVELSOFT_TRAVELBOOKING";
    const PHONE_REG = "#^((8|\+7|\+3)[\- ]?)?(\(?\d{2}\)?[\- ]?)?[\d\- ]{7,10}$#";
    const DATE_REG = "#^\d{2}\.\d{2}\.\d{4}$#";
    const CACHE_TIME = 600;

    public static function emailForNotification(): string {
        $email = (string) self::get('EMAIL_FOR_NOTIFICATION');

        $arPartsEmail = explode('@', $email);

        if (!strlen($email) || !strlen($arPartsEmail[1])) {

            $email = (string) Option::get('main', 'email_from');
        }

        return $email;
    }
    
    /**
     * Возвращает id пиcьма для уведомления о записи в посольство
     * @return int
     */
    public static function embassyDateMailId(): int {

        return (int) self::get('EMBASSY_DATE_MAIL_ID');
    }
    
    /**
     * Возвращает id пиcьма для уведомления о добавлено сообщении по путевке менеджеру
     * @return int
     */
    public static function managerVoucherMessageNotificationMailId(): int {

        return (int) self::get('MANAGER_VOUCHER_MESSAGE_NOTIFICATION_MAIL_ID');
    }
    
    /**
     * Возвращает id пиcьма для уведомления о добавлено сообщении по путевке клиенту
     * @return int
     */
    public static function clientVoucherMessageNotificationMailId(): int {

        return (int) self::get('CLIENT_VOUCHER_MESSAGE_NOTIFICATION_MAIL_ID');
    }
    
    /**
     * Возвращает id пиcьма для уведомления о выдаче документов
     * @return int
     */
    public static function documentsDateMailId(): int {

        return (int) self::get('DOCUMENTS_DATE_MAIL_ID');
    }
    
    /**
     * Возвращает id пиьма при создании путевки для пользователя
     * @return int
     */
    public static function clientBookingMailId(): int {

        return (int) self::get('CLIENT_BOOKING_MAIL_ID');
    }

    /**
     * Возвращает id пиьма при создании путевки для менеджеров
     * @return int
     */
    public static function managerBookingMailId(): int {

        return (int) self::get('MANAGER_BOOKING_MAIL_ID');
    }
    
    /**
     * Возвращает id пиьма при изменении статуса путевки
     * @return int
     */
    public static function voucherStatusChangedMailId(): int {
        
        return (int)self::get('CLIENT_BOOKING_CHANGE_STATUS_MAIL_ID');
    }
    
    /**
     * Возвращает id пиьма при создании агента
     * @return int
     */
    public static function agentRegisterMailId(): int {

        return (int) self::get('AGENT_REGISTER_MAIL_ID');
    }
    
    /**
     * Возвращает id пиьма при создании агента для менеджера
     * @return int
     */
    public static function agentRegisterMailIdForManager(): int {

        return (int) self::get('MANAGER_AGENT_REGISTER_MAIL_ID');
    }
    
    /**
     * Возвращает id пиьма при активации агента для клиента
     * @return int
     */
    public static function agentActivatedMailId(): int {

        return (int) self::get('AGENT_ACTIVATED_MAIL_ID');
    }

    /**
     * Возвращает id статуса заказа при его создании
     * @return int
     */
    public static function defStatus(): int {

        return (int) self::get("STATUS_ID_FOR_ORDER_CREATION");
    }

    /**
     * Возвращает id статуса заказа для аннуляции
     * @return int
     */
    public static function cancellationStatus(): int {

        return (int) self::get("STATUS_ID_FOR_ORDER_CANCELLATION");
    }
    
    /**
     * Возвращает id статуса запроса на аннуляцию
     * @return int
     */
    public static function cancellationRequestStatus(): int {

        return (int) self::get("STATUS_ID_FOR_ORDER_REQUEST_CANCELLATION");
    }
    
    /**
     * Возвращает id таблицы стран
     * @return int
     */
    public static function countriesStoreId(): int {

        return (int) self::get("COUNTRIES_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы городов
     * @return int
     */
    public static function citiesStoreId(): int {

        return (int) self::get("CITIES_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы туров
     * @return int
     */
    public static function travelStoreId(): int {

        return (int) self::get("TRAVEL_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы размещений
     * @return int
     */
    public static function placementsStoreId(): int {

        return (int) self::get("PLACEMENTS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы тарифов
     * @return int
     */
    public static function ratesStoreId(): int {

        return (int) self::get("RATES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы касс
     * @return int
     */
    public static function cashDesksStoreId(): int {

        return (int) self::get("CASH_DESK_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы типов питания
     * @return int
     */
    public static function foodStoreId(): int {

        return (int) self::get("FOOD_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы документов
     * @return int
     */
    public static function documentsStoreId(): int {

        return (int) self::get("DOCUMENTS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы продолжительности услуги
     * @return int
     */
    public static function durationStoreId(): int {

        return (int) self::get("DURATION_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы руминга
     * @return int
     */
    public static function roomingStoreId(): int {

        return (int) self::get("ROOMING_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы списка групп туристов
     * @return int
     */
    public static function touristsGroupsStoreId(): int {

        return (int) self::get("TOURISTS_GROUPS_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы автобусной базы
     * @return int
     */
    public static function busesStoreId(): int {

        return (int) self::get("BUSES_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы сообщений по путевке
     * @return int
     */
    public static function messagesStoreId(): int {

        return (int) self::get("VOUCHERS_MESSAGES_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы бронировок
     * @return int
     */
    public static function bookingsStoreId(): int {

        return (int) self::get("BOOKINGS_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы типов цен
     * @return int
     */
    public static function priceTypesStoreId(): int {

        return (int) self::get("PRICE_TYPES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы цен
     * @return int
     */
    public static function pricesStoreId(): int {

        return (int) self::get("PRICES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы типов оплаты
     * @return int
     */
    public static function paymentsTypesStoreId(): int {

        return (int) self::get("PAYMENT_TYPES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы типов оплаты
     * @return int
     */
    public static function calculationTypesStoreId(): int {

        return (int) self::get("CALCULATION_TYPES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы истории оплаты
     * @return int
     */
    public static function paymentHistoryStoreId(): int {

        return (int) self::get("PAYMENT_HISTORY_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы квот
     * @return int
     */
    public static function quotasStoreId(): int {

        return (int) self::get("QUOTAS_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы квот для проезда
     * @return int
     */
    public static function transferQuotasStoreId(): int {

        return (int) self::get("TRANSFER_QUOTAS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы квот
     * @return int
     */
    public static function discountsStoreId(): int {

        return (int) self::get("DISCOUNTS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы статусов
     * @return int
     */
    public static function statusesStoreId(): int {

        return (int) self::get("STATUSES_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы номеров
     * @return int
     */
    public static function roomsStoreId(): int {

        return (int) self::get("ROOMS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы туристов
     * @return int
     */
    public static function touristsStoreId(): int {

        return (int) self::get("TOURISTS_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы пакетных туров
     * @return int
     */
    public static function packagetourStoreId(): int {

        return (int) self::get("PACKAGE_TOUR_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы экскурсионных туров
     * @return int
     */
    public static function excursiontourStoreId(): int {

        return (int) self::get("EXCURSION_TOUR_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы туруслуг
     * @return int
     */
    public static function tourservicesStoreId(): int {

        return (int) self::get("TOURSERVICE_STORAGE_ID");
    }
    
    /**
     * Возвращает id таблицы наценки
     * @return int
     */
    public static function markupStoreId(): int {

        return (int) self::get("MARKUP_STORAGE_ID");
    }

    /**
     * Возвращает id таблицы петевок
     * @return int
     */
    public static function vouchersStoreId(): int {

        return (int) self::get("VOUCHERS_STORAGE_ID");
    }

    /**
     * Возвращает id группы пользователей для агентов
     * @return int
     */
    public static function agentsUGroup(): int {

        return (int) self::get("AGENT_GROUP_ID");
    }

    /**
     * Возвращает id группы пользователей для клиентов
     * @return int
     */
    public static function clientsUGroup(): int {

        return (int) self::get("CLIENT_GROUP_ID");
    }

    /**
     * Возвращает id группы пользователей для менеджеров
     * @return int
     */
    public static function managersUGroup(): int {

        return (int) self::get("MANAGER_GROUP_ID");
    }

    /**
     * Возвращает id группы пользователей для кассиров
     * @return int
     */
    public static function cashersUGroup(): int {

        return (int) self::get("CASHER_GROUP_ID");
    }

    /**
     * Путь для сохранения документов
     * @return string
     */
    public static function getDocsSavePath() {
        return $_SERVER['DOCUMENT_ROOT'] . self::getRelDocsSavePath();
    }

    /**
     * Путь для сохранения документов
     * @return string
     */
    public static function getRelDocsSavePath() {
        return '/upload/docs';
    }

    /**
     * Возвращает массив данных по типам услуг
     * @return array
     */
    public static function getServicesTypes() {
        return array(
            "placements" => array("type" => "placements", "name" => "Отель", "cache_dir" => "/travelsoft/bookign/placements"),
            "transfer" => array("type" => "transfer", "name" => "Проезд туда", "cache_dir" => "/travelsoft/bookign/transfer"),
            "transferback" => array("type" => "transferback", "name" => "Проезд обратно", "cache_dir" => "/travelsoft/bookign/transferback"),
            "packagetour" => array("type" => "packagetour", "name" => "Пакетный тур", "cache_dir" => "/travelsoft/bookign/packagetour"),
            "excursiontour" => array("type" => "excursiontour", "name" => "Экскурсионный тур", "cache_dir" => "/travelsoft/bookign/excursiontour"),
            "rooms" => array("type" => "rooms", "name" => "Номера в отеле", "cache_dir" => "/travelsoft/bookign/rooms")
        );
    }

    /**
     * Возвращает массив типов услуг без указанных типов
     * @return array
     */
    public static function getServicesTypesWithout(array $without = null) {
        $types = self::getServicesTypes();
        if (!empty($without)) {
            foreach ($without as $w) {
                unset($types[$w]);
            }
        }

        return $types;
    }

    /**
     * Возвращает валюту учета
     * @return string
     */
    public static function getAccountingCurrency(): string {
        return (string) self::get("ACCOUNTING_CURRENCY");
    }
    
    /**
     * Возвращает код свойства для связи описания тура со страной
     * @return string
     */
    public static function getCountryPropertyCode(): string {
        return (string) self::get("PROPERTY_COUNTRY_CODE");
    }
    
    /**
     * Возвращает валюту заказа
     * @return string
     */
    public static function getVoucherCurrency(): string {
        return (string) self::get("VOUCHER_CURRENCY");
    }
    
    /**
     * Возвращает id типа цены за одноместное размещение
     * @return int
     */
    public static function getSingleOccupancyPriceTypeId () {
        return (int) self::get("SINGLE_OCCUPANCY_PRICE_TYPE_ID");
    }
    
    /**
     * Дефолтные параметры поиска
     * @return array
     */
    public static function commonSearchRequest () {
        $date_from_timestamp = time() + (86400*3);
        return array(
            "date_from" => date(\travelsoft\booking\Settings::DATE_FORMAT, $date_from_timestamp),
            "date_to" => date(\travelsoft\booking\Settings::DATE_FORMAT, $date_from_timestamp + (86400 * 8)),
            "adults" => 2,
            "children" => 0,
            "children_age" => array()
        );
    }
    
    /**
     * @param string $name
     * @return string
     */
    protected static function get(string $name): string {

        return (string) Option::get("travelsoft.travelbooking", $name);
    }

}
