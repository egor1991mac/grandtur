<?php

namespace travelsoft\booking\transfer;

use travelsoft\booking\Request;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory{

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request): Request {

        $oRequest = parent::create($request);

        if (isset($request["tickets_buy"]) && $request["tickets_buy"] === false) {
            $oRequest->tickets_buy = $request["tickets_buy"];
        }
        
        if ($request['city_id'] > 0) {
            $oRequest->city_id = $request['city_id'];
        }
        
        return $oRequest;
    }

}
