<?php

namespace travelsoft\booking;

/**
 * Класс утилит
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Utils {

    /**
     * Возвращает результат конвертации массива в строку
     * @param array $arFields
     * @return string
     */
    public static function ats(array $arFields): string {
        return base64_encode(gzcompress(serialize($arFields), 9));
    }

    /**
     * Возвращает результат конвертации строки в массив
     * @param string $str
     * @return array
     */
    public static function sta(string $str): array {
        return (array) unserialize(gzuncompress(base64_decode($str)));
    }

    /**
     * Конвертирует дату к формату в соответствии с константой travelsoft\booking\Settings::DATE_FORMAT
     * @param string $date
     * @return string
     */
    public static function dateConvert(string $date) {
        return (new \DateTime($date))->format(Settings::DATE_FORMAT);
    }

    /**
     * Возвращает количество ночей
     * @param string $date_from
     * @param string $date_to
     * @return int
     */
    public static function getNightsDuration(string $date_from, string $date_to): int {
        return intVal((strtotime($date_to) - strtotime($date_from)) / 86400) + 1;
    }

    /**
     * Возвращает количество дней
     * @param string $date_from
     * @param string $date_to
     * @return int
     */
    public static function getDaysDuration(string $date_from, string $date_to): int {
        return intVal((strtotime($date_to) - strtotime($date_from)) / 86400);
    }

    /**
     * Получение калькулятора для расчета цен
     * @param string $service_type
     * @param array $request
     * @return \travelsoft\booking\abstraction\Calculator
     * @throws Exception
     */
    public static function getCalculator(string $service_type, array $request): abstraction\Calculator {
        $namespace_part = strtolower($service_type);
        $calculatorFactoryClass = "\\travelsoft\\booking\\" . $namespace_part . "\\CalculatorFactory";
        if (!class_exists($calculatorFactoryClass)) {
            (new Logger())->write("Class " . $calculatorFactoryClass . " not found");
            throw new \Exception("Error by price calculation");
        }
        $requestFactoryClass = "\\travelsoft\\booking\\" . $namespace_part . "\\RequestFactory";
        if (!class_exists($requestFactoryClass)) {

            (new Logger())->write("Class " . $requestFactoryClass . " not found");
            throw new \Exception("Error by price calculation");
        }
        return $calculatorFactoryClass::create($requestFactoryClass::create($request));
    }

    /**
     * Возвращает массив расчитанных цен
     * @param string $service_type
     * @param array $request
     * @return array
     */
    public static function getCalculationResult(string $service_type, array $request): array {

        return self::getCalculator($service_type, $request)->calculating()->addTourservice()->addMarkup()->applyDiscount()->get();
    }

    /**
     * Возвращает массив минимальных расчитанных цен
     * @param string $service_type
     * @param array $request
     * @return array
     */
    public static function getMinCalculationResult(string $service_type, array $request): array {
        return self::getCalculator($service_type, $request)->calculating()->addTourservice()->addMarkup()->applyDiscount()->min()->get();
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForPlacements() {
        $result = array();
        $request = self::makePlacementsSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "placements", "/travelsoft/booking/offers/placements", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("placements", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForPackageTour() {
        $result = array();
        $request = self::makePackagetourSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "packagetour", "/travelsoft/booking/offers/packagetour", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("packagetour", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForTransfer() {
        $result = array();
        $request = self::makeTransferSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "transfer", "/travelsoft/booking/offers/transfer", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("transfer", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForExcursionTour() {
        $result = array();
        $request = self::makeExcursiontourSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "excursiontour", "/travelsoft/booking/offers/excursiontour", 1);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("excursiontour", $request);
            });
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getMinCalculationResultForRooms() {
        $result = array();
        $request = self::makeRoomsSearchRequest();
        $cache = new adapters\Cache(serialize($request) . "rooms", "/travelsoft/booking/offers/rooms", Settings::CACHE_TIME);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($request) {
                return self::getMinCalculationResult("rooms", $request);
            });
        }
        return $result;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeRoomsSearchRequest(array $services_id = null) {
        $request = self::makeCommonSearchRequest($services_id);
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeTransferSearchRequest(array $services_id = null) {
        $request = self::makeCommonSearchRequest($services_id);
        if (@$_GET['travelbooking']["tickets_buy"]) {
            $request["tickets_buy"] = true;
        }
        if (isset($_GET["travelbooking"]["date"])) {
            $request["date_from"] = $_GET["travelbooking"]["date"];
            $request["date_to"] = $_GET["travelbooking"]["date"];
        }
        if (isset($_GET["travelbooking"]["city_id"]) && $_GET["travelbooking"]["city_id"] > 0) {
            $request["city_id"] = intVal($_GET["travelbooking"]["city_id"]);
        }
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeExcursiontourSearchRequest(array $services_id = null) {
        $request = self::makeCommonSearchRequest($services_id);
        if (isset($_GET["travelbooking"])) {
            if (isset($_GET["travelbooking"]["excursions_id"])) {
                $excursions_id = array_filter((array) $_GET["travelbooking"]["excursions_id"], function ($excursions_id) {
                    return $excursions_id > 0;
                });
                if (!empty($excursions_id)) {
                    $request["excursions_id"] = array_values($excursions_id);
                } else {
                    $request["excursions_id"] = [];
                }
            }

            if (isset($_GET["travelbooking"]["city_id"]) && $_GET["travelbooking"]["city_id"] > 0) {
                $request["city_id"] = intVal($_GET["travelbooking"]["city_id"]);
            }

            if (isset($_GET["travelbooking"]["date"])) {
                $request["date_from"] = $_GET["travelbooking"]["date"];
                $request["date_to"] = $_GET["travelbooking"]["date"];
            }
        }
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeTransferbackSearchRequest(array $services_id = null) {
        return self::makeTransferSearchRequest($services_id);
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makePlacementsSearchRequest(array $services_id = null) {
        $request = self::makeCommonSearchRequest($services_id);
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makePackagetourSearchRequest(array $services_id = null) {

        $request = self::makeCommonSearchRequest($services_id);
        if (!empty($_GET['travelbooking'])) {
            $request["rooms_id"] = (array) @$_GET['travelbooking']["rooms_id"];
            $request["placements_id"] = (array) @$_GET['travelbooking']["placements_id"];
            $request["transfers_id"] = (array) @$_GET['travelbooking']["transfers_id"];
            $request["placements_rates_id"] = (array) @$_GET['travelbooking']["placements_rates_id"];
            $request["transfers_rates_id"] = (array) @$_GET['travelbooking']["transfers_rates_id"];
            $request["city_id"] = intVal(@$_GET['travelbooking']["city_id"]);
        } else {
            $request["services_id"] = array();
            $request["rooms_id"] = array();
            $request["placements_id"] = array();
            $request["transfers_id"] = array();
            $request["placements_rates_id"] = array();
            $request["transfers_rates_id"] = array();
            $request["city_id"] = null;
        }
        return $request;
    }

    /**
     * @param array $services_id
     * @return array
     */
    public static function makeCommonSearchRequest(array $services_id = null) {
        $request = \travelsoft\booking\Settings::commonSearchRequest();
        if (isset($_GET['travelbooking'])) {
            if (isset($_GET['travelbooking']["services_id"]) && is_array($_GET['travelbooking']["services_id"])) {
                $request["services_id"] = $_GET['travelbooking']["services_id"];
            }
            if (@$_GET['travelbooking']["date_from"]) {
                $request["date_from"] = $_GET['travelbooking']["date_from"];
            }
            if (@$_GET['travelbooking']["date_to"]) {
                $request["date_to"] = $_GET['travelbooking']["date_to"];
            }
            if (@$_GET['travelbooking']["children"] > 0) {
                $request["children"] = intVal($_GET['travelbooking']["children"]);
            }
            if (@$_GET['travelbooking']["adults"] > 0) {
                $request["adults"] = intVal($_GET['travelbooking']["adults"]);
            }
            if (isset($_GET['travelbooking']['children_age']) && is_array($_GET['travelbooking']['children_age'])) {
                $request['children_age'] = $_GET['travelbooking']['children_age'];
            }
        } else {
            $request["services_id"] = array();
            $request["rates_id"] = array();
        }
        if (empty($request["services_id"]) && $services_id) {
            $services_id = array_filter($services_id, function ($service_id) {
                return $service_id > 0;
            });
            if (!empty($services_id)) {
                $request["services_id"] = array_values($services_id);
            } else {
                $request["services_id"] = array();
            }
        }
        return $request;
    }

    /**
     * @param array $common_parameters
     * @param array $object_id
     * @return array
     */
    public static function makeHighlightDatesParameters(array $common_parameters = [], int $object_id = 0) {
        if (!empty($common_parameters)) {
            if (isset($common_parameters["CALENDAR_FROM"])) {
                $common_parameters["CALENDAR_FROM"]["SERVICES_ID"] = \travelsoft\booking\Utils::getServicesIdByServiceTypeAndSearchObejctId($common_parameters["CALENDAR_FROM"]["SERVICE_TYPE"], $object_id);
            }
            if (isset($common_parameters["CALENDAR_TO"])) {
                $common_parameters["CALENDAR_TO"]["SERVICES_ID"] = \travelsoft\booking\Utils::getServicesIdByServiceTypeAndSearchObejctId($common_parameters["CALENDAR_TO"]["SERVICE_TYPE"], $object_id);
            }
        }
        return $common_parameters;
    }

    /**
     * @param int $placement_id
     * @return array
     */
    public static function getRoomsByPlacement(int $placement_id) {
        $arRooms = [];
        $cache = new adapters\Cache("getRoomsByPlacement_" . $placement_id, Settings::getServicesTypes()["rooms"]["cache_dir"]);
        if (empty($arRooms = $cache->get())) {
            $arRooms = $cache->caching(function () use ($cache, $placement_id) {
                $cache->setTagCache("highloadblock_" . Settings::roomsStoreId());
                return stores\Rooms::get(array("filter" => array("UF_PLACEMENT" => $placement_id)));
            });
        }
        return $arRooms;
    }

    /**
     * @param int $description_id
     * @return array
     */
    public static function getPackageTourByDescription(int $description_id) {
        $arPackageTour = [];
        $cache = new adapters\Cache("getPackageTourByDescriptionn_" . $description_id, Settings::getServicesTypes()["packagetour"]["cache_dir"]);
        if (empty($arPackageTour = $cache->get())) {
            $arPackageTour = $cache->caching(function () use ($cache, $description_id) {
                $cache->setTagCache("highloadblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());
                $arPackageTour = current(stores\PackageTour::get(array("filter" => array("UF_TOUR" => $description_id))));
                return $arPackageTour ?: [];
            });
        }
        return $arPackageTour;
    }

    /**
     * @param int $description_id
     * @return array
     */
    public static function getExcursiontourByDescription(int $description_id) {
        return current(stores\ExcursionTour::get(array("filter" => array("UF_TOUR" => $description_id))));
    }

    /**
     * @param int $service_id
     * @return array
     */
    public static function getExcursiontourDescriptionByServiceId(int $service_id) {
        return stores\Toursdesc::getById(@stores\ExcursionTour::getById($service_id)['UF_TOUR']);
    }

    /**
     * @param int $service_id
     * @return array
     */
    public static function getPackagetourDescriptionByServiceId(int $service_id) {
        return stores\Toursdesc::getById(@stores\PackageTour::getById($service_id)['UF_TOUR']);
    }

    /**
     * Массив в base64 строку
     * @param array $data
     * @return string
     */
    public static function base64encode(array $data): string {
        return base64_encode(serialize($data));
    }

    /**
     * base64 строку в массив
     * @param string $str
     * @return array
     */
    public static function base64decode(string $str): array {
        return unserialize(base64_decode($str));
    }

    /**
     * @param string $service_type
     * @param string $code
     * @return int
     */
    public static function getObjectIdByCode(string $service_type, string $code): int {
        $cache = new adapters\Cache(md5($service_type . $code), "/travelsoft/booking/objects");
        if (empty($arObject = $cache->get())) {
            $arObject = $cache->caching(function () use ($service_type, $code) {
                $arObject = [];
                $className = ucfirst($service_type);
                switch ($service_type) {
                    case "packagetour":
                    case "excursiontour":
                        $className = "Toursdesc";
                        break;
                    case "rooms":
                        $className = "Placements";
                        break;
                    case "transfer":
                    case "transferback":
                        return 0;
                }
                $class = "\\travelsoft\\booking\\stores\\" . $className;
                $arObject = current($class::get(array(
                            "filter" => array("CODE" => $code),
                            "select" => array("ID")
                )));
                return $arObject;
            });
        }
        return (int) @$arObject["ID"];
    }

    /**
     * @param int|array $pid
     * @param int|array $tid
     * @param int|array $tbid
     * @return array
     */
    public static function getPackageTours($pid = null, $tid = null, $tbid = null) {
        $cache = new adapters\Cache(md5("getPackageTours_" . serialize(func_get_args())), Settings::getServicesTypes()["packagetour"]["cache_dir"]);
        if (empty($arPackageTours = $cache->get())) {
            $arPackageTours = $cache->caching(function () use ($pid, $tid, $tbid, $cache) {
                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());
                $arr_filter = array();
                if ($pid > 0 || is_array($pid)) {
                    $arr_filter["UF_PLACEMENT"] = $pid;
                }
                if ($tid > 0 || is_array($tid)) {
                    $arr_filter["UF_TRANSFER"] = $tid;
                }
                if ($tbid > 0 || is_array($tbid)) {
                    $arr_filter["UF_TRANSFER_BACK"] = $tbid;
                }
                return stores\PackageTour::get(array(
                            "filter" => $arr_filter
                ));
            });
        }
        return $arPackageTours;
    }

    /**
     * @param int $packagetourDescription_id
     * @return array
     */
    public static function getTransferByPackageTourDescription(int $packagetourDescription_id) {
        $arPackageTour = self::getPackageTourByDescription($packagetourDescription_id);
        $arTransfer = [];
        $cache = new adapters\Cache("getTransferByPackageTourDescription_" . $packagetourDescription_id, Settings::getServicesTypes()["packagetour"]["cache_dir"]);
        if (empty($arTransfer = $cache->get()) && !empty($arPackageTour)) {
            $arTransfer = $cache->caching(function () use ($arPackageTour, $cache) {
                $cache->setTagCache("iblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::busesStoreId());
                return stores\Buses::getById($arPackageTour["UF_TRANSFER"]);
            });
        }
        return $arTransfer;
    }

    /**
     * @param int $packagetourDescription_id
     * @return array
     */
    public static function getTransferbackByPackageTourDescription(int $packagetourDescription_id) {
        $arPackageTour = self::getPackageTourByDescription($packagetourDescription_id);
        $arTransferback = [];
        $cache = new adapters\Cache("getTransferbackByPackageTourDescription_" . $packagetourDescription_id, Settings::getServicesTypes()["packagetour"]["cache_dir"]);
        if (empty($arTransferback = $cache->get()) && !empty($arPackageTour)) {
            $arTransferback = $cache->caching(function () use ($arPackageTour, $cache) {
                $cache->setTagCache("iblock_" . Settings::travelStoreId());
                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::busesStoreId());
                return stores\Buses::getById($arPackageTour["UF_TRANSFER_BACK"]);
            });
        }
        return $arTransferback;
    }

    /**
     * @param string $service_type
     * @param int $object_id
     * @return array
     */
    public static function getServicesIdByServiceTypeAndSearchObejctId(string $service_type, int $object_id) {
        $arServicesId = [];
        switch ($service_type) {
            case "placements":
            case "rooms":
                $arServicesId = \array_values(\array_map(function (array $item) {
                            return (int) $item["ID"];
                        }, self::getRoomsByPlacement($object_id)));
                break;
            case "packagetour":
                $package = self::getPackageTourByDescription($object_id);
                if (!empty($package)) {
                    $arServicesId[] = $package["ID"];
                }
            case "excursiontour":
                $excursion = self::getExcursiontourByDescription($object_id);
                if (!empty($excursion)) {
                    $arServicesId[] = $excursion["ID"];
                }
                break;
            case "transfer":
                $transfer = self::getTransferByPackageTourDescription($object_id);
                if (!empty($transfer)) {
                    $arServicesId[] = $transfer["ID"];
                }
                break;
            case "transferback":
                $transferback = self::getTransferbackByPackageTourDescription($object_id);
                if (!empty($transferback)) {
                    $arServicesId[] = $transferback["ID"];
                }
                break;
            case "rates":
                $arServicesId = @stores\Rates::getById($object_id, ["ID", "UF_SERVICES"])["UF_SERVICES"];
                break;
        }
        return $arServicesId;
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreasePlacementsNumberOfSold(array $data) {
        self::decreaseRoomsNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increasePlacementsNumberOfSold(array $data) {
        self::increaseRoomsNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseRoomsNumberOfSold(array $data) {
        if (isset($data["UF_ROOM"]) && $data["UF_ROOM"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_ROOM"],
                    "UF_SERVICE_TYPE" => "rooms",
                    "><UF_DATE" => array(
                        $data["UF_DATE_FROM_PLACE"],
                        adapters\Date::createFromTimestamp(intVal($data["UF_DATE_TO_PLACE"]->getTimestamp() - 86400))
                    )
                ),
                "order" => array("UF_DATE" => "ASC"),
                "select" => array("ID", "UF_SOLD_NUMBER"))
            ) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - 1;
                if ($sold <= 0) {
                    $sold = 0;
                }
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseRoomsNumberOfSold(array $data) {
        if (isset($data["UF_ROOM"]) && $data["UF_ROOM"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_ROOM"],
                    "UF_SERVICE_TYPE" => "rooms",
                    "><UF_DATE" => array(
                        $data["UF_DATE_FROM_PLACE"],
                        adapters\Date::createFromTimestamp(intVal($data["UF_DATE_TO_PLACE"]->getTimestamp() - 86400))
                    )
                ),
                "order" => array("UF_DATE" => "ASC"),
                "select" => array("ID", "UF_SOLD_NUMBER"))
            ) as $arr_quota) {
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + 1
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseExcursiontourNumberOfSold(array $data) {

        if (isset($data["UF_SERVICE"]) && $data["UF_SERVICE"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_SERVICE"],
                    "UF_SERVICE_TYPE" => "excursiontour",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);
                if ($sold <= 0) {
                    $sold = 0;
                }

                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseExcursiontourNumberOfSold(array $data) {
        if (isset($data["UF_SERVICE"]) && $data["UF_SERVICE"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_SERVICE"],
                    "UF_SERVICE_TYPE" => "excursiontour",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);
                if ($sold < 0) {
                    $sold = 0;
                }
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER"]) && $data["UF_TRANSFER"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER"],
                    "UF_SERVICE_TYPE" => "transfer",
                    "UF_DATE" => array(
                        $data["UF_DATE_FROM_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);
                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferbackNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferbackNumberOfSold(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\Quotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                stores\Quotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function increaseTransferbackNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $arr_quota["UF_SOLD_NUMBER"] + $data["UF_ADULTS"] + $data["UF_CHILDREN"]
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreaseTransferbackNumberOfSoldTickets(array $data) {
        if (isset($data["UF_TRANSFER_BACK"]) && $data["UF_TRANSFER_BACK"] > 0) {
            foreach (stores\TransferQuotas::get(array(
                "filter" => array(
                    "UF_SERVICE_ID" => $data["UF_TRANSFER_BACK"],
                    "UF_SERVICE_TYPE" => "transferback",
                    "UF_DATE" => array(
                        $data["UF_DATE_BACK_TRANS"]
                    )
                ),
                "select" => array("ID", "UF_SOLD_NUMBER"))) as $arr_quota) {
                $sold = $arr_quota["UF_SOLD_NUMBER"] - ($data["UF_ADULTS"] + $data["UF_CHILDREN"]);
                if ($sold <= 0) {
                    $sold = 0;
                }
                stores\TransferQuotas::update($arr_quota["ID"], array(
                    "UF_SOLD_NUMBER" => $sold
                ));
            }
        }
    }

    /**
     * @param array $data массив полей таблицы бронировок
     */
    public static function decreasePackagetourNumberOfSold(array $data) {
        self::decreasePlacementsNumberOfSold($data);
        self::decreaseTransferNumberOfSold($data);
        self::decreaseTransferbackNumberOfSold($data);
    }

    /**
     * @param array $data массив полей таблицы путевок
     */
    public static function increasePackagetourNumberOfSold(array $data) {
        self::increasePlacementsNumberOfSold($data);
        self::increaseTransferNumberOfSold($data);
        self::increaseTransferbackNumberOfSold($data);
    }

    /**
     * Создание ссылки для запроса на поиск цен
     * @param string $source_link
     * @return string
     */
    public static function createRequestLink(string $source_link) {
        $request_link = $source_link;
        $arr_query_string = array();

        if ($_GET["travelbooking"]) {
            foreach ($_GET["travelbooking"] as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $vv) {
                        $arr_query_string[] = "travelbooking[$k][]=$vv";
                    }
                } else {
                    $arr_query_string[] = "travelbooking[$k]=$v";
                }
            }
        }
        if (!empty($arr_query_string)) {
            $request_link .= "?s2o=Y&" . implode("&", $arr_query_string);
        }
        return $request_link;
    }

    /**
     * Очистка связанных данных при удалении номера
     * @param int $id
     */
    public static function clearForRooms(int $id) {
        self::_clearForService("rooms", $id);
    }

    /**
     * Очистка связанных данных при удалении экскурсионного тура
     * @param int $id
     */
    public static function clearForExcursionTour(int $id) {
        self::_clearForService("excursiontour", $id);
    }

    /**
     * Очистка связанных данных при удалении проезда
     * @param int $id
     */
    public static function clearForTransfer(int $id) {
        self::_clearForService("transfer", $id);
    }

    /**
     * Очистка связанных данных при удалении проезда
     * @param int $id
     */
    public static function clearForTransferBack(int $id) {
        self::_clearForService("tansferback", $id);
    }

    /**
     * Очистка связанных данных при удалении проживания
     * @param int $id
     */
    public static function clearForPlacements(int $id) {
        foreach (stores\Rooms::get(array(
            "filter" => array("UF_PLACEMENT" => $id),
            "select" => array("ID")
        )) as $arr_room) {
            stores\Rooms::delete($arr_room["ID"]);
        }
    }

    /**
     * @param string $serviceType
     * @param int $id
     */
    protected static function _clearForService(string $serviceType, int $id) {
        // очистка квот
        foreach (stores\Quotas::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_quota) {
            stores\Quotas::delete($arr_quota["ID"]);
        }
        // очистка продолжительности
        foreach (stores\Duration::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_duration) {
            stores\Duration::delete($arr_duration["ID"]);
        }
        // очистка цен
        foreach (stores\Prices::get(array(
            "filter" => array("UF_SERVICE_ID" => $id, "UF_SERVICE_TYPE" => $serviceType),
            "select" => array('ID')
        )) as $arr_prices) {
            stores\Prices::delete($arr_prices["ID"]);
        }
        // отвязываем услугу от тарифов
        foreach (stores\Rates::get(array(
            "filter" => array("UF_SERVICES" => array($id), "UF_SERVICES_TYPE" => $serviceType),
            "select" => array("ID", "UF_SERVICES")
        )) as $arr_rate) {
            $key = array_search($id, $arr_rate["UF_SERVICES"]);
            if ($key !== false) {
                unset($arr_rate["UF_SERVICES"][$key]);
            }
            stores\Rates::update($arr_rate["ID"], array("UF_SERVICES" => $arr_rate["UF_SERVICES"]));
        }
    }

    /**
     * Отправка json-строки
     * @global \CMain $APPLICATION
     * @param string $body
     */
    public static function sendJsonResponse(string $body) {
        global $APPLICATION;
        \header('Content-Type: application/json; charset=' . \SITE_CHARSET);
        $APPLICATION->RestartBuffer();
        echo $body;
        die();
    }

    public static function send404() {
        $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
        header($protocol . " 404 Not Found");
        exit;
    }

    /**
     * @param array $parameters
     */
    public static function includeSearchFormComponent(array $parameters = []) {
        $_paraemeters = [
            "component_name" => "travelsoft:booking.search_form",
            "component_template" => $parameters["component_template"],
            "component_parameters" => [
                "SERVICE_TYPE" => is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"],
                "HIGHLIGHT_FOR" => \travelsoft\booking\Utils::makeHighlightDatesParameters(
                        is_callable($parameters["highlight_dates_parameters"]) ? $parameters["highlight_dates_parameters"]() : $parameters["highlight_dates_parameters"], \travelsoft\booking\Utils::getObjectIdByCode(is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"], $parameters["element_code"]))
            ]
        ];
        adapters\ComponentIncluder::includeComponent($_paraemeters);
    }

    /**
     * @return array
     */
    public static function getCities (array $cities_id = null) {
        $cache = new adapters\Cache("cities-of-departure--" . serialize($cities_id), "/travelsoft/booking/cities-of-departure", 3600000);
        if (empty($result = $cache->get())) {
            $result = $cache->caching(function () use ($cities_id) {
                $filter = [];
                if (!empty($cities_id)) {$filter["ID"] = $cities_id;}
                return stores\Cities::get(["filter" => $filter, "select" => ["ID", "NAME"]]);
            });
        }

        return $result;
    }

    /**
     * @param array $parameters
     */
    public static function includeOffersComponent(array $parameters = []) {
        $service_type = is_callable($parameters["service_type"]) ? $parameters["service_type"]() : $parameters["service_type"];
        $_paraemeters = [
            "component_name" => "travelsoft:booking.offers",
            "component_template" => $parameters["component_template"],
            "component_parameters" => [
                "SERVICE_TYPE" => $service_type,
                "BOOKING_URL" => $parameters["booking_url"],
                "SERVICES_ID" => self::getServicesIdByServiceTypeAndSearchObejctId($service_type, self::getObjectIdByCode($service_type, $parameters["element_code"]))
            ]
        ];
        adapters\ComponentIncluder::includeComponent($_paraemeters);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public static function getHTMLSearchOffersFormAndOffersListOnDetailPage(array $parameters = []) {
        $arHTML = [
            "SEARCH_OFFERS_RESULT_HTML" => "",
            "SEARCH_FORM_HTML" => ""
        ];
        ob_start();
        self::includeSearchFormComponent(\array_merge($parameters, ["component_template" => $parameters["search_form_component_template"]]));
        $arHTML["SEARCH_FORM_HTML"] = ob_get_clean();
        ob_start();
        self::includeOffersComponent(\array_merge($parameters, ["component_template" => $parameters["offers_component_template"]]));
        $arHTML["SEARCH_OFFERS_RESULT_HTML"] = ob_get_clean();
        return $arHTML;
    }

    /**
     * Расчет цены "от"
     * @param callable $before должен вернуть массив вида [тип услуг => дополнительные параметры поиска (для $_GET['travelbooking'])]
     * @param callable $after (задать, если необходимо) дополнительная обработка результата. Принимает массив результата расчета и тип объекта.
     *
     * @return array $result
     */
    public static function getCaluculationsPriceFrom(callable $before, callable $after = null) {
        $arData = $before();
        $result = [];
        if (!isset($_GET['travelbooking'])) {
            $_GET['travelbooking'] = [];
        }
        foreach ($arData as $service_type => $data) {

            $method = "getMinCalculationResultFor" . ucfirst($service_type);
            if (method_exists("\\travelsoft\\booking\\Utils", $method) && !empty($data)) {
                $_GET["travelbooking"] = \array_merge($_GET["travelbooking"], $data);
                $result[$service_type] = self::$method();
            }

            unset($_GET["travelbooking"]["services_id"]);
            unset($_GET["travelbooking"]["exursions_id"]);
        }
        if ($after) {
            $after($result);
        }

        return $result;
    }

    /**
     * @param string $type
     * @param int $service_id
     * @return array
     */
    public static function getServiceByTypeAndId($type, $service_id) {

        switch ($type) {

            case "excursiontour":
                $tour = stores\ExcursionTour::getById($service_id);
                $tour['NAME'] = $tour['UF_NAME'];
                return $tour;
            case "packagetour":
                return stores\Toursdesc::getById((int) stores\PackageTour::getById($service_id)['UF_TOUR']);
        }

        return [];
    }

    /**
     * @param mixed $var
     */
    public static function dump($var) {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    /**
     * Поиск туруслуги, сгруппированных по услугам и по городам посадки
     * @param array $arr_services_id
     * @param string $service_type
     * @param int $adults
     * @param int $children
     * @param int $city_id
     * @return array
     */
    public static function findTourservices(array $arr_services_id, string $service_type, int $adults, int $children = null, int $city_id = null) {
        $tourservices = array();
        $filter = ["UF_SERVICE_TYPE" => $service_type];
        if ($city_id) {
            $filter["UF_CITY"] = $city_id;
        }

        $arr_tourservices = \travelsoft\booking\stores\Tourservices::get(array(
                    "filter" => \array_merge(["UF_SERVICES" => $arr_services_id], $filter)
        ));

        $arr_total_tourservice = current(\travelsoft\booking\stores\Tourservices::get(array(
                    "filter" => \array_merge(["UF_SERVICES" => false], $filter),
                    "order" => ['ID' => 'ASC'],
                    "limit" => 1
        )));

        if (!empty($arr_tourservices) || !empty($arr_total_tourservice)) {

            foreach ($arr_services_id as $service_id) {
                $key = false;
                foreach ($arr_tourservices as $arr_tourservice) {
                    $key = array_search($service_id, $arr_tourservice["UF_SERVICES"]);
                    if ($key !== false) {
                        $tourservices[$service_id][intVal($arr_tourservice["UF_CITY"])] = array(
                            //"total" => ((float) $arr_tourservice["UF_ADULTS_PRICE"] * $adults) + ((float) $arr_tourservice["UF_CHILDREN_PRICE"] * $children),
                            "for_adults" => (float) $arr_tourservice["UF_ADULTS_PRICE"],
                            "for_children" => \array_values(\array_map(function ($price) {
                                                return (float) $price;
                                            }, $arr_tourservice["UF_CHILDREN_PRICE"])),
                            "currency" => (string) $arr_tourservice["UF_CURRENCY"],
                            "adults" => $adults,
                            "children" => $children,
                            "discount" => 0.00,
                            "min_age" => $arr_tourservice["UF_MIN_AGE"],
                            "max_age" => $arr_tourservice["UF_MAX_AGE"],
                            "city_id" => intVal($arr_tourservice["UF_CITY"])
                        );
                    }
                }
                if (!isset($tourservices[$service_id]) && !empty($arr_total_tourservice)) {
                    $tourservices[$service_id][intVal($arr_tourservice["UF_CITY"])] = array(
                        //"total" => ((float) $arr_tourservice["UF_ADULTS_PRICE"] * $adults) + ((float) $arr_tourservice["UF_CHILDREN_PRICE"] * $children),
                        "for_adults" => (float) $arr_total_tourservice["UF_ADULTS_PRICE"],
                        "for_children" => \array_values(\array_map(function ($price) {
                                                return (float) $price;
                                            }, $arr_tourservice["UF_CHILDREN_PRICE"])),
                        "currency" => (string) $arr_total_tourservice["UF_CURRENCY"],
                        "adults" => $adults,
                        "children" => $children,
                        "discount" => 0.00,
                        "min_age" => $arr_tourservice["UF_MIN_AGE"],
                        "max_age" => $arr_tourservice["UF_MAX_AGE"],
                        "city_id" => intVal($arr_tourservice["UF_CITY"])
                    );
                }
            }
        }
        return $tourservices;
    }

}
