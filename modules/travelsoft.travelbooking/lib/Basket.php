<?php

namespace travelsoft\booking;

/**
 * Класс корзины
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Basket {

    /**
     * @var array
     */
    public $_basket = null;

    public function __construct() {

        if (!isset($_SESSION["__travelsoft_basket"]) || !is_array($_SESSION["__travelsoft_basket"])) {
            $_SESSION["__travelsoft_basket"] = array();
        }

        $this->_basket = &$_SESSION["__travelsoft_basket"];
    }

    /**
     * @param array $fields
     * @throws \Exception
     */
    public function basketFieldsValidating(array $fields) {

        if (!isset($fields["service_id"]) || $fields["service_id"] <= 0) {
            throw new \Exception("Add to basket is failed. Service id is not found.");
        }

        if (isset($fields["room_id"]) && $fields["room_id"] <= 0) {
            throw new \Exception("Add to basket is failed. Room id must be more then zero.");
        }
        
        if (isset($fields["transfer_id"]) && $fields["transfer_id"] <= 0) {
            throw new \Exception("Add to basket is failed. Transfer id must be more then zero.");
        }
        
        if (isset($fields["placement_id"]) && $fields["placement_id"] <= 0) {
            throw new \Exception("Add to basket is failed. Placement id must be more then zero.");
        }

        if (
                (!isset($fields["rate_id"]) && $fields["rate_id"] <= 0) && 
                (!isset($fields["placement_rate_id"]) || $fields["placement_rate_id"] <= 0) &&
                (!isset($fields["transfer_rate_id"]) || $fields["transfer_rate_id"] <= 0)
        ) {
            throw new \Exception("Add to basket is failed. Rate id is not found.");
        }

        if (!strlen($fields["service_type"])) {
            throw new \Exception("Add to basket is failed. Service type is not found.");
        }
        
        if (isset($fields["date"]) && !strlen($fields["date"])) {
            throw new \Exception("Add to basket is failed. Date is not found.");
        }

        if (isset($fields["date_from"]) && !strlen($fields["date_from"])) {
            throw new \Exception("Add to basket is failed. Date from is not found.");
        }

        if (isset($fields["date_to"]) && !strlen($fields["date_to"])) {
            throw new \Exception("Add to basket is failed. Date to is not found.");
        }

        if ($fields["adults"] <= 0) {
            throw new \Exception("Add to basket is failed. Adults is not found or less or equal then zero.");
        }

        if (isset($fields["children"]) && $fields["children"] < 0) {
            throw new \Exception("Add to basket is failed. Children must be more or equal then zero.");
        } elseif ($fields["children"] != 0 && (!is_array($fields["children_age"]) || empty($fields["children_age"]))) {
            throw new \Exception("Add to basket is failed. Age of children is not found.");
        }
    }

    /**
     * @param array $fields
     * @return string
     */
    public function add(array $fields) {

        $this->basketFieldsValidating($fields);

        $id = time();

        $this->_basket[$id] = $fields;

        return (string) $id;
    }

    /**
     * @param mixed $id
     * @return boolean
     */
    public function delete($id) {

        if (isset($this->_basket[$id])) {
            unset($this->_basket[$id]);
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function get() {
        return $this->_basket;
    }
    
    public function clear () {
        $this->_basket = array();
    }
    
}
