<?php

namespace travelsoft\booking\packagetour;

use travelsoft\booking\Request;

/**
 * Класс-фабрика для класса запроса
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class RequestFactory extends \travelsoft\booking\RequestFactory{

    /**
     * @param array $request
     * @return \travelsoft\booking\Request
     */
    public static function create(array $request): Request {

        $oRequest = parent::create($request);

        if (is_array($request["rooms_id"]) && !empty($request["rooms_id"])) {

            $oRequest->rooms_id = array_map(function ($id) {
                return intVal($id);
            }, $request["rooms_id"]);
        }

        if (is_array($request["placements_id"]) && !empty($request["placements_id"])) {

            $oRequest->placements_id = array_map(function ($id) {
                return intVal($id);
            }, $request["placements_id"]);
        }

        if (is_array($request["transfers_id"]) && !empty($request["transfers_id"])) {

            $oRequest->transfers_id = array_map(function ($id) {
                return intVal($id);
            }, $request["transfers_id"]);
        }
        
        if (is_array($request["transfersback_id"]) && !empty($request["transfersback_id"])) {

            $oRequest->transfersback_id = array_map(function ($id) {
                return intVal($id);
            }, $request["transfersback_id"]);
        }

        if (is_array($request["placements_rates_id"]) && !empty($request["placements_rates_id"])) {

            $oRequest->placements_rates_id = array_map(function ($id) {
                return intVal($id);
            }, $request["placements_rates_id"]);
        }

        if (is_array($request["transfers_rates_id"]) && !empty($request["transfers_rates_id"])) {

            $oRequest->transfers_rates_id = array_map(function ($id) {
                return intVal($id);
            }, $request["transfers_rates_id"]);
        }
        
        if (is_array($request["transfersback_rates_id"]) && !empty($request["transfersback_rates_id"])) {

            $oRequest->transfersback_rates_id = array_map(function ($id) {
                return intVal($id);
            }, $request["transfersback_rates_id"]);
        }
        
        if ($request['city_id'] > 0) {
            $oRequest->city_id = $request['city_id'];
        }

        return $oRequest;
    }

}
