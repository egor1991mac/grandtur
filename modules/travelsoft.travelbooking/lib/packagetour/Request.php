<?php

namespace travelsoft\booking\packagetour;

/**
 * Класс запроса на поиск пакетных туров
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\Request{
        
    /**
     * @var array
     */
    public $rooms_id = null;
    
    /**
     * @var array
     */
    public $placements_id = null;
    
    /**
     * @var array
     */
    public $transfers_id = null;
    
    /**
     * @var array
     */
    public $transfersback_id = null;

    /**
     * @var array
     */
    public $placements_rates_id = null;
    
    /**
     * @var array
     */
    public $transfers_rates_id = null;
    
    /**
     * @var array
     */
    public $transfersback_rates_id = null;
    
    /**
     * @var string
     */
    public $date_from = null;
    
    /**
     * @var string
     */
    public $date_to = null;
    
    /**
     * @var int
     */
    public $adults = null;
    
    /**
     * @var int
     */
    public $children = null;
    
    /**
     * @var array
     */
    public $children_age = null;
    
    /**
     * @var int
     */
    public $city_id = null;
}
