<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с кассами
 *
 * @author dimabresky
 */
class PaymentTypesUtils extends Utils {

    /**
     * Возвращает результат обработки формы добавления/редактирования типа оплаты
     * @return type
     */
    public static function processingPaymentTypeEditForm() {

        $url = Settings::PAYMENTS_TYPES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {
            
            $data = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::paymentsTypesStoreId(), array());

            Validator::stringLessThenTwo($data['UF_NAME'], 'Название', $arErrors);

            if (empty($arErrors)) {

                if ($_REQUEST['ID'] > 0) {

                    $ID = intval($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\PaymentsTypes::update($ID, $data);
                } else {

                    $result = \travelsoft\booking\stores\PaymentsTypes::add($data);
                }

                if ($result) {

                    LocalRedirect($url);
                }
            }
        }


        return array('errors' => $arErrors, 'result' => $result);
    }
    
    /**
     * Возвращает HTML полей для формы редактирования типа оплаты
     * @param array $data
     * @return string
     */
    public static function getPaymentsTypesEditFieldsContent(array $data): string {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::paymentsTypesStoreId(), $data);

        $isFormRequest = self::isEditFormRequest();

        $content = '';

        foreach ($arUserFields as $arUserField) {

            if ($arUserField['FIELD_NAME'] == 'UF_PAYMENT_HANDLER') {
                continue;
            }
            $arUserField['ENTITY_VALUE_ID'] = 2;
            $content .= \travelsoft\booking\adapters\UserFieldsManager::getEditFormHtml($isFormRequest, $arUserField);
        }

        return $content;
    }

}
