<?php

namespace travelsoft\booking\crm;

use travelsoft\booking\Settings;

/*
 * Функционал для страниц crm
 */

class Utils {

    /**
     * Определяет права доступа к CRM
     * @global type $USER
     * @return bool
     */
    public static function access(): bool {

        global $USER;

        $access = false;
        if ($USER->IsAdmin()) {

            $access = true;
        } else {

            $allowGroups = array(
                Settings::managersUGroup(),
                Settings::cashersUGroup()
            );
            $arUserGroups = $USER->GetUserGroupArray();

            foreach ($arUserGroups as $groupId) {

                if (in_array($groupId, $allowGroups)) {
                    $access = true;
                    break;
                }
            }
        }

        return $access;
    }

    /**
     * Отправка json-строки
     * @global \CMain $APPLICATION
     * @param string $body
     */
    public static function sendJsonResponse(string $body) {

        global $APPLICATION;

        header('Content-Type: application/json');

        $APPLICATION->RestartBuffer();

        echo $body;

        die();
    }

    /**
     * Выводит формы фильтрации на странце списка
     * @param array $parameters
     */
    public static function showFilterForm(array $parameters) {

        global $APPLICATION;

        $form = '<form name="find_form" method="get" action="' . $APPLICATION->GetCurPage() . '">';

        $form .= '<input name="lang" value="' . LANGUAGE_ID . '" type="hidden">';

        $form .= '<div class="adm-filter-wrap" >';

        $form .= '<table class="adm-filter-main-table">';

        $form .= '<tbody>';

        $form .= '<tr><td class="adm-filter-main-table-cell">';

        $form .= '<div class="adm-filter-content">';

        $form .= '<div class="adm-filter-content-table-wrap">';

        $form .= '<table cellspacing="0" class="adm-filter-content-table">';

        $form .= '<tbody>';

        $form .= '<tr>';

        $form .= '<td id="filter-title" style="font-size: 23px; padding-bottom: 10px;" colspan="2"><b>Фильтр<b></td>';

        $form .= '</tr>';

        $form .= '<tr>';

        foreach ($parameters['form_elements'] as $key => $arFormElement) {

            $form .= '<td class="form-element-cell">';
            $form .= '<label><b>' . $arFormElement['label'] . '</b></label><br>';
            $form .= '<div class="adm-filter-box-sizing">';
            $form .= $arFormElement['view'];
            $form .= '</div>';
            $form .= '</td>';

            if (($key + 1) % 2 === 0 || ($key + 1) === count($parameters['form_elements'])) {
                $form .= '</tr>';
                if (($key + 1) !== count($parameters['form_elements'])) {
                    $form .= '<tr>';
                }
            }
        }

        $form .= '<tbody>'
                . '</table>'
                . '</div>'
                . '<div class="adm-filter-bottom-separate"></div>'
                . '<div class="adm-filter-bottom">'
                . '<input type="submit" calss="adm-btn" name="SHOW_BY_FILTER" value="Найти">'
                . '<input type="submit" calss="adm-btn" name="CANCEL" value="Отменить">'
                . '</div>'
                . '</div>'
                . '</td>'
                . '</tr>'
                . '</tbody>'
                . '</table>'
                . '</div>'
                . '</form>';

        $style = '<style>
            .adm-filter-select {
                width: 100% !important;
            }

            .adm-filter-item-center .select2-container {
                margin-left: 12px;
            }

            .form-element-cell {
                width: 50%;
                padding-bottom: 10px;
            }

            .adm-input-wrap {
                margin: 0px !important;
            }

            .adm-filter-main-table label {
                margin-left: 0px !important;
            }

            #filter-title {
                font-size: 23px;
                padding-bottom: 10px;
            }
            </style>';

        echo $style . $form;
    }

    

    /**
     * Вывод формы редактирования
     * @param array $parameters
     */
    public static function showEditForm(array $parameters) {

        echo '<form enctype="multipart/form-data" action="' . $parameters['action'] . '" method="POST" name="' . $parameters['name'] . '" id="' . $parameters['id'] . '">';

        if ($_REQUEST['ID'] > 0) {

            echo '<input name="ID" value="' . intVal($_REQUEST['ID']) . '" type="hidden">';
        }

        echo '<input type="hidden" name="lang" value="' . LANGUAGE_ID . '">';

        echo bitrix_sessid_post();

        $tabControl = new \CAdminTabControl("tabControl", $parameters['tabs']);

        $tabControl->Begin();

        foreach ($parameters['tabs'] as $tab) {

            $tabControl->BeginNextTab();

            if ($tab["__CONTENT"]) {
                echo $tab["__CONTENT"];
            } else {
                foreach ($tab["FIELDS"] as $field) {

                    echo self::getEditFieldHtml($field["label"], $field['view'], (bool) $field['required'], (bool) @$field['hide']);
                }
            }
        }

        $tabControl->Buttons();

        foreach ($parameters['buttons'] as $button) {

            $class = $button['class'] ? 'class="' . $button['class'] . '"' : '';

            $id = $button['id'] ? 'id="' . $button['class'] . '"' : '';

            $onclick = $button['onclick'] ? 'onclick="' . $button['onclick'] . '"' : '';

            echo '<input ' . $onclick . ' type="submit" name="' . $button['name'] . '" ' . $id . ' value="' . $button['value'] . '" ' . $class . '>';
        }

        $tabControl->End();

        echo '</form>';
    }

    /**
     * Является ли запрос запросом от формы редактирования
     * @return bool
     */
    public static function isEditFormRequest(): bool {

        return $_SERVER['REQUEST_METHOD'] === 'POST' && check_bitrix_sessid() && ($_POST['SAVE'] || $_POST['APPLY']);
    }
    
    /**
     * HTML поля редактирования
     * @param string $label
     * @param string $field
     * @param bool $required
     * @param bool $hide
     * @return string
     */
    public static function getEditFieldHtml(string $label, string $field, bool $required = false, bool $hide = false): string {

        if ($required) {
            $label .= '<span class="required">*</span>';
        }

        $content .= '<tr ' . ($hide ? 'style="display:none"' : "") . '>';
        $content .= '<td width="40%">' . $label . '</td>';
        $content .= '<td width="60%">' . $field . '</td>';
        $content .= '</tr>';

        return $content;
    }

    /**
     * Возвращает массив array("REFERENCE" => array(), "REFERENCE_ID" => array())для SelectBoxFromArray, SelectBoxMFromArray
     * @param array $arElements
     * @param string $referenceField
     * @param string $referenceIdField
     * @return array
     */
    public static function getReferencesSelectData(array $arElements, string $referenceField, string $referenceIdField): array {

        $arData = array(
            "REFERENCE" => array(),
            "REFERENCE_ID" => array()
        );

        $arData['REFERENCE'][] = '...';
        $arData['REFERENCE_ID'][] = '';

        foreach ($arElements as $arElement) {

            $arData['REFERENCE'][] = $arElement[$referenceField];
            $arData['REFERENCE_ID'][] = $arElement[$referenceIdField];
        }

        return $arData;
    }
    
    /**
     * @return array
     */
    public static function getExcursionTour() {
        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_excursion_tour", "/travelsoft/admin/excursion_tour");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("highloadblock_" . Settings::excursiontourStoreId());

                $arRes = array();
                foreach (\travelsoft\booking\stores\ExcursionTour::get(array("select" => array("ID", "UF_NAME"))) as $arr_excur) {
                    $arRes[$arr_excur["ID"]] = array(
                        "ID" => $arr_excur["ID"],
                        "NAME" => $arr_excur["UF_NAME"]
                    );
                }
                
                return $arRes;
            });
        }
        return $arRes;
    }

    /**
     * @return array
     */
    public static function getExcursionTourForSelect() {

        return self::getReferencesSelectData(self::getExcursionTour(), "NAME", "ID");
    }

    /**
     * Отображение select для размещений
     * @param string $name
     * @param mixed $select
     * @param bool $multiple
     */
    public static function getExcursionTourSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getExcursionTourForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getExcursionTourForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
        
    /**
     * @return array
     */
    public static function getPlacements() {
        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_placements", "/travelsoft/admin/placements");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("iblock_id_" . Settings::placementsStoreId());

                return \travelsoft\booking\stores\Placements::get(array("filter" => array("ACTIVE" => "Y"), "select" => array("ID", "NAME")));
            });
        }
        return $arRes;
    }

    /**
     * @return array
     */
    public static function getPlacementsForSelect() {

        return self::getReferencesSelectData(self::getPlacements(), "NAME", "ID");
    }

    /**
     * Отображение select для размещений
     * @param string $name
     * @param mixed $select
     * @param bool $multiple
     */
    public static function getPlacementsSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getPlacementsForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getPlacementsForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * Список тарифов
     * @param string $service_type
     * @param array $services
     * @return array
     */
    public static function getRates (string $service_type = null, array $services = null) {
        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_rates" . serialize(func_get_args()), "/travelsoft/admin/rates");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache, $service_type, $services) {

                $cache->setTagCache("highloadblock_" . Settings::ratesStoreId());

                $arr_filter = array();
                if (!empty($services)) {
                    $arr_filter["UF_SERVICES"] = $services;
                }
                if (!empty($service_type)) {
                    $arr_filter["UF_SERVICES_TYPE"] = $service_type;
                }
                
                $arRes = array();
                foreach (\travelsoft\booking\stores\Rates::get(array("filter" => $arr_filter, "select" => array("ID", "UF_NAME"))) as $arr_rate) {
                    $arRes[] = array(
                        "ID" => $arr_rate["ID"],
                        "NAME" => $arr_rate["UF_NAME"]
                    );
                }
                return $arRes;
            });
        }

        return $arRes;
    }
    
    /**
     * @param string $service_type
     * @param array $services
     * @return array
     */
    public static function getRatesForSelect(string $service_type = null, array $services = null) {

        return self::getReferencesSelectData(self::getRates($service_type, $services), "NAME", "ID");
    }
    
    /**
     * Отображение select для тарифы
     * @param string $name
     * @param int $select
     * @param bool $multiple
     * @param string $service_type
     * @param array $services
     */
    public static function getRatesSelectView($name, $select, $multiple = false, string $service_type = null, array $services = null) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getRatesForSelect($service_type, $services), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getRatesForSelect($service_type, $services), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * @return array
     */
    public static function getTravel() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_travel", "/travelsoft/admin/travel");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("iblock_id_" . Settings::travelStoreId());

                return \travelsoft\booking\stores\Toursdesc::get(array("filter" => array("ACTIVE" => "Y"), "select" => array("ID", "NAME")));
            });
        }

        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getExcursionTourIB() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_excursiontourIB", "/travelsoft/admin/excursiontourIB");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("iblock_id_" . Settings::travelStoreId());

                return \travelsoft\booking\stores\Toursdesc::get(array("filter" => array("ACTIVE" => "Y"), "select" => array("ID", "NAME")));
            });
        }

        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getTransfer() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_travel", "/travelsoft/admin/transfers");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("highloadblock_" . Settings::busesStoreId());
                
                $arRes = array();
                foreach (\travelsoft\booking\stores\Buses::get(array("select" => array("ID", "UF_NAME"))) as $arr_transfer) {
                    $arRes[] = array(
                        "ID" => $arr_transfer["ID"],
                        "NAME" => $arr_transfer["UF_NAME"]
                    );
                }
                
                return $arRes;
            });
        }

        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getTransferback() {
        return self::getTransfer();
    }

    /**
     * @return array
     */
    public static function getTravelForSelect() {

        return self::getReferencesSelectData(self::getTravel(), "NAME", "ID");
    }
    
    /**
     * @return array
     */
    public static function getExcursionTourIBForSelect() {

        return self::getReferencesSelectData(self::getExcursionTourIB(), "NAME", "ID");
    }
    
    /**
     * @return array
     */
    public static function getTransferForSelect() {

        return self::getReferencesSelectData(self::getTransfer(), "NAME", "ID");
    }

    /**
     * Отображение select для туров
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getTravelSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getTravelForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getTravelForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * Отображение select для экскурсионных туров
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getExcursionTourIBSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getExcursionTourIBForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getExcursionTourIBForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * Отображение select для туров
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getTransferSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getTransferForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getTransferForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * Отображение select для туров
     * @param type $name
     * @param type $select
     * @param bool $multiple
     * return string
     */
    public static function getTransferbackSelectView($name, $select, $multiple = false) {

        return self::getTransferSelectView($name, $select, $multiple);
    }

    /**
     * @return array
     */
    public static function getPriceTypes() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_price_types", "/travelsoft/admin/price_types");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("highloadblock_" . Settings::priceTypesStoreId());

                return \travelsoft\booking\stores\PriceTypes::get(array("select" => array("ID", "UF_NAME")));
            });
        }
        return $arRes;
    }

    /**
     * @return array
     */
    public static function getCalculationTypesForSelect() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_calculation_types", "/travelsoft/admin/calculation_types");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("iblock_id_" . Settings::placementsStoreId());

                return \travelsoft\booking\stores\CalculationTypes::get(array("select" => array("ID", "UF_NAME")));
            });
        }

        return self::getReferencesSelectData($arRes, "UF_NAME", "ID");
    }

    /**
     * Отображение select для типов расчета
     * @param type $name
     * @param type $select
     */
    public static function getCalculationTypesSelectView($name, $select) {

        return \SelectBoxFromArray($name, self::getCalculationTypesForSelect(), $select, "", 'id="' . $name . '" class="select2"');
    }

    /**
     * @param $placement_id  если нужны комнаты по конкретному отелю
     * @return array
     */
    public static function getRooms(int $placement_id = null) {
        
        $cache_id = "travelsoft_admin_view_rooms" . ($placement_id ?: "");
        
        $cache = new \travelsoft\booking\adapters\Cache($cache_id, "/travelsoft/admin/rooms");

        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache, $placement_id) {

                $cache->setTagCache("iblock_id_" . Settings::placementsStoreId());
                $cache->setTagCache("highloadblock_" . Settings::roomsStoreId());
                
                $arr_filter = array();
                if ($placement_id > 0) {
                    $arr_filter = array("UF_PLACEMENT" => $placement_id);
                }
                
                $arRooms = \travelsoft\booking\stores\Rooms::get(array("filter" => $arr_filter));

                $arr_grouped_rooms_by_placements = array();

                foreach ($arRooms as $arRoom) {

                    if ($arRoom["UF_PLACEMENT"] > 0) {
                        $arr_grouped_rooms_by_placements[$arRoom["UF_PLACEMENT"]][] = $arRoom;
                    }
                }

                if (!empty($arr_grouped_rooms_by_placements)) {
                    $arPlacements = \travelsoft\booking\stores\Placements::get(array("filter" => array("ID" => array_keys($arr_grouped_rooms_by_placements)), "select" => array("ID", "NAME")));
                    foreach ($arPlacements as $arPlacement) {

                        foreach ($arr_grouped_rooms_by_placements[$arPlacement["ID"]] as $room) {
                            $arRes[] = array(
                                "ID" => $room["ID"],
                                "NAME" => $room["UF_NAME"] . " [" . $arPlacement["NAME"] . "]"
                            );
                        }
                    }
                }

                return $arRes;
            });
        }

        return $arRes;
    }

    /**
     * @param $placement_id если нужены номера по конкретному отелю
     * @return array
     */
    public static function getRoomsForSelect(int $placement_id = null) {

        return self::getReferencesSelectData(self::getRooms((int)$placement_id), "NAME", "ID");
    }

    /**
     * Отображение select для номеров
     * @param string $name
     * @param mixed $select
     * @param bool $multiple
     */
    public static function getRoomsSelectView($name, $select, $multiple = false, int $placement_id = null) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getRoomsForSelect((int)$placement_id), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getRoomsForSelect((int)$placement_id), $select, "", 'id="' . $name . '" class="select2"');
        }
    }

    /**
     * @return array
     */
    public static function getPackageTour() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_package_tours", "/travelsoft/admin/package_tours");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());

                $arPackageTours = \travelsoft\booking\stores\PackageTour::get();
                $arPlacements = $arTransfers = array();
                foreach ($arPackageTours as $arPackageTour) {
                    if (!isset($arPlacements[$arPackageTour["UF_PLACEMENT"]])) {
                        $arPlacements[$arPackageTour["UF_PLACEMENT"]] = current(\travelsoft\booking\stores\Placements::get(array(
                                    "filter" => array(
                                        "ID" => $arPackageTour["UF_PLACEMENT"]
                                    ),
                                    "select" => array("ID", "NAME")
                        )));
                    }
                    if (!isset($arTransfers[$arPackageTour["UF_TRANSFER"]])) {
                        $arTransfers[$arPackageTour["UF_TRANSFER"]] = current(\travelsoft\booking\stores\Buses::get(array(
                                    "filter" => array(
                                        "ID" => $arPackageTour["UF_TRANSFER"]
                                    ),
                                    "select" => array("ID", "UF_NAME")
                        )));
                    }
                    $arRes[$arPackageTour["ID"]] = array(
                        "ID" => $arPackageTour["ID"],
                        "NAME" => implode(" + ", array($arTransfers[$arPackageTour["UF_TRANSFER"]]["UF_NAME"],
                            $arPlacements[$arPackageTour["UF_PLACEMENT"]]["NAME"]))
                    );
                }

                return $arRes;
            });
        }

        return $arRes;
    }

    /**
     * @return array
     */
    public static function getPackageTourForSelect() {

        return self::getReferencesSelectData(self::getPackageTour(), "NAME", "ID");
    }

    /**
     * Отображение select для пакетных туров
     * @param string $name
     * @param mixed $select
     * @param bool $multiple
     */
    public static function getPackageTourSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getPackageTourForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getPackageTourForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }

    /**
     * @return array
     */
    public static function getFood() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_food", "/travelsoft/admin/food");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("iblock_id_" . Settings::foodStoreId());

                return \travelsoft\booking\stores\Food::get(array("select" => array("ID", "NAME")));
            });
        }

        return $arRes;
    }

    /**
     * @return array
     */
    public static function getFoodForSelect() {

        return self::getReferencesSelectData(self::getFood(), "NAME", "ID");
    }

    /**
     * Отображение select для питания
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getFoodSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getFoodForSelect(), $select, "", "", "", 'id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getFoodForSelect(), $select, "", 'id="' . $name . '" class="select2"');
        }
    }

    /**
     * @return array
     */
    public static function getManagers() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_managers", "/travelsoft/admin/managers");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("USER_NAME");

                return \travelsoft\booking\stores\Users::get(array(
                            "filter" => array("GROUPS_ID" => array(Settings::managersUGroup())),
                            "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "EMAIL")));
            });
        }

        return $arRes;
    }

    /**
     * @return array
     */
    public static function getManagersForSelect() {

        return self::getReferencesSelectData(self::getManagers(), "FULL_NAME_WITH_EMAIL", "ID");
    }

    /**
     * Отображение select для менеджеров
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getManagersSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getManagersForSelect(), $select, "", "", "", 'style="width: 300px" id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getManagersForSelect(), $select, "", 'style="width: 300px" id="' . $name . '" class="select2"');
        }
    }

    /**
     * @return array
     */
    public static function getClients() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_clients", "/travelsoft/admin/clients");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("USER_NAME");
                
                $arr_users = array();
                foreach (\travelsoft\booking\stores\Users::get(array(
                    "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "EMAIL", "UF_LEGAL_NAME")))
                as $arr_user) {

                    if (strlen($arr_user["UF_LEGAL_NAME"]) > 0) {
                        $arr_user["FULL_NAME_WITH_EMAIL"] .= "[".$arr_user["UF_LEGAL_NAME"]."]";
                    }
                    
                    $arr_users[$arr_user["ID"]] = $arr_user;
                }

                return $arr_users;
            });
            
        }
        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getClientsForSelect() {

        return self::getReferencesSelectData(self::getClients(), "FULL_NAME_WITH_EMAIL", "ID");
    }

    /**
     * Отображение select для клиентов
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getClientsSelectView($name, $select, $multiple = false) {
        
        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getClientsForSelect(), $select, "", "", "", 'style="width: 300px" id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getClientsForSelect(), $select, "", 'style="width: 300px" id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * @return array
     */
    public static function getCities () {
        
        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_clients", "/travelsoft/admin/cities");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () {

                return \travelsoft\booking\stores\Cities::get([
                    'select' => ['ID', 'NAME']
                ]);
            });
        }
        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getCitiesForSelect() {

        return self::getReferencesSelectData(self::getCities(), "NAME", "ID");
    }
    
    /**
     * Отображение select для городов
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getCitiesSelectView($name, $select, $multiple = false) {

        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getCitiesForSelect(), $select, "", "", "", 'style="width: 300px" id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getCitiesForSelect(), $select, "", 'style="width: 300px" id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * @return array
     */
    public static function getStatuses() {

        $cache = new \travelsoft\booking\adapters\Cache("travelsoft_admin_view_statuses", "/travelsoft/admin/statuses");
        if (empty($arRes = $cache->get())) {
            $arRes = $cache->caching(function () use ($cache) {

                $cache->setTagCache("highloadblock_" . Settings::statusesStoreId());

                return \travelsoft\booking\stores\Statuses::get();
            });
        }

        return $arRes;
    }
    
    /**
     * @return array
     */
    public static function getStatusesForSelect() {

        return self::getReferencesSelectData(self::getStatuses(), "UF_NAME", "ID");
    }
    
    /**
     * Отображение select для статусов
     * @param type $name
     * @param type $select
     * @param bool $multiple
     */
    public static function getStatusesSelectView($name, $select, $multiple = false) {
        
        if ($multiple) {
            return \SelectBoxMFromArray($name, self::getStatusesForSelect(), $select, "", "", "", 'style="width: 300px" id="' . $name . '" class="select2"');
        } else {
            return \SelectBoxFromArray($name, self::getStatusesForSelect(), $select, "", 'style="width: 300px" id="' . $name . '" class="select2"');
        }
    }
    
    /**
     * @param array $types
     */
    public static function getBookingServices(array $types) {

        $result = array();
        foreach ($types as $type) {

            $method = "get" . ucfirst($type);

            if (method_exists("\\travelsoft\\booking\\crm\\Utils", $method)) {

                $result[$type] = self::$method();
            }
        }
        
        return $result;
    }
    
    /**
     * Возвращает select для валюты
     * @staticvar array $arCurrencies
     * @param string $code
     * @param string $currentValue
     * @return string
     */
    public static function getCurrencySelect (string $code, string $currentValue) : string {
        static $arCurrencies = null;

        if (empty($arCurrencies)) {
            $arCurrencies = \travelsoft\booking\adapters\CurrencyConverter::getAcceptableISO();
        }

        if (key_exists($code, $_POST)) {

            $currentValue = $_POST[$code];
        }

        if (strlen($currentValue) <= 0) {

            $currentValue = current($arCurrencies);
        }

        $select = '<select id="' . $code . '" name="' . $code . '">';
        foreach ($arCurrencies as $arCurrency) {

            $select .= '<option ' . ($arCurrency["ISO"] == $currentValue ? 'selected=""' : '') . ' value="' . $arCurrency["ISO"] . '">' . $arCurrency["ISO"] . '</option>';
        }
        $select .= '</select>';

        return $select;
    }
    
    /**
     * @param array $types
     * @return array
     */
    public static function getBookedServices (array $types, int $timestamp) {
        
        $arBookedServices = [];
        
        foreach ($types as $type) {
            
            $arBookedServices[$type] = $arServices = [];
            
            $method = "get" . ucfirst($type);

            if (method_exists("\\travelsoft\\booking\\crm\\Utils", $method)) {

                $arServices = self::$method();
            }
            
            foreach (\travelsoft\booking\stores\Bookings::get([
                "filter" => [">=UF_DATE_FROM" => \travelsoft\booking\adapters\Date::createFromTimestamp($timestamp), "UF_SERVICE_TYPE" => $type]
            ]) as $arBooking) {
                
                $arBookedServices[$type][$arBooking["UF_SERVICE"]] = \array_merge($arBooking, ["SERVICE_INFO" => $arServices[$arBooking["UF_SERVICE"]]]);
                
            }
            
        }
        
        return $arBookedServices;
        
    }
    
    /**
     * @param int $packagetourId
     * @return array
     */
    public static function getPlacementsByPackageTour (int $packagetourId) {
        $cache = new \travelsoft\booking\adapters\Cache(md5("getPlacementsByPackageTour_" . serialize(func_get_args())), Settings::getServicesTypes()["packagetour"]["cache_dir"]);
        
        if (empty($arPlacements = $cache->get())) {
            $arPlacements = $cache->caching(function () use ($packagetourId, $cache) {
                $cache->setTagCache("highloadblock_" . Settings::packagetourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::roomsStoreId());
                $cache->setTagCache("iblock_" . Settings::placementsStoreId());
                $cache->setTagCache("iblock_" . Settings::travelStoreId());
                $arPlacements = \travelsoft\booking\stores\Placements::get([
                    "filter" => ["ID" => \travelsoft\booking\stores\PackageTour::getById($packagetourId, ["UF_PLACEMENT"])["UF_PLACEMENT"]],
                    "select" => ["ID", "NAME"]
                ]);
                
                foreach ($arPlacements as &$arPlacement) {
                    $arPlacement["ROOMS"] = \travelsoft\booking\stores\Rooms::get([
                        "filter" => ["UF_PLACEMENT" => $arPlacement["ID"]]
                    ]);
                }
                
                return $arPlacements;
            });
        }

        return $arPlacements;
    }
    
    /**
     * @param int $excursiontour
     * @return array
     */
    public static function getPlacementsByExcursionTour (int $excursiontour) {
        $cache = new \travelsoft\booking\adapters\Cache(md5("getPlacementsByExcursionTour_" . serialize(func_get_args())), Settings::getServicesTypes()["excursiontour"]["cache_dir"]);
        
        if (empty($arPlacements = $cache->get())) {
            $arPlacements = $cache->caching(function () use ($excursiontour, $cache) {
                $cache->setTagCache("highloadblock_" . Settings::excursiontourStoreId());
                $cache->setTagCache("highloadblock_" . Settings::roomsStoreId());
                $cache->setTagCache("iblock_" . Settings::excursiontourIBStoreId());
                $cache->setTagCache("iblock_" . Settings::placementsStoreId());
                $arPlacements = \travelsoft\booking\stores\Placements::get([
                    "filter" => ["ID" => \travelsoft\booking\stores\ExcursionTour::getById($excursiontour, ["UF_PLACEMENTS"])["UF_PLACEMENTS"]],
                    "select" => ["ID", "NAME"]
                ]);
                
                foreach ($arPlacements as &$arPlacement) {
                    $arPlacement["ROOMS"] = \travelsoft\booking\stores\Rooms::get([
                        "filter" => ["UF_PLACEMENT" => $arPlacement["ID"]]
                    ]);
                }
                
                return $arPlacements;
            });
        }

        return $arPlacements;
    }
}
