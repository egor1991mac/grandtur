<?php

namespace travelsoft\booking\crm;

/**
 * Класс crm утилит для работы с автобусной базой
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class BusesUtils extends Utils {

    static public function processingEditForm() {

        $url = \travelsoft\booking\crm\Settings::BUSES_LIST_URL . '?lang=' . LANGUAGE_ID;

        if (strlen($_POST['CANCEL']) > 0) {

            LocalRedirect($url);
        }

        $arErrors = array();

        if (self::isEditFormRequest()) {

            $arSave = \travelsoft\booking\adapters\UserFieldsManager::editFormAddFields('HLBLOCK_' . \travelsoft\booking\Settings::busesStoreId(), array());

            $arUserFieldsData = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData('HLBLOCK_' . \travelsoft\booking\Settings::busesStoreId(), array());

            $arFields = array_keys($arUserFieldsData);

            foreach ($arFields as $field) {

                if ($field === "UF_NAME") {
                    Validator::strIsEmpty((string) $arSave[$field], $arUserFieldsData[$field]["EDIT_FORM_LABEL"], $arErrors);
                    continue;
                }
            }
            
            if (empty($arErrors)) {
                if ($_REQUEST['ID'] > 0) {
                    $ID = intVal($_REQUEST['ID']);
                    $result = \travelsoft\booking\stores\Buses::update($ID, $arSave);
                } else {
                    $result = \travelsoft\booking\stores\Buses::add($arSave);
                }
            }

            if ($result) {

                LocalRedirect($url);
            }
        }

        return array('errors' => $arErrors, 'result' => $result);
    }

    public static function getEditFormFields($arData) {

        $arUserFields = \travelsoft\booking\adapters\UserFieldsManager::getFieldsWithReadyData("HLBLOCK_" . \travelsoft\booking\Settings::busesStoreId(), $arData);

        $fields = array();
        foreach ($arUserFields as $fieldName => $arUserField) {
            
            if ($fieldName !== "UF_NAME") {
                continue;
            }
            
            $formField = array("label" => $arUserField["EDIT_FORM_LABEL"]);

            if (key_exists($arUserField['FIELD_NAME'], $_POST)) {

                $arUserField['VALUE'] = $_POST[$arUserField['FIELD_NAME']];
            }

            
            $formField["view"] = '<input name="' . $arUserField["FIELD_NAME"] . '" value="' . $arUserField["VALUE"] . '" type="text">';
            $formField["required"] = true;

            $fields[] = $formField;
        }
        return $fields;
    }

}
