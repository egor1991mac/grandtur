<?php

namespace travelsoft\booking\crm;

/**
 * Класс статических настроек CRM
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Settings {

    /**
     * ID таблицы касс
     */
    const CASH_DESKS_HTML_TABLE_ID = 'CASH_DESKS_TABLE';

    /**
     * ID таблицы бронировок
     */
    const BOOKINGS_HTML_TABLE_ID = 'BOOKINGS_TABLE';
    
    /**
     * ID таблицы типов расчета
     */
    const CALCULATION_TYPES_HTML_TABLE_ID = 'CALCULATION_TYPES_TABLE';
    
    /**
     * ID таблицы типов цен
     */
    const PRICE_TYPES_HTML_TABLE_ID = 'PRICE_TYPES_TABLE';

    /**
     * ID таблицы туристов
     */
    const TOURISTS_HTML_TABLE_ID = 'TOURISTS_TABLE';
    
    /**
     * ID таблицы наценки
     */
    const MARKUP_HTML_TABLE_ID = 'MARKUP_TABLE';

    /**
     * ID таблицы типов оплаты
     */
    const PAYMENTS_TYPES_HTML_TABLE_ID = 'PAYMENTS_TYPES_TABLE';

    /**
     * ID таблицы истории оплаты
     */
    const PAYMENT_HISTORY_HTML_TABLE_ID = 'PAYMENT_HISTORY_TABLE';

    /**
     * ID таблицы номерного фонда
     */
    const ROOMS_HTML_TABLE_ID = 'ROOMS_HTML_TABLE';
    
    /**
     * ID таблицы автобусной базы
     */
    const BUSES_HTML_TABLE_ID = 'BUSES_HTML_TABLE';
    
    /**
     * ID таблицы группы туров
     */
    const TOURISTSGROUPS_HTML_TABLE_ID = 'TOURISTSGROUPS_HTML_TABLE_ID';
    
    /**
     * ID таблицы экскурсионных туров
     */
    const EXCURSION_TOUR_HTML_TABLE_ID = 'EXCURSIONTOUR_HTML_TABLE';
    
    /**
     * ID таблицы турслуг
     */
    const TOURSERVICES_HTML_TABLE_ID = 'TOURSERVICES_HTML_TABLE';
    
    /**
     * ID таблицы пакетных туров
     */
    const PACKAGE_TOUR_HTML_TABLE_ID = 'PACKAGE_TOUR_HTML_TABLE';
    
    /**
     * ID таблицы тарифа
     */
    const RATES_HTML_TABLE_ID = 'RATES_HTML_TABLE';

    /**
     * ID таблицы клиентов
     */
    const CLIENT_LIST_HTML_TABLE_ID = 'CLIENT_LIST_TABLE';

    /**
     * ID таблицы скидок
     */
    const DISCOUNTS_LIST_HTML_TABLE_ID = 'CLIENT_LIST_TABLE';
    
    /**
     * ID теблицы путевок
     */
    const VOUCHERS_LIST_HTML_TABLE_ID = 'VOUCHERS_LIST_TABLE';

    /**
     * Url страницы добавления цен
     */
    const ADD_PRICES_URL = 'travelsoft_crm_booking_add_prices.php';
    
    /**
     * Url страницы списка путевок
     */
    const VOUCHERS_LIST_URL = 'travelsoft_crm_booking_voucher_list.php';
    
    /**
     * Url страницы списка путевок
     */
    const VOUCHER_EDIT_URL = 'travelsoft_crm_booking_voucher_edit.php';
    
    /**
     * Url страницы списка номеров
     */
    const ROOMS_LIST_URL = 'travelsoft_crm_booking_rooms.php';
    
    /**
     * Url страницы добавления/редактирования номера
     */
    const ROOM_EDIT_URL = 'travelsoft_crm_booking_room_edit.php';
    
    /**
     * Url страницы автобусные базы
     */
    const BUSES_LIST_URL = 'travelsoft_crm_booking_buses_list.php';
    
    /**
     * Url страницы экскурсионные туры
     */
    const EXCURSION_TOUR_LIST_URL = 'travelsoft_crm_booking_excursion_tour_list.php';
    
    /**
     * Url страницы добавления/редактирования автобуса
     */
    const BUS_EDIT_URL = 'travelsoft_crm_booking_bus_edit.php';
    
    /**
     * Url страницы добавления/редактирования экскурсионных туров
     */
    const EXCURSION_TOUR_EDIT_URL = 'travelsoft_crm_booking_excursion_tour_edit.php';
    
    /**
     * Url страницы списка наценок
     */
    const MARKUP_LIST_URL = 'travelsoft_crm_booking_markup_list.php';
    
    /**
     * Url страницы добавления/редактирования наценки
     */
    const MARKUP_EDIT_URL = 'travelsoft_crm_booking_markup_edit.php';
    
    /**
     * Url страницы списка туруслуг
     */
    const TOURSERVICES_LIST_URL = 'travelsoft_crm_booking_tourservices.php';
    
    /**
     * Url страницы добавления/редактирования туруслуги
     */
    const TOURSERVICE_EDIT_URL = 'travelsoft_crm_booking_tourservice_edit.php';
    
    /**
     * Url страницы списка пакетных туров
     */
    const PACKAGE_TOUR_LIST_URL = 'travelsoft_crm_booking_package_tour_list.php';
    
    /**
     * Url страницы добавления/редактирования пакетного тура
     */
    const PACKAGE_TOUR_EDIT_URL = 'travelsoft_crm_booking_package_tour_edit.php';
    
    /**
     * Url страницы списка тарифов
     */
    const RATES_LIST_URL = 'travelsoft_crm_booking_rates_list.php';
    
    /**
     * Url страницы добавления/редактирования тарифа
     */
    const RATES_EDIT_URL = 'travelsoft_crm_booking_rates_edit.php';

    /**
     * Url стрницы списка заказов
     */
    const BOOKINGS_LIST_URL = 'travelsoft_crm_booking_bookings_list.php';

    /**
     * Url стрницы редактирования заказа
     */
    const BOOKING_EDIT_URL = 'travelsoft_crm_booking_booking_edit.php';

    /**
     * Url страницы списка клиентов
     */
    const CLIENTS_LIST_URL = 'travelsoft_crm_booking_clients_list.php';

    /**
     * Url страницы редактирования клиента
     */
    const CLIENT_EDIT_URL = 'travelsoft_crm_booking_client_edit.php';

    /**
     * Url страницы списка туристов
     */
    const TOURISTS_LIST_URL = 'travelsoft_crm_booking_tourists_list.php';

    /**
     * Url страницы редактирования туриста
     */
    const TOURIST_EDIT_URL = 'travelsoft_crm_booking_tourist_edit.php';

    /**
     * Url страницы скидок
     */
    const DISCOUNTS_LIST_URL = 'travelsoft_crm_booking_discounts.php';

    /**
     * Url страницы редактирования скидки
     */
    const DISCOUNT_EDIT_URL = 'travelsoft_crm_booking_discount_edit.php';

    /**
     * Url страницы документов
     */
    const DOCUMENTS_URL = 'travelsoft_crm_booking_documents.php';

    /**
     * Url страницы редактирования документа
     */
    const DOCUMENT_EDIT_URL = 'travelsoft_crm_booking_document_edit.php';

    /**
     * Url страницы списка касс
     */
    const CASH_DESKS_LIST_URL = 'travelsoft_crm_booking_cash_desks_list.php';

    /**
     * Url страницы добавления/редактирования касс
     */
    const CASH_DESK_EDIT_URL = 'travelsoft_crm_booking_cash_desk_edit.php';

    /**
     * Url страницы списка типов платежей
     */
    const PAYMENTS_TYPES_LIST_URL = 'travelsoft_crm_booking_payments_types_list.php';

    /**
     * Url страницы списка типов платежей
     */
    const PAYMENT_TYPE_EDIT_URL = 'travelsoft_crm_booking_payment_type_edit.php';

    /**
     * Url страницы списка истории платежей
     */
    const PAYMENT_HISTORY_LIST_URL = 'travelsoft_crm_booking_payment_history_list.php';
    
    /**
     * Url страницы списка групп туристов
     */
    const TOURISTS_GROUPS_LIST_URL = 'travelsoft_crm_tourists_groups_list.php';
    
    /**
     * Url страницы редактирования группы туристов
     */
    const TOURIST_GROUP_EDIT_URL = 'travelsoft_crm_tourist_group_edit.php';

    /**
     * Url страницы добавления/редактирования истории платежей
     */
    const PAYMENT_HISTORY_EDIT_URL = 'travelsoft_crm_booking_payment_history_edit.php';
    
    /**
     * Url страницы списка типов расчета
     */
    const CALCULATION_TYPES_LIST_URL = 'travelsoft_crm_booking_calculation_types_list.php';
    
    /**
     * Url страницы добавления/редактирования типов расчета
     */
    const CALCULATION_TYPES_EDIT_URL = 'travelsoft_crm_booking_calculation_types_edit.php';
    
    /**
     * Url страницы списка типов цен
     */
    const PRICE_TYPES_LIST_URL = 'travelsoft_crm_booking_price_types_list.php';
    
    /**
     * Url страницы добавления/редактирования типов цен
     */
    const PRICE_TYPES_EDIT_URL = 'travelsoft_crm_booking_price_types_edit.php';
    
    /**
     * Относительный путь к папке модуля
     */
    const REL_PATH_TO_MODULE = "/local/modules/travelsoft.travelbooking";
    
    /**
     * Относительный путь к папке js модуля 
     */
    const REL_PATH_TO_MODULE_JS = "/local/modules/travelsoft.travelbooking/crm/js";
    
    /**
     * Относительный путь к папке css модуля 
     */
    const REL_PATH_TO_MODULE_CSS = "/local/modules/travelsoft.travelbooking/crm/css";
    
    /**
     * Относительный путь к папке ajax обработчиков модуля 
     */
    const REL_PATH_TO_MODULE_AJAX = "/local/modules/travelsoft.travelbooking/crm/ajax";
}
