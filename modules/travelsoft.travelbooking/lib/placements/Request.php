<?php

namespace travelsoft\booking\placements;

/**
 * Класс запроса на поиск проживания
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class Request extends \travelsoft\booking\Request{

    /**
     * @var array
     */
    public $rooms_id = null;

}
