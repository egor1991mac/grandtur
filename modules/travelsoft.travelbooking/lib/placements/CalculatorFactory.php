<?php

namespace travelsoft\booking\placements;

/**
 * Класс-фабрика Calculator
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class CalculatorFactory {

    public static function create(\travelsoft\booking\Request $request) {

        $arr_filter = array();

        if (!empty($request->services_id)) {
            $arr_filter["UF_PLACEMENT"] = $request->services_id;
        }

        if (!empty($request->rooms_id)) {
            $arr_filter["ID"] = $request->rooms_id;
        }

        // группируем номера по отелям
        $arr_services_id = $arr_group = array();
        foreach (\travelsoft\booking\stores\Rooms::get(array(
            "filter" => $arr_filter,
            "select" => array("ID", "UF_PLACEMENT")
        )) as $arr_room) {

            $arr_services_id[] = $arr_room["ID"];
            $arr_placements[] = $arr_room["UF_PLACEMENT"];
            $arr_group[$arr_room["ID"]] = $arr_room["UF_PLACEMENT"];
        }

        if (!empty($arr_services_id)) {

            // поиск туруслуг
            $tourservices = \travelsoft\booking\Utils::findTourservices($arr_placements, "placements", $request->adults, $request->children);

            // поиск наценок
            $markup = array();

            $arr_markup = \travelsoft\booking\stores\Markup::get(array(
                        "filter" => array(
                            "UF_SERVICES" => !empty($arr_placements) ? $arr_placements : array(-1),
                            "UF_SERVICE_TYPE" => "placements"),
                        "limit" => 1
            ));

            $arr_total_markup = current(\travelsoft\booking\stores\Markup::get(array(
                        "filter" => array(
                            "UF_SERVICES" => false,
                            "UF_SERVICE_TYPE" => "placements"),
                        "limit" => 1
            )));

            if (!empty($arr_markup) || !empty($arr_total_markup)) {

                foreach ($arr_placements as $service_id) {
                    $key = false;
                    foreach ($arr_markup as $arrr_markup) {
                        $key = array_search($service_id, $arrr_markup["UF_SERVICES"]);
                        if ($key !== false) {
                            $markup[$service_id] = array(
                                "price" => (float) $arrr_markup["UF_PRICE"],
                                "currency" => (string) $arrr_markup["UF_CURRENCY"],
                            );
                            break;
                        }
                    }
                    if ($key === false && !empty($arr_total_markup)) {
                        $markup[$service_id] = array(
                            "price" => (float) $arr_total_markup["UF_PRICE"],
                            "currency" => (string) $arr_total_markup["UF_CURRENCY"]
                        );
                    }
                }
            }

            // поиск скидок
            $discount = array();
            $time = \travelsoft\booking\adapters\Date::createFromTimestamp(time());
            if (\travelsoft\booking\adapters\User::isAgent()) {

                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arr_placements,
                                "UF_SERVICE_TYPE" => "placements",
                                "UF_DTYPE" => "A",
                                "!UF_VALUE" => false,
                                "UF_AGENT" => \travelsoft\booking\adapters\User::id(),
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));

                if (empty($arr_discount)) {

                    $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                "filter" => array(
                                    "UF_SERVICES" => $arr_placements,
                                    "UF_SERVICE_TYPE" => "placements",
                                    "UF_DTYPE" => "A",
                                    "!UF_VALUE" => false,
                                    "<=UF_LP_FROM" => $time,
                                    ">=UF_LP_TO" => $time
                                ),
                                "order" => array(
                                    "UF_SORT" => "ASC",
                                    "ID" => "DESC"),
                                "limit" => 1)));

                    if (empty($arr_discount)) {

                        $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                                    "filter" => array(
                                        "UF_SERVICES" => $arr_placements,
                                        "UF_SERVICE_TYPE" => "placements",
                                        "UF_DTYPE" => "PR",
                                        "<=UF_LP_FROM" => $time,
                                        ">=UF_LP_TO" => $time
                                    ),
                                    "order" => array(
                                        "UF_SORT" => "ASC",
                                        "ID" => "DESC"),
                                    "limit" => 1)));
                    }
                }
            } else {

                $arr_discount = current(\travelsoft\booking\stores\Discounts::get(array(
                            "filter" => array(
                                "UF_SERVICES" => $arr_placements,
                                "UF_SERVICE_TYPE" => "placements",
                                "UF_DTYPE" => "PR",
                                "<=UF_LP_FROM" => $time,
                                ">=UF_LP_TO" => $time
                            ),
                            "order" => array(
                                "UF_SORT" => "ASC",
                                "ID" => "DESC"),
                            "limit" => 1)));
            }

            if (!empty($arr_discount)) {
                $discount = $arr_discount;
            }

            return new Calculator(\travelsoft\booking\rooms\CalculatorFactory::create(\travelsoft\booking\rooms\RequestFactory::create(array(
                                "services_id" => $arr_services_id,
                                "rates_id" => $request->rates_id,
                                "date_from" => $request->date_from,
                                "date_to" => $request->date_to,
                                "adults" => $request->adults,
                                "children" => $request->children,
                                "children_age" => $request->children_age
                    ))), $arr_group, $tourservices, $markup, $discount, $request);
        }



        return new Calculator(\travelsoft\booking\rooms\CalculatorFactory::create(\travelsoft\booking\rooms\RequestFactory::create(array())), array(), array(), array(), array(), null);
    }

}
