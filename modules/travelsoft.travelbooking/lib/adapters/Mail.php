<?php

namespace travelsoft\booking\adapters;

use travelsoft\booking\Settings;

/**
 *  Класс адаптер отправки почтовых уведомлений
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Mail {

    protected static $_commonMailParameters = array(
        "EVENT_NAME" => Settings::MAIL_EVENT_TYPE,
        "LID" => "s1",
        "DUPLICATE" => "N"
    );
    
    private static function __sendMail (array $fields, int $message_id) {
        \Bitrix\Main\Mail\Event::send(array_merge(array(
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => $message_id
                        ), self::$_commonMailParameters));
    }
    
    /**
     * Отправка почтового уведомления клиенту при создании бронировки
     * @param array $fields
     */
    public static function sendNewBookingNotificationForClient(array $fields) {
        
        self::__sendMail($fields, Settings::clientBookingMailId());
    }

    /**
     * Отправка почтового уведомления менеджеру при создании бронировки
     * @param array $fields
     */
    public static function sendNewBookingNotificationForManager(array $fields) {
        
        self::__sendMail($fields, Settings::managerBookingMailId());
    }
    
    /**
     * Отправка письма пользователю, что он зарегистрирован как агент
     * @param array $fields
     */
    public static function sendAgentRegisterNotification(array $fields) {
        
        self::__sendMail($fields, Settings::agentRegisterMailId());
    }
    
    /**
     * Отправка письма менеджеру, что пользователь зарегистрирован как агент
     * @param array $fields
     */
    public static function sendAgentRegisterNotificationForManager(array $fields) {
        
        self::__sendMail($fields, Settings::agentRegisterMailIdForManager());
    }
    
    /**
     * Отправка письма пользователю, что он активирован как агент
     * @param array $fields
     */
    public static function sendAgentActivated (array $fields) {
        
        self::__sendMail($fields, Settings::agentActivatedMailId());
    }
    
    /**
     * Отправка письма пользователю, что сменился статус путевки
     * @param array $fields
     */
    public static function sendChangeStatusNotification (array $fields) {
        
        self::__sendMail($fields, Settings::voucherStatusChangedMailId());
    }
    
    /**
     * Отправка даты записи в посольство
     * @param array $fields
     */
    public static function sendEmbassyDate (array $fields) {
        
        self::__sendMail($fields, Settings::embassyDateMailId());
    }
    
    /**
     * Отправка даты выдачи документов
     * @param array $fields
     */
    public static function sendDocumentsDate (array $fields) {
        
        self::__sendMail($fields, Settings::documentsDateMailId());
    }
    
    /**
     * Отправка уведомления по добавленному сообщению менеджеру
     * @param array $fields
     */
    public static function sendManagerVoucherMessageNotification (array $fields) {
        
        self::__sendMail($fields, Settings::managerVoucherMessageNotificationMailId());
    }
    
    /**
     * Отправка уведомления по добавленному сообщению клиенту
     * @param array $fields
     */
    public static function sendClientVoucherMessageNotification (array $fields) {
        
        self::__sendMail($fields, Settings::clientVoucherMessageNotificationMailId());
    }
}
