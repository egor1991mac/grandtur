<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Iblock;

/**
 * Класс для работы с таблицей описания туров
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Toursdesc extends Iblock{

    protected static $storeName = 'travel';
}
