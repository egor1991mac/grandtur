<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей сообщений
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Messages extends Highloadblock {

    protected static $storeName = 'messages';
}
