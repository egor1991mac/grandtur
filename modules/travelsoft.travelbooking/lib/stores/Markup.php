<?php

namespace travelsoft\booking\stores;

use travelsoft\booking\adapters\Highloadblock;

/**
 * Класс для работы с таблицей наценки
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class Markup extends Highloadblock {

    protected static $storeName = 'markup';
}