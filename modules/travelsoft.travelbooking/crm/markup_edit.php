<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\crm\Utils;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS."/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='".\travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS."/common.js?".randString(7)."'></script>");
?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление наценки';
    if ($ID > 0) {

        $arMarkup = current(\travelsoft\booking\stores\Markup::get(array('filter' => array('ID' => $ID))));

        if (!$arMarkup['ID']) {

            throw new Exception('Наценка с ID="' . $ID . '" не найдена');
        }

        $title = 'Редактирование наценки #' . $arMarkup['ID'];
    }

    $APPLICATION->SetTitle($title);

    $arResult = travelsoft\booking\crm\MarkupUtils::processingEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }
    
    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'markup_form',
        'id' => 'markup_form',
        'tabs' => array(
            array(
                "DIV" => "MARKUP_FIELD",
                "TAB" => 'Поля для добавления/редактирования наценки',
                "FIELDS" => travelsoft\booking\crm\MarkupUtils::getEditFormFields($arMarkup)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            ),
            array(
                'name' => 'CANCEL',
                'value' => 'Отменить'
            )
        )
    ));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

