/**
 * common.js
 * 
 * @author dimabresky
 * @copyright 2018, travelsoft
 */

$(document).ready(function () {

    $(".select2").each(function () {

        var $this = $(this);
        var select;

        $this.select2();

        if ($this.data("services-target")) {
            select = $($this.data("services-target"));

            $this.on("select2:select", function () {

                var type = $(this).val();

                if (type in jsBookingServices) {

                    select.html((function (services) {

                        var html = "";
                        for (var i = 0; i < services.REFERENCE.length; i++) {
                            html += `<option value="${services.REFERENCE_ID[i]}">${services.REFERENCE[i]}</option>`;
                        }

                        return html;
                    })(jsBookingServices[type]));

                    select.select2('destroy');
                    select.select2();
                }

            });
        }
    });

    $(".__ui_datepicker").each(function () {
        $(this).datepicker({
            dateFormat: "dd.mm.yy",
            minDate: new Date()
        });
    })
});