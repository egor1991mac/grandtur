/* 
 * Tourists groups admin page
 */

$(document).ready(function () {

    "use strict";

    var __tsconfig = window.__tsconfig;

    var utils = {

        __destroySelect2: function ($context) {
            if ($context.data('select2-inited') === "Y") {
                $context.select2('destroy');
            }
        },
        __initSelect2: function ($context, options) {

            $context.select2(options || {});
            $context.data('select2-inited', "Y");
        },
        __initSelects2: function (options) {

            options.forEach(function (it) {
                it.contexts.each(function () {
                    utils.__destroySelect2($(this));
                    utils.__initSelect2($(this), it.options);
                });
            });

        },
        __destroyDatepicker: function ($calendar) {
            $calendar.val('');
            $calendar.datepicker("destroy");
            $calendar.data('datepicker-inited', 'N');
        },
        __initDatepicker: function ($calendar, beforeShowDay) {
            $calendar.val('');
            if ($calendar.data('datepicker-inited') === 'Y') {
                utils.__destroyDatepicker($calendar);
            }
            $calendar.each(function () {
                $(this).datepicker({
                    dateFormat: "dd.mm.yy",
                    minDate: new Date(),
                    beforeShowDay: beforeShowDay
                });
            });
            $calendar.data('datepicker-inited', 'Y');
        },
        __setPopupContent: function (popup, $edit_checkboxes) {

            popup.setContent((function () {
                var tour = $('select[name=tour]').val();
                return `<form class="edit-tourists-groups-form" action="#" type="POST" >
                    <input name="sessid" type="hidden" value="${BX.bitrix_sessid()}">
                    <input name="action" type="hidden" value="edit-tourists-groups">
                    <input name="tour" type="hidden" value="${tour}">
                    <input name="date_from" type="hidden" value="${$('input[name=date_from]').val()}">
                    ${(function () {
                    var hidden_inputs = '';
                    $edit_checkboxes.each(function () {

                        hidden_inputs += `<input name="tourists_id[]" value="${$(this).data('tourist-id')}" type="hidden">`;

                    });

                    return hidden_inputs;
                })()}
                    <div class="visible-input-section">
                        <div class="form-group">
                            <label><b>Группа</b>:</label> <select name="group">${utils.__getCountableSelectOptions(5)}</select>
                        </div>
                        <div class="form-group">
                            
                            <label><b>Номер</b>: </label>
                            <select name="room_id">
                                ${(function () {
                    var hotel = {};
                    var options = '';
                    var $first = true;

                    if (!$.isArray(__tsconfig.tours[tour].HOTELS) && __tsconfig.tours[tour].HOTELS) {
                        for (var k in __tsconfig.tours[tour].HOTELS) {
                            if (__tsconfig.tours[tour].HOTELS.hasOwnProperty(k)) {
                                hotel = __tsconfig.tours[tour].HOTELS[k];

                                options += `<optgroup label="${hotel.NAME}">${
                                        (function (rooms) {

                                            var options = '';
                                            rooms.forEach(function (room) {
                                                options += `<option ${$first ? 'selected=""' : ''} value="${room.ID}">${room.UF_NAME}</option>`;
                                            });
                                            return options;
                                        })(hotel.ROOMS)
                                        }</optgroup>`;
                            }
                            $first = false;
                        }
                    }
                    return options;
                })()}
                            </select>
                            <label><b>#</b>: </label>
                            <select name="room_number">
                                ${utils.__getCountableSelectOptions(5)}
                            </select>
                        </div>
                    </div>
                </form>`;

            })());

            return popup;
        },
        __getCountableSelectOptions: function (count) {
            var options = '<option value="1">1</option>';

            for (var i = 2; i <= count; i++) {
                options += `<option value="${i}">${i}</option>`;
            }

            return options;
        },
        __setTableArea: function (html) {
            $("#tourists-groups-table").html(html);
        },

        __exportToExcel: (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                    , base64 = function (s) {
                        return window.btoa(unescape(encodeURIComponent(s)));
                    }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            }
            , downloadURI = function (uri, name) {
                var link = document.createElement("a");
                link.download = name;
                link.href = uri;
                link.click();
            };

            return function (name, fileName) {
                var table_html = $('#tourists-groups-table').find('table').html().replace('<th>Edit</th>', '').replace(/<td class="edit-td">(.*)<\/td>/g, '');
                var ctx = {worksheet: name || 'Worksheet', table: table_html};
                var resuri = uri + base64(format(template, ctx));
                downloadURI(resuri, fileName);
            };
        })()
    };

    var events = {

        clickByExportExcel: function () {

            utils.__exportToExcel('Группы туристов', 'tourists_groups.xls');
        },

        /**
         * @param {DOMElement} context
         * @returns {undefined}
         */
        changeSelectTour: function (context) {

            var $hotelsSelect = $('select[name="hotels[]"]');

            if (context.value && typeof __tsconfig.tours[context.value] !== "undefined") {

                $hotelsSelect.html((function (hotels) {

                    var options = '';

                    for (var k in hotels) {
                        if (hotels.hasOwnProperty(k)) {
                            options += `<option value="${hotels[k].ID}">${hotels[k].NAME}</option>`;
                        }
                    }

                    return options;

                })(__tsconfig.tours[context.value].HOTELS));

                $hotelsSelect.prop('disabled', false);

                
                $('input[name=date_from]').prop('disabled', false);
                utils.__initDatepicker($('input[name=date_from]'), function (date) {
                    
                    return [true, $.inArray(moment(date).format('DD.MM.YYYY'), __tsconfig.dates[context.value]) !== -1 ? '__highlight' : ''];
                });

            } else {

                $hotelsSelect.html(`<option></option>`);
                $hotelsSelect.prop('disabled', true);
                $('input[name=date_from]').prop('disabled', true);
                utils.__destroyDatepicker($('input[name=date_from]'));
            }

            utils.__initSelects2([{contexts: $hotelsSelect, options: {}}]);
        },

        /**
         * @param {DOMElement} context
         * @returns {undefined}
         */
        changeSelectCountry: function (context) {

            var $toursSelect = $('select[name=tour]');

            if (context.value && typeof __tsconfig.countries[context.value] !== "undefined") {

                $toursSelect.html((function (tours) {

                    var options = '<option></option>';

                    for (var k in tours) {
                        if (tours.hasOwnProperty(k)) {
                            options += `<option value="${k}">${tours[k]}</option>`;
                        }
                    }

                    return options;

                })((function (country_id) {

                    var tours = {};

                    for (var tour_id in __tsconfig.tours) {
                        if (__tsconfig.tours.hasOwnProperty(tour_id)) {
                            if ($.inArray(country_id, __tsconfig.tours[tour_id].COUNTRIES_ID) === -1) {
                                tours[tour_id] = __tsconfig.tours[tour_id].NAME;
                            }
                        }
                    }

                    return tours;

                })(context.value)));

                $toursSelect.prop('disabled', false);

            } else {

                $toursSelect.html(`<option></option>`);
                $toursSelect.prop('disabled', true);
            }

            utils.__initSelects2([{contexts: $toursSelect, options: {}}]);
            $toursSelect.trigger('change');
        },

        /**
         * @param {DOMElement} checkbox
         * @returns {undefined}
         */
        clickByEditCheckbox: function (checkbox) {

            if ($(checkbox).is(":checked")) {
                $('#tourists-groups-table tfoot').show();
            } else if (!$('#tourists-groups-table').find('input[name=to_edit]:checked').length) {
                $('#tourists-groups-table tfoot').hide();
            }
        },

        /**
         * @param {DOMElement} btn
         * @returns {undefined}
         */
        clickByEditBtn: function (btn) {

            var $edit_checkboxes = $('#tourists-groups-table').find('input[name=to_edit]:checked');

            var popup = null;

            if (!$edit_checkboxes.length) {
                return;
            }

            popup = new BX.PopupWindow('edit-tourists-groups-popup', window.body, {
                closeIcon: {top: "0"},
                zIndex: 0,
                offsetLeft: 0,
                width: "600px",
                offsetTop: 0,
                draggable: {restrict: false},
                overlay: {backgroundColor: 'black', opacity: '80'},
                buttons: [
                    new BX.PopupWindowButton({
                        text: "Сохранить",
                        className: "edit-tourists-groups-save",
                        events: {
                            click: function () {
                                var _this = this;
                                var form = $(".edit-tourists-groups-form");
                                $.post(__tsconfig.ajax_url, form.serialize(), function (resp) {

                                    _this.popupWindow.destroy();
                                    $("#tourists-groups-generator").trigger('submit');
                                }, "json");

                            }
                        }
                    }),
                    new BX.PopupWindowButton({
                        text: "Отмена",
                        className: "edit-tourists-groups-cancel",
                        events: {
                            click: function () {
                                this.popupWindow.destroy();
                            }
                        }
                    })
                ]
            });

            utils.__destroySelect2($('select[name=group]'));
            utils.__destroySelect2($('select[name=room_id]'));
            utils.__destroySelect2($('select[name=room_number]'));

            utils.__setPopupContent(popup, $edit_checkboxes).show();

            setTimeout(function () {
                utils.__initSelect2($('select[name=group]'));
                utils.__initSelect2($('select[name=room_id]'));
                utils.__initSelect2($('select[name=room_number]'));
            }, 500);
        }
    };

    // переопределяем закрытие popup на удаление окна
    BX.PopupWindow.prototype._onCloseIconClick = function (event) {
        event = event || window.event;
        this.destroy();
        BX.PreventDefault(event);
    };

    utils.__initSelects2([{contexts: $('.select-2'), options: {}}]);

    $("#tourists-groups-generator").submit(function (e) {

        var $this = $(this);

        var errors = [];

        $(".overlay").show();

        if (!$this.find('select[name=tour]').val()) {
            errors.push('Укажите тур');
        }

        if (!$this.find('input[name=date_from]').val()) {
            errors.push('Укажите дату тура');
        }

        if (errors.length) {
            alert(errors.join('\n'));
            $(".overlay").hide();
        } else {

            $.ajax({
                url: $this.attr('action'),
                data: $this.serializeArray(),
                beforeSend: function () {
                    utils.__setTableArea('');
                },
                complete: function () {
                    $(".overlay").hide();
                },
                success: function (resp) {

                    if (resp.error) {
                        alert(resp.error);
                        return;
                    }

                    if (resp.result !== null) {

                        utils.__setTableArea(resp.result);
                    }
                }
            });
        }

        e.preventDefault();
    });

    $("#form-collapser").on('click', function () {

        var $this = $(this);

        var target = $this.data('target');

        var next_title = $this.data('title');

        var current_title = $this.text();

        $(target).toggleClass('collapsed');

        $this.data('title', current_title);

        $this.text(next_title);

    });

    // Смена select'a по отелям при выборе тура
    $('select[name=tour]').on('change', function () {
        events.changeSelectTour(this);
    });

    // Смена select'a по турам при выборе страны
    $('select[name=country]').on('change', function () {
        events.changeSelectCountry(this);
    });

    // чекбокс редактирования
    $('#tourists-groups-table').on('click', 'input[name=to_edit]', function () {
        events.clickByEditCheckbox(this);
    });

    // выгрузка в excel
    $('#tourists-groups-table').on('click', '.excel-btn', function () {
        events.clickByExportExcel(this);
    });

    // клик по кнопке "Редактировать"
    $('#tourists-groups-table').on('click', 'input[name=show_edit]', function () {
        events.clickByEditBtn(this);
    });
});


