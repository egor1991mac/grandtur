<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Vouchers;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/vouchers-list.css'>");

$APPLICATION->AddHeadString("<link rel='stylesheet' href='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_CSS . "/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/plugins/select2.full.min.js'></script>");
$APPLICATION->AddHeadString("<script src='" . \travelsoft\booking\crm\Settings::REL_PATH_TO_MODULE_JS . "/common.js?" . randString(7) . "'></script>");

$sort = new CAdminSorting(Settings::VOUCHERS_LIST_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::VOUCHERS_LIST_HTML_TABLE_ID, $sort);

if ($arVouchersId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arVouchersId = array_keys(Vouchers::get(array('select' => array('ID'))));
    }

    foreach ($arVouchersId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Vouchers::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}
$filter = travelsoft\booking\crm\VouchersUtils::getFilter();
$getParams = array("order" => array($by => $order), 'filter' => $filter);

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::VOUCHERS_LIST_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Vouchers::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)')), 'filter' => $filter), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arVouchers = Vouchers::get($getParams);

$dbResult = new CAdminResult($arVouchers, Settings::VOUCHERS_LIST_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "Номер",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_DATE_CREATE",
        "content" => "Дата создания",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "NAME",
        "content" => "Название",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_DATE_FROM",
        "content" => "Дата начала",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_СLIENT",
        "content" => "Клиент",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_MANAGER",
        "content" => "Менеджер",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "TOTAL_COST_FORMATTED",
        "content" => "Стоимость",
        "align" => "center",
        "default" => true
    )
));

$vouchers_exists = false;
while ($arVoucher = $dbResult->Fetch()) {
    $vouchers_exists = true;
    $row = $list->AddRow($arVoucher["ID"], $arVoucher);
    \travelsoft\booking\crm\VouchersUtils::prepareRowForVochersTable($row, $arVoucher);
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect(Settings::VOUCHER_EDIT_URL . "?ID=" . $arVoucher["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить путевку')) " . $list->ActionDoGroup($arVoucher["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать путевку",
        'TITLE' => "Создание путевки",
        'LINK' => Settings::VOUCHER_EDIT_URL . '?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список путевок");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


$tours = $clients = $managers = $agents = ['' => ''];

foreach (\travelsoft\booking\stores\ExcursionTour::get() as $tour) {
    $tours['excursiontour_' . $tour['ID']] = $tour['UF_NAME'];
}

foreach (\travelsoft\booking\stores\PackageTour::get() as $tour) {
    $tours['packagetour_' . $tour['ID']] = travelsoft\booking\stores\Toursdesc::getById(intval($tour['UF_TOUR']))['NAME'];
}

foreach (travelsoft\booking\stores\Users::get(['filter' => ['GROUPS_ID' => [travelsoft\booking\Settings::clientsUGroup()]]]) as $client) {
    $clients[$client['ID']] = $client['FULL_NAME_WITH_EMAIL'];
}

foreach (travelsoft\booking\stores\Users::get(['filter' => ['GROUPS_ID' => [travelsoft\booking\Settings::agentsUGroup()]]]) as $agent) {
    $agents[$agent['ID']] = $agent['FULL_NAME_WITH_EMAIL'];
}

foreach (travelsoft\booking\stores\Users::get(['filter' => ['GROUPS_ID' => [travelsoft\booking\Settings::managersUGroup()]]]) as $manager) {
    $managers[$manager['ID']] = $manager['FULL_NAME_WITH_EMAIL'];
}

\travelsoft\booking\crm\Utils::showFilterForm(
        array(
            'table_id' => Settings::VOUCHERS_LIST_HTML_TABLE_ID,
            'form_elements' => array(
                array(
                    'label' => 'Тур',
                    'view' => SelectBoxFromArray("TOUR_ID", array(
                        "REFERENCE" => \array_values($tours),
                        "REFERENCE_ID" => \array_keys($tours)), @$_GET['TOUR_ID'], "", 'class="adm-filter-select select2"', false, "find_form")
                ),
                array(
                    'label' => 'Клиент',
                    'view' => SelectBoxFromArray("CLIENT", array(
                        "REFERENCE" => \array_values($clients),
                        "REFERENCE_ID" => \array_keys($clients)), $_GET['CLIENT'], "", 'class="adm-filter-select select2"', false, "find_form")
                ),
                array(
                    'label' => 'Агент',
                    'view' => SelectBoxFromArray("AGENT", array(
                        "REFERENCE" => \array_values($agents),
                        "REFERENCE_ID" => \array_keys($agents)), $_GET['AGENT'], "", 'class="adm-filter-select select2"', false, "find_form")
                ),
                array(
                    'label' => 'Менеджер',
                    'view' => SelectBoxFromArray("MANAGER", array(
                        "REFERENCE" => \array_values($managers),
                        "REFERENCE_ID" => \array_keys($managers)), $_GET['MANAGER'], "", 'class="adm-filter-select select2"', false, "find_form")
                ),
                array(
                    'label' => 'Дата создания',
                    'view' => \CAdminCalendar::CalendarDate('DATE_CREATE', @$_GET['DATE_CREATE'])
                ),
                array(
                    'label' => 'Дата начала',
                    'view' => \CAdminCalendar::CalendarDate('DATE_FROM', @$_GET['DATE_FROM'])
                )
            ),
        )
);

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
