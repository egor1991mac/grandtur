<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if (!travelsoft\booking\crm\Utils::access() || !$request->isAjaxRequest()) {
    
    echo \travelsoft\booking\crm\Utils::sendJsonResponse(\Bitrix\Main\Web\Json::encode(array('error' => 'access denided')));
    die;
}

$arResponse = array('error' => null, 'items' => array(), 'result' => null);
