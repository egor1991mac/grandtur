<?php

require_once 'header.php';

try {
    switch ($request->getPost('action')) {

        case "add-message":

            $message = \strip_tags(\trim((string) $request->getPost('message')));

            if (!strlen($message) || $request->getPost('voucher_id') <= 0) {
                throw new \Exception('Bad message or voucher id');
            }
            
            $data = [
                'user' => \travelsoft\booking\stores\Users::getById(travelsoft\booking\adapters\User::id()),
                'date' => date('d.m.Y H:i:s', time()),
                'message' => $message
            ];
            
            $message_id = \travelsoft\booking\stores\Messages::add([
                        'UF_MESSAGE' => $message,
                        'UF_USER_ID' => $data['user']['ID'],
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp(\strtotime($data['date'])),
                        'UF_VOUCHER_ID' => intVal($request->getPost('voucher_id'))
            ]);

            if ($message_id <= 0) {
                 throw new \Exception('Message can\'t be added');
            }
            
            $client = travelsoft\booking\stores\Users::getById((int)travelsoft\booking\stores\Vouchers::getById(intVal($request->getPost('voucher_id')))['UF_CLIENT']);
            
            \travelsoft\booking\adapters\Mail::sendClientVoucherMessageNotification([
                "#EMAIL_TO#" => $client['EMAIL'],
                "#VOUCHER_ID#" => intVal($request->getPost('voucher_id')),
                "#MESSAGE_ID#" => $message_id
            ]);
            
            travelsoft\booking\crm\Utils::sendJsonResponse(\json_encode(['message_id' => $message_id, 'data' => $data]));
            
            break;
        case "get-messages":



            break;
    }
} catch (Exception $ex) {
    travelsoft\booking\crm\Utils::sendJsonResponse(\json_encode(['error' => true, 'message' => $ex->getMessage()]));
}


