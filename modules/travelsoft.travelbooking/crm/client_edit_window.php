<?php
require_once 'header.php';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

use travelsoft\booking\crm\Utils;

$APPLICATION->AddHeadString("<link rel='stylesheet' href='/local/modules/travelsoft.travelbooking/crm/css/select2.min.css'>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/jquery-3.2.1.min.js'></script>");
$APPLICATION->AddHeadString("<script src='/local/modules/travelsoft.travelbooking/crm/js/plugins/select2.full.min.js'></script>");

$APPLICATION->SetTitle('Заполнение данных клиента');

try {
    $ID = intval($_REQUEST["ID"]);

    $arr_client = array();
    if ($ID > 0) {
        $arr_client = current(travelsoft\booking\stores\Users::get(array(
                    "filter" => array("ID" => $ID),
                    "select" => array("ID", "NAME", "SECOND_NAME", "LAST_NAME", "EMAIL", "PERSONAL_PHONE", "PERSONAL_BIRTHDAY", "UF_PASS_SERIES", "UF_PASS_NUMBER", "UF_ADDRESS", "UF_PASS_DATE_ISSUE", "UF_PASS_ISSUED_BY")
        )));
        if (!isset($arr_client["ID"])) {
            throw new Exception("Клиент с ID=" . $ID . " не найден.");
        }
    }

    $arResult = travelsoft\booking\crm\ClientUtils::processingEditForm();

    if ($arResult['result']) {

        $arUser = current(travelsoft\booking\stores\Users::get(array('filter' => array('ID' => $arResult['result']), 'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME'))));
        ?>
        <script>

            var value = Number("<?= $arUser['ID'] ?>");
            var text = "<?=
        implode(' ', array_filter(array($arUser['NAME'], $arUser['SECOND_NAME'], $arUser['LAST_NAME']), function ($item) {
                    return strlen($item) > 0;
                }))
        ?>";
        <? if (!$ID): ?>
                var pd = window.opener.document;
                var jq = window.opener.jQuery;
                var clientSelect = jq("#UF_CLIENT");
                clientSelect.append(`<option selected="" value="${value}">${text}</option>`);
                clientSelect.trigger("change");
                clientSelect.select2("destroy");
                clientSelect.select2();
        <? endif ?>
            window.close();
        </script>
        <?
    } elseif (!$ID) {
        ?>
        <script>

            $(document).ready(function () {

                var $touristsSelect = $("#TOURISTS");

                $touristsSelect.select2();

                $touristsSelect.on("change", function () {

                    var id = $(this).val();

                    if (typeof jsTouristsStorage === "object" && id > 0) {

                        $("#NAME").val(jsTouristsStorage[id].UF_NAME);
                        $("#SECOND_NAME").val(jsTouristsStorage[id].UF_SECOND_NAME);
                        $("#LAST_NAME").val(jsTouristsStorage[id].UF_LAST_NAME);
                        $("input[name=PERSONAL_BIRTHDAY]").val(jsTouristsStorage[id].BIRTHDATE);
                        $("#UF_PASS_SERIES").val(jsTouristsStorage[id].UF_PASS_SERIES);
                        $("#UF_PASS_NUMBER").val(jsTouristsStorage[id].UF_PASS_NUMBER);
                        $("#UF_PASS_DATE_ISSUE").val(jsTouristsStorage[id].PASS_DATE_ISSUE);
                        $("#UF_PASS_ISSUED_BY").val(jsTouristsStorage[id].UF_PASS_ISSUED_BY);

                    }
                });

            });
        </script>    
        <?
    }

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }

    Utils::showEditForm(array(
        'action' => $APPLICATION->GetCurPageParam(),
        'name' => 'client_form',
        'id' => 'client_form',
        'tabs' => array(
            array(
                "DIV" => "PERSONAL_DATA",
                "TAB" => 'Личные данные',
                'CONTENT' => travelsoft\booking\crm\ClientUtils::getFieldsContent($arr_client)
            )
        ),
        'buttons' => array(
            array(
                'class' => 'adm-btn-save',
                'name' => 'SAVE',
                'value' => 'Сохранить'
            )
        )
    ));
} catch (Exception $e) {

    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");

