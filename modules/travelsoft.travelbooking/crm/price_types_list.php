<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\PriceTypes;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$sort = new CAdminSorting(Settings::PRICE_TYPES_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::PRICE_TYPES_HTML_TABLE_ID, $sort);

if ($arPriceTypesId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arPriceTypesId = array_keys(PriceTypes::get(array('select' => array('ID'))));
    }

    foreach ($arPriceTypesId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                PriceTypes::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::PRICE_TYPES_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = PriceTypes::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arPriceTypes = PriceTypes::get($getParams);

$dbResult = new CAdminResult($arPriceTypes, Settings::PRICE_TYPES_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_NAME",
        "content" => "Название",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CALC_TYPE",
        "content" => "Тип расчета",
        "sort" => "UF_CALC_TYPE",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_MIN_AGE",
        "content" => "Минимальный возраст",
        "sort" => "UF_MIN_AGE",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_MAX_AGE",
        "content" => "Максимальный возраст",
        "sort" => "UF_MAX_AGE",
        "align" => "center",
        "default" => true
    )
));

$arCalculationTypes = \travelsoft\booking\stores\CalculationTypes::get();
while ($arPriceType = $dbResult->Fetch()) {

    $row = &$list->AddRow($arPriceType["ID"], $arPriceType);
    
    if ($arPriceType["UF_CALC_TYPE"] > 0) {
        
        $row->AddViewField("UF_CALC_TYPE", $arCalculationTypes[$arPriceType["UF_CALC_TYPE"]]["UF_NAME"]);
    }
    
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect("travelsoft_crm_booking_price_types_edit.php?ID=" . $arPriceType["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить тип цены')) " . $list->ActionDoGroup($arPriceType["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать тип цены",
        'TITLE' => "Создание типа цены",
        'LINK' => 'travelsoft_crm_booking_price_types_edit.php?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список типов цен");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");


$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
