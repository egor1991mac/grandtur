<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Tourists;
use travelsoft\booking\crm\Utils;

require_once 'header.php';

?>

<style>
    img[src="/bitrix/js/main/core/images/hint.gif"] {
        display: none;
    }
</style>

<?

try {

    $ID = intVal($_REQUEST['ID']);

    $title = 'Добавление туриста';
    $editRequest = false;
    if ($ID > 0) {

        $arTourist = current(Tourists::get(array('filter' => array('ID' => $ID))));

        if (!$arTourist['ID']) {

            throw new Exception('Турист с ID="' . $ID . '" не найден');
        }

        $title = 'Редактирование туриста #' . $arTourist['ID'];
        $editRequest = true;
    }

    $APPLICATION->SetTitle($title);

    $arResult = \travelsoft\booking\crm\TouristsUtils::processingTouristEditForm();

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if (!empty($arResult['errors'])) {

        CAdminMessage::ShowMessage(array(
            "MESSAGE" => implode('<br>', $arResult['errors']),
            "TYPE" => "ERROR"
        ));
    }

    Utils::showEditForm(array(
    'action' => $APPLICATION->GetCurPageParam(),
    'name' => 'tourist_form',
    'id' => 'tourist_form',
    'tabs' => array(
        array(
            "DIV" => "MAIN_TOURIST_DATA",
            "TAB" => 'Основные данные по туристу',
            '__CONTENT' => \travelsoft\booking\crm\TouristsUtils::getTouristFieldsContent($arTourist, 'MAIN_TOURIST_DATA')
        )
    ),
    'buttons' => array(
        array(
            'class' => 'adm-btn-save',
            'name' => 'SAVE',
            'value' => 'Сохранить'
        ),
        array(
            'name' => 'CANCEL',
            'value' => 'Отменить'
        )
    )
));
    
} catch (Exception $e) {

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage(array('MESSAGE' => $e->getMessage(), 'TYPE' => 'ERROR'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

