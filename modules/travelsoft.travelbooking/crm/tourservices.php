<?

/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

/** @global CUser $USER */
use travelsoft\booking\stores\Tourservices;
use Bitrix\Main\Entity\ExpressionField;
use travelsoft\booking\crm\Settings;

require_once 'header.php';

$sort = new CAdminSorting(Settings::TOURSERVICES_HTML_TABLE_ID, "ID", "DESC");
$list = new CAdminList(Settings::TOURSERVICES_HTML_TABLE_ID, $sort);

if ($arTourservicesId = $list->GroupAction()) {

    if ($_REQUEST['action_target'] == 'selected') {

        $arTourservicesId = array_keys(Tourservices::get(array('select' => array('ID'))));
    }

    foreach ($arTourservicesId as $ID) {

        switch ($_REQUEST['action']) {

            case "delete":

                Tourservices::delete($ID);

                break;
        }
    }
}

if ($_REQUEST["by"]) {

    $by = $_REQUEST["by"];
}

if ($_REQUEST["order"]) {

    $order = $_REQUEST["order"];
}

$getParams = array("order" => array($by => $order));

$usePageNavigation = true;
$navParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        Settings::TOURSERVICES_HTML_TABLE_ID, array('nPageSize' => 20)
        ));

$navParams['PAGEN'] = (int) $navParams['PAGEN'];
$navParams['SIZEN'] = (int) $navParams['SIZEN'];

if ($usePageNavigation) {

    $totalCount = Tourservices::get(array('select' => array(new ExpressionField('CNT', 'COUNT(1)'))), false)->fetch();

    $totalCount = (int) $totalCount['CNT'];

    if ($totalCount > 0) {

        $totalPages = ceil($totalCount / $navParams['SIZEN']);
        if ($navParams['PAGEN'] > $totalPages) {

            $navParams['PAGEN'] = $totalPages;
        }
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = $navParams['SIZEN'] * ($navParams['PAGEN'] - 1);
    } else {

        $navParams['PAGEN'] = 1;
        $getParams['limit'] = $navParams['SIZEN'];
        $getParams['offset'] = 0;
    }
}

$arTourservices = Tourservices::get($getParams);

$dbResult = new CAdminResult($arTourservices, Settings::TOURSERVICES_HTML_TABLE_ID);

if ($usePageNavigation) {

    $dbResult->NavStart($getParams['limit'], $navParams['SHOW_ALL'], $navParams['PAGEN']);
    $dbResult->NavRecordCount = $totalCount;
    $dbResult->NavPageCount = $totalPages;
    $dbResult->NavPageNomer = $navParams['PAGEN'];
} else {

    $dbResult->NavStart();
}

$list->NavText($dbResult->GetNavPrint('Страницы'));

$list->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "sort" => "ID",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICE_TYPE",
        "content" => "Тип услуг",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_SERVICES",
        "content" => "Услуги",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_ADULTS_PRICE",
        "content" => "Стоимость для взрослого",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CHILDREN_PRICE",
        "content" => "Стоимость для ребенка",
        "align" => "center",
        "default" => true
    ),
    array(
        "id" => "UF_CITY",
        "content" => "Город выезда",
        "align" => "center",
        "default" => true
    )
));

$object = array();
$servicesTypes = \travelsoft\booking\Settings::getServicesTypes();
while ($arTourservice = $dbResult->Fetch()) {

    $row = &$list->AddRow($arTourservice["ID"], $arTourservice);
    
    if (!empty($arTourservice["UF_SERVICES"])) {
        $arServicesNames = array();
        
        if (!isset($object[$arTourservice["UF_SERVICE_TYPE"]])) {
            $object[$arTourservice["UF_SERVICE_TYPE"]] = current(travelsoft\booking\crm\Utils::getBookingServices(
                    array($arTourservice["UF_SERVICE_TYPE"])));
        }
 
        foreach ($arTourservice["UF_SERVICES"] as $service_id) {
            
            if (strlen($object[$arTourservice["UF_SERVICE_TYPE"]][$service_id]["NAME"]) > 0) {
                $arServicesNames[] = $object[$arTourservice["UF_SERVICE_TYPE"]][$service_id]["NAME"];
            } elseif (strlen($object[$arTourservice["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"]) > 0) {
                $arServicesNames[] = $object[$arTourservice["UF_SERVICE_TYPE"]][$service_id]["UF_NAME"];
            }
        }
        
        if (!empty($arServicesNames)) {
            $row->AddViewField("UF_SERVICES", implode(", ", $arServicesNames));
        }
    } else {
        $row->AddViewField("UF_SERVICES", "Все");
    }
    
    $children_price_display = [];
    if (!empty($arTourservice["UF_CHILDREN_PRICE"])) {
        
        
        foreach ($arTourservice["UF_CHILDREN_PRICE"] as $k => $v) {
            $children_price_display[] = "{$v} {$arTourservice["UF_CURRENCY"]} [{$arTourservice["UF_MIN_AGE"][$k]}-{$arTourservice["UF_MAX_AGE"][$k]} лет]";
        }
        
        
    }
    
    $row->AddViewField("UF_ADULTS_PRICE", "{$arTourservice["UF_ADULTS_PRICE"]} {$arTourservice["UF_CURRENCY"]}");
    
    $row->AddViewField("UF_CHILDREN_PRICE", implode("<br><br>", $children_price_display));
    
    $row->AddViewField("UF_SERVICE_TYPE", $servicesTypes[$arTourservice["UF_SERVICE_TYPE"]]["name"]);
    
    $row->AddViewField("UF_CITY", $arTourservice["UF_CITY"] ? \travelsoft\booking\stores\Cities::nameById(intval($arTourservice["UF_CITY"])) : '');
    
    $row->AddActions(array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => "Изменить",
            "ACTION" => $list->ActionRedirect("travelsoft_crm_booking_tourservice_edit.php?ID=" . $arTourservice["ID"])
        ),
        array(
            "ICON" => "delete",
            "DEFAULT" => true,
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Действительно хотите удалить туруслугу')) " . $list->ActionDoGroup($arTourservice["ID"], "delete")
        )
    ));
}

$list->AddFooter(array(
    array("title" => "Количество элементов", "value" => $dbResult->SelectedRowsCount()),
    array("counter" => true, "title" => "Количество выбранных элементов", "value" => 0)
));

$list->AddGroupActionTable(Array(
    "delete" => "Удалить"
));


$list->AddAdminContextMenu(array(array(
        'TEXT' => "Создать турулугу",
        'TITLE' => "Создание турулугу",
        'LINK' => 'travelsoft_crm_booking_tourservice_edit.php?lang=' . LANG,
        'ICON' => 'btn_new'
)));

$list->CheckListMode();

$APPLICATION->SetTitle("Список туруслуг");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
