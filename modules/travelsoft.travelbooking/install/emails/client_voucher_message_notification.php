<?php

return array(
    "NAME" => "Сообщение по путевке на сайте #SITE_NAME#",
    "SUBJECT" => "Сообщение по путевке на сайте #SITE_NAME#",
    "MESSAGE" => "Добавлено сообщение по путевке <a href='https://#SERVER_NAME#/personal/vouchers/detail.php?voucher_id=#VOUCHER_ID#&message-#MESSAGE_ID#'>https://#SERVER_NAME#/personal/vouchers/detail.php?voucher_id=#VOUCHER_ID#&message-#MESSAGE_ID#</a>"
    . "<br>---------------------------------------------------------------------<br>"
    . "Сообщение сгенерировано автоматически.",
    "OPTION_PARAMETER" => "CLIENT_VOUCHER_MESSAGE_NOTIFICATION_MAIL_ID",
    "OPTION_NAME" => "Письмо для уведомления о сообщении по путевке для клиента на сайте"
);
