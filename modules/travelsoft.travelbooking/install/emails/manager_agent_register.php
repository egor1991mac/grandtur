<?php

return array(
    "NAME" => "Регистрация агента на сайте #SITE_NAME#",
    "SUBJECT" => "Регистрация агента на сайте #SITE_NAME#",
    "MESSAGE" => "На сайте зарегистрирован <a href=\"https://#SERVER_NAME#/bitrix/admin/travelsoft_crm_booking_client_edit.php?ID=#ID#&lang=ru\" >агент</a> и находится в режиме ожидания активации."
    . "<br>---------------------------------------------------------------------<br>"
    . "Сообщение сгенерировано автоматически.",
    "OPTION_PARAMETER" => "MANAGER_AGENT_REGISTER_MAIL_ID",
    "OPTION_NAME" => "Письмо для регистрация агента на сайте для менеджеров"
);
