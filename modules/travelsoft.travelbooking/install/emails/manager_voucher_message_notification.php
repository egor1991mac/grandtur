<?php

return array(
    "NAME" => "Сообщение по путевке на сайте #SITE_NAME#",
    "SUBJECT" => "Сообщение по путевке на сайте #SITE_NAME#",
    "MESSAGE" => "Добавлено сообщение по путевке <a href='https://#SERVER_NAME#/bitrix/admin/travelsoft_crm_booking_voucher_edit.php?ID=#VOUCHER_ID#&lang=ru'>https://#SERVER_NAME#/bitrix/admin/travelsoft_crm_booking_voucher_edit.php?ID=#VOUCHER_ID#&lang=ru</a>"
    . "<br>---------------------------------------------------------------------<br>"
    . "Сообщение сгенерировано автоматически.",
    "OPTION_PARAMETER" => "MANAGER_VOUCHER_MESSAGE_NOTIFICATION_MAIL_ID",
    "OPTION_NAME" => "Письмо для уведомления о сообщении по путевке для менеджера на сайте"
);
