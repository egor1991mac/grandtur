<?php


return array(
    "table" => "ts_rooming",
    "table_data" => array(
        "NAME" => "TSROOMING",
        "ERR" => "Ошибка при создании highloadblock'a руминг",
        "LANGS" => array(
            "ru" => 'Таблица Руминг',
            "en" => "Rooming"
        ),
        "OPTION_PARAMETER" => "ROOMING_STORAGE_ID"
    ),
    "fields" => array(
        
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_TYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип объекта',
                'en' => 'Service type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип объекта',
                'en' => 'Service type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип объекта',
                'en' => 'Service type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип объекта" ',
                'en' => 'An error in completing the field "Service type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Объект',
                'en' => 'Services',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Объект',
                'en' => 'Services',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Объект',
                'en' => 'Services',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Объект" ',
                'en' => 'An error in completing the field "Services"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE",
            "USER_TYPE_ID" => 'date',
            "XML_ID" => "",
            "SORT" => 1000,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Дата',
                'en' => 'Date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Дата',
                'en' => 'Date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Даты',
                'en' => 'Date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Дата" ',
                'en' => 'An error in completing the field "Date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ROOM",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Категория номера',
                'en' => 'Room',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Категория номера',
                'en' => 'Room',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Категория номера',
                'en' => 'Room',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Категория номера" ',
                'en' => 'An error in completing the field "Room"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ROOM_NUMBER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Порядковый номер',
                'en' => 'Number',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Порядковый номер',
                'en' => 'Number',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Порядковый номер',
                'en' => 'Number',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Порядковый номер" ',
                'en' => 'An error in completing the field "Number"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TOURISTS",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Туристы',
                'en' => 'Tourists',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Туристы',
                'en' => 'Tourists',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Туристы',
                'en' => 'Tourists',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Туристы" ',
                'en' => 'An error in completing the field "Tourists"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        )
    )
);
