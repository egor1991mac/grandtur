<?php



return array(
    "table" => "ts_cashdesk",
    "table_data" => array(
        "NAME" => "TSCASHDESK",
        "ERR" => "Ошибка при создании highloadblock'a кассы",
        "LANGS" => array(
            "ru" => 'Таблица Кассы',
            "en" => "Cash desk"
        ),
        "OPTION_PARAMETER" => "CASH_DESK_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CREATER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Создатель',
                'en' => 'Creater',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Создатель" ',
                'en' => 'An error in completing the field "Creater"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
