<?php

return array(
    "table" => "ts_calculation_types",
    "table_data" => array(
        "NAME" => "TSCALCULATIONTYPES",
        "ERR" => "Ошибка при создании highloadblock'a типы расчета",
        "LANGS" => array(
            "ru" => 'Таблица Типы расчета',
            "en" => "Calculation types"
        ),
        "OPTION_PARAMETER" => "CALCULATION_TYPES_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название',
                'en' => 'Name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название" ',
                'en' => 'An error in completing the field "Name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_METHOD",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Метод расчета',
                'en' => 'Method of calculation',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Метод расчета" ',
                'en' => 'An error in completing the field "Method of calculation"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    ),
    "items" => array(
        array(
            "UF_NAME" => "Взрослый на основном месте",
            "UF_METHOD" => "adultOnMainPlace"
        ),
        array(
            "UF_NAME" => "Взрослый на дополнительном месте",
            "UF_METHOD" => "adultOnAddPlace"
        ),
        array(
            "UF_NAME" => "Ребенок на основном месте",
            "UF_METHOD" => "childrenOnMainPlace"
        ),
        array(
            "UF_NAME" => "Ребенок на дополнительном месте",
            "UF_METHOD" => "childrenOnAddPlace"
        ),
        array(
            "UF_NAME" => "Добавить к стоимости(проживание)",
            "UF_METHOD" => "addToCostPlacements"
        ),
        array(
            "UF_NAME" => "Добавить к стоимости(проезд)",
            "UF_METHOD" => "addToCostTransfer"
        ),
        array(
            "UF_NAME" => "Проезд взрослый",
            "UF_METHOD" => "adultTransfer"
        ),
        array(
            "UF_NAME" => "Проезд ребенок",
            "UF_METHOD" => "childrenTransfer"
        ),
        array(
            "UF_NAME" => "Ребенок без места",
            "UF_METHOD" => "childrenWithoutPlace"
        ),
        array(
            "UF_NAME" => "Одноместное размещение",
            "UF_METHOD" => "singleOccupancy",
        ),
        array(
            "UF_NAME" => "Экскурсия взрослый",
            "UF_METHOD" => "excursionAdult",
        ),
        array(
            "UF_NAME" => "Экскурсия - ребенок",
            "UF_METHOD" => "excursionChildren",
        ),
    )
);
