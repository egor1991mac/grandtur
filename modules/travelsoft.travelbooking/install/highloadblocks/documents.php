<?php



return array(
    "table" => "ts_documents",
    "table_data" => array(
        "NAME" => "TSDOCUMENTS",
        "ERR" => "Ошибка при создании highloadblock'a документы",
        "LANGS" => array(
            "ru" => 'Таблица Документов',
            "en" => "Documents"
        ),
        "OPTION_PARAMETER" => "DOCUMENTS_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Название документа',
                'en' => 'Document name',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Название документа',
                'en' => 'Document name',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Название документа',
                'en' => 'Document name',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Название документа" ',
                'en' => 'An error in completing the field "Document name"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TEMPLATE",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => '',
                'EXTENSTIONS' => "doc,docx,html"
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Шаблон документа(doc,docx,html)',
                'en' => 'Document template',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Шаблон документа(doc,docx,html)',
                'en' => 'Document template',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Шаблон документа(doc,docx,html)',
                'en' => 'Document template',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Шаблон документа" ',
                'en' => 'An error in completing the field "Document template"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CLASS",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Класс обработки',
                'en' => 'Class of proccessor',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Класс обработки',
                'en' => 'Class of proccessor',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Класс обработки',
                'en' => 'Class of proccessor',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Класс обработки" ',
                'en' => 'An error in completing the field "Class of proccessor"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
