<?php



return array(
    "table" => "ts_quotas",
    "table_data" => array(
        "NAME" => "TSQUOTAS",
        "ERR" => "Ошибка при создании highloadblock'a Квоты",
        "LANGS" => array(
            "ru" => 'Таблица Квоты',
            "en" => "Quotas"
        ),
        "OPTION_PARAMETER" => "QUOTAS_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATE",
            "USER_TYPE_ID" => 'date',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Даты',
                'en' => 'Date',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Даты',
                'en' => 'Date',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Даты',
                'en' => 'Date',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Даты" ',
                'en' => 'An error in completing the field "Date"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_STOP",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Stop sale',
                'en' => 'Stop sale',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Stop sale',
                'en' => 'Stop sale',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Stop sale',
                'en' => 'Stop sale',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Stop sale" ',
                'en' => 'An error in completing the field "Stop sale"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SOLD_NUMBER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Количество проданных',
                'en' => 'Sold number',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Количество проданных',
                'en' => 'Sold number',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Количество проданных',
                'en' => 'Sold number',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Количество проданных" ',
                'en' => 'An error in completing the field "Sold number"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_QUOTA",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Квота',
                'en' => 'Quota',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Квота',
                'en' => 'Quota',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Квота',
                'en' => 'Quota',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Квота" ',
                'en' => 'An error in completing the field "Quota"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Услуга',
                'en' => 'Service',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Услуга" ',
                'en' => 'An error in completing the field "Service"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SERVICE_TYPE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип услуги',
                'en' => 'Service type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип услуги" ',
                'en' => 'An error in completing the field "Service type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
