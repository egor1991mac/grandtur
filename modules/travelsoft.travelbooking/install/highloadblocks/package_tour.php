<?php



return array(
    "table" => "ts_package_tour",
    "table_data" => array(
        "NAME" => "TSPACKAGETOUR",
        "ERR" => "Ошибка при создании highloadblock'a пакетный тур",
        "LANGS" => array(
            "ru" => 'Таблица Пакетный тур',
            "en" => "Package tour"
        ),
        "OPTION_PARAMETER" => "PACKAGE_TOUR_STORAGE_ID"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TOUR",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Описание тура',
                'en' => 'Tour description',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Описание тура',
                'en' => 'Tour description',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Описание тура',
                'en' => 'Tour description',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Описание тура" ',
                'en' => 'An error in completing the field "Tour description"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRANSFER",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Проезд',
                'en' => 'Transfer',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Проезд',
                'en' => 'Transfer',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Проезд',
                'en' => 'Transfer',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Проезд" ',
                'en' => 'An error in completing the field "Transfer"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRANS_RATE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тариф для проезда',
                'en' => 'Rate for transfer',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тариф для проезда',
                'en' => 'Rate for transfer',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тариф для проезда',
                'en' => 'Rate for transfer',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тариф для проезда" ',
                'en' => 'An error in completing the field "Rate for transfer"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRANSFER_BACK",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Проезд обратно',
                'en' => 'Transfer back',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Проезд обратно',
                'en' => 'Transfer back',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Проезд обратно',
                'en' => 'Transfer back',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Проезд обратно" ',
                'en' => 'An error in completing the field "Transfer back"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRANS_BACK_RATE",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тариф для проезда обратно',
                'en' => 'Rate for transferback',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тариф для проезда обратно',
                'en' => 'Rate for transferback',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тариф для проезда обратно',
                'en' => 'Rate for transferback',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тариф для проезда обратно" ',
                'en' => 'An error in completing the field "Rate for transferback"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_PLACEMENT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Проживание',
                'en' => 'Placement',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Проживание" ',
                'en' => 'An error in completing the field "Placement"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRAVEL_DAYS",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Количество ночей в пути',
                'en' => 'Travel days',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Количество ночей в пути',
                'en' => 'Travel days',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Количество ночей в пути',
                'en' => 'Travel days',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Количество ночей в пути" ',
                'en' => 'An error in completing the field "Travel days"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TRAVEL_DAYS_BACK",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Количество ночей в пути обратно',
                'en' => 'Travel days back',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Количество ночей в пути обратно',
                'en' => 'Travel days back',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Количество ночей в пути обратно',
                'en' => 'Travel days back',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Количество ночей в пути обратно" ',
                'en' => 'An error in completing the field "Travel days back"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
