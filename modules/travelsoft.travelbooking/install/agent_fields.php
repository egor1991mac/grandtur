<?php

return array(
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_LEGAL_NAME",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Юр. название" ',
            'en' => 'An error in completing the field "Legal name"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_LEGAL_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Юр. адрес" ',
            'en' => 'An error in completing the field "Legal address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_NAME",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Bank name',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Bank name',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Bank name',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Наименование банка" ',
            'en' => 'An error in completing the field "Bank name"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Bank address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Bank address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Bank address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Адрес банка" ',
            'en' => 'An error in completing the field "Bank address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_CODE",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Код банка" ',
            'en' => 'An error in completing the field "Bank code"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_CHECKING_ACCOUNT",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Расчетный счет',
            'en' => 'Checking account',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Расчетный счет',
            'en' => 'Checking account',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Расчетный счет',
            'en' => 'Checking account',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Расчетный счет" ',
            'en' => 'An error in completing the field "Checking account"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ACCOUNT_CURRENCY",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Валюта счета',
            'en' => 'account currency ',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Валюта счета',
            'en' => 'account currency ',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Валюта счета',
            'en' => 'account currency',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Валюта счета" ',
            'en' => 'An error in completing the field "account currency"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_UNP",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "УНП" ',
            'en' => 'An error in completing the field "UNP"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_OKPO",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "ОКПО" ',
            'en' => 'An error in completing the field "OKPO"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ACTUAL_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Фактический адрес" ',
            'en' => 'An error in completing the field "Actual address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BIK",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'БИК',
            'en' => 'BIK',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'БИК',
            'en' => 'BIK',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'БИК',
            'en' => 'BIK',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "БИК" ',
            'en' => 'An error in completing the field "BIK"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_AGENT_WAITING",
        "USER_TYPE_ID" => 'boolean',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "1",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Агент в ожидании активации',
            'en' => 'Agent waiting',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Агент в ожидании активации',
            'en' => 'Agent waiting',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Агент в ожидании активации',
            'en' => 'Agent waiting',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Агент в ожидании активации" ',
            'en' => 'An error in completing the field "Agent waiting"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
);
