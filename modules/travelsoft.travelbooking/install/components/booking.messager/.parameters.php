<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "VOUCHER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID путевки",
            "TYPE" => "LIST"
        )
    )
);
?>