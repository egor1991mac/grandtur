<?php

/* 
 * Ajax booking messager processor
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if (!$request->isAjaxRequest() || !\check_bitrix_sessid()) {
    
    echo travelsoft\booking\Utils::send404();
    
}


try {
    switch ($request->getPost('action')) {

        case "add-message":

            $message = \strip_tags(\trim((string) $request->getPost('message')));

            if (!strlen($message) || $request->getPost('voucher_id') <= 0) {
                throw new \Exception('Bad message or voucher id');
            }
            
            $data = [
                'user' => \travelsoft\booking\stores\Users::getById(travelsoft\booking\adapters\User::id()),
                'date' => date('d.m.Y H:i:s', time()),
                'message' => $message
            ];
            
            $message_id = \travelsoft\booking\stores\Messages::add([
                        'UF_MESSAGE' => $message,
                        'UF_USER_ID' => $data['user']['ID'],
                        'UF_DATE' => \travelsoft\booking\adapters\Date::createFromTimestamp(\strtotime($data['date'])),
                        'UF_VOUCHER_ID' => intVal($request->getPost('voucher_id'))
            ]);

            if ($message_id <= 0) {
                 throw new \Exception('Message can\'t be added');
            }
            
            $manager = travelsoft\booking\stores\Users::getById((int)travelsoft\booking\stores\Vouchers::getById(intVal($request->getPost('voucher_id')))['UF_MANAGER']);
            
            if (!empty($manager)) {
                \travelsoft\booking\adapters\Mail::sendManagerVoucherMessageNotification([
                    "#EMAIL_TO#" => $manager['EMAIL'],
                    "#VOUCHER_ID#" => intVal($request->getPost('voucher_id')),
                    "#MESSAGE_ID#" => $message_id
                ]);
            }
            travelsoft\booking\Utils::sendJsonResponse(\json_encode(['message_id' => $message_id, 'data' => $data]));
            
            break;
        case "get-messages":



            break;
    }
} catch (Exception $ex) {
    travelsoft\booking\Utils::sendJsonResponse(\json_encode(['error' => true, 'message' => $ex->getMessage()]));
}