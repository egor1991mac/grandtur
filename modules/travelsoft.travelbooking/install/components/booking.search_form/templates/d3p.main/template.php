<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);
?>
<div class="search-tabs search-tabs-bg search-tabs-to-top">
    <div class="tabbable">
        <ul class="nav nav-tabs" id="myTab">
            <? if ($arResult["SHOW_PACKAGE_TOUR_TAB"]): ?>
                <li <? if ($arResult["TAB_ACTIVE"] === "PACKAGE_TOUR"): ?>class="active"<? endif ?>><a href="#tab-1" data-toggle="tab"><i class="fa fa-building-o"></i> <span >Туры</span></a>
                </li>
            <? endif ?>
            <? if ($arResult["SHOW_PLACEMENTS_TAB"]): ?>
                <li <? if ($arResult["TAB_ACTIVE"] === "PLACEMENTS"): ?>class="active"<? endif ?>><a href="#tab-2" data-toggle="tab"><i class="fa fa-home"></i> <span ><?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB") ?></span></a>
                </li>
            <? endif ?>
            <? if ($arResult["SHOW_TRAVEL_TAB"] && $USER->IsAdmin()): ?>
                <li <? if ($arResult["TAB_ACTIVE"] === "TRAVEL"): ?>class="active"<? endif ?>><a href="#tab-3" data-toggle="tab"><i class="fa fa-car"></i> <span ><?= GetMessage("TRAVELBOOKING_TRAVEL_TAB")?></span></a>
                </li>
            <? endif ?>
        </ul>
        <div class="tab-content">
            <? if ($arResult["SHOW_PACKAGE_TOUR_TAB"]): ?>
                <div class="tab-pane fade <? if ($arResult["TAB_ACTIVE"] === "PACKAGE_TOUR"): ?>in active<? endif ?>" id="tab-1">
                    <!--                <h2>Поиск туров с проездом и проживанием</h2>
                                    <form>
                                        <input type="hidden" name="s2o" value="Y">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                    <label>Where are you going?</label>
                                                    <input class="typeahead form-control" placeholder="City, Airport, Point of Interest or U.S. Zip Code" type="text" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="input-daterange" data-date-format="M d, D">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                <label>Заезд с</label>
                                                                <input class="form-control" name="start" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                <label>по</label>
                                                                <input class="form-control" name="end" type="text" />
                                                            </div>
                                                        </div>
                    
                                                        <div class="col-md-3">
                                                            <div class="form-group form-group-lg form-group-select-plus">
                                                                <label>Туристов</label>
                                                                <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                    <label class="btn btn-primary active">
                                                                        <input type="radio" name="options" />1</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" />2</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" />3</label>
                                                                    <label class="btn btn-primary">
                                                                        <input type="radio" name="options" />3+</label>
                                                                </div>
                                                                <select class="form-control hidden">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option selected="selected">4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                    <option>13</option>
                                                                    <option>14</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary btn-lg" type="submit">Search for Hotels</button>
                                    </form>-->
                </div>
            <? endif ?>
            <? if ($arResult["SHOW_PLACEMENTS_TAB"]): ?>
                <div class="tab-pane fade <? if ($arResult["TAB_ACTIVE"] === "PLACEMENTS"): ?>in active<? endif ?>" id="tab-2">
                    <h2><?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB_TEXT") ?></h2>
                    <form action="<?= $arResult["PLACEMENTS_SEARCH_PAGE"] ?>" method="get">
                        <input type="hidden" name="s2o" value="Y">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-home input-icon"></i>
                                    <label><?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB_OBJECT_SEARCH") ?></label>
                                    <input data-ajax-url="/local/components/travelsoft/booking.search_form/ajax/search_placements.php" class="typeahead form-control" placeholder="<?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB_OBJECT_PLACEHOLDER") ?>" type="text" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label><?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB_DATE_FROM") ?></label>
                                                <input autocomplete="off" data-date-default="<?= $arResult["DATE_FROM"] ?>" class="input-datepicker form-control" name="travelbooking[date_from]" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label><?= GetMessage("TRAVELBOOKING_PLACEMENTS_TAB_DATE_TO") ?></label>
                                                <input autocomplete="off" data-date-default="<?= $arResult["DATE_TO"] ?>" class="input-datepicker form-control" name="travelbooking[date_to]" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-select-plus">
                                                <label><?= GetMessage("TRAVELBOOKING_TAB_ADULTS_FIELD") ?></label>

                                                <select name="travelbooking[adults]" class="form-control">
                                                    <option <? if ($arResult["ADULTS"] === 1): ?>selected<? endif ?> value="1">1</option>
                                                    <option <? if ($arResult["ADULTS"] === 2): ?>selected<? endif ?> value="2">2</option>
                                                    <option <? if ($arResult["ADULTS"] === 3): ?>selected<? endif ?> value="3">3</option>
                                                    <option <? if ($arResult["ADULTS"] === 4): ?>selected<? endif ?> value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-select-plus">
                                                <label><?= GetMessage("TRAVELBOOKING_TAB_CHILDREN_FIELD") ?></label>

                                                <select class="form-control" name="travelbooking[children]">
                                                    <option value="0"><?= GetMessage("TRAVELBOOKING_TAB_WITHOUT_CHILDREN_FIELD") ?></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                                <div class="children-age-box hidden"><div class="closer">&times;</div></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="loading hidden"></span>
                        <button class="search-btn btn btn-primary btn-lg" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN") ?></button>
                    </form>
                </div>
            <? endif ?>
            <? if ($arResult["SHOW_TRAVEL_TAB"] && $USER->IsAdmin()): ?>
                <div class="tab-pane fade <? if ($arResult["TAB_ACTIVE"] === "TRAVEL"): ?>in active<? endif ?>" id="tab-3">
                    <h2><?= GetMessage("TRAVELBOOKING_TRAVEL_TAB_TEXT") ?></h2>
                    <form id="travel-serach-form" action="<?= $arResult["TRAVEL_SEARCH_PAGE"] ?>" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                    <label><?= GetMessage('TRAVELBOOKING_TRAVEL_TAB_OBJECT_SEARCH') ?></label>
                                    <input data-error="<?= GetMessage("TRAVELBOOKING_TRAVEL_TAB_RATE_ERROR")?>" type="hidden" name="travelbooking[rates_id][]" value="" />
                                    <input type="hidden" name="travelbooking[service_type]" value="" />
                                    <input data-ajax-url="/local/components/travelsoft/booking.search_form/ajax/search_transfers_rates.php" class="typeahead form-control" placeholder="<?= GetMessage("TRAVELBOOKING_TRAVEL_TAB_WHERE")?>" type="text" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-daterange">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label><?= GetMessage("TRAVELBOOKING_TRAVEL_TAB_DATE_FROM"); ?></label>
                                                <input autocomplete="off" data-date-default="<?= $arResult["DATE_FROM"] ?>" class="input-datepicker form-control" name="travelbooking[date]" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group form-group-lg form-group-select-plus">
                                    <label><?= GetMessage("TRAVELBOOKING_TAB_ADULTS_FIELD") ?></label>

                                    <select name="travelbooking[adults]" class="form-control">
                                        <option <? if ($arResult["ADULTS"] === 1): ?>selected<? endif ?> value="1">1</option>
                                        <option <? if ($arResult["ADULTS"] === 2): ?>selected<? endif ?> value="2">2</option>
                                        <option <? if ($arResult["ADULTS"] === 3): ?>selected<? endif ?> value="3">3</option>
                                        <option <? if ($arResult["ADULTS"] === 4): ?>selected<? endif ?> value="4">4</option>
                                        <option <? if ($arResult["ADULTS"] === 5): ?>selected<? endif ?> value="5">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group form-group-lg form-group-select-plus">
                                    <label><?= GetMessage("TRAVELBOOKING_TAB_CHILDREN_FIELD") ?></label>

                                    <select class="form-control" name="travelbooking[children]">
                                        <option value="0"><?= GetMessage("TRAVELBOOKING_TAB_WITHOUT_CHILDREN_FIELD") ?></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                    <div class="children-age-box hidden"><div class="closer">&times;</div></div>
                                </div>

                            </div>
                            <div class="col-md-2 mt-27 text-center">
                                <span class="loading hidden"></span>
                                <button class="btn btn-primary btn-lg" id="travel-booking" type="submit"><?= GetMessage("TRAVELBOOKING_SEARCH_BTN")?></button>
                            </div>
                        </div>
                    </form>
                </div>
            <? endif ?>
        </div>
    </div>
</div>
<template id="children-age-template">
    <div class="select-age-box form-group">
        <label><?= GetMessage("TRAVELBOOKING_CHILDREN_AGE_TEXT")?> {{index}}</label>
        <select name="travelbooking[children_age][]" class="form-control">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <?/*<option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>*/?>
        </select>
    </div>
</template>