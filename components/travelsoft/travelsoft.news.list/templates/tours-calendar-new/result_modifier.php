<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult["ICONS"] = array();
$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>IBLOCK_TYPE_TOURS), false, false, Array("ID", "NAME", "PROPERTY_PICTURES"));
while($ob = $res->GetNext()){
    $arResult["ICONS"][$ob["ID"]] = array("NAME" => $ob["NAME"], "SRC" => CFile::GetPath($ob["PROPERTY_PICTURES_VALUE"]));
}

$arResult["PLACES"] = array();
$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>30), false, false, Array("ID", "NAME", "IBLOCK_ID"));
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $arResult["PLACES"][mb_strtolower($arProps["LETTER"]["VALUE"])] = array(
        "name" => $arFields["NAME"],
        "color" => $arProps["COLOR"]["VALUE_XML_ID"]
    );
}

$results["ITEMS"] = array();

/*array_walk($arResult["ITEMS"], function (&$arItem) {
    $timestamp = time();
    $curdate = null;
    foreach ($arItem["PROPERTIES"]["DEPARTURE"]["VALUE"] as $date) {
        $date_timestamp = MakeTimestamp($date);
        if ($date_timestamp > $timestamp) {
            $curdate = $date_timestamp;
            $arItem["CURDATE"] = date("d.m.Y", $curdate);
            $result["ITEMS"][] = $arItem;
        }
    }
});*/

foreach ($arResult["ITEMS"] as $key=>$arItem){
    $timestamp = time();
    $curdate = null;
    foreach ($arItem["PROPERTIES"]["DEPARTURE"]["VALUE"] as $k=>$date) {
        $date_timestamp = MakeTimestamp($date);
        $date_embassy = MakeTimestamp($arItem["PROPERTIES"]["DATE_TO_EMBASSY"]["VALUE"][$k]);
        $date_place = mb_strtolower($arItem["PROPERTIES"]["DEPARTURE"]["DESCRIPTION"][$k]);
        if ($date_timestamp > $timestamp) {
            $curdate = $date_timestamp;
            $arItem["CURDATE"] = date("d.m.Y", $curdate);
            $arItem["EMBASSY"] = !empty($date_embassy) ? date("d.m.Y", $date_embassy) : '';
            $arItem["PLACE"] = !empty($date_place) ? $date_place : '';
            $results["ITEMS"][] = $arItem;
        }
    }
}

usort($results["ITEMS"], function ($a, $b) {
    if (MakeTimestamp($a["CURDATE"]) == MakeTimestamp($b["CURDATE"])) {
        return 0;
    }
    return (MakeTimestamp($a["CURDATE"]) < MakeTimestamp($b["CURDATE"])) ? -1 : 1;
});

$result = array();
foreach ($results["ITEMS"] as $arItem) {
    if(!empty($arItem["CURDATE"])) {
        $month = date('m', strtotime($arItem["CURDATE"]));
        $result[$month][] = $arItem;
    }
}

/*
$today = time();
$mnth[date('m',$today)] = [];
for ($i = 1;$i<=12;$i++){
    $mnth[add_month($today,$i,false)] = [];
}
$result = array();
foreach ($arResult["ITEMS"] as $arItem) {
    if(!empty($arItem["CURDATE"])) {
        foreach ($arItem["CURDATE"] as $d) {
            $month = date('m', strtotime($d));

            $mnth[$month][$arItem["ID"]] = $arItem;
        }
    }
}
foreach ($mnth as $m=>$armnth){
    if(!empty($mnth[$m])){
        $result[$m] = $armnth;
    }
}*/

$arResult["ITEMS"] = $result;
