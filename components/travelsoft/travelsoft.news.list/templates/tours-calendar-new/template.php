<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule("travelsoft.currency");
?>
<?
$arMonth = [
    '01' => [
        'N' => 'Январь',
        'G' => 'Января',
    ],
    '02' => [
        'N' => 'Февраль',
        'G' => 'Февраля',
    ],
    '03' => [
        'N' => 'Март',
        'G' => 'Марта',
    ],
    '04' => [
        'N' => 'Апрель',
        'G' => 'Апреля',
    ],
    '05' => [
        'N' => 'Май',
        'G' => 'Мая',
    ],
    '06' => [
        'N' => 'Июнь',
        'G' => 'Июня',
    ],
    '07' => [
        'N' => 'Июль',
        'G' => 'Июля',
    ],
    '08' => [
        'N' => 'Август',
        'G' => 'Августа',
    ],
    '09' => [
        'N' => 'Сентябрь',
        'G' => 'Сентября',
    ],
    '10' => [
        'N' => 'Октябрь',
        'G' => 'Октября',
    ],
    '11' => [
        'N' => 'Ноябрь',
        'G' => 'Ноября',
    ],
    '12' => [
        'N' => 'Декабрь',
        'G' => 'Декабря',
    ],
];
?>

<? if (!empty($arResult["ITEMS"])): ?>

    <div class="ts-table display-768">

    <?/* if (!empty($arParams["TEXT_TITLE"])): */?><!--
        <div class="related tours">
            <h2><?/*= $arParams["TEXT_TITLE"] */?></h2>
    <?/* else: */?>
        <div class="site-main alignright">
    --><?/* endif */?>

        <div class="table-header">
            <div class="tr">
                <div class="td"><?=GetMessage('DATE_TOUR')?></div>
                <div class="td"><?=GetMessage('DAYS')?></div>
                <div class="td"><?=GetMessage('PROGRAM')?></div>
                <div class="td"><?=GetMessage('PRICE')?></div>
                <div class="td"><?=GetMessage('STATUS')?></div>
                <div class="td" ><?=GetMessage('DOC')?></div>
            </div>
        </div>

        <div class="table-body">
            <?foreach ($arResult["ITEMS"] as $month => $items):?>
                <?$cnt = 1;?>
                <div class="tr table-row-header">
                    <p><?= $arMonth[$month]['N'] ?></p>
                </div>
                <? foreach ($items as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    $img = '';
                    $date = [];
                    ?>

                    <? if (!empty($arItem["CURDATE"])): ?>
                            <?
                            $dateFrom = strtotime($arItem["CURDATE"]);
                            $dateTo = (strtotime($arItem["CURDATE"]) + $arItem["PROPERTIES"]["DAYS"]["VALUE"] * 86400);

                            $monthFrom = date('m', $dateFrom);
                            if($month == $monthFrom) {
                                $date = date('d.m', $dateFrom) . ' - ' . date('d.m', $dateTo);
                            }

                            //$date = date('d.m.Y', $dateFrom) . ' - ' . date('d.m.Y', $dateTo);

                            /*$monthFrom = date('m', $dateFrom);
                            $monthTo = date('m', $dateTo);
                            if(date('Y', $dateFrom) != date('Y', $dateTo)) {
                                $date = date('d.m.Y', $dateFrom) . ' - ' . date('d.m.Y', $dateTo);
                            } elseif($monthFrom != $monthTo) {
                                $date = date('d', $dateFrom) . '.' . $monthFrom . ' - ' . date('d', $dateTo) . '.' . $monthTo;
                            } elseif ($dateFrom != $dateTo) {
                                $date = date('d', $dateFrom) . ' - ' . date('d', $dateTo) . '.' . $monthTo;
                            } else {
                                $date = date('d', $dateFrom) . '.' . $monthTo;
                            }*/
                            ?>

                    <? endif ?>

                    <div class="tr<?if($cnt == 1 || $cnt % 2 > 0):?> color-light-gray<?endif?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="td"><?=$date?></div>
                        <div class="td<?if($cnt == 1 || $cnt % 2 > 0):?> color-dark-gray<?endif?> text-bold jc-center"><? if (!empty($arItem["PROPERTIES"]["DAYS"]["VALUE"])): ?>
                                <?= (int)strip_tags($arItem["PROPERTIES"]["DAYS"]["VALUE"]) +1 ?> <?=GetMessage('DAY')?>
                            <? endif; ?>
                        </div>
                        <div class="td text-blue text-bold"><a href="/tury/<?=$arItem["CODE"]?>/" class="title" target="_blank"><?= $arItem["NAME"] ?></a></div>
						<?/* деление на 2  так как базовая цена за 2 чел.*/?>
                        <div class="td<?if($cnt == 1 || $cnt % 2 > 0):?> color-dark-gray<?endif?> text-bold">
							<?/* условие для показа цен из системы бронирования if (isset($arParams["MIN_PRICES"][$arItem["ID"]]) && !empty($arParams["MIN_PRICES"][$arItem["ID"]])): ?>
                                <?$price_tour = \travelsoft\currency\factory\Converter::getInstance()->convert(
                                    $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_for_adults"] + $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_for_children"], $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_currency"], $arParams["MIN_PRICES"][$arItem["ID"]]["currency"]
                                )->getResult();?>
                                <?= \travelsoft\currency\factory\Converter::getInstance()->convert(
                                    ($arParams["MIN_PRICES"][$arItem["ID"]]["result_price"]-$price_tour)/2, $arParams["MIN_PRICES"][$arItem["ID"]]["currency"], "EUR"
							)->getResult(); */?>
                            <? if (!empty($arItem["PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["CURRENCY"]["VALUE"])): ?>
                                <?= \travelsoft\currency\factory\Converter::getInstance()->convert(
                                        $arItem["PROPERTIES"]["PRICE"]["VALUE"], $arItem["PROPERTIES"]["CURRENCY"]["VALUE"], "EUR"
                                    )->getResult();
                                    ?>
                            <? endif ?>
                        </div>
                        <div class="td <?=!empty($arItem["PLACE"]) ? 'seats-' . $arResult["PLACES"][$arItem["PLACE"]]["color"] : ''?>"><?= isset($arResult["PLACES"][$arItem["PLACE"]]["name"]) ? $arResult["PLACES"][$arItem["PLACE"]]["name"] : '' ?></div>
                        <div class="td"><?= $arItem["EMBASSY"] ?></div>
                    </div>
                    <?$cnt++?>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>
    <!--</div>-->

        </div>
    <div class="container-wrap  display-568">
        <div class="wrap">
            <div class="ts-table-horizontal horizontal position-absolute">
                <div class="table-header">
                    <div class="tr" style="max-width: 100px;">
						<div class="td"><?=GetMessage('DATE_TOUR')?></div>
						<div class="td"><?=GetMessage('DAYS')?></div>
						<div class="td"><?=GetMessage('PROGRAM')?></div>
						<div class="td"><?=GetMessage('PRICE')?></div>
						<div class="td"><?=GetMessage('STATUS')?></div>
						<div class="td" style="height:48px"><?=GetMessage('DOC')?></div>
                    </div>
                </div>
            </div>
             <?foreach ($arResult["ITEMS"] as $month => $items):?>
            <div class="ts-table-horizontal horizontal" >
                <?$cnt = 1;?>
                <div class="tr table-row-header">
                    <p><?= $arMonth[$month]['N'] ?></p>
                </div>
                <div class="table-body">
                <? foreach ($items as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    $img = '';
                    $date = [];
                    ?>

                    <? if (!empty($arItem["CURDATE"])): ?>
                            <?
                            $dateFrom = strtotime($arItem["CURDATE"]);
                            $dateTo = (strtotime($arItem["CURDATE"]) + $arItem["PROPERTIES"]["DAYS"]["VALUE"] * 86400);

                            $monthFrom = date('m', $dateFrom);
                            if($month == $monthFrom) {
                                $date = date('d.m', $dateFrom) . ' - ' . date('d.m', $dateTo);
                            }

                            //$date = date('d.m.Y', $dateFrom) . ' - ' . date('d.m.Y', $dateTo);

                            /*$monthFrom = date('m', $dateFrom);
                            $monthTo = date('m', $dateTo);
                            if(date('Y', $dateFrom) != date('Y', $dateTo)) {
                                $date = date('d.m.Y', $dateFrom) . ' - ' . date('d.m.Y', $dateTo);
                            } elseif($monthFrom != $monthTo) {
                                $date = date('d', $dateFrom) . '.' . $monthFrom . ' - ' . date('d', $dateTo) . '.' . $monthTo;
                            } elseif ($dateFrom != $dateTo) {
                                $date = date('d', $dateFrom) . ' - ' . date('d', $dateTo) . '.' . $monthTo;
                            } else {
                                $date = date('d', $dateFrom) . '.' . $monthTo;
                            }*/
                            ?>

                    <? endif ?>
                    <div class="tr<?if($cnt == 1 || $cnt % 2 > 0):?> color-light-gray<?endif?>">
                        <div class="td"><?=$date?></div>
                        <div class="td<?if($cnt == 1 || $cnt % 2 > 0):?> color-dark-gray<?endif?> text-bold"><? if (!empty($arItem["PROPERTIES"]["DAYS"]["VALUE"])): ?>
                                <?= (int)strip_tags($arItem["PROPERTIES"]["DAYS"]["VALUE"]) +1 ?> <?=GetMessage('DAY')?>
                            <? endif; ?></div>
                        <div class="td text-blue text-bold"><a href="/tury/<?=$arItem["CODE"]?>/" class="title" target="_blank"><?= $arItem["NAME"] ?></a></div>
                        <div class="td<?if($cnt == 1 || $cnt % 2 > 0):?> color-dark-gray<?endif?> text-bold"><? if (isset($arParams["MIN_PRICES"][$arItem["ID"]]) && !empty($arParams["MIN_PRICES"][$arItem["ID"]])): ?>
                                <?$price_tour = \travelsoft\currency\factory\Converter::getInstance()->convert(
                                    $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_for_adults"] + $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_for_children"], $arParams["MIN_PRICES"][$arItem["ID"]]["tourservice_currency"], $arParams["MIN_PRICES"][$arItem["ID"]]["currency"]
                                )->getResult();?>
                                <?= \travelsoft\currency\factory\Converter::getInstance()->convert(
                                    ($arParams["MIN_PRICES"][$arItem["ID"]]["result_price"]-$price_tour)/2, $arParams["MIN_PRICES"][$arItem["ID"]]["currency"], "EUR"
                                )->getResult();?>
                            <? elseif (!empty($arItem["PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["CURRENCY"]["VALUE"])): ?>
                                <?= \travelsoft\currency\factory\Converter::getInstance()->convert(
                                        $arItem["PROPERTIES"]["PRICE"]["VALUE"], $arItem["PROPERTIES"]["CURRENCY"]["VALUE"], "EUR"
                                    )->getResult();
                                    ?>
                            <? endif ?></div>
                        <div class="td <?=!empty($arItem["PLACE"]) ? 'seats-' . $arResult["PLACES"][$arItem["PLACE"]]["color"] : ''?>"><?= isset($arResult["PLACES"][$arItem["PLACE"]]["name"]) ? $arResult["PLACES"][$arItem["PLACE"]]["name"] : '' ?></div>
                        <div class="td"><?= $arItem["EMBASSY"] ?></div>
                    </div>
                    <?$cnt++?>
                <? endforeach; ?>
                </div>
            </div>
			<? endforeach; ?>
        </div>
    </div>

<? endif ?>
<script>
    let widthTable = document.querySelectorAll('.ts-table-horizontal');
    let width=0;
    let widthTbody = document.querySelectorAll('.ts-table-horizontal .table-body');
    widthTable[1].style.marginLeft = '100px';
    [].slice.call(widthTbody).forEach(item=>{

        let tr = item.querySelectorAll('.tr').length;
         item.style.width = (120*tr)+'px';
    });
    [].slice.call(widthTable).forEach(item=>{
        width+= item.clientWidth;
    });

    document.querySelector('.container-wrap .wrap').style.width = width+100+'px';

</script>
