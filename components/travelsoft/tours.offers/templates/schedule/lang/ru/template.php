<?php
$MESS["ALL_COUNTRIES"] = "Все";
$MESS["LEGEND"] = "Легенда:";
$MESS["NO_PLACES"] = "мест нет";
$MESS["FEW_PLACES"] = "мест мало";
$MESS["PLACES_ARE"] = "места есть";
$MESS["NAME_TITLE"] = "Название";
$MESS["PRICE_TITLE"] = "Цена от";
$MESS["DATE_TITLE"] = "Даты";
$MESS["TOUR_TYPE"] = "Горящий тур";
$MESS["NEW"] = "Новинка";
$MESS["FREE"] = "Виза бесплатно";
$MESS["LEADER"] = "Лидер продаж";
$MESS["OFFER_DAY"] = "Предложение дня";
$MESS["SPECIAL_OFFER"] = "Специальное предложение";
$MESS["READ_MORE"] = "Подробнее о туре";