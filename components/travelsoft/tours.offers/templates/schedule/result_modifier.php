<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$_year = date("Y");

for ($i = 0; $i < 7; $i++) {
    $arResult["MONTH"][date('m', strtotime('01.' . date("m") . '.' . $_year . '+' . $i . ' month'))] = FormatDate("f Y", MakeTimeStamp(date('d.m.Y', strtotime('01.' . date("m") . '.' . $_year . '+' . $i . ' month'))));
}

if(!empty($arResult["OFFERS"])){

    foreach ($arResult["OFFERS"] as $key => $arOffer){

        if(!empty($arOffer["DATES"])) {
            $arResult["OFFERS"][$key]["DATES_"] = array();
            foreach ($arOffer["DATES"] as $date => $date_val) {
                $m = date("m",strtotime($date_val["date"]));
                if(!isset($arResult["OFFERS"][$key]["DATES_"][$m])){
                    $arResult["OFFERS"][$key]["DATES_"][$m] = array();
                }
                //$arResult["OFFERS"][$key]["DATES_"][$m][$date] = $date_val;
                $arResult["OFFERS"][$key]["DATES_"][$m][] = $date_val;
            }
        }

        $arResult["COUNTRIES"][$arOffer["COUNTRY"]]["OFFERS"][$key] = $arResult["OFFERS"][$key];

    }
}