<?php

/**
 * Компонент результатов поиска ценовых предложений
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftBookingOffers extends CBitrixComponent {

    /**
     * @var \travelsoft\booking\adapters\Cache
     */
    public $cache = null;

    /**
     * @return $this
     */
    public function prepareParameters() {

        Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

        $this->arResult["SERVICE_TYPE"] = (string) $this->arParams["SERVICE_TYPE"];
        $method = "make" . ucfirst($this->arResult["SERVICE_TYPE"]) . "SearchRequest";
        $this->arResult["REQUEST"] = travelsoft\booking\Utils::$method(
                        (array) $this->arParams["SERVICES_ID"]
        );
        
        $this->arResult["BOOKING_URL"] = $this->arParams["BOOKING_URL"];

        $cache_id = serialize($this->arResult["REQUEST"]) . $this->arResult["SERVICE_TYPE"];

        $this->cache = new travelsoft\booking\adapters\Cache($cache_id, "/travelsoft/booking/offers/" . $this->arParams["SERVICE_TYPE"], 600);

        return $this;
    }

    /**
     * @return array
     */
    public function getOffers() {

        $result = $this->cache->get();

        if (!isset($result["offers"]) || empty($result["offers"])) {

            $parameters = $this->arResult;
            $cache = $this->cache;
            $result = $cache->caching(function () use ($parameters, $cache) {

                $method = "get" . ucfirst($parameters["SERVICE_TYPE"]) . "Offers";

                $result["offers"] = $this->$method($parameters["REQUEST"]);

                $result["nearest_available"] = false;

                if (empty($result["offers"])) {
                    $result = array();
                }

                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::pricesStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::quotasStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::durationStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::ratesStoreId());
                return $result;
            });
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getNearestAvailableOffers() {

        $result = $this->cache->get();

        if (!isset($result["offers"]) || empty($result["offers"])) {

            $parameters = $this->arResult;

            $cache = $this->cache;

            $result = $cache->caching(function () use ($parameters, $cache) {

                $arOffers = array();

                $method = "get" . ucfirst($parameters["SERVICE_TYPE"]) . "Offers";

                if (!in_array($parameters["SERVICE_TYPE"], ["transfer", "transferback", "excursiontour"])) {
                    $current_duration = travelsoft\booking\Utils::getNightsDuration($parameters["REQUEST"]["date_from"], $parameters["REQUEST"]["date_to"]);
                    // ищем предложения на более позднюю дату
                    if (empty($arOffers = \findByDurations(0, 21, $current_duration, $parameters, $method, $this))) {
                        // ищем предложения на более раннюю дату
                        $arOffers = \findByDurations(-3, 0, $current_duration, $parameters, $method, $this);
                    }
                } elseif ($parameters["SERVICE_TYPE"] === "excursiontour") {
                    if (empty($arOffers = \findForExcursion(0, 21, $parameters, $method, $this))) {
                        // ищем предложения на более раннюю дату
                        $arOffers = \findForExcursion(-3, 0, $parameters, $method, $this);
                    }
                }

                if (!empty($arOffers)) {
                    $result["offers"] = $arOffers;
                    $result["nearest_available"] = true;
                }

                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::pricesStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::quotasStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::durationStoreId());
                $cache->setTagCache("higloadblock_" . \travelsoft\booking\Settings::ratesStoreId());

                return $result;
            });
        }

        return $result;
    }

    /**
     * component body
     */
    public function executeComponent() {

        try {

            $arr_result = $this->prepareParameters()->cache->get();
            $this->arResult["IS_AVAIL_BOOKING"] = true;
            if (travelsoft\booking\adapters\User::haveAgentGroup()) {
                $this->arResult["IS_AVAIL_BOOKING"] = travelsoft\booking\adapters\User::isAgent();
            }
            $this->arResult["OFFERS"] = $arr_result["offers"];
            $this->arResult["NEAREST_AVAILABLE"] = $arr_result["nearest_available"];
            
            $this->IncludeComponentTemplate();
            CJSCore::Init();
        } catch (\Exception $e) {
            (new \travelsoft\booking\Logger)->write("Component booking.offers: " . $e->getMessage());
            ShowError("System error.");
        }
    }

    /**
     * @param array $request
     * @return array
     */
    public function getPlacementsOffers(array $request) {

        $arOffers = array();

        $converter = new travelsoft\booking\adapters\CurrencyConverter();

        $isAgent = \travelsoft\booking\adapters\User::isAgent();

        $arCalculationResult = travelsoft\booking\Utils::getCalculationResult("placements", $request);

        if ($arCalculationResult) {
            $arRoom = $arRate = $arFood = array();
            foreach ($arCalculationResult as $placement_id => $arr_data_grouped_by_rooms) {

                foreach ($arr_data_grouped_by_rooms as $room_id => $arr_data_grouped_by_rate) {

                    $arRoom = $this->_getRoom($room_id);
                    uasort($arr_data_grouped_by_rate, function ($a, $b) {
                        return $a["result_price"] > $b["result_price"];
                    });
                    foreach ($arr_data_grouped_by_rate as $rate_id => $arr_data) {

                        $arRate = $this->_getRate($rate_id);

                        $arFood = $this->_getFood((int) $arRate["UF_FOOD"]);
$tmp_price = $tourservice_price = 0.00;
                        /* if ($isAgent && $arr_data["discount_price"]) {
                          $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                          $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                          $formatted_price_usd = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");
                          } else {
                          $formatted_price = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"]);
                          $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                          } */
                        if ($isAgent && $arr_data["discount_price"]) {
                            $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        } else {
                            $tmp_price = $arr_data["result_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        }
                        if ($tourservice_price > 0) {
                            $tmp_price -= $converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                        $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                        $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                        $offer_price_tourservice = $tourservice_price > 0 ? $converter->convertWithFormatting($tourservice_price, $arr_data["tourservice_currency"], "BYN") : 0;
                        $formatted_price_special_currency = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");

                        $arOffers[] = array(
                            "ID" => $placement_id,
                            "RATE" => $arRate,
                            "ROOM_ID" => $room_id,
                            "PICTURE_PATH" => $arRoom["PICTURE_PATH"],
                            "PICTURES" => $arRoom["PICTURES"],
                            "DESCRIPTION" => $arRoom["DESCRIPTION"],
                            "NAME" => $arRoom["UF_NAME"],
                            "NIGHTS" => \travelsoft\booking\Utils::getNightsDuration($request["date_from"], $request["date_to"]),
                            "DATE_FROM" => $request["date_from"],
                            "DATE_TO" => $request["date_to"],
                            "DATE_FROM_PLACEMENT" => $request["date_from"],
                            "DATE_TO_PLACEMENT" => $request["date_to"],
                            "FOOD" => $arFood,
                            "FORMATTED_PRICE" => $formatted_price,
                            "FORMATTED_PRICE_USD" => $formatted_price_usd,
                            "FORMATTED_PRICE_TOURPRODUCT" => $formatted_price_special_currency,
                            "FORMATTED_PRICE_TOURSERVICE" => $offer_price_tourservice,
                            "ADD2BASKET" => travelsoft\booking\Utils::base64encode(
                                    array(
                                        "adults" => $request["adults"],
                                        "children" => $request["children"],
                                        "date_from" => $request["date_from"],
                                        "date_to" => $request["date_to"],
                                        "date_from_placement" => $request["date_from"],
                                        "date_to_placement" => $request["date_to"],
                                        "children_age" => $request["children_age"],
                                        "service_type" => "placements",
                                        "placement_rate_id" => $rate_id,
                                        "service_id" => $placement_id,
                                        "room_id" => $room_id,
                                        "food_id" => $arFood["ID"],
                                        "food_name" => $arFood["NAME"]
                                    )
                            )
                        );
                    }
                }
            }
        }

        return $arOffers;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getPackagetourOffers(array $request) {

        $arOffers = array();

        $converter = new travelsoft\booking\adapters\CurrencyConverter();

        $isAgent = \travelsoft\booking\adapters\User::isAgent();

        $arCalculationResult = travelsoft\booking\Utils::getCalculationResult("packagetour", $request);

        if ($arCalculationResult) {
            $arRoom = $arRate = $arFood = $arr_package_tours_by_transferback = array();
            foreach ($arCalculationResult as $package_id => $arr_grouped_data) {
                uasort($arr_grouped_data, function ($a, $b) {
                    return $a["result_price"] > $b["result_price"];
                });
                foreach ($arr_grouped_data as $arr_data) {

                    $arRoom = $this->_getRoom($arr_data["room_id"]);

                    $arRate = $this->_getRate($arr_data["placement_rate_id"]);

                    $arFood = $this->_getFood((int) $arRate["UF_FOOD"]);

                    $arCity = NULL;
                    if ($arr_data["city_id"] > 0) {
                        $arCity = \travelsoft\booking\stores\Cities::getById($arr_data["city_id"], ["ID", "NAME"]);
                    }
$tmp_price = $tourservice_price = 0.00;
                    if ($isAgent && $arr_data["discount_price"]) {
                        $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                        $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                    } else {
                        $tmp_price = $arr_data["result_price"];
                        $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                    }
                    if ($tourservice_price > 0) {
                        $tmp_price -= $converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                    }
                    $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                    $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                    $offer_price_tourservice = $tourservice_price > 0 ? $converter->convertWithFormatting($tourservice_price, $arr_data["tourservice_currency"], "BYN") : 0;
                    $formatted_price_special_currency = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");


                    $arOffers[] = array(
                        "ID" => $package_id,
                        "CITY" => $arCity,
                        "RATE" => $arRate,
                        "ROOM_ID" => $arr_data["room_id"],
                        "NAME" => $arRoom["UF_NAME"],
                        "PICTURES" => $arRoom["PICTURES"],
                        "PICTURE_PATH" => $arRoom["PICTURE_PATH"],
                        "PICTURES" => $arRoom["PICTURES"],
                        "DESCRIPTION" => $arRoom["DESCRIPTION"],
                        "NIGHTS" => \travelsoft\booking\Utils::getNightsDuration($arr_data["date_from"], $arr_data["date_to"]),
                        "DATE_FROM" => $request["date_from"],
                        "DATE_TO" => $request["date_to"],
                        "DATE_FROM_PLACEMENT" => $arr_data["date_from"],
                        "DATE_TO_PLACEMENT" => $arr_data["date_to"],
                        "DATE_FROM_TRANSFER" => $arr_data["date_from_transfer"],
                        "DATE_BACK_TRANSFER" => date(travelsoft\booking\Settings::DATE_FORMAT, strtotime($arr_data["date_back_transfer"]) + (86400 * $arr_data["travel_days_back"])),
                        "FOOD" => $arFood,
                        "ALL_PRICE" => $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"]),
                        "FORMATTED_PRICE" => $formatted_price,
                        "FORMATTED_PRICE_USD" => $formatted_price_usd,
                        "FORMATTED_PRICE_TOURPRODUCT" => $formatted_price_special_currency,
                        "FORMATTED_PRICE_TOURSERVICE" => $offer_price_tourservice,
                        "ADD2BASKET" => travelsoft\booking\Utils::base64encode(
                                array(
                                    "adults" => $request["adults"],
                                    "children" => $request["children"],
                                    "date_from" => $request["date_from"],
                                    "date_to" => $request["date_to"],
                                    "date_from_placement" => $arr_data["date_from"],
                                    "date_to_placement" => $arr_data["date_to"],
                                    "date_from_transfer" => $arr_data["date_from_transfer"],
                                    "date_back_transfer" => $arr_data["date_back_transfer"],
                                    "children_age" => $request["children_age"],
                                    "service_type" => "packagetour",
                                    "placement_rate_id" => $arr_data["placement_rate_id"],
                                    "transfer_rate_id" => $arr_data["transfer_rate_id"],
                                    "transferback_rate_id" => $arr_data["transferback_rate_id"],
                                    "room_id" => $arr_data["room_id"],
                                    "transfer_id" => $arr_data["transfer_id"],
                                    "transferback_id" => $arr_data["transferback_id"],
                                    "placement_id" => $arr_data["placement_id"],
                                    "service_id" => $package_id,
                                    "food_id" => $arFood["ID"],
                                    "food_name" => $arFood["NAME"],
                                    "travel_days" => $arr_data["travel_days"],
                                    "travel_days_back" => $arr_data["travel_days_back"]
                                )
                        )
                    );
                }
            }
        }

        return $arOffers;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getTransferOffers(array $request) {

        $arOffers = array();

        $converter = new travelsoft\booking\adapters\CurrencyConverter();

        $isAgent = \travelsoft\booking\adapters\User::isAgent();

        $arCalculationResult = travelsoft\booking\Utils::getCalculationResult("transfer", $request);

        if ($arCalculationResult) {
            $arRate = array();
            foreach ($arCalculationResult as $transfer_id => $arr_data_grouped_by_rate) {
                foreach ($arr_data_grouped_by_rate as $_k => $arr_data_grouped_by_dates) {

                    $explode = explode("~", $_k);

                    $rate_id = $explode[0];
                    $city_id = intVal($explode[1]);

                    $arCity = NULL;
                    if ($city_id > 0) {
                        $arCity = \travelsoft\booking\stores\Cities::getById($city_id, ["ID", "NAME"]);
                    }

                    uasort($arr_data_grouped_by_dates, function ($a, $b) {
                        return $a["result_price"] > $b["result_price"];
                    });

                    foreach ($arr_data_grouped_by_dates as $date => $arr_data) {

                        $arRate = $this->_getRate($rate_id);

$tmp_price = $tourservice_price = 0.00;

                        if ($isAgent && $arr_data["discount_price"]) {
                            $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        } else {
                            $tmp_price = $arr_data["result_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        }
                        if ($tourservice_price > 0) {
                            $tmp_price -= $converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                        $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                        $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                        $offer_price_tourservice = $tourservice_price > 0 ? $converter->convertWithFormatting($tourservice_price, $arr_data["tourservice_currency"], "BYN") : 0;
                        $formatted_price_special_currency = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");


                        $arOffers[] = array(
                            "ID" => $transfer_id,
                            "CITY" => $arCity,
                            "RATE" => $arRate,
                            "NAME" => $arRate["UF_NAME"],
                            "PICTURES" => array(),
                            "DESCRIPTION" => "",
                            "DATE_FROM_TRANSFER" => $arr_data["date"],
                            "DATE_FROM" => $arr_data["date"],
                            "FORMATTED_PRICE" => $formatted_price,
                            "FORMATTED_PRICE_USD" => $formatted_price_usd,
                            "FORMATTED_PRICE_TOURPRODUCT" => $formatted_price_special_currency,
                            "FORMATTED_PRICE_TOURSERVICE" => $offer_price_tourservice,
                            "ADD2BASKET" => travelsoft\booking\Utils::base64encode(
                                    array(
                                        "adults" => $request["adults"],
                                        "children" => $request["children"],
                                        "date" => $arr_data["date"],
                                        "date_from_transfer" => $arr_data["date"],
                                        "date_from" => $arr_data["date"],
                                        "children_age" => $request["children_age"],
                                        "service_type" => "transfer",
                                        "rate_id" => $rate_id,
                                        "transfer_rate_id" => $rate_id,
                                        "service_id" => $transfer_id,
                                        "transfer_id" => $transfer_id
                                    )
                            )
                        );
                    }
                }
            }
        }

        return $arOffers;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getTransferbackOffers(array $request) {
        $arOffers = array();

        $converter = new travelsoft\booking\adapters\CurrencyConverter();

        $isAgent = \travelsoft\booking\adapters\User::isAgent();

        $arCalculationResult = travelsoft\booking\Utils::getCalculationResult("transferback", $request);

        if ($arCalculationResult) {
            $arRate = array();
            foreach ($arCalculationResult as $transferback_id => $arr_data_grouped_by_rate) {

                foreach ($arr_data_grouped_by_rate as $_k => $arr_data_grouped_by_dates) {
                    
                    $explode = explode("~", $_k);

                    $rate_id = $explode[0];
                    $city_id = intVal($explode[1]);

                    $arCity = NULL;
                    if ($city_id > 0) {
                        $arCity = \travelsoft\booking\stores\Cities::getById($city_id, ["ID", "NAME"]);
                    }
                    
                    uasort($arr_data_grouped_by_dates, function ($a, $b) {
                        return $a["result_price"] > $b["result_price"];
                    });

                    foreach ($arr_data_grouped_by_dates as $date => $arr_data) {

                        $arRate = $this->_getRate($rate_id);
$tmp_price = $tourservice_price = 0.00;
                        if ($isAgent && $arr_data["discount_price"]) {
                            $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        } else {
                            $tmp_price = $arr_data["result_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        }
                        if ($tourservice_price > 0) {
                            $tmp_price -= $converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                        $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                        $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                        $offer_price_tourservice = $tourservice_price > 0 ? $converter->convertWithFormatting($tourservice_price, $arr_data["tourservice_currency"], "BYN") : 0;
                        $formatted_price_special_currency = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");


                        $arOffers[] = array(
                            "ID" => $transferback_id,
                            "CITY" => $arCity,
                            "RATE" => $arRate,
                            "NAME" => $arRate["UF_NAME"],
                            "PICTURES" => array(),
                            "DESCRIPTION" => "",
                            "DATE_FROM_TRANSFER" => $arr_data["date"],
                            "FORMATTED_PRICE" => $formatted_price,
                            "FORMATTED_PRICE_USD" => $formatted_price_usd,
                            "FORMATTED_PRICE_TOURPRODUCT" => $formatted_price_special_currency,
                            "FORMATTED_PRICE_TOURSERVICE" => $offer_price_tourservice,
                            "ADD2BASKET" => travelsoft\booking\Utils::base64encode(
                                    array(
                                        "adults" => $request["adults"],
                                        "children" => $request["children"],
                                        "date" => $arr_data["date"],
                                        "date_back_transfer" => $arr_data["date"],
                                        "date_from" => $arr_data["date"],
                                        "children_age" => $request["children_age"],
                                        "service_type" => "transferback",
                                        "rate_id" => $rate_id,
                                        "transferback_rate_id" => $rate_id,
                                        "service_id" => $transferback_id,
                                        "transferback_id" => $transferback_id
                                    )
                            )
                        );
                    }
                }
            }
        }

        return $arOffers;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getExcursiontourOffers(array $request) {

        $arOffers = array();

        $converter = new travelsoft\booking\adapters\CurrencyConverter();

        $isAgent = \travelsoft\booking\adapters\User::isAgent();

        $arCalculationResult = travelsoft\booking\Utils::getCalculationResult("excursiontour", $request);

        if ($arCalculationResult) {
            $arRate = array();
            
            foreach ($arCalculationResult as $excursiontour_id => $arr_data_grouped_by_rate) {

                foreach ($arr_data_grouped_by_rate as $_k => $arr_data_grouped_by_dates) {
                    
                    $explode = explode("~", $_k);

                    $rate_id = $explode[0];
                    $city_id = intVal($explode[1]);

                    $arCity = NULL;
                    if ($city_id > 0) {
                        $arCity = \travelsoft\booking\stores\Cities::getById($city_id, ["ID", "NAME"]);
                    }
                    
                    uasort($arr_data_grouped_by_dates, function ($a, $b) {
                        return $a["result_price"] > $b["result_price"];
                    });

                    foreach ($arr_data_grouped_by_dates as $date => $arr_data) {

                        $arRate = $this->_getRate((int)$rate_id);

                        $arFood = $this->_getFood((int) $arRate["UF_FOOD"]);
$tmp_price = $tourservice_price = 0.00;
                        if ($isAgent && $arr_data["discount_price"]) {
                            $tmp_price = $arr_data["result_price"] + $arr_data["discount_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        } else {
                            $tmp_price = $arr_data["result_price"];
                            $tourservice_price = $arr_data["tourservice_for_adults"] + $arr_data["tourservice_for_children"];
                        }
                        
                        if ($tourservice_price > 0) {
                            $tmp_price -= $converter->convert($tourservice_price, $arr_data["tourservice_currency"], $arr_data["currency"]);
                        }
                        
                        $formatted_price = $converter->convertWithFormatting($tmp_price, $arr_data["currency"]);
                        
                        $formatted_price_usd = $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"], "USD");
                        $offer_price_tourservice = $tourservice_price > 0 ? $converter->convertWithFormatting($tourservice_price, $arr_data["tourservice_currency"], "BYN") : 0;
                        $formatted_price_special_currency = $converter->convertWithFormatting($tmp_price, $arr_data["currency"], "USD");

                        $arOffers[] = array(
                            "ID" => $excursiontour_id,
                            "CITY" => $arCity,
                            "RATE" => $arRate,
                            "NAME" => $arRate["UF_NAME"],
                            "PICTURES" => array(),
                            "DESCRIPTION" => "",
                            "FOOD" => $arFood,
                            "DATE_FROM" => $arr_data["date"],
                            "ALL_PRICE" => $converter->convertWithFormatting($arr_data["result_price"], $arr_data["currency"]),
                            "FORMATTED_PRICE" => $formatted_price,
                            "FORMATTED_PRICE_USD" => $formatted_price_usd,
                            "FORMATTED_PRICE_TOURPRODUCT" => $formatted_price_special_currency,
                            "FORMATTED_PRICE_TOURSERVICE" => $offer_price_tourservice,
                            "ADD2BASKET" => travelsoft\booking\Utils::base64encode(
                                    array(
                                        "adults" => $request["adults"],
                                        "children" => $request["children"],
                                        "date" => $arr_data["date"],
                                        "date_from" => $arr_data["date"],
                                        "children_age" => $request["children_age"],
                                        "service_type" => "excursiontour",
                                        "rate_id" => $rate_id,
                                        "city_id" => $city_id,
                                        "excursion_tour_rate_id" => $rate_id,
                                        "service_id" => $excursiontour_id
                                    )
                            )
                        );
                    }
                }
            }
        }

        return $arOffers;
    }

    /**
     * @param int $room_id
     */
    protected function _getRoom(int $room_id) {
        static $arr = array();
        if (!isset($arr[$room_id])) {
            $arr[$room_id] = current(travelsoft\booking\stores\Rooms::get(array(
                        "filter" => array("ID" => $room_id),
                        "select" => array("ID", "UF_NAME", "UF_PICTURES", "UF_DESCRIPTION")
            )));

            if (empty($arr[$room_id])) {
                $arr[$room_id] = array(
                    "ID" => null, "UF_NAME" => "",
                    "PICTURE_PATH" => "",
                    "DESCRIPTION" => "",
                    "PICTURES" => array()
                );
            } else {

                if (!empty($arr[$room_id]["UF_PICTURES"])) {
                    $arr_img = CFile::ResizeImageGet(current($arr[$room_id]["UF_PICTURES"]), array('width' => 300, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true);

                    if (isset($arr_img['src'])) {
                        $arr[$room_id]["PICTURE_PATH"] = $arr_img['src'];
                    }
                    foreach ($arr[$room_id]["UF_PICTURES"] as $img_id) {
                        $arr_img = CFile::ResizeImageGet($img_id, array('width' => 450, 'height' => 300), BX_RESIZE_IMAGE_EXACT, true);
                        $arr[$room_id]["PICTURES"][] = $arr_img["src"];
                    }
                } else {
                    $arr[$room_id]["PICTURE_PATH"] = "";
                    $arr[$room_id]["PICTURES"] = array();
                }

                if (strlen($arr[$room_id]["UF_DESCRIPTION"]) > 0) {
                    $arr[$room_id]["DESCRIPTION"] = $arr[$room_id]["UF_DESCRIPTION"];
                } else {
                    $arr[$room_id]["DESCRIPTION"] = "";
                }
            }
        }
        return $arr[$room_id];
    }

    /**
     * @param int $rate_id
     */
    protected function _getRate(int $rate_id) {
        static $arr = array();
        if (!isset($arr[$rate_id])) {
            $arr[$rate_id] = current(travelsoft\booking\stores\Rates::get(array(
                        "filter" => array("ID" => $rate_id),
                        "select" => array("ID", "UF_NAME", "UF_FOOD")
            )));
            if (empty($arr[$rate_id])) {
                $arr[$rate_id] = array("ID" => null, "UF_NAME" => null, "UF_FOOD" => null);
            }
        }
        return $arr[$rate_id];
    }

    /**
     * @param int $food_id
     */
    protected function _getFood(int $food_id) {
        static $arr = array();
        if (!isset($arr[$food_id])) {
            $arr[$food_id] = current(travelsoft\booking\stores\Food::get(array(
                        "filter" => array("ID" => $food_id),
                        "select" => array("ID", "NAME")
            )));
            if (empty($arr[$food_id])) {
                $arr[$food_id] = array("ID" => null, "NAME" => null);
            }
        }
        return $arr[$food_id];
    }

}

if (!function_exists("findByDurations")) {

    function findByDurations($d_from, $d_to, $current_duration, $parameters, $method, $component) {
        $arOffers = array();
        for ($d = $d_from; $d <= $d_to; $d++) {

            $timestamp_from = strtotime($parameters["REQUEST"]["date_from"]) + (86400 * $d);
            for ($n = $current_duration; $n <= ($current_duration + 5); $n++) {

                $parameters["REQUEST"]["date_from"] = date(
                        \travelsoft\booking\Settings::DATE_FORMAT, $timestamp_from);
                $parameters["REQUEST"]["date_to"] = date(travelsoft\booking\Settings::DATE_FORMAT, $timestamp_from + (86400 * $n));

                $arOffers = $component->$method($parameters["REQUEST"]);
                if (!empty($arOffers)) {
                    return $arOffers;
                }
            }
        }
        return $arOffers;
    }

}

if (!function_exists("findForExcursion")) {

    function findForExcursion($d_from, $d_to, $parameters, $method, $component) {
        $arOffers = array();
        for ($d = $d_from; $d <= $d_to; $d++) {

            $parameters["REQUEST"]["date"] = date(
                    \travelsoft\booking\Settings::DATE_FORMAT, strtotime($parameters["REQUEST"]["date"]) + (86400 * $d));

            $arOffers = $component->$method($parameters["REQUEST"]);
            if (!empty($arOffers)) {
                return $arOffers;
            }
        }
        return $arOffers;
    }

}

