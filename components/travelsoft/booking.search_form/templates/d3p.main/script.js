
/**
 * Component booking.search_form 
 * Template "d3p.main"
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */

$(document).ready(function () {
    console.log('hello - main');
    var childrenAgeTemplate = $("#children-age-template").html();
    
    var placeholder = "";
    
    $.fn.datepicker.dates.RU = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четвер", "Пятница", "Суббота"],
        daysShort: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
        daysMin: ["Вс.", "Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб."],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сен.", "Окт.", "Ноя.", "Дек."],
        today: "Сегодня",
        clear: "Очистить",
        format: "dd.mm.yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 1
    };
    
    $('.input-datepicker').each(function () {
        var $this = $(this);
        $this.datepicker({
            todayHighlight: true,
            format: 'dd.mm.yyyy',
            startDate: "today",
            autoclose: true,
            language: 'RU'
        });
        $this.datepicker("setDate", $this.data("date-default"));
        if ($this.attr("name") === "travelbooking[date_from]") {
            $this.on("changeDate", function () {
                var $date_to = $this.closest("form").find("input[name='travelbooking[date_to]']");
                $date_to.datepicker("setDate", $this.val());
                $date_to.datepicker("show");

            });
        } else if ($this.attr("name") === "travelbooking[date_to]") {
            $this.on("changeDate", function () {
                var $date_from = $this.closest("form").find("input[name='travelbooking[date_from]']");
                var arr_date_from = $date_from.val().split(".");
                var arr_date_to = $this.val().split(".");
                if ((new Date(arr_date_from[2], arr_date_from[1], arr_date_from[0])).getTime()
                        > (new Date(arr_date_to[2], arr_date_to[1], arr_date_to[0])).getTime()) {
                    $date_from.val($this.val());
                }

            });
        }
    });

    $('.typeahead').each(function () {
        var $this = $(this);
        $this.typeahead({
            hint: true,
            highlight: true,
            minLength: 0,
            limit: 999
        }, {
            source: function (q, cb) {
                
                return $.ajax({
                    dataType: 'json',
                    type: 'get',
                    url: $this.data("ajax-url") + '?q=' + q + '&sessid=' + BX.bitrix_sessid(),
                    cache: false,
                    success: function (data) {
                        var result = [];
                        $.each(data, function (index, val) {
                            result.push({
                                value: val.text,
                                id: val.id,
                                page: val.page,
                                type: val.type
                            });
                        });
                        
                        cb(result);
                    }
                });
            },
            templates: {
                empty: "<span style='padding:0 5px'>Ничего не найдено.</span>"
            }
        });
        $this.on('typeahead:selected', function (ev, suggestion) {
            if (suggestion.type === "placements") {
                $this.closest("form").attr("action", suggestion.page);
            } else if (suggestion.type === "travel" || suggestion.type === "travelback") {
                $this.closest("form").find("input[name='travelbooking[rates_id][]']").val(suggestion.id);
                $this.closest("form").find("input[name='travelbooking[service_type]']").val(suggestion.type);
            }
            
        });

    }).on("focusin", function () {
        placeholder = this.placeholder;
        this.placeholder = "";
    }).on("focusout", function () {
        this.placeholder = placeholder;
    });

    $("select[name='travelbooking[children]']").on("change", function () {

        var $this = $(this).closest("select");
        var children = $this.val();
        var childrenAgeBox = $this.closest("form").find(".children-age-box");
        childrenAgeBox.find(".select-age-box").each(function () {
            $(this).remove();
        });
        childrenAgeBox.addClass("hidden");
        if (children > 0) {
            for (var i = 1; i <= children; i++) {
                childrenAgeBox.append(childrenAgeTemplate.replace("{{index}}", i));
            }
            childrenAgeBox.removeClass("hidden");
        }

    });
    
    $(".closer").on("click", function () {
        $(this).parent().addClass("hidden");
    });
    
    $(".search-btn").on("click", function () {
        $(this).addClass("hidden").prev("span.loading").removeClass("hidden");
    });
    
    $("#travel-serach-form").on("submit", function (e) {
        
        var $rate_input = $("input[name='travelbooking[rates_id][]']");
        if (!$rate_input.val()) {
            alert($rate_input.data("error"));
            e.preventDefault();
            return;
        }
        $("#travel-booking").addClass("hidden").prev("span.loading").removeClass("hidden");
    });
    
});
