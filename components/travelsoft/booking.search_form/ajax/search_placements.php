<?php

/* 
 * Обработка запроса на поиск проживаний
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

header('Content-Type: application/json; charset=' . SITE_CHARSET);

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

$arPlacements = \travelsoft\booking\stores\Placements::get(array(
    "filter" => array("NAME" => "%" .$_GET["q"]. "%", "ACTIVE" => "Y"),
    "select" => array("NAME", "ID", "DETAIL_PAGE_URL")
));

$response = array();
if (!empty($arPlacements)) {
    foreach ($arPlacements as $arPlacement) {
        $response[] = array("type" => "placements", "text" => $arPlacement["NAME"], "id" => $arPlacement["ID"], "page" => $arPlacement["DETAIL_PAGE_URL"]);
    }
}

echo json_encode($response);
