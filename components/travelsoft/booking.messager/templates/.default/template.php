<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>

<div class="row">
    <div class="col-md-12">

        <h2 id="messages-container-title">Сообщения по путевке</h2>

        <div id="messages-container">

            <? foreach ($arResult['MESSAGES'] as $message): ?>
                <div class="message-container">
                    <div class="user-container"><b><i><?= $message['USER']['FULL_NAME_WITH_EMAIL'] ?> &nbsp;&nbsp;&nbsp; <?= $message['DATE'] ?></b></i></div>
                    <div id="message-<?= $message['ID'] ?>" class="message"><?= $message['MESSAGE'] ?></div>
                </div>
            <? endforeach; ?>
        </div>

        <div id="add-message-container">
            <textarea rows="10" name="message"></textarea><br>
            <button data-voucher-id="<?= $arParams['VOUCHER_ID'] ?>" data-sessid="<?= bitrix_sessid() ?>" data-ajax-url="<?= $templateFolder ?>/ajax.php" id="send-message" type="button" class="btn btn-primary">Отправить</button>
        </div>

    </div>
</div>