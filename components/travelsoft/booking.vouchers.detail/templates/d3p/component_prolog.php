<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$special_docs_for_special_agents = array(
    
    // user id => doc id
    
    // ck.by => Счет Капитал МТН
    185 => 6
);

$current_user_id = $GLOBALS["USER"]->GetID();
if (isset($special_docs_for_special_agents[$current_user_id])) {
    $this->arParams["DOCS_FOR_AGENTS"][] = $special_docs_for_special_agents[$current_user_id];
}