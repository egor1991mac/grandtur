<?php

/* 
 * Extantion for news.list component
 */


class  TravelsoftNewDetailComponent extends CBitrixComponent {
    
    public $dr;
    
    // include component_prolog.php
    public function includeComponentProlog()
    {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $this->dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $this->dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => $this->arParams["ELEMENT_CODE"], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
        if ($element = $res->GetNextElement()) {
            $this->arPrologData["ELEMENT"] = $element;
        }

        if(file_exists($file_path))
            require $file_path;
        else {

            $file_path = $this->dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if(file_exists($file_path))
                require $file_path;
            else {
                $file_path = $this->dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if(file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $this->dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if(file_exists($file_path))
                        require $file_path;
                }

            }
        }
    }
    
    public function executeComponent () {

        $this->includeComponentProlog();
        
        parent::executeComponent();
        
    }
    
}