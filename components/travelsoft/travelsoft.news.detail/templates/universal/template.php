<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('travelsoft.currency');
$converter = \travelsoft\currency\factory\Converter::getInstance();
$this->addExternalJS("http://maps.googleapis.com/maps/api/js?key=");
$this->addExternalJS(SITE_TEMPLATE_PATH . "/js/jquery-custom-google-map-lib.js");
$htmlMapID = 'map';
?>
<? $properties = $arResult["DISPLAY_PROPERTIES"] ?>
<? if (!empty($properties["DATE"]["DISPLAY_VALUE"])): ?>
    <? foreach ((array)$properties["DATE"]["DISPLAY_VALUE"] as $key => $item): ?>
        <? $properties["DATE"]["DISPLAY_VALUE"][$key] = CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($item, CSite::GetDateFormat())); ?>
    <? endforeach; ?>
<? endif; ?>
<div class="container">
    <div class="menu-widget with-switch mt-30 mb-30">
        <ul class="magic-line">
            <li class="current_item"><a href="#overview" class="scrollto">Описание</a></li>
            <? if (!empty($properties["NDAYS"]["DISPLAY_VALUE"])): ?>
                <li><a href="#programm" class="scrollto">Программа тура по дням</a></li>
            <? endif; ?>
            <? if (!empty($arResult["SERVICES"])): ?>
                <li><a href="#amenties" class="scrollto">Услуги в отеле</a></li>
            <? endif; ?>
            <? if (!empty($arResult["ROUTE_INFO"])): ?>
                <li><a href="#location" class="scrollto">Карта</a></li>
            <? endif; ?>
            <? if (!empty($properties["VIDEO"]["DISPLAY_VALUE"])): ?>
                <li><a href="#video" class="scrollto">Видео</a></li>
            <? endif; ?>
            <? if (!empty($arResult["TOWNS_IN_COUNTRY"])): ?>
                <li><a href="#towns" class="scrollto">Города</a></li>
            <? endif; ?>
            <? if ($arResult["IBLOCK_ID"] == "25"): ?>
                <li><a href="#staff" class="scrollto">Сотрудники</a></li>
            <? endif; ?>
            <li><a href="#reviews" class="scrollto">Отзывы</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <?= $arParams["SLIDER"] ?>
</div>
<? /* <div id="prices" class="container mb-50 mt-40">
        <div class="search-hotels room-search pattern">
            <div class="search-room-title">
                <h5>Выберите комнату для бронирования тура</h5>
            </div>
            <div class="tours-container">
                <div class="tours-box">
                    <div class="tours-search mb-20">
                        <form method="post" class="form search divider-skew">
                            <div class="search-wrap">
                                <input type="text" placeholder="вылет из Минска" class="form-control search-field"><i
                                        class="flaticon-suntour-map search-icon"></i>
                            </div>
                        </form>
                        <div class="tours-calendar divider-skew">
                            <input placeholder="Дата с" type="text" onfocus="(this.type='date')"
                                   onblur="(this.type='text')"
                                   class="calendar-default textbox-n"><i
                                    class="flaticon-suntour-calendar calendar-icon"></i>
                        </div>
                        <div class="tours-calendar divider-skew">
                            <input placeholder="Дата по" type="text" onfocus="(this.type='date')"
                                   onblur="(this.type='text')" class="calendar-default textbox-n"><i
                                    class="flaticon-suntour-calendar calendar-icon"></i>
                        </div>
                        <div class="selection-box divider-skew"><i class="flaticon-suntour-adult box-icon"></i>
                            <select>
                                <option>Взрослых</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="selection-box divider-skew"><i class="flaticon-suntour-children box-icon"></i>
                            <select>
                                <option>Детей</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="selection-box"><i class="flaticon-suntour-bed box-icon"></i>
                            <select>
                                <option>Ночей</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="button-search">Искать</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-table">
            <table class="table alt-2">
                <thead>
                <tr>
                    <th> Тип номера</th>
                    <th> Размещение</th>
                    <th> Опции</th>
                    <th> Сегодняшняя цена</th>
                    <th> Бронирование</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><img src="<?= SITE_TEMPLATE_PATH ?>/pic/190x130.jpg" data-at2x="pic/190x130@2x.jpg" alt>
                    </td>
                    <td>
                        <h6>Grand Hotel Wien</h6>
                        <p class="mb-0">2 взрослых</p>
                    </td>
                    <td>
                        <ul class="style-1">
                            <li> Особые условия, оплата при пребывании</li>
                            <li> Завтрак включен</li>
                            <li> Бесплатная парковка</li>
                        </ul>
                    </td>
                    <td class="room-price">2000 BYN</td>
                    <td><a href="#" class="cws-button alt gray">Бронировать</a></td>
                </tr>
                <tr>
                    <td><img src="<?= SITE_TEMPLATE_PATH ?>/pic/190x130.jpg" data-at2x="pic/190x130@2x.jpg" alt>
                    </td>
                    <td>
                        <h6>Grand Hotel Wien</h6>
                        <p class="mb-0">2 взрослых</p>
                    </td>
                    <td>
                        <ul class="style-1">
                            <li> Особые условия, оплата при пребывании</li>
                            <li> Завтрак включен</li>
                            <li> Бесплатная парковка</li>
                        </ul>
                    </td>
                    <td class="room-price">2000 BYN</td>
                    <td><a href="#" class="cws-button alt gray">Бронировать</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>*/ ?>
<? $propertiesList = Array(
    "Скидка" => $properties["DISCOUNT"]["DISPLAY_VALUE"],
    "Страна" => $properties["COUNTRY"]["DISPLAY_VALUE"],
    "Маршрут" => $properties["ROUTE"]["DISPLAY_VALUE"],
    "Туры" => $properties["TOUR"]["DISPLAY_VALUE"],
    "Город" => $properties["TOWN"]["DISPLAY_VALUE"],
    "Регион" => $properties["REGION"]["DISPLAY_VALUE"],
    "Город отправления" => $properties["DEPARTURE"]["DISPLAY_VALUE"],
    "Дата начала тура" => $properties["DATE"]["DISPLAY_VALUE"],
    "Тип" => strip_tags($properties["TYPE"]["DISPLAY_VALUE"]),
    "Тип транспорта" => strip_tags($properties["TRANSPORT"]["DISPLAY_VALUE"]),
    "Отель" => $properties["HOTEL"]["DISPLAY_VALUE"],
    "Количество ночей" => $properties["DAYS"]["DISPLAY_VALUE"],
    "Количество ночных переездов" => $properties["NIGHT_SHIFTS"]["DISPLAY_VALUE"],
    "Количество взрослых" => $properties["ADULTS"]["DISPLAY_VALUE"],
    "Количество детей" => $properties["CHILDREN"]["DISPLAY_VALUE"],
    "Питание" => strip_tags($properties["FOOD"]["DISPLAY_VALUE"]),
    "Год постройки" => $properties["BUILDING"]["DISPLAY_VALUE"],
    "Год последней реновации" => $properties["RENOVATION"]["DISPLAY_VALUE"],
    "Адрес" => $properties["ADDRESS"]["DISPLAY_VALUE"],
    "Адрес (на языке страны пребывания)" => $properties["ADDRESS_COUNTRY_LANGUAGE"]["DISPLAY_VALUE"],
    "Телефон" => $properties["PHONE"]["DISPLAY_VALUE"],
    "Факс" => $properties["FAX"]["DISPLAY_VALUE"],
    "Email" => $properties["EMAIL"]["DISPLAY_VALUE"],
    "Сайт" => $properties["HTTP"]["DISPLAY_VALUE"],
    "Площадь номера" => $properties["AREA_OF_THE_ROOM"]["DISPLAY_VALUE"],
    "Типы туров" => strip_tags($properties["TYPE_TOUR"]["DISPLAY_VALUE"]),
    "Длительность" => $properties["DURATION"]["DISPLAY_VALUE"],
    "Тип новости" => strip_tags($properties["TYPE_NEWS"]["DISPLAY_VALUE"]),
    "Имя" => $properties["NAME"]["DISPLAY_VALUE"]
); ?>
<div class="container mb-50">
    <div class="row">
        <div class="col-md-12">
            <h4 class="trans-uppercase mb-10"><?= $arResult["NAME"] ?>
                <? if (!empty($properties["CATEGORY"]["DISPLAY_VALUE"])): ?>
                    <?= star_rating($properties["CATEGORY"]["DISPLAY_VALUE"]); ?>
                <? endif; ?>
            </h4>
            <div class="cws_divider mb-30"></div>
            <? foreach ($propertiesList as $name => $property): ?>
                <? if (!empty($property)): ?>
                    <div style="display: flex">
                        <p style="flex: 0 1 30%"><?= $name ?></p>
                        <?= implode(", ", (array)$property) ?>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>
<? $propertiesTable = Array(
    "Описание" => $properties["DESC"]["DISPLAY_VALUE"],
    "Проживание" => $properties["HOTEL_DESCRIPTION"]["DISPLAY_VALUE"],
    "Стоимость тура" => $properties["PRICE_TOUR"]["DISPLAY_VALUE"],
    "В стоимость тура входит" => $properties["PRICE_INCLUDE"]["DISPLAY_VALUE"],
    "В стоимость тура не входит" => $properties["PRICE_NO_INCLUDE"]["DISPLAY_VALUE"],
    "Необходимые документы" => $properties["DOCUMENT"]["DISPLAY_VALUE"],
    "Примечание" => $properties["NOTE"]["DISPLAY_VALUE"],
    "Описание номеров" => $properties["DESCROOM"]["DISPLAY_VALUE"],
    "Описание питания" => $properties["DESCMEAL"]["DISPLAY_VALUE"],
    "Описание услуг" => $properties["DESCSERVICE"]["DISPLAY_VALUE"],
    "Спорт и отдых" => $properties["DESCSPORT"]["DISPLAY_VALUE"],
    "Оздоровление" => $properties["DESCSHEALTH"]["DISPLAY_VALUE"],
    "Услуги для детей" => $properties["DESCCHILD"]["DISPLAY_VALUE"],
    "Пляж" => $properties["DESCBEACH"]["DISPLAY_VALUE"],
    "Лечебная база" => $properties["MEDICAL_TREATMENT"]["DISPLAY_VALUE"],
    "Конференц-зал" => $properties["CONFERENCE_HALL"]["DISPLAY_VALUE"],
    "Условия отмены" => $properties["CANCELLATION"]["DISPLAY_VALUE"],
    "Дополнительная информация" => $properties["ADDINFORMATION"]["DISPLAY_VALUE"],
    "Рекомендация оператора" => $properties["RECOMMENDATIONS"]["DISPLAY_VALUE"],
    "Территория" => $properties["DESCTERRITORY"]["DISPLAY_VALUE"],
    "Правила" => $properties["DESCRULES"]["DISPLAY_VALUE"],
    "Время заселения" => $properties["OCCUPANCY"]["DISPLAY_VALUE"],
    "Время выселения" => $properties["EVICTION"]["DISPLAY_VALUE"],
    "Климат" => $properties["CLIMATE"]["DISPLAY_VALUE"],
    "География" => $properties["GEOGRAFY"]["DISPLAY_VALUE"],
    "Население" => $properties["POPULATION"]["DISPLAY_VALUE"],
    "Язык" => $properties["LANGUAGE"]["DISPLAY_VALUE"],
    "Кухня" => $properties["KITCHEN"]["DISPLAY_VALUE"],
    "Отзыв" => $properties["COMMENT"]["DISPLAY_VALUE"]
) ?>
<? foreach ($propertiesTable as $name => $property): ?>
    <? if (!empty($property)): ?>
        <div id="overview" class="container mb-50">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="trans-uppercase mb-10"><?= $name ?></h4>
                    <div class="cws_divider mb-30"></div>
                    <p><?= $property ?></p>
                </div>
            </div>
        </div>
    <? endif; ?>
<? endforeach; ?>
<? if (!empty($properties["NDAYS"]["DISPLAY_VALUE"])): ?>
    <div id="programm" class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Программа тура по дням</h4>
                <div class="cws_divider mb-10"></div>
            </div>
        </div>
        <div class="row mt-0 masonry">
            <div class="col-md-12">
                <div class="accordion">
                    <? foreach ($properties["NDAYS"]["DISPLAY_VALUE"] as $key => $nday): ?>
                        <div class="content-title active">
                        <span <? if ($key == 0): ?>class="active"<? endif; ?>>
                            <i class="active-icon"></i>
                            <?= ($key + 1) ?>
                            <? if (!empty($properties["NDAYS"]["DESCRIPTION"][$key])): ?>
                                <?= $properties["NDAYS"]["DESCRIPTION"][$key] ?>
                            <? endif; ?>
                        </span>
                        </div>
                        <div class="content" style="display: block;">
                            <?= $nday ?>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
<? if (!empty($properties["FILES"]["VALUE"])): ?>
    <div class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Файлы для скачивания</h4>
                <div class="cws_divider mb-30"></div>
                <? if (count($properties["FILES"]["VALUE"]) > 1): ?>
                    <? foreach ($properties["FILE_VALUE"] as $k => $item): ?>
                        <a href="<?= $item["SRC"] ?>">
                            <? if (!empty($properties["FILES"]["DESCRIPTION"][$k])): ?>
                                <?= $properties["FILES"]["DESCRIPTION"][$k] ?>
                            <? else: ?>
                                <?= $item["ORIGINAL_NAME"] ?>
                            <? endif; ?>
                        </a><br/>
                    <? endforeach; ?>
                <? else: ?>
                    <a href="<?= $properties["FILE_VALUE"]["SRC"] ?>">
                        <? if (!empty($properties["FILES"]["DESCRIPTION"]["0"])): ?>
                            <?= $properties["FILES"]["DESCRIPTION"]["0"] ?>
                        <? else: ?>
                            <?= $properties["FILES"]["FILE_VALUE"]["ORIGINAL_NAME"] ?>
                        <? endif; ?>
                    </a>
                <? endif; ?>
            </div>
        </div>
    </div>
<? endif; ?>
<? if (!empty($arResult["TOWNS_IN_COUNTRY"])): ?>
    <div id="towns" class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Города/курорты</h4>
                <div class="cws_divider mb-10"></div>
            </div>
        </div>
        <div class="row mt-0 masonry">
            <? foreach ($arResult["TOWNS_IN_COUNTRY_"] as $arItem): ?>
                <div class="col-sm-12 col-md-12 thm-padding">
                    <h3><?= $arItem["NAME"] ?></h3>
                </div>
                <div class="col-sm-6 col-md-3 thm-padding news-item">
                    <div class="grid-item-inner">
                        <div class="grid-img-thumb">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                <div class="img">
                                    <img class="img-responsive" src="<?= $arItem["PICTURES"] ?>"
                                         alt="<?= $arItem["NAME"] ?>"/>
                                </div>
                            </a>
                        </div>
                        <div class="grid-content">
                            <div class="grid-price text-right">
                                <div class="hotel-person">
                                    <div class="place-name">
                                        <?= $arItem["NAME"] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <? foreach ($arItem["ID_TOWNS"] as $city): ?>
                    <div class="col-sm-6 col-md-3 thm-padding news-item">
                        <div class="grid-item-inner">
                            <div class="grid-img-thumb">
                                <a href="<?= $arResult["TOWNS_IN_COUNTRY"][$city]["DETAIL_PAGE_URL"] ?>">
                                    <div class="img">
                                        <img class="img-responsive"
                                             src="<?= $arResult["TOWNS_IN_COUNTRY"][$city]["PICTURES"] ?>"
                                             alt="<?= $arResult["TOWNS_IN_COUNTRY"][$city]["NAME"] ?>"/>
                                    </div>
                                </a>
                            </div>
                            <div class="grid-content">
                                <div class="grid-price text-right">
                                    <div class="hotel-person">
                                        <div class="place-name">
                                            <?= $arResult["TOWNS_IN_COUNTRY"][$city]["NAME"] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endforeach; ?>
            <? if (!empty($arResult["OTHER_TOWNS_IN_COUNTRY_"])): ?>
                <? if (!empty($arResult["TOWNS_IN_COUNTRY_"])): ?>
                    <div class="col-sm-12 col-md-12 thm-padding">
                        <h3>Другие регионы</h3>
                    </div>
                <? endif ?>
                <? foreach ($arResult["OTHER_TOWNS_IN_COUNTRY_"] as $otCity): ?>
                    <div class="col-sm-6 col-md-3 thm-padding news-item">
                        <div class="grid-item-inner">
                            <div class="grid-img-thumb">
                                <a href="<?= $otCity["DETAIL_PAGE_URL"] ?>">
                                    <div class="img">
                                        <img class="img-responsive" src="<?= $otCity["PICTURES"] ?>"
                                             alt="<?= $otCity["NAME"] ?>"/>
                                    </div>
                                </a>
                            </div>
                            <div class="grid-content">
                                <div class="grid-price text-right">
                                    <div class="hotel-person">
                                        <div class="place-name">
                                            <?= $otCity["NAME"] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endif ?>
        </div>
    </div>
<? endif ?>
<? if (!empty($arResult["SERVICES"])): ?>
    <div id="amenties" class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Удобства</h4>
                <div class="cws_divider mb-10"></div>
            </div>
        </div>
        <div class="row mt-0 masonry">
            <div class="col-md-12 col-sm-12">
                <!--                <h6 class="trans-uppercase">Основные услуги</h6>-->
                <ul class="style-1">
                    <? foreach ($arResult["SERVICES"] as $service): ?>
                        <li class="service-detail" style="background: url(<?= $service["PICTURES"] ?>) left top no-repeat;"><?= $service["NAME"] ?></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<? endif; ?>
<? if (!empty($arResult["ROUTE_INFO"])): ?>
    <div id="location" class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Карта</h4>
                <div class="cws_divider mb-30"></div>
                <div class="map-wrapper">
                    <div id="<?= $htmlMapID ?>"></div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
<? if (!empty($properties["VIDEO"]["DISPLAY_VALUE"])): ?>
    <div id="video" class="container mb-50">
        <div class="row">
            <div class="col-md-12">
                <h4 class="trans-uppercase mb-10">Видео</h4>
                <div class="cws_divider mb-30"></div>
                <div class="thumb-wrap">
                    <iframe width="100%"
                            src="https://www.youtube.com/embed/<?= $properties["VIDEO"]["VALUE"] ?>?ecver=1"
                            frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>

<? if (!empty($arResult["ROUTE_INFO"])): ?>
    <script>
        (function (gm) {
            gm.createGoogleMap("<?=$htmlMapID?>", {
                center: gm.LatLng(<?=$arResult['ROUTE_INFO']["lat"]?>, <?=$arResult['ROUTE_INFO']["lng"]?>),
                zoom: <?=$arResult["MAP_SCALE"]?>})
                .createMarkersOptions([{
                    title: '<?=$arResult['ROUTE_INFO']["title"]?>',
                    lat: '<?= $arResult['ROUTE_INFO']["lat"]?>',
                    lng: '<?= $arResult['ROUTE_INFO']["lng"]?>',
                    infoWindow: "<?= $arResult['ROUTE_INFO']["infoWindow"]?>",
                    icon: "<?=MAP_MARKER_PATH?>"
                }])._addMarker(gm._markersOptions[0]);
        })(window.GoogleMapFunctionsContainer);
    </script>
<? endif; ?>

<? $this->SetViewTarget('price_detail'); ?>
<? if (!empty($properties["PRICE"]["VALUE"]) && !empty($properties["CURRENCY"]["VALUE"])): ?>
    <div class="breadright">
        <p>
        <span class="font-4">
            <?= $converter->convert($properties["PRICE"]["VALUE"], $properties["CURRENCY"]["VALUE"])->getResult(); ?>
        </span>
            / <?= $converter->convert($properties["PRICE"]["VALUE"], $properties["CURRENCY"]["VALUE"], "USD")->getResult(); ?>
            <? if (!empty($properties["PRICE_FOR"]["DISPLAY_VALUE"])): ?>
                <br><?= $properties["PRICE_FOR"]["DISPLAY_VALUE"] ?>
            <? endif; ?>
        </p>
    </div>
<? endif ?>
<? $this->EndViewTarget(); ?>
