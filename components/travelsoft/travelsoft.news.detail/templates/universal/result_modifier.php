<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$resTown = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"]);
$resCountry = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"]);
$resTownFrom = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"]);

if(is_array($arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"])){
    foreach ($arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"] as &$town){
        $town = strip_tags($town);
    }
}
else {
    $arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"]);
}
$arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"]);

if($arResCountry = $resCountry->GetNext()){
    if($arResTown = $resTown->GetNext()) {
        $arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"] = '<a href="/tourists/сountries-resorts-cities/'.$arResCountry["CODE"].'/'.$arResTown["CODE"].'/">'.$arResult["DISPLAY_PROPERTIES"]["TOWN"]["DISPLAY_VALUE"].'</a>';
    }
    if($arResTownFrom = $resTownFrom->GetNext()){
        $arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"] = '<a href="/tourists/сountries-resorts-cities/'.$arResCountry["CODE"].'/'.$arResTownFrom["CODE"].'/">'.$arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["DISPLAY_VALUE"].'</a>';
    }
}

if (!empty($arResult["PROPERTIES"]["MAP"]["VALUE"])) {
    $LATLNG = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
    $arResult['MAP_SCALE'] = $arResult["PROPERTIES"]["MAP_SCALE"]["VALUE"] ? $arResult["PROPERTIES"]["MAP_SCALE"]["VALUE"] : 13;
    $arResult['ROUTE_INFO'] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arResult['NAME'],
        "infoWindow" => "<div style='color:red'><b>" . $arResult['NAME'] . "</b></div>"
    );
}
$arCervices = array(); $iblick_id_cervice = null; $arResult["SERVICES"] = array();
if (!empty($arResult["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"])) {
    $resHotel = CIBlockElement::GetProperty($arResult["DISPLAY_PROPERTIES"]["HOTEL"]["LINK_IBLOCK_ID"], $arResult["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"], array("sort" => "asc"), Array("ACTIVE "=>"Y", "CODE" => "SERVICES"));
    while($obHotel = $resHotel->Fetch()){
        if(!empty($obHotel["VALUE"])){
            $iblick_id_cervice = $obHotel["LINK_IBLOCK_ID"];
            $arCervices[] = $obHotel["VALUE"];
        }
    }
}
if(!empty($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"])){
    $iblick_id_cervice = $arResult["DISPLAY_PROPERTIES"]["SERVICES"]["LINK_IBLOCK_ID"];
    foreach ($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"] as $item){
        $arCervices[] = $item;
    }
}
if(!empty($arCervices) && !empty($iblick_id_cervice)){
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $iblick_id_cervice, "ID" => $arCervices, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        $arImage = getSrc($arProps["PICTURES"]["VALUE"], Array('width' => 29, 'height' => 29), NO_PHOTO_PATH_29_29, 1);
        $arResult["SERVICES"][] = Array("NAME" => $arFields["NAME"], "PAID" => $arProps["PAID"]["VALUE"], "PICTURES" => $arImage["0"]);
    }
}
if ($arResult["IBLOCK_ID"] == 35) {
    $arTownInTown = Array();
    $arResult["TOWNS_IN_COUNTRY_"] = array();
    $arResult["OTHER_TOWNS_IN_COUNTRY_"] = array();
    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 30, "PROPERTY_COUNTRY" => $arResult["ID"], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE"));
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        $linkTown = $arResult["DETAIL_PAGE_URL"] . $arFields["CODE"] . "/";
        $arResult["ARRAY_TOWNS_ID"][] = $arFields["ID"];
        $arCurrentTown = Array();
        if (!empty($arProps["TOWN"]["VALUE"])) {
            foreach ($arProps["TOWN"]["VALUE"] as $item) {
                $arTownInTown[] = $item;
                $arCurrentTown[] = $item;
            }
        }

        $arImage = getSrc($arProps["PICTURES"]["VALUE"], array("width" => 626, "height" => 464), NO_PHOTO_PATH_626_464, 1);
        $arResult["TOWNS_IN_COUNTRY"][$arFields["ID"]] = Array("ID" => $arFields["ID"],
            "PICTURES" => $arImage["0"],
            "ID_TOWNS" => $arCurrentTown,
            "NAME" => $arFields["NAME"],
            "DETAIL_PAGE_URL" => $linkTown,
            "PREVIEW_TEXT" => substr2($arProps["PREVIEW_TEXT"]["~VALUE"]["TEXT"]));
        if(!empty($arCurrentTown)) {
            $arResult["TOWNS_IN_COUNTRY_"][$arFields["ID"]] = Array(
                "ID" => $arFields["ID"],
                "PICTURES" => $arImage["0"],
                "ID_TOWNS" => $arCurrentTown,
                "NAME" => $arFields["NAME"],
                "DETAIL_PAGE_URL" => $linkTown,
                "PREVIEW_TEXT" => substr2($arProps["PREVIEW_TEXT"]["~VALUE"]["TEXT"]));
        }
    }

    if(!empty($arResult["ARRAY_TOWNS_ID"])){
        $keys_cities = array_keys($arResult["TOWNS_IN_COUNTRY_"]);
        foreach ($arResult["ARRAY_TOWNS_ID"] as $twn){
            if(!in_array($twn, $keys_cities) && !in_array($twn, $arTownInTown)){
                $arResult["OTHER_TOWNS_IN_COUNTRY_"][$twn] = $arResult["TOWNS_IN_COUNTRY"][$twn];
            }
        }
    }

    if (!empty($arTownInTown)) {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 30, "ID" => $arTownInTown, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE"));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $linkTown = $arResult["DETAIL_PAGE_URL"] . $arFields["CODE"] . "/";
            $arResult["TOWNINTOWN"][$arFields["ID"]] = Array("ID" => $arFields["ID"], "NAME" => $arFields["NAME"], "LINK" => $linkTown);
        }
    }
}