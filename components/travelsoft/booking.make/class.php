<?php

Bitrix\Main\Loader::includeModule("travelsoft.travelbooking");

/**
 * Компонент бронирования туристических услуг
 *
 * @author dimabresky
 * @copyright (c) 2018, travelsoft
 */
class TravelsoftBookingMake extends CBitrixComponent {

    /**
     * @var \travelsoft\booking\Basket
     */
    public $basket = null;

    public function add2basket() {

        if (strlen($_GET["add2basket"])) {

            if (check_bitrix_sessid()) {
                $this->basket->clear(); // только один пакет услуг за бронь
                $this->basket->add(\travelsoft\booking\Utils::base64decode((string) $_GET["add2basket"]));
            }

            LocalRedirect($GLOBALS["APPLICATION"]->GetCurDir());
        }
    }

    /**
     * (ajax)
     * @param string $position
     * @return int
     */
    public function deleteBasketPosition(string $position) {
        $status = 0;
        if (!is_null($position)) {
            if ($this->basket->delete($position)) {
                $status = 1;
            }
        }
        return $status;
    }

    /**
     * @return $this
     */
    public function prepareBasketItems() {

        $this->arResult["BASKET"] = $arRooms = $arPlacements = array();
        foreach ($this->basket->get() as $id => $arr_input_fields) {

            $method = "_set" . ucfirst($arr_input_fields["service_type"]) . "BasketItems";
            if (method_exists($this, $method)) {
                $this->$method((string) $id, $arr_input_fields);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function initBasket() {
        $this->basket = new \travelsoft\booking\Basket;
        return $this;
    }

    /**
     * (ajax)
     * @return int
     */
    public function userAuthorization() {
        $response["status"] = 3;
        $email = (string) $_POST["email"];
        $arUser = $this->_getUserByEmail($email);
        if ($arUser["ID"] > 0 && $GLOBALS['USER']->Login($arUser["LOGIN"], $_POST["password"], "Y", "Y") === true) {
            $response["status"] = 2;
            $response["user_id"] = $GLOBALS["USER"]->GetID();
            $response["user_phone"] = travelsoft\booking\adapters\User::phone();
            $response["user_is_agent"] = travelsoft\booking\adapters\User::isAgent();
            $response["booking_is_avail"] = true;
            if (travelsoft\booking\adapters\User::haveAgentGroup()) {
                $response["booking_is_avail"] = $response["user_is_agent"]; // если агент активирован, то бронирование доступно
            }
        }
        return $response;
    }

    /**
     * (ajax)
     * @return array
     */
    public function userRegistration() {
        $arr_result = array("status" => 4, "message" => "");
        if (!preg_match(\travelsoft\booking\Settings::PHONE_REG, $_POST["phone"])) {
            $arr_result["status"] = 7;
        } elseif ($this->_getUserByEmail($_POST["email"])["ID"] > 0) {
            $arr_result["status"] = 5;
        } else {

            if (empty(ExtOptions::getLocaly("main", "captcha_registration"))) {
                ExtOptions::get('main', 'captcha_registration');
            }
            ExtOptions::changeLocaly("main", "captcha_registration", "N");
            $email = $_POST["email"];

            // хак для отмены вывода сообщения о неправильном логине
            if (!check_email($_POST["email"]) && (!strlen($_POST["password"]) || !strlen($_POST["change_password"]))) {
                $login = $email = randString(7) . "@fake";
            } else {
                $login = $email;
            }

            $result = $GLOBALS['USER']->Register($login, "", "", $_POST["password"], $_POST["confirm_password"], $email);
            if ($result["TYPE"] != "ERROR") {
                $arr_result["status"] = 6;
                $GLOBALS["USER"]->Update($GLOBALS["USER"]->GetID(), array("PERSONAL_PHONE" => $_POST["phone"]));
            }

            $arr_result["message"] = $result["MESSAGE"];
            $arr_result["user_id"] = $GLOBALS["USER"]->GetID();
            $arr_result["user_phone"] = travelsoft\booking\adapters\User::phone();
            $arr_result["user_is_agent"] = travelsoft\booking\adapters\User::isAgent();
            $arr_result["booking_is_avail"] = true;
            if (travelsoft\booking\adapters\User::haveAgentGroup()) {
                $arr_result["booking_is_avail"] = $arr_result["user_is_agent"]; // если агент активирован, то бронирование доступно
            }
        }
        return $arr_result;
    }

    /**
     * (ajax)
     * @return array
     */
    public function booking() {

        $arr_errors = array();
        if ($GLOBALS["USER"]->IsAuthorized()) {

            $arr_tourists = $_POST["tourists"];
            foreach ($arr_tourists as $k => $arr_tourist) {

                if (!empty($errors = $this->_touristValidation($arr_tourist))) {

                    $arr_errors["TOURISTS"][$k] = $errors;
                }
            }

            $is_buyer_check_ok = true;
            if (!travelsoft\booking\adapters\User::isAgent()) {
                $arr_errors["BUYER"] = $this->_buyerValidation($_POST['buyer']);
                $is_buyer_check_ok = empty($arr_errors["BUYER"]);
            }


            if (empty($arr_errors["TOURISTS"]) && $is_buyer_check_ok) {
                $this->arResult["CONVERTER"] = new travelsoft\booking\adapters\CurrencyConverter();
                if (!empty($this->initBasket()->prepareBasketItems()->arResult["BASKET"])) {

                    $arr_tourists_grouped_by_basket_items_id = array();
                    $bookings_pool_id = array();
                    foreach ($arr_tourists as $arr_tourist) {
                        $link2basket = $arr_tourist["LINK_TO_BASKET"];
                        unset($arr_tourist["LINK_TO_BASKET"]);
                        $arr_tourist["UF_USER_ID"] = $GLOBALS["USER"]->GetID();
                        if ($arr_tourist["ID"] > 0) {

                            $TID = $arr_tourist["ID"];
                            unset($arr_tourist["ID"]);
                            travelsoft\booking\stores\Tourists::update($TID, $arr_tourist);
                        } else {
                            $TID = travelsoft\booking\stores\Tourists::add($arr_tourist);
                        }

                        if ($TID > 0) {
                            foreach ($link2basket as $basket_item_id) {
                                $arr_tourists_grouped_by_basket_items_id[$basket_item_id][] = $TID;
                            }
                        }
                    }

                    if (travelsoft\booking\adapters\User::haveAgentGroup() && !travelsoft\booking\adapters\User::isAgent()) {

                        return array("errors" => array("BOOKING_NOT_AVAIL" => true), "result" => false, "booking_id" => null);
                    }

                    if (!travelsoft\booking\adapters\User::isAgent()) {
                        \travelsoft\booking\stores\Users::update($GLOBALS["USER"]->GetID(), array(
                            "NAME" => $_POST["buyer"]["NAME"],
                            "SECOND_NAME" => $_POST["buyer"]["SECOND_NAME"],
                            "LAST_NAME" => $_POST["buyer"]["LAST_NAME"],
                            "PERSONAL_PHONE" => $_POST["buyer"]["PERSONAL_PHONE"],
                            "PERSONAL_BIRTHDAY" => $_POST["buyer"]["PERSONAL_BIRTHDAY"],
                            "UF_PASS_ISSUED_BY" => $_POST["buyer"]["UF_PASS_ISSUED_BY"],
                            "UF_PASS_DATE_ISSUE" => $_POST["buyer"]["UF_PASS_DATE_ISSUE"],
                            "UF_PASS_SERIES" => $_POST["buyer"]["UF_PASS_SERIES"],
                            "UF_PASS_NUMBER" => $_POST["buyer"]["UF_PASS_NUMBER"]
                        ));
                    }

                    foreach ($this->arResult["BASKET"] as $arr_basket_item) {

                        if ($arr_basket_item["CAN_BUY"]) {

                            $arr_service_name = array();
                            if (strlen($arr_basket_item["TRANSFER_NAME"])) {
                                $arr_service_name[] = $arr_basket_item["TRANSFER_NAME"];
                            }
                            if (strlen($arr_basket_item["PLACEMENT_NAME"])) {
                                $arr_service_name[] = $arr_basket_item["ROOM_NAME"] . "[" . $arr_basket_item["PLACEMENT_NAME"] . "]";
                            }

                            $gross = $this->arResult["CONVERTER"]->convertWithFormatting($arr_basket_item["CALCULATIONS"]["gross"], $arr_basket_item["CALCULATIONS"]["currency"], travelsoft\booking\Settings::getVoucherCurrency());
                            $netto = $this->arResult["CONVERTER"]->convertWithFormatting($arr_basket_item["CALCULATIONS"]["netto"], $arr_basket_item["CALCULATIONS"]["currency"], travelsoft\booking\Settings::getVoucherCurrency());
                            $discount = 0.00;
                            if ($arr_basket_item["CALCULATIONS"]["discount_price"] > 0) {
                                $discount = $this->arResult["CONVERTER"]->convertWithFormatting($arr_basket_item["CALCULATIONS"]["discount_price"], $arr_basket_item["CALCULATIONS"]["currency"], travelsoft\booking\Settings::getVoucherCurrency());
                            }

                            $ID = \travelsoft\booking\stores\Bookings::add(array(
                                        "UF_SERVICE_TYPE" => $arr_basket_item["SOURCE"]["service_type"],
                                        "UF_SERVICE" => $arr_basket_item["SOURCE"]["service_id"],
                                        "UF_PLACEMENT" => @$arr_basket_item["SOURCE"]["placement_id"],
                                        "UF_ROOM" => @$arr_basket_item["SOURCE"]["room_id"],
                                        "UF_TRANSFER" => @$arr_basket_item["SOURCE"]["transfer_id"],
                                        "UF_TRANSFER_BACK" => @$arr_basket_item["SOURCE"]["transferback_id"],
                                        "UF_SERVICE_NAME" => implode(" + ", $arr_service_name),
                                        "UF_DATE_CREATE" => date(travelsoft\booking\Settings::DATE_FORMAT, time()),
                                        "UF_DATE_FROM" => @$arr_basket_item["SOURCE"]["date_from"],
                                        "UF_DATE_TO" => @$arr_basket_item["SOURCE"]["date_to"],
                                        "UF_DATE_FROM_TRANS" => @$arr_basket_item["SOURCE"]["date_from_transfer"],
                                        "UF_DATE_BACK_TRANS" => @$arr_basket_item["SOURCE"]["date_back_transfer"],
                                        "UF_DATE_FROM_PLACE" => @$arr_basket_item["SOURCE"]["date_from_placement"],
                                        "UF_DATE_TO_PLACE" => @$arr_basket_item["SOURCE"]["date_to_placement"],
                                        "UF_ADULTS" => $arr_basket_item["SOURCE"]["adults"],
                                        "UF_CHILDREN" => $arr_basket_item["SOURCE"]["children"],
                                        "UF_CHILDREN_AGE" => $arr_basket_item["SOURCE"]["children_age"],
                                        "UF_COST" => $gross,
                                        "UF_NETTO" => $netto,
                                        "UF_DISCOUNT" => $discount,
                                        "UF_CURRENCY" => travelsoft\booking\Settings::getVoucherCurrency(),
                                        "UF_MARKUP_PRICE" => $arr_basket_item["CALCULATIONS"]["markup_price"],
                                        "UF_MARKUP_CURRENCY" => $arr_basket_item["CALCULATIONS"]["markup_currency"],
                                        "UF_TS_PRICE" => $arr_basket_item["CALCULATIONS"]["tourservice_for_adults"] + $arr_basket_item["CALCULATIONS"]["tourservice_for_children"],
                                        "UF_TS_CURRENCY" => $arr_basket_item["CALCULATIONS"]["tourservice_currency"],
                                        "UF_TOURISTS" => $arr_tourists_grouped_by_basket_items_id[$arr_basket_item["BASKET_ITEM_ID"]],
                                        "UF_FOOD" => @$arr_basket_item["SOURCE"]["food_name"],
                                        "UF_EXCUR_TOUR_RATE" => @$arr_basket_item["SOURCE"]["excursion_tour_rate_id"],
                                        "UF_PLACEMENT_RATE" => @$arr_basket_item["SOURCE"]["placement_rate_id"],
                                        "UF_TRANS_RATE" => @$arr_basket_item["SOURCE"]["transfer_rate_id"],
                                        "UF_TRANS_BACK_RATE" => @$arr_basket_item["SOURCE"]["transferback_rate_id"],
                            ));

                            if ($ID > 0) {
                                $bookings_pool_id[] = $ID;
                            }
                        }
                    }
                }
                if (!empty($bookings_pool_id)) {
                    $voucher_id = \travelsoft\booking\stores\Vouchers::add(
                                    array(
                                        "UF_BOOKINGS" => $bookings_pool_id,
                                        "UF_CLIENT" => travelsoft\booking\adapters\User::id(),
                                        "UF_COMMENT" => strip_tags($_POST["comment"]),
                                        "UF_DATE_CREATE" => travelsoft\booking\adapters\Date::createFromTimestamp(time()),
                                        "UF_STATUS" => travelsoft\booking\Settings::defStatus(),
                                        "UF_MANAGER" => ""
                                    )
                    );
                    if ($voucher_id > 0) {
                        $this->basket->clear();
                        return array("errors" => null, "result" => true, "voucher_id" => $voucher_id);
                    }
                } else {
                    $arr_errors["NOT_AVAIL_BASKET_ITEMS"] = true;
                }
            }
        } else {
            $arr_errors["USER_NOT_AUTH"] = true;
        }

        return array("errors" => $arr_errors, "result" => false, "booking_id" => null);
    }

    /**
     * (ajax)
     * @param int $client_id
     * @param string $search_str
     * @return array
     */
    public function getTouristsByClientId(int $client_id, string $search_str) {
        $arr_tourists = array();
        $dbTourists = \travelsoft\booking\stores\Tourists::get(array(
                    "filter" => array(
                        "UF_USER_ID" => $client_id > 0 ? $client_id : -1,
                        array(
                            "LOGIC" => "OR",
                            array("UF_NAME" => "%" . $search_str . "%"),
                            array("UF_SECOND_NAME" => "%" . $search_str . "%"),
                            array("UF_LAST_NAME" => "%" . $search_str . "%"),
                        )
                    ),
                    "select" => array("ID", "UF_NAME", "UF_SECOND_NAME", "UF_LAST_NAME", "UF_LAT_NAME",
                        "UF_LAT_LAST_NAME", "UF_MALE", "UF_BIRTHDATE", "UF_PASS_SERIES", "UF_PASS_NUMBER",
                        "UF_PASS_ISSUED_BY", "UF_PASS_DATE_ISSUE")
                        ), false);


        while ($arr_tourist = $dbTourists->fetch()) {
            $arr_tourist["UF_BIRTHDATE"] = $arr_tourist["UF_BIRTHDATE"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
            $arr_tourist["UF_PASS_DATE_ISSUE"] = $arr_tourist["UF_PASS_DATE_ISSUE"]->format(\travelsoft\booking\Settings::DATE_FORMAT);
            $arr_tourists[] = $arr_tourist;
        }

        return $arr_tourists;
    }

    /**
     * component body
     */
    public function executeComponent() {

        try {

            $this->arResult["IS_AVAIL_BOOKING"] = true;
            $this->arResult["USER_IS_AGENT"] = \travelsoft\booking\adapters\User::isAgent();
            if (travelsoft\booking\adapters\User::haveAgentGroup()) {
                $this->arResult["IS_AVAIL_BOOKING"] = $this->arResult["USER_IS_AGENT"]; // если активированный агент, то бронирование доступно
            }

            $this->arResult["CONVERTER"] = new travelsoft\booking\adapters\CurrencyConverter();

            $this->initBasket();

            $this->add2basket();

            $this->arResult["USER_IS_AUTH"] = $GLOBALS["USER"]->IsAuthorized();
            $this->arResult["USER_ID"] = $GLOBALS["USER"]->GetID();
            $this->arResult["USER_FIELDS"] = array(
                "NAME" => "",
                "SECOND_NAME" => "",
                "LAST_NAME" => "",
                "PERSONAL_BIRTHDAY" => "",
                "PERSONAL_PHONE" => "",
                "UF_PASS_SERIES" => "",
                "UF_PASS_NUMBER" => "",
                "UF_PASS_DATE_ISSUE" => "",
                "UF_PASS_ISSUED_BY" => "",
            );
            if ($this->arResult["USER_ID"] > 0) {
                $this->arResult["USER_FIELDS"] = \travelsoft\booking\stores\Users::getById($this->arResult["USER_ID"], array(
                            "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_PHONE",
                            "UF_PASS_SERIES", "UF_PASS_NUMBER", "UF_PASS_DATE_ISSUE", "UF_PASS_ISSUED_BY"
                ));
            }

            $this->prepareBasketItems();

            CJSCore::Init();

            $this->IncludeComponentTemplate();
        } catch (\Exception $e) {
            dm($e->getMessage());die;
            (new \travelsoft\booking\Logger)->write("Component booking.make: " . $e->getMessage());
            ShowError("System error.");
        }
    }

    /**
     * @param array $fields
     * @return string
     */
    protected function _touristValidation(array $fields) {

        $errors = array();
        if (!strlen(trim($fields["UF_NAME"]))) {
            $errors["UF_NAME"] = "EMPTY";
        }
//        if (!strlen(trim($fields["UF_SECOND_NAME"]))) {
//            $errors["UF_SECOND_NAME"] = "EMPTY";
//        }
        if (!strlen(trim($fields["UF_LAST_NAME"]))) {
            $errors["UF_LAST_NAME"] = "EMPTY";
        }
//        if (!strlen(trim($fields["UF_LAT_NAME"]))) {
//            $errors["UF_LAT_NAME"] = "EMPTY";
//        } elseif (preg_match("/^[a-zA-Z]+$/", $fields["UF_LAT_NAME"]) !== 1) {
//            $errors["UF_LAT_NAME"] = "WRONG_LAT";
//        }
//        if (!strlen(trim($fields["UF_LAT_LAST_NAME"]))) {
//            $errors["UF_LAT_LAST_NAME"] = "EMPTY";
//        } elseif (preg_match("/^[a-zA-Z]+$/", $fields["UF_LAT_LAST_NAME"]) !== 1) {
//            $errors["UF_LAT_LAST_NAME"] = "WRONG_LAT";
//        }
//        if (!strlen(trim($fields["UF_MALE"]))) {
//            $errors["UF_MALE"] = "EMPTY_SELECT";
//        }
        if (!strlen(trim($fields["UF_PASS_SERIES"]))) {
            $errors["UF_PASS_SERIES"] = "EMPTY";
        } elseif (preg_match("/^[a-zA-Z]+$/", $fields["UF_PASS_SERIES"]) !== 1) {
            $errors["UF_PASS_SERIES"] = "WRONG_LAT";
        }
        if (!strlen(trim($fields["UF_PASS_NUMBER"]))) {
            $errors["UF_PASS_NUMBER"] = "EMPTY";
        } elseif (preg_match("/^[0-9]+$/", $fields["UF_PASS_NUMBER"]) !== 1) {
            $errors["UF_PASS_NUMBER"] = "WRONG_NUMBERS";
        }
        if (!strlen(trim($fields["UF_PASS_ISSUED_BY"]))) {
            $errors["UF_PASS_ISSUED_BY"] = "EMPTY";
        }
        if (!strlen(trim($fields["UF_BIRTHDATE"]))) {
            $errors["UF_BIRTHDATE"] = "EMPTY";
        }
//        if (!strlen(trim($fields["UF_PASS_DATE_ISSUE"]))) {
//            $errors["UF_PASS_DATE_ISSUE"] = "EMPTY";
//        }
        if (empty($fields["LINK_TO_BASKET"])) {
            $errors["LINK_TO_BASKET"] = "WRONG_LINK_TO_BASKET";
        }
        return $errors;
    }

    /**
     * @param array $fields
     * @return string
     */
    protected function _buyerValidation(array $fields) {
        $errors = array();
        if (!strlen(trim($fields["NAME"]))) {
            $errors["buyer[NAME]"] = "EMPTY";
        }
        if (!strlen(trim($fields["SECOND_NAME"]))) {
            $errors["buyer[SECOND_NAME]"] = "EMPTY";
        }
        if (!strlen(trim($fields["LAST_NAME"]))) {
            $errors["buyer[LAST_NAME]"] = "EMPTY";
        }
        if (!strlen(trim($fields["UF_PASS_SERIES"]))) {
            $errors["buyer[UF_PASS_SERIES]"] = "EMPTY";
        } elseif (preg_match("/^[a-zA-Z]+$/", $fields["UF_PASS_SERIES"]) !== 1) {
            $errors["buyer[UF_PASS_SERIES]"] = "WRONG_LAT";
        }
        if (!strlen(trim($fields["UF_PASS_NUMBER"]))) {
            $errors["buyer[UF_PASS_NUMBER]"] = "EMPTY";
        } elseif (preg_match("/^[0-9]+$/", $fields["UF_PASS_NUMBER"]) !== 1) {
            $errors["buyer[UF_PASS_NUMBER]"] = "WRONG_NUMBERS";
        }
        if (!strlen(trim($fields["UF_PASS_ISSUED_BY"]))) {
            $errors["buyer[UF_PASS_ISSUED_BY]"] = "EMPTY";
        }
        if (!strlen(trim($fields["PERSONAL_BIRTHDAY"]))) {
            $errors["buyer[PERSONAL_BIRTHDAY]"] = "EMPTY";
        }
        if (!strlen(trim($fields["UF_PASS_DATE_ISSUE"]))) {
            $errors["buyer[UF_PASS_DATE_ISSUE]"] = "EMPTY";
        }
        if (!preg_match(\travelsoft\booking\Settings::PHONE_REG, $fields["PERSONAL_PHONE"])) {
            $errors["buyer[PERSONAL_PHONE]"] = "WRONG_PHONE";
        }
        return $errors;
    }

    /**
     * @param string $email
     * @return array
     */
    protected function _getUserByEmail($email) {
        return CUser::GetList(($by = "ID"), ($order = "DESC"), array("EMAIL" => strlen($email) ? $email : -1))->Fetch();
    }

    /**
     * @param string $basket_item_id
     * @param array $arr_input_fields
     */
    protected function _setPackageTourBasketItems(string $basket_item_id, array $arr_input_fields) {

        $arRooms = $arPlacements = $arTransfer = array();

        // пересчет стоимости пакетного тура
        $calculation_result = current(\travelsoft\booking\Utils::getCalculationResult("packagetour", array(
                            "services_id" => array($arr_input_fields["service_id"]),
                            "rooms_id" => array($arr_input_fields["room_id"]),
                            "placements_id" => array($arr_input_fields["placement_id"]),
                            "transfers_id" => array($arr_input_fields["transfer_id"]),
                            "transfersback_id" => array($arr_input_fields["transferback_id"]),
                            "placements_rates_id" => array($arr_input_fields["placement_rate_id"]),
                            "transfers_rates_id" => array($arr_input_fields["transfer_rate_id"]),
                            "transfersback_rates_id" => array($arr_input_fields["transfer_rate_id"]),
                            "date_from" => $arr_input_fields["date_from"],
                            "date_to" => $arr_input_fields["date_to"],
                            "adults" => $arr_input_fields["adults"],
                            "children" => $arr_input_fields["children"],
                            "children_age" => $arr_input_fields["children_age"]
                )))[0];

        if (!isset($arRooms[$arr_input_fields["room_id"]])) {
            $arRooms[$arr_input_fields["room_id"]] = \travelsoft\booking\stores\Rooms::getById((int) $arr_input_fields["room_id"], array("ID", "UF_NAME", "UF_PICTURES"));
        }
        if (!isset($arPlacements[$arr_input_fields["placement_id"]])) {
            $arPlacements[$arr_input_fields["placement_id"]] = travelsoft\booking\stores\Placements::getById($arr_input_fields["placement_id"], array("ID", "NAME", "PROPERTY_PICTURES"));
        }
        if (!isset($arTransfer[$arr_input_fields["transfer_id"]])) {
            $arTransfer[$arr_input_fields["transfer_id"]] = \travelsoft\booking\stores\Buses::getById($arr_input_fields["transfer_id"], array("ID", "UF_NAME"));
        }
        $arr_basket_item = array(
            "BASKET_ITEM_ID" => $basket_item_id,
            "CALCULATIONS" => $calculation_result,
            "SOURCE" => $arr_input_fields,
            "TYPE" => "packagetour",
            "PICTURE" => null,
            "PLACEMENT_NAME" => $arPlacements[$arr_input_fields["placement_id"]]["~NAME"],
            "ROOM_NAME" => $arRooms[$arr_input_fields["room_id"]]["UF_NAME"],
            "TRANSFER" => true,
            "TRANSFER_NAME" => $arTransfer[$arr_input_fields["transfer_id"]]["~NAME"],
            "DATE_FROM" => $arr_input_fields["date_from"],
            "DATE_TO" => $arr_input_fields["date_to"],
            "DATE_FROM_PLACEMENT" => $arr_input_fields["date_from_placement"],
            "DATE_TO_PLACEMENT" => $arr_input_fields["date_to_placement"],
            "DATE_FROM_TRANSFER" => $arr_input_fields["date_from_transfer"],
            "DATE_BACK_TRANSFER" => date(travelsoft\booking\Settings::DATE_FORMAT, strtotime($arr_input_fields["date_back_transfer"]) + (86400 * $arr_input_fields["travel_days_back"])),
            "ADULTS" => $arr_input_fields["adults"],
            "CHILDREN" => $arr_input_fields["children"],
            "POSITION" => $basket_item_id,
            "FOOD" => $arr_input_fields["food_name"]
        );

        if ($calculation_result["result_price"] > 0) {

            if (\travelsoft\booking\adapters\User::isAgent() && $calculation_result["discount_price"] > 0) {
                $tmp_price = $calculation_result["result_price"] + $calculation_result["discount_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            } else {
                $tmp_price = $calculation_result["result_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            }

            $arr_basket_item["CAN_BUY"] = true;
            $arr_basket_item["PRICE_FORMATTED"] = $tmp_price_formatted;
            $arr_basket_item["PRICE"] = $tmp_price;
            $arr_basket_item["CURRENCY"] = $calculation_result["currency"];
        } else {
            $arr_basket_item["CAN_BUY"] = false;
            $arr_basket_item["PRICE_FORMATTED"] = "";
            $arr_basket_item["PRICE"] = 0.00;
            $arr_basket_item["CURRENCY"] = "";
        }

        if (!empty($arRooms[$arr_input_fields["room_id"]]["UF_PICTURES"])) {
            $arr_basket_item['PICTURE'] = CFile::ResizeImageGet($arRooms[$arr_input_fields["room_id"]]["UF_PICTURES"][0], array('width' => 250, 'height' => 250), BX_RESIZE_IMAGE_EXACT, true)["src"];
        } elseif (!empty($arPlacements[$arr_input_fields["placement_id"]]["PROPERTY_PICTURES_VALUE"])) {
            $arr_basket_item['PICTURE'] = CFile::ResizeImageGet($arPlacements[$arr_input_fields["placement_id"]]["PROPERTY_PICTURES_VALUE"][0], array('width' => 250, 'height' => 250), BX_RESIZE_IMAGE_EXACT, true)["src"];
        }

        $this->arResult["BASKET"][] = $arr_basket_item;
    }

    protected function _setTransferBasketItems(string $basket_item_id, array $arr_input_fields) {

        // пересчет стоимости проезда
        $calculation_result = \travelsoft\booking\Utils::getCalculationResult("transfer", array(
                    "services_id" => array($arr_input_fields["service_id"]),
                    "rates_id" => array($arr_input_fields["rate_id"]),
                    "date" => $arr_input_fields["date"],
                    "adults" => $arr_input_fields["adults"],
                    "children" => $arr_input_fields["children"],
                    "children_age" => $arr_input_fields["children_age"]
                ))[$arr_input_fields["service_id"]][$arr_input_fields["rate_id"]];


        $arr_basket_item = array(
            "BASKET_ITEM_ID" => $basket_item_id,
            "CALCULATIONS" => $calculation_result,
            "SOURCE" => $arr_input_fields,
            "TYPE" => "transfer",
            "PICTURE" => null,
            "TRANSFER_NAME" => \travelsoft\booking\stores\Buses::nameById($arr_input_fields["service_id"]) . "[" . travelsoft\booking\stores\Rates::nameById($arr_input_fields["rate_id"]) . "]",
            "DATE_FROM_TRANSFER" => $arr_input_fields["date"],
            "ADULTS" => $arr_input_fields["adults"],
            "CHILDREN" => $arr_input_fields["children"],
            "POSITION" => $basket_item_id,
        );

        if ($calculation_result["result_price"] > 0) {

            if (\travelsoft\booking\adapters\User::isAgent() && $calculation_result["discount_price"] > 0) {
                $tmp_price = $calculation_result["result_price"] + $calculation_result["discount_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            } else {
                $tmp_price = $calculation_result["result_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            }

            $arr_basket_item["CAN_BUY"] = true;
            $arr_basket_item["PRICE_FORMATTED"] = $tmp_price_formatted;
            $arr_basket_item["PRICE"] = $tmp_price;
            $arr_basket_item["CURRENCY"] = $calculation_result["currency"];
        } else {
            $arr_basket_item["CAN_BUY"] = false;
            $arr_basket_item["PRICE_FORMATTED"] = "";
            $arr_basket_item["PRICE"] = 0.00;
            $arr_basket_item["CURRENCY"] = "";
        }

        $this->arResult["BASKET"][] = $arr_basket_item;
    }

    protected function _setTransferbackBasketItems(string $basket_item_id, array $arr_input_fields) {

        // пересчет стоимости проезда
        $calculation_result = \travelsoft\booking\Utils::getCalculationResult("transferback", array(
                    "services_id" => array($arr_input_fields["service_id"]),
                    "rates_id" => array($arr_input_fields["rate_id"]),
                    "date" => $arr_input_fields["date"],
                    "adults" => $arr_input_fields["adults"],
                    "children" => $arr_input_fields["children"],
                    "children_age" => $arr_input_fields["children_age"]
                ))[$arr_input_fields["service_id"]][$arr_input_fields["rate_id"]];


        $arr_basket_item = array(
            "BASKET_ITEM_ID" => $basket_item_id,
            "CALCULATIONS" => $calculation_result,
            "SOURCE" => $arr_input_fields,
            "TYPE" => "transferback",
            "PICTURE" => null,
            "TRANSFER_NAME" => \travelsoft\booking\stores\Buses::nameById($arr_input_fields["service_id"]) . "[" . travelsoft\booking\stores\Rates::nameById($arr_input_fields["rate_id"]) . "]",
            "DATE_FROM_TRANSFER" => $arr_input_fields["date"],
            "ADULTS" => $arr_input_fields["adults"],
            "CHILDREN" => $arr_input_fields["children"],
            "POSITION" => $basket_item_id,
        );

        if ($calculation_result["result_price"] > 0) {

            if (\travelsoft\booking\adapters\User::isAgent() && $calculation_result["discount_price"] > 0) {
                $tmp_price = $calculation_result["result_price"] + $calculation_result["discount_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            } else {
                $tmp_price = $calculation_result["result_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            }

            $arr_basket_item["CAN_BUY"] = true;
            $arr_basket_item["PRICE_FORMATTED"] = $tmp_price_formatted;
            $arr_basket_item["PRICE"] = $tmp_price;
            $arr_basket_item["CURRENCY"] = $calculation_result["currency"];
        } else {
            $arr_basket_item["CAN_BUY"] = false;
            $arr_basket_item["PRICE_FORMATTED"] = "";
            $arr_basket_item["PRICE"] = 0.00;
            $arr_basket_item["CURRENCY"] = "";
        }

        $this->arResult["BASKET"][] = $arr_basket_item;
    }

    protected function _setExcursionTourBasketItems(string $basket_item_id, array $arr_input_fields) {

        // пересчет стоимости проезда
        $calculation_result = @current(\travelsoft\booking\Utils::getCalculationResult("excursiontour", array(
                            "services_id" => [$arr_input_fields["service_id"]],
                            "rates_id" => [$arr_input_fields["rate_id"]],
                            "date" => $arr_input_fields["date"],
                            "date_from" => $arr_input_fields["date"],
                            "date_to" => $arr_input_fields["date"],
                            "adults" => $arr_input_fields["adults"],
                            "children" => $arr_input_fields["children"],
                            "children_age" => $arr_input_fields["children_age"]
                )))["{$arr_input_fields["rate_id"]}~{$arr_input_fields["city_id"]}"][$arr_input_fields["date"]];

        $arr_basket_item = array(
            "BASKET_ITEM_ID" => $basket_item_id,
            "CALCULATIONS" => $calculation_result,
            "SOURCE" => $arr_input_fields,
            "TYPE" => "excursiontour",
            "PICTURE" => null,
            "TRANSFER_NAME" => \travelsoft\booking\stores\Buses::nameById($arr_input_fields["service_id"]) . "[" . travelsoft\booking\stores\Rates::nameById($arr_input_fields["rate_id"]) . "]",
            "DATE_FROM_TRANSFER" => $arr_input_fields["date"],
            "ADULTS" => $arr_input_fields["adults"],
            "CHILDREN" => $arr_input_fields["children"],
            "POSITION" => $basket_item_id,
        );

        if ($calculation_result["result_price"] > 0) {

            if (\travelsoft\booking\adapters\User::isAgent() && $calculation_result["discount_price"] > 0) {
                $tmp_price = $calculation_result["result_price"] + $calculation_result["discount_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            } else {
                $tmp_price = $calculation_result["result_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            }

            $arr_basket_item["CAN_BUY"] = true;
            $arr_basket_item["PRICE_FORMATTED"] = $tmp_price_formatted;
            $arr_basket_item["PRICE"] = $tmp_price;
            $arr_basket_item["CURRENCY"] = $calculation_result["currency"];
        } else {
            $arr_basket_item["CAN_BUY"] = false;
            $arr_basket_item["PRICE_FORMATTED"] = "";
            $arr_basket_item["PRICE"] = 0.00;
            $arr_basket_item["CURRENCY"] = "";
        }

        $this->arResult["BASKET"][] = $arr_basket_item;
    }

    /**
     * @param string $basket_item_id
     * @param array $arr_input_fields
     */
    protected function _setPlacementsBasketItems(string $basket_item_id, array $arr_input_fields) {

        $arRooms = $arPlacements = array();

        // пересчет стоимости размещения
        $calculation_result = \travelsoft\booking\Utils::getCalculationResult("placements", array(
                    "services_id" => array($arr_input_fields["service_id"]),
                    "rooms_id" => array($arr_input_fields["room_id"]),
                    "rates_id" => array($arr_input_fields["placement_rate_id"]),
                    "date_from" => $arr_input_fields["date_from"],
                    "date_to" => $arr_input_fields["date_to"],
                    "adults" => $arr_input_fields["adults"],
                    "children" => $arr_input_fields["children"],
                    "children_age" => $arr_input_fields["children_age"]
                ))[$arr_input_fields["service_id"]][$arr_input_fields["room_id"]][$arr_input_fields["placement_rate_id"]];

        if (!isset($arRooms[$arr_input_fields["room_id"]])) {
            $arRooms[$arr_input_fields["room_id"]] = \travelsoft\booking\stores\Rooms::getById(intval($arr_input_fields["room_id"]), array("ID", "UF_NAME", "UF_PICTURES"));
        }
        if (!isset($arPlacements[$arr_input_fields["placement_id"]])) {
            $arPlacements[$arr_input_fields["service_id"]] = travelsoft\booking\stores\Placements::getById(intval($arr_input_fields["service_id"]), array("ID", "NAME", "PROPERTY_PICTURES"));
        }

        $arr_basket_item = array(
            "BASKET_ITEM_ID" => $basket_item_id,
            "CALCULATIONS" => $calculation_result,
            "SOURCE" => $arr_input_fields,
            "TYPE" => "placements",
            "PICTURE" => null,
            "PLACEMENT_NAME" => $arPlacements[$arr_input_fields["service_id"]]["~NAME"],
            "ROOM_NAME" => $arRooms[$arr_input_fields["room_id"]]["UF_NAME"],
            "TRANSFER" => false,
            "TRANSFER_NAME" => "",
            "DATE_FROM" => $arr_input_fields["date_from"],
            "DATE_TO" => $arr_input_fields["date_to"],
            "DATE_FROM_PLACEMENT" => $arr_input_fields["date_from_placement"],
            "DATE_TO_PLACEMENT" => $arr_input_fields["date_to_placement"],
            "ADULTS" => $arr_input_fields["adults"],
            "CHILDREN" => $arr_input_fields["children"],
            "POSITION" => $basket_item_id,
            "FOOD" => $arr_input_fields["food_name"]
        );

        if ($calculation_result["result_price"] > 0) {

            if (\travelsoft\booking\adapters\User::isAgent() && $calculation_result["discount_price"] > 0) {
                $tmp_price = $calculation_result["result_price"] + $calculation_result["discount_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            } else {
                $tmp_price = $calculation_result["result_price"];
                $tmp_price_formatted = $this->arResult["CONVERTER"]->convertWithFormatting($tmp_price, $calculation_result["currency"]);
            }

            $arr_basket_item["CAN_BUY"] = true;
            $arr_basket_item["PRICE_FORMATTED"] = $tmp_price_formatted;
            $arr_basket_item["PRICE"] = $tmp_price;
            $arr_basket_item["CURRENCY"] = $calculation_result["currency"];
        } else {
            $arr_basket_item["CAN_BUY"] = false;
            $arr_basket_item["PRICE_FORMATTED"] = "";
            $arr_basket_item["PRICE"] = 0.00;
            $arr_basket_item["CURRENCY"] = "";
        }

        if (!empty($arRooms[$arr_input_fields["room_id"]]["UF_PICTURES"])) {
            $arr_basket_item['PICTURE'] = CFile::ResizeImageGet($arRooms[$arr_input_fields["room_id"]]["UF_PICTURES"][0], array('width' => 250, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true)["src"];
        } elseif (!empty($arPlacements[$arr_input_fields["service_id"]]["PROPERTY_PICTURES_VALUE"])) {
            $arr_basket_item['PICTURE'] = CFile::ResizeImageGet($arPlacements[$arr_input_fields["service_id"]]["PROPERTY_PICTURES_VALUE"][0], array('width' => 250, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true)["src"];
        }

        $this->arResult["BASKET"][] = $arr_basket_item;
    }

}

class ExtOptions extends Bitrix\Main\Config\Option {

    public static function changeLocaly($module, $name, $value) {
        self::$options["-"][$module][$name] = $value;
    }

    public static function getLocaly($module, $name) {
        return self::$options["-"][$module][$name];
    }

}
