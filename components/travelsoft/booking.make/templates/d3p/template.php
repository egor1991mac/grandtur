<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->setFrameMode(true);
?>
<div id="booking-box">
    <? if (!$arResult["IS_AVAIL_BOOKING"]): ?>
        <div class="alert alert-danger"><?= GetMessage("TRAVELBOOKING_BOOKING_NOT_AVAIL2") ?></div>
    <?
    elseif (!empty($arResult["BASKET"])):

        $total_people_count = $max_people_count = 0;

        foreach ($arResult["BASKET"] as $arItem) :

            if ($arItem["CAN_BUY"]) {
                $people_count = $arItem["ADULTS"] + $arItem["CHILDREN"];
                $total_people_count += $people_count;
                if ($max_people_count < $people_count) {
                    $max_people_count = $people_count;
                }
            }
            ?>
            <div class="panel panel-default basket-item">
                <div class="panel-heading"><h4><?= $arItem["PLACEMENT_NAME"] ?: $arItem["TRANSFER_NAME"] ?> <? if (!$arItem["CAN_BUY"]): ?>(<span class="can-buy-alert"><?= GetMessage("TRAVELBOOKING_CAN_BUY_ALERT") ?></span>)<? endif; ?></h4><div data-position="<?= $arItem["POSITION"] ?>" class="deleter">&times;</div></div>
                <div class="panel-body">
                    <? if ($arItem["PICTURE"]): ?>
                        <div class="col-md-3"><div class="picture"><img src="<?= $arItem["PICTURE"] ?>"></div></div>
        <? endif ?>
                    <div class="col-md-3">
                        <ul class="details">
                            <li><?= GetMessage("TRAVELBOOKING_POSITION_TITLE") ?>: <b><?= $arItem["BASKET_ITEM_ID"] ?></b></li>
                            <? if (key_exists("FOOD", $arItem)): ?>
                            <li><?= GetMessage("TRAVELBOOKING_ROOM_NAME_TITLE") ?>: <b><?= $arItem["ROOM_NAME"] ?></b></li>
                            <?endif;?>
                            <? if (key_exists("FOOD", $arItem)): ?>
                                <li><?= GetMessage("TRAVELBOOKING_FOOD_TITLE") ?>: <b><?= (strlen($arItem["FOOD"]) ? $arItem["FOOD"] : GetMessage("TRAVELBOOKING_WITHOUT_FOOD_TITLE")) ?></b></li>
                            <? endif ?>
                            <? if (key_exists("TRANSFER", $arItem)): ?>
                                <li><?= GetMessage("TRAVELBOOKING_TRAVEL_NAME_TITLE") ?>: <b><?= $arItem["TRANSFER"] ? GetMessage("TRAVELBOOKING_WITH_TRAVEL_TITLE") : GetMessage("TRAVELBOOKING_WITHOUT_TRAVEL_TITLE") ?></b></li>
                            <? endif ?>
                            <? if (strlen($arItem["DATE_FROM_TRANSFER"]) > 0): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_TRAVEL_TITLE") ?>: <b><?= $arItem["DATE_FROM_TRANSFER"] ?></b></li>
                            <? endif ?>
                            <? if (strlen($arItem["DATE_FROM_PLACEMENT"]) > 0): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_FROM_TITLE") ?>: <b><?= $arItem["DATE_FROM_PLACEMENT"] ?></b></li>
                            <? endif ?>
                            <? if (strlen($arItem["DATE_TO_PLACEMENT"]) > 0): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_TO_TITLE") ?>: <b><?= $arItem["DATE_TO_PLACEMENT"] ?></b></li>
                            <? endif ?>
                            <? if (strlen($arItem["DATE_BACK_TRANSFER"]) > 0): ?>
                                <li><?= GetMessage("TRAVELBOOKING_DATE_BACK_TRAVEL_TITLE") ?>: <b><?= $arItem["DATE_BACK_TRANSFER"] ?></b></li>
        <? endif ?>
                            <li><?= GetMessage("TRAVELBOOKING_ADULTS_TITLE") ?>: <b><?= $arItem["ADULTS"] ?></b></li>
                            <li><?= GetMessage("TRAVELBOOKING_CHILDREN_TITLE") ?>: <b><?= $arItem["CHILDREN"] ?></b></li>
                            <? if ($arItem["CAN_BUY"]): ?>
                                <li><?= GetMessage("TRAVELBOOKING_PRICE_TITLE") ?>: <b><?= $arItem["PRICE_FORMATTED"] ?></b></li>
        <? endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
            <? if (!$arResult["USER_IS_AUTH"]): ?>
            <form id="identification-form" action="#" method="post">
        <?= bitrix_sessid_post() ?>
                <input type="hidden" name="action" value="authorization">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12"><h4 id="identification-form-title"><?= GetMessage("TRAVELBOOKING_AUTHORIZATION") ?></h4></div>
                        <div class="col-md-12 errors-box" id="identification-errors-box"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?= GetMessage("TRAVELBOOKING_EMAIL") ?></label>
                                <input class="form-control" type="email" name="email" value="" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label><?= GetMessage("TRAVELBOOKING_PASSWORD") ?></label>
                                <input class="form-control" type="password" name="password" value="" autocomplete="off">
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary" type="submit"><?= GetMessage("TRAVELBOOKING_AUTHORIZATION_BTN") ?></button>
                                <span class="loading hidden"></span>
                            </div>
                            <div class="text-right"><a href="/auth/?forgot_password=yes" >Забыли пароль ?</a></div>
                            <div class="text-right"><a id="toggle-context" href="javascript:void(0)"><?= GetMessage("TRAVELBOOKING_REGISTRATION") ?></a></div>
                        </div>
                    </div>
                </div>
            </form>
    <? endif ?>

        <div class="panel panel-default basket-item">
            <div class="panel-body">
                <form action="#" id="booking-form" method="post">
    <?= bitrix_sessid_post() ?>
                    <input type="hidden" value="booking" name="action">
                    <div id="tourists-box" class="row">
    <? for ($i = 1; $i <= $max_people_count; $i++): ?>
                            <div class="col-md-<? if ($total_people_count - $max_people_count > 0 || $max_people_count > 1): ?>6<? else: ?>12<? endif ?> col-sm-12 col-xs-12" id="tourist_<?= $i ?>">
                                <div class="col-md-12"><h4><?= GetMessage("TRAVELBOOKING_TOURIST") ?> <?= $i ?></h4></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_NAME") ?></label>
                                        <span data-error-for="UF_NAME-<?= $i ?>" class="errors-box hidden"></span>
                                        <input data-index="<?= $i ?>" class="form-control" name="tourists[<?= $i ?>][UF_NAME]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAST_NAME") ?></label>
                                        <span data-error-for="UF_LAST_NAME-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_LAST_NAME]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_SECOND_NAME") ?></label>
                                        <span data-error-for="UF_SECOND_NAME-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_SECOND_NAME]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAT_NAME") ?></label>
                                        <span data-error-for="UF_LAT_NAME-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_LAT_NAME]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAT_LAST_NAME") ?></label>
                                        <span data-error-for="UF_LAT_LAST_NAME-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_LAT_LAST_NAME]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_MALE") ?></label>
                                        <span data-error-for="UF_MALE-<?= $i ?>" class="errors-box hidden"></span>
                                        <select class="form-control" name="tourists[<?= $i ?>][UF_MALE]">
                                            <option value="<?= GetMessage("TRAVELBOOKING_TOURIST_MAN_MALE") ?>"><?= GetMessage("TRAVELBOOKING_TOURIST_MAN_MALE") ?></option>
                                            <option value="<?= GetMessage("TRAVELBOOKING_TOURIST_WOMAN_MALE") ?>"><?= GetMessage("TRAVELBOOKING_TOURIST_WOMAN_MALE") ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield form-group-icon-left">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_BIRTHDATE") ?></label>
                                        <span data-error-for="UF_BIRTHDATE-<?= $i ?>" class="errors-box hidden"></span>
                                        <div class="prel">
                                            <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                            <input class="form-control date-input" name="tourists[<?= $i ?>][UF_BIRTHDATE]" value="" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_SERIES") ?></label>
                                        <span data-error-for="UF_PASS_SERIES-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_PASS_SERIES]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_NUMBER") ?></label>
                                        <span data-error-for="UF_PASS_NUMBER-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_PASS_NUMBER]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_ISSUED_BY") ?></label>
                                        <span data-error-for="UF_PASS_ISSUED_BY-<?= $i ?>" class="errors-box hidden"></span>
                                        <input class="form-control" name="tourists[<?= $i ?>][UF_PASS_ISSUED_BY]" value="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group tfield form-group-icon-left">
                                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_DATE_ISSUE") ?></label>
                                        <span data-error-for="UF_PASS_DATE_ISSUE-<?= $i ?>" class="errors-box hidden"></span>
                                        <div class="prel">
                                            <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                            <input class="form-control date-input" name="tourists[<?= $i ?>][UF_PASS_DATE_ISSUE]" value="" type="text">
                                        </div>
                                    </div>
                                </div>
                                <? if (count($arResult["BASKET"]) === 1): ?>
                                    <input type="hidden" name="tourists[<?= $i ?>][LINK_TO_BASKET][]" value="<?= $arResult["BASKET"][0]["BASKET_ITEM_ID"] ?>">
        <? else: ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group prel">
                                            <label><?= GetMessage("TRAVELBOOKING_TOURIST_LINK_TO_BASKET") ?></label>
                                            <span data-error-for="LINK_TO_BASKET-<?= $i ?>" class="errors-box hidden"></span>
                                            <div class="text-right form-control fantom"><div class="closer hidden">&times;</div></div>
                                            <div class="link2basket-box hidden">
                                                <? foreach ($arResult["BASKET"] as $arItem) : ?>
                <? if ($arItem["CAN_BUY"]): ?>
                                                        <div class="checkbox">


                                                            <label><input type="checkbox" name="tourists[<?= $i ?>][LINK_TO_BASKET][]" value="<?= $arItem["BASKET_ITEM_ID"] ?>"><?= $arItem["PLACEMENT_NAME"] . "[" . $arItem["BASKET_ITEM_ID"] . "]" ?></label>


                                                        </div>
                                                    <? endif ?>
            <? endforeach ?>
                                            </div>

                                        </div>
                                    </div>
                                <? endif ?>
        <? if (!$arResult["USER_IS_AGENT"]): ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12 is-buyer-radio-box">
                                        <div class="form-group">
                                            <label><?= GetMessage("TRAVELBOOKING_TOURIST_IS_BUYER") ?></label>
                                            <input class="is-buyer-radio" name="is_buyer" value="<?= $i ?>" type="radio">
                                        </div>
                                    </div>
                            <? endif ?>
                            </div>
                    <? endfor; ?>
                    </div>
    <? if ($total_people_count - $max_people_count > 0): ?>
                        <div id="tourists-add-btn-box" class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 text-right"><button type="button" class="btn btn-primary" id="add-tourist"><?= GetMessage("TRAVELBOOKING_ADD_TOURIST") ?></button></div>
                            </div>
                        </div>
                    <? endif ?>
    <? if (!$arResult["USER_IS_AGENT"]): ?>
                        <div id="buyer-fields-box" class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-12"><h4><?= GetMessage("TRAVELBOOKING_BYUER_INFO") ?></h4></div></div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[LAST_NAME]"><?= GetMessage("TRAVELBOOKING_TOURIST_LAST_NAME") ?></label>
                                    <span data-error-for="buyer[LAST_NAME]" class="errors-box hidden"></span>
                                    <input type="text" name="buyer[LAST_NAME]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["LAST_NAME"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[NAME]"><?= GetMessage("TRAVELBOOKING_TOURIST_NAME") ?></label>
                                    <span data-error-for="buyer[NAME]" class="errors-box hidden"></span>
                                    <input type="text" name="buyer[NAME]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["NAME"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[SECOND_NAME]"><?= GetMessage("TRAVELBOOKING_TOURIST_SECOND_NAME") ?></label>
                                    <span data-error-for="buyer[SECOND_NAME]" class="errors-box hidden"></span>
                                    <input type="text" name="buyer[SECOND_NAME]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["SECOND_NAME"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[PERSONAL_PHONE]"><?= GetMessage("TRAVELBOOKING_BUYER_PHONE") ?></label>
                                    <span data-error-for="buyer[PERSONAL_PHONE]" class="errors-box hidden"></span>
                                    <input type="phone" name="buyer[PERSONAL_PHONE]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["PERSONAL_PHONE"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group form-group-icon-left">
                                    <label for="buyer[PERSONAL_BIRTHDAY]"><?= GetMessage("TRAVELBOOKING_TOURIST_BIRTHDATE") ?></label>
                                    <span data-error-for="buyer[PERSONAL_BIRTHDAY]" class="errors-box hidden"></span>
                                    <div class="prel">
                                        <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                        <input type="text" name="buyer[PERSONAL_BIRTHDAY]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["PERSONAL_BIRTHDAY"]) ?>" class="form-control date-input">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[UF_PASS_SERIES]"><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_SERIES") ?></label>
                                    <span data-error-for="buyer[UF_PASS_SERIES]" class="errors-box hidden"></span>
                                    <input name="buyer[UF_PASS_SERIES]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["UF_PASS_SERIES"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[UF_PASS_NUMBER]"><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_NUMBER") ?></label>
                                    <span data-error-for="buyer[UF_PASS_NUMBER]" class="errors-box hidden"></span>
                                    <input name="buyer[UF_PASS_NUMBER]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["UF_PASS_NUMBER"]) ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group form-group-icon-left">
                                    <label for="buyer[UF_PASS_DATE_ISSUE]"><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_DATE_ISSUE") ?></label>
                                    <span data-error-for="buyer[UF_PASS_DATE_ISSUE]" class="errors-box hidden"></span>
                                    <div class="prel">
                                        <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                        <input name="buyer[UF_PASS_DATE_ISSUE]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["UF_PASS_DATE_ISSUE"]) ?>" class="form-control date-input">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-12 form-group">
                                    <label for="buyer[UF_PASS_ISSUED_BY]"><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_ISSUED_BY") ?></label>
                                    <span data-error-for="buyer[UF_PASS_ISSUED_BY]" class="errors-box hidden"></span>
                                    <input name="buyer[UF_PASS_ISSUED_BY]" value="<?= htmlspecialcharsbx($arResult["USER_FIELDS"]["UF_PASS_ISSUED_BY"]) ?>" class="form-control">
                                </div>
                            </div>
                        </div>
    <? endif ?>
                    <div id="buyer-btn-box" class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label><?= GetMessage("TRAVELBOOKING_COMMENT") ?></label>
                                <textarea class="form-control" name="comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center" id="booking-btn-box">
                        <span class="loading hidden"></span>
                        <button id="booking-btn" class="btn btn-lg btn-primary"><?= GetMessage("TRAVELBOOKING_BOOKING_BTN") ?></button>
                    </div>
                </form>
            </div>
        </div>
        <template id="confirm_password_template">
            <div class="form-group">
                <label><?= GetMessage("TRAVELBOOKING_CONFIRM_PASSWORD") ?></label>
                <input type="password" class="form-control" name="confirm_password" value="" autocomplete="off">
            </div>
        </template>
        <template id="phone_template">
            <div class="form-group">
                <label><?= GetMessage("TRAVELBOOKING_PHONE") ?></label>
                <input type="phone" class="form-control"name="phone" value="" autocomplete="off">
            </div>
        </template>
        <template id="ok_authorization_template">
            <div class="panel panel-default">
                <div class="panel-body"><h3 class="message-ok"><?= GetMessage("TRAVELBOOKING_AUTHORIZATION_OK") ?></h3></div>
            </div>
        </template>
        <template id="ok_registration_template">
            <div class="panel panel-default">
                <div class="panel-body"><h3 class="message-ok"><?= GetMessage("TRAVELBOOKING_REGISTRATION_OK") ?></h3></div>
            </div>
        </template>
        <template id="tourists_template">
            <div class="col-md-6 col-sm-12 col-xs-12" id="tourist_{{index}}">
                <div class="col-md-12"><h4><?= GetMessage("TRAVELBOOKING_TOURIST") ?> {{index}}</h4></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_NAME") ?></label>
                        <span data-error-for="UF_NAME-{{index}}" class="errors-box hidden"></span>
                        <input data-index="{{index}}" class="form-control" name="tourists[{{index}}][UF_NAME]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAST_NAME") ?></label>
                        <span data-error-for="UF_LAST_NAME-{{index}}"  class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_LAST_NAME]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_SECOND_NAME") ?></label>
                        <span data-error-for="UF_SECOND_NAME-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_SECOND_NAME]" value="" type="text">
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAT_NAME") ?></label>
                        <span data-error-for="UF_LAT_NAME-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_LAT_NAME]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_LAT_LAST_NAME") ?></label>
                        <span  data-error-for="UF_LAT_LAST_NAME-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_LAT_LAST_NAME]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_MALE") ?></label>
                        <span data-error-for="UF_MALE-{{index}}" class="errors-box hidden"></span>
                        <select class="form-control" name="tourists[{{index}}][UF_MALE]">
                            <option value="<?= GetMessage("TRAVELBOOKING_TOURIST_MAN_MALE") ?>"><?= GetMessage("TRAVELBOOKING_TOURIST_MAN_MALE") ?></option>
                            <option value="<?= GetMessage("TRAVELBOOKING_TOURIST_WOMAN_MALE") ?>"><?= GetMessage("TRAVELBOOKING_TOURIST_WOMAN_MALE") ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield form-group-icon-left">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_BIRTHDATE") ?></label>
                        <span data-error-for="UF_BIRTHDATE-{{index}}" class="errors-box hidden"></span>
                        <div class="prel">
                            <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <input class="form-control date-input" name="tourists[{{index}}][UF_BIRTHDATE]" value="" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_SERIES") ?></label>
                        <span data-error-for="UF_PASS_SERIES-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_PASS_SERIES]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_NUMBER") ?></label>
                        <span data-error-for="UF_PASS_NUMBER-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_PASS_NUMBER]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_ISSUED_BY") ?></label>
                        <span data-error-for="UF_PASS_ISSUED_BY-{{index}}" class="errors-box hidden"></span>
                        <input class="form-control" name="tourists[{{index}}][UF_PASS_ISSUED_BY]" value="" type="text">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group tfield form-group-icon-left">
                        <label><?= GetMessage("TRAVELBOOKING_TOURIST_PASS_DATE_ISSUE") ?></label>
                        <span data-error-for="UF_PASS_DATE_ISSUE-{{index}}" class="errors-box hidden"></span>
                        <div class="prel">
                            <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <input class="form-control date-input" name="tourists[{{index}}][UF_PASS_DATE_ISSUE]" value="" type="text">
                        </div>
                    </div>
                </div>
                <? if (count($arResult["BASKET"]) === 1): ?>
                    <input type="hidden" name="tourists[{{index}}][LINK_TO_BASKET][]" value="<?= $arResult["BASKET"][0]["BASKET_ITEM_ID"] ?>">
    <? else: ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group prel">
                            <label><?= GetMessage("TRAVELBOOKING_TOURIST_LINK_TO_BASKET") ?></label>
                            <span data-error-for="LINK_TO_BASKET-{{index}}" class="errors-box hidden"></span>
                            <div class="text-right form-control fantom"><div class="closer hidden">&times;</div></div>
                            <div class="link2basket-box hidden">
                                <? foreach ($arResult["BASKET"] as $arItem) : ?>
            <? if ($arItem["CAN_BUY"]): ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="tourists[{{index}}][LINK_TO_BASKET][]" value="<?= $arItem["BASKET_ITEM_ID"] ?>"><?= $arItem["PLACEMENT_NAME"] . "[" . $arItem["BASKET_ITEM_ID"] . "]" ?></label>
                                        </div>
                                    <? endif ?>
        <? endforeach ?>
                            </div>

                        </div>
                    </div>
                <? endif ?>
    <? if (!$arResult["USER_IS_AGENT"]): ?>
                    <div class="col-md-6 col-sm-6 col-xs-12 is-buyer-radio-box">
                        <div class="form-group">
                            <label><?= GetMessage("TRAVELBOOKING_TOURIST_IS_BUYER") ?></label>
                            <input class="is-buyer-radio" name="is_buyer" value="{{index}}" type="radio">
                        </div>
                    </div>
    <? endif ?>
            </div>
        </template>
        <script>
            window.jsBookingExtData = {
                url: "<?= $componentPath ?>/ajax/processor.php",
                basketItemsCount: <?= count($arResult["BASKET"]) ?>,
                voucher_detail_page: "/personal/vouchers/detail.php?voucher_id={{voucher_id}}",
                userId: "<?= $arResult["USER_ID"] ?>",
                isAuth: <? if ($arResult["USER_IS_AUTH"]): ?>true<? else: ?>false<? endif ?>,
                        isAvailBooking: <? if ($arResult["IS_AVAIL_BOOKING"]): ?>true<? else: ?>false<? endif ?>,
                                peopleCount: <?= ($total_people_count - $max_people_count) ?>,
                                lastTouristIndex: <?= $i ?>,
                                messages: {
                                    booking_not_avail: "<?= GetMessage("TRAVELBOOKING_BOOKING_NOT_AVAIL") ?>",
                                    booking_not_avail2: "<?= GetMessage("TRAVELBOOKING_BOOKING_NOT_AVAIL2") ?>",
                                    empty_basket: '<div class="alert alert-danger"><?= GetMessage("TRAVELBOOKING_EMPTY_BASKET") ?></div>',
                                    authorization: "<?= GetMessage("TRAVELBOOKING_AUTHORIZATION") ?>",
                                    authorization_btn: "<?= GetMessage("TRAVELBOOKING_AUTHORIZATION_BTN") ?>",
                                    authorization_fail: "<?= GetMessage("TRAVELBOOKING_AUTHORIZATION_FAIL") ?>",
                                    registration: "<?= GetMessage("TRAVELBOOKING_REGISTRATION") ?>",
                                    registration_btn: "<?= GetMessage("TRAVELBOOKING_REGISTRATION_BTN") ?>",
                                    email_already_exists: "<?= GetMessage("TRAVELBOOKING_EMAIL_ALREADY_EXISTS") ?>",
                                    wrong_phone: "<?= GetMessage("TRAVELBOOKING_WRONG_PHONE") ?>",
                                    not_avail_basket_items: "<?= GetMessage("TRAVELBOOKING_NOT_AVAIL_BASKET_ITEMS") ?>",
                                    calendar: {
                                        days: '<?= GetMessage("TRAVELBOOKING_CALENDAR_DAYS")?>',
                                        days_short: '<?= GetMessage("TRAVELBOOKING_CALENDAR_DAYS_SHORT")?>',
                                        days_min: '<?= GetMessage("TRAVELBOOKING_CALENDAR_DAYS_MIN")?>',
                                        months: '<?= GetMessage("TRAVELBOOKING_CALENDAR_MONTHS")?>',
                                        months_short: '<?= GetMessage("TRAVELBOOKING_CALENDAR_MONTHS_SHORT")?>',
                                        clear: '<?= GetMessage("TRAVELBOOKING_CALENDAR_CLEAR")?>',
                                        today: '<?= GetMessage("TRAVELBOOKING_CALENDAR_TODAY")?>',
                                    },
                                    err: {
                                        EMPTY: "<?= GetMessage("TRAVELBOOKING_EMPTY_ERR") ?>",
                                        WRONG_LINK_TO_BASKET: "<?= GetMessage("TRAVELBOOKING_WRONG_LINK_TO_BASKET_ERR") ?>",
                                        WRONG_LAT: "<?= GetMessage("TRAVELBOOKING_WRONG_LAT_ERR") ?>",
                                        WRONG_NUMBERS: "<?= GetMessage("TRAVELBOOKING_WRONG_NUMBERS_ERR") ?>",
                                        WRONG_PHONE: "<?= GetMessage("TRAVELBOOKING_WRONG_PHONE") ?>"
                                    }
                                }
                            };
        </script>
    <? else: ?>
        <div class="alert alert-danger"><?= GetMessage("TRAVELBOOKING_EMPTY_BASKET") ?></div>
<? endif; ?>
</div>
