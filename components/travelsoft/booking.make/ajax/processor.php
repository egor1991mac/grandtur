<?php

/*
 * Статусы ответов:
 *                  0 - не удалось удалить позицию в корзине
 *                  1 - удаление позиции прошло удачно
 *                  2 - авторизация прошла успешно
 *                  3 - авторизация провалилась.
 *                  4 - регистрация провалилась.
 *                  5 - данный email уже существует.
 *                  6 - регистрация прошла успешно.
 *                  7 - телефон не соответствует формату.
 */

define('PUBLIC_AJAX_MODE', true);
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

CBitrixComponent::includeComponentClass("travelsoft:booking.make");

$component = new TravelsoftBookingMake;

try {

    if ($_REQUEST["action"] === "delete_position") {
        
        throw new Exception(json_encode(
                array("status" => $component->initBasket()->deleteBasketPosition($_REQUEST['position']))));
        
    } elseif ($_REQUEST["action"] === "authorization") {

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            
            throw new Exception(json_encode($component->userAuthorization()));
            
        }
    } elseif ($_REQUEST["action"] === "registration") {

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            
            throw new Exception(json_encode($component->userRegistration()));
            
        }
        
    } elseif ($_REQUEST["action"] === "get_tourists") {
        
        throw new Exception(json_encode($component->getTouristsByClientId((int)$_GET["client_id"], (string)$_GET["q"])));
        
    } elseif ($_REQUEST["action"] === "booking") {
        
        throw new Exception(json_encode($component->booking()));
        
    }
} catch (Exception $e) {

    header('Content-Type: application/json; charset=' . SITE_CHARSET);
    echo $e->getMessage();
    exit;
}
